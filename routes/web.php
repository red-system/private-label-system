<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::get('/redirect/{link}', "Login@index2")->name("loginPageRedirect");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin:administrator|distributor', 'prefix' => 'admin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::post('/growth', "Dashboard@import")->name('dashboardGrowth');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');
        Route::get('cannot-access', "Dashboard@cannot")->name('cannotPage');
        Route::get('company-not-found', "Dashboard@company_null")->name('companyNullPage');
        Route::get('support-center/{id}', "Dashboard@support_center")->name('supportCenter');
        Route::get('/services', "Services@index")->name('servicesPage');
        Route::get('/services-detail/{id}', "Services@services_detail")->name('servicesDetail');

    });
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Client //
    Route::get('/client', "Client@index")->name('clientPage');
    Route::post('/client', "Client@insert")->name('clientInsert');
    Route::get('/client-list', "Client@list")->name('clientList');
    Route::delete('/client/{id}', "Client@delete")->name('clientDelete');
    Route::post('/client/{id}', "Client@update")->name('clientUpdate');

    // Agency //
    Route::get('/agency', "Agency@index")->name('agencyPage');
    Route::post('/agency', "Agency@insert")->name('agencyInsert');
    Route::post('/agency/import', "Agency@import")->name('agencyImport');
    Route::get('/agency-list', "Agency@list")->name('agencyList');
    Route::delete('/agency/{id}', "Agency@delete")->name('agencyDelete');
    Route::post('/agency/{id}', "Agency@update")->name('agencyUpdate');

    // Agent //
    Route::get('/agent', "Agent@index")->name('agentPage');
    Route::post('/agent', "Agent@insert")->name('agentInsert');
    Route::get('/agent-list', "Agent@list")->name('agentList');
    Route::delete('/agent/{id}', "Agent@delete")->name('agentDelete');
    Route::post('/agent/{id}', "Agent@update")->name('agentUpdate');

    // Admin //
    Route::get('/admin', "Admin@index")->name('adminPage');
    Route::post('/admin', "Admin@insert")->name('adminInsert');
    Route::get('/admin-list', "Admin@list")->name('adminList');
    Route::delete('/admin/{id}', "Admin@delete")->name('adminDelete');
    Route::post('/admin/{id}', "Admin@update")->name('adminUpdate');

    // ProjectType //
    Route::get('/project-type', "ProjectType@index")->name('projectTypePage');
    Route::post('/project-type', "ProjectType@insert")->name('projectTypeInsert');
    Route::get('/project-type-list', "ProjectType@list")->name('projectTypeList');
    Route::delete('/project-type/{id}', "ProjectType@delete")->name('projectTypeDelete');
    Route::post('/project-type/{id}', "ProjectType@update")->name('projectTypeUpdate');

    // AgencyType //
    Route::get('/agency-type', "AgencyType@index")->name('agencyTypePage');
    Route::post('/agency-type', "AgencyType@insert")->name('agencyTypeInsert');
    Route::get('/agency-type-list', "AgencyType@list")->name('agencyTypeList');
    Route::delete('/agency-type/{id}', "AgencyType@delete")->name('agencyTypeDelete');
    Route::post('/agency-type/{id}', "AgencyType@update")->name('agencyTypeUpdate');

    // Status //
    Route::get('/status', "Status@index")->name('statusPage');
    Route::post('/status', "Status@insert")->name('statusInsert');
    Route::get('/status-list', "Status@list")->name('statusList');
    Route::delete('/status/{id}', "Status@delete")->name('statusDelete');
    Route::post('/status/{id}', "Status@update")->name('statusUpdate');

    // Channel
    Route::get('/channel', "Channel@index")->name('channelPage');
    Route::post('/channel', "Channel@insert")->name('channelInsert');
    Route::get('/channel-list', "Channel@list")->name('channelList');
    Route::delete('/channel/{id}', "Channel@delete")->name('channelDelete');
    Route::post('/channel/{id}', "Channel@update")->name('channelUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::get('/new-user/', "User@create")->name('userCreate');
    Route::get('/user/{id}', "User@edit")->name('userEdit');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
//    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
//    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/{id}', "UserRole@edit")->name('userRoleEdit');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    //Service
    Route::get('/service', "Service@index")->name('servicePage');
    Route::post('/service', "Service@insert")->name('servicesInsert');
    Route::get('/service-list', "Service@list")->name('servicesList');
    Route::delete('/service/{id}', "Service@delete")->name('servicesDelete');
    Route::post('/service/{id}', "Service@update")->name('servicesUpdate');

    //Service-Content
    Route::get('/services-content', "ServicesContent@index")->name('contentPage');
    Route::post('/services-content', "ServicesContent@insert")->name('contentInsert');
    Route::get('/services-content-list', "ServicesContent@list")->name('contentList');
    Route::delete('/services-content/{id}', "ServicesContent@delete")->name('contentDelete');
    Route::post('/services-content/{id}', "ServicesContent@update")->name('contentUpdate');
    Route::post('/services-content-detail/{id}', "ServicesContent@update_detail")->name('contentUpdateDetail');

    //Company
    Route::get('/company', "Company@index")->name('companyPage');
    Route::post('/company', "Company@insert")->name('companyInsert');
    Route::get('/company-list', "Company@list")->name('companyList');
    Route::delete('/company/{id}', "Company@delete")->name('companyDelete');
    Route::post('/company/{id}', "Company@update")->name('companyUpdate');

    //Company Type
    Route::get('/company-type', "CompanyType@index")->name('companyTypePage');
    Route::post('/company-type', "CompanyType@insert")->name('companyTypeInsert');
    Route::get('/company-type-list', "CompanyType@list")->name('companyTypeList');
    Route::delete('/company-type/{id}', "CompanyType@delete")->name('companyTypeDelete');
    Route::post('/company-type/{id}', "CompanyType@update")->name('companyTypeUpdate');

    //Project Status
    Route::get('/project-status', "ProjectStatus@index")->name('projectStatusPage');
    Route::post('/project-status', "ProjectStatus@insert")->name('projectStatusInsert');
    Route::get('/project-status-list', "ProjectStatus@list")->name('projectStatusList');
    Route::delete('/project-status/{id}', "ProjectStatus@delete")->name('projectStatusDelete');
    Route::post('/project-status/{id}', "ProjectStatus@update")->name('projectStatusUpdate');

    // Support Center //
    Route::get('/support', "Support@index")->name('supportPage');
    Route::post('/support', "Support@insert")->name('supportInsert');
    Route::get('/support-list', "Support@list")->name('supportList');
    Route::delete('/support/{id}', "Support@delete")->name('supportDelete');
    Route::post('/support/{id}', "Support@update")->name('supportUpdate');

    //Support Data
    Route::get('/support-data/{id}', "Support@data_index")->name('supportDataPage');
    Route::post('/support-data', "Support@data_insert")->name('supportDataInsert');
    Route::get('/support-data-list/{id}', "Support@data_list")->name('supportDataList');
    Route::delete('/support-data/{id}', "Support@data_delete")->name('supportDataDelete');
    Route::post('/support-data/{id}', "Support@data_update")->name('supportDataUpdate');

});
// ========= Management ==========
Route::group(['namespace' => 'Project', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {
    // Project List Agency
    Route::get('/project', "ProjectList@index")->name('projectListPage');
    Route::get('/project/{id}', "ProjectList@index2")->name('projectPerPage');
    Route::post('/project', "ProjectList@insert")->name('projectListInsert');
    Route::get('/project-list', "ProjectList@list")->name('projectList');
    Route::delete('/project/{id}', "ProjectList@delete")->name('projectListDelete');
    Route::post('/project/{id}', "ProjectList@update")->name('projectListUpdate');
    Route::get('/project-detail/{id}', "ProjectList@detail")->name('projectListDetail');
    Route::get('/project-task/{id}', "ProjectList@tasks")->name('projectListTasks');
    Route::post('/project-task/{id}', "ProjectList@approve_tasks")->name('approveTasks');
    Route::post('/project-task', "ProjectList@insert_tasks")->name('projectListInsertTasks');
    Route::post('/project-metric/{id}', "ProjectList@insert_metric")->name('projectListInsertMetric');
    Route::get('/project-file/{id}', "ProjectList@file")->name('projectListFile');
    Route::post('/project-file', "ProjectList@file_insert")->name('projectListInsertFile');
    Route::get('/project-view/{id}', "ProjectView@index")->name('projectView');
    Route::get('/performance-dashboard/{id}', "ProjectList@performance_dashboard")->name('performanceDashboard');
    Route::get('/project-result/{id}', "ProjectList@project_result")->name('projectResult');
    Route::get('/project-client/{id}', "ProjectList@index_single")->name('projectSingle');
    Route::get('/project-list-single', "ProjectList@list")->name('projectListSingle');
    Route::post('/project-chat-insert', "ProjectList@insert_chat")->name('projectInsertChat');
    Route::post('/sub-project-insert', "ProjectList@insert_sub")->name('projectInsertSub');




    //Project Report
    Route::get('/project-report', "ProjectReport@index")->name('projectReportPage');
    Route::get('/project-report-list', "ProjectReport@list")->name('projectReportList');
    Route::get('/project-report-pdf/{id}', "ProjectReport@print")->name('projectReportPrint');

});
// ========= Agency ==========
Route::group(['namespace' => 'Agency', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {
    // myClient
    Route::get('/my-client', "MyClient@index")->name('myClientPage');
    Route::get('/my-client-list', "MyClient@list")->name('myClientList');
    Route::post('/my-client/commision-cost/{id}', "MyClient@commision_cost")->name('myClientCommisionCost');

    //Report
    Route::get('/report', "Report@index")->name('reportPage');
    Route::get('/report-list', "Report@list")->name('reportList');
    Route::get('/report-pdf', "Report@pdf")->name('reportPdf');
    Route::get('/report-excel', "Report@excel")->name('reportExcel');
});

Route::group(['namespace' => 'Client', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {
    // Client Info
    Route::get('/client-info', "ClientInfo@index")->name('clientInformationPage');
    Route::post('/client-info', "ClientInfo@update")->name('clientDataUpdate');

    // Company Info
    Route::get('/my-company', "ClientCompany@index")->name('clientCompanyPage');
    Route::post('/my-company', "ClientCompany@update")->name('companyDataUpdate');

    // Client Project
    Route::get('/my-project', "ClientProject@index")->name('clientProjectPage');
    Route::get('/my-project-list', "ClientProject@list")->name('clientProject');
//    Route::post('/my-project/{id}', "ClientProject@update")->name('clientProjectUpdate');
    Route::get('/my-project-detail/{id}', "ClientProject@detail")->name('clientProjectDetail');
    Route::get('/my-project-task/{id}', "ClientProject@tasks")->name('clientProjectTasks');
    Route::get('/my-project-report/{id}', "ClientProject@print")->name('clientProjectReport');
//    Route::post('/my-project-task', "ClientProject@insert_tasks")->name('clientProjectInsertTasks');
//    Route::get('/my-project-file/{id}', "ClientProject@file")->name('clientProjectFile');

    /**
     * piutang supplier
     */
    Route::get('/hutang-giro', "HutangGiro@index")->name('hutangGiroPage');
    Route::get('/hutang-giro-list', "HutangGiro@list")->name('hutangGiroList');
    Route::get('/hutang-giro/{id}', "HutangGiro@bayar")->name('hutangGiroBayar');
    Route::get('/hutang-giro/detail/{id}', "HutangGiro@detail")->name('hutangGiroDetail');
    Route::post('/hutang-giro', "HutangGiro@insert")->name('hutangGiroInsert');

});

Route::group(['namespace' => 'Chat', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {
    // Chat
    Route::get('/chat/{id}', "Chat@index_chat")->name('chatBox');
//    Route::get('/chat-list/{id}', "Chat@list")->name('chatList');
    Route::get('/chat-list', "Chat@list")->name('chatListProject');
    Route::get('/chat', "Chat@index")->name('chatPage');

//    Route::get('/my-client-list', "MyClient@list")->name('myClientList');
    Route::post('/chat', "Chat@insert_new_chat")->name('chatInsert');
    Route::post('/chat/{id}', "Chat@send_chat")->name('chatSend');

//    //Report
//    Route::get('/report', "Report@index")->name('reportPage');
//    Route::get('/report-list', "Report@list")->name('reportList');
//    Route::get('/report-pdf', "Report@pdf")->name('reportPdf');
//    Route::get('/report-excel', "Report@excel")->name('reportExcel');
});

// ========= Laporan ==========
Route::group(['namespace' => 'Laporan', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    Route::get('/laporan-rangkuman', "LaporanRangkuman@index")->name('laporanRangkumanPage');

    Route::get('/laporan-daftar', "LaporanDaftar@index")->name('laporanDaftarPage');

    Route::get('/laporan-stok/daftar-stok', "LaporanStok@index")->name('subDaftarStokPage');
    Route::get('/laporan-stok/daftar-stok-list', "LaporanStok@list")->name('subDaftarStokList');
    Route::get('/laporan-stok/daftar-stok-pdf', "LaporanStok@pdf")->name('subDaftarStokPdf');
    Route::get('/laporan-stok/daftar-stok-excel', "LaporanStok@excel")->name('subDaftarStokExcel');

    Route::get('/laporan-stok/mutasi-stok', "LaporanKartuStok@index")->name('subKartuStokPage');
    Route::get('/laporan-stok/mutasi-stok-list', "LaporanKartuStok@list")->name('laporanKartuStokList');
    Route::get('/laporan-stok/mutasi-stok-pdf', "LaporanKartuStok@pdf")->name('laporanKartuStokPdf');
    Route::get('/laporan-stok/mutasi-stok-excel', "LaporanKartuStok@excel")->name('laporanKartuStokExcel');

    Route::get('/laporan-history-harga', "LaporanHistoryHarga@index")->name('historyHargaPage');

    Route::get('/laporan-rekap-history-harga', "RekapHistoryHarga@index")->name('rekapHistoryHargaPage');

    Route::get('/laporan-history-harga-per-toko', "HistoryHargaPerToko@index")->name('historyHargaPerTokoPage');

    Route::get('/laporan-mutasi-harian', "MutasiHarian@index")->name('laporanMutasiHarianPage');

    Route::get('/laporan-profit-margin', "LaporanProfitMargin@index")->name('laporanProfitMarginPage');
    Route::get('/laporan-profit-margin-list', "LaporanProfitMargin@list")->name('laporanProfitMarginList');
    Route::get('/laporan-profit-margin-list-pdf', "LaporanProfitMargin@pdf")->name('laporanProfitMarginPdf');
    Route::get('/laporan-profit-margin-list-excel', "LaporanProfitMargin@excel")->name('laporanProfitMarginExcel');

    Route::get('/laporan-daftar-item-saldo-nol', "DaftarItemSaldoNol@index")->name('daftarItemSaldoNolPage');
    Route::get('/laporan-daftar-item-saldo-nol-pdf', "DaftarItemSaldoNol@pdf")->name('daftarItemSaldoNolPdf');
    Route::get('/laporan-daftar-item-saldo-nol-excel', "DaftarItemSaldoNol@excel")->name('daftarItemSaldoNolExcel');
    Route::get('/laporan-daftar-item-saldo-nol-list', "DaftarItemSaldoNol@list")->name('daftarItemSaldoNolList');

    Route::get('/laporan-daftar-item-over-stok', "DaftarItemOverStok@index")->name('daftarItemOverStokPage');

    Route::get('/laporan-stok/daftar-harga', "LaporanDaftarHarga@index")->name('subDaftarHargaPage');
    Route::get('/laporan-stok/daftar-harga-list', "LaporanDaftarHarga@list")->name('laporanDaftarHargaList');
    Route::get('/laporan-stok/daftar-harga-excel', "LaporanDaftarHarga@excel")->name('laporanDaftarHargaExcel');
    Route::get('/laporan-stok/daftar-harga-pdf', "LaporanDaftarHarga@pdf")->name('laporanDaftarHargaPdf');

    Route::get('/laporan-stok/laporan-saldo-stok', "LaporanSaldoStok@index")->name('laporanSaldoStokPage');
    Route::get('/laporan-stok/laporan-saldo-stok-list', "LaporanSaldoStok@list")->name('laporanSaldoStokList');
    Route::get('/laporan-stok/laporan-saldo-stok-pdf', "LaporanSaldoStok@pdf")->name('laporanSaldoStokPdf');
    Route::get('/laporan-stok/laporan-saldo-stok-excel', "LaporanSaldoStok@excel")->name('laporanSaldoStokExcel');

    Route::get('/laporan-stok/laporan-pergerakan-stok', "LaporanPergerakanStok@index")->name('laporanPergerakanStokPage');
    Route::get('/laporan-stok/laporan-pergerakan-stok-list', "LaporanPergerakanStok@list")->name('laporanPergerakanStokList');
    Route::get('/laporan-stok/laporan-pergerakan-stok-pdf', "LaporanPergerakanStok@pdf")->name('laporanPergerakanStokPdf');
    Route::get('/laporan-stok/laporan-pergerakan-stok-excel', "LaporanPergerakanStok@excel")->name('laporanPergerakanStokExcel');

    Route::get('/laporan-stok/laporan-pemindahan-stok', "LaporanPemindahanStok@index")->name('laporanPemindahanStokPage');
    Route::get('/laporan-stok/laporan-pemindahan-stok-list', "LaporanPemindahanStok@list")->name('laporanPemindahanStokList');
    Route::get('/laporan-stok/laporan-pemindahan-stok-pdf', "LaporanPemindahanStok@pdf")->name('laporanPemindahanStokPdf');
    Route::get('/laporan-stok/laporan-pemindahan-stok-excel', "LaporanPemindahanStok@excel")->name('laporanPemindahanStokExcel');

    Route::get('/laporan-stok/rekap-saldo-stok', "LaporanStok@rekap_saldo_stok")->name('rekapSaldoStokPage');

    Route::get('/laporan-supplier', "LaporanSupplier@index")->name('laporanSupplierPage');

    Route::get('/laporan-stok/stok-gabungan', "LaporanStokGabungan@index")->name('laporanStokGabunganPage');
    Route::get('/laporan-stok/stok-gabungan-list', "LaporanStokGabungan@list")->name('laporanStokGabunganList');
    Route::get('/laporan-stok/stok-gabungan-excel', "LaporanStokGabungan@excel")->name('laporanStokGabunganExcel');
    Route::get('/laporan-stok/stok-gabungan-pdf', "LaporanStokGabungan@pdf")->name('laporanStokGabunganPdf');

    Route::get('/laporan-stok/daftar-stok-minimal', "DaftarStokMinimal@index")->name('daftarStokMinimalPage');
    Route::get('/laporan-stok/daftar-stok-minimal-list', "DaftarStokMinimal@list")->name('daftarStokMinimalList');
    Route::get('/laporan-stok/daftar-stok-minimal-pdf', "DaftarStokMinimal@pdf")->name('daftarStokMinimalPdf');
    Route::get('/laporan-stok/daftar-stok-minimal-excel', "DaftarStokMinimal@excel")->name('daftarStokMinimalExcel');

    Route::get('/laporan-penjualan/item-paling-laris', "LaporanItemPalingLaris@index")->name('laporanItemPalingLarisPage');
    Route::get('/laporan-penjualan/item-paling-laris-list', "LaporanItemPalingLaris@list")->name('laporanItemPalingLarisList');
    Route::get('/laporan-penjualan/item-paling-laris-excel', "LaporanItemPalingLaris@excel")->name('laporanItemPalingLarisExcel');
    Route::get('/laporan-penjualan/item-paling-laris-pdf', "LaporanItemPalingLaris@pdf")->name('laporanItemPalingLarisPdf');

    Route::get('/laporan-penjualan/item-kurang-laris', "LaporanItemKurangLaris@index")->name('laporanItemKurangLarisPage');
    Route::get('/laporan-penjualan/item-kurang-laris-list', "LaporanItemKurangLaris@list")->name('laporanItemKurangLarisList');
    Route::get('/laporan-penjualan/item-kurang-laris-excel', "LaporanItemKurangLaris@excel")->name('laporanItemKurangLarisExcel');
    Route::get('/laporan-penjualan/item-kurang-laris-pdf', "LaporanItemKurangLaris@pdf")->name('laporanItemKurangLarisPdf');

    Route::get('/laporan-stok/stok-expired', "DaftarStokExpired@index")->name('daftarStokExpiredPage');
    Route::get('/laporan-stok/stok-expired-list', "DaftarStokExpired@list")->name('daftarStokExpiredList');
    Route::get('/laporan-stok/stok-expired-excel', "DaftarStokExpired@excel")->name('daftarStokExpiredExcel');
    Route::get('/laporan-stok/stok-expired-pdf', "DaftarStokExpired@pdf")->name('daftarStokExpiredPdf');

    Route::get('/laporan-stok/stok-akan-expired', "DaftarStokAkanExpired@index")->name('daftarStokAkanExpiredPage');
    Route::get('/laporan-stok/stok-akan-expired-list', "DaftarStokAkanExpired@list")->name('daftarStokAkanExpiredList');
    Route::get('/laporan-stok/stok-akan-expired-excel', "DaftarStokAkanExpired@excel")->name('daftarStokAkanExpiredExcel');
    Route::get('/laporan-stok/stok-akan-expired-pdf', "DaftarStokAkanExpired@pdf")->name('daftarStokAkanExpiredPdf');

    Route::get('/laporan-penjualan/stok-out', "DaftarStokOut@index")->name('daftarStokOutPage');
    Route::get('/laporan-penjualan/stok-out-list', "DaftarStokOut@list")->name('daftarStokOutList');
    Route::get('/laporan-penjualan/stok-out-excel', "DaftarStokOut@excel")->name('daftarStokOutExcel');
    Route::get('/laporan-penjualan/stok-out-pdf', "DaftarStokOut@pdf")->name('daftarStokOutPdf');

    Route::get('/laporan-stok/stok-zero', "DaftarStokZero@index")->name('daftarStokZeroPage');
    Route::get('/laporan-stok/stok-zero-list', "DaftarStokZero@list")->name('daftarStokZeroList');
    Route::get('/laporan-stok/stok-zero-excel', "DaftarStokZero@excel")->name('daftarStokZeroExcel');
    Route::get('/laporan-stok/stok-zero-pdf', "DaftarStokZero@pdf")->name('daftarStokZeroPdf');

    Route::get('/laporan-barang/harga-zero', "DaftarBarangZero@index")->name('daftarBarangZeroPage');
    Route::get('/laporan-barang/harga-zero-list', "DaftarBarangZero@list")->name('daftarBarangZeroList');
    Route::get('/laporan-barang/harga-zero-excel', "DaftarBarangZero@excel")->name('daftarBarangZeroExcel');
    Route::get('/laporan-barang/harga-zero-pdf', "DaftarBarangZero@pdf")->name('daftarBarangZeroPdf');

    Route::get('/laporan-langganan/kartu-langganan', "KartuLangganan@index")->name('kartuLanggananPage');
    Route::get('/laporan-langganan/kartu-langganan-list', "KartuLangganan@list")->name('kartuLanggananList');
    Route::get('/laporan-langganan/kartu-langganan-pdf', "KartuLangganan@pdf")->name('kartuLanggananPdf');
    Route::get('/laporan-langganan/kartu-langganan-excel', "KartuLangganan@excel")->name('kartuLanggananExcel');

    Route::get('/laporan-langganan/saldo-piutang', "SaldoPiutangLangganan@index")->name('saldoPiutangLanggananPage');
    Route::get('/laporan-langganan/saldo-piutang-list', "SaldoPiutangLangganan@list")->name('saldoPiutangLanggananList');
    Route::get('/laporan-langganan/saldo-piutang-pdf', "SaldoPiutangLangganan@pdf")->name('saldoPiutangLanggananPdf');
    Route::get('/laporan-langganan/saldo-piutang-excel', "SaldoPiutangLangganan@excel")->name('saldoPiutangLanggananExcel');

    Route::get('/laporan-langganan/mutasi-langganan', "MutasiLangganan@index")->name('mutasiLanggananPage');
    Route::get('/laporan-langganan/mutasi-langganan-list', "MutasiLangganan@list")->name('mutasiLanggananList');
    Route::get('/laporan-langganan/mutasi-langganan-pdf', "MutasiLangganan@pdf")->name('mutasiLanggananPdf');
    Route::get('/laporan-langganan/mutasi-langganan-excel', "MutasiLangganan@excel")->name('mutasiLanggananExcel');

    Route::get('/laporan-langganan/nota-tagihan', "NotaTagihanLangganan@index")->name('notaTagihanLanggananPage');
    Route::get('/laporan-langganan/nota-tagihan-list', "NotaTagihanLangganan@list")->name('notaTagihanLanggananList');
    Route::get('/laporan-langganan/nota-tagihan-pdf', "NotaTagihanLangganan@pdf")->name('notaTagihanLanggananPdf');
    Route::get('/laporan-langganan/nota-tagihan-excel', "NotaTagihanLangganan@excel")->name('notaTagihanLanggananExcel');

    Route::get('/laporan-supplier/kartu-supplier', "KartuSupplier@index")->name('kartuSupplierPage');
    Route::get('/laporan-supplier/kartu-supplier-list', "KartuSupplier@list")->name('kartuSupplierList');
    Route::get('/laporan-supplier/kartu-supplier-excel', "KartuSupplier@excel")->name('kartuSupplierExcel');
    Route::get('/laporan-supplier/kartu-supplier-pdf', "KartuSupplier@pdf")->name('kartuSupplierPdf');

    Route::get('/laporan-supplier/laporan-mutasi-supplier', "LaporanMutasiSupplier@index")->name('laporanMutasiSupplierPage');
    Route::get('/laporan-supplier/laporan-mutasi-supplier-list', "LaporanMutasiSupplier@list")->name('laporanMutasiSupplierList');
    Route::get('/laporan-supplier/laporan-mutasi-supplier-excel', "LaporanMutasiSupplier@excel")->name('laporanMutasiSupplierExcel');
    Route::get('/laporan-supplier/laporan-mutasi-supplier-pdf', "LaporanMutasiSupplier@pdf")->name('laporanMutasiSupplierPdf');

    Route::get('/laporan-supplier/laporan-saldo-supplier', "LaporanSaldoSupplier@index")->name('laporanSaldoSupplierPage');
    Route::get('/laporan-supplier/laporan-saldo-supplier-list', "LaporanSaldoSupplier@list")->name('laporanSaldoSupplierList');
    Route::get('/laporan-supplier/laporan-saldo-supplier-excel', "LaporanSaldoSupplier@excel")->name('laporanSaldoSupplierExcel');
    Route::get('/laporan-supplier/laporan-saldo-supplier-pdf', "LaporanSaldoSupplier@pdf")->name('laporanSaldoSupplierPdf');


    Route::get('/laporan-po/laporan-nota-po', "LaporanNotaPO@index")->name('laporanNotaPOPage');


    Route::get('/laporan-so/laporan-daftar-pesanan', "LaporanDaftarPesanan@index")->name('laporanDaftarPesananPage');
    Route::get('/laporan-so/laporan-nota-so', "LaporanNotaSO@index")->name('laporanNotaSOPage');

    Route::get('/laporan-penjualan/laporan-bonus', "LaporanBonus@index")->name('laporanBonusPage');

    /**
     * Laporan PO
     */
    Route::get('/laporan-po/laporan-po', "LaporanPO@index")->name('laporanPOPage');
    Route::get('/laporan-po/laporan-po-list', "LaporanPO@list")->name('laporanPOList');
    Route::get('/laporan-po/laporan-po-excel', "LaporanPO@excel")->name('laporanPOExcel');
    Route::get('/laporan-po/laporan-po-pdf', "LaporanPO@pdf")->name('laporanPOPdf');

    /**
     * Laporan PO Detail
     */
    Route::get('/laporan-po/laporan-po-detail', "LaporanPODetail@index")->name('laporanPODetailPage');
    Route::get('/laporan-po/laporan-po-detail-list', "LaporanPODetail@list")->name('laporanPODetailList');
    Route::get('/laporan-po/laporan-po-detail-excel', "LaporanPODetail@excel")->name('laporanPODetailExcel');
    Route::get('/laporan-po/laporan-po-detail-pdf', "LaporanPODetail@pdf")->name('laporanPODetailPdf');

    /**
     * Laporan Pembelian
     */
    Route::get('/laporan-pembelian/laporan-pembelian-page', "LaporanPembelian@index")->name('laporanPembelianPage');
    Route::get('/laporan-pembelian/laporan-pembelian-list', "LaporanPembelian@list")->name('laporanPembelianList');
    Route::get('/laporan-pembelian/laporan-pembelian-excel', "LaporanPembelian@excel")->name('laporanPembelianExcel');
    Route::get('/laporan-pembelian/laporan-pembelian-pdf', "LaporanPembelian@pdf")->name('laporanPembelianPdf');

    /**
     *  Laporan Pembelian Detail
     */
    Route::get('/laporan-pembelian/laporan-pembelian-detail', "LaporanPembelianDetail@index")->name('laporanPembelianDetailPage');
    Route::get('/laporan-pembelian/laporan-pembelian-detail-list', "LaporanPembelianDetail@list")->name('laporanPembelianDetailList');
    Route::get('/laporan-pembelian/laporan-pembelian-detail-excel', "LaporanPembelianDetail@excel")->name('laporanPembelianDetailExcel');
    Route::get('/laporan-pembelian/laporan-pembelian-detail-pdf', "LaporanPembelianDetail@pdf")->name('laporanPembelianDetailPdf');

    /**
     * Laporan Pembelian Jatuh Tempo
     */
    Route::get('/laporan-pembelian/laporan-pembelian-jatuh-tempo', "LaporanPembelianJatuhTempo@index")->name('laporanPembelianJatuhTempoPage');
    Route::get('/laporan-pembelian/laporan-pembelian-jatuh-tempo-list', "LaporanPembelianJatuhTempo@list")->name('laporanPembelianJatuhTempoList');
    Route::get('/laporan-pembelian/laporan-pembelian-jatuh-tempo-excel', "LaporanPembelianJatuhTempo@excel")->name('laporanPembelianJatuhTempoExcel');
    Route::get('/laporan-pembelian/laporan-pembelian-jatuh-tempo-pdf', "LaporanPembelianJatuhTempo@pdf")->name('laporanPembelianJatuhTempoPdf');

    /**
     * Laporan Rekap Pembelian
     */
    Route::get('/laporan-pembelian/laporan-rekap-pembelian', "LaporanRekapPembelian@index")->name('laporanRekapPembelianPage');
    Route::get('/laporan-pembelian/laporan-rekap-pembelian-list', "LaporanRekapPembelian@list")->name('laporanRekapPembelianList');
    Route::get('/laporan-pembelian/laporan-rekap-pembelian-excel', "LaporanRekapPembelian@excel")->name('laporanRekapPembelianExcel');
    Route::get('/laporan-pembelian/laporan-rekap-pembelian-pdf', "LaporanRekapPembelian@pdf")->name('laporanRekapPembelianPdf');

    /**
     * Laporan Analisa Pembelian
     */
    Route::get('/laporan-pembelian/laporan-analisa-pembelian', "LaporanAnalisaPembelian@index")->name('laporanAnalisaPembelianPage');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-list', "LaporanAnalisaPembelian@list")->name('laporanAnalisaPembelianList');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-excel', "LaporanAnalisaPembelian@excel")->name('laporanAnalisaPembelianExcel');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-pdf', "LaporanAnalisaPembelian@pdf")->name('laporanAnalisaPembelianPdf');

    /**
     * Laporan Analisa Pembelian Tahunan
     */
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-tahun', "LaporanAnalisaPembelianTahun@index")->name('laporanAnalisaPembelianTahunPage');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-tahun-list', "LaporanAnalisaPembelianTahun@list")->name('laporanAnalisaPembelianTahunList');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-tahun-excel', "LaporanAnalisaPembelianTahun@excel")->name('laporanAnalisaPembelianTahunExcel');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-tahun-pdf', "LaporanAnalisaPembelianTahun@pdf")->name('laporanAnalisaPembelianTahunPdf');

    /**
     * Laporan Analisa Pembelian VS Penjualan
     */
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan', "LaporananAlisaPembelianVSPenjualan@index")->name('laporanAnalisaPembelianVSPenjualanPage');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-list', "LaporanAnalisaPembelianVSPenjualan@list")->name('laporanAnalisaPembelianVSPenjualanList');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-excel', "LaporanAnalisaPembelianVSPenjualan@excel")->name('laporanAnalisaPembelianVSPenjualanExcel');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-pdf', "LaporanAnalisaPembelianVSPenjualan@pdf")->name('laporanAnalisaPembelianVSPenjualanPdf');

    /**
     * Laporan Analisa Pembelian VS Penjualan Tahunan
     */
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-tahunan', "LaporanAnalisaPembelianVSPenjualanTahunan@index")->name('laporanAnalisaPembelianVSPenjualanTahunanPage');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-tahunan-list', "LaporanAnalisaPembelianVSPenjualanTahunan@list")->name('laporanAnalisaPembelianVSPenjualanTahunanList');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-tahunan-excel', "LaporanAnalisaPembelianVSPenjualanTahunan@excel")->name('laporanAnalisaPembelianVSPenjualanTahunanExcel');
    Route::get('/laporan-pembelian/laporan-analisa-pembelian-vs-penjualan-tahunan-pdf', "LaporanAnalisaPembelianVSPenjualanTahunan@pdf")->name('laporanAnalisaPembelianVSPenjualanTahunanPdf');

    /**
     * Laporan Penjualan
     */
    Route::get('/laporan-penjualan/laporan-penjualan', "LaporanPenjualan@index")->name('laporanPenjualanPage');
    Route::get('/laporan-penjualan/laporan-penjualan-list', "LaporanPenjualan@list")->name('laporanPenjualanList');
    Route::get('/laporan-penjualan/laporan-penjualan-pdf', "LaporanPenjualan@pdf")->name('laporanPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-penjualan-excel', "LaporanPenjualan@excel")->name('laporanPenjualanExcel');

    /**
     * Laporan Sales Order
     */
    Route::get('/laporan-so/laporan-so', "LaporanSO@index")->name('laporanSOPage');
    Route::get('/laporan-so/laporan-so-list', "LaporanSO@list")->name('laporanSOList');
    Route::get('/laporan-so/laporan-so-pdf', "LaporanSO@pdf")->name('laporanSOPdf');
    Route::get('/laporan-so/laporan-so-excel', "LaporanSO@excel")->name('laporanSOExcel');

    /**
     * Laporan Daftar Pesanan
     */
    Route::get('/laporan-so/laporan-daftar-pesanan', "LaporanDaftarPesanan@index")->name('laporanDaftarPesananPage');
    Route::get('/laporan-so/laporan-daftar-pesanan-list', "LaporanDaftarPesanan@list")->name('laporanDaftarPesananList');
    Route::get('/laporan-so/laporan-daftar-pesanan-pdf', "LaporanDaftarPesanan@pdf")->name('laporanDaftarPesananPdf');
    Route::get('/laporan-so/laporan-daftar-pesanan-excel', "LaporanDaftarPesanan@excel")->name('laporanDaftarPesananExcel');

    /**
     * Laporan Detail Penjualan
     */
    Route::get('/laporan-penjualan/laporan-detail-penjualan', "LaporanDetailPenjualan@index")->name('laporanDetailPenjualanPage');
    Route::get('/laporan-penjualan/laporan-detail-penjualan-list', "LaporanDetailPenjualan@list")->name('laporanDetailPenjualanList');
    Route::get('/laporan-penjualan/laporan-detail-penjualan-pdf', "LaporanDetailPenjualan@pdf")->name('laporanDetailPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-detail-penjualan-excel', "LaporanDetailPenjualan@excel")->name('laporanDetailPenjualanExcel');

    /**
     * Laporan Detail Sales Order
     */
    Route::get('/laporan-so/laporan-detail-so', "LaporanDetailSO@index")->name('laporanDetailSOPage');
    Route::get('/laporan-so/laporan-detail-so-list', "LaporanDetailSO@list")->name('laporanDetailSOList');
    Route::get('/laporan-so/laporan-detail-so-pdf', "LaporanDetailSO@pdf")->name('laporanDetailSOPdf');
    Route::get('/laporan-so/laporan-detail-so-excel', "LaporanDetailSO@excel")->name('laporanDetailSOExcel');

    /**
     * Laporan Rekap penjualan
     */
    Route::get('/laporan-penjualan/laporan-rekap-penjualan', "LaporanRekapPenjualan@index")->name('laporanRekapPenjualanPage');
    Route::get('/laporan-penjualan/laporan-rekap-penjualan-list', "LaporanRekapPenjualan@list")->name('laporanRekapPenjualanList');
    Route::get('/laporan-penjualan/laporan-rekap-penjualan-pdf', "LaporanRekapPenjualan@pdf")->name('laporanRekapPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-rekap-penjualan-excel', "LaporanRekapPenjualan@excel")->name('laporanRekapPenjualanExcel');

    /**
     * Laporan Detail Penjualan Laba
     */
    Route::get('/laporan-penjualan/laporan-penjualan-detail-laba', "LaporanDetailPenjualanLaba@index")->name('laporanDetailPenjualanLabaPage');
    Route::get('/laporan-penjualan/laporan-penjualan-detail-laba-list', "LaporanDetailPenjualanLaba@list")->name('laporanDetailPenjualanLabaList');
    Route::get('/laporan-penjualan/laporan-penjualan-detail-laba-pdf', "LaporanDetailPenjualanLaba@pdf")->name('laporanDetailPenjualanLabaPdf');
    Route::get('/laporan-penjualan/laporan-penjualan-detail-laba-excel', "LaporanDetailPenjualanLaba@excel")->name('laporanDetailPenjualanLabaExcel');

    /**
     * Laporan Tagihan Penjualan
     */
    Route::get('/laporan-penjualan/laporan-tagihan-penjualan', "LaporanTagihanPenjualan@index")->name('laporanTagihanPenjualanPage');
    Route::get('/laporan-penjualan/laporan-tagihan-penjualan-list', "LaporanTagihanPenjualan@list")->name('laporanTagihanPenjualanList');
    Route::get('/laporan-penjualan/laporan-tagihan-penjualan-pdf', "LaporanTagihanPenjualan@pdf")->name('laporanTagihanPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-tagihan-penjualan-excel', "LaporanTagihanPenjualan@excel")->name('laporanTagihanPenjualanExcel');

    /**
     * Laporan Penjualan Jatuh Tempo
     */
    Route::get('/laporan-penjualan/laporan-penjualan-jatuh-tempo', "LaporanPenjualanJatuhTempo@index")->name('laporanPenjualanJatuhTempoPage');
    Route::get('/laporan-penjualan/laporan-penjualan-jatuh-tempo-list', "LaporanPenjualanJatuhTempo@list")->name('laporanPenjualanJatuhTempoList');
    Route::get('/laporan-penjualan/laporan-penjualan-jatuh-tempo-pdf', "LaporanPenjualanJatuhTempo@pdf")->name('laporanPenjualanJatuhTempoPdf');
    Route::get('/laporan-penjualan/laporan-penjualan-jatuh-tempo-excel', "LaporanPenjualanJatuhTempo@excel")->name('laporanPenjualanJatuhTempoExcel');

    /**
     * Laporan Laba Penjualan
     */
    Route::get('/laporan-penjualan/laporan-laba-penjualan', "LaporanLabaPenjualan@index")->name('laporanLabaPenjualanPage');
    Route::get('/laporan-penjualan/laporan-laba-penjualan-list', "LaporanLabaPenjualan@list")->name('laporanLabaPenjualanList');
    Route::get('/laporan-penjualan/laporan-laba-penjualan-pdf', "LaporanLabaPenjualan@pdf")->name('laporanLabaPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-laba-penjualan-excel', "LaporanLabaPenjualan@excel")->name('laporanLabaPenjualanExcel');

    /**
     * Laporan Pembayaran Tepat Waktu
     */
    Route::get('/laporan-penjualan/laporan-pembayaran-tepat-waktu', "LaporanPembayaranTepatWaktu@index")->name('laporanPembayaranTepatWaktuPage');
    Route::get('/laporan-penjualan/laporan-pembayaran-tepat-waktu-list', "LaporanPembayaranTepatWaktu@list")->name('laporanPembayaranTepatWaktuList');
    Route::get('/laporan-penjualan/laporan-pembayaran-tepat-waktu-pdf', "LaporanPembayaranTepatWaktu@pdf")->name('laporanPembayaranTepatWaktuPdf');
    Route::get('/laporan-penjualan/laporan-pembayaran-tepat-waktu-excel', "LaporanPembayaranTepatWaktu@excel")->name('laporanPembayaranTepatWaktuExcel');

    /**
     * Laporan Komisi Langganan
     */
    Route::get('/laporan-komisi-langganan', "LaporanKomisiLangganan@index")->name('laporanKomisiLanggananPage');
    Route::get('/laporan-komisi-langganan-list', "LaporanKomisiLangganan@list")->name('laporanKomisiLanggananList');
    Route::get('/laporan-komisi-langganan-pdf', "LaporanKomisiLangganan@pdf")->name('laporanKomisiLanggananPdf');
    Route::get('/laporan-komisi-langganan-excel', "LaporanKomisiLangganan@excel")->name('laporanKomisiLanggananExcel');


    Route::get('/laporan-penjualan/laporan-analisa-penjualan-semua-langganan', "LaporanAnalisaPenjualanSemuaLangganan@index")->name('laporanAnalisaPenjualanSemuaLanggananPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-termasuk-ppn', "LaporanAnalisaPenjualanTermasukPPN@index")->name('laporanAnalisaPenjualanTermasukPPNPage');

    Route::get('/laporan-penjualan/laporan-analisa-penjualan-stock', "LaporanAnalisaPenjualanStock@index")->name('laporanAnalisaPenjualanStockPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-langganan', "LaporanAnalisaPenjualanPerLangganan@index")->name('laporanAnalisaPenjualanPerLanggananPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-salesman', "LaporanAnalisaPenjualanPerSalesman@index")->name('laporanAnalisaPenjualanPerSalesmanPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-supplier', "LaporanAnalisaPenjualanPerSupplier@index")->name('laporanAnalisaPenjualanPerSupplierPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-wilayah', "LaporanAnalisaPenjualanPerWilayah@index")->name('laporanAnalisaPenjualanPerWilayahPage');

    /**
     * Laporan Analisa Penjualan Langganan
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-langganan', "LaporanAnalisaPenjualanLangganan@index")->name('laporanAnalisaPenjualanLanggananPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-langganan-list', "LaporanAnalisaPenjualanLangganan@list")->name('laporanAnalisaPenjualanLanggananList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-langganan-pdf', "LaporanAnalisaPenjualanLangganan@pdf")->name('laporanAnalisaPenjualanLanggananPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-langganan-excel', "LaporanAnalisaPenjualanLangganan@excel")->name('laporanAnalisaPenjualanLanggananExcel');

    Route::get('/laporan-penjualan/laporan-analisa-piutang-per-supplier', "LaporanAnalisaPiutangPerSupplier@index")->name('laporanAnalisaPiutangPerSupplierPage');

    /**
     * Laporan Analisa Umur Penjualan
     */
    Route::get('/laporan-penjualan/laporan-analisa-umur-penjualan', "LaporanAnalisaUmurPenjualan@index")->name('laporanAnalisaUmurPenjualanPage');
    Route::get('/laporan-penjualan/laporan-analisa-umur-penjualan-list', "LaporanAnalisaUmurPenjualan@list")->name('laporanAnalisaUmurPenjualanList');
    Route::get('/laporan-penjualan/laporan-analisa-umur-penjualan-pdf', "LaporanAnalisaUmurPenjualan@pdf")->name('laporanAnalisaUmurPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-analisa-umur-penjualan-excel', "LaporanAnalisaUmurPenjualan@excel")->name('laporanAnalisaUmurPenjualanExcel');

    /**
     * Laproan Omzet SO Per Barang
     */
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-barang', "LaporanOmzetSalesmanPerBarang@index")->name('laporanOmzetSalesmanPerBarangPage');
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-barang-list', "LaporanOmzetSalesmanPerBarang@list")->name('laporanOmzetSalesmanPerBarangList');
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-barang-pdf', "LaporanOmzetSalesmanPerBarang@pdf")->name('laporanOmzetSalesmanPerBarangPdf');
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-barang-excel', "LaporanOmzetSalesmanPerBarang@excel")->name('laporanOmzetSalesmanPerBarangExcel');

    /**
     * Laproan Omzet SO Per Golongan
     */
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-golongan', "LaporanOmzetSalesmanPerGolongan@index")->name('laporanOmzetSalesmanPerGolonganPage');
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-golongan-list', "LaporanOmzetSalesmanPerGolongan@list")->name('laporanOmzetSalesmanPerGolonganList');
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-golongan-pdf', "LaporanOmzetSalesmanPerGolongan@pdf")->name('laporanOmzetSalesmanPerGolonganPdf');
    Route::get('/laporan-omzet/laporan-omzet-salesman-per-golongan-excel', "LaporanOmzetSalesmanPerGolongan@excel")->name('laporanOmzetSalesmanPerGolonganExcel');

    /**
     * Laporan Rekap Omzet salesman
     */
    Route::get('/laporan-omzet/laporan-rekap-omzet-salesman', "LaporanRekapOmzetSalesman@index")->name('laporanRekapOmzetSalesmanPage');
    Route::get('/laporan-omzet/laporan-rekap-omzet-salesman-list', "LaporanRekapOmzetSalesman@list")->name('laporanRekapOmzetSalesmanList');
    Route::get('/laporan-omzet/laporan-rekap-omzet-salesman-pdf', "LaporanRekapOmzetSalesman@pdf")->name('laporanRekapOmzetSalesmanPdf');
    Route::get('/laporan-omzet/laporan-rekap-omzet-salesman-excel', "LaporanRekapOmzetSalesman@excel")->name('laporanRekapOmzetSalesmanExcel');

    /**
     * Laporan Omzet salesman Per Faktur
     */
    Route::get('/laporan-omzet/laporan-salesman-omzet-per-faktur', "LaporanOmzetSalesmanPerFaktur@index")->name('laporanOmzetSalesmanPerFakturPage');
    Route::get('/laporan-omzet/laporan-salesman-omzet-per-faktur-list', "LaporanOmzetSalesmanPerFaktur@list")->name('laporanOmzetSalesmanPerFakturList');
    Route::get('/laporan-omzet/laporan-salesman-omzet-per-faktur-pdf', "LaporanOmzetSalesmanPerFaktur@pdf")->name('laporanOmzetSalesmanPerFakturPdf');
    Route::get('/laporan-omzet/laporan-salesman-omzet-per-faktur-excel', "LaporanOmzetSalesmanPerFaktur@excel")->name('laporanOmzetSalesmanPerFakturExcel');
    /**
     * Laporan Analisa Penjualan
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan', "LaporanAnalisaPenjualan@index")->name('laporanAnalisaPenjualanPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-list', "LaporanAnalisaPenjualan@list")->name('laporanAnalisaPenjualanList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-pdf', "LaporanAnalisaPenjualan@pdf")->name('laporanAnalisaPenjualanPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-excel', "LaporanAnalisaPenjualan@excel")->name('laporanAnalisaPenjualanExcel');

    /**
     * Laporan Analisa Penjualan dan Stock
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-stock', "LaporanAnalisaPenjualanStock@index")->name('laporanAnalisaPenjualanStockPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-stock-list', "LaporanAnalisaPenjualanStock@list")->name('laporanAnalisaPenjualanStockList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-stock-pdf', "LaporanAnalisaPenjualanStock@pdf")->name('laporanAnalisaPenjualanStockPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-stock-excel', "LaporanAnalisaPenjualanStock@excel")->name('laporanAnalisaPenjualanStockExcel');

    /**
     * Laporan Analisa Penjualan Per Langganan
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-langganan', "LaporanAnalisaPenjualanPerLangganan@index")->name('laporanAnalisaPenjualanPerLanggananPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-langganan-list', "LaporanAnalisaPenjualanPerLangganan@list")->name('laporanAnalisaPenjualanPerLanggananList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-langganan-pdf', "LaporanAnalisaPenjualanPerLangganan@pdf")->name('laporanAnalisaPenjualanPerLanggananPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-langganan-excel', "LaporanAnalisaPenjualanPerLangganan@excel")->name('laporanAnalisaPenjualanPerLanggananExcel');

    /**
     * Laporan Analisa Penjualan Per SO
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-salesman', "LaporanAnalisaPenjualanPerSalesman@index")->name('laporanAnalisaPenjualanPerSalesmanPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-salesman-list', "LaporanAnalisaPenjualanPerSalesman@list")->name('laporanAnalisaPenjualanPerSalesmanList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-salesman-pdf', "LaporanAnalisaPenjualanPerSalesman@pdf")->name('laporanAnalisaPenjualanPerSalesmanPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-salesman-excel', "LaporanAnalisaPenjualanPerSalesman@excel")->name('laporanAnalisaPenjualanPerSalesmanExcel');

    /**
     * Laporan Analisa Penjualan Per Supplier
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-supplier', "LaporanAnalisaPenjualanPerSupplier@index")->name('laporanAnalisaPenjualanPerSupplierPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-supplier-list', "LaporanAnalisaPenjualanPerSupplier@list")->name('laporanAnalisaPenjualanPerSupplierList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-supplier-pdf', "LaporanAnalisaPenjualanPerSupplier@pdf")->name('laporanAnalisaPenjualanPerSupplierPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-supplier-excel', "LaporanAnalisaPenjualanPerSupplier@excel")->name('laporanAnalisaPenjualanPerSupplierExcel');

    /**
     * Laporan Analisa Penjualan Per Wilayah
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-wilayah', "LaporanAnalisaPenjualanPerWilayah@index")->name('laporanAnalisaPenjualanPerWilayahPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-wilayah-list', "LaporanAnalisaPenjualanPerWilayah@list")->name('laporanAnalisaPenjualanPerWilayahList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-wilayah-pdf', "LaporanAnalisaPenjualanPerWilayah@pdf")->name('laporanAnalisaPenjualanPerWilayahPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-per-wilayah-excel', "LaporanAnalisaPenjualanPerWilayah@excel")->name('laporanAnalisaPenjualanPerWilayahExcel');

    /**
     * Laporan Analisa Penjualan Semua Langganan
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-semua-langganan', "AnalisaPenjualanSemuaLangganan@index")->name('laporanAnalisaPenjualanSemuaLanggananPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-semua-langganan-list', "AnalisaPenjualanSemuaLangganan@list")->name('laporanAnalisaPenjualanSemuaLanggananList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-semua-langganan-pdf', "AnalisaPenjualanSemuaLangganan@pdf")->name('laporanAnalisaPenjualanSemuaLanggananPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-semua-langganan-excel', "AnalisaPenjualanSemuaLangganan@excel")->name('laporanAnalisaPenjualanSemuaLanggananExcel');

    /**
     * Laporan Analisa Penjualan Termasuk PPN
     */
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-termasuk-ppn', "LaporanAnalisaPenjualanTermasukPPN@index")->name('laporanAnalisaPenjualanTermasukPPNPage');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-termasuk-ppn-list', "LaporanAnalisaPenjualanTermasukPPN@list")->name('laporanAnalisaPenjualanTermasukPPNList');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-termasuk-ppn-pdf', "LaporanAnalisaPenjualanTermasukPPN@pdf")->name('laporanAnalisaPenjualanTermasukPPNPdf');
    Route::get('/laporan-penjualan/laporan-analisa-penjualan-termasuk-ppn-excel', "LaporanAnalisaPenjualanTermasukPPN@excel")->name('laporanAnalisaPenjualanTermasukPPNExcel');

});

// ========= Akunting

Route::group(['namespace' => 'Akunting', 'middleware' => 'authLogin:administrator', 'prefix' => 'admin'], function () {

    // Kode Perkiraan
    Route::get('/kode-perkiraan', "KodePerkiraan@index")->name('perkiraanPage');
    Route::post('/kode-perkiraan', 'KodePerkiraan@insert')->name('perkiraanInsert');
    Route::get('/kode-perkiraan/{kode}', 'KodePerkiraan@edit')->name('perkiraanEdit');
    Route::post('/kode-perkiraan/{kode}', 'KodePerkiraan@update')->name('perkiraanUpdate');
    Route::delete('/kode-perkiraan/{kode}', 'KodePerkiraan@delete')->name('perkiraanDelete');

    Route::get('/kode-perkiraan-pdf', "KodePerkiraan@pdf")->name('perkiraanPdf');
    Route::get('/kode-perkiraan-excel', "KodePerkiraan@excel")->name('perkiraanExcel');

    // Jurnal Umum
    Route::get('/jurnal-umum', "JurnalUmum@index")->name('jurnalUmumPage');
    Route::get('/jurnal-umum/create', "JurnalUmum@create")->name('jurnalUmumCreate');
    Route::post('/jurnal-umum', "JurnalUmum@insert")->name('jurnalUmumInsert');
    Route::get('/jurnal-umum/{id}', "JurnalUmum@edit")->name('jurnalUmumEdit');
    Route::post('/jurnal-umum/{id}', "JurnalUmum@update")->name('jurnalUmumUpdate');
    Route::delete('/jurnal-umum/{id}', "JurnalUmum@delete")->name('jurnalUmumDelete');

    Route::get('/jurnal-umum-pdf', "JurnalUmum@pdf")->name('jurnalUmumPdf');
    Route::get('/jurnal-umum-excel', "JurnalUmum@excel")->name('jurnalUmumExcel');

    // Arus Kas
    Route::get('/arus-kas', "ArusKas@index")->name('arusKasPage');
    Route::get('/arus-kas-pdf', "ArusKas@pdf")->name('arusKasPdf');
    Route::get('/arus-kas-excel', "ArusKas@excel")->name('arusKasExcel');

    // Laba Rugi
    Route::get('/laba-rugi', "LabaRugi@index")->name('labaRugiPage');
    Route::get('/laba-rugi-pdf', "LabaRugi@pdf")->name('labaRugiPdf');
    Route::get('/laba-rugi-excel', "LabaRugi@excel")->name('labaRugiExcel');

    // Neraca
    Route::get('/neraca', "Neraca@index")->name('neracaPage');
    Route::get('/neraca-pdf', "Neraca@pdf")->name('neracaPdf');
    Route::get('/neraca-excel', "Neraca@excel")->name('neracaExcel');

    // Asset
    Route::get('/asset', "Asset@index")->name('assetPage');
    Route::get('/asset/create', "Asset@create")->name('assetCreate');
    Route::post('/asset', "Asset@insert")->name('assetInsert');
    Route::get('/asset/penyusutan/{id}', "Asset@penyusutan_form")->name('assetPenyusutanModal');
    Route::post('/asset/penyusutan', "Asset@penyusutan_insert")->name('assetPenyusutan');

    Route::get('/asset/{id}', "Asset@edit")->name('assetEdit');
    Route::post('/asset/{id}', "Asset@update")->name('assetUpdate');
    Route::delete('/asset/{id}', "Asset@delete")->name('assetDelete');

    // Penyusutan Asset
    Route::get('/penyusutan-asset', "PenyusutanAsset@index")->name('penyusutanAssetPage');
    Route::get('/penyusutan-asset-pdf', "PenyusutanAsset@pdf")->name('penyusutanAssetPdf');
    Route::get('/penyusutan-asset-excel', "PenyusutanAsset@excel")->name('penyusutanAssetExcel');

    // Buku Besar
    Route::get('/buku-besar', "BukuBesar@index")->name('bukuBesarPage');
    Route::get('/buku-besar-pdf', "BukuBesar@pdf")->name('bukuBesarPdf');
    Route::get('/buku-besar-excel', "BukuBesar@excel")->name('bukuBesarExcel');

    //Anggaran
    Route::get('/anggaran', "Anggaran@index")->name('anggaranPage');
    Route::get('/anggaran/create', "Anggaran@create")->name('anggaranCreate');
    Route::post('/anggaran/create', "Anggaran@insert")->name('anggaranInsert');
    Route::get('/anggaran/{id}', "Anggaran@edit")->name('anggaranEdit');
    Route::post('/anggaran/{id}', "Anggaran@update")->name('anggaranUpdate');
    Route::get('/anggaran-detail/{id}', "Anggaran@detail")->name('anggaranDetail');
    Route::get('/anggaran-pdf/{id}', "Anggaran@pdf")->name('anggaranPdf');
    Route::get('/anggaran-excel/{id}', "Anggaran@excel")->name('anggaranExcel');
    Route::delete('/anggaran/{id}', "Anggaran@delete")->name('anggaranDelete');
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('/logo', "logo@logo");

Route::get('#')->name('#');

