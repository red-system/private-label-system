<?php
/**
 * Created by PhpStorm.
 * User: mahendrawardana
 * Date: 07/02/19
 * Time: 13.36
 */

return [
    'acc_api_token' => 'c890cc0bd6562ed5b3a2a1105d6ddb9af9fdadab0c55216255bb0465d23c604aab845955b7b43787392c108f5a8cc82292435c99e7e93720145d89193cac06ba',
    'prefixProduksi' => 'MKK-',
    'ppnPersen' => 0.1,
    'decimalStep' => .01,
    'kodeHutangSupplier' => 'HS',
    'kodePreOrder' => 'PO',
    'kodeHutangLain' => 'HL',
    'kodePiutangPelanggan' => 'PP',
    'kodePiutangLain' => 'PL',
    'kodePenyesuaianStokBahan' => 'PSB',
    'kodePenyesuaianStokProduk' => 'PSP',
    'kodeJurnalUmum' => 'JU',
    'kodeAsset' => 'AT',
    'kodeKategoryAsset' => 'KAS',
    'bankType' => 'BCA',
    'bankRekening' => '4161877888',
    'bankAtasNama' => 'BAMBANG PRANOTO',
    'companyName' => 'Private Label ID',
    'companyAddress' => 'JL. Jendral A Yani, No. 04, Baler Bale Agung, Negara',
    'companyPhone' => '0818 0260 9624',
    'companyTelp' => '(0365) 41100',
    'companyEmail' => 'kutuskutusbali@gmail.com',
    'companyBendahara' => 'ARNIEL',
    'companyTuju' => 'Bapak Angga',
    'topMenu' => [
        'dashboard' => 'dashboard',
        'dashboard_2' => 'agency_dashboard',
        'dashboard_3' => 'client_dashboard',
        'masterData' => 'master_data',
        'master_1' => 'data_client',
        'master_2' => 'data_agency',
        'master_3' => 'data_admin',
        'master_4' => 'role',
        'master_5' => 'user',
        'master_6' => 'project_type',
        'master_7' => 'status',
        'master_8' => 'channel',
        'master_9' => 'service',
        'master_10' => 'company',
        'master_11' => 'company_type',
        'master_12' => 'project_status',
        'master_13' => 'agency_type',
        'master_14' => 'data_support_center',
        'master_15' => 'data_agent',
        'master_16' => 'services_content',

        'project' => 'ecom_clients',
        'project_1' => 'project',
        'project_2' => 'project_report',

        'agency' => 'agency',
        'agency_1' => 'my_clients',
        'agency_2' => 'my_dashboard',
        'agency_4' => 'reporting',

        'setting' => 'settings',

        'client' => 'client',
        'client_1' => 'my_data',
        'client_2' => 'my_company',
        'client_3' => 'my_project',

        'report' => 'reporting',
        'report_1' => 'client_report',
        'report_2' => 'task_report',


        'faq' => 'support_center',

        'services' => 'services',
        'my_company' => 'my_company',
        'my_project' => 'my_project',
        'message' => 'messages',


    ],
    'accUrl' => [
        'base' => 'http://akunta.redsystem.id/v1',
        'listCoa' => '/coa',
        'listBukuBesar' => '/buku-besar',
        'listMaster' => '/master',
        'listNeraca' => '/neraca',
        'listMasterDetail' => '/master-detail',
        'listJurnalUmum' => '/jurnal-umum',
        'listTransaksi' => '/transaksi',
        'insert' => '/insert',
        'listLabaRugi' => '/laba-rugi',
        'listAnggaran' => '/anggaran',
    ]
];
