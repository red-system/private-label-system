<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_user')->insert([

            [
                'user_role_id' => 1,
                'agency_id' => 1,
                'user_position_id' => 1,
                'user_name' => 'user',
                'user_email' => 'user@gmail.com',
                'user_dob' => now(),
                'user_data' => 'data',
                'user_desc' => 'desc',
                'user_pict' => null,
                'user_password' => bcrypt('password'),
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        ]);
    }
}
