<form action="<?php echo e(route('rolesLogin')); ?>" method="post" class="m-form roles-login-form" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

    <div class="modal" id="modal-roles-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login Edit Peran untuk Komputer Ini</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Username Akses</label>
                            <input type="text" class="form-control m-input" name="username" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Password Akses</label>
                            <input type="password" class="form-control m-input" name="password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Login Akses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
