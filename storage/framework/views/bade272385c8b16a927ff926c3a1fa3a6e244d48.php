<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/demo/demo5_new/css/pages/wizard/wizard-4.css')); ?>" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/demo/demo5_new/js/pages/custom/user/edit-user.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/demo/default/custom/crud/forms/widgets/select2.js')); ?>"
            type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>


    <!--begin::Subheader-->

    <!--end::Subheader-->

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">

        <!--begin::Container-->
        <div class=" container ">
            <div class="subheader py-2 py-lg-4  subheader-transparent " id="kt_subheader">
                <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Details-->
                    <div class="d-flex align-items-center flex-wrap mr-2">

                        <!--begin::Title-->
                        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                            Add User</h5>
                        <!--end::Title-->

                        <!--begin::Separator-->
                        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                        <!--end::Separator-->

                        <!--begin::Search Form-->
                        <!--end::Search Form-->

                    </div>
                    <!--end::Details-->

                    <!--begin::Toolbar-->
                    <div class="d-flex align-items-center">
                        <!--begin::Button-->
                        <a href="javascript:history.back()"
                           class="btn btn-default font-weight-bold btn-sm px-3 font-size-base">

                            Back </a>
                        <!--end::Button-->
                    </div>
                    <!--end::Toolbar-->
                </div>
            </div>
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Card header-->
                <div class="card-header card-header-tabs-line nav-tabs-line-3x">
                    <!--begin::Toolbar-->
                    <div class="card-toolbar">
                        <ul class="nav nav-tabs nav-bold nav-tabs-line nav-tabs-line-3x">
                            <!--begin::Item-->
                            <li class="nav-item mr-3">
                                <a class="nav-link active" data-toggle="tab" href="#kt_user_edit_tab_1">
                        <span class="nav-icon"><span class="svg-icon"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Design/Layers.svg--><svg
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z"
              fill="#000000" fill-rule="nonzero"></path>
        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z"
              fill="#000000" opacity="0.3"></path>
    </g>
</svg><!--end::Svg Icon--></span></span>
                                    <span class="nav-text font-size-lg">Profile</span>
                                </a>
                            </li>
                            <!--end::Item-->
                            <!--end::Item-->
                        </ul>
                    </div>
                    <!--end::Toolbar-->
                </div>
                <!--end::Card header-->

                <!--begin::Card body-->
                <div class="card-body px-0">
                    <form action="<?php echo e(route('userInsert')); ?>"
                          method="post"
                          class="form-send"
                          data-redirect="<?php echo e(route('userPage')); ?>"
                          data-alert-show="true"
                          data-alert-field-message="true">
                        <?php echo e(csrf_field()); ?>

                        <div class="tab-content">
                            <!--begin::Tab-->
                            <div class="tab-pane show px-7 active" id="kt_user_edit_tab_1" role="tabpanel">
                                <!--begin::Row-->
                                <div class="row">
                                    <div class="col-xl-2"></div>
                                    <div class="col-xl-7 my-2">
                                        <!--begin::Row-->
                                        <div class="row">
                                            <label class="col-3"></label>
                                            <div class="col-9">
                                                <h6 class=" text-dark font-weight-bold mb-10">Account Info:</h6>
                                            </div>
                                        </div>
                                        <!--end::Row-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">Avatar</label>
                                            <div class="col-9">
                                                <input id="old" type="hidden" value="">
                                                <div class="image-input image-input-empty image-input-outline"
                                                     id="kt_user_edit_avatar"
                                                     style="background-image: none">
                                                    <div class="image-input-wrapper"></div>
                                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                           data-action="change" data-toggle="tooltip" title=""
                                                           data-original-title="Change avatar">
                                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                                        <input type="file" class="foto-pict" name="user_pict"
                                                               accept=".png, .jpg, .jpeg">
                                                        <input type="hidden" class="hapus-saja" name="user_pict_remove">

                                                    </label>
                                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                          data-action="cancel" data-toggle="tooltip" title=""
                                                          data-original-title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>

                                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                          data-action="remove" data-toggle="tooltip" title=""
                                                          data-original-title="Remove avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">User
                                                Name</label>
                                            <div class="col-9">
                                                <input class="form-control form-control-lg form-control-solid"
                                                       type="text" name="user_name" value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">User
                                                From</label>
                                            <div class="col-9">
                                                <select name="from" class="form-control m-select2" style="width: 100%">
                                                    <option value="">Select</option>
                                                    <?php if(!empty($from['admin'])): ?>
                                                        <optgroup label="List Admin">
                                                        <?php $__currentLoopData = $from['admin']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($ad['value']); ?>">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($ad['label']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </optgroup>
                                                    <?php endif; ?>
                                                    <?php if(!empty($from['agent'])): ?>
                                                        <optgroup label="List Agent">
                                                        <?php $__currentLoopData = $from['agent']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($agt['value']); ?>">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($agt['label']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </optgroup>
                                                    <?php endif; ?>
                                                    <?php if(!empty($from['agency'])): ?>
                                                        <optgroup label="List Agency">
                                                        <?php $__currentLoopData = $from['agency']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($ag['value']); ?>">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($ag['label']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </optgroup>
                                                    <?php endif; ?>
                                                    <?php if(!empty($from['client'])): ?>
                                                        <optgroup label="List Client">
                                                        <?php $__currentLoopData = $from['client']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($cl['value']); ?>">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($cl['label']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </optgroup>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                        <!--begin::Group-->

























                                        <!--end::Group-->
                                        <!--begin::Group-->













                                        <!--end::Group-->
                                        <!--begin::Group-->
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">Company
                                                Site</label>
                                            <div class="col-9">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <input type="text"
                                                           class="form-control form-control-lg form-control-solid"
                                                           placeholder="site" value="">
                                                    <div class="input-group-append"><span
                                                                class="input-group-text">.com</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Group-->
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">Password</label>
                                            <div class="col-9">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <input type="password"
                                                           class="form-control form-control-lg form-control-solid"
                                                           name="user_password" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">Confirm Password</label>
                                            <div class="col-9">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <input type="password"
                                                           class="form-control form-control-lg form-control-solid"
                                                           name="user_password_confirm" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer pb-0">
                                    <div class="row">
                                        <div class="col-xl-2"></div>
                                        <div class="col-xl-7">
                                            <div class="row">
                                                <div class="col-3"></div>
                                                <div class="col-9">
                                                    <button type="submit" class="btn btn-success font-weight-bold btn-sm px-3 font-size-base">
                                                        Save Changes
                                                    </button>
                                                    <a href="#" class="btn btn-clean font-weight-bold">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Tab-->

                            <!--begin::Tab-->
                            <div class="tab-pane px-7" id="kt_user_edit_tab_3" role="tabpanel">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <!--begin::Row-->
                                    <div class="row">
                                        <div class="col-xl-2"></div>
                                        <div class="col-xl-7">
                                            <!--begin::Row-->
                                            <div class="row mb-5">
                                                <label class="col-3"></label>
                                                <div class="col-9">
                                                    <div class="alert alert-custom alert-light-danger fade show py-4"
                                                         role="alert">
                                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                        <div class="alert-text font-weight-bold">
                                                            Configure user passwords to expire periodically.<br>
                                                            Users will need warning that their passwords are going to
                                                            expire, or they might inadvertently get locked out of the
                                                            system!
                                                        </div>
                                                        <div class="alert-close">
                                                            <button type="button" class="close" data-dismiss="alert"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true"><i
                                                                            class="la la-close "></i></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Row-->

                                            <!--begin::Row-->
                                            <div class="row">
                                                <label class="col-3"></label>
                                                <div class="col-9">
                                                    <h6 class="text-dark font-weight-bold mb-10">Change Or Recover Your
                                                        Password:</h6>
                                                </div>
                                            </div>
                                            <!--end::Row-->

                                            <!--begin::Group-->
                                            <div class="form-group row">
                                                <label class="col-form-label col-3 text-lg-right text-left">Current
                                                    Password</label>
                                                <div class="col-9">
                                                    <input class="form-control form-control-lg form-control-solid mb-1"
                                                           type="text" value="Current password">
                                                    
                                                    
                                                </div>
                                            </div>
                                            <!--end::Group-->

                                            <!--begin::Group-->
                                            <div class="form-group row">
                                                <label class="col-form-label col-3 text-lg-right text-left">New
                                                    Password</label>
                                                <div class="col-9">
                                                    <input class="form-control form-control-lg form-control-solid"
                                                           type="text" value="New password">
                                                </div>
                                            </div>
                                            <!--end::Group-->

                                            <!--begin::Group-->
                                            <div class="form-group row">
                                                <label class="col-form-label col-3 text-lg-right text-left">Verify
                                                    Password</label>
                                                <div class="col-9">
                                                    <input class="form-control form-control-lg form-control-solid"
                                                           type="text" value="Verify password">
                                                </div>
                                            </div>
                                            <!--end::Group-->
                                        </div>
                                    </div>
                                    <!--end::Row-->
                                </div>
                                <!--end::Body-->

                                <!--begin::Footer-->
                                <div class="card-footer pb-0">
                                    <div class="row">
                                        <div class="col-xl-2"></div>
                                        <div class="col-xl-7">
                                            <div class="row">
                                                <div class="col-3"></div>
                                                <div class="col-9">
                                                    <a href="#" class="btn btn-light-primary font-weight-bold">Save
                                                        changes</a>
                                                    <a href="#" class="btn btn-clean font-weight-bold">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Tab-->

                            <!--begin::Tab-->
                            <div class="tab-pane px-7" id="kt_user_edit_tab_4" role="tabpanel">
                                <div class="row">
                                    <div class="col-xl-2"></div>
                                    <div class="col-xl-8">
                                        <div class="my-2">
                                            <div class="row">
                                                <label class="col-form-label col-3 text-lg-right text-left"></label>
                                                <div class="col-9">
                                                    <h6 class="text-dark font-weight-bold mb-7">Setup Email
                                                        Notification:</h6>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-form-label col-3 text-lg-right text-left">Email
                                                    Notification</label>
                                                <div class="col-3">
                                        <span class="switch">
                                        <label>
                                        <input type="checkbox" checked="checked" name="select">
                                        <span></span>
                                        </label>
                                        </span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-3 text-lg-right text-left">Send Copy To
                                                    Personal Email</label>
                                                <div class="col-3">
                                        <span class="switch">
                                        <label>
                                        <input type="checkbox" name="select">
                                        <span></span>
                                        </label>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-form-label col-3 text-lg-right text-left"></label>
                                            <div class="col-9">
                                                <h6 class="text-dark font-weight-bold mb-7">Activity Related
                                                    Emails:</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="separator separator-dashed mb-10"></div>
                                <div class="row">
                                    <div class="col-xl-2"></div>
                                    <div class="col-xl-8">
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">When To
                                                Email</label>
                                            <div class="col-9">
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox">
                                                        <input type="checkbox">You have new notifications.
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox">
                                                        <input type="checkbox">You're sent a direct message
                                                        <span class="checkbox-primary"></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox">
                                                        <input type="checkbox" checked="">Someone adds you as a
                                                        connection
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">When To Escalate
                                                Emails</label>
                                            <div class="col-9">
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox">Upon new order.
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox">New membership approval
                                                        <span class="checkbox-primary"></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox">Member registration
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="separator separator-dashed mb-10"></div>
                                <div class="row">
                                    <div class="col-xl-2"></div>
                                    <div class="col-xl-8">
                                        <div class="row">
                                            <label class="col-form-label col-3 text-lg-right text-left"></label>
                                            <div class="col-9">
                                                <h6 class=" text-dark font-weight-bold mb-7">Updates From
                                                    Keenthemes:</h6>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-3 text-lg-right text-left">Email You
                                                With</label>
                                            <div class="col-9">
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox">News about Metronic product and feature
                                                        updates
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox">Tips on getting more out of Keen
                                                        <span class="checkbox-primary"></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox" checked="checked">Things you missed since
                                                        you last logged into Keen
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox" checked="checked">News about Metronic on
                                                        partner products and other services
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="checkbox-single mb-2">
                                                    <label class="checkbox checkbox-success">
                                                        <input type="checkbox" checked="checked">Tips on Metronic
                                                        business products
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Tab-->
                        </div>
                    </form>
                </div>
                <!--begin::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../general/index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>