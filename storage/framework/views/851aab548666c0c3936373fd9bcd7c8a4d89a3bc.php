<link href="<?php echo e(asset('assets/vendors/base/vendors.bundle.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('assets/demo/default/base/style.bundle.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet" type="text/css"/>

<div class="form-group m-form__group row">
    <label class="form-control-label col-2">You have received a new message for the Order</label>
    <label>:</label>






    <label class="form-control-label col-8"><?php echo e($project->project_order_title); ?></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">From</label>
    <label>:</label>
    <label class="form-control-label col-8" id="nama_package_edit"><?php echo e($dari); ?></label>
</div>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Message</label>
    <label>:</label>
    <label class="form-control-label col-8" id="nama_package_edit"><?php echo e($comment); ?></label>
</div>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Regards</label>
    <label>:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">Project Manager</label>
</div>
<?php if($file): ?>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Uploade Files</label>
    <label>:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit"><a href="<?php echo e('http://dev-storage.redsystem.id/private-label-system/'.$file); ?>"><?php echo e($nama_file); ?></a></label>
</div>
<?php endif; ?>
<br>
<div class="form-group m-form__group row">
    <a href="<?php echo e(route('projectListDetail', \app\Helpers\Main::encrypt($project->project_id))); ?>">Click here to view & reply</a>
</div>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Best Regards</label>
</div>
<div class="form-group m-form__group row">
    <label><b>Private Label ID</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2"> Jl. Ratna No.68, Tonja, Kec. Denpasar Utara, Kota Denpasar, Bali 80236</label>
</div>