<?php $__env->startSection('js'); ?>
    
    <script src="<?php echo e(asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js')); ?>"
            type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/demo/demo5_new/js/pages/widgets.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/demo/demo5_new/plugins/global/plugins.bundle.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/demo/demo5_new/js/scripts.bundle.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/demo/demo5_new/plugins/custom/fullcalendar/fullcalendar.bundle.js')); ?>"></script>


    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->




    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->

    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/demo/demo5_new/plugins/custom/prismjs/prismjs.bundle.css')); ?>" rel="stylesheet"
          type="text/css"/>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
    <?php echo $__env->make('dashboard/importExcel', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container">
        <br>
        <!--begin::Dashboard-->

        <!--begin::Row-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Charts Widget 2-->
                <div class="card card-custom bg-white-100 gutter-b card-stretch card-shadowless">
                    <!--begin::Header-->
                    <div class="card-header h-auto border-0">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                            <h3 class="card-label">
                                <span class="d-block text-dark font-weight-bolder">Agency Growth</span>
                                
                            </h3>
                        </div>
                        <?php if($from[0] == 'admin_id'): ?>
                            <div class="card-toolbar">
                                
                                
                                <a href="#" class="card-title btn btn-success text-light font-size-sm"
                                   data-toggle="modal" data-target="#import-excel">
                                    <i class="la la-plus"></i>Import</a>
                                
                                
                                
                                
                                
                                
                                
                            </div>
                    <?php endif; ?>
                    <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin::Chart-->
                        <input type="hidden" id="growth" value="<?php echo e($growth); ?>">
                        <div id="kt_charts_widget_2_chart"></div>
                        <!--end::Chart-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Charts Widget 2-->
            </div>
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row">
            <div class="col-xl-8">
                <!--begin::Advance Table Widget 2-->
                <div class="card card-custom gutter-b card-stretch card-shadowless bg-white">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">New Client</span>
                            
                        </h3>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-3 pb-0">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-borderless table-vertical-center">
                                <thead>
                                <tr>
                                    <th class="p-0" style="width: 50px">No</th>
                                    <th class="p-0" style="min-width: 200px">Client Name</th>
                                    <th class="p-0" style="min-width: 100px">Channel</th>
                                    
                                    <th class="p-0" style="min-width: 110px">Status</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php ($no = 1); ?>
                                <?php $__currentLoopData = $project_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="pl-0 py-4">
                                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo e($no++); ?></span>
                                        </td>
                                        <td class="pl-0">
                                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo e($pd->client_name); ?></span>

                                        </td>
                                        <td class="pl-0">
                                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><?php echo e($pd->channel_name); ?></span>
                                        </td>
                                        
                                        
                                        
                                        <td class="pl-0">
                                            <span class="label font-weight-bold label-lg label-inline"
                                                  style="color: <?php echo e($pd->project_status->project_status_text_color); ?> ; background-color: <?php echo e($pd->project_status->project_status_bc_color); ?> ;"><?php echo e($pd->project_status->project_status_name); ?></span>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 2-->
            </div>
            <div class="col-xl-4">
                <!--begin::List Widget 5-->
                <div class="card card-custom bg-light-warning gutter-b card-stretch">
                    <!--begin::header-->
                    <div class="card-header border-0">
                        <h3 class="card-title font-weight-bolder text-warning">Approvals</h3>
                        <div class="card-toolbar">
                            <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left"
                                 data-original-title="Quick actions">
                            </div>
                        </div>
                    </div>
                    <!--end::header-->
                    <!--begin::Body-->
                    <div class="card-body pt-0">
                        <!--begin::Item-->
                        <div class="d-flex align-items-center mb-6">
                            <!--begin::Text-->
                            <div class="d-flex flex-column flex-grow-1 py-2">
                                <a class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1"
                                   style="text-align: center">Coming Soon</a>
                            </div>
                            <!--end::Text-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <!--end::Item-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::List Widget 5-->
            </div>
        </div>
        <!--end::Row-->
        <!--end::Dashboard-->
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('../general/index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>