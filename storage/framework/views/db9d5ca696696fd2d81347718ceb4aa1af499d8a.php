<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i
            class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left bg-white">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(!isset($value['sub'])): ?>
                    <?php
                        $aksesName = 'akses-'.$key;
                        $menuStatus = '';
                        if($menuActive) {
                            if($key == $menuActive) {
                                $menuStatus = 'm-menu__item--active';
                            }
                        } elseif($routeName == $value['route'] && $routeName !== 'underconstructionPage') {
                            $menuStatus = 'm-menu__item--active';
                        }
                    ?>
                    <li class="m-menu__item <?php echo e($menuStatus); ?> <?php echo e($aksesName); ?>"
                        data-akses-name="<?php echo e($aksesName); ?>"
                        aria-haspopup="true" hidden>
                        <?php if($value['route'] == 'supportCenter'): ?>
                            <a href="<?php echo e(route($value['route'], $menu_support->support_id)); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?php echo e($value['icon']); ?>"></i>
                                <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text"><?php echo e(Main::menuAction($key)); ?> </span>
                                </span>
                            </span>
                            </a>
                        <?php else: ?>
                            <a href="<?php echo e(route($value['route'])); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?php echo e($value['icon']); ?>"></i>
                                <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text"><?php echo e(Main::menuAction($key)); ?> </span>
                                </span>
                            </span>
                            </a>
                        <?php endif; ?>
                    </li>
                <?php else: ?>
                    <?php

                        $aksesName = 'akses-'.$key;

                        $menuArr = [];
                        foreach($value['sub'] as $r) {
                            $menuArr[] = $r['route'];
                        }

                        $labelArr = [];
                        foreach($value['sub'] as $label=>$r) {
                            $labelArr[] = $label;
                        }

                        $subMenuStatus = '';
                        if(in_array($routeName, $menuArr)) {
                            $subMenuStatus = 'm-menu__item--active m-menu__item--open akses-aktif';
                        } else {
                            if(in_array($menuActive, $labelArr)) {
                                $subMenuStatus = 'm-menu__item--active m-menu__item--open akses-aktif';
                            }
                        }

                    //ksort($value['sub']);

                    ?>
                    <li class="m-menu__item  m-menu__item--submenu m-menu__item--open <?php echo e($aksesName); ?>"
                        data-akses-name="<?php echo e($aksesName); ?>"
                        aria-haspopup="true"
                        m-menu-submenu-toggle="hover" hidden>
                        <a href="javascript:;" class="m-menu__link m-menu__toggle ">
                            <i class="m-menu__link-icon <?php echo e($value['icon']); ?>"></i>
                            <span class="m-menu__link-text"><?php echo e(Main::menuAction($key)); ?></span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <?php $__currentLoopData = $value['sub']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php

                                        $aksesName = 'akses-'.$key2;

                                        $subMenuActive = '';
                                        if($routeName == $value2['route'] || $menuActive == $key2 ) {
                                            $subMenuActive = 'm-menu__item--active akses-aktif';
                                        }

                                    ?>
                                    <?php if($key2 == 'project'): ?>


















                                                    <?php ($project_sub_parent = ''); ?>
                                                    <?php if($routeName == 'projectListPage'): ?>
                                                        <?php ($project_sub_parent = 'm-menu__item--active akses-aktif'); ?>
                                                    <?php endif; ?>
                                                    <?php $__currentLoopData = $project_status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kunci => $ps): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php ($project_menu = ''); ?>
                                                        <?php if($menuActive == 'project-status-'.$ps->project_status_id): ?>
                                                            <?php ($project_menu = 'm-menu__item--active akses-aktif'); ?>
                                                        <?php endif; ?>
                                                        <li class="m-menu__item <?php echo e($project_menu); ?>"
                                                            aria-haspopup="true">
                                                            <a href="<?php echo e(route('projectPerPage', $ps->project_status_id)); ?>"
                                                               class="m-menu__link ">
                                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                    <span></span>
                                                                </i>
                                                                <span class="m-menu__link-title">
                                                                    <span class="m-menu__link-wrap">
                                                                        <span class="m-menu__link-text"><?php echo e($ps->project_status_name); ?></span>
                                                                        <?php if($from[0] == 'agency_id' && $ps->count != 0): ?>
                                                                            <span class="m-menu__link-badge">
                                                                                <span class="m-badge m-badge--danger"><?php echo e($ps->count); ?></span>
                                                                            </span>
                                                                        <?php endif; ?>
                                                                    </span>
                                                                </span>
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            </a>

                                                        </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="m-menu__item <?php echo e($project_sub_parent); ?>"
                                                        aria-haspopup="true">
                                                        <a href="<?php echo e(route($value2['route'])); ?>" class="m-menu__link ">
                                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                <span></span>
                                                            </i>
                                                            <span class="m-menu__link-title">
                                                                    <span class="m-menu__link-wrap">
                                                                        <span class="m-menu__link-text">All Project</span>
                                                                        <?php if($from[0] == 'agency_id' && $count != 0): ?>
                                                                            <span class="m-menu__link-badge">
                                                                                <span class="m-badge m-badge--danger"><?php echo e($count); ?></span>
                                                                            </span>
                                                                        <?php endif; ?>
                                                                    </span>
                                                                </span>



                                                        </a>
                                                    </li>



                                    <?php else: ?>
                                        <li class="m-menu__item <?php echo e($subMenuActive); ?> <?php echo e($aksesName); ?>"
                                            data-akses-name="<?php echo e($aksesName); ?>"
                                            aria-haspopup="true">
                                            <a href="<?php echo e(route($value2['route'])); ?>" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text"><?php echo e(Main::menuAction($key2)); ?></span>

                                            </a>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
            
            

        </ul>

        <ul class="m-menu__nav" hidden id="profile_menu">
            <li class="m-menu__item">
                <hr>

                <div class="d-flex align-items-center mt-5">
                    <div class="symbol symbol-100 mr-5">
                    </div>
                    <div class="symbol symbol-100 mr-3 d-flex flex-column">
                        <div class="symbol-label"
                             style="background-image:url(<?php echo e('http://dev-storage.redsystem.id/private-label-system/'.$user_foto); ?>); height: 80px; width: 80px"></div>
                    </div>
                    <div class="d-flex flex-column">
                        <a href="<?php echo e(route('profilPage')); ?>"
                           class="font-weight-bold text-dark-75 text-hover-primary"
                           style="font-size: 13px"><?php echo e($user->user_name); ?></a>
                        <div class="text-muted mt-1" style="font-size: 10px"><?php echo e($user->user_role->user_role_name); ?></div>
                        <div class="navi mt-2">
                            <a href="<?php echo e(route('profilPage')); ?>" class="navi-item">
								<span class="navi-link p-0 pb-2">
									<span class="navi-icon mr-1">
										<span class="svg-icon svg-icon-lg svg-icon-primary">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
											<svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px"
                                                 viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
                                                          fill="#000000"/>
													<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"/>
												</g>
											</svg>
                                            <!--end::Svg Icon-->
										</span>
									</span>
									<span class="navi-text text-muted text-hover-primary"
                                          style="font-size: 9px"><?php echo e($user->user_email); ?></span>
								</span>
                            </a>
                            <a href="<?php echo e(route('logoutDo')); ?>"
                               class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5" style="font-size: 9px">Sign
                                Out</a>
                        </div>
                    </div>
                </div>
                <!--end::Separator-->
                <!--begin::Nav-->





























            </li>
        </ul>

    </div>
</div>
