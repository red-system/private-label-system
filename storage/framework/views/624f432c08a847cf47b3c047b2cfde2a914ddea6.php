<!DOCTYPE html>
<!--
Template Name: Metronic - Bootstrap 4 HTML, React, Angular 9 & VueJS Admin Dashboard Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: https://1.envato.market/EA4JP
Renew Support: https://1.envato.market/EA4JP
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->
<head>
    <meta charset="utf-8"/>
    <title><?php echo e($pageTitle); ?> Private Label ID Agency Platform</title>

    <meta name="description" content="Agency Life just got easier - Keep your clients smiling today
Invest into your client's future success - Increase your agency ROI"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Custom Styles(used by this page)-->

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link href="<?php echo e(asset('assets/vendors/base/vendors.bundle.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/demo/default/base/style.bundle.css')); ?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/app.css')); ?>">
    <link href="<?php echo e(asset('assets/demo/demo5_new//css/pages/login/login-2.css')); ?>" rel="stylesheet" type="text/css"/>
    <!--end::Page Custom Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="<?php echo e(asset('assets/demo/demo5_new/plugins/global/plugins.bundle.css')); ?>" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo e(asset('assets/demo/demo5_new/plugins/custom/prismjs/prismjs.bundle.css')); ?>" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo e(asset('assets/demo/demo5_new/css/style.bundle.css')); ?>" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="<?php echo e(asset('images/ps-ikon.png')); ?>"/>
</head>
<!--end::Head-->
<body id="kt_body"
      class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed subheader-enabled aside-enabled aside-static page-loading">
<!--begin::Main-->
<?php echo $__env->make('general/modal_password', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
        <!--begin::Aside-->
        <div class="login-aside position-relative overflow-hidden order-2 order-lg-1 d-flex flex-column-fluid flex-lg-row-auto py-9 px-7 py-lg-13 px-lg-35">
            <!--begin: Aside Container-->
            <div class="d-flex flex-row-fluid flex-column justify-content-between">
                <!--begin::Logo-->
                <a href="#" class="pt-2">
                    <img src="<?php echo e(asset('images/ps.png')); ?>" class="max-h-75px">
                    
                    
                    
                </a>

                <!--end::Logo-->
                <!--begin::Aside body-->
                <div class="d-flex flex-column-fluid flex-column flex-center">
                    <!--begin::Signin-->
                    <div class="login-form login-signin py-11">
                        <!--begin::Form-->
                        <form class="m-login__form m-form form-send" id="kt_login_signin_form"
                              action="<?php echo e(route('loginDo')); ?>" method="POST">
                        <?php echo e(csrf_field()); ?>

                        <!--begin::Title-->
                            <div class="text-center pb-2">
                                <h2 class="font-weight-bolder text-dark display5">Sign In to Positive Sparks</h2>
                            </div>
                            <span class="font-weight-bolder">Enter your detail to login to your account

                            </span>
                            <!--end::Title-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <br>
                                <input type="hidden" name="link" value="<?php echo e($link); ?>">
                                
                                <input class="form-control m-input form-control-solid h-auto py-7 px-6 rounded-lg"
                                       type="text" placeholder="Username" name="username" autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                
                                
                                
                                
                                <input class="form-control m-input form-control-solid h-auto py-7 px-6 rounded-lg"
                                       type="password" placeholder="Password" name="password" autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->


                            <!--end::Form group-->
                            <!--begin::Action-->
                            
                            
                            
                            
                            
                            
                            <div class="pb-lg-0 pb-10">
                                <button type="submit"
                                        class="btn btn-primary font-weight-bolder font-size-h6 px-13 py-4 my-3 mr-3">
                                    Sign In
                                </button>
                                <button type="button" onclick="sweet()"
                                        class="btn btn-light-primary font-weight-bolder px-8 py-4 my-3 font-size-lg">
									<span class="svg-icon svg-icon-md">
										<svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
											<path d="M19.9895 10.1871C19.9895 9.36767 19.9214 8.76973 19.7742 8.14966H10.1992V11.848H15.8195C15.7062 12.7671 15.0943 14.1512 13.7346 15.0813L13.7155 15.2051L16.7429 17.4969L16.9527 17.5174C18.879 15.7789 19.9895 13.221 19.9895 10.1871Z"
                                                  fill="#4285F4"></path>
											<path d="M10.1993 19.9313C12.9527 19.9313 15.2643 19.0454 16.9527 17.5174L13.7346 15.0813C12.8734 15.6682 11.7176 16.0779 10.1993 16.0779C7.50243 16.0779 5.21352 14.3395 4.39759 11.9366L4.27799 11.9466L1.13003 14.3273L1.08887 14.4391C2.76588 17.6945 6.21061 19.9313 10.1993 19.9313Z"
                                                  fill="#34A853"></path>
											<path d="M4.39748 11.9366C4.18219 11.3166 4.05759 10.6521 4.05759 9.96565C4.05759 9.27909 4.18219 8.61473 4.38615 7.99466L4.38045 7.8626L1.19304 5.44366L1.08875 5.49214C0.397576 6.84305 0.000976562 8.36008 0.000976562 9.96565C0.000976562 11.5712 0.397576 13.0882 1.08875 14.4391L4.39748 11.9366Z"
                                                  fill="#FBBC05"></path>
											<path d="M10.1993 3.85336C12.1142 3.85336 13.406 4.66168 14.1425 5.33717L17.0207 2.59107C15.253 0.985496 12.9527 0 10.1993 0C6.2106 0 2.76588 2.23672 1.08887 5.49214L4.38626 7.99466C5.21352 5.59183 7.50242 3.85336 10.1993 3.85336Z"
                                                  fill="#EB4335"></path>
										</svg>
									</span>Sign in with Google
                                </button>
                            </div>
                            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">

                                <div>
                                    <a href="javascript:;" class="text-primary font-weight-bold" id="kt_login_forgot">Forgot
                                        Password ?</a>
                                </div>
                                <div>
                                    <span class="text-muted mr-2">Don't have an account?</span>
                                    <a href="javascript:;" id="kt_login_signup" class="font-weight-bold">Signup</a>
                                </div>

                            </div>
                            <!--end::Action-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signin-->
                    <!--begin::Signup-->
                    <div class="login-form login-signup pt-11">
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_signup_form">
                            <!--begin::Title-->
                            <div class="text-center pb-8">
                                <h3 class="font-weight-bolder text-dark display5">Sign Up</h3>
                                <p class="text-muted font-weight-bold font-size-h4">Enter your details to create your
                                    account</p>
                            </div>
                            <!--end::Title-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                                       type="text" placeholder="Fullname" name="fullname" autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                                       type="email" placeholder="Email" name="email" autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                                       type="password" placeholder="Password" name="password" autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                                       type="password" placeholder="Confirm password" name="cpassword"
                                       autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <label class="checkbox mb-0">
                                    <input type="checkbox" name="agree"/>I Agree the
                                    <a href="#">terms and conditions</a>.
                                    <span></span></label>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
                                <button type="button" id="kt_login_signup_submit"
                                        class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">
                                    Submit
                                </button>
                                <button type="button" id="kt_login_signup_cancel"
                                        class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">
                                    Cancel
                                </button>
                            </div>
                            <!--end::Form group-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signup-->
                    <!--begin::Forgot-->
                    <div class="login-form login-forgot pt-11">
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_forgot_form">
                            <!--begin::Title-->
                            <div class="text-center pb-8">
                                <h3 class="font-weight-bolder text-dark display5">Forgotten Password ?</h3>
                                <p class="text-muted font-weight-bold font-size-h4">Enter your email to reset your
                                    password</p>
                            </div>
                            <!--end::Title-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                                       type="email" placeholder="Email" name="email" autocomplete="off"/>
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
                                <button type="button" id="kt_login_forgot_submit"
                                        class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">
                                    Submit
                                </button>
                                <button type="button" id="kt_login_forgot_cancel"
                                        class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">
                                    Cancel
                                </button>
                            </div>
                            <!--end::Form group-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Forgot-->
                </div>
                <!--end::Aside body-->
            </div>
            <!--end: Aside Container-->
        </div>
        <!--begin::Aside-->
        <!--begin::Content-->
        <div class="content order-1 order-lg-2 flex-column-row-auto flex-grow-1 pt-md-0 pb-0 bgi-no-repeat bgi-position-y-bottom bgi-position-x-center"
             style="background-size:100% 100%; background-image: url(<?php echo e(asset('images/bc-login3.jpg')); ?>);">

            <!--begin::Content body-->

            
            <div class="d-flex flex-center pb-lg-40 pt-lg-0 pt-md-0 pb-40" style="height: 100%">
                <div class="d-flex flex-column pt-xl-10 pt-lg-20 pb-sm-10">
                    <div class="pb-2">
                        <h3 class="display4 font-weight-bolder my-2 text-light">Agency Life just got easier</h3>
                        <span class="font-weight-bolder font-size-h4 font-size-lg text-light pb-lg-3 pb-sm-40 pb-10">Keep your clients smiling today</span>


                    </div>
                    <div class="d-flex align-items-center mb-6">
                        <!--begin::Checkbox-->
                        <label class="checkbox checkbox-lg checkbox-warning checkbox-single flex-shrink-0 m-0 mr-4">
                            
                            <span class="bg-gray-300"></span>
                        </label>
                        <!--end::Checkbox-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1 py-2">
                            <span class="font-weight-bolder font-size-h4 font-size-lg text-light pb-lg-3 pb-sm-40 pb-10">Invest into your client's future success</span>
                        </div>
                        <!--end::Text-->
                        <!--end::Dropdown-->
                    </div>
                    <div class="d-flex align-items-center mb-6">
                        <!--begin::Checkbox-->
                        <label class="checkbox checkbox-lg checkbox-warning checkbox-single flex-shrink-0 m-0 mr-4">
                            
                            <span class="bg-gray-300"></span>
                        </label>
                        <!--end::Checkbox-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1 py-2">
                            <span class="font-weight-bolder font-size-h4 font-size-lg text-light pb-lg-3 pb-sm-40 pb-10">Increase your agency ROI</span>
                        </div>
                        <!--end::Text-->
                        <!--end::Dropdown-->
                    </div>
                    <div class="d-flex align-items-center mb-6">
                        <!--begin::Checkbox-->
                        <label class="checkbox checkbox-lg checkbox-warning checkbox-single flex-shrink-0 m-0 mr-4">
                            
                            <span class="bg-gray-300"></span>
                        </label>
                        <!--end::Checkbox-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1 py-2">
                            <span class="font-weight-bolder font-size-h4 font-size-lg text-light pb-lg-3 pb-sm-40 pb-10">Personalised for you</span>
                        </div>
                        <!--end::Text-->
                        <!--end::Dropdown-->
                    </div>
                </div>
            </div>
            <!--end::Content body-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Login-->
</div>
<!--end::Main-->
<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = {
        "breakpoints": {"sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200},
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#6993FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#F3F6F9",
                    "dark": "#212121"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1E9FF",
                    "secondary": "#ECF0F3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#212121",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#ECF0F3",
                "gray-300": "#E5EAEE",
                "gray-400": "#D6D6E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#80808F",
                "gray-700": "#464E5F",
                "gray-800": "#1B283F",
                "gray-900": "#212121"
            }
        },
        "font-family": "Poppins"
    };</script>
<script>
    function sweet() {
        // $('#login-google').on('click').
        swal({
            title: "Sorry This Function is not available yet",
            type: "warning",
        })
    }
</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="<?php echo e(asset('assets/demo/demo5_new/plugins/global/plugins.bundle.js')); ?>"></script>
<script src="<?php echo e(asset('assets/demo/demo5_new/plugins/custom/prismjs/prismjs.bundle.js')); ?>"></script>
<script src="<?php echo e(asset('assets/demo/demo5_new/js/scripts.bundle.js')); ?>"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo e(asset('assets/demo/demo5_new/js/pages/custom/login/login.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/vendors/base/vendors.bundle.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/demo/default/base/scripts.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>