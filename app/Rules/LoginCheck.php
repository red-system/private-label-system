<?php

namespace app\Rules;

use app\Models\mUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use app\Models\mUserRole;
use Session;

class LoginCheck implements Rule
{

    protected $user_name;
    protected $user_role_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($user_name, $user_role_id, $link)
    {
        $this->user_name = $user_name;
        $this->user_role_id = $user_role_id;
        $this->link = $link;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user_name = $this->user_name;
        $user_password = $value;
        $userPassword = mUser::where('user_name', $user_name)->value('user_password');
        $level = '';
        $status = FALSE;

        /**
         * Login user
         */
        if (Hash::check($user_password, $userPassword)) {
            $level = 'administrator';
            $status = TRUE;
        }

        if ($status) {
            if ($level == 'administrator') {
                $user_role = mUserRole::where('user_role_id', $this->user_role_id)->first();
                $user = mUser
                    ::with([
                        'user_role'
                    ])
                    ->where([
                        'user_name' => $user_name,
                    ])
                    ->first();


                if ($user->user_pict == '' || $user->user_pict == null) {
                    $user->user_pict = 'empty.png';
                }

                $session = [
                    'login' => TRUE,
                    'level' => $level,
                    'user' => $user,
                    'user_role'=>$user_role,
                    'link' => $this->link
                ];
            }

            //echo json_encode($session);

            Session::put($session);
        }


        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Username or Password is incorrect';
    }
}
