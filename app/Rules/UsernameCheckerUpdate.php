<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use app\Models\mUser;

class UsernameCheckerUpdate implements Rule
{

    protected $id;
    protected $user_name;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = mUser::select('user_name')->where('user_id', $this->id)->first();
        $user_name_first = $user->user_name;
        $user_name = $value;
        $this->user_name = $value;

        if($user_name == $user_name_first) {
            return TRUE;
        } else {
            $checkSame = mUser::where('user_name', $user_name)->whereNotIn('user_name', array($user_name_first))->count();
            if($checkSame > 0) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Username <strong>'.$this->user_name.'</strong> is not Available';
    }
}
