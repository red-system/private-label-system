<?php

namespace app\Helpers;

use app\Http\Controllers\MasterData\Support;
use app\Models\mChat;
use app\Models\mProject;
use app\Models\mProjectStatus;
use app\Models\mSupport;
use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use app\Models\Widi\mBahan;
use app\Models\Widi\mProduk;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mStokProduk;


class Main
{
    public static $date_format_view = 'd F Y H:i';
    public static $error = 'error';
    public static $success = 'success';
    public static $get = 'Success getting data';
    public static $store = 'Storing data success';
    public static $update = 'Updating data success';
    public static $delete = 'Removing data success';
    public static $import = 'Importing data success';


    public static function data($breadcrumb = array(), $menuActive = '')
    {
        $notifAlertBahan = Main::notifAlertBahan();
        $notifAlertProduk = Main::notifAlertProduk();
        $notifications = [
            $notifAlertBahan,
            $notifAlertProduk
        ];
        $totalNotification = Main::totalNotification($notifications);
        $badgeNotification = Main::badgeNotification($totalNotification);
        $level = Session::get('level');
        $user = Session::get('user');
//        $user_role = mUser::where('user_id', $user->id)->with('user_role')->first();
        $users = mUser::where('user_id', $user->user_id)->with('user_role')->first();
        $user_foto = '';
        $user_nama = '';
        $user_email = '';

        if ($level == 'administrator') {
            $user_foto = $user->user_pict;
            $user_nama = $user->user_name;
            $user_email = $user->user_email;
        } elseif ($level == 'distributor') {
            $user_foto = $user->foto_distributor;
            $user_nama = $user->nama_distributor;
            $user_email = $user->email_distributor;
        }
        $data['user'] = Session::get('user');
        $kirim = [
            0 => $user,
            1 => $user_foto
        ];
        $data['menu'] = Main::generateTopMenu($menuActive, $kirim);
        $data['menuAction'] = Main::menuActionData($menuActive);
        $data['footer'] = Main::footer();
        $data['metaTitle'] = Main::metaTitle($breadcrumb);
        $data['pageTitle'] = Main::pageTitle($breadcrumb);
        $data['breadcrumb'] = Main::breadcrumb($breadcrumb);
        $data['user_role_data'] = json_decode($users->user_role->user_role_access);
        $data['user_foto'] = $user_foto;
        $data['user_nama'] = $user_nama;
        $data['user_email'] = $user_email;
        $data['level'] = $level;
        $data['pageMethod'] = '';
        $data['no'] = 1;
        $data['imgWidth'] = 100;
        $data['decimalStep'] = '.01';
        $data['roundDecimal'] = 2;
        $data['prefixNoSeriProduk'] = 'SPO-';
        $data['prefixNoSeriBahan'] = 'SBH-';
        $data['prefixProduksi'] = Config::get('constants.prefixProduksi');
        $data['prefixNoPenjualan'] = 'PJL-';
        $data['prefixNoPoBahan'] = 'PBH-';
        $data['prefixNoPoBahanPembelian'] = 'PBL-';
        $data['prefixNoDistributorOrder'] = 'DOR-';
        $data['prefixNoDistribusi'] = 'DST-';
        $data['kodeHutangSupplier'] = 'HS';
        $data['labelBalance'] = '<span class="m-badge m-badge--success m-badge--wide">BALANCE</span>';
        $data['labelBalanceNot'] = '<span class="m-badge m-badge--danger m-badge--wide">NOT BALANCE</span>';
        $data['ppnPersen'] = Config::get('constants.ppnPersen');
        $data['methodProses'] = '';
        $data['tableValue'] = isset($_GET['table_value']) ? $_GET['table_value'] : '';
        $data['tableIndex'] = isset($_GET['table_index']) ? $_GET['table_index'] : '';

        $data['cons'] = Config::get('constans');

        $data['notifAlertBahan'] = $notifAlertBahan;
        $data['notifAlertProduk'] = $notifAlertProduk;
        $data['totalNotification'] = $totalNotification;
        $data['badgeNotification'] = $badgeNotification;

        $data['urlCoa'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listCoa');
        $data['urlAnggaran'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listAnggaran');
        $data['urlNeraca'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listNeraca');
        $data['urlCoaInsert'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listCoa') . Config::get('constants.accUrl.insert');
        $data['urlCoaPembayaran'] = Config::get('constants.accUrl.base') . '/coa-pembayaran';
        $data['urlBukuBesar'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listBukuBesar');
        $data['urlMaster'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listMaster');
        $data['urlMasterDetail'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listMasterDetail');
        $data['urlJurnalUmum'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listJurnalUmum');
        $data['urlLabaRugi'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listLabaRugi');
        $data['urlTransaksi'] = Config::get('constants.accUrl.base') . Config::get('constants.accUrl.listTransaksi');
        $data['acctoken'] = Config::get('constants.acc_api_token');
//        $chat = mChat::where('from', $data['user']->from)->first();
//        $data['chat_id'] = $chat->chat_id;
        $result = explode('|', $data['user']->from);
        $chat = [];
        if ($result[0] == 'agency_id') {
            $chat = mChat::where('agency_id', $result[1])->with(['chat_data', 'agency', 'client'])->get();
        } elseif ($result[0] == 'client_id') {
            $chat = mChat::where('client_id', $result[1])->with(['chat_data', 'agency', 'client'])->get();
        }
        $notif = 'kosong';
        foreach($chat as $value){
            $value->notif = 'kosong';
            $value->dari = '';
            foreach($value->chat_data as $cd){
                if($cd->to == $data['user']->from && $cd->status == 'belum'){
                    $res = explode('|', $cd->from);
                    $value->dari = $res[0];
                    $value->notif = 'ada';
                    $notif = 'ada';
                }
            }
        }
        $data['dari'] = $result[0];
        $data['chats'] = $chat;
        $data['notif'] = $notif;
        return $data;
    }

    public static function companyInfo()
    {
        $data = [
            'bankType' => Config::get('constants.bankType'),
            'bankRekening' => Config::get('constants.bankRekening'),
            'bankAtasNama' => Config::get('constants.bankAtasNama'),
            'companyName' => Config::get('constants.companyName'),
            'companyAddress' => Config::get('constants.companyAddress'),
            'companyPhone' => Config::get('constants.companyPhone'),
            'companyTelp' => Config::get('constants.companyTelp'),
            'companyEmail' => Config::get('constants.companyEmail'),
            'companyBendahara' => Config::get('constants.companyBendahara'),
            'companyTuju' => Config::get('constants.companyTuju'),
        ];

        $data = (object)$data;

        return $data;
    }

    public static function response($status = 'error', $message = 'Empty', $data = NULL, $errors = NULL)
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'errors' => $errors
        ];
    }

    public static function totalNotification($notifications)
    {
        return count($notifications);
    }

    public static function badgeNotification($totalNotification)
    {
        if ($totalNotification > 0) {
            return '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot m-badge--danger"></span>';
        }
        return '';
    }

    public static function totalAlertBahan()
    {
        $list = mBahan
            ::with(
                'stok_bahan:id_bahan,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_bahan as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }

        return $totalAlert;
    }

    public static function totalAlertProduk()
    {
        $list = mProduk
            ::with(
                'stok_produk:id_produk,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_produk as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }


        return $totalAlert;
    }

    public static function badgeAlertProduk($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertProduk();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function badgeAlertBahan($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertBahan();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function bagdeInventory($totalAlert = '')
    {
        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function notifAlertBahan()
    {
//        $totalAlert = Main::totalAlertBahan();
//        if ($totalAlert > 0) {
//            return '
//                <a href="' . route('stokAlertBahan') . '" class="m-list-timeline__item">
//                    <span class="m-list-timeline__badge"></span>
//                    <span href="" class="m-list-timeline__text">Stok Bahan kurang dari Minimal Stok, segera setarakan. <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
//                </a>
//            ';
//        }
        return '';
    }

    public static function notifAlertProduk()
    {
//        $totalAlert = Main::totalAlertProduk();
//        if ($totalAlert > 0) {
//            return '
//                <a href="' . route('stokAlertProduk') . '" class="m-list-timeline__item">
//                    <span class="m-list-timeline__badge"></span>
//                    <span href="" class="m-list-timeline__text">Stok Produk kurang dari Minimal Stok, segera setarakan.
//                    <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
//                </a>
//            ';
//        }
        return '';
    }

    public static function generateTopMenu($menuActive, $kirim)
    {
//        $totalAlertBahan = Main::totalAlertBahan();
//        $totalAlertProduk = Main::totalAlertProduk();

        $data['routeName'] = Route::currentRouteName();
        $data['menu'] = Main::menuList();
        $data['user'] = $kirim[0];
        $from = explode('|', $kirim[0]->from);
        $data['user_foto'] = $kirim[1];
        $data['menuActive'] = $menuActive;
        $data['consMenu'] = Config::get('constants.topMenu');
        $data['menu_support'] = $id = mSupport::first();
        $project_status = mProjectStatus::all();
        foreach ($project_status as $value){
            if($from[0] == 'agency_id'){
                if(mProject::where('agency_id', $from[1])->where('project_status_id', $value->project_status_id)->where('akses', 'new')->count() > 0){
                    $value->count = 'new';
                }else{
                    $value->count = mProject::where('agency_id', $from[1])->where('project_status_id', $value->project_status_id)->where('akses', 'old')->count();
                }
            }else{
                $value->count = null;
            }
        }
        $count = null;
        if($from[0] == 'agency_id') {
            if (mProject::where('agency_id', $from[1])->where('akses', 'new')->count() > 0){
                $count = 'new';
            }else{
                $count = mProject::where('agency_id', $from[1])->where('akses', 'old')->count();
            }
        }
        $data['from'] = $from;
        $data['count'] = $count;
        $data['project_status'] = $project_status;
//        $data['badgeAlertBahan'] = Main::badgeAlertBahan($totalAlertBahan);
//        $data['badgeAlertProduk'] = Main::badgeAlertProduk($totalAlertProduk);
//        $data['badgeInventory'] = Main::bagdeInventory($totalAlertBahan + $totalAlertProduk);
//

        return view('component/menuStay', $data);
    }

    public static function footer()
    {
        $data = [];
        return view('component/footer', $data);
    }

    public static function format_money($number)
    {
        if (Main::check_decimal($number)) {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        } else {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        }
    }

    public static function unformat_money($number)
    {
        $number = str_replace(['Rp', ''], ['.', ''], [',', '.'], $number);
        $number = self::format_decimal($number);
        return self::format_number($number);
    }

    public static function format_number($number)
    {
        if (Main::check_decimal($number)) {
            return number_format($number, 2, '.', ',');
        } else {
            return number_format($number, 0, '', ',');
        }
    }

    public static function format_number_system($number)
    {
        return number_format($number, 2, ',', '.');
    }

    public static function format_number_db($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 2, '.', '');
    }

    public static function format_decimal($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 0, ',', '.');
    }

    public static function format_discount($number)
    {
        return $number . ' %';
    }

    public static function format_date($date)
    {
        return date('d-m-Y', strtotime($date));
    }

    public static function format_date_name($date)
    {
        return date('d, M h:i A', strtotime($date));
    }

    public static function date()
    {
        return date('Y-m-d');
    }

    public static function datetime()
    {
        return date('Y-m-d H:i:s');
    }

    public static function format_date_label($date)
    {
        return date('d F Y', strtotime($date));
    }

    public static function format_date_db($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function format_time_db($time)
    {
        return date('H:i:s', strtotime($time));
    }

    public static function format_time_label($time)
    {
        return date('H:i A', strtotime($time));
    }

    public static function convert_money($str)
    {
        $find = array('Rp', '.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function encrypt($id)
    {
        return Crypt::encrypt($id);
    }

    public static function decrypt($id)
    {
        return Crypt::decrypt($id);
    }

    public static function convert_number($str)
    {
        $find = array('.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function convert_discount($str)
    {
        $find = array('%', ' ', '_');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function check_decimal($number)
    {
        if ($number - floor($number) >= 0.1) {
            return true;
        } else {
            return false;
        }
    }

    public static function metaTitle($breadcrumb)
    {
        krsort($breadcrumb);
        $title = '';
        foreach ($breadcrumb as $label => $value) {
            $title .= Main::menuAction($value['label']) . ' < ';
        }

        $title .= env("APP_NAME", "Private Label ID");

        return $title;

    }

    public static function pageTitle($breadcrumb = array())
    {
        krsort($breadcrumb);
        $title = isset($breadcrumb[1]) ? Main::menuAction($breadcrumb[1]['label']) : Main::menuAction($breadcrumb[0]['label']);

        return $title;

    }

    public static function menuAction($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = ucwords($string);

        return $string;
    }

    public static function menuStrip($string)
    {
        return str_replace([' ', '/'], '_', strtolower($string));
    }

    public static function menuActionData($menuActive)
    {
        $menuList = Main::menuAdministrator();
        $routeName = Route::currentRouteName();
        $user = Session::get('user');
        $user_new = mUser::find($user->user_id);
        $userRole = json_decode($user_new->user_role->user_role_access, TRUE);
        //$menuActive = '';
        $action = [];

        if ($menuActive == '') {
            foreach ($menuList as $menu => $val) {
                if ($val['route'] == $routeName) {
                    $menuActive = $menu;
                    break;
                } else {
                    if (isset($val['sub'])) {
                        foreach ($val['sub'] as $menu_sub => $val_sub) {
                            if ($val_sub['route'] == $routeName) {
                                $menuActive = $menu_sub;
                            }
                        }
                    }
                }
            }
        }
        if ($userRole) {
            foreach ($userRole as $menuName => $menuVal) {
                if ($menuName == $menuActive) {
                    $action = $menuVal;
                } else {
                    foreach ($menuVal as $menuSubName => $menuSubVal) {
                        if ($menuSubName == $menuActive) {
                            $action = $menuSubVal;
                        }
                    }
                }
            }
        }

        return $action;

    }

    public static function string_to_number($text)
    {
        return str_replace(',', '', $text);
    }

    public static function breadcrumb($breadcrumb_extend = array())
    {
        $cons = Config::get('constants.topMenu');

        $breadcrumb[] = [
            'label' => $cons['dashboard'],
            'route' => 'dashboardPage'
        ];

        $data['breadcrumb'] = array_merge($breadcrumb, $breadcrumb_extend);
        return view('component.breadcrumb', $data);

    }

    public static function no_seri_produk($month, $year, $id_produk, $id_lokasi, $urutan)
    {
        $id_kategori_produk = mProduk::select('id_kategori_produk')->where('id', $id_produk)->first()->id_kategori_produk;
        $kode_kategori_produk = mKategoriProduk::select('kode_kategori_produk')->where('id', $id_kategori_produk)->first()->kode_kategori_produk;
        $kode_lokasi = mLokasi::select('kode_lokasi')->where('id', $id_lokasi)->first()->kode_lokasi;

        return $month . $year . $kode_kategori_produk . $kode_lokasi . $urutan;
    }

    public static function urutan_produk($id_produk, $id_lokasi, $month, $year)
    {
        $urutan = 1;
        $where = [
            'id_produk' => $id_produk,
            'id_lokasi' => $id_lokasi,
            'month' => $month,
            'year' => $year
        ];

        $stok_urutan = mStokProduk::where($where);

        if ($stok_urutan->count() > 0) {
            $urutan_now = $stok_urutan->select('urutan')->orderBy('urutan', 'DESC')->first()->urutan;
            $urutan = $urutan_now + 1;
        }

        return $urutan;
    }

    public static function status($status)
    {
        switch ($status) {
            case "yes":
                return '<span class="m-badge m-badge--info m-badge--wide">Ya</span>';
                break;
            case "no":
                return '<span class="m-badge m-badge--warning m-badge--wide">Tidak</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function distributor_order_status($status)
    {
        switch ($status) {
            case "order":
                return '<span class="m-badge m-badge--info m-badge--wide">Order</span>';
                break;
            case "konfirm_admin":
                return '<span class="m-badge m-badge--accent m-badge--wide">Konfirmasi Admin</span>';
                break;
            case "konfirm_distributor":
                return '<span class="m-badge m-badge--warning m-badge--wide">Konfirmasi Distributor</span>';
                break;
            case "done":
                return '<span class="m-badge m-badge--success m-badge--wide">Selesai</span>';
                break;
            case "reject":
                return '<span class="m-badge m-badge--danger m-badge--wide">Batal</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function distributor_pengiriman($pengiriman)
    {
        switch ($pengiriman) {
            case "yes":
                return '<span class="m-badge m-badge--info m-badge--wide">Ya</span>';
                break;
            case "no":
                return '<span class="m-badge m-badge--warning m-badge--wide">Tidak</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function hutang_supplier_status($pengiriman)
    {
        switch ($pengiriman) {
            case "lunas":
                return '<span class="m-badge m-badge--info m-badge--wide">Lunas</span>';
                break;
            case "belum_lunas":
                return '<span class="m-badge m-badge--warning m-badge--wide">Belum Lunas</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function hutang_lain_status($pengiriman)
    {
        switch ($pengiriman) {
            case "lunas":
                return '<span class="m-badge m-badge--info m-badge--wide">Lunas</span>';
                break;
            case "belum_bayar":
                return '<span class="m-badge m-badge--warning m-badge--wide">Belum Bayar</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    /**
     * @return array
     *
     * Menu variable
     */
    public static function menuList()
    {

        $level = Session::get('level');

        if ($level == 'administrator') {
            return Main::menuAdministrator();
        } elseif ($level == 'distributor') {
            return Main::menuDistributor();
        }

    }

    public static function menuAdministrator()
    {
        $cons = Config::get('constants.topMenu');

        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage'
            ],
            $cons['dashboard_2'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage'
            ],
            $cons['dashboard_3'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage'
            ],
            $cons['masterData'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['master_1'] => [
                        'route' => 'clientPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_2'] => [
                        'route' => 'agencyPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_3'] => [
                        'route' => 'adminPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_15'] => [
                        'route' => 'agentPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_4'] => [
                        'route' => 'userRolePage',
                        'action' => [
                            'list',
                            'menu_akses',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_5'] => [
                        'route' => 'userPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_13'] => [
                        'route' => 'agencyTypePage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_11'] => [
                        'route' => 'companyTypePage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_10'] => [
                        'route' => 'companyPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_6'] => [
                        'route' => 'projectTypePage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_12'] => [
                        'route' => 'projectStatusPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_7'] => [
                        'route' => 'statusPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_8'] => [
                        'route' => 'channelPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_9'] => [
                        'route' => 'servicePage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_14'] => [
                        'route' => 'supportPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete',
                            'detail'
                        ]
                    ],
                    $cons['master_16'] => [
                        'route' => 'contentPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'view',
                            'delete',
                        ]
                    ],
                ]
            ],
            $cons['project'] => [
                'icon' => 'flaticon-file-2',
                'route' => '#',
                'sub' => [
                    $cons['project_1'] => [
                        'route' => 'projectListPage',
                        'action' => [
                            'list',
                            'create',
//                            'tasks',
                            'metric',
                            'file',
                            'edit',
                            'detail',
                            'delete',
                        ]
                    ],
                    $cons['agency_1'] => [
                        'route' => 'myClientPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'commisison_cost',
                            'detail',
                            'delete'
                        ]
                    ],
                ]
            ],
            $cons['message'] => [
                'icon' => 'flaticon-envelope',
                'route' => 'chatPage',
            ],
            $cons['services'] => [
                'icon' => 'fas fa-tasks',
                'route' => 'servicesPage'
            ],
            $cons['my_company'] => [
                'icon' => 'fas fa-building',
                'route' => 'clientCompanyPage'
            ],
            $cons['my_project'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'clientProjectPage'
            ],
            $cons['report'] => [
                'icon' => 'flaticon2-print',
                'route' => '#',
                'sub' => [
                    $cons['report_1'] => [
                        'route' => 'projectReportPage',
                        'action' => [
                            'print',
                        ]
                    ],
                    $cons['report_2'] => [
                        'route' => 'underconstructionPage',
                        'action' => [
                            'update',
                        ]
                    ],
                ]
            ],
            $cons['faq'] => [
                'icon' => 'flaticon2-phone',
                'route' => 'supportCenter'
            ],
            $cons['setting'] => [
                'icon' => 'flaticon-settings',
                'route' => 'underconstructionPage',
                ],

        ];
    }

    public static function menuDistributor()
    {
        $cons = Config::get('constants.topMenu');
        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
            ],
            $cons['transaksi_3'] => [
                'icon' => 'flaticon-arrows',
                'route' => 'orderOnlinePage'
            ]
        ];
    }

}
