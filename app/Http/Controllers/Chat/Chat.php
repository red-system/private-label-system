<?php

namespace app\Http\Controllers\Chat;

use app\Helpers\Main;
use app\Mail\sendMail;
use app\Models\mAgency;
use app\Models\mChat;
use app\Models\mChatData;
use app\Models\mClient;
use app\Models\mProject;
use app\Models\mProjectChat;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class Chat extends Controller
{
    private $view = [
        'project_order_number_label' => ["search_field" => "tb_project.project_order_number"],
        'project_order_title_label' => ["search_field" => "tb_project.project_order_title"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['message'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
//        $data = '';
//        Mail::to('iwayanwidiastika@gmail.com')->send(new sendMail($data));
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $tabel = explode('|', $user->from);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
//        $action = array();
//        foreach ($data['menuAction'] as $key => $value) {
//            if ($value) {
//                array_push($action, $key);
//            }
//        }
//        dd($action);
        if ($tabel[0] == 'agency_id'){
            mProject::where('agency_id', $tabel[1])->where('akses', 'new')->update(['akses' => 'old']);
        }
        $data['from'] = $tabel[0];
        $data['id'] = $tabel[1];
//        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);

        return view('chat/chat', $data);

    }

    function list(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $project_status_id = 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $chat = new mProjectChat();
        $result['iTotalRecords'] = $chat->count_all($from[0], $from[1], $project_status_id)->count();
        $result['iTotalDisplayRecords'] = $chat->count_filter($query, $this->view, $from[0], $from[1], $project_status_id)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $chat->list($start, $length, $query, $this->view, $from[0], $from[1], $project_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
//            $value->direct['detail'] = route('projectListDetail', ['id' => $value->project_id]);
//            $value->route['tasks'] = route('projectListTasks', ['id' => $value->project_id]);
//            $value->route['view'] = route('underconstructionPage');
//            $value->route['view'] = route('projectView', ['id' => $value->project_id]);
//            $value->direct['view'] = route('projectView', ['id' => $value->project_id]);
//            $value->route['metric'] = route('projectListInsertMetric', ['id' => $value->project_id]);
//            $value->route['file'] = route('projectListFile', ['id' => $value->project_id]);
//            $value->ignore['detail'] = true;
            $chat = mProjectChat::where('project_id', $value->project_id)->orderBy('created_at', 'DESC')->first();
            if(!empty($chat)){
                if($chat->dari == 'agency_id'){
                    $agency = DB::table('tb_user')->where('from', 'agency_id|'.$chat->agency_id)->first();
                    $chat->dari = $agency->user_name;
                }elseif($chat->dari == 'client_id'){
                    $client = DB::table('tb_user')->where('from', 'client_id|'.$chat->client_id)->first();
                    $chat->dari = $client->user_name;
                }elseif($chat->dari == 'agent_id'){
                    $agent = DB::table('tb_user')->where('from', 'agent_id|'.$chat->agent_id)->first();
                    $chat->dari = $agent->user_name;
                }
                $value->project_order_title_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => Main::encrypt($value->project_id)]) . "#chat_page". '">' . $value->project_order_title . '</a>'.
                    '<div>
                <span class="font-weight-bolder">'.$chat->dari.' -</span>
                    <a class="text-muted font-weight-bold text-hover-primary">'.$chat->chat.'</a>
                </div>';
            }
            else{
                $value->project_order_title_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => Main::encrypt($value->project_id)]) . "#chat_page". '">' . $value->project_order_title . '</a>';
            }

            $value->project_order_number_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => Main::encrypt($value->project_id)]) . "#chat_page". '">' . $value->project_order_number . '</a>';

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function index_chat($id)
    {
        $user = Session::get('user');
        mChatData::where('to', $user->from)->where('status', 'belum')->update(['status' => 'terbaca']);
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $result = explode('|', $data['user']->from);
        $chat_private = mChat::find($id);
        $to = '';
        if ($result[0] == 'agency_id') {
            $chat = mChat::where('agency_id', $result[1])->get();
            $client = mClient::where('agency_id', $result[1])->get();
            $to = 'client_id|'.$chat_private->client_id;
        } elseif ($result[0] == 'client_id') {
            $chat = mChat::where('client_id', $result[1])->get();
            $to = 'agency_id|'.$chat_private->agency_id;
        } else {
            $chat = mChat::all();
        }

        $data['chat_private'] = $chat_private;
        $data['chat'] = $chat;
        $data['to'] = $to;
        $data['result'] = $result;
        $data['chat_data'] = mChatData::where('chat_id', $id)->get();
        return view('chat/chatPrivate', $data);
    }

    function insert_new_chat(Request $request)
    {
        $rules = [
            'agency_id' => 'required',
            'client_id' => 'required',
        ];

        $attributes = [
            'agency_id' => 'Agency',
            'client_id' => 'Client',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_id = $request->input('agency_id');
        $client_id = $request->input('client_id');
        $count = mChat::where('agency_id', $agency_id)->where('client_id', $client_id)->count();
        if ($count > 0) {
            return response([
                'message' => '<strong>Chat Already Exist</strong>'
            ], 422);
        } else {
            $data = [
                'agency_id' => $agency_id,
                'client_id' => $client_id,
            ];
            $chat = mChat::create($data);
            return redirect(route('chatBox', ['id' => $chat->id]));
        }
    }

    function send_chat(Request $request, $id)
    {
        $rules = [
            'data_chat' => 'required',
        ];

        $attributes = [
            'data_chat' => 'Chat',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $from = $request->input('from');
        $to = $request->input('to');
        $data_chat = $request->input('data_chat');
        $status = $request->input('status');
        if (mChatData::where('chat_id', $id)->where('to', $to)->count() > 0) {
            mChatData::where('chat_id', $id)->where('to', $to)->update(['status' => 'terbaca']);
        }

        $data = [
            'chat_id' => $id,
            'from' => $from,
            'to' => $to,
            'data_chat' => $data_chat,
            'status' => $status,
        ];
        $chat = mChatData::create($data);
    }

}
