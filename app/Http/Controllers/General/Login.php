<?php

namespace app\Http\Controllers\General;

use app\Helpers\Main;
use app\Models\mUserRole;
use app\Rules\LoginRolesCheck;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Rules\LoginCheck;
use Illuminate\Support\Facades\Log;
use app\Models\mUser;
use Illuminate\Support\Facades\Session;

class Login extends Controller
{
    function index()
    {
//        dd($link);
//        $link = Main::decrypt($link);
//        $link = $link ? $link : 'sampi';
        $link = null;
        $data = [
            'pageTitle' => 'Login | ' . env('APP_NAME'),
//            'roles' => mUserRole::orderBy('user_role_name', 'ASC')->get(),
            'link' => $link
        ];

        return view('general/login', $data);
    }

    function index2($link)
    {
//        dd($link);
//        $link = Main::decrypt($link);
//        $link = $link ? $link : 'sampi';
        $data = [
            'pageTitle' => 'Login | ' . env('APP_NAME'),
            'roles' => mUserRole::orderBy('user_role_name', 'ASC')->get(),
            'link' => $link
        ];

        return view('general/login', $data);
    }
    function do(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => ['required', new LoginCheck($request->username, $request->user_role_id, $request->link)]
        ]);

    }

    function roles(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => ['required', new LoginRolesCheck($request->username)]
        ]);
    }
}
