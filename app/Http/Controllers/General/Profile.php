<?php

namespace app\Http\Controllers\General;

use app\Models\mDistributor;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use app\Models\mUser;

use app\Rules\UsernameCheckerUpdate;
use app\Rules\UsernameDistributorCheckerUpdate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class Profile extends Controller
{

    function index()
    {
        $breadcrumb = [
            [
                'label' => 'Profil',
                'route' => ''
            ]
        ];
        $data = Main::data($breadcrumb);
        $from = explode('|', $data['user']->from);
        $data['from'] = $from;
        $level = Session::get('level');
        if ($level == 'administrator') {
            return view('general/profile_administrator', $data);
        } else {
            return view('general/profile_distributor', $data);
        }
    }

    function update_administrator(Request $request)
    {
        $user = Session::get('user');
        $id = $user->user_id;

        if ($request->input('user_password') != null || $request->input('user_password') != "") {
            $request->validate([
                'user_name' => ['bail', 'required', new UsernameCheckerUpdate($id)],
                'user_password' => 'bail|required',
                'user_password_confirm' => 'bail|same:user_password|required',
//                'user_email' => 'bail|required|email'
            ]);
        } else {
            $request->validate([
                'user_name' => ['bail', 'required', new UsernameCheckerUpdate($id)],
                'user_password' => 'bail',
                'user_password_confirm' => 'bail|same:user_password',
//                'user_email' => 'bail|required|email'
            ]);
        }

        $data_user = [
            'user_name' => $request->input('user_name'),
//            'user_email' => $request->input('user_email'),
            'user_data' => $request->input('user_data'),
            'user_desc' => $request->input('user_desc'),
        ];

        if ($request->filled('user_password')) {
            if (Hash::check($request->input('old_password'), $user->user_password)) {
                $data_user['user_password'] = Hash::make($request->input('user_password'));
            } else {
                return response([
                    'message' => '<strong>Your old password is incorrect </strong>'
                ], 422);
            }
        }

        if ($request->hasFile('user_pict')) {
            $path = 'private-label-system';
            $ada = Storage::disk('sftp')->exists($path . '/' . $user->user_pict);
            if ($ada) {
                Storage::disk('sftp')->delete($path . '/' . $user->user_pict);
            }
            $file = $request->file('user_pict');
            Storage::disk('sftp')->putFileAs($path, $file, 'user-' . $user->user_id . '-' . $file->getClientOriginalName());
            $data_user['user_pict'] = 'user-' . $user->user_id . '-' . $file->getClientOriginalName();
        }
        mUser::where('user_id', $id)->update($data_user);

        $user = mUser::where('user_id', $id)->first();
        Session::put(['user' => $user]);
    }

    function update_distributor(Request $request)
    {
        $user = Session::get('user');
        $id_distributor = $user->id;

        $request->validate([
            'user_name' => ['bail', 'required', new UsernamedistributorCheckerUpdate($id_distributor)],
            'user_password' => 'bail',
            'user_password_confirm' => 'bail|same:user_password',
            'nama_distributor' => 'required',
            'alamat_distributor' => 'required',
            'telp_distributor' => 'required',
            'email_distributor' => 'bail|required|email'
        ]);

        $data_distributor = [
            'user_name' => $request->input('user_name'),
        ];
        if ($request->filled('user_password')) {
            $data_distributor['user_password'] = Hash::make($request->input('user_password'));
        }

        mDistributor::where('id', $id_distributor)->update($data_distributor);

        $data_distributor = [
            'nama_distributor' => $request->nama_distributor,
            'alamat_distributor' => $request->alamat_distributor,
            'telp_distributor' => $request->telp_distributor,
            'email_distributor' => $request->email_distributor
        ];

        if ($request->hasFile('foto_distributor')) {
            $file = $request->file('foto_distributor');
            $file->move('upload', $file->getClientOriginalName());
            $data_distributor['foto_distributor'] = $file->getClientOriginalName();
        }
        mDistributor::where('id', $id_distributor)->update($data_distributor);

        $user = mDistributor::find($id_distributor);

        if ($user->foto_distributor == '') {
            $user->foto_distributor = 'empty.png';
        }

        Session::put(['user' => $user]);

    }
}
