<?php

namespace app\Http\Controllers\General;

use app\Imports\importNew;
use app\Models\mAgency;
use app\Models\mClient;
use app\Models\mGrowth;
use app\Models\mProject;
use app\Models\mServicesContent;
use app\Models\mSupport;
use app\Models\mSupportData;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
        $level = Session::get('level');
        if ($level == 'distributor') {
            $data = $this->data_dashboard_distributor($request);
            return view('dashboard/dashboard_distributor', $data);
        } elseif ($level == 'administrator') {
            $data = $this->data_dashboard_admin($request);
            return view('dashboard/dashboard_admin', $data);
        }

    }

    function data_dashboard_admin($request)
    {
        $user = Session::get('user');
        $users = mUser::where('user_name', $user->user_name)->first();
        $date_range_get = $request->input('date_range');
        $date_range = explode(' - ', $date_range_get);
        $from = explode('|', $user->from);
        if ($from[0] == 'agency_id') {
            $project_data = mProject::where('agency_id', $from[1])->with(['channel', 'client', 'project_status'])->orderBy('created_at', 'DESC')->paginate(5);
            foreach ($project_data as $value) {

                $client = DB::table('tb_client')->where('client_id', $value->client_id)->first();
                $channel = DB::table('tb_channel')->where('channel_id', $value->channel_id)->first();
//                if($value->client){
                $value->client_name = $client->client_name;
                $value->channel_name = $channel->channel_name;
//                }
                $value->agreed_fee = 0;
//                $value->project_status_name = '<span class="label font-weight-bold label-lg label-inline" style="color: ' . $value->project_status->project_status_text_color . '; background-color: ' . $value->project_status->project_status_bc_color . ';">' . $value->project_status->project_status_name . '</span>';
            }
        } else {
            $project_data = [];
        }

        $date_start = $date_range_get ? $date_range[0] : date('01-m-Y');
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $date_range_get ? $date_range[1] : date('d-m-Y');
        $date_end_db = Main::format_date_db($date_end);
        $where_date = [$date_start_db, $date_end_db];
        $date_year_now = date('Y', strtotime($date_end_db));
        $data_penjualan = [];

        $data = Main::data($this->breadcrumb);
        $total_hpp = 0;
        $total_biaya = 0;

        $total_nominal_transaksi = 0;
        $gross_profit = $total_nominal_transaksi - $total_hpp;
        $net_profit = $total_nominal_transaksi - $total_hpp - $total_biaya;
        $total_data_transaksi = 0;
        $produk = [];
        $bahan = [];

        foreach ($this->bulan as $index => $month) {
            $total_penjualan = 0;
            $data_penjualan[] = [
                'month' => $month,
                'total_penjualan' => $total_penjualan
            ];
        }
        $growth = [];
        if ($from[0] != 'client_id') {
            $growth_data = mGrowth::where('from', $from[0])->where('from_id', $from[1])->orderBy('created_at', 'desc')->first();
            if ($growth_data) {
                $growth = [
                    0 => json_decode($growth_data->fullfillment),
                    1 => json_decode($growth_data->profit),
                    2 => json_decode($growth_data->revenue),
                    3 => json_decode($growth_data->bulan),
                ];
            }
        }


//        $growth = [
//            0 => [3800.80, 3444.92, 9259.21, 13170.19, 11686.32, 19000.72],
//            1 => [25338.64, 22966.11, 61728.05, 87801.27, 77908.79, 126671.46],
//            2 => [168924.28, 153107.42, 411520.31, 585341.79, 519391.90, 844476.42],
//            3 => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
//
//        ];

//        dd($growth);

        $data = array_merge($data, array(
            'total_nominal_transaksi' => Main::format_number($total_nominal_transaksi),
            'gross_profit' => Main::format_number($gross_profit),
            'net_profit' => Main::format_number($net_profit),
            'total_data_transaksi' => Main::format_number($total_data_transaksi),
            'produk' => $produk,
            'bahan' => $bahan,
            'agency' => mAgency::all(),
            'client' => mClient::all(),
            'data_penjualan' => $data_penjualan,
            'user' => $users,
            'penjualan_produk_count' => 0,
            'detail_produksi_count' => 0,
            'total_penggunaan_bahan' => 0,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'from' => $from,
            'project_data' => $project_data,
            'growth' => json_encode($growth),
        ));
        return $data;
    }

    function data_dashboard_distributor()
    {

        $data = Main::data($this->breadcrumb);
        $id_distributor = Session::get('user.id');

        $list_piutang_pelanggan = mPiutangPelanggan
            ::where([
                'id_distributor' => $id_distributor,
                'pp_status' => 'belum_bayar'
            ])
            ->orderBy('id', 'ASC')
            ->get();
        $list_piutang_lain = mPiutangLain
            ::where([
                'id_distributor' => $id_distributor,
                'pl_status' => 'belum_bayar'
            ])
            ->orderBy('id_distributor', $id_distributor)
            ->get();
        $list_penjualan = mPenjualan
            ::where([
                'id_distributor' => $id_distributor
            ])
            ->orderBy('id', 'DESC')
            ->get();

        $data = array_merge($data, array(
            'list_piutang_pelanggan' => $list_piutang_pelanggan,
            'list_piutang_lain' => $list_piutang_lain,
            'list_penjualan' => $list_penjualan,

            'no_piutang' => 1,
            'no_history' => 1,
            'piutang_total' => 0,
            'history_total' => 0,
        ));

        return $data;
    }

    function detail_piutang_pelanggan($id)
    {
        $id_piutang_pelanggan = Main::decrypt($id);
        $piutang_pelanggan = mPiutangPelanggan::find($id_piutang_pelanggan);
        $penjualan = mPenjualan
            ::where([
                'id' => $piutang_pelanggan->id_penjualan
            ])
            ->first();
        $penjualan_produk = mPenjualanProduk
            ::with([
                'produk',
                'agencies',
                'stok_produk'
            ])
            ->where([
                'id_penjualan' => $piutang_pelanggan->id_penjualan
            ])
            ->get();

        $data = [
            'piutang_pelanggan' => $piutang_pelanggan,
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1
        ];

        return view('dashboard/piutangPelangganDetail', $data);
    }

    function import(Request $request)
    {
        $rules = [
            'file' => 'required|mimes:csv,xls,xlsx',
            'agency_id' => 'required'
        ];

        $attributes = [
            'file' => 'File Excel',
            'agency_id' => 'Agency',
        ];
        /**
         * proses validasi
         */
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        if ($from[0] == 'client_id') {
            return redirect(route('cannotPage'));
        }
//        $user = Session::get('user');
//        $from = explode('|', $user->from);
        $file = $request->file('file');
//        $admin_id = $from[1];
        $import = new importNew;
//        Excel::import(new importNew, $file);
        Excel::import($import, $file);

//        return response([
//            'message' => 'data <strong>' . $import->getId() . '</strong>'
//        ], 422);
        $agency_id = $request->input('agency_id');
        if ($agency_id == 'admin') {
            $data_update = [
                'from' => 'admin_id',
                'from_id' => $from[1]
            ];
        } else {
            $data_update = [
                'from' => 'agency_id',
                'from_id' => $agency_id
            ];
        }


        mGrowth::where('growth_id', $import->getId())->update($data_update);

    }

    function detail_piutang_lain($id)
    {
        $id_piutang_lain = Main::decrypt($id);
        $piutang_lain = mPiutangLain::find($id_piutang_lain);

        $data = [
            'piutang_lain' => $piutang_lain,
        ];

        return view('dashboard/piutangLainDetail', $data);
    }

    function detail_history_belanja($id)
    {
        $id_penjualan = Main::decrypt($id);
        $penjualan = mPenjualan
            ::where([
                'id' => $id_penjualan
            ])
            ->first();
        $penjualan_produk = mPenjualanProduk
            ::with([
                'produk',
                'agencies',
                'stok_produk'
            ])
            ->where([
                'id_penjualan' => $id_penjualan
            ])
            ->get();

        $data = [
            'penjualan' => $penjualan,
            'penjualan_produk' => $penjualan_produk,
            'no' => 1
        ];

        return view('dashboard/historyBelanjaDetail', $data);
    }

    function cannot()
    {
        $data = Main::data($this->breadcrumb);
        return view('general/cannot', $data);
    }

    function company_null()
    {
        $data = Main::data($this->breadcrumb);
        return view('general/companyNull', $data);
    }

    function support_center($id)
    {
        $data = Main::data($this->breadcrumb);
        $data['support'] = mSupport::all();

        $data['id'] = $id;
        $support_data = mSupportData::where('support_id', $id)->get();
        foreach ($support_data as $value) {
            $value->support_id = Main::encrypt($value->support_id);
            $value->support_data_id = Main::encrypt($value->support_data_id);
//            $value->support_desc = str_replace('>', '&gt;', $value->support_desc);
        }
        $data['support_data'] = $support_data;
//        dd($data);
        return view('general/supportCenter', $data);
    }
}
