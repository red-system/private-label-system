<?php

namespace app\Http\Controllers\General;

use app\Helpers\Main;
use app\Models\mServicesContent;
use app\Models\mSupport;
use app\Models\mSupportData;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;

class Services extends Controller
{

    function index()
    {
        $breadcrumb_services = [
            [
                'label' => 'Services',
                'route' => ''
            ]
        ];
        $data = Main::data($breadcrumb_services);
        $content = [];
        $services = mServicesContent::all();

        foreach ($services as $s){
            $content[$s->row][$s->block] = $s;
        }
        $data['content'] = $content;
        return view('masterData/servicesContent/front/services', $data);
    }

    function services_detail($id)
    {
        $id = Main::decrypt($id);
        $breadcrumb_services = [
            [
                'label' => 'Services',
                'route' => ''
            ],
            [
                'label' => 'Detail',
                'route' => ''
            ]
        ];
        $data = Main::data($breadcrumb_services);
        $services = mServicesContent::find($id);
        $faq = mSupportData::where('support_id', $services->faq_id)->get();
        $judul = json_decode($services->text_title);
        $isi = json_decode($services->text_detail);

        $content = [];
        foreach ($judul as $key => $title){
            $text = $isi[$key];
            $bersih = strip_tags($text);
            $content[$key] = [
                'judul' => $title,
                'isi_label' => str_limit($bersih, 490),
                'panjang' => strlen($bersih),
                'isi' => $isi[$key],
            ];
        }

        $data['faq'] = $faq;
        $data['content'] = $content;
        $data['services'] = $services;
        return view('masterData/servicesContent/front/services_detail', $data);
    }
}
