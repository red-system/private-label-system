<?php

namespace app\Http\Controllers\Project;

use app\Helpers\Main;
use app\Models\mAgency;
use app\Models\mCompany;
use app\Models\mFile;
use app\Models\mMetric;
use app\Models\mProject;
use app\Models\mProjectStatus;
use app\Models\mProjectType;
use app\Models\mService;
use app\Models\mTasks;
use PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ProjectReport extends Controller
{
    private $view = [
        'project_order_title' => ["search_field" => "tb_project.project_order_title"],
        'project_status_name' => ["search_field" => "tb_project_status.project_status_name"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['project'],
                'route' => ''
            ],
            [
                'label' => $cons['project_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $tabel = explode('|', $user->from);
        $status = mProjectStatus::get();
        $project_status_id = $request->input('project_status_id');
        $project_status_id = $project_status_id ? $project_status_id : 'all';
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['params'] = [
            'project_status_id' => $project_status_id
        ];
        $data['project_status_id'] = $project_status_id;
        $data['status'] = $status;
        $data['from'] = $tabel[0];
        $data['id'] = $tabel[1];
        $data['services'] = mService::all();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['agency'] = mAgency::orderBy('tb_agency.agency_name', 'ASC')->get();
        $data['project_type'] = mProjectType::orderBy('tb_project_type.project_type_name', 'ASC')->get();
        $data['project_status'] = mProjectStatus::orderBy('tb_project_status.project_status_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);

        if ($tabel[0] != 'admin_id' || $tabel[0] != 'agent_id') {
            return redirect(route('cannotPage'));
        }


        return view('project/projectReport/projectReportList', $data);
    }

    function list(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $project_status_id = $request->input('project_status_id');
        $project_status_id = $project_status_id ? $project_status_id : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $project = new mProject();
        $result['iTotalRecords'] = $project->count_all($from[0], $from[1], $project_status_id);
        $result['iTotalDisplayRecords'] = $project->count_filter($query, $this->view, $from[0], $from[1], $project_status_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $project->list($start, $length, $query, $this->view, $from[0], $from[1], $project_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->route['print'] = route('projectReportPrint', ['id' => $value->project_id]);
            $value->target_blank['print'] = 'ada';
            $metric = mMetric::where('project_id', '=', $value->project_id)->count();
            if($metric == 0){
                $value->visibility['print'] = 'none';
            }
            $value->project_status_name = '<span class="label font-weight-bold label-lg label-inline" style="color: '.$value->project_status_text_color.'; background-color: '.$value->project_status_bc_color.';">'.$value->project_status_name.'</span>';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function print($id)
    {
        $data = Main::data($this->breadcrumb);

        $list = mProject::find($id);
        /**
         * mencari data tasks
         */
        $next_tasks = mTasks::where('project_id', '=', $id)->where('tasks_status', '=', 'Next Tasks')->get();
        $completed = mTasks::where('project_id', '=', $id)->where('tasks_status', '=', 'Completed')->get();

        /**
         * Mencari data Metric
         */
        $metric = new mMetric();

        /**
         * mencari data metric dari bulan terakhir
         */
        $bulan_ini = [
            0 => 'ada',
            1 => $metric->bulan_ini($id)
        ];
        /**
         * memecah tanggal, bulan dan tahun
         */
        $date = explode('-', $bulan_ini[1]->date);

        /**
         * mencari jumlah metric dari project
         */
        $metric_list = mMetric::where('project_id', '=', $id)->get();
        $count = $metric_list->count();

        /**
         * jika nilai tahun saat ini sama dengan tahun metric
         */
        if (date('Y') == $date[0]) {
            /**
             * membuat kondisi jika nilai bulan dari metric sama dengan nilai bulan ini
             */
            if ($date[1] == date('m')) {

                /**
                 * jika jumlah metric hanya 1 maka bulan lalu kosong
                 * karena nilai bulan di metric ada bulan ini
                 */
                if ($count == 1) {
                    $bulan_lalu = [
                        0 => 'kosong'
                    ];
                } /**
                 * jika tidak maka nilai nulan lalu ada
                 */
                elseif ($count > 1) {
                    $bulan_lalu = [
                        0 => 'ada',
                        1 => $metric->bulan_lalu($id, $date[1], $date[0])
                    ];
                }
            } /**
             * jika bulan metric lebih kecil dari bulan sekarang
             */
            elseif ($date[1] < date('m')) {
                /**
                 * maka bulan ini kosong
                 */
                $bulan_ini = [
                    0 => 'kosong'
                ];
                /**
                 * bulan lalu ada karena nilai bulan metric lebih kecil dari bulan sekarang
                 */
                $bulan_lalu = [
                    0 => 'ada',
                    1 => $metric->bulan_ini($id)
                ];
            }
        } /**
         * jika nilai tahun ini lebih besar dari matric
         */
        elseif (date('Y') > $date[0]) {
            /**
             * maka bulan ini kosong karena tahun nya lebih kecil
             */
            $bulan_ini = [
                0 => 'kosong'
            ];
            /**
             * bulan lalu ada karena nilai bulan metric lebih kecil dari bulan sekarang
             */
            $bulan_lalu = [
                0 => 'ada',
                1 => $metric->bulan_ini($id)
            ];
        }

        /**
         * mencari data file
         */
        $file = mFile::where('project_id', '=', $id)->get();

        if($bulan_ini[0] == 'ada' && $bulan_lalu[0] == 'ada'){
            $change = [
                0 => ($bulan_ini[1]->impr - $bulan_lalu[1]->impr),
                1 => ($bulan_ini[1]->clicks - $bulan_lalu[1]->clicks),
                2 => ($bulan_ini[1]->spend - $bulan_lalu[1]->spend),
                3 => ($bulan_ini[1]->all_rev - $bulan_lalu[1]->all_rev),
                4 => ($bulan_ini[1]->paid_rev - $bulan_lalu[1]->paid_rev),
                5 => ($bulan_ini[1]->roas - $bulan_lalu[1]->roas),
            ];

            $change_persen = [
                0 => ($change[0]/$bulan_ini[1]->impr*100),
                1 => ($change[1]/$bulan_ini[1]->clicks*100),
                2 => ($change[2]/$bulan_ini[1]->spend*100),
                3 => ($change[3]/$bulan_ini[1]->all_rev*100),
                4 => ($change[4]/$bulan_ini[1]->paid_rev*100),
                5 => ($change[5]/$bulan_ini[1]->roas*100),
            ];
        }
        elseif ($bulan_ini[0] == 'kosong'){
            $change = [
                0 => '-'.$bulan_lalu[1]->impr,
                1 => '-'.$bulan_lalu[1]->clicks,
                2 => '-'.$bulan_lalu[1]->spend,
                3 => '-'.$bulan_lalu[1]->all_rev,
                4 => '-'.$bulan_lalu[1]->paid_rev,
                5 => '-'.$bulan_lalu[1]->roas,
            ];

            $change_persen = [
                0 => '-100',
                1 => '-100',
                2 => '-100',
                3 => '-100',
                4 => '-100',
                5 => '-100',
            ];
        }
        elseif ($bulan_lalu[0] == 'kosong'){
            $change = [
                0 => $bulan_ini[1]->impr,
                1 => $bulan_ini[1]->clicks,
                2 => $bulan_ini[1]->spend,
                3 => $bulan_ini[1]->all_rev,
                4 => $bulan_ini[1]->paid_rev,
                5 => $bulan_ini[1]->roas,
            ];

            $change_persen = [
                0 => '100',
                1 => '100',
                2 => '100',
                3 => '100',
                4 => '100',
                5 => '100',
            ];
        }


        $data = array_merge($data, [
            'list' => $list,
            'next_tasks' => $next_tasks,
            'completed' => $completed,
            'bulan_lalu' => $bulan_lalu,
            'bulan_ini' => $bulan_ini,
            'file' => $file,
            'change' => $change,
            'change_persen' => $change_persen,
        ]);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('project/projectReport/projectReportPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Project Report (' . $list->project_order_title . ')');
    }
}
