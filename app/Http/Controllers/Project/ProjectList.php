<?php

namespace app\Http\Controllers\Project;

use app\Helpers\Main;
use app\Mail\sendMail;
use app\Models\mAgency;
use app\Models\mAgent;
use app\Models\mClient;
use app\Models\mCompany;
use app\Models\mFile;
use app\Models\mMetric;
use app\Models\mProject;
use app\Models\mProjectChat;
use app\Models\mProjectStatus;
use app\Models\mProjectType;
use app\Models\mService;
use app\Models\mSubProject;
use app\Models\mTasks;
use app\Models\mUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProjectList extends Controller
{
    private $view = [
        'project_order_number_label' => ["search_field" => "tb_project.project_order_number"],
        'channel_name_label' => ["search_field" => "tb_channel.channel_name"],
        'client_name_label' => ["search_field" => "tb_client.client_name"],
        'project_order_title_label' => ["search_field" => "tb_project.project_order_title"],
        'project_order_date_label' => ["search_field" => "tb_project.project_order_date"],
        'project_status_name_label' => ["search_field" => "tb_project_status.project_status_name"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['project'],
                'route' => ''
            ],
            [
                'label' => 'All Project',
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $tabel = explode('|', $user->from);
        $status = mProjectStatus::get();
        $project_status_id = $request->input('project_status_id');
        $project_status_id = $project_status_id ? $project_status_id : 'all';
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        if ($tabel[0] == 'admin_id' || $tabel[0] == 'agent_id') {
            foreach ($data['menuAction'] as $key => $value) {
                if ($value) {
                    array_push($action, $key);
                }
            }
        }
//        dd($action);
        $data['params'] = [
            'project_status_id' => $project_status_id
        ];
        if ($tabel[0] == 'agency_id') {
            mProject::where('agency_id', $tabel[1])->where('akses', 'new')->update(['akses' => 'old']);
        }
        $data['project_status_id'] = $project_status_id;
        $data['status'] = $status;
        $data['from'] = $tabel[0];
        $data['id'] = $tabel[1];
        $data['services'] = mService::all();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['agency'] = mAgency::orderBy('tb_agency.agency_name', 'ASC')->get();
        $data['project_type'] = mProjectType::orderBy('tb_project_type.project_type_name', 'ASC')->get();
        $data['project_status'] = mProjectStatus::orderBy('tb_project_status.project_status_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);


        return view('project/projectList/projectList', $data);
    }

    function index2(Request $request, $id)
    {
        $project_status_id = $id;
        $project_status_id = $project_status_id ? $project_status_id : 'all';
        $project_status = mProjectStatus::find($project_status_id);
        $cons = Config::get('constants.topMenu');
        $breadcrumb = [
            [
                'label' => $cons['project'],
                'route' => ''
            ],
            [
                'label' => $project_status->project_status_name,
                'route' => ''
            ]
        ];
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($breadcrumb, 'project-status-' . $id);
        $user = Session::get('user');
        $tabel = explode('|', $user->from);
        $status = mProjectStatus::get();
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
//        $action = array();
//        foreach ($data['menuAction'] as $key => $value) {
//            if ($value) {
//                array_push($action, $key);
//            }
//        }
        $action = [
            0 => "akses_menu",
            1 => "list",
            2 => "create",
            3 => "tasks",
            4 => "metric",
            5 => "file",
            6 => "edit",
            7 => "detail",
            8 => "delete"
        ];
        if ($tabel[0] == 'agency_id') {
            mProject::where('agency_id', $tabel[1])->where('project_status_id', $id)->where('akses', 'new')->update(['akses' => 'old']);
        }
        $data['params'] = [
            'project_status_id' => $project_status_id
        ];
        $data['project_status_id'] = $project_status_id;
        $data['status'] = $status;
        $data['from'] = $tabel[0];
        $data['id'] = $tabel[1];
        $data['services'] = mService::all();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['agency'] = mAgency::orderBy('tb_agency.agency_name', 'ASC')->get();
        $data['project_type'] = mProjectType::orderBy('tb_project_type.project_type_name', 'ASC')->get();
        $data['project_status'] = mProjectStatus::orderBy('tb_project_status.project_status_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);


        return view('project/projectList/projectList', $data);
    }

    function list(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $project_status_id = $request->input('project_status_id');
        $project_status_id = $project_status_id ? $project_status_id : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $project = new mProject();
        $result['iTotalRecords'] = $project->count_all($from[0], $from[1], $project_status_id);
        $result['iTotalDisplayRecords'] = $project->count_filter($query, $this->view, $from[0], $from[1], $project_status_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $project->list($start, $length, $query, $this->view, $from[0], $from[1], $project_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
//            $value->project_id = Main::encrypt($value->project_id);
            $value->route['delete'] = route('projectListDelete', ['id' => Main::encrypt($value->project_id)]);
            $value->route['edit'] = route('projectListUpdate', ['id' => Main::encrypt($value->project_id)]);
            $value->route['detail'] = route('projectListDetail', ['id' => Main::encrypt($value->project_id)]);
            $value->ignore['detail'] = true;
            if ($from[0] == 'admin_id' || $from[0] == 'agent_id') {
                $value->direct['detail'] = route('projectListDetail', ['id' => Main::encrypt($value->project_id)]);
                $value->route['tasks'] = route('projectListTasks', ['id' => Main::encrypt($value->project_id)]);
                $value->route['view'] = route('projectView', ['id' => Main::encrypt($value->project_id)]);
                $value->route['view'] = route('projectView', ['id' => Main::encrypt($value->project_id)]);
                $value->direct['view'] = route('projectView', ['id' => Main::encrypt($value->project_id)]);
                $value->route['metric'] = route('projectListInsertMetric', ['id' => Main::encrypt($value->project_id)]);
                $value->route['file'] = route('projectListFile', ['id' => Main::encrypt($value->project_id)]);
                $value->ignore['detail'] = true;
            }
            $value->project_cost = Main::format_number($value->project_cost);
            $value->project_order_number_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->project_order_number . '</a>';
            $value->channel_name_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->channel_name . '</a>';
            $value->client_name_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->client_name . '</a>';
            $value->project_order_title_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->project_order_title . '</a>';
            $value->project_order_date_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . Main::format_date($value->project_order_date) . '</a>';
            $value->project_status_name_label = '<span class="label font-weight-bold label-lg label-inline" style="color: ' . $value->project_status_text_color . '; background-color: ' . $value->project_status_bc_color . ';">' . $value->project_status_name . '</span>';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function insert(Request $request)
    {
        $rules = [
            'project_order_number' => 'required',
            'company_id' => 'required',
            'services_id' => 'required',
            'project_ammount' => 'required',
            'project_cost' => 'required',
            'project_timeline' => 'required',
            'project_progress' => 'required',
            'project_order_date' => 'required',
            'project_status_id' => 'required',
            'project_type_id' => 'required',
        ];

        $attributes = [
            'project_order_number' => 'Order ID',
            'company_id' => 'Company',
            'services_id' => 'Services Type',
            'project_ammount' => 'Project Ammount',
            'project_cost' => 'Project Cost',
            'project_timeline' => 'Project Timeline',
            'project_progress' => 'Project Progress',
            'project_order_date' => 'Order Date',
            'project_status_id' => 'Project Status',
            'project_type_id' => 'Project Type',
        ];

        $user = Session::get('user');
        $tabel = explode('|', $user->from);

        if ($tabel[0] == 'admin_id' || $tabel[0] == 'agent_id') {
            $rules['agency_id'] = 'required';
            $attributes['agency_id'] = 'Agency';
        }

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $project_order_number = $request->input('project_order_number');
        $company_id = $request->input('company_id');
        $company = mCompany::find($company_id);
        $client_id = $company->client_id;
        $client = mClient::find($client_id);
        $services_id = $request->input('services_id');
        $services = mService::find($services_id);
        $channel_id = $services->channel_id;
        $agency_id = $request->input('agency_id');
        $project_status_id = $request->input('project_status_id');
        $project_type_id = $request->input('project_type_id');
        $project_ammount = Main::string_to_number($request->input('project_ammount'));
        $project_cost = Main::string_to_number($request->input('project_cost'));
        $project_timeline = $request->input('project_timeline');
        $project_progress = $request->input('project_progress');
        $project_order_date = Main::format_date_db($request->input('project_order_date'));
        $month = date("m", strtotime($project_order_date));
        $project_order_title = $client->client_name . '.' . $services->channel->channel_name . '.' . $month;


        /**
         * kondisi penting saat simpan project
         */
        if ($tabel[0] != 'admin_id' && $tabel[0] != 'agent_id') {
            return response([
                'message' => 'You do not have access rights to this menu'
            ], 422);
        }

        DB::beginTransaction();
        try {
            $data = [
                'company_id' => $company_id,
                'agency_id' => $agency_id,
                'client_id' => $client_id,
                'services_id' => $services_id,
                'channel_id' => $channel_id,
                'project_status_id' => $project_status_id,
                'project_type_id' => $project_type_id,
                'project_order_number' => $project_order_number,
                'project_order_title' => $project_order_title,
                'project_ammount' => $project_ammount,
                'project_cost' => $project_cost,
                'project_timeline' => $project_timeline,
                'project_progress' => $project_progress,
                'project_order_date' => $project_order_date,
                'user_id' => $user->user_id,
                'akses' => 'new'
            ];
            if ($tabel[0] == 'agency_id') {
                $data['agency_id'] = $tabel[1];
            }

            $project = mProject::create($data);

            $sub_project = array();
            $sub_project[0] = [
                'project_id' => $project->project_id,
                'services_title' => 'Keyword Refinements',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'sub_project',
            ];
            $sub_project[1] = [
                'project_id' => $project->project_id,
                'services_title' => 'Ad Creative Refinements',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'sub_project',
            ];
            $sub_project[2] = [
                'project_id' => $project->project_id,
                'services_title' => 'Bid Management',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'sub_project',
            ];
            $sub_project[3] = [
                'project_id' => $project->project_id,
                'services_title' => 'Campaign Refinements',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'sub_project',
            ];
            $sub_project[4] = [
                'project_id' => $project->project_id,
                'services_title' => '- Week 1',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[5] = [
                'project_id' => $project->project_id,
                'services_title' => '- Week 2',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[6] = [
                'project_id' => $project->project_id,
                'services_title' => '- Week 3',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[7] = [
                'project_id' => $project->project_id,
                'services_title' => '- Week 4',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[8] = [
                'project_id' => $project->project_id,
                'services_title' => 'Google Analytics',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[9] = [
                'project_id' => $project->project_id,
                'services_title' => 'Conversion Tracking',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[10] = [
                'project_id' => $project->project_id,
                'services_title' => 'Campaign Performance Report',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[11] = [
                'project_id' => $project->project_id,
                'services_title' => 'Client Supprt',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];
            $sub_project[12] = [
                'project_id' => $project->project_id,
                'services_title' => 'Quality Check',
                'qty' => 1,
                'status' => 'In Progress',
                'start_date' => $project_order_date,
                'end_date' => Carbon::parse($project_order_date)->addDays(30),
                'assign_to' => 0,
                'position' => 'campaign_monitoring',
            ];

            foreach ($sub_project as $sp) {
                mSubProject::create($sp);
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'project_order_number' => 'required',
            'company_id' => 'required',
            'services_id' => 'required',
            'project_ammount' => 'required',
            'project_cost' => 'required',
            'project_timeline' => 'required',
            'project_progress' => 'required',
            'project_order_date' => 'required',
            'project_status_id' => 'required',
            'project_type_id' => 'required',
        ];

        $attributes = [
            'project_order_number' => 'Order ID',
            'company_id' => 'Company',
            'services_id' => 'Services Type',
            'project_ammount' => 'Project Ammount',
            'project_cost' => 'Project Cost',
            'project_timeline' => 'Project Timeline',
            'project_progress' => 'Project Progress',
            'project_order_date' => 'Order Date',
            'project_status_id' => 'Project Status',
            'project_type_id' => 'Project Type',
        ];

        $user = Session::get('user');
        $tabel = explode('|', $user->from);

        if ($tabel[0] == 'admin_id' || $tabel[0] == 'agent_id') {
            $rules['agency_id'] = 'required';
            $attributes['agency_id'] = 'Agency';
        }

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $project_order_number = $request->input('project_order_number');
        $company_id = $request->input('company_id');
        $company = mCompany::find($company_id);
        $client_id = $company->client_id;
        $client = mClient::find($client_id);
        $services_id = $request->input('services_id');
        $services = mService::find($services_id);
        $channel_id = $services->channel_id;
        $agency_id = $request->input('agency_id');
        $project_status_id = $request->input('project_status_id');
        $project_type_id = $request->input('project_type_id');
        $project_ammount = Main::string_to_number($request->input('project_ammount'));
        $project_cost = Main::string_to_number($request->input('project_cost'));
        $project_timeline = $request->input('project_timeline');
        $project_progress = $request->input('project_progress');
        $project_order_date = Main::format_date_db($request->input('project_order_date'));
        $month = date("m", strtotime($project_order_date));
        $project_order_title = $client->client_name . '.' . $services->channel->channel_name . '.' . $month;


        /**
         * kondisi penting saat simpan project
         */
        if ($tabel[0] != 'admin_id' && $tabel[0] != 'agent_id') {
            return response([
                'message' => 'You do not have access rights to this menu'
            ], 422);
        }
        DB::beginTransaction();
        try {
            $data = [
                'company_id' => $company_id,
                'agency_id' => $agency_id,
                'client_id' => $client_id,
                'services_id' => $services_id,
                'channel_id' => $channel_id,
                'project_status_id' => $project_status_id,
                'project_type_id' => $project_type_id,
                'project_order_number' => $project_order_number,
                'project_order_title' => $project_order_title,
                'project_ammount' => $project_ammount,
                'project_cost' => $project_cost,
                'project_timeline' => $project_timeline,
                'project_progress' => $project_progress,
                'project_order_date' => $project_order_date,
                'user_id' => $user->user_id,
            ];
            if ($tabel[0] == 'agency_id') {
                $data['agency_id'] = $tabel[1];
            }

            mProject::where(['project_id' => $id])->update($data);

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $user = Session::get('user');
        $tabel = explode('|', $user->from);

        if ($tabel[0] != 'admin_id' && $tabel[0] != 'agent_id') {
            return response([
                'message' => 'You do not have access rights to this menu'
            ], 422);
        }

        mProject::where('project_id', $id)->delete();
    }

    function detail($id)
    {
        $id = Main::decrypt($id);

        $cons = Config::get('constants.topMenu');
        $breadcrumb = [
            [
                'label' => $cons['project'],
                'route' => ''
            ],
            [
                'label' => 'Client View',
                'route' => ''
            ]
        ];
        $data = Main::data($breadcrumb);
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $chat = $this->chat($id);

        $count = mSubProject::where('project_id', $id)->count();
        $project = mProject::where('project_id', $id)->first();


        $sub_project = mSubProject::where('project_id', $id)->where('position', 'sub_project')->get();
        $campaign_monitoring = mSubProject::where('project_id', $id)->where('position', 'campaign_monitoring')->get();

        foreach ($sub_project as $sp) {
            if ($sp->assign_to == '' || $sp->assign_to == 0) {
                $sp->assign_to_label = 'None';
            } else {
                $agent = mAgent::find($sp->assign_to);
                $sp->assign_to_label = $agent->agent_name;
            }
        }

        foreach ($campaign_monitoring as $cm) {
            if ($cm->assign_to == '' || $cm->assign_to == 0) {
                $cm->assign_to_label = 'None';
            } else {
                $agent = mAgent::find($cm->assign_to);
                $cm->assign_to_label = $agent->agent_name;
            }
        }

        $count_less = count($chat['chat']);
        $count_all = count($chat['chat_all']);

        if ($count_all == $count_less) {
            $count = 'sama';
        } else {
            $count = 'beda';
        }
        $data['project'] = $project;
        $data['project']->project_order_date = Main::format_date($data['project']->project_order_date);

        $data['agent'] = mAgent::all();
        $data['sub_project'] = $sub_project;
        $data['campaign_monitoring'] = $campaign_monitoring;
        $data['project'] = mProject::where('project_id', $id)->with('client')->first();
        $data['from'] = $from;
        $data['chat'] = $chat;
        $data['count'] = $count;

        return view('project/projectList/projectBodyDetail', $data);
    }

    function insert_chat(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $rules = [
            'chat' => 'required',
        ];

        $attributes = [
            'chat' => 'Chat',
        ];

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $chat = $request->input('chat');
        $nama_file = $request->input('file');
        $tipe_file = $request->input('tipe_file');
//        dd($my_file);
        $project_id = $request->input('project_id');
        $project_id = Main::decrypt($project_id);
        $project = mProject::find($project_id);
        $kirim = [];
        $client_name = '';
        $agency_name = '';

        DB::beginTransaction();
        try {
            $data = [
                'user_id' => $user->user_id,
                'chat' => $chat,
                'project_id' => $project_id,
                'agency_id' => $project->agency_id,
                'client_id' => $project->client_id,
                'dari' => $from[0],
                'file' => null,
                'nama_file' => null,
            ];

            if ($request->hasFile('file')) {
                $path = 'private-label-system';
                $file = $request->file('file');
                $filename = 'chat-' . $project_id . '-' . now() . '-' . $file->getClientOriginalName();
                $replaced = str_replace(' ', '-', $filename);
                Storage::disk('sftp')->putFileAs($path, $file, $replaced);
                $data['nama_file'] = $file->getClientOriginalName();
                $data['file'] = $replaced;
                $data['tipe_file'] = $tipe_file;
            }

            if ($from[0] == 'agent_id') {
                $data['agent_id'] = $from[1];

                $agency = mAgency::find($project->agency_id);
                $client = mClient::find($project->client_id);

                $kirim = [
                    0 => $agency->agency_email,
                    1 => $client->client_email
                ];

                $agency_name = $agency->agency_name;
                $client_name = $client->client_name;


//                $client = mUser::where('from', 'client_id|' . $project->client_id)->value('user_name');
//                $agency = mUser::where('from', 'agency_id|' . $project->agency_id)->value('user_name');

//                if($agency){
//                    $kirim[0] = $agency->user_email;
//                }
//                if($client){
//                    $kirim[1] = $agency->user_email;
//                }


            }
//            elseif ($from[0] == 'agency_id'){
//                $kirim = [
//                    0 => mUser::where('from', 'client_id|' . $project->client_id)->value('user_email'),
//                ];
//                $client = mUser::where('from', 'client_id|' . $project->client_id)->value('user_name');
//            }elseif ($from[0] == 'client_id'){
//                $kirim = [
//                    0 => mUser::where('from', 'agency_id|' . $project->agency_id)->value('user_email'),
//                ];
//                $agency = mUser::where('from', 'agency_id|' . $project->agency_id)->value('user_name');
//            }

            $dari = mUser::where('from', $user->from)->value('user_name');


//            $kirim = [
//                0 => 'iwayanwidiastika@gmail.com',
//                1 => 'paramarthadwi@gmail.com'
//            ];

            $data_email = [
                'project' => $project,
                'comment' => $chat,
                'client' => $client_name,
                'agency' => $agency_name,
                'dari' => $dari,
                'nama_file' => $data['nama_file'],
                'file' => $data['file'],
            ];

            mProjectChat::create($data);
//            if ($from[0] == 'agent_id') {
//                Mail::to($kirim)->send(new sendMail($data_email));
//            }
            DB::commit();
        } catch (\Exception $exception) {
            if ($exception->getMessage() == "Expected response code 354 but got code \"554\", with message \"554 5.5.1 Error: no valid recipients\r\n\"") {
                return response([
                    'message' => 'Message could not be sent, please check again whether the Agency or Client email is correct'
                ], 422);
            } else {
                throw $exception;
            }

//            return response([
//                'message' => $exception->getMessage()
//            ], 422);

            DB::rollBack();
        }

//        return redirect(route('underConstruction'));
    }

    function insert_sub(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);

        if ($from[0] != 'agent_id') {
            return redirect(route('cannotPage'));
        }

        $sub_project_id_arr = $request->input('sub_project_id');
        $status_arr = $request->input('status');
        $assign_to_arr = $request->input('assign_to');

        $sub_project_id_campaign_arr = $request->input('sub_project_id_campaign');
        $status_campaign_arr = $request->input('status_campaign');
        $assign_to_campaign_arr = $request->input('assign_to_campaign');

        DB::beginTransaction();
        try {
            foreach ($sub_project_id_arr as $key => $sub_project_id) {
                $status = $status_arr[$key];
                $assign_to = $assign_to_arr[$key];

                $data_update = [
                    'status' => $status,
                    'assign_to' => $assign_to,
                    'last_update' => $user->from
                ];

                mSubProject::where(['sub_project_id' => $sub_project_id])->update($data_update);
            }

            foreach ($sub_project_id_campaign_arr as $key => $sub_project_id) {
                $status = $status_campaign_arr[$key];
                $assign_to = $assign_to_campaign_arr[$key];

                $data_update = [
                    'status' => $status,
                    'assign_to' => $assign_to,
                    'last_update' => $user->from
                ];

                mSubProject::where(['sub_project_id' => $sub_project_id])->update($data_update);
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

//        return redirect(route('underConstruction'));
    }

    function chat($id)
    {
//        $id = Main::decrypt($id);
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $chat = mProjectChat::where('project_id', $id)->orderBy('created_at', 'DESC')->paginate(5);
        $chat_all = mProjectChat::where('project_id', $id)->orderBy('created_at', 'DESC')->get();
        Carbon::setLocale('en');
        foreach ($chat as $value) {
//            $value->waktu = Carbon::parse($value->created_at)->diffForHumans();
            $value->waktu = Main::format_date_name($value->created_at);
            $value->dari = mUser::where('user_id', $value->user_id)->value('user_name');
            if($value->user_id == $user->user_id){
                $value->dari = 'You';
            }
//            if ($from[0] == $value->dari) {
//                if ($from[0] == 'agent_id' && $from[1] != $value->agent_id) {
//                    $value->warna = 'bg-light-warning';
//                    $agent = DB::table('tb_user')->where('from', 'agent_id|' . $value->agent_id)->first();
//                    $value->dari = $agent->user_name;
//                    $value->foto = $agent->user_pict;
//                } else {
//                    $value->warna = 'bg-light-primary';
//                    $value->dari = 'You';
//                }
//            } else {
//
//                if ($value->dari == 'agency_id') {
//                    $value->warna = 'bg-light-success';
//                    $agency = DB::table('tb_user')->where('from', 'agency_id|' . $value->agency_id)->first();
//                    $value->dari = $agency->user_name;
//                    $value->foto = $agency->user_pict;
//                } elseif ($value->dari == 'client_id') {
//                    $value->warna = 'bg-light-info';
//                    $client = DB::table('tb_user')->where('from', 'client_id|' . $value->client_id)->first();
//                    $value->dari = $client->user_name;
//                    $value->foto = $client->user_pict;
//
//                } else {
//                    $value->warna = 'bg-light-warning';
//                    $agent = DB::table('tb_user')->where('from', 'agent_id|' . $value->agent_id)->first();
//                    $value->dari = $agent->user_name;
//                    $value->foto = $agent->user_pict;
//
//                }
//            }
        }

        foreach ($chat_all as $value) {
            $value->waktu = Carbon::parse($value->created_at)->diffForHumans();
            $value->waktu = Main::format_date_name($value->created_at);
            $value->dari = mUser::where('user_id', $value->user_id)->value('user_name');
            if($value->user_id == $user->id) {
                $value->dari = 'You';
            }
//            if ($from[0] == $value->dari) {
//                if ($from[0] == 'agent_id' && $from[1] != $value->agent_id) {
//                    $value->warna = 'bg-light-warning';
//                    $agent = DB::table('tb_user')->where('from', 'agent_id|' . $value->agent_id)->first();
//                    $value->dari = $agent->user_name;
//                    $value->foto = $agent->user_pict;
//                } else {
//                    $value->warna = 'bg-light-primary';
//                    $value->dari = 'You';
//                }
//            } else {
//                $value->warna = 'bg-light-success';
//                if ($value->dari == 'agency_id') {
//                    $agency = DB::table('tb_user')->where('from', 'agency_id|' . $value->agency_id)->first();
//                    $value->dari = $agency->user_name;
//                    $value->foto = $agency->user_pict;
//                } elseif ($value->dari == 'client_id') {
//                    $client = DB::table('tb_user')->where('from', 'client_id|' . $value->client_id)->first();
//                    $value->dari = $client->user_name;
//                    $value->foto = $client->user_pict;
//                } else {
//                    $value->warna = 'bg-light-warning';
//                    $agent = DB::table('tb_user')->where('from', 'agent_id|' . $value->agent_id)->first();
//                    $value->dari = $agent->user_name;
//                    $value->foto = $agent->user_pict;
//                }
//            }

        }
        $kirim = [
            'chat' => $chat,
            'chat_all' => $chat_all,
        ];
        return $kirim;
    }

    function tasks($id)
    {
        $id = Main::decrypt($id);
        $data['project'] = mProject::where('project_id', $id)->first();

//        $data['next_tasks'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Next Tasks')->get();
//        $data['count_next_tasks'] = $data['next_tasks']->count();
//        $data['completed'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Completed')->get();
//        $data['count_completed'] = $data['completed']->count();
        return view('project/projectList/modalBodyTasks', $data);
    }

    function insert_tasks(Request $request)
    {
        $rules = [
            'tasks_name.*' => 'required',
//            'tasks_status.*' => 'required',
            'start_date.*' => 'required',
            'end_date.*' => 'required',
        ];

        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attributes['tasks_name.' . $i] = 'Tasks Name row-' . $next;
//            $attributes['tasks_status.' . $i] = 'Task Status row-' . $next;
            $attributes['start_date.' . $i] = 'Start row-' . $next;
            $attributes['end_date.' . $i] = 'End row-' . $next;
        }

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

//        $rules_ada = [
//            'next_tasks_name.*' => 'required',
//            'next_tasks_status.*' => 'required',
//            'next_start_date.*' => 'required',
//            'next_end_date.*' => 'required',
//            'completed_tasks_name.*' => 'required',
//            'completed_tasks_status.*' => 'required',
//            'completed_start_date.*' => 'required',
//            'completed_end_date.*' => 'required',
//        ];

//        for ($i = 0; $i <= 200; $i++) {
//            $next = $i + 1;
//            $attributes_ada['next_tasks_name.' . $i] = 'Next Tasks Name row-' . $next;
//            $attributes_ada['next_tasks_status.' . $i] = 'Next Task Status row-' . $next;
//            $attributes_ada['next_start_date.' . $i] = 'Next Start Date row-' . $next;
//            $attributes_ada['next_end_date.' . $i] = 'Next End Date row-' . $next;
//            $attributes_ada['completed_tasks_name.' . $i] = 'Completed Tasks Name row-' . $next;
//            $attributes_ada['completed_tasks_status.' . $i] = 'Completed Task Status row-' . $next;
//            $attributes_ada['completed_start_date.' . $i] = 'Completed Start Date row-' . $next;
//            $attributes_ada['completed_end_date.' . $i] = 'Completed End Date row-' . $next;
//        }
//
//        /**
//         * proses validasi
//         */
//        $validator = Validator::make($request->all(), $rules_ada, [], $attributes_ada);
//        if ($validator->fails()) {
//            return response([
//                'errors' => $validator->errors()
//            ], 422);
//        }
//
        $project_id = $request->input('project_id');
        $tasks_name_arr = $request->input('tasks_name');
        $tasks_desc_arr = $request->input('tasks_desc');
//        $tasks_status_arr = $request->input('tasks_status');
        $start_date_arr = $request->input('start_date');
        $end_date_arr = $request->input('end_date');
        $assign_to_arr = $request->input('assign_to');
//        $next_tasks_name_arr = $request->input('next_tasks_name');
//        $next_tasks_desc_arr = $request->input('next_tasks_desc');
//        $next_tasks_status_arr = $request->input('next_tasks_status');
//        $next_start_date_arr = $request->input('next_start_date');
//        $next_end_date_arr = $request->input('next_end_date');
//        $next_assign_to_arr = $request->input('next_assign_to');
//        $completed_tasks_name_arr = $request->input('completed_tasks_name');
//        $completed_tasks_desc_arr = $request->input('completed_tasks_desc');
//        $completed_tasks_status_arr = $request->input('completed_tasks_status');
//        $completed_start_date_arr = $request->input('completed_start_date');
//        $completed_end_date_arr = $request->input('completed_end_date');
//        $completed_assign_to_arr = $request->input('completed_assign_to');

        DB::beginTransaction();
        try {
//            $tasks = mTasks::where('project_id', $project_id)->count();
//            if ($tasks > 0) {
//                mTasks::where('project_id', $project_id)->delete();
//            }
            if (!empty($tasks_name_arr)) {
                foreach ($tasks_name_arr as $key => $tasks_name) {
                    $tasks_desc = $tasks_desc_arr[$key];
//                    $tasks_status = $tasks_status_arr[$key];
                    $start_date = Main::format_date_db($start_date_arr[$key]);
                    $end_date = Main::format_date_db($end_date_arr[$key]);
                    $assign_to = $assign_to_arr[$key];

                    $data_new_tasks = [
                        'project_id' => $project_id,
                        'tasks_name' => $tasks_name,
                        'tasks_desc' => $tasks_desc,
                        'tasks_status' => '',
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'assign_to' => $assign_to,
                    ];

                    mTasks::create($data_new_tasks);
                }
            }

//            if (!empty($next_tasks_name_arr)) {
//                foreach ($next_tasks_name_arr as $k => $next_tasks_name) {
//                    $next_tasks_desc = $next_tasks_desc_arr[$k];
//                    $next_tasks_status = $next_tasks_status_arr[$k];
//                    $next_start_date = Main::format_date_db($next_start_date_arr[$k]);
//                    $next_end_date = Main::format_date_db($next_end_date_arr[$k]);
//                    $next_assign_to = $next_assign_to_arr[$k];
//
//                    $data_next_tasks = [
//                        'project_id' => $project_id,
//                        'tasks_name' => $next_tasks_name,
//                        'tasks_desc' => $next_tasks_desc,
//                        'tasks_status' => $next_tasks_status,
//                        'start_date' => $next_start_date,
//                        'end_date' => $next_end_date,
//                        'assign_to' => $next_assign_to,
//                    ];
//
//                    mTasks::create($data_next_tasks);
//                }
//            }
//
//            if (!empty($completed_tasks_name_arr)) {
//                foreach ($completed_tasks_name_arr as $kunci => $completed_tasks_name) {
//                    $completed_tasks_desc = $completed_tasks_desc_arr[$kunci];
//                    $completed_tasks_status = $completed_tasks_status_arr[$kunci];
//                    $completed_start_date = Main::format_date_db($completed_start_date_arr[$kunci]);
//                    $completed_end_date = Main::format_date_db($completed_end_date_arr[$kunci]);
//                    $completed_assign_to = $completed_assign_to_arr[$kunci];
//
//                    $data_completed_tasks = [
//                        'project_id' => $project_id,
//                        'tasks_name' => $completed_tasks_name,
//                        'tasks_desc' => $completed_tasks_desc,
//                        'tasks_status' => $completed_tasks_status,
//                        'start_date' => $completed_start_date,
//                        'end_date' => $completed_end_date,
//                        'assign_to' => $completed_assign_to,
//                    ];
//
//                    mTasks::create($data_completed_tasks);
//                }
//            }
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function approve_tasks($id)
    {
        $id = Main::decrypt($id);
        mTasks::where('tasks_id', $id)->update(['tasks_status' => Input::get(0)]);
    }

    function insert_metric(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'clicks' => 'required',
            'impr' => 'required',
            'spend' => 'required',
        ];

        $attributes = [
            'clicks' => 'Clicks',
            'impr' => 'Impr',
            'spend' => 'Spend',
        ];

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $impr = Main::string_to_number($request->input('impr'));
        $spend = Main::string_to_number($request->input('spend'));
        $clicks = Main::string_to_number($request->input('clicks'));
        $all_rev = Main::string_to_number($request->input('all_rev'));
        $paid_rev = Main::string_to_number($request->input('paid_rev'));
        $roas = Main::string_to_number($request->input('roas'));
        $month = now();

        DB::beginTransaction();
        try {
            $data = [
                'project_id' => $id,
                'impr' => $impr,
                'spend' => $spend,
                'clicks' => $clicks,
                'all_rev' => $all_rev,
                'paid_rev' => $paid_rev,
                'roas' => $roas,
                'date' => now(),
            ];

            mMetric::create($data);
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function file($id)
    {
        $id = Main::decrypt($id);
        $data['project'] = mProject::where('project_id', $id)->first();

        $data['file'] = mFile::where('project_id', $id)->get();
        $data['count_file'] = $data['file']->count();
        return view('project/projectList/modalBodyFile', $data);
    }

    function file_insert(Request $request)
    {
        $rules = [
            'file_name.*' => 'required',
            'file_link.*' => 'required',
        ];

        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attributes['file_name.' . $i] = 'File Name row-' . $next;
            $attributes['file_link.' . $i] = 'File Link row-' . $next;
        }

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $rules_ada = [
            'file_name_ada.*' => 'required',
            'file_link_ada.*' => 'required',
        ];

        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attributes_ada['file_name_ada.' . $i] = 'Files Name that are already in row-' . $next;
            $attributes_ada['file_link_ada.' . $i] = 'Files Link that are already in row-' . $next;
        }

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules_ada, [], $attributes_ada);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $project_id = $request->input('project_id');
        $file_name_arr = $request->input('file_name');
        $file_link_arr = $request->input('file_link');
        $file_name_ada_arr = $request->input('file_name_ada');
        $file_link_ada_arr = $request->input('file_link_ada');

        DB::beginTransaction();
        try {
            $file = mFile::where('project_id', $project_id)->count();
            if ($file > 0) {
                mFile::where('project_id', $project_id)->delete();
            }
            if (!empty($file_name_arr)) {
                foreach ($file_name_arr as $key => $file_name) {
                    $file_link = $file_link_arr[$key];

                    $data_new_file = [
                        'project_id' => $project_id,
                        'file_name' => $file_name,
                        'file_link' => $file_link,
                    ];

                    mFile::create($data_new_file);
                }
            }

            if (!empty($file_name_ada_arr)) {
                foreach ($file_name_ada_arr as $k => $file_name_ada) {
                    $file_link_ada = $file_link_ada_arr[$k];

                    $data_file = [
                        'project_id' => $project_id,
                        'file_name' => $file_name_ada,
                        'file_link' => $file_link_ada,
                    ];

                    mFile::create($data_file);
                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function performance_dashboard($id)
    {
        $id = Main::decrypt($id);
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $data['project'] = mProject::where('project_id', $id)->first();

        $data['next_tasks'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Next Tasks')->get();
        $data['count_next_tasks'] = $data['next_tasks']->count();
        $data['completed'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Completed')->get();
        $data['count_completed'] = $data['completed']->count();
        $data['project'] = mProject::where('project_id', $id)->first();
        return view('project/projectList/performanceDashboard', $data);
    }

    function project_result($id)
    {
        $id = Main::decrypt($id);
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $data['project'] = mProject::where('project_id', $id)->first();

        $data['next_tasks'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Next Tasks')->get();
        $data['count_next_tasks'] = $data['next_tasks']->count();
        $data['completed'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Completed')->get();
        $data['count_completed'] = $data['completed']->count();
        $data['project'] = mProject::where('project_id', $id)->first();
        return view('project/projectList/resultView', $data);
    }

    function index_single($id)
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb, 'project_2');
        $user = Session::get('user');
        $tabel = explode('|', $user->from);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = [
            0 => "akses_menu",
            1 => "list",
            2 => "create",
            3 => "tasks",
            4 => "metric",
            5 => "file",
            6 => "edit",
            7 => "detail",
            8 => "delete",
        ];
//        $action = array();
//        foreach ($data['menuAction'] as $key => $value) {
//            if ($value) {
//                array_push($action, $key);
//            }
//        }
        $data['params'] = [
            'client_id' => $id
        ];
        $data['from'] = $tabel[0];
        $data['id'] = $tabel[1];
        $data['services'] = mService::all();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['agency'] = mAgency::orderBy('tb_agency.agency_name', 'ASC')->get();
        $data['project_type'] = mProjectType::orderBy('tb_project_type.project_type_name', 'ASC')->get();
        $data['project_status'] = mProjectStatus::orderBy('tb_project_status.project_status_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);


        return view('project/projectList/projectSingle', $data);
    }

    function list_single(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $project_status_id = $request->input('project_id');
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $project = new mProject();
        $result['iTotalRecords'] = $project->count_single_all($from[0], $from[1], $project_status_id);
        $result['iTotalDisplayRecords'] = $project->count_single_filter($query, $this->view, $from[0], $from[1], $project_status_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $project->list_single($start, $length, $query, $this->view, $from[0], $from[1], $project_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->route['delete'] = route('projectListDelete', ['id' => Main::encrypt($value->project_id)]);
            $value->route['edit'] = route('projectListUpdate', ['id' => Main::encrypt($value->project_id)]);
            $value->route['detail'] = route('projectListDetail', ['id' => Main::encrypt($value->project_id)]);
            $value->ignore['detail'] = true;
            $value->direct['detail'] = route('projectListDetail', ['id' => Main::encrypt($value->project_id)]);
            $value->route['tasks'] = route('projectListTasks', ['id' => Main::encrypt($value->project_id)]);
            $value->route['view'] = route('underconstructionPage');
//            $value->route['view'] = route('projectView', ['id' => Main::encrypt($value->project_id)]);
//            $value->direct['view'] = route('projectView', ['id' => Main::encrypt($value->project_id)]);
            $value->route['metric'] = route('projectListInsertMetric', ['id' => Main::encrypt($value->project_id)]);
            $value->route['file'] = route('projectListFile', ['id' => Main::encrypt($value->project_id)]);
//            $value->ignore['detail'] = true;
            $value->project_cost = Main::format_number($value->project_cost);
            $value->project_order_number_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->project_order_number . '</a>';
            $value->channel_name_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->channel_name . '</a>';
            $value->client_name_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->client_name . '</a>';
            $value->project_order_title_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . $value->project_order_title . '</a>';
            $value->project_order_date_label = '<a style="color : #000" href="' . $value->route['detail'] . '">' . Main::format_date($value->project_order_date) . '</a>';
            $value->project_status_name_label = '<span class="label font-weight-bold label-lg label-inline" style="color: ' . $value->project_status_text_color . '; background-color: ' . $value->project_status_bc_color . ';">' . $value->project_status_name . '</span>';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
