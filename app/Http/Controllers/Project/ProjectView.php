<?php

namespace app\Http\Controllers\Project;

use app\Helpers\Main;
use app\Models\mAgency;
use app\Models\mCompany;
use app\Models\mProjectStatus;
use app\Models\mProjectType;
use app\Models\mService;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ProjectView extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['project'],
                'route' => ''
            ],
            [
                'label' => $cons['project_1'],
                'route' => ''
            ],
            [
            'label' => 'Project View',
            'route' => ''
        ]
        ];
    }

    function index($id)
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb, 'project_list');
        $user = Session::get('user');
        $data['services'] = mService::all();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['company'] = mCompany::orderBy('tb_company.company_name', 'ASC')->get();
        $data['agency'] = mAgency::orderBy('tb_agency.agency_name', 'ASC')->get();
        $data['project_type'] = mProjectType::orderBy('tb_project_type.project_type_name', 'ASC')->get();
        $data['project_status'] = mProjectStatus::orderBy('tb_project_status.project_status_name', 'ASC')->get();



        return view('project/projectList/projectView/projectView', $data);

    }
}
