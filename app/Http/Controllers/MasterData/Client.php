<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mAgency;
use app\Models\mChat;
use app\Models\mClient;
use app\Models\mClientStatus;
use app\Models\mClientType;
use app\Models\mProject;
use app\Models\mProjectChat;
use app\Models\mUser;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Client extends Controller
{
    private $view = [
        'client_name' => ["search_field" => "tb_client.client_name"],
        'client_contact' => ["search_field" => "tb_client.client_contact"],
        'client_email' => ["search_field" => "tb_client.client_email"],
        'client_type_name' => ["search_field" => "tb_client_type.client_type_name"],
        'client_status_name' => ["search_field" => "tb_client_status.client_status_name"],
        'client_desc' => ["search_field" => "tb_client.client_desc"],
        'client_data' => ["search_field" => "tb_client.client_data"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
//        dd($data);
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['client_status'] = mClientStatus::orderBy('client_status_name')->get();
        $data['agency'] = mAgency::orderBy('agency_name')->get();
        $data['client_type'] = mClientType::orderBy('client_type_name')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/client/clientList', $data);
    }

    function insert(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $rules = [
            'client_name' => 'required',
            'client_contact' => 'required',
            'client_email' => 'required',
            'client_type_id' => 'required',
            'client_status_id' => 'required',
            'client_desc' => 'required',
            'client_data' => 'required',
        ];


        $attributes = [
            'client_name' => 'Client Name',
            'client_contact' => 'Client Contact',
            'client_email' => 'Client Email',
            'client_type_id' => 'Client Type',
            'client_status_id' => 'Client Status',
            'client_desc' => 'Client Description',
            'client_data' => 'Client Data',
        ];

        if ($from[0] == 'admin_id' || $from[0] == 'agent_id') {
            $rules['agency_id'] = 'required';
            $attributes['agency_id'] = 'Agency';
        }

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_id = $request->input('agency_id');

        if ($from[0] == 'agency_id') {
            $agency_id = $from[1];
        }
        $client_name = $request->input('client_name');
        $client_contact = $request->input('client_contact');
        $client_email = $request->input('client_email');
        $client_type_id = $request->input('client_type_id');
        $client_desc = $request->input('client_desc');
        $client_data = $request->input('client_data');
        $client_status_id = $request->input('client_status_id');
        $data = [
            'agency_id' => $agency_id,
            'client_name' => $client_name,
            'client_contact' => $client_contact,
            'client_email' => $client_email,
            'client_type_id' => $client_type_id,
            'client_desc' => $client_desc,
            'client_data' => $client_data,
            'client_status_id' => $client_status_id,
            'user_status' => 'kosong'
        ];
        $client = mClient::create($data);

        $chat_data = [
            'agency_id' => $agency_id,
            'client_id' => $client->client_id,
        ];

        mChat::create($chat_data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $client = mClient::find($id);
        $project = mProject::where('client_id', $id)->first();

        if ($client->user_status == 'ada') {
            return response([
                'message' => '<strong>This client still has a user account, please delete the user account before deleting this client data</strong>'
            ], 422);
        }else{
            mProjectChat::where('project_id', $project->project_id)->delete();
            mProject::where('client_id', $id)->delete();
            mClient::where('client_id', $id)->delete();
        }
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $rules = [
            'client_name' => 'required',
            'client_contact' => 'required',
            'client_email' => 'required',
            'client_type_id' => 'required',
            'client_status_id' => 'required',
            'client_desc' => 'required',
            'client_data' => 'required',
        ];

        $attributes = [
            'client_name' => 'Client Name',
            'client_contact' => 'Client Contact',
            'client_email' => 'Client Email',
            'client_type_id' => 'Client Type',
            'client_status_id' => 'Client Status',
            'client_desc' => 'Client Description',
            'client_data' => 'Client Data',
        ];

        if ($from[0] == 'admin_id' || $from[0] == 'agent_id') {
            $rules['agency_id'] = 'required';
            $attributes['agency_id'] = 'Agency';
        }
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_id = $request->input('agency_id');

        if ($from[0] == 'agency_id') {
            $agency_id = $from[1];
        }
        $client_name = $request->input('client_name');
        $client_contact = $request->input('client_contact');
        $client_email = $request->input('client_email');
        $client_type_id = $request->input('client_type_id');
        $client_desc = $request->input('client_desc');
        $client_data = $request->input('client_data');
        $client_status_id = $request->input('client_status_id');
        $data = [
            'agency_id' => $agency_id,
            'client_name' => $client_name,
            'client_contact' => $client_contact,
            'client_email' => $client_email,
            'client_type_id' => $client_type_id,
            'client_desc' => $client_desc,
            'client_data' => $client_data,
            'client_status_id' => $client_status_id,
        ];
        mClient::where(['client_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $client = new mClient();
        $result['iTotalRecords'] = $client->count_all();
        $result['iTotalDisplayRecords'] = $client->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $client->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('clientDelete', ['id' => Main::encrypt($value->client_id)]);
            $value->message['delete'] = 'all data related to this client will be deleted, are you sure you want to delete?';
            $value->route['edit'] = route('clientUpdate', ['id' => Main::encrypt($value->client_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
