<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mProjectStatus;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class ProjectStatus extends Controller
{
    private $view = [
        'project_status_name_label' => ["search_field" => "tb_project_status.project_status_name"],
//        'project_status_data' => ["search_field" => "tb_project_status.project_status_data"],
        'project_status_desc' => ["search_field" => "tb_project_status.project_status_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_12'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/projectStatus/projectStatusList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'project_status_name' => 'required',
            'project_status_text_color' => 'required',
            'project_status_bc_color' => 'required',
        ];

        $attributes = [
            'project_status_name' => 'Project Status Name',
            'project_status_text_color' => 'Project Status Text Color',
            'project_status_bc_color' => 'Project Status Background Color',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $project_status_name = $request->input('project_status_name');
        $project_status_text_color = $request->input('project_status_text_color');
        $project_status_bc_color = $request->input('project_status_bc_color');
        $project_status_data = $request->input('project_status_data');
        $project_status_desc = $request->input('project_status_desc');
        $data = [
            'project_status_name' => $project_status_name,
            'project_status_text_color' => $project_status_text_color,
            'project_status_bc_color' => $project_status_bc_color,
            'project_status_data' => $project_status_data,
            'project_status_desc' => $project_status_desc,
        ];
        mProjectStatus::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mProjectStatus::where('project_status_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'project_status_name' => 'required',
            'project_status_text_color' => 'required',
            'project_status_bc_color' => 'required',
        ];

        $attributes = [
            'project_status_name' => 'Project Status Name',
            'project_status_text_color' => 'Project Status Text Color',
            'project_status_bc_color' => 'Project Status Background Color',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $project_status_name = $request->input('project_status_name');
        $project_status_text_color = $request->input('project_status_text_color');
        $project_status_bc_color = $request->input('project_status_bc_color');
        $project_status_data = $request->input('project_status_data');
        $project_status_desc = $request->input('project_status_desc');
        $data = [
            'project_status_name' => $project_status_name,
            'project_status_text_color' => $project_status_text_color,
            'project_status_bc_color' => $project_status_bc_color,
            'project_status_data' => $project_status_data,
            'project_status_desc' => $project_status_desc,
        ];
        mProjectStatus::where(['project_status_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $project_status = new mProjectStatus();
        $result['iTotalRecords'] = $project_status->count_all();
        $result['iTotalDisplayRecords'] = $project_status->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $project_status->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('projectStatusDelete', ['id' => Main::encrypt($value->project_status_id)]);
            $value->route['edit'] = route('projectStatusUpdate', ['id' => Main::encrypt($value->project_status_id)]);
            $value->project_status_name_label = '<span class="label font-weight-bold label-lg label-inline" style="color: '.$value->project_status_text_color.'; background-color: '.$value->project_status_bc_color.';">'.$value->project_status_name.'</span>';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
