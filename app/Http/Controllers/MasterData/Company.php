<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mClient;
use app\Models\mCompany;
use app\Models\mCompanyType;
use app\Models\mUser;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Company extends Controller
{
    private $view = [
        'company_name' => ["search_field" => "tb_company.company_name"],
        'company_email' => ["search_field" => "tb_company.company_email"],
        'company_phone' => ["search_field" => "tb_company.company_phone"],
        'client_name' => ["search_field" => "tb_client.client_name"],
        'company_type_name' => ["search_field" => "tb_company_type.company_type_name"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_10'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $result = explode('|', $user->from);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['client'] = mClient::orderBy('tb_client.client_name', 'ASC')->get();
        $data['from'] = $result[0];
        $data['company_type'] = mCompanyType::orderBy('tb_company_type.company_type_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);

        return view('masterData/company/companyList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'company_name' => 'required',
            'client_id' => 'required',
            'company_type_id' => 'required',
            'company_email' => 'required',
            'company_phone' => 'required',
            'company_address' => 'required',
        ];

        $attributes = [
            'company_name' => 'Company Name',
            'client_id' => 'Client',
            'company_type_id' => 'Company Type',
            'company_email' => 'Company Email',
            'company_phone' => 'Company Phone',
            'company_address' => 'Company Address',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $company_name = $request->input('company_name');
        $company_email = Main::string_to_number($request->input('company_email'));
        $company_phone = Main::string_to_number($request->input('company_phone'));
        $company_address = Main::string_to_number($request->input('company_address'));
        $company_overview = Main::string_to_number($request->input('company_overview'));
        $company_data = $request->input('company_data');
        $client_id = $request->input('client_id');
        $company_type_id = $request->input('company_type_id');

        $data = [
            'company_name' => $company_name,
            'company_email' => $company_email,
            'company_address' => $company_address,
            'company_phone' => $company_phone,
            'company_overview' => $company_overview,
            'company_data' => $company_data,
            'client_id' => $client_id,
            'company_type_id' => $company_type_id,
        ];
        mCompany::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mCompany::where('company_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'company_name' => 'required',
            'client_id' => 'required',
            'company_type_id' => 'required',
            'company_email' => 'required',
            'company_phone' => 'required',
            'company_address' => 'required',
        ];

        $attributes = [
            'company_name' => 'Company Name',
            'client_id' => 'Client',
            'company_type_id' => 'Company Type',
            'company_email' => 'Company Email',
            'company_phone' => 'Company Phone',
            'company_address' => 'Company Address',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $company_name = $request->input('company_name');
        $company_email = Main::string_to_number($request->input('company_email'));
        $company_phone = Main::string_to_number($request->input('company_phone'));
        $company_address = Main::string_to_number($request->input('company_address'));
        $company_overview = Main::string_to_number($request->input('company_overview'));
        $company_data = $request->input('company_data');
        $client_id = $request->input('client_id');
        $company_type_id = $request->input('company_type_id');

        $data = [
            'company_name' => $company_name,
            'company_email' => $company_email,
            'company_address' => $company_address,
            'company_phone' => $company_phone,
            'company_overview' => $company_overview,
            'company_data' => $company_data,
            'client_id' => $client_id,
            'company_type_id' => $company_type_id,
        ];
        mCompany::where(['company_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $user = Session::get('user');
        $result = explode('|', $user->from);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $company = new mCompany();
        $result['iTotalRecords'] = $company->count_all($result[0], $result[1]);
        $result['iTotalDisplayRecords'] = $company->count_filter($query, $this->view, $result[0], $result[1]);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $company->list($start, $length, $query, $this->view, $result[0], $result[1]);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('companyDelete', ['id' => Main::encrypt($value->company_id)]);
            $value->route['edit'] = route('companyUpdate', ['id' => Main::encrypt($value->company_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
