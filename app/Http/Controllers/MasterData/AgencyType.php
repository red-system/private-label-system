<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mAgencyType;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class AgencyType extends Controller
{
    private $view = [
        'agency_type_name' => ["search_field" => "tb_agency_type.agency_type_name"],
        'agency_type_desc' => ["search_field" => "tb_agency_type.agency_type_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_13'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/agencyType/agencyTypeList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'agency_type_name' => 'required',
        ];

        $attributes = [
            'agency_type_name' => 'Agency Type Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_type_name = $request->input('agency_type_name');
        $agency_type_desc = $request->input('agency_type_desc');
        $data = [
            'agency_type_name' => $agency_type_name,
            'agency_type_desc' => $agency_type_desc,
        ];
        mAgencyType::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mAgencyType::where('agency_type_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'agency_type_name' => 'required',
        ];

        $attributes = [
            'agency_type_name' => 'Agency Type Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_type_name = $request->input('agency_type_name');
        $agency_type_desc = $request->input('agency_type_desc');
        $data = [
            'agency_type_name' => $agency_type_name,
            'agency_type_desc' => $agency_type_desc,
        ];
        mAgencyType::where(['agency_type_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $agency_type = new mAgencyType();
        $result['iTotalRecords'] = $agency_type->count_all();
        $result['iTotalDisplayRecords'] = $agency_type->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $agency_type->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('agencyTypeDelete', ['id' => Main::encrypt($value->agency_type_id)]);
            $value->route['edit'] = route('agencyTypeUpdate', ['id' => Main::encrypt($value->agency_type_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
