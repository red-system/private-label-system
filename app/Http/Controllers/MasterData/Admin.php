<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mAdmin;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class Admin extends Controller
{
    private $view = [
        'admin_name' => ["search_field" => "tb_admin.admin_name"],
        'admin_data' => ["search_field" => "tb_admin.admin_data"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/admin/adminList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'admin_name' => 'required',
            'admin_data' => 'required',
        ];

        $attributes = [
            'admin_name' => 'Admin Name',
            'admin_data' => 'Admin Data',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $admin_name = $request->input('admin_name');
        $admin_data = $request->input('admin_data');
        $data = [
            'admin_name' => $admin_name,
            'admin_data' => $admin_data,
            'user_status' => 'kosong'
        ];
        mAdmin::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $admin = mAdmin::find($id);
        if ($admin->user_status == 'ada') {
            return response([
                'message' => '<strong>This admin still has a user account, please delete the user account before deleting this admin data</strong>'
            ], 422);
        }else{
            mAdmin::where('admin_id', $id)->delete();
        }
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'admin_name' => 'required',
            'admin_data' => 'required',
        ];

        $attributes = [
            'admin_name' => 'Admin Name',
            'admin_data' => 'Admin Data',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $admin_name = $request->input('admin_name');
        $admin_data = $request->input('admin_data');
        $data = [
            'admin_name' => $admin_name,
            'admin_data' => $admin_data,
        ];
        mAdmin::where(['admin_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $admin = new mAdmin();
        $result['iTotalRecords'] = $admin->count_all();
        $result['iTotalDisplayRecords'] = $admin->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $admin->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('adminDelete', ['id' => Main::encrypt($value->admin_id)]);
            $value->route['edit'] = route('adminUpdate', ['id' => Main::encrypt($value->admin_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
