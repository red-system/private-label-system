<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Imports\importAgency;
use app\Imports\importExcel;
use app\Imports\importNew;
use app\Models\mAgency;
use app\Models\mAgencyType;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class Agency extends Controller
{
    private $view = [
        'agency_name' => ["search_field" => "tb_agency.agency_name"],
        'agency_email' => ["search_field" => "tb_agency.agency_email"],
        'agency_data' => ["search_field" => "tb_agency.agency_data"],
        'agency_desc' => ["search_field" => "tb_agency.agency_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['agency_type'] = mAgencyType::orderBy('agency_type_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/agency/agencyList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'agency_name' => 'required',
            'agency_desc' => 'required',
            'agency_data' => 'required',
            'agency_type_id' => 'required',
            'agency_email' => 'bail|required|email'
        ];

        $attributes = [
            'agency_name' => 'Agency Name',
            'agency_desc' => 'Agency Description',
            'agency_data' => 'Agency Data',
            'agency_type_id' => 'Agency Type',
            'agency_email' => 'Email Address',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_email = $request->input('agency_email');
        $agency_name = $request->input('agency_name');
        $agency_desc = $request->input('agency_desc');
        $agency_data = $request->input('agency_data');
        $agency_type_id = $request->input('agency_type_id');
        $data = [
            'agency_email' => $agency_email,
            'agency_name' => $agency_name,
            'agency_desc' => $agency_desc,
            'agency_data' => $agency_data,
            'agency_type_id' => $agency_type_id,
            'user_status' => 'kosong'
        ];
        mAgency::create($data);
    }

    function import(Request $request)
    {
        $rules = [
            'file' => 'required|mimes:csv,xls,xlsx'
        ];

        $attributes = [
            'file' => 'File Excel',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $file = $request->file('file');

        Excel::import(new importNew, $file);

//        mAgency::create($data);
//        return $excel;
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $agency = mAgency::find($id);
        if ($agency->user_status == 'ada') {
            return response([
                'message' => '<strong>This agency still has a user account, please delete the user account before deleting this agency data</strong>'
            ], 422);
        }else{
            mAgency::where('agency_id', $id)->delete();
        }
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'agency_name' => 'required',
            'agency_desc' => 'required',
            'agency_data' => 'required',
            'agency_type_id' => 'required',
            'agency_email' => 'bail|required|email'
        ];

        $attributes = [
            'agency_name' => 'Agency Name',
            'agency_desc' => 'Agency Description',
            'agency_data' => 'Agency Data',
            'agency_type_id' => 'Agency Type',
            'agency_email' => 'Email Address',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_email = $request->input('agency_email');
        $agency_name = $request->input('agency_name');
        $agency_desc = $request->input('agency_desc');
        $agency_data = $request->input('agency_data');
        $agency_type_id = $request->input('agency_type_id');

        $data = [
            'agency_email' => $agency_email,
            'agency_name' => $agency_name,
            'agency_desc' => $agency_desc,
            'agency_data' => $agency_data,
            'agency_type_id' => $agency_type_id,
        ];
        mAgency::where(['agency_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $agency = new mAgency();
        $result['iTotalRecords'] = $agency->count_all();
        $result['iTotalDisplayRecords'] = $agency->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $agency->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('agencyDelete', ['id' => Main::encrypt($value->agency_id)]);
            $value->route['edit'] = route('agencyUpdate', ['id' => Main::encrypt($value->agency_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
