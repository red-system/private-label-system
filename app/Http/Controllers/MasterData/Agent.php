<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mAgent;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class Agent extends Controller
{
    private $view = [
        'agent_name' => ["search_field" => "tb_agent.agent_name"],
        'agent_data' => ["search_field" => "tb_agent.agent_data"],
        'agent_desc' => ["search_field" => "tb_agent.agent_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_15'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/agent/agentList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'agent_name' => 'required',
            'agent_desc' => 'required',
            'agent_data' => 'required',
        ];

        $attributes = [
            'agent_name' => 'Agent Name',
            'agent_desc' => 'Agent Description',
            'agent_data' => 'Agent Data',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agent_name = $request->input('agent_name');
        $agent_desc = $request->input('agent_desc');
        $agent_data = $request->input('agent_data');
        $data = [
            'agent_name' => $agent_name,
            'agent_desc' => $agent_desc,
            'agent_data' => $agent_data,
            'user_status' => 'kosong'
        ];
        mAgent::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $agent = mAgent::find($id);
        if ($agent->user_status == 'ada') {
            return response([
                'message' => '<strong>This agent still has a user account, please delete the user account before deleting this agent data</strong>'
            ], 422);
        }else{
            mAgent::where('agent_id', $id)->delete();
        }
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'agent_name' => 'required',
            'agent_desc' => 'required',
            'agent_data' => 'required',
        ];

        $attributes = [
            'agent_name' => 'Agent Name',
            'agent_desc' => 'Agent Description',
            'agent_data' => 'Agent Data',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agent_name = $request->input('agent_name');
        $agent_desc = $request->input('agent_desc');
        $agent_data = $request->input('agent_data');

        $data = [
            'agent_name' => $agent_name,
            'agent_desc' => $agent_desc,
            'agent_data' => $agent_data,
        ];
        mAgent::where(['agent_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $agent = new mAgent();
        $result['iTotalRecords'] = $agent->count_all();
        $result['iTotalDisplayRecords'] = $agent->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $agent->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('agentDelete', ['id' => Main::encrypt($value->agent_id)]);
            $value->route['edit'] = route('agentUpdate', ['id' => Main::encrypt($value->agent_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
