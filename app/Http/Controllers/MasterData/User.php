<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mAdmin;
use app\Models\mAgency;
use app\Models\mAgent;
use app\Models\mClient;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Hash;
use app\Rules\UsernameChecker;
use app\Rules\UsernameCheckerUpdate;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use app\Models\mUserRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class User extends Controller
{
    private $breadcrumb;
    private $view = [
        'user_name' => ["search_field" => "tb_user.user_name"],
//        'user_email' => ["search_field" => "tb_user.user_email"],
        'user_data' => ["search_field" => "tb_user.user_data"],
        'user_desc' => ["search_field" => "tb_user.user_desc"],
        'user_role_name' => ["search_field" => "tb_user_role.user_role_name"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
//        $admin = mAdmin::where('deleted_at', '=', null)->orderBy('admin_name')->get();
//        $agency = mAgency::where('deleted_at', '=', null)->orderBy('agency_name')->get();
//        $client = mClient::where('deleted_at', '=', null)->orderBy('client_name')->get();
//
//        $admins = [];
//        foreach ($admin as $key => $a) {
//            $admins[$key] = [
//                'value' => 'admin_id' . '|' . $a->admin_id,
//                'label' => $a->admin_name
//            ];
//        }
//
//        $agencys = [];
//        foreach ($agency as $k => $ag) {
//            $agencys[$k] = [
//                'value' => 'agency_id' . '|' . $ag->agency_id,
//                'label' => $ag->agency_name
//            ];
//        }
//
//        $clients = [];
//        foreach ($client as $kunci => $c) {
//            $clients[$kunci] = [
//                'value' => 'client_id' . '|' . $c->client_id,
//                'label' => $c->client_name
//            ];
//        }
//        $from = [
//            'admin' => $admins,
//            'agency' => $agencys,
//            'client' => $clients,
//        ];
//        dd($from);
//        $data['from'] = $from;
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/user/userList', $data);
    }
    function create()
    {
        $data = Main::data($this->breadcrumb);
        $admin = mAdmin::where('deleted_at', '=', null)->orderBy('admin_name')->get();
        $agency = mAgency::where('deleted_at', '=', null)->orderBy('agency_name')->get();
        $client = mClient::where('deleted_at', '=', null)->orderBy('client_name')->get();
        $agent = mAgent::where('deleted_at', '=', null)->orderBy('agent_name')->get();

        $admins = [];
        foreach ($admin as $key => $a) {
            $admins[$key] = [
                'value' => 'admin_id' . '|' . $a->admin_id,
                'label' => $a->admin_name
            ];
        }

        $agents = [];
        foreach ($agent as $keys => $agt) {
            $agents[$keys] = [
                'value' => 'agent_id' . '|' . $agt->agent_id,
                'label' => $agt->agent_name
            ];
        }

        $agencys = [];
        foreach ($agency as $k => $ag) {
            $agencys[$k] = [
                'value' => 'agency_id' . '|' . $ag->agency_id,
                'label' => $ag->agency_name
            ];
        }

        $clients = [];
        foreach ($client as $kunci => $c) {
            $clients[$kunci] = [
                'value' => 'client_id' . '|' . $c->client_id,
                'label' => $c->client_name
            ];
        }
        $from = [
            'admin' => $admins,
            'agency' => $agencys,
            'client' => $clients,
            'agent' => $agents,
        ];
        $data['from'] = $from;
        return view('masterData/user/userCreate', $data);
    }

    function edit($id)
    {
        $id = Main::decrypt($id);
        $data = Main::data($this->breadcrumb);
        $admin = mAdmin::where('deleted_at', '=', null)->orderBy('admin_name')->get();
        $agency = mAgency::where('deleted_at', '=', null)->orderBy('agency_name')->get();
        $client = mClient::where('deleted_at', '=', null)->orderBy('client_name')->get();
        $agent = mAgent::where('deleted_at', '=', null)->orderBy('agent_name')->get();

        $admins = [];
        foreach ($admin as $key => $a) {
            $admins[$key] = [
                'value' => 'admin_id' . '|' . $a->admin_id,
                'label' => $a->admin_name
            ];
        }

        $agents = [];
        foreach ($agent as $keys => $agt) {
            $agents[$keys] = [
                'value' => 'agent_id' . '|' . $agt->agent_id,
                'label' => $agt->agent_name
            ];
        }

        $agencys = [];
        foreach ($agency as $k => $ag) {
            $agencys[$k] = [
                'value' => 'agency_id' . '|' . $ag->agency_id,
                'label' => $ag->agency_name
            ];
        }

        $clients = [];
        foreach ($client as $kunci => $c) {
            $clients[$kunci] = [
                'value' => 'client_id' . '|' . $c->client_id,
                'label' => $c->client_name
            ];
        }
        $from = [
            'admin' => $admins,
            'agency' => $agencys,
            'client' => $clients,
            'agent' => $agents,
        ];
//        dd($from);
        $data['from'] = $from;
        $data['user_edit'] = mUser::find($id);
        return view('masterData/user/userEdit', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'from' => 'required',
            'user_name' => ['required', new UsernameChecker],
            'user_password' => 'bail',
            'user_password_confirm' => 'bail|same:user_password',
//            'user_email' => 'bail|required|email'
        ]);


        $from = $request->input('from');
        $user_name = $request->input('user_name');
//        $user_email = $request->input('user_email');
        $user_password = $request->input('user_password');
        $user_data = $request->input('user_data');
        $user_desc = $request->input('user_desc');
        $result = explode('|', $from);

        $data = [
            'user_name' => $user_name,
//            'user_email' => $user_email,
            'user_password' => $user_password,
            'user_data' => $user_data,
            'user_desc' => $user_desc,
            'from' => $from
        ];
//        dd($result);
        if ($result[0] == 'admin_id') {
            $cek = mAdmin::find($result[1]);
            if ($cek->user_status == 'ada') {
                return response([
                    'message' => '<strong>This Admin already has a user account</strong>'
                ], 422);
            } else {
                mAdmin::where('admin_id', $result[1])->update(['user_status' => 'ada']);
                $data['user_role_id'] = 1;
            }
        } elseif ($result[0] == 'agency_id') {
//            $cek = mAgency::find($result[1]);
            $cek = mUser::where('from', '=', $from)->count();
            if ($cek >= 5) {
                return response([
                    'message' => '<strong>This Agency already has a user account</strong>'
                ], 422);
            } else {
                mAgency::where('agency_id', $result[1])->update(['user_status' => 'ada']);
                $data['user_role_id'] = 2;
                $data['count'] = $cek+1;
            }
        } elseif ($result[0] == 'client_id') {
            $cek = mClient::find($result[1]);
            if ($cek->user_status == 'ada') {
                return response([
                    'message' => '<strong>This Client already has a user account</strong>'
                ], 422);
            } else {
                mClient::where('client_id', $result[1])->update(['user_status' => 'ada']);
                $data['user_role_id'] = 3;
            }
        } elseif ($result[0] == 'agent_id') {
            $cek = mAgent::find($result[1]);
            if ($cek->user_status == 'ada') {
                return response([
                    'message' => '<strong>This Agent already has a user account</strong>'
                ], 422);
            } else {
                mAgent::where('agent_id', $result[1])->update(['user_status' => 'ada']);
                $data['user_role_id'] = 4;
            }
        }
        $data['user_password'] = Hash::make($data['user_password']);
        $new_user = mUser::create($data);
        if ($request->hasFile('user_pict')) {
            $file = $request->file('user_pict');
            $path = 'private-label-system';
            Storage::disk('sftp')->putFileAs($path, $file, 'user-'.$new_user->user_id.'-'.$file->getClientOriginalName());
            mUser::where('user_id', $new_user->user_id)->update(['user_pict' => 'user-'.$new_user->user_id.'-'.$file->getClientOriginalName()]);
        }

    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $user = mUser::where('user_id', $id)->first();
        $result = explode('|', $user->from);
        if ($result[0] == 'admin_id') {
            mAdmin::where('admin_id', $result[1])->update(['user_status' => 'kosong']);
        } elseif ($result[0] == 'agency_id') {
            mAgency::where('agency_id', $result[1])->update(['user_status' => 'kosong']);
        } elseif ($result[0] == 'client_id') {
            mClient::where('client_id', $result[1])->update(['user_status' => 'kosong']);
        } elseif ($result[0] == 'agent_id') {
            mAgent::where('agent_id', $result[1])->update(['user_status' => 'kosong']);
        }
        mUser::where('user_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $user = mUser::where('user_id', $id)->first();
        $from = $request->input('from');
        $user_name = $request->input('user_name');
//        $user_email = $request->input('user_email');
        $user_password = $request->input('user_password');
        $user_data = $request->input('user_data');
        $user_desc = $request->input('user_desc');
        $result = explode('|', $from);
        $awal = explode('|', $user->from);
        if ($user->user_name != $user_name) {
            $request->validate([
                'from' => 'required',
                'user_name' => ['bail', 'required', new UsernameCheckerUpdate($id)],
                'user_password' => 'bail',
                'user_password_confirm' => 'bail|same:user_password',
//                'user_email' => 'bail|required|email'
            ]);
        } else {
            $request->validate([
                'from' => 'required',
                'user_name' => 'required',
//                'user_email' => 'required|email'
            ]);
        }

        $data = [
            'user_name' => $user_name,
//            'user_email' => $user_email,
            'user_password' => $user_password,
            'user_data' => $user_data,
            'user_desc' => $user_desc,
            'from' => $from
        ];

        if($user->from != $from){
            if ($result[0] == 'admin_id') {
                $cek = mAdmin::find($result[1]);
                if ($cek->user_status == 'ada') {
                    return response([
                        'message' => '<strong>This Admin already has a user account</strong>'
                    ], 422);
                } else {
                    mAdmin::where('admin_id', $result[1])->update(['user_status' => 'ada']);
                    $data['user_role_id'] = 1;
                }
            } elseif ($result[0] == 'agency_id') {
                $cek = mUser::where('from', '=', $from)->count();
                if ($cek >= 5) {
                    return response([
                        'message' => '<strong>This Agency already has a user account</strong>'
                    ], 422);
                } else {
                    mAgency::where('agency_id', $result[1])->update(['user_status' => 'ada']);
                    $data['user_role_id'] = 2;
                    $data['count'] = $cek+1;
                }
            } elseif ($result[0] == 'client_id') {
                $cek = mClient::find($result[1]);
                if ($cek->user_status == 'ada') {
                    return response([
                        'message' => '<strong>This Client already has a user account</strong>'
                    ], 422);
                } else {
                    mClient::where('client_id', $result[1])->update(['user_status' => 'ada']);
                    $data['user_role_id'] = 3;
                }
            }elseif ($result[0] == 'agent_id') {
                $cek = mAgent::find($result[1]);
                if ($cek->user_status == 'ada') {
                    return response([
                        'message' => '<strong>This Client already has a user account</strong>'
                    ], 422);
                } else {
                    mAgent::where('agent_id', $result[1])->update(['user_status' => 'ada']);
                    $data['user_role_id'] = 4;
                }
            }

            if ($awal[0] == 'admin_id') {
                mAdmin::where('admin_id', $awal[1])->update(['user_status' => 'kosong']);
            } elseif ($awal[0] == 'agent_id') {
                mAgent::where('agent_id', $awal[1])->update(['user_status' => 'kosong']);
            }
//            elseif ($awal[0] == 'agency_id') {
//                mAgency::where('agency_id', $awal[1])->update(['user_status' => 'kosong']);
//            }
            elseif ($awal[0] == 'client_id') {
                mClient::where('client_id', $awal[1])->update(['user_status' => 'kosong']);
            }
        }

        if ($user_password) {
            $data['user_password'] = Hash::make($data['user_password']);
        } else {
            unset($data['user_password']);
        }
        if ($request->hasFile('user_pict')) {
            $path = 'private-label-system';
            $ada = Storage::disk('sftp')->exists($path.'/'.$user->user_pict);
            if($ada){
                Storage::disk('sftp')->delete($path.'/'.$user->user_pict);
            }
            $file = $request->file('user_pict');
            Storage::disk('sftp')->putFileAs($path, $file, 'user-'.$user->user_id.'-'.$file->getClientOriginalName());
            $data_user['user_pict'] = 'user-'.$user->user_id.'-'.$file->getClientOriginalName();
        }
        mUser::where(['user_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $user = new mUser();
        $result['iTotalRecords'] = $user->count_all();
        $result['iTotalDisplayRecords'] = $user->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $user->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('userDelete', ['id' => Main::encrypt($value->user_id)]);
            $value->route['edit'] = route('userUpdate', ['id' => Main::encrypt($value->user_id)]);
            $value->direct['edit'] = route('userUpdate', ['id' => Main::encrypt($value->user_id)]);
            $value->user_password = '';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
