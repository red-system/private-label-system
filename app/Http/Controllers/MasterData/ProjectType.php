<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mProjectType;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class ProjectType extends Controller
{
    private $view = [
        'project_type_name' => ["search_field" => "tb_project_type.project_type_name"],
        'project_type_desc' => ["search_field" => "tb_project_type.project_type_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/projectType/projectTypeList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'project_type_name' => 'required',
        ];

        $attributes = [
            'project_type_name' => 'Project Type Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $project_type_name = $request->input('project_type_name');
        $project_type_desc = $request->input('project_type_desc');
        $data = [
            'project_type_name' => $project_type_name,
            'project_type_desc' => $project_type_desc,
        ];
        mProjectType::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mProjectType::where('project_type_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'project_type_name' => 'required',
        ];

        $attributes = [
            'project_type_name' => 'Project Type Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $project_type_name = $request->input('project_type_name');
        $project_type_desc = $request->input('project_type_desc');
        $data = [
            'project_type_name' => $project_type_name,
            'project_type_desc' => $project_type_desc,
        ];
        mProjectType::where(['project_type_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $project_type = new mProjectType();
        $result['iTotalRecords'] = $project_type->count_all();
        $result['iTotalDisplayRecords'] = $project_type->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $project_type->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('projectTypeDelete', ['id' => Main::encrypt($value->project_type_id)]);
            $value->route['edit'] = route('projectTypeUpdate', ['id' => Main::encrypt($value->project_type_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
