<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mSupport;
use app\Models\mSupportData;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class Support extends Controller
{
    private $view = [
        'support_title' => ["search_field" => "tb_support.support_title"],
    ];
    private $view_data = [
        'support_data' => ["search_field" => "tb_support_data.support_data"],
        'support_desc' => ["search_field" => "tb_support_data.support_desc"],
    ];
    private $breadcrumb;
    private $breadcrumb_data;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_14'],
                'route' => ''
            ]
        ];

        $this->breadcrumb_data = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_14'],
                'route' => ''
            ],
            [
                'label' => 'Support Data',
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/support/supportList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'support_title' => 'required',
        ];

        $attributes = [
            'support_title' => 'Support Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $support_title = $request->input('support_title');
        $data = [
            'support_title' => $support_title,
        ];
        mSupport::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mSupport::where('support_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'support_title' => 'required',
        ];

        $attributes = [
            'support_title' => 'Support Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $support_title = $request->input('support_title');
        $data = [
            'support_title' => $support_title,
        ];
        mSupport::where(['support_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $support = new mSupport();
        $result['iTotalRecords'] = $support->count_all();
        $result['iTotalDisplayRecords'] = $support->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $support->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('supportDelete', ['id' => Main::encrypt($value->support_id)]);
            $value->route['edit'] = route('supportUpdate', ['id' => Main::encrypt($value->support_id)]);
            $value->route['detail'] = route('supportDataPage', ['id' => Main::encrypt($value->support_id)]);
            $value->ignore['detail'] = route('supportDataPage', ['id' => Main::encrypt($value->support_id)]);
            $value->direct['detail'] = route('supportDataPage', ['id' => Main::encrypt($value->support_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function data_index($id)
    {
        $id = Main::decrypt($id);
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb_data, 'data_support_center');
        $view_data = array();
        array_push($view_data, array("data" => "no"));
        foreach ($this->view_data as $key => $value) {
            array_push($view_data, array("data" => $key));
        }
        $action = [
            0 => 'edit',
            1 => 'delete'
        ];
        $data['id'] = Main::encrypt($id);
        $data['pageTitle'] = 'Support Data';
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view_data);
        return view('masterData/support/data/supportDataList', $data);
    }

    function data_list(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $support = new mSupportData();
        $result['iTotalRecords'] = $support->count_all($id);
        $result['iTotalDisplayRecords'] = $support->count_filter($query, $this->view_data,$id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $support->list($start, $length, $query, $this->view_data,$id);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('supportDataDelete', ['id' => Main::encrypt($value->support_data_id)]);
            $value->route['edit'] = route('supportDataUpdate', ['id' => Main::encrypt($value->support_data_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function data_insert(Request $request)
    {
        $rules = [
            'support_data' => 'required',
            'support_desc' => 'required',
        ];

        $attributes = [
            'support_data' => 'Support Data Title',
            'support_desc' => 'Support Data Desc',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $support_id = Main::decrypt($request->input('support_id'));
        $support_data = $request->input('support_data');
        $support_desc = $request->input('support_desc');
        $data = [
            'support_id' => $support_id,
            'support_data' => $support_data,
            'support_desc' => $support_desc,
        ];
        mSupportData::create($data);
    }

    function data_delete($id)
    {
        $id = Main::decrypt($id);
        mSupportData::where('support_data_id', $id)->delete();
    }

    function data_update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'support_data' => 'required',
            'support_desc' => 'required',
        ];

        $attributes = [
            'support_data' => 'Support Data Title',
            'support_desc' => 'Support Data Desc',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $support_data = $request->input('support_data');
        $support_desc = $request->input('support_desc');
        $data = [
            'support_data' => $support_data,
            'support_desc' => $support_desc,
        ];
        mSupportData::where(['support_data_id' => $id])->update($data);
    }
}
