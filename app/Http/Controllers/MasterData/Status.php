<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mStatus;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class Status extends Controller
{
    private $view = [
        'status_name' => ["search_field" => "tb_status.status_name"],
        'status_data' => ["search_field" => "tb_status.status_data"],
        'status_desc' => ["search_field" => "tb_status.status_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/status/statusList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'status_name' => 'required',
        ];

        $attributes = [
            'status_name' => 'Status Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $status_name = $request->input('status_name');
        $status_data = $request->input('status_data');
        $status_desc = $request->input('status_desc');
        $data = [
            'status_name' => $status_name,
            'status_data' => $status_data,
            'status_desc' => $status_desc,
        ];
        mStatus::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mStatus::where('status_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'status_name' => 'required',
        ];

        $attributes = [
            'status_name' => 'Status Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $status_name = $request->input('status_name');
        $status_data = $request->input('status_data');
        $status_desc = $request->input('status_desc');
        $data = [
            'status_name' => $status_name,
            'status_data' => $status_data,
            'status_desc' => $status_desc,
        ];
        mStatus::where(['status_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $status = new mStatus();
        $result['iTotalRecords'] = $status->count_all();
        $result['iTotalDisplayRecords'] = $status->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $status->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('statusDelete', ['id' => Main::encrypt($value->status_id)]);
            $value->route['edit'] = route('statusUpdate', ['id' => Main::encrypt($value->status_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
