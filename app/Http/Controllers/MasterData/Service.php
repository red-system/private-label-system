<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mChannel;
use app\Models\mService;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class Service extends Controller
{
    private $view = [
        'services_name' => ["search_field" => "tb_services.services_name"],
        'channel_name' => ["search_field" => "tb_channel.channel_name"],
        'services_fee' => ["search_field" => "tb_services.services_fee"],
        'services_cost' => ["search_field" => "tb_services.services_cost"],
        'services_data' => ["search_field" => "tb_services.services_data"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_9'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['channel'] = mChannel::orderBy('tb_channel.channel_name', 'ASC')->get();
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/services/servicesList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'services_name' => 'required',
            'channel_id' => 'required',
        ];

        $attributes = [
            'services_name' => 'Service Name',
            'channel_id' => 'Channel',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $services_name = $request->input('services_name');
        $services_fee = Main::string_to_number($request->input('services_fee'));
        $services_cost = Main::string_to_number($request->input('services_cost'));
        $services_data = $request->input('services_data');
        $channel_id = $request->input('channel_id');
        $data = [
            'services_name' => $services_name,
            'services_fee' => $services_fee,
            'services_cost' => $services_cost,
            'services_data' => $services_data,
            'channel_id' => $channel_id,
        ];
        mService::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mService::where('services_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'services_name' => 'required',
            'channel_id' => 'required',
        ];

        $attributes = [
            'services_name' => 'Service Name',
            'channel_id' => 'Channel',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $services_name = $request->input('services_name');
        $services_fee = Main::string_to_number($request->input('services_fee'));
        $services_cost = Main::string_to_number($request->input('services_cost'));
        $services_data = $request->input('services_data');
        $channel_id = $request->input('channel_id');
        $data = [
            'services_name' => $services_name,
            'services_fee' => $services_fee,
            'services_cost' => $services_cost,
            'services_data' => $services_data,
            'channel_id' => $channel_id,
        ];
        mService::where(['services_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $services = new mService();
        $result['iTotalRecords'] = $services->count_all();
        $result['iTotalDisplayRecords'] = $services->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $services->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('servicesDelete', ['id' => Main::encrypt($value->services_id)]);
            $value->route['edit'] = route('servicesUpdate', ['id' => Main::encrypt($value->services_id)]);
            $value->services_fee = Main::format_number($value->services_fee);
            $value->services_cost = Main::format_number($value->services_cost);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
