<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mChannel;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class Channel extends Controller
{
    private $view = [
        'channel_name' => ["search_field" => "tb_channel.channel_name"],
        'channel_fee' => ["search_field" => "tb_channel.channel_fee"],
        'channel_cost' => ["search_field" => "tb_channel.channel_cost"],
        'channel_data' => ["search_field" => "tb_channel.channel_data"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_8'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/channel/channelList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'channel_name' => 'required',
        ];

        $attributes = [
            'channel_name' => 'Channel Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $channel_name = $request->input('channel_name');
        $channel_fee = Main::string_to_number($request->input('channel_fee'));
        $channel_cost = Main::string_to_number($request->input('channel_cost'));
        $channel_data = $request->input('channel_data');
        $data = [
            'channel_name' => $channel_name,
            'channel_fee' => $channel_fee,
            'channel_cost' => $channel_cost,
            'channel_data' => $channel_data,
        ];
        mChannel::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mChannel::where('channel_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'channel_name' => 'required',
        ];

        $attributes = [
            'channel_name' => 'Channel Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $channel_name = $request->input('channel_name');
        $channel_fee = Main::string_to_number($request->input('channel_fee'));
        $channel_cost = Main::string_to_number($request->input('channel_cost'));
        $channel_data = $request->input('channel_data');
        $data = [
            'channel_name' => $channel_name,
            'channel_fee' => $channel_fee,
            'channel_cost' => $channel_cost,
            'channel_data' => $channel_data,
        ];
        mChannel::where(['channel_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $channel = new mChannel();
        $result['iTotalRecords'] = $channel->count_all();
        $result['iTotalDisplayRecords'] = $channel->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $channel->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('channelDelete', ['id' => Main::encrypt($value->channel_id)]);
            $value->route['edit'] = route('channelUpdate', ['id' => Main::encrypt($value->channel_id)]);
            $value->channel_fee = Main::format_number($value->channel_fee);
            $value->channel_cost = Main::format_number($value->channel_cost);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
