<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mCompanyType;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class CompanyType extends Controller
{
    private $view = [
        'company_type_name' => ["search_field" => "tb_company_type.company_type_name"],
        'company_type_desc' => ["search_field" => "tb_company_type.company_type_desc"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_11'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/companyType/companyTypeList', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'company_type_name' => 'required',
        ];

        $attributes = [
            'company_type_name' => 'COmpany Type Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $company_type_name = $request->input('company_type_name');
        $company_type_desc = $request->input('company_type_desc');
        $data = [
            'company_type_name' => $company_type_name,
            'company_type_desc' => $company_type_desc,
        ];
        mCompanyType::create($data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mCompanyType::where('company_type_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'company_type_name' => 'required',
        ];

        $attributes = [
            'company_type_name' => 'COmpany Type Name',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $company_type_name = $request->input('company_type_name');
        $company_type_desc = $request->input('company_type_desc');
        $data = [
            'company_type_name' => $company_type_name,
            'company_type_desc' => $company_type_desc,
        ];
        mCompanyType::where(['company_type_id' => $id])->update($data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $company_type = new mCompanyType();
        $result['iTotalRecords'] = $company_type->count_all();
        $result['iTotalDisplayRecords'] = $company_type->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $company_type->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('companyTypeDelete', ['id' => Main::encrypt($value->company_type_id)]);
            $value->route['edit'] = route('companyTypeUpdate', ['id' => Main::encrypt($value->company_type_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
