<?php

namespace app\Http\Controllers\MasterData;

use app\Helpers\Main;
use app\Models\mServicesContent;
use app\Models\mSupport;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ServicesContent extends Controller
{
    private $view = [
        'row' => ["search_field" => "tb_services_content.row"],
        'block' => ["search_field" => "tb_services_content.block"],
        'judul' => ["search_field" => "tb_services_content.judul"],
        'isi' => ["search_field" => "tb_services_content.isi"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_16'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $row_count = mServicesContent::max('row');
        $row = [];
        for ($i = 1; $i <= $row_count + 1; $i++) {
            $row[$i] = $i;
        }
        $block = [];
        for ($i = 1; $i <= 4; $i++) {
            $block[$i] = $i;
        }

        $count = [];
        for ($i = 0; $i <= 2; $i++) {
            $count[$i] = $i;
        }

        $faq = mSupport::all();
        $data['faq'] = $faq;
        $data['count'] = $count;
        $data['row'] = $row;
        $data['block'] = $block;
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('masterData/servicesContent/contentList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $services = new mServicesContent();
        $result['iTotalRecords'] = $services->count_all();
        $result['iTotalDisplayRecords'] = $services->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $services->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('contentDelete', ['id' => Main::encrypt($value->content_id)]);
            $value->route['edit'] = route('contentUpdate', ['id' => Main::encrypt($value->content_id)]);
            $value->route['view'] = route('contentUpdateDetail', ['id' => Main::encrypt($value->content_id)]);
            $value->class_tag['view'] = 'btn-view-edit';

//            if($value->text_detail == null){
//                $value->text_detail = [];
//            }
            $value->text_title = json_decode($value->text_title, true);
            $value->text_detail = json_decode($value->text_detail, true);

//            dd($value->text_detail);

//            $value->redirect['view'] = route('contentPage', ['id' => Main::encrypt($value->content_id)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function insert(Request $request)
    {
        $rules = [
            'row' => 'required',
            'block' => 'required',
            'judul' => 'required',
        ];

        $attributes = [
            'row' => 'Row',
            'block' => 'Block',
            'judul' => 'Title',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $user = Session::get('user');
        $row = $request->input('row');
        $block = $request->input('block');
        $bc = $request->input('bc');
        $judul = $request->input('judul');
        $judul_bc = $request->input('judul_bc');
        $isi = $request->input('isi');
        $isi_bc = $request->input('isi_bc');
        $gambar = $request->input('gambar');
        $button = $request->input('button') == true ? 1 : '0';
        $button_color = $request->input('button_color');
        $button_text = $request->input('button_text');
        $button_text_bc = $request->input('button_text_bc');


        $count = mServicesContent::where('row', $row)->count();
        $count_block = mServicesContent::where(['row' => $row, 'block' => $block])->count();

        if ($count >= 4) {
            return response([
                'message' => 'This row is full, please select another row, or if all rows are full, please reload this page'
            ], 422);
        }

        if ($count_block > 0) {
            return response([
                'message' => 'This block has been filled, please choose another block'
            ], 422);
        }

        if ($button == 0) {
            $button_color = null;
            $button_text = null;
            $button_text_bc = null;
        }
        $nama_gambar = null;
        if ($request->hasFile('gambar')) {
            $path = 'private-label-system';
            $file = $request->file('gambar');
            $filename = $user->user_id . '-' . now() . '-' . $row . '-' . $block . '-' . $file->getClientOriginalName();
            $replaced = str_replace(' ', '-', $filename);
            Storage::disk('sftp')->putFileAs($path . '/content', $file, $replaced);
            $nama_gambar = $replaced;
//            dd($nama_gambar);
        }

        DB::beginTransaction();
        try {
            $data = [
                'row' => $row,
                'block' => $block,
                'bc' => $bc,
                'judul' => $judul,
                'judul_bc' => $judul_bc,
                'isi' => $isi,
                'isi_bc' => $isi_bc,
                'gambar' => $nama_gambar,
                'button' => $button,
                'button_color' => $button_color,
                'button_text' => $button_text,
                'button_text_bc' => $button_text_bc,
            ];
            mServicesContent::create($data);
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mServicesContent::where('content_id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'row' => 'required',
            'block' => 'required',
            'judul' => 'required',
        ];

        $attributes = [
            'row' => 'Row',
            'block' => 'Block',
            'judul' => 'Title',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $user = Session::get('user');
        $row = $request->input('row');
        $block = $request->input('block');
        $bc = $request->input('bc');
        $judul = $request->input('judul');
        $judul_bc = $request->input('judul_bc');
        $isi = $request->input('isi');
        $isi_bc = $request->input('isi_bc');
        $gambar = $request->input('gambar');
        $button = $request->input('button') == true ? 1 : '0';
        $button_color = $request->input('button_color');
        $button_text = $request->input('button_text');
        $button_text_bc = $request->input('button_text_bc');


        $count = mServicesContent::where('row', $row)->count();
        $count_block = mServicesContent::where(['row' => $row, 'block' => $block])->count();
        $content = mServicesContent::find($id);
        if ($row != $content->row) {
            if ($count >= 4) {
                return response([
                    'message' => 'This row is full, please select another row, or if all rows are full, please reload this page'
                ], 422);
            }
        }

        if ($block != $content->block) {
            if ($count_block > 0) {
                return response([
                    'message' => 'This block has been filled, please choose another block'
                ], 422);
            }
        }


        if ($button != 1) {
            $button_color = null;
            $button_text = null;
            $button_text_bc = null;
        }


        DB::beginTransaction();
        try {
            $data = [
                'row' => $row,
                'block' => $block,
                'bc' => $bc,
                'judul' => $judul,
                'judul_bc' => $judul_bc,
                'isi' => $isi,
                'isi_bc' => $isi_bc,
                'button' => $button,
                'button_color' => $button_color,
                'button_text' => $button_text,
                'button_text_bc' => $button_text_bc,
            ];

            if ($request->hasFile('gambar')) {
                $path = 'private-label-system';
                $ada = Storage::disk('sftp')->exists($path . '/content' . $content->gambar);
                if ($ada) {
                    Storage::disk('sftp')->delete($path . '/content' . $content->gambar);
                }
                $file = $request->file('gambar');
                $filename = $user->user_id . '-' . now() . '-' . $row . '-' . $block . '-' . $file->getClientOriginalName();
                $replaced = str_replace(' ', '-', $filename);
                Storage::disk('sftp')->putFileAs($path, $file, $replaced);
                $data['gambar'] = $replaced;
            }
            mServicesContent::where('content_id', $id)->update($data);
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function update_detail(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $rules = [
            'faq_id' => 'required',
        ];

        $attributes = [
            'faq_id' => 'FAQ',
        ];

        $content = mServicesContent::find($id);

        if($content->gambar_detail == null){
            $rules['gambar_detail'] = 'required';
            $attributes['gambar_detail'] = 'Image';
        }


        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['text_title.' . $i] = 'Text Title -' . $next;
            $attr['text_detail.' . $i] = 'Text Detail -' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $faq_id = $request->input('faq_id');
        $text_title = $request->input('text_title');
        $text_title = json_encode($text_title);
        $text_detail = $request->input('text_detail');
        $text_detail = json_encode($text_detail);


        DB::beginTransaction();
        try {
            $data = [
                'faq_id' => $faq_id,
                'text_title' => $text_title,
                'text_detail' => $text_detail,
            ];

            if ($request->hasFile('gambar_detail')) {
                $path = 'private-label-system';
                $ada = Storage::disk('sftp')->exists($path . '/content/' . $content->gambar_detail);
                if ($ada) {
                    Storage::disk('sftp')->delete($path . '/content/' . $content->gambar_detail);
                }
                $file = $request->file('gambar_detail');
                $filename = $user->user_id . '-' . now() . '-' . $content->row . '-' . $content->block . '-' . $file->getClientOriginalName();
                $replaced = str_replace(' ', '-', $filename);
                Storage::disk('sftp')->putFileAs($path . '/content/', $file, $replaced);
                $data['gambar_detail'] = $replaced;
            }
            mServicesContent::where('content_id', $id)->update($data);
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

}
