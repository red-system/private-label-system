<?php

namespace app\Http\Controllers\Agency;

use app\Helpers\Main;
use app\Models\mClient;
use app\Models\mClientStatus;
use app\Models\mClientType;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Report extends Controller
{
    private $view = [
        'client_name' => ["search_field" => "tb_client.client_name"],
        'client_contact' => ["search_field" => "tb_client.client_contact"],
        'client_email' => ["search_field" => "tb_client.client_email"],
        'client_type_name' => ["search_field" => "tb_client_type.client_type_name"],
        'client_status_name' => ["search_field" => "tb_client_status.client_status_name"],
        'client_desc' => ["search_field" => "tb_client.client_desc"],
        'client_data' => ["search_field" => "tb_client.client_data"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['agency'],
                'route' => ''
            ],
            [
                'label' => $cons['agency_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }

        $data['client'] = mClient::orderBy('client_name', 'ASC')->get();
        $data['client_type'] = mClientType::orderBy('client_type_name', 'ASC')->get();
        $data['client_status'] = mClientStatus::orderBy('client_status_name', 'ASC')->get();
        $data['client_id'] = $request->input('client_id') ? $request->input('client_id') : null;
        $data['client_type_id'] = $request->input('client_type_id') ? $request->input('client_type_id') : null;
        $data['client_status_id'] = $request->input('client_status_id') ? $request->input('client_status_id') : null;
        $data['params'] = [
            'client_id' => $data['client_id'],
            'client_type_id' => $data['client_type_id'],
            'client_status_id' => $data['client_status_id'],
        ];
//        dd($data['client']);
        $data['view'] = json_encode($view);
        return view('agency/report/reportList', $data);
    }

    function list(Request $request)
    {
        $client_id = $request->input('client_id');
        $client_type_id = $request->input('client_type_id');
        $client_status_id = $request->input('client_status_id');
        $user = Session::get('user');
        $from = explode('|', $user->from);
        if ($from[0] != 'agency_id') {
            $from[1] = 'kosong';
        }
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $client = new mClient();
        $result['iTotalRecords'] = $client->count_all_report($from[1], $client_id, $client_type_id, $client_status_id);
        $result['iTotalDisplayRecords'] = $client->count_filter_report($query, $this->view, $from[1], $client_id, $client_type_id, $client_status_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $client->list_report($start, $length, $query, $this->view, $from[1], $client_id, $client_type_id, $client_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('clientDelete', ['id' => $value->client_id]);
            $value->route['edit'] = route('clientUpdate', ['id' => $value->client_id]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $client_id = $request->input('client_id');
        $client_type_id = $request->input('client_type_id');
        $client_status_id = $request->input('client_status_id');
        $user = Session::get('user');
        $from = explode('|', $user->from);

        if ($from[0] != 'agency_id') {
            $from[1] = 'kosong';
        }
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $client = new mClient();

        $list = $client->list_report($start, $length, $query, $this->view, $from[1], $client_id, $client_type_id, $client_status_id);

        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('agency/report/reportPdf', $data);


        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('My Client Report ' . Main::format_date(now()));
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $client_id = $request->input('client_id');
        $client_type_id = $request->input('client_type_id');
        $client_status_id = $request->input('client_status_id');
        $user = Session::get('user');
        $from = explode('|', $user->from);

        if ($from[0] != 'agency_id') {
            $from[1] = 'kosong';
        }
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $client = new mClient();

        $list = $client->list_report($start, $length, $query, $this->view, $from[1], $client_id, $client_type_id, $client_status_id);

        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        return view('agency/report/reportExcel', $data);
    }
}
