<?php

namespace app\Http\Controllers\Agency;

use app\Helpers\Main;
use app\Models\mAgency;
use app\Models\mClient;
use app\Models\mClientStatus;
use app\Models\mClientType;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MyClient extends Controller
{
    private $view = [
        'client_name_label' => ["search_field" => "tb_client.client_name"],
        'client_contact_label' => ["search_field" => "tb_client.client_contact"],
        'client_email_label' => ["search_field" => "tb_client.client_email"],
        'client_type_name_label' => ["search_field" => "tb_client_type.client_type_name"],
        'client_status_name_label' => ["search_field" => "tb_client_status.client_status_name"],
        'client_desc_label' => ["search_field" => "tb_client.client_desc"],
        'client_data_label' => ["search_field" => "tb_client.client_data"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['agency'],
                'route' => ''
            ],
            [
                'label' => $cons['agency_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
//        dd($data);
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['client_status'] = mClientStatus::orderBy('client_status_name')->get();
        $data['client_type'] = mClientType::orderBy('client_type_name')->get();
        $data['from'] = $from;
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('agency/myClient/myClientList', $data);
    }

    function list(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $client_id = [];
        $client_type_id = [];
        $client_status_id = [];
        if($from[0] != 'agency_id'){
            $from[1] = 'kosong';
        }
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $client = new mClient();
        $result['iTotalRecords'] = $client->count_all_report($from[1], $client_id, $client_type_id, $client_status_id);
        $result['iTotalDisplayRecords'] = $client->count_filter_report($query, $this->view, $from[1], $client_id, $client_type_id, $client_status_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $client->list_report($start, $length, $query, $this->view, $from[1], $client_id, $client_type_id, $client_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->route['delete'] = route('clientDelete', ['id' => $value->client_id]);
            $value->route['edit'] = route('clientUpdate', ['id' => $value->client_id]);
            $value->route['detail'] = route('clientUpdate', ['id' => $value->client_id]);
            $value->route['commisison_cost'] = route('myClientCommisionCost', ['id' => $value->client_id]);
            if($from[0] != 'agency_id'){
                $value->visibility['delete'] = 'none';
                $value->visibility['edit'] = 'none';
                $value->visibility['detail'] = 'none';
                $value->visibility['commisison_cost'] = 'none';
            }
            $value->client_name_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_name.'</a>';
            $value->client_contact_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_contact.'</a>';
            $value->client_email_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_email.'</a>';
            $value->client_type_name_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_type_name.'</a>';
            $value->client_status_name_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_status_name.'</a>';
            $value->client_desc_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_desc.'</a>';
            $value->client_data_label = '<a style="color : #000" href="'.route('projectSingle', $value->client_id).'">'.$value->client_data.'</a>';

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function commision_cost(Request $request, $id)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $rules = [
            'commisions' => 'required',
            'fixed_cost' => 'required',
        ];

        $attributes = [
            'commisions' => 'Commision',
            'fixed_cost' => 'Fixed Cost',
        ];
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $agency_id = $request->input('agency_id');

        $commisions = Main::string_to_number($request->input('commisions'));
        $fixed_cost = Main::string_to_number($request->input('fixed_cost'));

        $data = [
            'commisions' => $commisions,
            'fixed_cost' => $fixed_cost,
        ];
        mClient::where(['client_id' => $id])->update($data);
    }
}
