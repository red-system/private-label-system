<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPaymentPiutangLangganan;
use app\Models\Widi\mPaymentPiutangGiro;
use app\Models\Widi\mPiutangGiro;
use app\Models\Widi\mPiutangLangganan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PiutangGiro extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_piutang_giro' => ["search_field" => "tb_piutang_giro.no_piutang_giro"],
        'tgl_pencairan_piutang_giro' => ["search_field" => "tb_piutang_giro.tgl_pencairan_piutang_giro"],
        'jumlah_piutang_giro' => ["search_field" => "tb_piutang_giro.jumlah_piutang_giro"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'no_faktur_penjualan' => ["search_field" => "tb_piutang_giro.no_faktur_penjualan"],
        'tgl_faktur_penjualan' => ["search_field" => "tb_piutang_giro.tgl_faktur_penjualan"],
        'nama_bank_piutang_giro' => ["search_field" => "tb_piutang_giro.nama_bank_piutang_giro"],
        'keterangan_piutang_giro' => ["search_field" => "tb_piutang_giro.keterangan_piutang_giro"],
        'status_piutang_giro' => ["search_field" => "tb_piutang_giro.status_piutang_giro"],

    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['arap'],
                'route' => ''
            ],
            [
                'label' => $cons['arap_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
//         dd($data);
        return view('hutangPiutang/piutangGiro/piutangGiroList', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * proses menampilkan data di form pembayaran
     */
    function bayar($id)
    {
        $year = date('y');
        $month = date('m');
        $date = date('d');
        /**
         * proses menghitung urutan melalui model mPaymentPiutangGiro
         */
        $payment = new mPaymentPiutangGiro();
        $data = $payment->countNoPayment();
        $urutan = $data + 1;
        $no_bukti = $date . $month . $year . '.' . $urutan;
        $pg = mPiutangGiro::where('id', $id)->first();
        $langganan = mLangganan::where('id', $pg->id_langganan)->value('langganan');
        $data = [
            'pg' => $pg,
            'no_bukti' => $no_bukti,
            'langganan' => $langganan
        ];
        return view('hutangPiutang/piutangGiro/modalBodyBayar', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $pl = new mPiutangGiro();
        $result['iTotalRecords'] = $pl->count_all();
        $result['iTotalDisplayRecords'] = $pl->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $pl->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $tgl_faktur = Carbon::parse($value->tgl_faktur_penjualan)->format('d-m-Y');
            $value->tgl_faktur_penjualan = $tgl_faktur;
            $tempo = Carbon::parse($value->tgl_pencairan_piutang_giro)->format('d-m-Y');
            $value->tgl_pencairan_piutang_giro = $tempo;
            if ($value->status_piutang_giro == 'cair') {
                $value->status_piutang_giro = 'Sudah Cair';
                $value->route['bayar'] = route('piutangGiroBayar', ['id' => $value->id]);
                $value->ignore['bayar'] = true;
                $value->visibility['bayar'] = 'none';
            } else {
                $value->status_piutang_giro = 'Belum Cair';
                $value->route['bayar'] = route('piutangGiroBayar', ['id' => $value->id]);
                $value->label['bayar'] = 'Cairkan';
                $value->icon['bayar'] = 'fa fa-money-bill';
                $value->class_tag['bayar'] = 'btn-bayar akses-bayar';
                $value->ignore['bayar'] = true;
            }
            $value->route['detail_view'] = route('piutangGiroDetail', ['id' => $value->id]);

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail($id)
    {
        $piutang = mPiutangGiro::where('id', $id)->first();
        if ($piutang->status_piutang_giro == 'cair') {
            $piutang->status_piutang_giro = 'Sudah Cair';
        } else {
            $piutang->status_piutang_giro = 'Belum Cair';
        }
        $history = mPaymentPiutangGiro::where('id_piutang_giro', $id)->get();
        $data = [
            'piutang' => $piutang,
            'history' => $history
        ];
        return view('hutangPiutang/piutangGiro/modalBodyDetailPiutangGiro', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * fungsi untuk menyimpan data
     */
    function insert(Request $request)
    {
        /**
         * membuat rule untuk data yang wajib di isi
         */
        $rules = [
            'jumlah.*' => 'required'
        ];

        $attributes = [
            'jumlah.*' => 'Jumlah Pembayaran'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['jumlah.*' . $i] = ' ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        /**
         * proses validasi
         * jika data yang wajib di isi kongong maka akan ada peringatan
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * data yang disimpan ke tabel piutang giro
         */
        $id = $request->input('id');
        $piutang = mPiutangGiro::where('id', $id)->first();
        $jumlah_piutang_giro = $piutang->jumlah_piutang_giro;
        $langganan = $request->input('dari_piutang_giro');
        $tanggal_transaksi = $request->input('tanggal_transaksi');
        $no_bukti = $request->input('no_bukti');
        $tanggal_transaksi = date('Y-m-d', strtotime($tanggal_transaksi));

        $id_langganan = mLangganan::where('langganan', $langganan)->value('id');


        /**
         * data yang disimpan di tabel payment
         */
        $metode_pembayaran_arr = $request->input('metode_pembayaran');
        $jumlah_arr = $request->input('jumlah');
        $nama_bank_arr = $request->input('nama_bank');
        $keterangan_arr = $request->input('keterangan');
        $giro = new mPaymentPiutangGiro();
        $data = $giro->countNoPayment();
        $urutan = $data + 1;

        /**
         * Proses mengecek jumlah pembayaran
         */
        $total = 0;
        foreach ($jumlah_arr as $jumlah) {
            $jumlah = str_replace(',', '', $jumlah);
            $total = $total + $jumlah;
        }

        /**
         * proses pengecekan apakah jumlah pembayaran sudah sesuai atau belum
         */
        $hasil = $jumlah_piutang_giro - $total;

        /**
         * jika jumlah pembayaran lebih kecil dari hutang
         * akan muncul peringatan kalau jumlah pembayaran kurang
         */
        if ($hasil > 0) {
            return response([
                'message' => 'Jumlah Pembayaran Anda <strong>Kurang : ' . Main::format_money(abs($hasil)) . '</strong>'
            ], 422);
        } /**
         * jika jumlah pembayaran lebih besar dari hutang
         * akan muncul peringatan kalau jumlah pembayaran lebih
         */
        elseif ($hasil < 0) {
            return response([
                'message' => 'Jumlah Pembayaran Anda <strong>Lebih : ' . Main::format_money(abs($hasil)) . '</strong>'
            ], 422);
        }


        /**
         * update data ke tb_piutang_langganan
         */
        $data_piutang = [
            'status_piutang_giro' => "cair"
        ];

        DB::beginTransaction();
        try {
            mPiutangGiro::where('id', $id)->update($data_piutang);
            $id_piutang = $id;

            /**
             * penyimpanan data di tb_payment
             */
            foreach ($metode_pembayaran_arr as $index => $metode_pembayaran) {
                $jumlah = $jumlah_arr[$index];
                $jumlah = str_replace(',', '', $jumlah);
                $nama_bank = $nama_bank_arr[$index];
                $keterangan = $keterangan_arr[$index];

                /**
                 * data yang akan disimpan di tabel payment
                 */
                $data_payment = [
                    'id_piutang_giro' => $id_piutang,
                    'id_langganan' => $id_langganan,
                    'urutan' => $urutan,
                    'no_bukti_pembayaran' => $no_bukti,
                    'tgl_pembayaran' => $tanggal_transaksi,
                    'metode_pembayaran' => $metode_pembayaran,
                    'jumlah' => $jumlah,
                    'bank' => $nama_bank,
                    'keterangan' => $keterangan,
                    'date' => now()
                ];

                mPaymentPiutangGiro::create($data_payment);
            }
            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }
}
