<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Models\Widi\mHutangGiro;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPaymentHutangGiro;
use app\Models\Widi\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HutangGiro extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_hutang_giro' => ["search_field" => "tb_hutang_giro.no_hutang_giro"],
        'tgl_pencairan_hutang_giro' => ["search_field" => "tb_hutang_giro.tgl_pencairan_hutang_giro"],
        'jumlah_hutang_giro' => ["search_field" => "tb_hutang_giro.jumlah_hutang_giro"],
        'supplier' => ["search_field" => "tb_supplier.supplier"],
        'no_faktur_pembelian' => ["search_field" => "tb_hutang_giro.no_faktur_pembelian"],
        'tgl_faktur_pembelian' => ["search_field" => "tb_hutang_giro.tgl_faktur_pembelian"],
        'nama_bank_hutang_giro' => ["search_field" => "tb_hutang_giro.nama_bank_hutang_giro"],
        'keterangan_hutang_giro' => ["search_field" => "tb_hutang_giro.keterangan_hutang_giro"],
        'status_hutang_giro' => ["search_field" => "tb_hutang_giro.status_hutang_giro"],

    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['arap'],
                'route' => ''
            ],
            [
                'label' => $cons['arap_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('hutangPiutang/hutangGiro/hutangGiroList', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * proses menampilkan data di form pencairan
     */
    function bayar($id)
    {
        $year = date('y');
        $month = date('m');
        $date = date('d');
        /**
         * proses menghitung urutan melalui model mPaymentHutangGiro
         */
        $payment = new mPaymentHutangGiro;
        $data = $payment->countNoPayment();
        $urutan = $data + 1;
        $no_bukti = $date . $month . $year . '.' . $urutan;
        $hg = DB::table('tb_hutang_giro')->select('tb_hutang_giro.*', 'tb_supplier.supplier')
            ->leftjoin('tb_supplier', 'tb_hutang_giro.id_supplier_untuk', '=', 'tb_supplier.id')
            ->where('tb_hutang_giro.id', $id)->first();;
        $data = [
            'hg' => $hg,
            'no_bukti' => $no_bukti,
        ];
        return view('hutangPiutang/hutangGiro/modalBodyBayar', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $hg = new mHutangGiro();
        $result['iTotalRecords'] = $hg->count_all();
        $result['iTotalDisplayRecords'] = $hg->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $hg->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $tgl_faktur = Main::format_date($value->tgl_faktur_pembelian);
            $value->tgl_faktur_pembelian = $tgl_faktur;
            $tempo = Main::format_date($value->tgl_pencairan_hutang_giro);
            $value->tgl_pencairan_hutang_giro = $tempo;
            if ($value->status_hutang_giro == 'cair') {
                $value->status_hutang_giro = 'Sudah Cair';
                $value->route['bayar'] = route('hutangGiroBayar', ['id' => $value->id]);
                $value->ignore['bayar'] = true;
                $value->visibility['bayar'] = 'none';
            } else {
                $value->status_hutang_giro = 'Belum Cair';
                $value->route['bayar'] = route('hutangGiroBayar', ['id' => $value->id]);
                $value->label['bayar'] = 'Cairkan';
                $value->icon['bayar'] = 'fa fa-money-bill';
                $value->class_tag['bayar'] = 'btn-bayar akses-bayar';
                $value->ignore['bayar'] = true;
            }
            $value->route['detail_view'] = route('hutangGiroDetail', ['id' => $value->id]);

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail($id)
    {
        $hutang = mHutangGiro::leftjoin('tb_supplier', 'tb_hutang_giro.id_supplier_untuk', '=', 'tb_supplier.supplier')
            ->where('tb_hutang_giro.id', $id)->first();
        $history = mPaymentHutangGiro::where('id_hutang_giro', $id)->get();
        if ($hutang->status_hutang_giro == 'cair') {
            $hutang->status_hutang_giro = 'Sudah Cair';
        } else {
            $hutang->status_hutang_giro = 'Belum Cair';
        }
        $data = [
            'hutang' => $hutang,
            'history' => $history
        ];
        return view('hutangPiutang/hutangGiro/modalBodyDetailHutang', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     * fungsi untuk menyimpan data
     */
    function insert(Request $request)
    {
        /**
         * membuat rule untuk data yang wajib di isi
         */
        $rules = [
            'jumlah.*' => 'required',
            'metode_pembayaran.*' => 'required'
        ];

        $attributes = [
            'jumlah.*' => 'Jumlah Pembayaran',
            'metode_pembayaran.*' => 'Metode Pembayaran'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['jumlah.*' . $i] = ' ke-' . $next;
            $attr['metode_pembayaran.*' . $i] = ' ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        /**
         * proses validasi
         * jika data yang wajib di isi kosong maka akan ada peringatan
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * data yang disimpan ke tabel hutang giro
         */
        $id = $request->input('id');
        $hutang = mHutangGiro::where('id', $id)->first();
        $jumlah_hutang_giro = $hutang->jumlah_hutang_giro;
        $id_supplier = $request->input('id_supplier_untuk');
        $tanggal_transaksi = $request->input('tanggal_transaksi');
        $no_bukti = $request->input('no_bukti');
        $tanggal_transaksi = date('Y-m-d', strtotime($tanggal_transaksi));


        /**
         * data yang disimpan di tabel payment
         */
        $metode_pembayaran_arr = $request->input('metode_pembayaran');
        $jumlah_arr = $request->input('jumlah');
        $nama_bank_arr = $request->input('nama_bank');
        $keterangan_arr = $request->input('keterangan');
        $giro = new mPaymentHutangGiro;
        $data = $giro->countNoPayment();
        $urutan = $data + 1;

        /**
         * Proses mengecek jumlah pembayaran
         */
        $total = 0;
        foreach ($jumlah_arr as $jumlah) {
            $jumlah = str_replace(',', '', $jumlah);
            $total = $total + $jumlah;
            /**
             * jika jumlah pembayaran sama dengan 0
             * maka akan muncul peringatan kalau tidak boleh membayar 0
             */
            if ($jumlah == 0) {
                return response([
                    'message' => 'Anda tidak boleh membayar <strong> ' . $jumlah . '</strong>'
                ], 422);
            }
        }
        /**
         * proses pengecekan apakah jumlah pembayaran sudah sesuai atau belum
         */
        $hasil = $jumlah_hutang_giro - $total;

        if ($hasil > 0) {
            return response([
                'message' => 'Jumlah Pembayaran Anda <strong>Kurang ' . 'Rp. ' . $hasil . '</strong>'
            ], 422);
        } /**
         * jika jumlah pembayaran lebih besar dari hutang
         * akan muncul peringatan kalau jumlah pembayaran lebih
         */
        elseif ($hasil < 0) {
            $hasil = str_replace('-', '', $hasil);
            return response([
                'message' => 'Jumlah Pembayaran Anda <strong>Lebih ' . Main::format_money(abs($hasil)) . '</strong>'
            ], 422);
        }

        /**
         * update data ke tb_hutang_langganan
         */
        $data_hutang = [
            'status_hutang_giro' => "cair"
        ];

        DB::beginTransaction();
        try {
            mHutangGiro::where('id', $id)->update($data_hutang);
            $id_hutang = $id;

            /**
             * penyimpanan data di tb_payment
             */
            foreach ($metode_pembayaran_arr as $index => $metode_pembayaran) {
                $jumlah = $jumlah_arr[$index];
                $jumlah = Main::string_to_number($jumlah);
                $nama_bank = $nama_bank_arr[$index];
                $keterangan = $keterangan_arr[$index];

                /**
                 * data yang akan disimpan di tabel payment
                 */
                $data_payment = [
                    'id_hutang_giro' => $id_hutang,
                    'id_supplier' => $id_supplier,
                    'urutan' => $urutan,
                    'no_bukti_pembayaran' => $no_bukti,
                    'tgl_pembayaran' => $tanggal_transaksi,
                    'metode_pembayaran' => $metode_pembayaran,
                    'jumlah' => $jumlah,
                    'bank' => $nama_bank,
                    'keterangan' => $keterangan,
                    'date' => now()
                ];

                mPaymentHutangGiro::create($data_payment);
            }
            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }
}
