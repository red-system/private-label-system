<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Models\Widi\mSupplier;
use app\Models\Widi\mPaymentHutang;
use app\Models\Widi\mHutangGiro;
use app\Models\Widi\mHutangSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HutangSupplier extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_hutang_supplier' => ["search_field" => "tb_hutang_supplier.no_hutang_supplier"],
        'no_faktur_pembelian' => ["search_field" => "tb_hutang_supplier.no_faktur_pembelian"],
        'tgl_faktur_pembelian' => ["search_field" => "tb_hutang_supplier.tgl_faktur_pembelian"],
        'jatuh_tempo_hutang_supplier' => ["search_field" => "tb_hutang_supplier.jatuh_tempo_hutang_supplier"],
        'supplier' => ["search_field" => "tb_hutang_supplier.supplier"],
        'total_hutang_supplier' => ["search_field" => "tb_hutang_supplier.total_hutang_supplier"],
        'sisa_hutang_supplier' => ["search_field" => "tb_hutang_supplier.sisa_hutang_supplier"],
        'keterangan_hutang_supplier' => ["search_field" => "tb_hutang_supplier.keterangan_hutang_supplier"],
        'status_hutang_supplier' => ["search_field" => "tb_hutang_supplier.status_hutang_supplier"],

    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['arap'],
                'route' => ''
            ],
            [
                'label' => $cons['arap_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
//         dd($data);
        return view('hutangPiutang/hutangSupplier/hutangSupplierList', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * proses menampilkan data di form pembayaran
     */
    function bayar($id)
    {
        $year = date('y');
        $month = date('m');
        $date = date('d');
        /**
         * proses menghitung urutan melalui model mPaymentHutang
         */
        $payment = new mPaymentHutang();
        $data = $payment->countNoPayment();
        $urutan = $data + 1;
        $no_bukti_pembayaran = $date . $month . $year . '.' . $urutan;
        $hs = mHutangSupplier::select('tb_hutang_supplier.*',
            DB::raw('if(tb_hutang_supplier.id_supplier is null,tb_hutang_supplier.supplier,tb_supplier.supplier) as suppliers'))
            ->leftjoin('tb_supplier', 'tb_hutang_supplier.id_supplier', '=', 'tb_supplier.id')
            ->where('tb_hutang_supplier.id', $id)->first();
        $data = [
            'hs' => $hs,
            'no_bukti_pembayaran' => $no_bukti_pembayaran,
        ];
        return view('hutangPiutang/hutangSupplier/modalBodyBayar', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $hs = new mHutangSupplier();
        $result['iTotalRecords'] = $hs->count_all();
        $result['iTotalDisplayRecords'] = $hs->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $hs->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $tgl_faktur = Main::format_date($value->tgl_faktur_pembelian);
            $value->tgl_faktur_pembelian = $tgl_faktur;
            $tempo = Main::format_date($value->jatuh_tempo_hutang_supplier);
            $value->jatuh_tempo_hutang_supplier = $tempo;
            if ($value->status_hutang_supplier == 'lunas') {
                $value->status_hutang_supplier = 'Lunas';
                $value->route['bayar'] = route('hutangSupplierBayar', ['id' => $value->id]);
                $value->ignore['bayar'] = true;
                $value->visibility['bayar'] = 'none';
            } else {
                $value->status_hutang_supplier = 'Belum Lunas';
                $value->route['bayar'] = route('hutangSupplierBayar', ['id' => $value->id]);
                $value->ignore['bayar'] = true;
            }
            $value->route['detail_view'] = route('hutangSupplierDetail', ['id' => $value->id]);

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail($id)
    {
        $hutang = mHutangSupplier::select('tb_hutang_supplier.*',
            DB::raw('if(tb_hutang_supplier.id_supplier is null,tb_hutang_supplier.supplier,tb_supplier.supplier) as suppliers'))
            ->leftjoin('tb_supplier', 'tb_hutang_supplier.id_supplier', '=', 'tb_supplier.id')
            ->where('tb_hutang_supplier.id', $id)->first();
        $history = mPaymentHutang::where('id_hutang', $id)->get();
        if ($hutang->status_hutang_supplier == 'lunas') {
            $hutang->status_hutang_supplier = 'Lunas';
        } else {
            $hutang->status_hutang_supplier = 'Belum Lunas';
        }
        $data = [
            'hutang' => $hutang,
            'history' => $history
        ];
        return view('hutangPiutang/hutangSupplier/modalBodyDetailHutang', $data);
    }


    function insert(Request $request)
    {
        $rules = [
            'metode_pembayaran.*' => 'required'
        ];

        $attributes = [
            'metode_pembayaran' => 'Metode Pembayaran'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['metode_pembayran.' . $i] = ' ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $id = $request->input('id');
        $hutang = mHutangSupplier::where('id', $id)->first();
        /**
         * data yang disimpan ke tabel hutang supplier
         */
        $supplier = $request->input('id_supplier');
        $no_bukti_pembayaran = $request->input('no_bukti_pembayaran');
        $id_pembelian = $request->input('id_pembelian');
        $sisa_hutang_supplier_awal = $hutang->sisa_hutang_supplier;
        $sisa_hutang_supplier = $request->input('sisa_hutang_supplier');

        $id_supplier = mSupplier::where('id', $supplier)->value('id');


        /**
         * data yang disimpan di tabel payment
         */
        $metode_pembayaran_arr = $request->input('metode_pembayaran');
        $jumlah_arr = $request->input('jumlah');
        $no_cek_giro_arr = $request->input('no_bg');
        $tanggal_pencairan_arr = $request->input('tanggal_pencairan');
        $nama_bank_arr = $request->input('nama_bank');
        $keterangan_arr = $request->input('keterangan');
        $payment = new mPaymentHutang();
        $data = $payment->countNoPayment();
        $urutan = $data + 1;

        $total = 0;
        foreach ($jumlah_arr as $jumlah) {
            $jumlah = Main::string_to_number($jumlah);
            $total = $total + $jumlah;

            if ($jumlah == 0) {
                return response([
                    'message' => 'Anda tidak boleh membayar <strong>' . 'Rp. ' . $jumlah . '</strong>'
                ], 422);
            }
        }

        $hasil = $sisa_hutang_supplier_awal - $total;

        if ($hasil < 0) {
            $hasil = Main::string_to_number($hasil);
            return response([
                'message' => 'Jumlah Pembayaran Anda <strong>Lebih : ' . Main::format_money(abs($hasil)) . '</strong>'
            ], 422);
        };

        if ($sisa_hutang_supplier > 0){
            $status_hutang_supplier = 'belum_lunas';
        }
        else{
            $status_hutang_supplier = 'lunas';
        }
        DB::beginTransaction();
        try {
            /**
             * update data ke tb_hutang_supplier
             */
            $data_hutang = [
                'sisa_hutang_supplier' => $sisa_hutang_supplier,
                'status_hutang_supplier' => $status_hutang_supplier
            ];

            mHutangSupplier::where('id', $id)->update($data_hutang);
            $id_hutang = $id;

            /**
             * penyimpanan data di tb_payment
             */
            foreach ($metode_pembayaran_arr as $index => $metode_pembayaran) {
                $jumlah = $jumlah_arr[$index];
                $jumlah = Main::string_to_number($jumlah);
                $no_cek_giro = $no_cek_giro_arr[$index];
                $tanggal_pencairan = $tanggal_pencairan_arr[$index];
                $tanggal_pencairan = date('Y-m-d', strtotime($tanggal_pencairan));
                $nama_bank = $nama_bank_arr[$index];
                $keterangan = $keterangan_arr[$index];

                /**
                 * jika metode pembayaran adalaha Hutang Giro simpan data di tabel payment dan tabel hutang giro
                 */
                if ($metode_pembayaran == 'Hutang Giro') {

                    /**
                     * data yang akan disimpan di tabel hutang giro
                     */
                    $data_giro = [
                        'id_hutang_supplier' => $id_hutang,
                        'id_pembelian' => $id_pembelian,
                        'no_hutang_giro' => $no_cek_giro,
                        'tgl_pencairan_hutang_giro' => $tanggal_pencairan,
                        'jumlah_hutang_giro' => $jumlah,
                        'id_supplier_untuk' => $supplier,
                        'no_faktur_pembelian' => $hutang->no_faktur_pembelian,
                        'tgl_faktur_pembelian' => $hutang->tgl_faktur_pembelian,
                        'nama_bank_hutang_giro' => $nama_bank,
                        'keterangan_hutang_giro' => $keterangan,
                        'status_hutang_giro' => 'belum_cair',
                        'date' => now()
                    ];

                    mHutangGiro::create($data_giro);

                    /**
                     * data yang akan disimpan di tabel payment
                     */
                    $data_payment = [
                        'id_hutang' => $id_hutang,
                        'id_supplier' => $id_supplier,
                        'urutan' => $urutan,
                        'no_bukti_pembayaran' => $no_bukti_pembayaran,
                        'tgl_pembayaran' => now(),
                        'metode_pembayaran' => $metode_pembayaran,
                        'jumlah' => $jumlah,
                        'no_cek_giro' => $no_cek_giro,
                        'tgl_pencairan_giro' => $tanggal_pencairan,
                        'bank' => $nama_bank,
                        'keterangan' => $keterangan,
                        'date' => now()
                    ];

                    mPaymentHutang::create($data_payment);
                } /**
                 * jika tidak maka simpan data di tabel payment saja
                 */
                else {
                    /**
                     * data yang akan disimpan di tabel payment
                     */
                    $data_payment = [
                        'id_hutang' => $id_hutang,
                        'id_supplier' => $id_supplier,
                        'urutan' => $urutan,
                        'no_bukti_pembayaran' => $no_bukti_pembayaran,
                        'tgl_pembayaran' => now(),
                        'metode_pembayaran' => $metode_pembayaran,
                        'jumlah' => $jumlah,
                        'bank' => $nama_bank,
                        'keterangan' => $keterangan,
                        'date' => now()
                    ];

                    mPaymentHutang::create($data_payment);
                }
            }
            DB::commit();

        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }
}
