<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Helpers\Main;
use app\Models\Widi\mItemSO;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPaymentPiutangLangganan;
use app\Models\Widi\mPaymentHutang;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mPiutangGiro;
use app\Models\Widi\mPiutangLangganan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PiutangLangganan extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_piutang_langganan' => ["search_field" => "tb_piutang_langganan.no_piutang_langganan"],
        'no_faktur_penjualan' => ["search_field" => "tb_piutang_langganan.no_faktur_penjualan"],
        'tgl_faktur_penjualan' => ["search_field" => "tb_piutang_langganan.tgl_faktur_penjualan"],
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'total_piutang' => ["search_field" => "tb_piutang_langganan.total_piutang"],
        'sisa_piutang' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],
        'terbayar' => ["search_field" => "tb_piutang_langganan.terbayar"],
        'keterangan' => ["search_field" => "tb_piutang_langganan.keterangan"],
        'status' => ["search_field" => "tb_piutang_langganan.status"],

    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['arap'],
                'route' => ''
            ],
            [
                'label' => $cons['arap_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('hutangPiutang/piutangLangganan/piutangLanggananList', $data);
    }

    function bayar($id)
    {
        $year = date('y');
        $month = date('m');
        $date = date('d');

        $payment = new mPaymentPiutangLangganan();
        $data = $payment->countNoPayment();
        $urutan = $data + 1;
        $no_bukti = $date . $month . $year . '.' . $urutan;
        $pl = mPiutangLangganan::where('id', $id)->first();
        $langganan = mLangganan::where('id', $pl->id_langganan)->value('langganan');
        $data = [
            'pl' => $pl,
            'no_bukti' => $no_bukti,
            'langganan' => $langganan
        ];
        return view('hutangPiutang/piutangLangganan/modalBodyBayar', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $pl = new mPiutangLangganan();
        $result['iTotalRecords'] = $pl->count_all();
        $result['iTotalDisplayRecords'] = $pl->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $pl->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $tgl_faktur = Main::format_date($value->tgl_faktur_penjualan);
            $value->tgl_faktur_penjualan = $tgl_faktur;
            $tempo = Main::format_date($value->jatuh_tempo);
            $value->jatuh_tempo = $tempo;
            if ($value->status == 'lunas') {
                $value->status = 'Lunas';
                $value->route['bayar'] = route('piutangLanggananBayar', ['id' => $value->id]);
                $value->ignore['bayar'] = true;
                $value->visibility['bayar'] = 'none';
            } else {
                $value->status = 'Belum Lunas';
                $value->route['bayar'] = route('piutangLanggananBayar', ['id' => $value->id]);
                $value->ignore['bayar'] = true;
            }
            $value->route['detail_view'] = route('piutangLanggananDetail', ['id' => $value->id]);


        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail($id)
    {
        $piutang = mPiutangLangganan::where('id', $id)->first();
        $history = mPaymentPiutangLangganan::where('id_piutang', $id)->get();
        $data = [
            'piutang' => $piutang,
            'history' => $history
        ];
        return view('hutangPiutang/piutangLangganan/modalBodyDetailPiutang', $data);
    }

    function status(request $request)
    {
        $id = $request->id;

        $status = mPiutangLangganan::where('id', $id)->value('status');

        if ($status == "Cair") {
            return response([
                'message' => '<strong>Sudah Cair</strong>'
            ], 422);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * proses simpan data
     */
    function insert(Request $request)
    {
        /**
         * membuat rule untuk data yang wajib di isi
         */
        $rules = [
            'metode_pembayaran.*' => 'required'
        ];

        $attributes = [
            'metode_pembayaran.*' => 'Metode Pembayaran'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['metode_pembayran.' . $i] = ' ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        /**
         * proses validasi
         * jika data yang wajib di isi kongong maka akan ada peringatan
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * data yang disimpan ke tabel piutang langganan
         */
        $id = $request->input('id');
        $piutang = mPiutangLangganan::where('id', $id)->first();
        $id_langganan = $request->input('id_langganan');
        $no_bukti = $request->input('no_bukti');
        $tanggal_transaksi = $request->input('tanggal_transaksi');
        $tanggal_transaksi = date('Y-m-d', strtotime($tanggal_transaksi));
        $total_piutang = $piutang->total_piutang;
        $sisa_piutang_awal = $piutang->sisa_piutang;
        $sisa_piutang = $request->input('sisa_piutang');
        $terbayar = $total_piutang - $sisa_piutang;

//        $langganan = mLangganan::where('id', $id_langganan)->value('langganan');


        /**
         * data yang disimpan di tabel payment
         */
        $metode_pembayaran_arr = $request->input('metode_pembayaran');
        $jumlah_arr = $request->input('jumlah');
        $no_bg_arr = $request->input('no_bg');
        $tanggal_pencairan_arr = $request->input('tanggal_pencairan');
        $nama_bank_arr = $request->input('nama_bank');
        $keterangan_arr = $request->input('keterangan');
        $payment = new mPaymentHutang();
        $data = $payment->countNoPayment();
        $urutan = $data + 1;

        /**
         * Proses mengecek jumlah pembayaran
         */
        $total = 0;
        foreach ($jumlah_arr as $jumlah) {
            $jumlah = Main::string_to_number($jumlah);
            $total = $total + $jumlah;

            /**
             * jika jumlah pembayaran sama dengan 0
             * maka akan muncul peringatan kalau tidak boleh membayar 0
             */
            if ($jumlah == 0) {
                return response([
                    'message' => 'Anda tidak boleh membayar <strong> ' . $jumlah . '</strong>'
                ], 422);
            }
        }

        /**
         * proses untuk mengecek apakah jumlah pembayaran sesuai
         */
        $hasil = $sisa_piutang_awal - $total;

        /**
         * jika jumlah pembayaran lebih besar dari piutang
         * maka akan muncul peringatan kalau jumlah pembayaran lebih
         */
        if ($hasil < 0) {
            $hasil = Main::string_to_number($hasil);
            return response([
                'message' => 'Jumlah Pembayaran Anda <strong>Lebih : ' . Main::format_money(abs($hasil)) . '</strong>'
            ], 422);
        };

        /**
         * proses menetukan status
         */

        /**
         * jika sisa piutang lebih besar dari nol
         * maka status belum dibayar
         */
        if ($sisa_piutang > 0) {
            $status = 'belum_lunas';
        } /**
         * jika tidak, maka status terbayar
         */
        else {
            $status = 'lunas';
        }

        /**
         * update data ke tb_piutang_langganan
         */
        $data_piutang = [
            'sisa_piutang' => $sisa_piutang,
            'terbayar' => $terbayar,
            'status' => $status
        ];

        mPiutangLangganan::where('id', $id)->update($data_piutang);
        $id_piutang = $id;

        /**
         * penyimpanan data di tb_payment
         */
        foreach ($metode_pembayaran_arr as $index => $metode_pembayaran) {
            $jumlah = $jumlah_arr[$index];
            $jumlah = Main::string_to_number($jumlah);
            $no_bg = $no_bg_arr[$index];
            $tanggal_pencairan = $tanggal_pencairan_arr[$index];
            $tanggal_pencairan = date('Y-m-d', strtotime($tanggal_pencairan));
            $nama_bank = $nama_bank_arr[$index];
            $keterangan = $keterangan_arr[$index];

            /**
             * jika metode pembayaran adalaha Piutang Giro simpan data di tabel payment dan tabel piutang giro
             */
            if ($metode_pembayaran == 'Piutang Giro') {

                /**
                 * data yang akan disimpan di tabel piutang giro
                 */
                $data_giro = [
                    'id_piutang_langganan' => $id_piutang,
                    'no_piutang_giro' => $no_bg,
                    'tgl_pencairan_piutang_giro' => $tanggal_pencairan,
                    'jumlah_piutang_giro' => $jumlah,
                    'id_langganan' => $id_langganan,
                    'no_faktur_penjualan' => $piutang->no_faktur_penjualan,
                    'tgl_faktur_penjualan' => $piutang->tgl_faktur_penjualan,
                    'nama_bank_piutang_giro' => $nama_bank,
                    'keterangan_piutang_giro' => $keterangan,
                    'status_piutang_giro' => 'belum_cair',
                    'date' => now()
                ];

                mPiutangGiro::create($data_giro);

                $data_payment = [
                    'id_piutang' => $id_piutang,
                    'id_langganan' => $id_langganan,
                    'urutan' => $urutan,
                    'no_bukti_pembayaran' => $no_bukti,
                    'tgl_pembayaran' => $tanggal_transaksi,
                    'metode_pembayaran' => $metode_pembayaran,
                    'jumlah' => $jumlah,
                    'no_piutang_giro' => $no_bg,
                    'tgl_pencairan_piutang_giro' => $tanggal_pencairan,
                    'bank' => $nama_bank,
                    'keterangan' => $keterangan,
                    'date' => now(),
                    'jenis_pembayaran' => 'bayar'
                ];

                mPaymentPiutangLangganan::create($data_payment);
            } /**
             * jika tidak maka simpan data di tabel payment saja
             */
            else {

                /**
                 * data yang akan disimpan di tabel payment
                 */
                $data_payment = [
                    'id_piutang' => $id_piutang,
                    'id_langganan' => $id_langganan,
                    'urutan' => $urutan,
                    'no_bukti_pembayaran' => $no_bukti,
                    'tgl_pembayaran' => $tanggal_transaksi,
                    'metode_pembayaran' => $metode_pembayaran,
                    'jumlah' => $jumlah,
                    'no_cek_giro' => $no_bg,
                    'bank' => $nama_bank,
                    'keterangan' => $keterangan,
                    'date' => now(),
                    'jenis_pembayaran' => 'bayar'
                ];

                mPaymentPiutangLangganan::create($data_payment);
            }
        }
    }
}
