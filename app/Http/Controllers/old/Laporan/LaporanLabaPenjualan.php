<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class laporanLabaPenjualan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_penjualan' => ["search_field" => "no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tgl_penjualan"],
        'jatuh_tempo' => ["search_field" => "jatuh_tempo"],
        'lokasi' => ["search_field" => "lokasi"],
        'langganan' => ["search_field" => "langganan"],
        'salesman' => ["search_field" => "salesman"],
        'netto' => ["search_field" => "netto"],
        'hpp_penjualan' => ["search_field" => "hpp_penjualan"],
        'laba' => ["search_field" => "netto"],
        'margin' => ["search_field" => "netto"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_7'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mpenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
        ];
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanLabaPenjualan/laporanLabaPenjualanList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mpenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mpenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mpenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_laporan_laba();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_laporan_laba($query, $this->detail, $date_start, $date_end)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_laporan_laba($start, $length, $query, $this->detail, $date_start, $date_end);
        foreach ($data as $value) {
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->netto = Main::format_money($value->netto);
            $value->hpp_penjualan = Main::format_money($value->hpp_penjualan);
            $value->laba = Main::format_money($value->laba);
            $value->margin = Main::format_number_db($value->margin) . '%';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mpenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mpenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = null;
        $start = null;
        $length = null;
        $penjualan = new mpenjualan();
        $list = $penjualan->list_laporan_laba($start, $length, $query, $this->detail, $date_start, $date_end);
        $count = $list->count();
        foreach ($list as $value) {
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->netto = Main::format_money($value->netto);
            $value->hpp_penjualan = Main::format_money($value->hpp_penjualan);
            $value->laba = Main::format_money($value->laba);
            $value->margin = Main::format_number_db($value->margin) . '%';
        }

        $data = [
            'list' => $list,
            'count' => $count,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanLabaPenjualan/laporanLabaPenjualanPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Laba Penjualan ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mpenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mpenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = null;
        $start = null;
        $length = null;
        $penjualan = new mpenjualan();
        $list = $penjualan->list_laporan_laba($start, $length, $query, $this->detail, $date_start, $date_end);
        $count = $list->count();
        foreach ($list as $value) {
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->netto = Main::format_money($value->netto);
            $value->hpp_penjualan = Main::format_money($value->hpp_penjualan);
            $value->laba = Main::format_money($value->laba);
            $value->margin = Main::format_number_db($value->margin) . '%';
        }

        $data = [
            'list' => $list,
            'count' => $count,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanLabaPenjualan/laporanLabaPenjualanExcel', $data);
    }
}
