<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mGolongan;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanOmzetSalesmanPerBarang extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'total_qty' => ["search_field" => "qty"],
        'harga_net' => ["search_field" => "harga_net"],
        'subtotal' => ["search_field" => "dpp"],
        'salesman' => ["search_field" => "salesman"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_omzet_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $id_golongan = $request->input('id_golongan');
        $id_golongan = $id_golongan ? $id_golongan : 'all';

        $id_barang = $request->input('id_barang');
        $id_barang = $id_barang ? $id_barang : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);

        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_barang' => $id_barang,
            'id_lokasi' => $id_lokasi,
            'id_golongan' => $id_golongan,
        ];
        $data['golongan'] = mGolongan::orderBy('golongan', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['barang'] = mBarang::orderBy('nama_barang', 'ASC')->get();
        $data['params'] = $params;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['id_golongan'] = $id_golongan;
        $data['id_barang'] = $id_barang;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanOmzetSalesmanPerBarang/laporanOmzetSalesmanPerBarangList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $id_golongan = $request->input('id_golongan');
        $id_golongan = $id_golongan ? $id_golongan : 'all';

        $id_barang = $request->input('id_barang');
        $id_barang = $id_barang ? $id_barang : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_omzet_salesman_per_barang();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_omzet_salesman_per_barang($query, $this->detail, $date_start, $date_end, $id_barang, $id_lokasi, $id_golongan)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_omzet_salesman_per_barang($start, $length, $query, $this->detail, $date_start, $date_end, $id_barang, $id_lokasi, $id_golongan);
        foreach ($data as $value) {
            $value->subtotal = Main::format_money($value->subtotal);
            $value->harga_net = Main::format_money($value->harga_net);
            $value->total_qty = Main::format_decimal($value->total_qty).' '.$value->satuan;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $id_golongan = $request->input('id_golongan');
        $id_golongan = $id_golongan ? $id_golongan : 'all';

        $id_barang = $request->input('id_barang');
        $id_barang = $id_barang ? $id_barang : 'all';


        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $list = $penjualan->list_omzet_salesman_per_barang($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_barang, $id_lokasi, $id_golongan);
        $no = $start + 1;

        $id_barangs = DB::table('analisa_penjualan_termasuk_ppn')->groupBy('id_barang')->get();
//        if (!empty($list)) {
//            foreach ($list as $key => $value) {
//                $id_barangs[$key] = ['id_barang' => $value->id_barang];
//            }
//        }
        $data = [
            'list' => $list,
            'id_barang' => $id_barangs,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanOmzetSalesmanPerBarang/laporanOmzetSalesmanPerBarangPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Omzet Sales Per Barang ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $id_golongan = $request->input('id_golongan');
        $id_golongan = $id_golongan ? $id_golongan : 'all';

        $id_barang = $request->input('id_barang');
        $id_barang = $id_barang ? $id_barang : 'all';


        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $list = $penjualan->list_omzet_salesman_per_barang($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_barang, $id_lokasi, $id_golongan);
        $no = $start + 1;

        $id_barangs = DB::table('analisa_penjualan_termasuk_ppn')->groupBy('id_barang')->get();
//        if (!empty($list)) {
//            foreach ($list as $key => $value) {
//                $id_barangs[$key] = ['id_barang' => $value->id_barang];
//            }
//        }
        $data = [
            'list' => $list,
            'id_barang' => $id_barangs,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanOmzetSalesmanPerBarang/laporanOmzetSalesmanPerBarangExcel', $data);
    }
}
