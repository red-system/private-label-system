<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mItemPenjualan;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class DaftarStokOut extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'golongan' => ["search_field" => "golongan"],
        'supplier' => ["search_field" => "supplier"],
        'total_penjualan_barang' => ["search_field" => "supplier"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_11'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_out') {
                    $data['uri'] = '?' . $key . '=' . $value;
                } else {
                    if ($key == 'id_golongan') {
                        foreach ($value as $golongan) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $golongan;
                        }
                    } elseif ($key == 'id_lokasi'){
                        foreach ($value as $lokasi){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$lokasi;
                        }
                    } else {
                        $data['uri'] .= '&' . $key . '=' . $value;
                    }
                }
            }
        }

        $data['date_out'] = $request->input('date_out') ? $request->input('date_out') : date('d-m-Y');
        $data['golongan'] = mGolongan::all();
        $data['select_golongan'] = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;

//        dd(($data['select_golongan']));

        return view('laporan/daftarStokOut/daftarStokOutList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $date_out = $request->input('date_out') ? $request->input('date_out') : date('d-m-Y');
//        $tanggal_out = Main::format_date_db($date_out);
        $golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

//        dd($lokasi);
        $item_penjualan = new mItemPenjualan();

        $result['iTotalRecords'] = count($item_penjualan->count_all($date_out, $golongan, $lokasi, $supplier));
        $result['iTotalDisplayRecords'] = count($item_penjualan->count_filter($query, $this->view, $date_out, $golongan, $lokasi, $supplier));
//        dd($result['iTotalDisplayRecords']);
//        dd($golongan);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $item_penjualan->list($start, $length, $query, $this->view, $date_out, $golongan, $lokasi, $supplier);
//        $data = $item_penjualan->take_all();
//        dd($data);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->created_at = Main::format_date_label($value->created_at);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $item_penjualan = new mItemPenjualan();

        $date_out = $request->input('date_out') ? $request->input('date_out') : date('d-m-Y');
        $tanggal_out = Main::format_date_db($date_out);
        $golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $list = $item_penjualan->export($tanggal_out, $golongan, $lokasi, $supplier);

        foreach ($list as $value) {
            $value->created_at = Main::format_date_label($value->created_at);
        }

        if (!empty($lokasi)) {
            $nama_lokasi = mLokasi::where('id', $lokasi)->first()->lokasi;
        } else {
            $nama_lokasi = 'Seluruh Lokasi';
        }

//        dd($nama_lokasi);
        $data = array_merge($data, [
            'list' => $list,
            'tanggal_out' => $tanggal_out,
            'lokasi' => $nama_lokasi,
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/daftarStokOut/daftarStokOutExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $item_penjualan = new mItemPenjualan();

        $date_out = $request->input('date_out') ? $request->input('date_out') : date('d-m-Y');
        $tanggal_out = Main::format_date_db($date_out);
        $golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $list = $item_penjualan->export($tanggal_out, $golongan, $lokasi, $supplier);

        foreach ($list as $value) {
            $value->created_at = Main::format_date_label($value->created_at);
        }

        if (!empty($lokasi)) {
            $nama_lokasi = mLokasi::where('id', $lokasi)->first()->lokasi;
        } else {
            $nama_lokasi = 'Seluruh Lokasi';
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_out' => $tanggal_out,
            'lokasi' => $nama_lokasi,
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/daftarStokOut/daftarStokOutPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Daftar Stok Out ' . date('d-m-Y'));
    }
}
