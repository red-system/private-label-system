<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class LaporanAnalisaUmurPenjualan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'sebelumnya' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'minggu_pertama' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'minggu_kedua' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'minggu_ketiga' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'seterusnya' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'jumlah' => ["search_field" => "tb_penjualan.grand_total_penjualan"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_analisa_penjualan_11'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date($date);

        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'date' => $date,
        ];
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['params'] = $params;
        $data['date'] = $date;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanAnalisaUmurPenjualan/laporanAnalisaUmurPenjualanList', $data);
    }

    function list(Request $request)
    {
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date_db($date);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_umur_penjualan()->count();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_umur_penjualan($query, $this->detail, $date)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_umur_penjualan($start, $length, $query, $this->detail, $date);

        foreach ($data as $value) {
            if ($value->sebelumnya == 0) {
                $value->sebelumnya = null;
            } else {
                $value->sebelumnya = Main::format_money($value->sebelumnya);
            }
            if ($value->minggu_pertama == 0) {
                $value->minggu_pertama = null;
            } else {
                $value->minggu_pertama = Main::format_money($value->minggu_pertama);
            }
            if ($value->minggu_kedua == 0) {
                $value->minggu_kedua = null;
            } else {
                $value->minggu_kedua = Main::format_money($value->minggu_kedua);
            }
            if ($value->minggu_ketiga == 0) {
                $value->minggu_ketiga = null;
            } else {
                $value->minggu_ketiga = Main::format_money($value->minggu_ketiga);
            }
            if ($value->seterusnya == 0) {
                $value->seterusnya = null;
            } else {
                $value->seterusnya = Main::format_money($value->seterusnya);
            }
            if ($value->jumlah == 0) {
                $value->jumlah = null;
            } else {
                $value->jumlah = Main::format_money($value->jumlah);
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date($date);
        $date_db = Main::format_date_db($date);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $list = $penjualan->list_umur_penjualan($start, $length, $query, $this->detail, $date_db);

        foreach ($list as $value) {
            if ($value->sebelumnya == 0) {
                $value->sebelumnya = null;
            } else {
                $value->sebelumnya = Main::format_money($value->sebelumnya);
            }
            if ($value->minggu_pertama == 0) {
                $value->minggu_pertama = null;
            } else {
                $value->minggu_pertama = Main::format_money($value->minggu_pertama);
            }
            if ($value->minggu_kedua == 0) {
                $value->minggu_kedua = null;
            } else {
                $value->minggu_kedua = Main::format_money($value->minggu_kedua);
            }
            if ($value->minggu_ketiga == 0) {
                $value->minggu_ketiga = null;
            } else {
                $value->minggu_ketiga = Main::format_money($value->minggu_ketiga);
            }
            if ($value->seterusnya == 0) {
                $value->seterusnya = null;
            } else {
                $value->seterusnya = Main::format_money($value->seterusnya);
            }
            if ($value->jumlah == 0) {
                $value->jumlah = null;
            } else {
                $value->jumlah = Main::format_money($value->jumlah);
            }
        }

        $data = [
            'list' => $list,
            'date' => $date,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanAnalisaUmurPenjualan/laporanAnalisaUmurPenjualanPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Analisa Umur Penjualan ' . $date);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date($date);
        $date_db = Main::format_date_db($date);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $list = $penjualan->list_umur_penjualan($start, $length, $query, $this->detail, $date_db);

        foreach ($list as $value) {
            if ($value->sebelumnya == 0) {
                $value->sebelumnya = null;
            } else {
                $value->sebelumnya = Main::format_money($value->sebelumnya);
            }
            if ($value->minggu_pertama == 0) {
                $value->minggu_pertama = null;
            } else {
                $value->minggu_pertama = Main::format_money($value->minggu_pertama);
            }
            if ($value->minggu_kedua == 0) {
                $value->minggu_kedua = null;
            } else {
                $value->minggu_kedua = Main::format_money($value->minggu_kedua);
            }
            if ($value->minggu_ketiga == 0) {
                $value->minggu_ketiga = null;
            } else {
                $value->minggu_ketiga = Main::format_money($value->minggu_ketiga);
            }
            if ($value->seterusnya == 0) {
                $value->seterusnya = null;
            } else {
                $value->seterusnya = Main::format_money($value->seterusnya);
            }
            if ($value->jumlah == 0) {
                $value->jumlah = null;
            } else {
                $value->jumlah = Main::format_money($value->jumlah);
            }
        }

        $data = [
            'list' => $list,
            'date' => $date,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanAnalisaUmurPenjualan/laporanAnalisaUmurPenjualanExcel', $data);
    }
}
