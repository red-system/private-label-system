<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class MutasiLangganan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_langganan' => ["search_field" => "kode_langganan"],
        'langganan' => ["search_field" => "langganan"],
        'saldo_awal' => ["search_field" => "saldo_awal"],
        'penjualan' => ["search_field" => "penjualan"],
        'bayar' => ["search_field" => "bayar"],
        'retur' => ["search_field" => "retur"],
        'sisa' => ["search_field" => "retur"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_langganan_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['url'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['url'] = '?' . $key . '=' . $value;
                } else {
                    $data['url'] .= '&' . $key . '=' . $value;
                }
            }
        }
//        dd($data['url']);
        $params = [
            'date_start' => $request->input('date_start') ? $request->input('date_start') : Main::format_date(now()),
            'date_end' => $request->input('date_end') ? $request->input('date_end') : Main::format_date(now()),
            'id_lokasi' => $request->input('id_lokasi') ? $request->input('id_lokasi') : 'all'
        ];
        $data['params'] = $params;
        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : Main::format_date(now());
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : Main::format_date(now());
        $data['lokasi'] = mLokasi::all();
        $data['id_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : 'all';


        return view('laporan/laporanMutasiLangganan/laporanMutasiLanggananList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start = $date_start ? $date_start : mPiutangLangganan::orderBy('created_at', 'ASC')->value('created_at');
        $date_start = Main::format_date_db($date_start);
        $date_end = $date_end ? $date_end : mPiutangLangganan::orderBy('created_at', 'DESC')->value('created_at');
        $date_end = Main::format_date_db($date_end);
        $id_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : 'all';

//        dd($lokasi);

        $piutang_langganan = new mPiutangLangganan();

        $result['iTotalRecords'] = $piutang_langganan->count_all_mutasi_langganan()->count();
        $result['iTotalDisplayRecords'] = count($piutang_langganan->count_filter_mutasi_langganan($query, $this->view, $date_start, $date_end, $id_lokasi));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $piutang_langganan->list_mutasi_langganan($start, $length, $query, $this->view, $date_start, $date_end, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $jumlah_awal_beli = $value->saldo_awal + $value->penjualan;
            $value->sisa = $jumlah_awal_beli - $value->bayar - $value->retur;

            $value->saldo_awal = Main::format_money($value->saldo_awal);
            $value->penjualan = Main::format_money($value->penjualan);
            $value->retur = Main::format_money($value->retur);
            $value->bayar = Main::format_money($value->bayar);
            $value->sisa = Main::format_money($value->sisa);
        }


//        dd($masuk_data);
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_barang = $request->input('id_barang');
        $id_lokasi = $request->input('id_lokasi');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start_db = Main::format_date_db($date_start);
        $date_end_db = Main::format_date_db($date_end);

        $piutang = new mPiutangLangganan();
        $list = $piutang->list_mutasi_langganan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $id_lokasi);
        foreach ($list as $value) {
            $jumlah_awal_beli = $value->saldo_awal + $value->penjualan;
            $value->sisa = $jumlah_awal_beli - $value->bayar;

            $value->saldo_awal = Main::format_money($value->saldo_awal);
            $value->penjualan = Main::format_money($value->penjualan);
            $value->retur = Main::format_money($value->retur);
            $value->bayar = Main::format_money($value->bayar);
            $value->sisa = Main::format_money($value->sisa);
        }
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('lokasi', $id_lokasi)->first();
        } else {
            $lokasi = 'Semua Lokasi';
        }


        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanMutasiLangganan/laporanMutasiLanggananPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Mutasi Langganan '  .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_barang = $request->input('id_barang');
        $id_lokasi = $request->input('id_lokasi');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start_db = Main::format_date_db($date_start);
        $date_end_db = Main::format_date_db($date_end);

        $piutang = new mPiutangLangganan();
        $list = $piutang->list_mutasi_langganan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $id_lokasi);
        foreach ($list as $value) {
            $jumlah_awal_beli = $value->saldo_awal + $value->penjualan;
            $value->sisa = $jumlah_awal_beli - $value->bayar;

            $value->saldo_awal = Main::format_money($value->saldo_awal);
            $value->penjualan = Main::format_money($value->penjualan);
            $value->retur = Main::format_money($value->retur);
            $value->bayar = Main::format_money($value->bayar);
            $value->sisa = Main::format_money($value->sisa);
        }
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('lokasi', $id_lokasi)->first();
        } else {
            $lokasi = 'Semua Lokasi';
        }


        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];
        return view('laporan/laporanMutasiLangganan/laporanMutasiLanggananExcel', $data);
    }
}
