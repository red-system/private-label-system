<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class KartuLangganan extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'tgl_terbayar' => ["search_field" => "tb_piutang_langganan.updated_at"],
        'grand_total_penjualan' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'total_retur' => ["search_field" => "tb_retur_penjualan.total_retur"],
        'pajak_nominal_penjualan' => ["search_field" => "tb_penjualan.pajak_nominal_penjualan"],
        'terbayar' => ["search_field" => "tb_piutang_langganan.terbayar"],
        'sisa_piutang' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],
        'keterangan_penjualan' => ["search_field" => "tb_penjualan.keterangan_penjualan"],
    ];
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_langganan_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['url'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['url'] = '?' . $key . '=' . $value;
                } else {
                    $data['url'] .= '&' . $key . '=' . $value;

                }
            }
        }

        $langganan = mLangganan::orderBy('langganan', 'ASC')->get();

        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_langganan' => $id_langganan
        ];

        $data['tab'] = 'trasfer';
        $data['langganan'] = $langganan;
        $data['id_langganan'] = $id_langganan;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['view'] = json_encode($view);
        return view('laporan/kartuLangganan/kartuLanggananList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_langganan = $request->input('id_langganan');

        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end_db = Main::format_date_db($date_end);

        $id_langganan = $id_langganan ? $id_langganan : 'all';

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $kartu_langganan = new mPenjualan();
        $result['iTotalRecords'] = $kartu_langganan->count_all_kartu_langganan();
        $result['iTotalDisplayRecords'] = $kartu_langganan->count_filter_kartu_langganan($query, $this->view, $id_langganan, $date_start_db, $date_end_db);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $kartu_langganan->list_kartu_langganan($start, $length, $query, $this->view, $id_langganan, $date_start_db, $date_end_db);
//        dd($data);
        foreach ($data as $value) {

            $value->tgl_terbayar = Main::format_date($value->tgl_terbayar);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);

            $value->total_retur = $value->total_retur ? Main::format_money($value->total_retur) : null;

            if ($value->pajak_nominal_penjualan != 0) {
                $value->pajak_nominal_penjualan = Main::format_money($value->pajak_nominal_penjualan);
            } else {
                $value->pajak_nominal_penjualan = null;
            }

            $value->sisa_piutang = Main::format_money($value->sisa_piutang);

            $value->grand_total_penjualan = Main::format_money($value->grand_total_penjualan);

            $value->terbayar = Main::format_money($value->terbayar);

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_langganan = $request->input('id_langganan');

        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end_db = Main::format_date_db($date_end);

        $id_langganan = $id_langganan ? $id_langganan : 'all';

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $kartu_langganan = new mPenjualan();

        $list = $kartu_langganan->list_kartu_langganan($start, $length, $query, $this->view, $id_langganan, $date_start_db, $date_end_db);
        foreach ($list as $value) {

            $value->tgl_terbayar = Main::format_date($value->tgl_terbayar);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);

            $value->total_retur = $value->total_retur ? $value->total_retur : null;
            $value->pajak_nominal_penjualan = !0 ? $value->pajak_nominal_penjualan : null;


        }
        if ($id_langganan == 'all') {
            $langganan = DB::table('tb_penjualan')->select('tb_penjualan.id_langganan', 'tb_langganan.langganan')
                ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
                ->groupBy('tb_penjualan.id_langganan')->get();
        } else {
            $langganan = mLangganan::find($id_langganan);
        }
        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'langganan' => $langganan,
            'company' => Main::companyInfo()
        ];

        if ($id_langganan != 'all') {
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('laporan/kartuLangganan/kartuLanggananPdf', $data);
        } else {
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('laporan/kartuLangganan/kartuLanggananAllPdf', $data);
        }

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Kartu Langganan ' . $date_start . ' s/d ' . $date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_langganan = $request->input('id_langganan');

        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end_db = Main::format_date_db($date_end);

        $id_langganan = $id_langganan ? $id_langganan : 'all';

        if ($id_langganan != 'all') {
            $langganan = mLangganan::find($id_langganan);
        } else {
            $langganan = DB::table('tb_penjualan')->select('tb_penjualan.id_langganan', 'tb_langganan.langganan')
                ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
                ->groupBy('tb_penjualan.id_langganan')->get();
        }

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $kartu_langganan = new mPenjualan();

        $list = $kartu_langganan->list_kartu_langganan($start, $length, $query, $this->view, $id_langganan, $date_start_db, $date_end_db);
        foreach ($list as $value) {

            $value->tgl_terbayar = Main::format_date($value->tgl_terbayar);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->total_retur = $value->total_retur ? $value->total_retur : null;
            $value->pajak_nominal_penjualan = !0 ? $value->pajak_nominal_penjualan : null;
        }

        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'langganan' => $langganan,
            'company' => Main::companyInfo()
        ];

        if ($id_langganan != 'all') {
            return view('laporan/kartuLangganan/kartuLanggananExcel', $data);
        } else {
            return view('laporan/kartuLangganan/kartuLanggananAllExcel', $data);
        }
    }
}
