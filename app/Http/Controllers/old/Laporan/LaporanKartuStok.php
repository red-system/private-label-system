<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mKartuStok;
use app\Models\Widi\mLokasi;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class LaporanKartuStok extends Controller
{
    private $breadcrumb;
    private $cons;
    private $view = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'saldo_awal' => ["search_field" => "saldo_awal"],
        'hpp_awal' => ["search_field" => "hpp_awal"],
        'total_masuk' => ["search_field" => "tb_kartu_stok.stok_masuk"],
        'total_keluar' => ["search_field" => "tb_kartu_stok.stok_keluar"],
        'saldo_akhir' => ["search_field" => "saldo_akhir"],
        'hpp_akhir' => ["search_field" => "hpp_akhir"],

    ];

    private $view_awal = [
        'saldo_awal' => ["search_field" => "saldo_awal"],
        'hpp_awal' => ["search_field" => "hpp_awal"],
    ];

    private $view_akhir = [
        'saldo_akhir' => ["search_field" => "saldo_akhir"],
        'hpp_akhir' => ["search_field" => "hpp_akhir"],
    ];

    private $view_keluar_masuk = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'total_masuk' => ["search_field" => "tb_kartu_stok.stok_masuk"],
        'total_keluar' => ["search_field" => "tb_kartu_stok.stok_keluar"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $dataMain = Main::data($this->breadcrumb);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : mKartuStok::orderBy('tgl_transit', 'ASC')->value('tgl_transit');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mKartuStok::orderBy('tgl_transit', 'DESC')->value('tgl_transit');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db
        ];

        $data = Main::data($this->breadcrumb);
        $view = array();
//        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);

        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
        ];
        $data['view'] = json_encode($view);

        $data = array_merge($dataMain, $data);

        return view('laporan/laporanKartuStok/laporanKartuStokList', $data);
    }
    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : mKartuStok::orderBy('tgl_transit', 'ASC')->value('tgl_transit');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mKartuStok::orderBy('tgl_transit', 'DESC')->value('tgl_transit');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mKartuStok();
        $list = $mstok->list_mutasi($start, $length, $query, $this->view, $this->view_awal, $this->view_akhir, $this->view_keluar_masuk, $date_start_db, $date_end_db);

        $result['iTotalRecords'] = mKartuStok::groupBy('id_stok')->get()->count();
        $result['iTotalDisplayRecords'] = count($list);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $list;

        foreach ($data as $value){
            $value->hpp_awal = Main::format_money($value->hpp_awal);
            $value->hpp_akhir = Main::format_money($value->hpp_akhir);
            $value->saldo_akhir = Main::format_decimal($value->saldo_akhir).' '.$value->satuan;
            $value->saldo_awal = Main::format_decimal($value->saldo_awal).' '.$value->satuan;
            $value->total_masuk = Main::format_decimal($value->total_masuk).' '.$value->satuan;
            $value->total_keluar = Main::format_decimal($value->total_keluar).' '.$value->satuan;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : mKartuStok::orderBy('tgl_transit', 'ASC')->value('tgl_transit');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mKartuStok::orderBy('tgl_transit', 'DESC')->value('tgl_transit');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mKartuStok();
        $laporan = $mstok->list_mutasi($start, $length, $query, $this->view, $this->view_awal, $this->view_akhir, $this->view_keluar_masuk, $date_start_db, $date_end_db);
        $id_barang = mKartuStok::groupBy('id_barang')->get();

        $data = [
            'laporan' => $laporan,
            'id_barang' => $id_barang,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanKartuStok/laporanKartuStokPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Kartu Stok ' . $date_start . 's/d' . $date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : mKartuStok::orderBy('tgl_transit', 'ASC')->value('tgl_transit');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mKartuStok::orderBy('tgl_transit', 'DESC')->value('tgl_transit');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mKartuStok();
        $laporan = $mstok->list_mutasi($start, $length, $query, $this->view, $this->view_awal, $this->view_akhir, $this->view_keluar_masuk, $date_start_db, $date_end_db);
        $id_barang = mKartuStok::groupBy('id_barang')->get();

        $data = [
            'laporan' => $laporan,
            'id_barang' => $id_barang,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanKartuStok/laporanKartuStokExcel', $data);
    }
}
