<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanStok extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'jml_barang' => ["search_field" => "tb_stok.jml_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $lokasi = mLokasi::all();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'id_lokasi' => $id_lokasi
        ];

        $data['detail'] = json_encode($detail);
        $data['id_lokasi'] = $id_lokasi;
        $data['params'] = $params;
        $data['lokasi'] = $lokasi;
        return view('laporan/laporanStok/laporanStokList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $stok = new mStok;
        $result['iTotalRecords'] = $stok->count_all();
        $result['iTotalDisplayRecords'] = $stok->count_filter($query, $this->detail, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list($start, $length, $query, $this->detail, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $stok = new mStok;
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $list = $stok->list($start, $length, $query, $this->detail, $id_lokasi);
        if($id_lokasi == 'all'){
            $lokasi = 'Semua Lokasi';
        }
        else{
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }
        $data = [
            'id_lokasi' => $id_lokasi,
            'lokasi' => $lokasi,
            'stok' => $list,
        ];
        $data['company'] = Main::companyInfo();

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanStok/laporanStokPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Stok ' . now());
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $stok = new mStok;
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $list = $stok->list($start, $length, $query, $this->detail, $id_lokasi);
        if($id_lokasi == 'all'){
            $lokasi = 'Semua Lokasi';
        }
        else{
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }
        $data = [
            'id_lokasi' => $id_lokasi,
            'lokasi' => $lokasi,
            'stok' => $list,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanStok/laporanStokExcel', $data);
    }
}
