<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanDetailPenjualan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'salesman' => ["search_field" => "tb_salesman.salesman"],
        'biaya_tambahan' => ["search_field" => "tb_penjualan.pajak_nominal_penjualan"],
        'total_penjualan' => ["search_field" => "tb_penjualan.total_penjualan"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $langganan = mLangganan::orderBy('langganan')->get();
        $lokasi = mLokasi::orderBy('lokasi')->get();
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
            'id_langganan' => $id_langganan,
            'id_lokasi' => $id_lokasi,
        ];
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['id_langganan'] = $id_langganan;
        $data['langganan'] = $langganan;
        $data['lokasi'] = $lokasi;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanDetailPenjualan/laporanDetailPenjualanList', $data);
    }

    function list(Request $request)
    {
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_laporan_penjualan();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_laporan_penjualan($query, $this->detail, $date_start, $date_end, $id_langganan, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_laporan_penjualan($start, $length, $query, $this->detail, $date_start, $date_end, $id_langganan, $id_lokasi);
        foreach ($data as $value) {
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->biaya_tambahan = $value->grand_total_penjualan - $value->total_penjualan;
            $value->biaya_tambahan = Main::format_money($value->biaya_tambahan);
            $value->total_penjualan = Main::format_money($value->total_penjualan);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $list = DB::table('tb_penjualan')
            ->select('tb_penjualan.*', 'tb_lokasi.lokasi', 'tb_langganan.langganan', 'tb_salesman.salesman', 'tb_piutang_langganan.jatuh_tempo')
            ->leftJoin('tb_piutang_langganan', 'tb_penjualan.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_penjualan.no_penjualan', 'ASC')->get();
        if ($id_langganan != 'all') {
            $list = $list->where('id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $list = $list->where('id_lokasi', $id_lokasi);
        }
        $item = DB::table('tb_item_penjualan')
            ->select("tb_item_penjualan.*", 'tb_barang.nama_barang', 'tb_barang.kode_barang')
            ->leftJoin('tb_barang', 'tb_barang.id', '=', 'tb_item_penjualan.id_barang')
            ->get();
        if ($id_langganan != 'all') {
            $langganan = mLangganan::where('id', $id_langganan)->value('langganan');
        } else {
            $langganan = 'Semua Langganan';
        }
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        } else {
            $lokasi = 'Semua Lokasi';
        }

        $data = [
            'list' => $list,
            'item' => $item,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'langganan' => $langganan,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanDetailPenjualan/laporanDetailPenjualanPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Detail Penjualan ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $list = DB::table('tb_penjualan')
            ->select('tb_penjualan.*', 'tb_lokasi.lokasi', 'tb_langganan.langganan', 'tb_salesman.salesman', 'tb_piutang_langganan.jatuh_tempo')
            ->leftJoin('tb_piutang_langganan', 'tb_penjualan.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_penjualan.no_penjualan', 'ASC')->get();
        if ($id_langganan != 'all') {
            $list = $list->where('id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $list = $list->where('id_lokasi', $id_lokasi);
        }
        $item = DB::table('tb_item_penjualan')
            ->select("tb_item_penjualan.*", 'tb_barang.nama_barang', 'tb_barang.kode_barang')
            ->leftJoin('tb_barang', 'tb_barang.id', '=', 'tb_item_penjualan.id_barang')
            ->get();
        if ($id_langganan != 'all') {
            $langganan = mLangganan::where('id', $id_langganan)->value('langganan');
        } else {
            $langganan = 'Semua Langganan';
        }
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        } else {
            $lokasi = 'Semua Lokasi';
        }

        $data = [
            'list' => $list,
            'item' => $item,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'langganan' => $langganan,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanDetailPenjualan/laporanDetailPenjualanExcel', $data);
    }
}

