<?php

namespace app\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\Bayu\mBarang;

use PDF;

class LaporanDaftarHarga extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'harga_jual_barang' => ["search_field" => "tb_barang.harga_jual_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        return view('laporan/laporanDaftarHarga/laporanDaftarHargaList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();
        $result['iTotalRecords'] = $barang->count_all();
        $result['iTotalDisplayRecords'] = $barang->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $barang->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->harga_jual_barang = Main::format_money($value->harga_jual_barang);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);
        $data = Main::data($this->breadcrumb);

        $list = mBarang::all();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo()
        ]);

        return view('laporan/laporanDaftarHarga/laporanDaftarHargaExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $list = mBarang::all();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanDaftarHarga/laporanDaftarHargaPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Daftar Harga Barang ' . date('d-m-Y'));
    }
}
