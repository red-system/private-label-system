<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mSO;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class LaporanDaftarPesanan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_so' => ["search_field" => "tb_so.no_so"],
        'tgl_so' => ["search_field" => "tb_so.tgl_so"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'pelanggan_sok' => ["search_field" => "tb_langganan.langganan"],
        'total_so' => ["search_field" => "tb_so.total_so"],
        'dp_so' => ["search_field" => "tb_so.dp_so"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_so_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }

        $data['detail'] = json_encode($detail);
        return view('laporan/laporanDaftarPesanan/laporanDaftarPesananList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $so = new mSO();
        $result['iTotalRecords'] = $so->count_all_laporan();
        $result['iTotalDisplayRecords'] = $so->count_filter_laporan($query, $this->detail);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $so->list_laporan($start, $length, $query, $this->detail);
        foreach ($data as $value) {
            if ($value->dp_so == 0) {
                $value->dp_so = null;
            } else {
                $value->dp_so = Main::format_money($value->dp_so);
            }
            $value->total_so = Main::format_money($value->total_so);
            $value->tgl_so = Main::format_date($value->tgl_so);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf()
    {
        ini_set("memory_limit", "-1");
        $so = new mSO();
        $query = null;
        $start = null;
        $length = null;
        $list = $so->list_laporan($start, $length, $query, $this->detail);
        foreach ($list as $value) {
            if ($value->dp_so == 0) {
                $value->dp_so = null;
            } else {
                $value->dp_so = Main::format_money($value->dp_so);
            }
            $value->total_so = Main::format_money($value->total_so);
            $value->tgl_so = Main::format_date($value->tgl_so);
        }
        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanDaftarPesanan/laporanDaftarPesananPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Daftar Pesanan ' . date('d-m-Y'));
    }

    function excel()
    {
        ini_set("memory_limit", "-1");
        $so = new mSO();
        $query = null;
        $start = null;
        $length = null;
        $list = $so->list_laporan($start, $length, $query, $this->detail);
        foreach ($list as $value) {
            if ($value->dp_so == 0) {
                $value->dp_so = null;
            } else {
                $value->dp_so = Main::format_money($value->dp_so);
            }
            $value->total_so = Main::format_money($value->total_so);
            $value->tgl_so = Main::format_date($value->tgl_so);
        }
        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanDaftarPesanan/laporanDaftarPesananExcel', $data);
    }
}
