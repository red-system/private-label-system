<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class KartuSupplier extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'no_pembelian_label' => ["search_field" => "no_pembelian_label"],
        'tanggal_pembelian' => ["search_field" => "tanggal_pembelian"],
        'tanggal_jatuh_tempo' => ["search_field" => "tanggal_jatuh_tempo"],
        'tanggal_bayar' => ["search_field" => "tanggal_bayar"],
        'harga' => ["search_field" => "harga"],
        'retur' => ["search_field" => "retur"],
        'pajak_nominal_po' => ["search_field" => "pajak_nominal_po"],
        'bayar_dn_kn' => ["search_field" => "bayar_dn_kn"],
        'sisa_hutang_supplier' => ["search_field" => "sisa_hutang_supplier"],
        'keterangan_pembelian' => ["search_field" => "keterangan_pembelian"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_supplier_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach($all_request as $key => $value){
                if ($key == 'date_start'){
                    $data['uri'] = '?'.$key.'='.$value;
                } else{
                    if ($key == 'id_supplier'){
                        foreach ($value as $supplier){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$supplier;
                        }
                    } else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }
//        dd($data['uri']);
        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        return view('laporan/laporanKartuSupplier/laporanKartuSupplierList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

//        dd($supplier);

        $pembelian_barang = new mPembelianBarang();

        $result['iTotalRecords'] = count($pembelian_barang->count_all_kartu_pembelian($tanggal_start, $tanggal_end, $supplier));
        $result['iTotalDisplayRecords'] = count($pembelian_barang->count_filter_kartu_pembelian($query, $this->view, $tanggal_start, $tanggal_end, $supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $pembelian_barang->list_kartu_pembelian($start, $length, $query, $this->view, $tanggal_start, $tanggal_end, $supplier);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->tanggal_pembelian = Main::format_date_label($value->tanggal_pembelian);
            $value->tanggal_jatuh_tempo = Main::format_date_label($value->tanggal_jatuh_tempo);
            $value->tanggal_bayar = Main::format_date_label($value->tanggal_bayar);
            $value->total_po = Main::format_money($value->total_po);
            $value->qty_return = Main::format_number($value->qty_return);
            $value->harga = Main::format_money($value->harga);
            $value->retur = Main::format_money($value->retur);
            $value->pajak_nominal_po = Main::format_money($value->pajak_nominal_po);
            $value->sisa_hutang_supplier = Main::format_money($value->sisa_hutang_supplier);
            $value->bayar_dn_kn = Main::format_money($value->bayar_dn_kn);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_nilai = 0;
        $total_pajak = 0;
        $total_retur = 0;
        $total_sisa = 0;
        $total_bayar_dn_kn = 0;

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian_barang = new mPembelianBarang();

        $list = $pembelian_barang->export_kartu_pembelian($tanggal_start, $tanggal_end, $supplier);

        if (empty($supplier)){
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_nilai += $value->harga;
            $total_pajak += $value->pajak_nominal_po;
            $total_retur += $value->retur;
            $total_sisa += $value->sisa_hutang_supplier;
            $total_bayar_dn_kn += $value->bayar_dn_kn;
            $value->tanggal_pembelian = Main::format_date_label($value->tanggal_pembelian);
            $value->tanggal_jatuh_tempo = Main::format_date_label($value->tanggal_jatuh_tempo);
            $value->tanggal_bayar = Main::format_date_label($value->tanggal_bayar);
            $value->total_po = Main::format_number($value->total_po);
            $value->qty_return = Main::format_number($value->qty_return);
            $value->harga = Main::format_money($value->harga);
            $value->retur = Main::format_money($value->retur);
            $value->pajak_nominal_po = Main::format_money($value->pajak_nominal_po);
            $value->sisa_hutang_supplier = Main::format_money($value->sisa_hutang_supplier);
            $value->bayar_dn_kn = Main::format_money($value->bayar_dn_kn);
        }

        $data = array_merge($data, [
            'list' => $list,
            'supplier' => $nama_supplier,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'total_nilai' => Main::format_money($total_nilai),
            'total_pajak' => Main::format_money($total_pajak),
            'total_retur' => Main::format_money($total_retur),
            'total_sisa' => Main::format_money($total_sisa),
            'total_bayar_dn_kn' => Main::format_money($total_bayar_dn_kn),
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanKartuSupplier/laporanKartuSupplierExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_nilai = 0;
        $total_retur = 0;
        $total_pajak = 0;
        $total_sisa = 0;
        $total_bayar_dn_kn = 0;

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian_barang = new mPembelianBarang();

        $list = $pembelian_barang->export_kartu_pembelian($tanggal_start, $tanggal_end, $supplier);

        if (empty($supplier)){
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_nilai += $value->harga;
            $total_pajak += $value->pajak_nominal_po;
            $total_retur += $value->retur;
            $total_sisa += $value->sisa_hutang_supplier;
            $total_bayar_dn_kn += $value->bayar_dn_kn;
            $value->tanggal_pembelian = Main::format_date_label($value->tanggal_pembelian);
            $value->tanggal_jatuh_tempo = Main::format_date_label($value->tanggal_jatuh_tempo);
            $value->tanggal_bayar = Main::format_date_label($value->tanggal_bayar);
            $value->total_po = Main::format_number($value->total_po);
            $value->qty_return = Main::format_number($value->qty_return);
            $value->harga = Main::format_money($value->harga);
            $value->retur = Main::format_money($value->retur);
            $value->pajak_nominal_po = Main::format_money($value->pajak_nominal_po);
            $value->sisa_hutang_supplier = Main::format_money($value->sisa_hutang_supplier);
            $value->bayar_dn_kn = Main::format_money($value->bayar_dn_kn);
        }

        $data = array_merge($data, [
            'list' => $list,
            'supplier' => $nama_supplier,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'total_nilai' => Main::format_money($total_nilai),
            'total_pajak' => Main::format_money($total_pajak),
            'total_retur' => Main::format_money($total_retur),
            'total_sisa' => Main::format_money($total_sisa),
            'total_bayar_dn_kn' => Main::format_money($total_bayar_dn_kn),
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanKartuSupplier/laporanKartuSupplierPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Kartu Supplier '. date('d-m-Y'));
    }
}
