<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPiutangLangganan;
use app\Models\Widi\mWilayah;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class NotaTagihanLangganan extends Controller
{
    private $breadcrumb;
    private $view = [
        'kode_langganan' => ["search_field" => "kode_langganan"],
        'langganan' => ["search_field" => "langganan"],
        'wilayah' => ["search_field" => "tb_wilayah.wilayah"],
        'langganan_alamat' => ["search_field" => "langganan_alamat"],
        'langganan_kontak' => ["search_field" => "langganan_kontak"],
    ];
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_langganan_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('laporan/laporanNotaTagihan/laporanNotaTagihanList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $langganan = new mLangganan();
        $result['iTotalRecords'] = $langganan->count_all();
        $result['iTotalDisplayRecords'] = $langganan->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $langganan->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $params = [
                'id_langganan' => $value->id
            ];
            $value->no = $no;
            $no++;
            $value->route['pdf'] = route('notaTagihanLanggananPdf', $params);
            $value->target_blank['pdf'] = 'ada';
            $value->route['excel'] = route('notaTagihanLanggananExcel', $params);
            $value->target_blank['excel'] = 'ada';
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');

        $id_langganan = $id_langganan ? $id_langganan : $id_langganan = 0;
        $langganan = mLangganan::find($id_langganan);
        $user = Session::get('user');
        $lokasi = mLokasi::find($user->id_lokasi);


        $saldo_piutang = new mPiutangLangganan();
        $list = $saldo_piutang->list_nota($id_langganan);

        $data = [
            'list' => $list,
            'langganan' => $langganan,
            'user' => $user,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanNotaTagihan/laporanNotaTagihanPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Tagihan Langganan : ' . $langganan->langganan);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');

        $id_langganan = $id_langganan ? $id_langganan : $id_langganan = 0;
        $langganan = mLangganan::find($id_langganan);
        $user = Session::get('user');
        $lokasi = mLokasi::find($user->id_lokasi);

        $saldo_piutang = new mPiutangLangganan();
        $list = $saldo_piutang->list_nota($id_langganan);

        $data = [
            'list' => $list,
            'langganan' => $langganan,
            'user' => $user,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanNotaTagihan/laporanNotaTagihanExcel', $data);
    }
}
