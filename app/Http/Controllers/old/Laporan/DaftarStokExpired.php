<?php

namespace app\Http\Controllers\Laporan;


use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class DaftarStokExpired extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'golongan' => ["search_field" => "golongan"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'jml_barang' => ["search_field" => "jml_barang"],
        'harga_beli_terakhir_barang' => ["search_field" => "harga_beli_terakhir_barang"],
        'harga_jual_barang' => ["search_field" => "harga_jual_barang"],
        'date_expired' => ["search_field" => "date_expired"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_9'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_expired') {
                    $data['uri'] = '?' . $key . '=' . $value;
                } else {
                    if ($key == 'id_golongan') {
                        foreach ($value as $golongan) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $golongan;
                        }
                    } elseif($key == 'id_lokasi'){
                        foreach ($value as $lokasi){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$lokasi;
                        }
                    }else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }

        $data['date_expired'] = $request->input('date_expired') ? $request->input('date_expired') : date('d-m-Y');
        $data['golongan'] = mGolongan::all();
        $data['select_golongan'] = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        return view('laporan/daftarStokExpired/daftarStokExpiredList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $expired = $request->input('date_expired') ? $request->input('date_expired') : date('d-m-Y');
        $tanggal_expired = Main::format_date_db($expired);
        $golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;
        $stok = new mStok();

        $result['iTotalRecords'] = $stok->count_all_expired( $tanggal_expired, $golongan, $lokasi, $supplier);
        $result['iTotalDisplayRecords'] = $stok->count_filter_expired($query,$this->view, $tanggal_expired, $golongan, $lokasi, $supplier);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $stok->list_expired($start,$length,$query,$this->view, $tanggal_expired, $golongan, $lokasi, $supplier);

        $no = $start +1;
        foreach ($data as $value){
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->harga_beli_terakhir_barang = Main::format_money($value->jml_barang * $value->harga_beli_terakhir_barang);
            $value->harga_jual_barang = Main::format_money($value->jml_barang * $value->harga_jual_barang);
            $value->date_expired = Main::format_date_label($value->date_expired);
        }
        $result['aaData'] = $data;
        
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_saldo = 0;
        $total_beli_barang = 0;
        $total_jual_barang = 0;

        $stok = new mStok();

        $expired = $request->input('date_expired') ? $request->input('date_expired') : date('d-m-Y');
        $tanggal_expired = Main::format_date_db($expired);
        $golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $list = $stok->export_expired($tanggal_expired, $golongan, $lokasi, $supplier);

        foreach ($list as $value) {
            $hitung_beli_barang = $value->harga_beli_terakhir_barang * $value->jml_barang;
            $hitung_jual_barang = $value->harga_jual_barang * $value->jml_barang;
            $total_saldo += $value->jml_barang;
            $total_beli_barang += $hitung_beli_barang;
            $total_jual_barang += $hitung_jual_barang;
            $value->jml_barang = Main::format_number($value->jml_barang);
            $value->harga_beli_terakhir_barang = Main::format_money($hitung_beli_barang);
            $value->harga_jual_barang = Main::format_money($hitung_jual_barang);
            $value->date_expired = Main::format_date_label($value->date_expired);
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_expired' => Main::format_date($tanggal_expired),
            'total_saldo' => Main::format_number($total_saldo),
            'total_beli_barang' => Main::format_money($total_beli_barang),
            'total_jual_barang' => Main::format_money($total_jual_barang),
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/daftarStokExpired/daftarStokExpiredExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_saldo = 0;
        $total_beli_barang = 0;
        $total_jual_barang = 0;

        $stok = new mStok();

        $expired = $request->input('date_expired') ? $request->input('date_expired') : date('d-m-Y');
        $tanggal_expired = Main::format_date_db($expired);
        $golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $list = $stok->export_expired($tanggal_expired, $golongan, $lokasi, $supplier);

        foreach ($list as $value) {
            $hitung_beli_barang = $value->harga_beli_terakhir_barang * $value->jml_barang;
            $hitung_jual_barang = $value->harga_jual_barang * $value->jml_barang;
            $total_saldo += $value->jml_barang;
            $total_beli_barang += $hitung_beli_barang;
            $total_jual_barang += $hitung_jual_barang;
            $value->jml_barang = Main::format_number($value->jml_barang);
            $value->harga_beli_terakhir_barang = Main::format_money($hitung_beli_barang);
            $value->harga_jual_barang = Main::format_money($hitung_jual_barang);
            $value->date_expired = Main::format_date_label($value->date_expired);
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_expired' => Main::format_date($tanggal_expired),
            'total_saldo' => Main::format_number($total_saldo),
            'total_beli_barang' => Main::format_money($total_beli_barang),
            'total_jual_barang' => Main::format_money($total_jual_barang),
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/daftarStokExpired/daftarStokExpiredPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Daftar Stok Expire ' . date('d-m-Y'));
    }
}
