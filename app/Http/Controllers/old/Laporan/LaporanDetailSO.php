<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSO;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanDetailSO extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_so' => ["search_field" => "tb_so.no_so"],
        'tgl_so' => ["search_field" => "tb_so.tgl_so"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'biaya_tambahan' => ["search_field" => "tb_so.pajak_nominal_so"],
        'grand_total_so' => ["search_field" => "tb_so.grand_total_so"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_so_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mSO::orderBy('tgl_so', 'ASC')->value('tgl_so');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mSO::orderBy('tgl_so', 'DESC')->value('tgl_so');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $langganan = mLangganan::orderBy('langganan')->get();
        $lokasi = mLokasi::orderBy('lokasi')->get();
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
            'id_langganan' => $id_langganan,
            'id_lokasi' => $id_lokasi,
        ];
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['id_langganan'] = $id_langganan;
        $data['langganan'] = $langganan;
        $data['lokasi'] = $lokasi;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanDetailSO/laporanDetailSOList', $data);
    }

    function list(Request $request)
    {
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mSO::orderBy('tgl_so', 'ASC')->value('tgl_so');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mSO::orderBy('tgl_so', 'DESC')->value('tgl_so');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $so = new mSO();
        $result['iTotalRecords'] = $so->count_all_laporan_so();
        $result['iTotalDisplayRecords'] = $so->count_filter_laporan_so($query, $this->detail, $date_start, $date_end, $id_langganan, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $so->list_laporan_so($start, $length, $query, $this->detail, $date_start, $date_end, $id_langganan, $id_lokasi);
        foreach ($data as $value) {
            $value->tgl_so = Main::format_date($value->tgl_so);
            $value->biaya_tambahan = $value->grand_total_so - $value->total_so;
            $value->biaya_tambahan = Main::format_money($value->biaya_tambahan);
            $value->grand_total_so = Main::format_money($value->grand_total_so);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mSO::orderBy('tgl_so', 'ASC')->value('tgl_so');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mSO::orderBy('tgl_so', 'DESC')->value('tgl_so');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $list = DB::table('tb_so')
            ->select("tb_so.*",
                'tb_lokasi.lokasi', 'tb_langganan.langganan')
            ->leftJoin('tb_lokasi', 'tb_so.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', 'tb_so.id_langganan', '=', 'tb_langganan.id')
            ->whereBetween('tgl_so', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_so.no_so', 'ASC');
        $list->where(function ($q) {
            $q->Where('penjualan', null)->orWhere('penjualan', '=', 'kosong');
        });
        if ($id_langganan != 'all') {
            $list = $list->where('id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $list = $list->where('id_lokasi', $id_lokasi);
        }
        $list = $list->get();
        $item = DB::table('tb_item_so')
            ->select("tb_item_so.*", 'tb_barang.nama_barang', 'tb_barang.kode_barang')
            ->leftJoin('tb_barang', 'tb_barang.id', '=', 'tb_item_so.id_barang')
            ->get();
        if ($id_langganan != 'all') {
            $langganan = mLangganan::where('id', $id_langganan)->value('langganan');
        } else {
            $langganan = 'Semua Langganan';
        }
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        } else {
            $lokasi = 'Semua Lokasi';
        }
        $data = [
            'list' => $list,
            'item' => $item,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'langganan' => $langganan,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanDetailSO/laporanDetailSOPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Detail Sales Order ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mSO::orderBy('tgl_so', 'ASC')->value('tgl_so');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mSO::orderBy('tgl_so', 'DESC')->value('tgl_so');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $list = DB::table('tb_so')
            ->select("tb_so.*",
                'tb_lokasi.lokasi', 'tb_langganan.langganan')
            ->leftJoin('tb_lokasi', 'tb_so.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', 'tb_so.id_langganan', '=', 'tb_langganan.id')
            ->whereBetween('tgl_so', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_so.no_so', 'ASC');
        $list->where(function ($q) {
            $q->Where('penjualan', null)->orWhere('penjualan', '=', 'kosong');
        });
        if ($id_langganan != 'all') {
            $list = $list->where('id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $list = $list->where('id_lokasi', $id_lokasi);
        }
        $list = $list->get();
        $item = DB::table('tb_item_so')
            ->select("tb_item_so.*", 'tb_barang.nama_barang', 'tb_barang.kode_barang')
            ->leftJoin('tb_barang', 'tb_barang.id', '=', 'tb_item_so.id_barang')
            ->get();
        if ($id_langganan != 'all') {
            $langganan = mLangganan::where('id', $id_langganan)->value('langganan');
        } else {
            $langganan = 'Semua Langganan';
        }
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        } else {
            $lokasi = 'Semua Lokasi';
        }
        $data = [
            'list' => $list,
            'item' => $item,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'langganan' => $langganan,
            'lokasi' => $lokasi,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanDetailSO/laporanDetailSOExcel', $data);
    }
}
