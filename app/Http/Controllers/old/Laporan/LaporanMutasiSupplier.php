<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanMutasiSupplier extends Controller
{

    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_supplier'=>["search_field"=>"mutasiSupplier.kode_supplier"],
        'supplier'=>["search_field"=>"mutasiSupplier.supplier"],
        'saldo_awal'=>["search_field"=>"saldo_awal"],
        'pembelian'=>["search_field"=>"pembelian"],
        'bayar'=>["search_field"=>"bayar"],
        'retur'=>["search_field"=>"retur"],
        'sisa'=>['search_field'=>"retur"]
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_supplier_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach($all_request as $key => $value){
                if ($key == 'date_start'){
                    $data['uri'] = '?'.$key.'='.$value;
                } else{
                    if ($key == 'id_wilayah'){
                        foreach ($value as $wilayah){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$wilayah;
                        }
                    } elseif ($key == 'id_supplier'){
                        foreach ($value as $supplier){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$supplier;
                        }
                    } else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }
//        dd($data['uri']);
        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['wilayah'] = mWilayah::all();
        $data['select_wilayah'] = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;


        return view('laporan/laporanMutasiSupplier/laporanMutasiSupplierList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);
        $wilayah = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mHutangSupplier();

        $result['iTotalRecords'] = count($hutang_supplier->count_all_mutasi_supplier( $tanggal_start, $tanggal_end, $wilayah, $supplier));
        $result['iTotalDisplayRecords'] = count($hutang_supplier->count_filter_mutasi_supplier($query,$this->view, $tanggal_start, $tanggal_end, $wilayah, $supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $hutang_supplier->list_mutasi_supplier($start,$length,$query,$this->view, $tanggal_start, $tanggal_end, $wilayah, $supplier);

        $no = $start +1;
        foreach ($data as $value){
            $jumlah_awal_beli = $value->saldo_awal + $value->pembelian;
            $jumlah_pembelian_retur = $value->bayar + $value->retur;
            $value->sisa = $jumlah_awal_beli - $jumlah_pembelian_retur;
            $value->saldo_awal = Main::format_money($value->saldo_awal);
            $value->pembelian = Main::format_money($value->pembelian);
            $value->bayar = Main::format_money($value->bayar);
            $value->retur = Main::format_money($value->retur);
            $value->sisa = Main::format_money($value->sisa);
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_saldo_awal = 0;
        $total_pembelian = 0;
        $total_bayar = 0;
        $total_retur = 0;
        $total_sisa = 0;

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);
        $wilayah = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mHutangSupplier();


        $list = $hutang_supplier->export_mutasi_supplier($tanggal_start, $tanggal_end, $wilayah, $supplier);
//        dd($list);
        if (empty($wilayah)) {
            $nama_wilayah = 'Seluruh Wilayah';
        } else {
            $nama_wilayah = array();
            foreach ($wilayah as $value){
                if ($value == 0){
                    array_push($nama_wilayah,'Seluruh Wilayah');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_wilayah, $nama);
                }
            }
        }

        if (empty($supplier)){
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $jumlah_awal_beli = $value->saldo_awal + $value->pembelian;
            $jumlah_pembelian_retur = $value->bayar + $value->retur;
            $value->sisa = $jumlah_awal_beli - $jumlah_pembelian_retur;
            $total_saldo_awal += $value->saldo_awal;
            $total_pembelian += $value->pembelian;
            $total_bayar += $value->bayar;
            $total_retur += $value->retur;
            $total_sisa += $value->sisa;
            $value->saldo_awal = Main::format_money($value->saldo_awal);
            $value->pembelian = Main::format_money($value->pembelian);
            $value->bayar = Main::format_money($value->bayar);
            $value->retur = Main::format_money($value->retur);
            $value->sisa = Main::format_money($value->sisa);
        }

        $data = array_merge($data, [
            'list' => $list,
            'wilayah' => $wilayah,
            'total_saldo_awal' => Main::format_money($total_saldo_awal),
            'total_pembelian' => Main::format_money($total_pembelian),
            'total_bayar' => Main::format_money($total_bayar),
            'total_retur' => Main::format_money($total_retur),
            'total_sisa' => Main::format_money($total_sisa),
            'tanggal_dari' => $start_date,
            'tanggal_sampai' => $end,
            'nama_wilayah' => $nama_wilayah ? $nama_wilayah : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanMutasiSupplier/laporanMutasiSupplierExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_saldo_awal = 0;
        $total_pembelian = 0;
        $total_bayar = 0;
        $total_retur = 0;
        $total_sisa = 0;

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);
        $wilayah = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mHutangSupplier();

        $list = $hutang_supplier->export_mutasi_supplier($tanggal_start, $tanggal_end, $wilayah, $supplier);

        if (empty($wilayah)){
            $nama_wilayah = 'Seluruh Wilayah';
        } else {
            $nama_wilayah = array();
            foreach ($wilayah as $value){
                if ($value == 0){
                    array_push($nama_wilayah,'Seluruh Wilayah');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_wilayah, $nama);
                }
            }
        }

        if (empty($supplier)){
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $jumlah_awal_beli = $value->saldo_awal + $value->pembelian;
            $jumlah_pembelian_retur = $value->bayar + $value->retur;
            $value->sisa = $jumlah_awal_beli - $jumlah_pembelian_retur;
            $total_saldo_awal += $value->saldo_awal;
            $total_pembelian += $value->pembelian;
            $total_bayar += $value->bayar;
            $total_retur += $value->retur;
            $total_sisa += $value->sisa;
            $value->saldo_awal = Main::format_money($value->saldo_awal);
            $value->pembelian = Main::format_money($value->pembelian);
            $value->bayar = Main::format_money($value->bayar);
            $value->retur = Main::format_money($value->retur);
            $value->sisa = Main::format_money($value->sisa);
        }

        $data = array_merge($data, [
            'list' => $list,
            'wilayah' => $wilayah,
            'total_saldo_awal' => Main::format_money($total_saldo_awal),
            'total_pembelian' => Main::format_money($total_pembelian),
            'total_bayar' => Main::format_money($total_bayar),
            'total_retur' => Main::format_money($total_retur),
            'total_sisa' => Main::format_money($total_sisa),
            'tanggal_dari' => $start_date,
            'tanggal_sampai' => $end,
            'nama_wilayah' => $nama_wilayah ? $nama_wilayah : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanMutasiSupplier/laporanMutasiSupplierPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Mutasi Supplier ' . $start_date.'-'.$end);
    }
}
