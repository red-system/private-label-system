<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class LaporanOmzetSalesmanPerFaktur extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'salesman' => ["search_field" => "tb_salesman.salesman"],
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'grand_total_penjualan' => ["search_field" => "tb_penjualan.grand_total_penjualan"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_omzet_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);

        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_lokasi' => $id_lokasi,
        ];
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['params'] = $params;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanOmzetSalesmanPerFaktur/laporanOmzetSalesmanPerFakturList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_omzet_salesman_per_faktur();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_omzet_salesman_per_faktur($query, $this->detail, $date_start, $date_end, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_omzet_salesman_per_faktur($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        foreach ($data as $value) {
            $value->grand_total_penjualan = Main::format_money($value->grand_total_penjualan);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $list = $penjualan->list_omzet_salesman_per_faktur($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_lokasi);

        foreach ($list as $value) {
            $value->grand_total_penjualan = Main::format_money($value->grand_total_penjualan);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
        }

        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanOmzetSalesmanPerFaktur/laporanOmzetSalesmanPerFakturPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Omzet Salesman Per Faktur ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $list = $penjualan->list_omzet_salesman_per_faktur($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_lokasi);

        foreach ($list as $value) {
            $value->grand_total_penjualan = Main::format_money($value->grand_total_penjualan);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
        }

        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanOmzetSalesmanPerFaktur/laporanOmzetSalesmanPerFakturExcel', $data);
    }
}
