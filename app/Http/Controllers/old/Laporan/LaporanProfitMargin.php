<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanProfitMargin extends Controller
{
    private $breadcrumb;
    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'hpp' => ["search_field" => "tb_barang.hpp"],
        'harga_jual_barang' => ["search_field" => "tb_barang.harga_jual_barang"],
        'margin' => ["search_field" => "tb_barang.harga_jual_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_15'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);

        return view('laporan/laporanProfitMargin/laporanProfitMarginList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();
        $result['iTotalRecords'] = $barang->count_all();
        $result['iTotalDisplayRecords'] = $barang->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $barang->list_margin($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->hpp = Main::format_money($value->hpp);
            $value->harga_jual_barang = Main::format_money($value->harga_jual_barang);
            $value->margin = Main::format_money($value->margin);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf()
    {
        ini_set("memory_limit", "-1");
        $lokasi = DB::table('tb_stok')->select('id_lokasi')->distinct()->get();
        $mbarang = new mBarang();
        $barang = $mbarang->margin();
        $count = $barang->count();
        $data = [
            'lokasi' => $lokasi,
            'barang' => $barang,
            'count' => $count,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanProfitMargin/laporanProfitMarginPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Profit Margin ' . now());
    }

    function excel()
    {
        ini_set("memory_limit", "-1");
        $lokasi = DB::table('tb_stok')->select('id_lokasi')->distinct()->get();
        $mbarang = new mBarang();
        $barang = $mbarang->margin();
        $count = $barang->count();
        $data = [
            'lokasi' => $lokasi,
            'barang' => $barang,
            'count' => $count,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanProfitMargin/laporanProfitMarginExcel', $data);
    }
}
