<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class SaldoPiutangLangganan extends Controller
{
    private $breadcrumb;
    private $view = [
        'kode_langganan' => ["search_field" => "tb_langganan.kode_langganan"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'wilayah' => ["search_field" => "tb_wilayah.wilayah"],
        'netto' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],
        'netto_rp' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],
    ];
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_langganan_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_langganan = $request->input('id_langganan');

        $id_langganan = $id_langganan ? $id_langganan : $id_langganan = [0];

        $langganan = mLangganan::orderBy('langganan', 'ASC')->get();
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $params = [
            'id_langganan' => $id_langganan
        ];
        $data['params'] = $params;
        $data['langganan'] = $langganan;
        $data['id_langganan'] = $id_langganan;
        $data['view'] = json_encode($view);
        return view('laporan/saldoPiutangLangganan/saldoPiutangList', $data);
    }

    function list(Request $request)
    {
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : $id_langganan = [0];
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $saldo_piutang = new mPiutangLangganan();
        $result['iTotalRecords'] = $saldo_piutang->count_all_saldo_piutang();
        $result['iTotalDisplayRecords'] = $saldo_piutang->count_filter_saldo_piutang($query, $this->view, $id_langganan);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $saldo_piutang->list_saldo_piutang($start, $length, $query, $this->view, $id_langganan);

        foreach ($data as $value) {

            $value->netto = Main::format_money($value->netto);
            $value->netto_rp = Main::format_number_system($value->netto_rp);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');

        $id_langganan = $id_langganan ? $id_langganan : $id_langganan = [0];

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $saldo_piutang = new mPiutangLangganan();
        $list = $saldo_piutang->list_saldo_piutang($start, $length, $query, $this->view, $id_langganan);

        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/saldoPiutangLangganan/saldoPiutangPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Saldo Piutang Langganan ' . date('d-m-Y'));
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');

        $id_langganan = $id_langganan ? $id_langganan : $id_langganan = [0];

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $saldo_piutang = new mPiutangLangganan();
        $list = $saldo_piutang->list_saldo_piutang($start, $length, $query, $this->view, $id_langganan);

        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        return view('laporan/saldoPiutangLangganan/saldoPiutangExcel', $data);
    }
}
