<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanPembayaranTepatWaktu extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_faktur_penjualan' => ["search_field" => "tb_piutang_langganan.no_faktur_penjualan"],
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'updated_at' => ["search_field" => "tb_piutang_langganan.updated_at"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'total_piutang' => ["search_field" => "tb_piutang_langganan.total_piutang"],
        'terbayar' => ["search_field" => "tb_piutang_langganan.terbayar"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_8'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPiutangLangganan::orderBy('jatuh_tempo', 'ASC')->value('jatuh_tempo');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPiutangLangganan::orderBy('jatuh_tempo', 'DESC')->value('jatuh_tempo');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
        ];
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanPembayaranTepatWaktu/laporanPembayaranTepatWaktuList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPiutangLangganan::orderBy('jatuh_tempo', 'ASC')->value('jatuh_tempo');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPiutangLangganan::orderBy('jatuh_tempo', 'DESC')->value('jatuh_tempo');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $result['iTotalRecords'] = $penjualan->count_all_tepat_waktu();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_tepat_waktu($query, $this->detail, $date_start, $date_end);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_tepat_waktu($start, $length, $query, $this->detail, $date_start, $date_end);
        foreach ($data as $value) {
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->updated_at = Main::format_date($value->updated_at);
            $value->total_piutang = Main::format_money($value->total_piutang);
            $value->terbayar = Main::format_money($value->terbayar);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPiutangLangganan::orderBy('jatuh_tempo', 'ASC')->value('jatuh_tempo');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPiutangLangganan::orderBy('jatuh_tempo', 'DESC')->value('jatuh_tempo');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $query = null;
        $start = null;
        $length = null;
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_tepat_waktu($start, $length, $query, $this->detail, $date_start_db, $date_end_db);
        $count = $list->count();
        foreach ($list as $value) {
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->updated_at = Main::format_date($value->updated_at);
            $value->total_piutang = Main::format_money($value->total_piutang);
            $value->terbayar = Main::format_money($value->terbayar);
        }

        $data = [
            'list' => $list,
            'count' => $count,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPembayaranTepatWaktu/laporanPembayaranTepatWaktuPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Pembayaran Tepat Waktu '  .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPiutangLangganan::orderBy('jatuh_tempo', 'ASC')->value('jatuh_tempo');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPiutangLangganan::orderBy('jatuh_tempo', 'DESC')->value('jatuh_tempo');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $query = null;
        $start = null;
        $length = null;
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_tepat_waktu($start, $length, $query, $this->detail, $date_start_db, $date_end_db);
        $count = $list->count();
        foreach ($list as $value) {
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->updated_at = Main::format_date($value->updated_at);
            $value->total_piutang = Main::format_money($value->total_piutang);
            $value->terbayar = Main::format_money($value->terbayar);
        }

        $data = [
            'list' => $list,
            'count' => $count,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanPembayaranTepatWaktu/laporanPembayaranTepatWaktuExcel', $data);
    }
}
