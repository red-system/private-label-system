<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelian;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanAnalisaPembelian extends Controller

{
//    TODO : - ubah perhitungan ppn agar seperti di penjualan
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'golongan' => ["search_field" => "golongan"],
        'barang' => ["search_field" => "barang"],
        'lokasi' => ["search_field" => "lokasi"],
        'qty' => ["search_field" => "qty"],
        'nilai' => ["search_field" => "nilai"],
        'nilai_ppn' => ["search_field" => "nilai_ppn"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_pembelian_5'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['uri'] = '?' . $key . '=' . $value;
                } else {
                    if ($key == 'id_lokasi') {
                        foreach ($value as $lokasi) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $lokasi;
                        }
                    } elseif ($key == 'id_supplier') {
                        foreach ($value as $supplier) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $supplier;
                        }
                    } elseif ($key == 'id_golongan') {
                        foreach ($value as $golongan) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $golongan;
                        }
                    } elseif ($key == 'id_barang') {
                        foreach ($value as $barang) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $barang;
                        }
                    } else {
                        $data['uri'] .= '&' . $key . '=' . $value;
                    }
                }
            }
        }

        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['golongan'] = mGolongan::all();
        $data['select_golongan'] = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;
        $data['barang'] = mBarang::all();
        $data['select_barang'] = $request->input('id_barang') ? $request->input('id_barang') : null;


        return view('laporan/laporanAnalisaPembelian/laporanAnalisaPembelianList', $data);
    }

    function list(Request $request)
    {
        $total_qty = 0;
        $total_nilai = 0;
        $total_nilai_ppn = 0;

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;
        $select_barang = $request->input('id_barang') ? $request->input('id_barang') : null;

        $pembelian = new mPembelian();

        $result['iTotalRecords'] = count($pembelian->count_all_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang));
        $result['iTotalDisplayRecords'] = count($pembelian->count_filter_analisa($query, $this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pembelian->list_analisa($start, $length, $query, $this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $total_qty += $value->qty;
            $total_nilai += $value->nilai;
            $total_nilai_ppn += $value->nilai_ppn;
            $value->nilai = Main::format_money(floatval($value->nilai));
            $value->qty = Main::format_number(floatval($value->qty)) . ' ' . $value->satuan;
            $value->nilai_ppn = Main::format_money(floatval($value->nilai_ppn));
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_qty = 0;
        $total_nilai = 0;
        $total_nilai_ppn = 0;

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;
        $select_barang = $request->input('id_barang') ? $request->input('id_barang') : null;

        $pembelian = new mPembelian();

        $list = $pembelian->export_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value) {
                if ($value == 0) {
                    array_push($nama_lokasi, 'Seluruh Lokasi');
                } else {
                    $nama = mSupplier::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = array();
            foreach ($select_golongan as $value) {
                if ($value == 0) {
                    array_push($nama_golongan, 'Seluruh Golongan');
                } else {
                    $nama = mSupplier::where('id', $value)->value('golongan');
                    array_push($nama_golongan, $nama);
                }
            }
        }

        if (empty($select_barang)) {
            $nama_barang = 'Seluruh Barang';
        } else {
            $nama_barang = array();
            foreach ($select_barang as $value) {
                if ($value == 0) {
                    array_push($nama_barang, 'Seluruh Barang');
                } else {
                    $nama = mBarang::where('id', $value)->value('nama_barang');
                    array_push($nama_barang, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value) {
                if ($value == 0) {
                    array_push($nama_supplier, 'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_qty += $value->qty;
            $total_nilai += $value->nilai;
            $total_nilai_ppn += $value->nilai_ppn;
            $value->nilai = Main::format_money($value->nilai);
            $value->qty = Main::format_number($value->qty) . ' ' . $value->satuan;
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'nama_barang' => $nama_barang ? $nama_barang : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'total_qty' => Main::format_number($total_qty),
            'total_nilai' => Main::format_money($total_nilai),
            'total_nilai_ppn' => Main::format_money($total_nilai_ppn),
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanAnalisaPembelian/laporanAnalisaPembelianExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_qty = 0;
        $total_nilai = 0;
        $total_nilai_ppn = 0;

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;
        $select_barang = $request->input('id_barang') ? $request->input('id_barang') : null;


        $pembelian = new mPembelian();

        $list = $pembelian->export_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value) {
                if ($value == 0) {
                    array_push($nama_lokasi, 'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = array();
            foreach ($select_golongan as $value) {
                if ($value == 0) {
                    array_push($nama_golongan, 'Seluruh Golongan');
                } else {
                    $nama = mGolongan::where('id', $value)->value('golongan');
                    array_push($nama_golongan, $nama);
                }
            }
        }

        if (empty($select_barang)) {
            $nama_barang = 'Seluruh Barang';
        } else {
            $nama_barang = array();
            foreach ($select_barang as $value) {
                if ($value == 0) {
                    array_push($nama_barang, 'Seluruh Barang');
                } else {
                    $nama = mBarang::where('id', $value)->value('nama_barang');
                    array_push($nama_barang, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value) {
                if ($value == 0) {
                    array_push($nama_supplier, 'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $key => $value) {
            $total_qty += $value->qty;
            $total_nilai += $value->nilai;
            $total_nilai_ppn += $value->nilai_ppn;
            $value->nilai = Main::format_money(floatval($value->nilai));
            $value->qty = Main::format_number(floatval($value->qty)) . ' ' . $value->satuan;
            $value->nilai_ppn = Main::format_money(floatval($value->nilai_ppn));
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'nama_barang' => $nama_barang ? $nama_barang : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'total_qty' => Main::format_number($total_qty),
            'total_nilai' => Main::format_money($total_nilai),
            'total_nilai_ppn' => Main::format_money($total_nilai_ppn),
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanAnalisaPembelian/laporanAnalisaPembelianPdf', $data);

//        return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Analisa Pembelian ' . $tanggal_start . 'S/d' . $tanggal_end);
    }
}
