<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class AnalisaPenjualanSemuaLangganan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'kode_langganan' => ["search_field" => "tb_langganan.kode_langganan"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'jml_barang_penjualan' => ["search_field" => "tb_item_penjualan.jml_barang_penjualan"],
        'subtotal' => ["search_field" => "tb_item_penjualan.subtotal"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_analisa_penjualan_7'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
        ];
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/analisaPenjualanSemuaLangganan/analisaPenjualanSemuaLanggananList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_analisa_penjualan_semua_langganan();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_analisa_penjualan_semua_langganan($query, $this->detail, $date_start, $date_end);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_analisa_penjualan_semua_langganan($start, $length, $query, $this->detail, $date_start, $date_end);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->jml_barang_penjualan = Main::format_decimal($value->jml_barang_penjualan).' '.$value->satuan;
            $value->subtotal =  Main::format_money($value->subtotal);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $list = $penjualan->list_analisa_penjualan_semua_langganan($start, $length, $query, $this->detail, $date_start_db, $date_end_db);
        $no = $start + 1;
        foreach ($list as $value) {
            $value->no = $no;
            $no++;
//            $value->jml_barang_penjualan = Main::self::format_money($value->jml_barang_penjualan);
        }
        $id_barang = mItemPenjualan::groupBy('id_barang')->get();

//dd($id_barang);
        $data = [
            'list' => $list,
            'id_barang' => $id_barang,
            'date_start' => $date_start,
            'date_end' => $date_end,
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/analisaPenjualanSemuaLangganan/analisaPenjualanSemuaLanggananPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Analisa Penjualan Semua Langganan ' .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $list = $penjualan->list_analisa_penjualan_semua_langganan($start, $length, $query, $this->detail, $date_start_db, $date_end_db);
        $no = $start + 1;
        foreach ($list as $value) {
            $value->no = $no;
            $no++;
//            $value->jml_barang_penjualan = Main::self::format_money($value->jml_barang_penjualan);
//            $value->subtotal =  Main::format_money($value->subtotal);
        }
        $id_barang = mItemPenjualan::groupBy('id_barang')->get();

//dd($id_barang);
        $data = [
            'list' => $list,
            'id_barang' => $id_barang,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/analisaPenjualanSemuaLangganan/analisaPenjualanSemuaLanggananExcel', $data);
    }
}
