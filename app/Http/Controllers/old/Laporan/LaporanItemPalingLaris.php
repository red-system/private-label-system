<?php

namespace app\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\Bayu\mBarang;

use PDF;

class LaporanItemPalingLaris extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "totalPenjualanPerBarang.kode_barang"],
        'nama_barang' => ["search_field" => "totalPenjualanPerBarang.nama_barang"],
        'golongan' => ["search_field" => "totalPenjualanPerBarang.golongan"],
        'total_jml_barang_penjualan' => ["search_field" => "totalPenjualanPerBarang.total_jml_barang_penjualan"],
        'total_subtotal' => ["search_field" => "totalPenjualanPerBarang.total_subtotal"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_17'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        return view('laporan/laporanItemPalingLaris/laporanItemPalingLarisList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();
        $result['iTotalRecords'] = $barang->count_all_penjualan();
        $result['iTotalDisplayRecords'] = $barang->count_filter_penjualan($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $barang->list_all_penjualan_terbesar($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            if ($value->nama_barang == null){
                $value->nama_barang = $value->nama_barang2;
            }
            if ($value->golongan == null){
                $value->golongan = $value->golongan2;
            }
            $value->total_jml_barang_penjualan = Main::format_number($value->total_jml_barang_penjualan);
            $value->total_subtotal = Main::format_money($value->total_subtotal);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $list = DB::table('totalPenjualanPerBarang')->orderBy('total_jml_barang_penjualan', 'desc')->get();

        foreach ($list as $value) {
            $value->password = '';
            if ($value->nama_barang == null) {
                $value->nama_barang = $value->nama_barang2;
            }
            if ($value->golongan == null) {
                $value->golongan = $value->golongan2;
            }
        }

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
        ]);
        return view('laporan/laporanItemPalingLaris/laporanItemPalingLarisExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $list = DB::table('totalPenjualanPerBarang')->orderBy('total_jml_barang_penjualan', 'desc')->get();

        foreach ($list as $value) {
            $value->password = '';
            if ($value->nama_barang == null) {
                $value->nama_barang = $value->nama_barang2;
            }
            if ($value->golongan == null) {
                $value->golongan = $value->golongan2;
            }
        }

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanItemPalingLaris/laporanItemPalingLarisPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Daftar Penjualan Barang Terlaris ' . date('d-m-Y') . '.pdf');
    }
}
