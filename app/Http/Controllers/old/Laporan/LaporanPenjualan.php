<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Bayu\mLangganan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class laporanPenjualan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'salesman' => ["search_field" => "tb_salesman.salesman"],
        'total_penjualan' => ["search_field" => "tb_penjualan.total_penjualan"],
        'terbayar' => ["search_field" => "tb_piutang_langganan.terbayar"],
        'sisa_piutang' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_langganan' => $id_langganan,
            'id_lokasi' => $id_lokasi,
        ];
        $data['langganan'] = mLangganan::orderBy('langganan', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['params'] = $params;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['id_langganan'] = $id_langganan;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanPenjualan/laporanPenjualanList', $data);
    }

    function list(Request $request)
    {
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_laporan_penjualan();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_laporan_penjualan($query, $this->detail, $date_start, $date_end, $id_langganan, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_laporan_penjualan($start, $length, $query, $this->detail, $date_start, $date_end, $id_langganan, $id_lokasi);
        foreach ($data as $value) {
            if ($value->terbayar == 0) {
                $value->terbayar = null;
            } else {
                $value->terbayar = Main::format_money($value->terbayar);
            }
            $value->total_penjualan = Main::format_money($value->total_penjualan);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $penjualan = new mPenjualan();
        $query = null;
        $start = null;
        $length = null;
        $list = $penjualan->list_laporan_penjualan($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_langganan, $id_lokasi);
        foreach ($list as $value) {
            if ($value->terbayar == 0) {
                $value->terbayar = null;
            } else {
                $value->terbayar = Main::format_money($value->terbayar);
            }
            $value->total_penjualan = Main::format_money($value->total_penjualan);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
        }
        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPenjualan/laporanPenjualanPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Penjualan '  .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_langganan = $request->input('id_langganan');
        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $penjualan = new mPenjualan();
        $query = null;
        $start = null;
        $length = null;
        $list = $penjualan->list_laporan_penjualan($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_langganan, $id_lokasi);
        foreach ($list as $value) {
            if ($value->terbayar == 0) {
                $value->terbayar = null;
            } else {
                $value->terbayar = Main::format_money($value->terbayar);
            }
            $value->total_penjualan = Main::format_money($value->total_penjualan);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
        }
        $data = [
            'list' => $list,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanPenjualan/laporanPenjualanExcel', $data);
    }
}
