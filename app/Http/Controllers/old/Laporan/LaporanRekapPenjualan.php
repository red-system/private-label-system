<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mGolongan;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanRekapPenjualan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'golongan' => ["search_field" => "tb_golongan.golongan"],
        'jumlah_barang' => ["search_field" => "tb_item_penjualan.jml_barang_penjualan"],
        'nilai' => ["search_field" => "tb_item_penjualan.subtotal"],
//        'persentase_nilai' => ["search_field" => "tb_item_penjualan.subtotal"],
        'laba' => ["search_field" => "tb_item_penjualan.subtotal"],
//        'persentase_laba' => ["search_field" => "tb_item_penjualan.subtotal"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
//        $id_langganan = $request->input('id_langganan');
//        $id_langganan = $id_langganan ? $id_langganan : 'all';
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
//        $langganan = mLangganan::orderBy('langganan')->get();
        $lokasi = mLokasi::orderBy('lokasi')->get();
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
//            'id_langganan' => $id_langganan,
            'id_lokasi' => $id_lokasi,
        ];
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
//        $data['id_langganan'] = $id_langganan;
//        $data['langganan'] = $langganan;
        $data['lokasi'] = $lokasi;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanRekapPenjualan/laporanRekapPenjualanList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_rekap_penjualan()->count();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_rekap_penjualan($query, $this->detail, $date_start, $date_end, $id_lokasi)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_rekap_penjualan($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->laba = $value->nilai - $value->hpp_total;
            $value->jumlah_barang = Main::format_decimal($value->jumlah_barang).' '.$value->satuan;
            $value->nilai = Main::format_money($value->nilai);
            $value->laba = Main::format_money($value->laba);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start_db = Main::format_date_db($date_start);
        $date_start = Main::format_date($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end_db = Main::format_date_db($date_end);
        $date_end = Main::format_date($date_end);

        $list = DB::table('tb_item_penjualan')->select("tb_item_penjualan.id", "tb_item_penjualan.satuan_penjualan as satuan", 'tb_golongan.golongan',
            DB::raw('SUM(tb_item_penjualan.jml_barang_penjualan * tb_item_penjualan.harga_penjualan) as nilai,
                SUM(tb_item_penjualan.jml_barang_penjualan * tb_barang.hpp) hpp_total,
                SUM(tb_item_penjualan.jml_barang_penjualan) as jumlah_barang'))
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_item_penjualan.id_penjualan')
            ->leftJoin('tb_barang', 'tb_item_penjualan.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59']);

        $lokasi = 'Semua Lokasi';

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_penjualan.id_lokasi', $id_lokasi);
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }

        $list = $list->groupBy('tb_golongan.id')->get();

        $total_nilai = 0;
        $total_laba = 0;
        $total_barang = 0;
        foreach ($list as $value) {
            $value->laba = $value->nilai - $value->hpp_total;
            $total_nilai = $value->nilai + $total_nilai;
            $total_laba = $value->laba + $total_laba;
            $total_barang = $value->jumlah_barang + $total_barang;
        }


        $data = [
            'list' => $list,
            'lokasi' => $lokasi,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'total_nilai' => $total_nilai,
            'total_laba' => $total_laba,
            'total_barang' => $total_barang,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanRekapPenjualan/laporanRekapPenjualanPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Rekap Penjualan '  .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start_db = Main::format_date_db($date_start);
        $date_start = Main::format_date($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end_db = Main::format_date_db($date_end);
        $date_end = Main::format_date($date_end);

        $list = DB::table('tb_item_penjualan')->select("tb_item_penjualan.id", "tb_item_penjualan.satuan_penjualan as satuan", 'tb_golongan.golongan',
            DB::raw('SUM(tb_item_penjualan.jml_barang_penjualan * tb_item_penjualan.harga_penjualan) as nilai,
                SUM(tb_item_penjualan.jml_barang_penjualan * tb_barang.hpp) hpp_total,
                SUM(tb_item_penjualan.jml_barang_penjualan) as jumlah_barang'))
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', 'tb_item_penjualan.id_penjualan')
            ->leftJoin('tb_barang', 'tb_item_penjualan.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59']);

        $lokasi = 'Semua Lokasi';

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_penjualan.id_lokasi', $id_lokasi);
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }

        $list = $list->groupBy('tb_golongan.id')->get();

        $total_nilai = 0;
        $total_laba = 0;
        $total_barang = 0;
        foreach ($list as $value) {
            $value->laba = $value->nilai - $value->hpp_total;
            $total_nilai = $value->nilai + $total_nilai;
            $total_laba = $value->laba + $total_laba;
            $total_barang = $value->jumlah_barang + $total_barang;
        }

        $total_nilai = abs($total_nilai);
        $total_laba = abs($total_laba);
        $total_barang = abs($total_barang);


        $data = [
            'list' => $list,
            'lokasi' => $lokasi,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'total_nilai' => $total_nilai,
            'total_laba' => $total_laba,
            'total_barang' => $total_barang,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanRekapPenjualan/laporanRekapPenjualanExcel', $data);
    }
}
