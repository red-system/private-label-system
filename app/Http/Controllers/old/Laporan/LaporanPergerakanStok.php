<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mStok;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanPergerakanStok extends Controller
{
    private $breadcrumb;
    private $cons;

    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'po_lokasi' => ["search_field" => "lokasi_po.lokasi"],
        'qty_beli' => ["search_field" => "tb_po_barang.qty"],
        'total_beli' => ["search_field" => "tb_po_barang.sub_total"],
        'so_lokasi' => ["search_field" => "lokasi_so.lokasi"],
        'qty_jual' => ["search_field" => "tb_item_penjualan.jml_barang_penjualan"],
        'total_jual' => ["search_field" => "tb_item_penjualan.subtotal"],
        'tanggal_pembelian' => ["search_field" => "tb_pembelian.tanggal_pembelian"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_7'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['url'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['url'] = '?' . $key . '=' . $value;
                } else {
                    $data['url'] .= '&' . $key . '=' . $value;

                }
            }
        }

        $lokasi = mLokasi::orderBy('lokasi', 'ASC');
        $lokasi = $lokasi->get();

        $id_lokasi = $request->input('id_lokasi');

        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();

        $date_start = Main::format_date($date_start);
        $date_end = Main::format_date($date_end);

        $params = [
            'id_lokasi' => $id_lokasi,
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data['tab'] = 'trasfer';
        $data['lokasi'] = $lokasi;
        $data['id_lokasi'] = $id_lokasi;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['view'] = json_encode($view);
        return view('laporan/laporanPergerakanStok/laporanPergerakanStokList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_lokasi = $request->input('id_lokasi');

        $date_start = $date_start ? $date_start : now();
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : now();
        $date_end_db = Main::format_date_db($date_end);

        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();
        $result['iTotalRecords'] = $barang->count_all_pergerakan()->count();
        $result['iTotalDisplayRecords'] = $barang->count_filter_pergerakan($query, $this->view, $date_start_db, $date_end_db, $id_lokasi)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $barang->list_pergerakan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            if ($id_lokasi != 'all') {
                if ($value->po_id_lokasi != $id_lokasi) {
                    $value->po_lokasi = null;
                    $value->qty_beli = null;
                    $value->total_beli = null;
                };

                if ($value->so_id_lokasi != $id_lokasi) {
                    $value->so_lokasi = null;
                    $value->qty_jual = null;
                    $value->total_jual = null;
                };
            }

            if ($value->tgl_penjualan != null) {
                $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);

                if ($value->qty_jual != null) {
                    $value->qty_jual = Main::format_decimal($value->qty_jual).' '.$value->satuan_penjualan;
                };

                if ($value->total_jual != null) {
                    $value->total_jual = Main::format_money($value->total_jual);
                };
            };

            if ($value->tanggal_pembelian != null) {
                $value->tanggal_pembelian = Main::format_date($value->tanggal_pembelian);
                if ($value->qty_beli != null) {
                    $value->qty_beli = Main::format_decimal($value->qty_beli).' '.$value->satuan_pembelian;
                };

                if ($value->total_beli != null) {
                    $value->total_beli = Main::format_money($value->total_beli);
                };
            };
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_start_db = Main::format_date_db($date_start);

        $date_start = Main::format_date($date_start);

        $date_end = $date_end ? $date_end : now();
        $date_end_db = Main::format_date_db($date_end);
        $date_end = Main::format_date($date_end);

        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $list = new mBarang();
        $barang = $list->list_pergerakan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $id_lokasi);

        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        } else {
            $lokasi = 'Semua Lokasi';
        }

        foreach ($barang as $value) {
            if ($id_lokasi != 'all') {
                if ($value->po_id_lokasi != $id_lokasi) {
                    $value->po_lokasi = null;
                    $value->qty_beli = null;
                    $value->total_beli = null;
                };

                if ($value->so_id_lokasi != $id_lokasi) {
                    $value->so_lokasi = null;
                    $value->qty_jual = null;
                    $value->total_jual = null;
                };
            }

            if ($value->tgl_penjualan != null) {
                $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);

                if ($value->qty_jual != null) {
                    $value->qty_jual = Main::format_decimal($value->qty_jual).' '.$value->satuan_penjualan;
                };

                if ($value->total_jual != null) {
                    $value->total_jual = Main::format_money($value->total_jual);
                };
            };

            if ($value->tanggal_pembelian != null) {
                $value->tanggal_pembelian = Main::format_date($value->tanggal_pembelian);
                if ($value->qty_beli != null) {
                    $value->qty_beli = Main::format_decimal($value->qty_beli).' '.$value->satuan_pembelian;
                };

                if ($value->total_beli != null) {
                    $value->total_beli = Main::format_money($value->total_beli);
                };
            };
        }


        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'lokasi' => $lokasi,
            'barang' => $barang,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPergerakanStok/laporanPergerakanStokPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Pergerakan Stok ' . $date_start . ' s/d ' . $date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_start_db = Main::format_date_db($date_start);

        $date_start = Main::format_date($date_start);

        $date_end = $date_end ? $date_end : now();
        $date_end_db = Main::format_date_db($date_end);
        $date_end = Main::format_date($date_end);

        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $list = new mBarang();
        $barang = $list->list_pergerakan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $id_lokasi);

        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        } else {
            $lokasi = 'Semua Lokasi';
        }

        foreach ($barang as $value) {
            if ($id_lokasi != 'all') {
                if ($value->po_id_lokasi != $id_lokasi) {
                    $value->po_lokasi = null;
                    $value->qty_beli = null;
                    $value->total_beli = null;
                };

                if ($value->so_id_lokasi != $id_lokasi) {
                    $value->so_lokasi = null;
                    $value->qty_jual = null;
                    $value->total_jual = null;
                };
            }

            if ($value->tgl_penjualan != null) {
                $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);

                if ($value->qty_jual != null) {
                    $value->qty_jual = Main::format_decimal($value->qty_jual).' '.$value->satuan_penjualan;
                };

                if ($value->total_jual != null) {
                    $value->total_jual = Main::format_money($value->total_jual);
                };
            };

            if ($value->tanggal_pembelian != null) {
                $value->tanggal_pembelian = Main::format_date($value->tanggal_pembelian);
                if ($value->qty_beli != null) {
                    $value->qty_beli = Main::format_decimal($value->qty_beli).' '.$value->satuan_pembelian;
                };

                if ($value->total_beli != null) {
                    $value->total_beli = Main::format_money($value->total_beli);
                };
            };
        }


        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'lokasi' => $lokasi,
            'barang' => $barang,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanPergerakanStok/laporanPergerakanStokExcel', $data);
    }
}
