<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mGolongan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\Bayu\mKaryawan;
use app\Models\Bayu\mKartuStok;
use app\Models\Bayu\mBarang;

use PDF;

class LaporanStokGabungan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'harga_jual_barang' => ["search_field" => "tb_barang.harga_jual_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_22'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        return view('laporan/laporanDaftarHarga/laporanDaftarHargaList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $date_start = date_create('2019/12/03');
        $date_end = date_create('2020/01/30');
        $month_start = date_format($date_start, 'F');
        $month_end = date_format($date_end, 'F');
        dd($month_start . $month_end);
        $stok = new mKartuStok();
        $barang = new mBarang();
        $barang_all = mBarang::all();
        $golongan_all = mGolongan::all();
        $result['iTotalRecords'] = $stok->count_all();
        $result['iTotalDisplayRecords'] = $stok->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $barang_golongan = mBarang::with('golongan')->orderBy('id_golongan')->get();
        foreach ($golongan_all as $golongan) {

        }
        $data = $stok->eomonth_stok($start, $length, $query, $this->view);
        echo json_encode($barang_golongan);
        $no = $start + 1;
//        foreach ($data as $value){
//            $value->no = $no;
//            $no++;
//            $value->password = '';
//        }
        $result['aaData'] = $data;
//        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $list = mBarang::all();

        $data = array_merge($data, [
            'list' => $list,
        ]);

        return view('laporan/laporanDaftarHarga/laporanDaftarHargaExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $list = mBarang::all();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanDaftarHarga/laporanDaftarHargaPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Daftar Harga Barang ' . date('d-m-Y'));
    }
}
