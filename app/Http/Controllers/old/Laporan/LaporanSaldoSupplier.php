<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanSaldoSupplier extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_supplier' => ["search_field" => "kode_supplier"],
        'supplier' => ["search_field" => "supplier"],
        'wilayah' => ["search_field" => "tb_wilayah.wilayah"],
        'saldo' => ["search_field" => "saldo"]
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_supplier_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach($all_request as $key => $value){
                if ($key == 'tanggal_filter'){
                    $data['uri'] = '?'.$key.'='.$value;
                } else{
                    if ($key == 'id_wilayah'){
                        foreach ($value as $wilayah){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$wilayah;
                        }
                    } elseif ($key == 'id_supplier'){
                        foreach ($value as $supplier){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$supplier;
                        }
                    } else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }
//        dd($data['uri']);
        $data['tanggal_filter'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y');
        $data['wilayah'] = mWilayah::all();
        $data['select_wilayah'] = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;


        return view('laporan/laporanSaldoSupplier/laporanSaldoSupplierList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $tanggal_filter = $request->input('tanggal_filter') ? $request->input('tanggal_filter') : date('d-m-Y');
        $filter = Main::format_date_db($tanggal_filter);
        $wilayah = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

//        dd($wilayah);

        $hutang_supplier = new mHutangSupplier();
        $result['iTotalRecords'] = count($hutang_supplier->count_all_saldo_supplier( $filter, $wilayah, $supplier));
        $result['iTotalDisplayRecords'] = count($hutang_supplier->count_filter_saldo_supplier($query,$this->view, $filter, $wilayah, $supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $hutang_supplier->list_saldo_supplier($start,$length,$query,$this->view, $filter, $wilayah, $supplier);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->saldo = Main::format_money($value->saldo);
        }


//        dd($masuk_data);
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_saldo = 0;

        $tanggal_filter = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y');
        $filter = Main::format_date_db($tanggal_filter);
        $wilayah = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mHutangSupplier();


        $list = $hutang_supplier->export_saldo_supplier($filter, $wilayah, $supplier);
//        dd($list);
        if (empty($wilayah)) {
            $nama_wilayah = 'Seluruh Wilayah';
        } else {
            $nama_wilayah = array();
            foreach ($wilayah as $value){
                if ($value == 0){
                    array_push($nama_wilayah,'Seluruh Wilayah');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_wilayah, $nama);
                }
            }
        }

        if (empty($supplier)){
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_saldo += $value->saldo;
            $value->saldo = Main::format_money($value->saldo);
        }

        $data = array_merge($data, [
            'list' => $list,
            'supplier' => $wilayah,
            'total_saldo' => Main::format_money($total_saldo),
            'tanggal_filter' => $tanggal_filter,
            'nama_wilayah' => $nama_wilayah ? $nama_wilayah : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanSaldoSupplier/laporanSaldoSupplierExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_saldo = 0;

        $tanggal_filter = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y');
        $filter = Main::format_date_db($tanggal_filter);
        $wilayah = $request->input('id_wilayah') ? $request->input('id_wilayah') : null;
        $supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mHutangSupplier();


        $list = $hutang_supplier->export_saldo_supplier($filter, $wilayah, $supplier);
//        dd($list);
        if (empty($wilayah)) {
            $nama_wilayah = 'Seluruh Wilayah';
        } else {
            $nama_wilayah = array();
            foreach ($wilayah as $value){
                if ($value == 0){
                    array_push($nama_wilayah,'Seluruh Wilayah');
                } else {
                    $nama = mWilayah::where('id',$value)->value('wilayah');
                    array_push($nama_wilayah,$nama);
                }
            }
        }

        if (empty($supplier)){
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_saldo += $value->saldo;
            $value->saldo = Main::format_money($value->saldo);
        }

        $data = array_merge($data, [
            'list' => $list,
            'supplier' => $wilayah,
            'total_saldo' => Main::format_money($total_saldo),
            'tanggal_filter' => $tanggal_filter,
            'nama_wilayah' => $nama_wilayah ? $nama_wilayah : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanSaldoSupplier/laporanSaldoSupplierPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Saldo Supplier ' . $tanggal_filter);
    }
}
