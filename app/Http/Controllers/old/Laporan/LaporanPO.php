<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanPO extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'no_faktur' => ["search_field" => "no_faktur"],
        'tanggal' => ["search_field" => "tanggal"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'supplier' => ["search_field" => "supplier"],
        'nilai' => ["search_field" => "nilai"],
        'bayar_dn_kn' => ["search_field" => "bayar_dn_kn"],
        'sisa' => ["search_field" => "bayar_dn_kn"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_po_1'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach($all_request as $key => $value){
                if ($key == 'date_start'){
                    $data['uri'] = '?'.$key.'='.$value;
                } else{
                    if ($key == 'id_lokasi'){
                        foreach ($value as $lokasi){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$lokasi;
                        }
                    } elseif ($key == 'id_supplier'){
                        foreach ($value as $supplier){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$supplier;
                        }
                    } else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }
//        dd($data['uri']);
        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;


        return view('laporan/laporanPO/laporanPOList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);

        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mPO();
        $result['iTotalRecords'] = count($hutang_supplier->count_all_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier));
        $result['iTotalDisplayRecords'] = count($hutang_supplier->count_filter_laporan($query,$this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $hutang_supplier->list_laporan($start,$length,$query,$this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->tanggal = Main::format_date($value->tanggal);
            $value->nilai = Main::format_money($value->nilai);
            $value->bayar_dn_kn = Main::format_money($value->bayar_dn_kn);
            $value->sisa = Main::format_money($value->sisa);
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_nilai = 0;
        $total_bayar_dn_kn = 0;
        $total_sisa = 0;

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);

        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mPO();


        $list = $hutang_supplier->export_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);
        if (empty($select_lokasi)){
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value){
                if ($value == 0){
                    array_push($nama_lokasi,'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_nilai += $value->nilai;
            $total_bayar_dn_kn += $value->bayar_dn_kn;
            $total_sisa += $value->sisa;
            $value->tanggal = Main::format_date($value->tanggal);
            $value->nilai = Main::format_money($value->nilai);
            $value->bayar_dn_kn = Main::format_money($value->bayar_dn_kn);
            $value->sisa = Main::format_money($value->sisa);
        }

        $data = array_merge($data, [
            'list' => $list,
            'total_nilai' => Main::format_money($total_nilai),
            'total_bayar_dn_kn' => Main::format_money($total_bayar_dn_kn),
            'total_sisa' => Main::format_money($total_sisa),
            'tanggal_start' => $tanggal_start,
            'tanggal_end' => $tanggal_end,
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        return view('laporan/laporanPO/laporanPOExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_sisa = 0;
        $total_bayar_dn_kn = 0;
        $total_nilai = 0;

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);

        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mPO();


        $list = $hutang_supplier->export_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value){
                if ($value == 0){
                    array_push($nama_lokasi,'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_nilai += $value->nilai;
            $total_bayar_dn_kn += $value->bayar_dn_kn;
            $total_sisa += $value->sisa;
            $value->tanggal = Main::format_date($value->tanggal);
            $value->nilai = Main::format_money($value->nilai);
            $value->bayar_dn_kn = Main::format_money($value->bayar_dn_kn);
            $value->sisa = Main::format_money($value->sisa);
        }

        $data = array_merge($data, [
            'list' => $list,
            'total_nilai' => Main::format_money($total_nilai),
            'total_bayar_dn_kn' => Main::format_money($total_bayar_dn_kn),
            'total_sisa' => Main::format_money($total_sisa),
            'tanggal_start' => $tanggal_start,
            'tanggal_end' => $tanggal_end,
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPO/laporanPOPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan PO '. $tanggal_start  .' S/d '.$tanggal_end);
    }
}
