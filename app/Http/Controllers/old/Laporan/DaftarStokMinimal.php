<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DaftarStokMinimal extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'jml_barang' => ["search_field" => "tb_stok.jml_barang"],
        'minimal_stok' => ["search_field" => "tb_barang.minimal_stok"],
        'harga_beli_terakhir_barang' => ["search_field" => "tb_barang.harga_beli_terakhir_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $data = Main::data($this->breadcrumb);
        $detail = array();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'id_lokasi' => $id_lokasi
        ];
        $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();

        $data['lokasi'] = $lokasi;
        $data['id_lokasi'] = $id_lokasi;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/daftarStokMinimal/daftarStokMinimalList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok;
        $result['iTotalRecords'] = $stok->count_all();
        $result['iTotalDisplayRecords'] = $stok->count_filter($query, $this->detail, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list($start, $length, $query, $this->detail, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->jml_barang = Main::format_number_system($value->jml_barang).' '.$value->satuan;
            $value->minimal_stok = Main::format_number_system($value->minimal_stok).' '.$value->satuan;
            $value->harga_beli_terakhir_barang = Main::format_money($value->harga_beli_terakhir_barang);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mStok;

        $stok = $mstok->list($start, $length, $query, $this->detail, $id_lokasi);
        if ($id_lokasi == 'all') {
            $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();
        } else {
            $lokasi = mLokasi::where('id', $id_lokasi)->first();
        }

        $data = [
            'id_lokasi' => $id_lokasi,
            'lokasi' => $lokasi,
            'stok' => $stok,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/daftarStokMinimal/daftarStokMinimalPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Stok Minimal ' . date('d-m-Y'));
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mStok;

        $stok = $mstok->list($start, $length, $query, $this->detail, $id_lokasi);
        if ($id_lokasi == 'all') {
            $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();
        } else {
            $lokasi = mLokasi::where('id', $id_lokasi)->first();
        }

        $data = [
            'id_lokasi' => $id_lokasi,
            'lokasi' => $lokasi,
            'stok' => $stok,
            'company' => Main::companyInfo()
        ];

        return view('laporan/daftarStokMinimal/daftarStokMinimalExcel', $data);
    }
}
