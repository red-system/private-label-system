<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class LaporanAnalisaPenjualanTermasukPPN extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'total_qty' => ["search_field" => "qty"],
        'total_dpp' => ["search_field" => "dpp"],
        'total_ppn' => ["search_field" => "ppn"],
        'total_nilai' => ["search_field" => "total"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_analisa_penjualan_8'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }

        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();
        $params = [
            'date_start' => $date_start_db,
            'date_end' => $date_end_db,
            'id_lokasi' => $id_lokasi
        ];
        $data['lokasi'] = $lokasi;
        $data['id_lokasi'] = $id_lokasi;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanAnalisaPenjualanTermasukPPN/laporanAnalisaPenjualanTermasukPPNList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all_analisa_penjualan_termasuk_ppn()->count();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_analisa_penjualan_termasuk_ppn($query, $this->detail, $date_start, $date_end, $id_lokasi)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_analisa_penjualan_termasuk_ppn($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->total_qty = Main::format_decimal($value->total_qty).' '.$value->satuan;
            $value->total_ppn = Main::format_money($value->total_ppn);
            $value->total_dpp =  Main::format_money($value->total_dpp);
            $value->total_nilai = Main::format_money($value->total_nilai);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $list = $penjualan->list_analisa_penjualan_termasuk_ppn($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        $no = $start + 1;
        foreach ($list as $value) {
            $value->no = $no;
            $no++;
            $value->total_qty = Main::format_decimal($value->total_qty).' '.$value->satuan;
            $value->total_ppn = Main::format_money($value->total_ppn);
            $value->total_dpp =  Main::format_money($value->total_dpp);
            $value->total_nilai = Main::format_money($value->total_nilai);
        }
        $lokasi = 'Semua Lokasi';
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }
        $data = [
            'list' => $list,
            'lokasi' => $lokasi,
            'id_lokasi' => $id_lokasi,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanAnalisaPenjualanTermasukPPN/laporanAnalisaPenjualanTermasukPPNPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Analisa Penjualan Termasuk PPN ' .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mItemPenjualan();
        $list = $penjualan->list_analisa_penjualan_termasuk_ppn($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        $no = $start + 1;
        foreach ($list as $value) {
            $value->no = $no;
            $no++;
            $value->total_qty = Main::format_decimal($value->total_qty).' '.$value->satuan;
            $value->total_ppn = Main::format_money($value->total_ppn);
            $value->total_dpp =  Main::format_money($value->total_dpp);
            $value->total_nilai = Main::format_money($value->total_nilai);
        }
        $lokasi = 'Semua Lokasi';
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }
        $data = [
            'list' => $list,
            'lokasi' => $lokasi,
            'id_lokasi' => $id_lokasi,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanAnalisaPenjualanTermasukPPN/laporanAnalisaPenjualanTermasukPPNExcel', $data);
    }
}
