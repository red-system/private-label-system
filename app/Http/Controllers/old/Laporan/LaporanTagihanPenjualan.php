<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanTagihanPenjualan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'telat' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'no_faktur_penjualan' => ["search_field" => "tb_piutang_langganan.no_faktur_penjualan"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'salesman' => ["search_field" => "tb_salesman.salesman"],
        'sisa_piutang' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_6'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date($date);
        $date_db = Main::format_date_db($date);

        $params = [
            'date' => $date_db,
        ];
        $data['date'] = $date;
        $data['params'] = $params;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanTagihanPenjualan/laporanTagihanPenjualanList', $data);
    }

    function list(Request $request)
    {

        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date_db($date);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $result['iTotalRecords'] = $penjualan->count_all_tagihan();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_tagihan($query, $this->detail, $date);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_tagihan($start, $length, $query, $this->detail, $date);
        $min = 0;
        foreach ($data as $key => $value) {
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
//            if($value->telat <= 0){
//                unset($data[$key]);
//                $min++;
//            }
//            else{
            $value->telat = $value->telat . ' Hari';
//            }
        }
//        $result['iTotalDisplayRecords'] = $result['iTotalDisplayRecords'] - $min;
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date($date);
        $date_db = Main::format_date_db($date);

        $query = null;
        $start = null;
        $length = null;
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_tagihan($start, $length, $query, $this->detail, $date_db);

        foreach ($list as $key => $value) {
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
//            if($value->telat <= 0){
//                unset($data[$key]);
//                $min++;
//            }
//            else{
            $value->telat = $value->telat . ' Hari';
//            }
        }
        $data = [
            'list' => $list,
            'date' => $date,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanTagihanPenjualan/laporanTagihanPenjualanPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Tagihan Penjualan ' . $date);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date = $request->input('date');
        $date = $date ? $date : now();
        $date = Main::format_date($date);
        $date_db = Main::format_date_db($date);

        $query = null;
        $start = null;
        $length = null;
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_tagihan($start, $length, $query, $this->detail, $date_db);

        foreach ($list as $key => $value) {
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
//            if($value->telat <= 0){
//                unset($data[$key]);
//                $min++;
//            }
//            else{
            $value->telat = $value->telat . ' Hari';
//            }
        }
        $data = [
            'list' => $list,
            'date' => $date,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanTagihanPenjualan/laporanTagihanPenjualanExcel', $data);
    }
}
