<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Bayu\mLokasi;
use app\Models\Widi\mBarang;
use app\Models\Widi\mGolongan;
use app\Models\Widi\mStok;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanSaldoStok extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'golongan' => ["search_field" => "tb_golongan.golongan"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'jml_barang' => ["search_field" => "tb_stok.jml_barang"],
        'harga_beli_terakhir_barang' => ["search_field" => "tb_barang.harga_beli_terakhir_barang"],
        'harga_jual_barang' => ["search_field" => "tb_barang.harga_jual_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_6'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_golongan = $request->input('id_golongan');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_golongan = $id_golongan ? $id_golongan : 'all';
        $data = Main::data($this->breadcrumb);
        $detail = array();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();
        $golongan = mGolongan::orderBy('golongan', 'ASC')->get();
        $params = [
            'id_lokasi' => $id_lokasi,
            'id_golongan' => $id_golongan
        ];
        $data['id_lokasi'] = $id_lokasi;
        $data['id_golongan'] = $id_golongan;
        $data['params'] = $params;
        $data['lokasi'] = $lokasi;
        $data['golongan'] = $golongan;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanSaldoStok/laporanSaldoStokList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_golongan = $request->input('id_golongan');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_golongan = $id_golongan ? $id_golongan : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok;
        $result['iTotalRecords'] = $stok->count_barang();
        $result['iTotalDisplayRecords'] = $stok->count_filter_barang($query, $this->detail, $id_lokasi, $id_golongan);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list_barang($start, $length, $query, $this->detail, $id_lokasi, $id_golongan);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->harga_beli_terakhir_barang = Main::format_money($value->harga_beli_terakhir_barang);
            $value->harga_jual_barang = Main::format_money($value->harga_jual_barang);
            $value->jml_barang = Main::format_number_system($value->jml_barang).' '.$value->satuan;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_golongan = $request->input('id_golongan');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_golongan = $id_golongan ? $id_golongan : 'all';
        $barang = DB::table('tb_barang')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->select('tb_barang.*', 'tb_golongan.golongan');
        if ($id_golongan != 'all') {
            $barang = $barang->where('id_golongan', $id_golongan);
        }
        $lokasi = 'Semua Lokasi';
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }
        $barang = $barang->get();
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mStok;
        $stok = $mstok->list_barang($start, $length, $query, $this->detail, $id_lokasi, $id_golongan);

        $data = [
            'lokasi' => $lokasi,
            'stok' => $stok,
            'barang' => $barang,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanSaldoStok/laporanSaldoStokPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Saldo Stok ' . date('d-m-Y'));
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_golongan = $request->input('id_golongan');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_golongan = $id_golongan ? $id_golongan : 'all';
        $barang = DB::table('tb_barang')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->select('tb_barang.*', 'tb_golongan.golongan');
        if ($id_golongan != 'all') {
            $barang = $barang->where('id_golongan', $id_golongan);
        }
        $lokasi = 'Semua Lokasi';
        if ($id_lokasi != 'all') {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
        }
        $barang = $barang->get();
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $mstok = new mStok;
        $stok = $mstok->list_barang($start, $length, $query, $this->detail, $id_lokasi, $id_golongan);

        $data = [
            'lokasi' => $lokasi,
            'stok' => $stok,
            'barang' => $barang,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanSaldoStok/laporanSaldoStokExcel', $data);
    }
}
