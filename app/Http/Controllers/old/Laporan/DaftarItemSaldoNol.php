<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DaftarItemSaldoNol extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'jml_barang' => ["search_field" => "tb_stok.jml_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_16'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        $satuan = mSatuan::all();
        $lokasi = mLokasi::all();
        $supplier = mSupplier::all();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }

        $data['detail'] = json_encode($detail);
        $data['satuan'] = $satuan;
        $data['lokasi'] = $lokasi;
        $data['supplier'] = $supplier;
        return view('laporan/daftarItemSaldoNol/daftarItemSaldoNolList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok;
        $result['iTotalRecords'] = $stok->count_all_nol();
        $result['iTotalDisplayRecords'] = $stok->count_filter_nol($query, $this->detail);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list_nol($start, $length, $query, $this->detail);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf()
    {
        ini_set("memory_limit", "-1");
        $lokasi = DB::table('tb_stok')->select('tb_stok.id_lokasi', 'tb_lokasi.lokasi')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->where('jml_barang', '=', 0)->get();
        $mstok = new mStok();
        $stok = $mstok->print_nol();

        $data = [
            'lokasi' => $lokasi,
            'stok' => $stok,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/daftarItemSaldoNol/daftarItemSaldoNolPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Item Saldo Nol ' . now());
    }

    function excel()
    {
        ini_set("memory_limit", "-1");
        $lokasi = DB::table('tb_stok')->select('tb_stok.id_lokasi', 'tb_lokasi.lokasi')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->where('jml_barang', '=', 0)->get();
        $mstok = new mStok();
        $stok = $mstok->print_nol();

        $data = [
            'lokasi' => $lokasi,
            'stok' => $stok,
            'company' => Main::companyInfo()
        ];

        return view('laporan/daftarItemSaldoNol/daftarItemSaldoNolExcel', $data);
    }
}
