<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelian;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanPODetail extends Controller


{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'no_faktur'=>["search_field"=>"no_faktur"],
        'tanggal'=>["search_field"=>"tanggal"],
        'lokasi'=>["search_field"=>"tb_lokasi.lokasi"],
        'supplier'=>["search_field"=>"supplier"],
        'ppn'=>["search_field"=>"ppn"],
        'grand_total'=>["search_field"=>"grand_total"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_po_2'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach($all_request as $key => $value){
                if ($key == 'date_start'){
                    $data['uri'] = '?'.$key.'='.$value;
                } else{
                    if ($key == 'id_lokasi'){
                        foreach ($value as $lokasi){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$lokasi;
                        }
                    } elseif ($key == 'id_supplier'){
                        foreach ($value as $supplier){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$supplier;
                        }
                    } else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }
//        dd($data['uri']);
        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;


        return view('laporan/laporanPODetail/laporanPODetailList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);

        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

//        dd($wilayah);

        $pembelian_detail = new mPO();
        $result['iTotalRecords'] = count($pembelian_detail->count_all_detail_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier));
        $result['iTotalDisplayRecords'] = count($pembelian_detail->count_filter_detail_laporan($query,$this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pembelian_detail->list_detail_laporan($start,$length,$query,$this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->tanggal = Main::format_date($value->tanggal);
            $value->ppn = Main::format_money($value->ppn);
            $value->grand_total = Main::format_money($value->grand_total);
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);

        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian_detail = new mPO();


        $list = $pembelian_detail->export_detail_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);
        $item = $pembelian_detail->export_detail_barang_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value){
                if ($value == 0){
                    array_push($nama_lokasi,'Seluruh Lokasi');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

//        buat pengurangna harga dengan discount nominal

        foreach ($item as $value){
            $value->harga = $value->harga - (($value->harga * $value->discount_barang) / 100);
            $value->harga = Main::format_money($value->harga);
            $value->sub_total = Main::format_money($value->sub_total);
            $value->grand_total = Main::format_money($value->grand_total);
        }
        foreach ($list as $value) {
            $value->tanggal = Main::format_date($value->tanggal);
            $value->discount_nominal_po = Main::format_money($value->discount_nominal_po);
            $value->ppn = Main::format_money($value->ppn);
            $value->ongkos_kirim_po = Main::format_money($value->ongkos_kirim_po);
            $value->grand_total = Main::format_money($value->grand_total);
        }

        $data = array_merge($data, [
            'list' => $list,
            'item' => $item,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanPODetail/laporanPODetailExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $start_date = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($start_date);
        $end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($end);

        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian_detail = new mPO();


        $list = $pembelian_detail->export_detail_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);
        $item = $pembelian_detail->export_detail_barang_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value){
                if ($value == 0){
                    array_push($nama_lokasi,'Seluruh Lokasi');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($item as $value){
            $value->harga = $value->harga - (($value->harga * $value->discount_barang) / 100);
            $value->harga = Main::format_money($value->harga);
            $value->sub_total = Main::format_money($value->sub_total);
            $value->grand_total = Main::format_money($value->grand_total);
        }

        foreach ($list as $value) {
            $value->tanggal = Main::format_date($value->tanggal);
            $value->discount_nominal_po = Main::format_money($value->discount_nominal_po);
            $value->ppn = Main::format_money($value->ppn);
            $value->ongkos_kirim_po = Main::format_money($value->ongkos_kirim_po);
            $value->grand_total = Main::format_money($value->grand_total);
        }


        $data = array_merge($data, [
            'list' => $list,
            'item' => $item,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPODetail/laporanPODetailPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan PO Detail '. $tanggal_start  .' S/d '.$tanggal_end);
    }
}
