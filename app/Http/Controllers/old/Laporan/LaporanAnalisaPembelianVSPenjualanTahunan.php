<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelian;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanAnalisaPembelianVSPenjualanTahunan extends Controller


{
//    TODO : tanyakan ke bli deva apakah benar data nilai_beli dan nilai_jual yang diambil itu dari sub_total
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode' => ["search_field" => "kode"],
        'bulan'=>["search_field" => "bulan"],
        'barang' => ["search_field" => "barang"],
        'qty_beli' => ["search_field" => "qty_beli"],
        'nilai_beli' => ["search_field" => "nilai_beli"],
        'qty_jual' => ["search_field" => "qty_beli"],
        'nilai_jual' => ["search_field" => "nilai_beli"],
    ];

    private $view_beli = [
        'kode' => ["search_field" => "kode"],
        'barang' => ["search_field" => "barang"],
        'qty_beli' => ["search_field" => "qty_beli"],
        'nilai_beli' => ["search_field" => "nilai_beli"],
        'qty_jual' => ["search_field" => "qty_beli"],
        'nilai_jual' => ["search_field" => "nilai_beli"],
    ];

    private $view_jual = [
        'kode' => ["search_field" => "kode"],
        'barang' => ["search_field" => "barang"],
        'qty_beli' => ["search_field" => "qty_jual"],
        'nilai_beli' => ["search_field" => "nilai_jual"],
        'qty_jual' => ["search_field" => "qty_jual"],
        'nilai_jual' => ["search_field" => "nilai_jual"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_pembelian_7'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'tahun_filter') {
                    $data['uri'] = '?' . $key . '=' . $value;
                } else {
                    if ($key == 'id_lokasi') {
                        foreach ($value as $lokasi) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $lokasi;
                        }
                    } elseif ($key == 'id_golongan') {
                        foreach ($value as $golongan) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $golongan;
                        }
                    } elseif ($key == 'id_barang') {
                        foreach ($value as $barang) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $barang;
                        }
                    } else {
                        $data['uri'] .= '&' . $key . '=' . $value;
                    }
                }
            }
        }
//        dd($data['uri']);

        $data['tahun_filter'] = $request->input('tahun_filter') ? $request->input('tahun_filter') : date('Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['golongan'] = mGolongan::all();
        $data['select_golongan'] = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $data['barang'] = mBarang::all();
        $data['select_barang'] = $request->input('id_barang') ? $request->input('id_barang') : null;


        return view('laporan/laporanAnalisaPembelianVSPenjualanTahunan/laporanAnalisaPembelianVSPenjualanTahunanList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');

        $tahun_filter = $request->input('tahun_filter') ? $request->input('tahun_filter') : date('Y');
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_barang = $request->input('id_barang') ? $request->input('id_barang') : null;

        $pembelian = new mPembelian();

        $result['iTotalRecords'] = count($pembelian->count_all_analisa_vs_penjualan_tahunan($tahun_filter, $select_lokasi, $select_golongan, $select_barang));
        $result['iTotalDisplayRecords'] = count($pembelian->count_filter_analisa_vs_penjualan_tahunan($query, $this->view_beli, $this->view_jual, $tahun_filter, $select_lokasi, $select_golongan, $select_barang));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pembelian->list_analisa_vs_penjualan_tahunan($start, $length, $query, $this->view_beli, $this->view_jual, $tahun_filter, $select_lokasi, $select_golongan, $select_barang);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->qty_beli = Main::format_number($value->qty_beli);
            $value->nilai_beli = Main::format_money($value->nilai_beli);
            $value->qty_jual = Main::format_number($value->qty_jual);
            $value->nilai_jual = Main::format_money($value->nilai_jual);
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_qty_beli = 0;
        $total_nilai_beli = 0;
        $total_qty_jual = 0;
        $total_nilai_jual = 0;

        $tahun_filter = $request->input('tahun_filter') ? $request->input('tahun_filter') : date('Y');
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_barang = $request->input('id_barang') ? $request->input('id_barang') : null;

        $pembelian = new mPembelian();

        $list = $pembelian->export_analisa_vs_penjualan_tahunan($tahun_filter, $select_lokasi, $select_golongan, $select_barang);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value) {
                if ($value == 0) {
                    array_push($nama_lokasi, 'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = array();
            foreach ($select_golongan as $value) {
                if ($value == 0) {
                    array_push($nama_golongan, 'Seluruh Golongan');
                } else {
                    $nama = mGolongan::where('id', $value)->value('golongan');
                    array_push($nama_golongan, $nama);
                }
            }
        }

        if (empty($select_barang)) {
            $nama_barang = 'Seluruh Barang';
        } else {
            $nama_barang = array();
            foreach ($select_barang as $value) {
                if ($value == 0) {
                    array_push($nama_barang, 'Seluruh Barang');
                } else {
                    $nama = mBarang::where('id', $value)->value('nama_barang');
                    array_push($nama_barang, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $total_qty_beli += $value->qty_beli;
            $total_nilai_beli += $value->nilai_beli;
            $total_qty_jual += $value->qty_jual;
            $total_nilai_jual += $value->nilai_jual;
            $value->qty_beli = Main::format_number($value->qty_beli);
            $value->nilai_beli = Main::format_money($value->nilai_beli);
            $value->qty_jual = Main::format_number($value->qty_jual);
            $value->nilai_jual = Main::format_money($value->nilai_jual);
        }

        $data = array_merge($data, [
            'list' => $list,
            'tahun_filter' => $tahun_filter,
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'nama_barang' => $nama_barang ? $nama_barang : null,
            'total_qty_beli' => Main::format_number($total_qty_beli),
            'total_nilai_beli' => Main::format_money($total_nilai_beli),
            'total_qty_jual' => Main::format_number($total_qty_jual),
            'total_nilai_jual' => Main::format_money($total_nilai_jual),
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanAnalisaPembelianVSPenjualanTahunan/laporanAnalisaPembelianVSPenjualanTahunanExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_qty_beli = 0;
        $total_nilai_beli = 0;
        $total_qty_jual = 0;
        $total_nilai_jual = 0;

        $tahun_filter = $request->input('tahun_filter') ? $request->input('tahun_filter') : date('Y');
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_barang = $request->input('id_barang') ? $request->input('id_barang') : null;

        $pembelian = new mPembelian();

        $list = $pembelian->export_analisa_vs_penjualan_tahunan($tahun_filter, $select_lokasi, $select_golongan, $select_barang);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value) {
                if ($value == 0) {
                    array_push($nama_lokasi, 'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = array();
            foreach ($select_golongan as $value) {
                if ($value == 0) {
                    array_push($nama_golongan, 'Seluruh Golongan');
                } else {
                    $nama = mGolongan::where('id', $value)->value('golongan');
                    array_push($nama_golongan, $nama);
                }
            }
        }

        if (empty($select_barang)) {
            $nama_barang = 'Seluruh Barang';
        } else {
            $nama_barang = array();
            foreach ($select_barang as $value) {
                if ($value == 0) {
                    array_push($nama_barang, 'Seluruh Barang');
                } else {
                    $nama = mBarang::where('id', $value)->value('nama_barang');
                    array_push($nama_barang, $nama);
                }
            }
        }

        foreach ($list as $key => $value) {
            $total_qty_beli += $value->qty_beli;
            $total_nilai_beli += $value->nilai_beli;
            $total_qty_jual += $value->qty_jual;
            $total_nilai_jual += $value->nilai_jual;
            $value->qty_beli = Main::format_number($value->qty_beli);
            $value->nilai_beli = Main::format_money($value->nilai_beli);
            $value->qty_jual = Main::format_number($value->qty_jual);
            $value->nilai_jual = Main::format_money($value->nilai_jual);
        }


        $data = array_merge($data, [
            'list' => $list,
            'tahun_filter' => $tahun_filter,
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'nama_barang' => $nama_barang ? $nama_barang : null,
            'total_qty_beli' => Main::format_number($total_qty_beli),
            'total_nilai_beli' => Main::format_money($total_nilai_beli),
            'total_qty_jual' => Main::format_number($total_qty_jual),
            'total_nilai_jual' => Main::format_money($total_nilai_jual),
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanAnalisaPembelianVSPenjualanTahunan/laporanAnalisaPembelianVSPenjualanTahunanPdf', $data);

//        return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Analisa Pembelian VS Penjualan ' . $tahun_filter);
    }
}
