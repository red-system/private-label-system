<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mSatuan;
use app\Models\Bayu\mStok;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\Bayu\mBarang;

use PDF;

class DaftarBarangZero extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'satuan' => ["search_field" => "satuan"],
        'harga_jual_barang' => ["search_field" => "harga_jual_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_13'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        return view('laporan/daftarBarangZero/daftarBarangZeroList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();
        $result['iTotalRecords'] = $barang->count_all_zero();
        $result['iTotalDisplayRecords'] = $barang->count_filter_zero($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $barang->list_zero($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            if (empty($value->satuan)) {
                $value->satuan = mSatuan::where('id', $value->id_satuan)->first()->satuan;
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $barang = new mBarang();

        $list = $barang->export_zero();
        $count = $list->count();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
            'count' => $count
        ]);

        return view('laporan/daftarBarangZero/daftarBarangZeroExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $barang = new mBarang();

        $list = $barang->export_zero();
        $count = $list->count();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
            'count' => $count
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/daftarBarangZero/daftarBarangZeroPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Daftar Barang Zero ' . date('d-m-Y'));
    }
}
