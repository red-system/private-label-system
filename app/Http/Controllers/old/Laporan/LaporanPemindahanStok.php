<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mItemPemindahanStok;
use app\Models\Widi\mLokasi;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanPemindahanStok extends Controller
{
    private $breadcrumb;
    private $cons;

    private $view = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'satuan' => ["search_field" => "tb_satuan.satuan"],
        'total_pindah' => ["search_field" => "tb_item_pemindahan.jml_barang"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_stok_8'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['url'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['url'] = '?' . $key . '=' . $value;
                } else {
                    if ($key == 'lokasi_arr') {
                        foreach ($value as $lokasi_arr) {
                            $data['url'] .= '&' . $key . '%5B%5D=' . $lokasi_arr;
                        }
                    } else {
                        $data['url'] .= '&' . $key . '=' . $value;
                    }

                }
            }
        }

        $lokasi = mLokasi::orderBy('lokasi', 'ASC');
        $lokasi = $lokasi->get();

        $lokasi_arr = $request->input('lokasi_arr');

        $lokasi_arr = $lokasi_arr ? $lokasi_arr : $loaksi_arr = [0];

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();

        $date_start = Main::format_date($date_start);
        $date_end = Main::format_date($date_end);

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'lokasi_arr' => $lokasi_arr
        ];

        $data['tab'] = 'trasfer';
        $data['lokasi'] = $lokasi;
        $data['lokasi_arr'] = $lokasi_arr;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['view'] = json_encode($view);
        return view('laporan/laporanPemindahanStok/laporanPemindahanStokList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $lokasi = $request->input('lokasi_arr');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();

        $date_start_db = Main::format_date_db($date_start);
        $date_end_db = Main::format_date_db($date_end);

        $lokasi = $lokasi ? $lokasi : $loaksi = [0];

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mItemPemindahanStok();
        $result['iTotalRecords'] = $stok->count_all_laporan();
        $result['iTotalDisplayRecords'] = $stok->count_filter_laporan($query, $this->view, $date_start_db, $date_end_db, $lokasi)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $data_lokasi = $request->input('lokasi_arr');

        $date_start = $date_start ? $date_start : now();
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : now();
        $date_end_db = Main::format_date_db($date_end);

        $data_lokasi = $data_lokasi ? $data_lokasi : $loaksi = [0];

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $list = new mItemPemindahanStok();
        $data_pemindahan = $list->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $data_lokasi);
        $count = $list->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $data_lokasi)->count();

        $tempat = [];
        if ($data_lokasi != null) {
            if (in_array(0, $data_lokasi)) {
                $tempat[0]['lokasi'] = 'Semua Lokasi';
            } else {
                foreach ($data_lokasi as $key => $l) {
                    $nama_tempat = mLokasi::where('id', $l)->first();
                    $tempat[$key]['lokasi'] = $nama_tempat->lokasi;
                }
            }
        } else {
            $tempat[0]['lokasi'] = 'Semua Lokasi';
        }


        if ($tempat[0]['lokasi'] == 'Semua Lokasi') {
            $lokasi = mLokasi::all();
        } else {
            $lokasi = mLokasi::whereIn('id',$data_lokasi)->get();
        }


        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'tempat' => $tempat,
            'lokasi' => $lokasi,
            'list' => $data_pemindahan,
            'count' => $count,
            'company' => Main::companyInfo()
        ];


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPemindahanStok/laporanPemindahanStokPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Pemindahan Stok ' . $date_start . ' s/d ' . $date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $data_lokasi = $request->input('lokasi_arr');

        $date_start = $date_start ? $date_start : now();
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : now();
        $date_end_db = Main::format_date_db($date_end);

        $data_lokasi = $data_lokasi ? $data_lokasi : $loaksi = [0];

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $list = new mItemPemindahanStok();
        $data_pemindahan = $list->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $data_lokasi);
        $count = $list->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $data_lokasi)->count();

        $tempat = [];
        if ($data_lokasi != null) {
            if (in_array(0, $data_lokasi)) {
                $tempat[0]['lokasi'] = 'Semua Lokasi';
            } else {
                foreach ($data_lokasi as $key => $l) {
                    $nama_tempat = mLokasi::where('id', $l)->first();
                    $tempat[$key]['lokasi'] = $nama_tempat->lokasi;
                }
            }
        } else {
            $tempat[0]['lokasi'] = 'Semua Lokasi';
        }


        if ($tempat[0]['lokasi'] == 'Semua Lokasi') {
            $lokasi = mLokasi::all();
        } else {
            $lokasi = mLokasi::whereIn('id',$data_lokasi)->get();
        }


        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'tempat' => $tempat,
            'lokasi' => $lokasi,
            'list' => $data_pemindahan,
            'count' => $count,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanPemindahanStok/laporanPemindahanStokExcel', $data);
    }
}
