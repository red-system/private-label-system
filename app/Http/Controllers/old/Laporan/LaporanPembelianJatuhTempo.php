<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelian;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanPembelianJatuhTempo extends Controller


{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'jatuh_tempo' => ["search_field" => "jatuh_tempo"],
        'hari' => ["search_field" => "jatuh_tempo"],
        'no_faktur' => ["search_field" => "no_faktur"],
        'supplier' => ["search_field" => "supplier"],
        'jumlah' => ["search_field" => "jumlah"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_pembelian_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach($all_request as $key => $value){
                if ($key == 'date_filter'){
                    $data['uri'] = '?'.$key.'='.$value;
                } else{
                    if ($key == 'id_lokasi'){
                        foreach ($value as $lokasi){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$lokasi;
                        }
                    } elseif ($key == 'id_supplier'){
                        foreach ($value as $supplier){
                            $data['uri'] .= '&'.$key.'%5B%5D='.$supplier;
                        }
                    } else {
                        $data['uri'] .= '&'.$key.'='.$value;
                    }
                }
            }
        }
//        dd($data['uri']);

        $data['date_filter'] = $request->input('date_filter') ? $request->input('date_filter') : date('d-m-Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;


        return view('laporan/laporanPembelianJatuhTempo/laporanPembelianJatuhTempoList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');

        $filter_date = $request->input('date_filter') ? $request->input('date_filter') : date('d-m-Y');
        $tanggal_filter = Main::format_date_db($filter_date);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

//        dd($wilayah);

        $hutang_supplier = new mPembelian();

        $result['iTotalRecords'] = count($hutang_supplier->count_all_jatuh_tempo($tanggal_filter, $select_lokasi, $select_supplier));
        $result['iTotalDisplayRecords'] = count($hutang_supplier->count_filter_jatuh_tempo($query, $this->view, $tanggal_filter, $select_lokasi, $select_supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $hutang_supplier->list_jatuh_tempo($start, $length, $query, $this->view, $tanggal_filter, $select_lokasi, $select_supplier);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $date_diff1 = date_create($tanggal_filter);
            $date_diff2 = date_create($value->jatuh_tempo);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->jumlah = Main::format_money($value->jumlah);
            $value->hari = date_diff($date_diff1, $date_diff2)->format("%R%a");
            $value->hari = trim($value->hari, '+');
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_date = $request->input('date_filter') ? $request->input('date_filter') : date('d-m-Y');
        $tanggal_filter = Main::format_date_db($filter_date);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mPembelian();

        $list = $hutang_supplier->export_jatuh_tempo($tanggal_filter, $select_lokasi, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value){
                if ($value == 0){
                    array_push($nama_lokasi,'Seluruh Lokasi');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $date_diff1 = date_create($tanggal_filter);
            $date_diff2 = date_create($value->jatuh_tempo);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->jumlah = Main::format_money($value->jumlah);
            $value->hari = date_diff($date_diff1, $date_diff2)->format("%R%a");
            $value->hari = trim($value->hari, '+');
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_filter' => Main::format_date($tanggal_filter),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        return view('laporan/laporanPembelianJatuhTempo/laporanPembelianJatuhTempoExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_date = $request->input('date_filter') ? $request->input('date_filter') : date('d-m-Y');
        $tanggal_filter = Main::format_date_db($filter_date);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $hutang_supplier = new mPembelian();

        $list = $hutang_supplier->export_jatuh_tempo($tanggal_filter, $select_lokasi, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value){
                if ($value == 0){
                    array_push($nama_lokasi,'Seluruh Lokasi');
                } else {
                    $nama = mWilayah::where('id', $value)->value('wilayah');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = array();
            foreach ($select_supplier as $value){
                if ($value == 0){
                    array_push($nama_supplier,'Seluruh Supplier');
                } else {
                    $nama = mSupplier::where('id', $value)->value('supplier');
                    array_push($nama_supplier, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $date_diff1 = date_create($tanggal_filter);
            $date_diff2 = date_create($value->jatuh_tempo);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->jumlah = Main::format_money($value->jumlah);
            $value->hari = date_diff($date_diff1, $date_diff2)->format("%R%a");
            $value->hari = trim($value->hari, '+');
        }


        $data = array_merge($data, [
            'list' => $list,
            'tanggal_filter' => Main::format_date($tanggal_filter),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPembelianJatuhTempo/laporanPembelianJatuhTempoPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Pembelian Jatuh Tempo '. $tanggal_filter);
    }
}
