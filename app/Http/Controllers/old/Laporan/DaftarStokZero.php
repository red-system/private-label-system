<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mStok;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use app\Models\Bayu\mBarang;

use PDF;

class DaftarStokZero extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_stok_12'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        return view('laporan/daftarStokZero/daftarStokZeroList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok();
        $result['iTotalRecords'] = $stok->count_all_zero();
        $result['iTotalDisplayRecords'] = $stok->count_filter_zero($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $stok->list_zero($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $stok = new mStok();

        $list = $stok->export_zero();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
        ]);

        return view('laporan/daftarStokZero/daftarStokZeroExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $stok = new mStok();

        $list = $stok->export_zero();

        $data = array_merge($data, [
            'list' => $list,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/daftarStokZero/daftarStokZeroPdf', $data);

        //return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Daftar Stok Zero ' . date('d-m-Y'));
    }
}
