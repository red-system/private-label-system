<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class LaporanPenjualanJatuhTempo extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'no_faktur_penjualan' => ["search_field" => "tb_piutang_langganan.no_faktur_penjualan"],
        'tgl_faktur_penjualan' => ["search_field" => "tb_piutang_langganan.tgl_faktur_penjualan"],
        'jatuh_tempo' => ["search_field" => "tb_piutang_langganan.jatuh_tempo"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'sisa_piutang' => ["search_field" => "tb_piutang_langganan.sisa_piutang"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_penjualan_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }

        $data['detail'] = json_encode($detail);
        return view('laporan/laporanPenjualanJatuhTempo/laporanPenjualanJatuhTempoList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $piutang = new mPiutangLangganan();
        $result['iTotalRecords'] = $piutang->count_all_laporan_jatuh_tempo();
        $result['iTotalDisplayRecords'] = $piutang->count_filter_laporan_jatuh_tempo($query, $this->detail);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $piutang->list_laporan_jatuh_tempo($start, $length, $query, $this->detail);
        foreach ($data as $value) {
            $value->tgl_faktur_penjualan = Main::format_date($value->tgl_faktur_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf()
    {
        ini_set("memory_limit", "-1");
        $date = now();
        $date = Main::format_date($date);
        $piutang = new mPiutangLangganan();
        $query = null;
        $start = null;
        $length = null;
        $list = $piutang->list_laporan_jatuh_tempo($start, $length, $query, $this->detail);
        foreach ($list as $value) {
            $value->tgl_faktur_penjualan = Main::format_date($value->tgl_faktur_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
        }
        $data = [
            'list' => $list,
            'date' => $date,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanPenjualanJatuhTempo/laporanPenjualanJatuhTempoPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Penjualan Jatuh Tempo ' . date('d-m-Y'));
    }

    function excel()
    {
        ini_set("memory_limit", "-1");
        $date = now();
        $date = Main::format_date($date);
        $piutang = new mPiutangLangganan();
        $query = null;
        $start = null;
        $length = null;
        $list = $piutang->list_laporan_jatuh_tempo($start, $length, $query, $this->detail);
        foreach ($list as $value) {
            $value->tgl_faktur_penjualan = Main::format_date($value->tgl_faktur_penjualan);
            $value->jatuh_tempo = Main::format_date($value->jatuh_tempo);
            $value->sisa_piutang = Main::format_money($value->sisa_piutang);
        }
        $data = [
            'list' => $list,
            'date' => $date,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanPenjualanJatuhTempo/laporanPenjualanJatuhTempoExcel', $data);
    }
}
