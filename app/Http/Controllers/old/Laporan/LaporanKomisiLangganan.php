<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mKomisi;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanKomisiLangganan extends Controller
{
    private $breadcrumb;
    private $view = [
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'min_beli' => ["search_field" => "tb_komisi.min_beli"],
        'qty_beli' => ["search_field" => "tb_komisi.qty_beli"],
        'jenis_komisi' => ["search_field" => "tb_komisi.jenis_komisi"],
        'komisi' => ["search_field" => "tb_komisi.komisi"],
        'komisi_nominal' => ["search_field" => "tb_komisi.komisi_nominal"],
        'status' => ["search_field" => "tb_komisi.status"],
        'updated_at' => ["search_field" => "tb_komisi.updated_at"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['komisi_langganan'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $status = $request->input('status');
        $status = $status ? $status : 'all';
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'status' => $status,
        ];

        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['status'] = $status;
        $data['params'] = $params;
        $data['view'] = json_encode($view);
        return view('laporan/laporanKomisiLangganan/laporanKomisiLanggananList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);
        $status = $request->input('status');
        $status = $status ? $status : 'all';
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $komisi = new mKomisi();
        $result['iTotalRecords'] = $komisi->count_all_laporan();
        $result['iTotalDisplayRecords'] = $komisi->count_filter_laporan($query, $this->view, $date_start, $date_end, $status);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $komisi->list_laporan($start, $length, $query, $this->view, $date_start, $date_end, $status);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->updated_at = Main::format_date($value->updated_at);
            $value->qty_beli = Main::format_decimal($value->qty_beli).' '.$value->satuan;
            $value->min_beli = Main::format_decimal($value->min_beli).' '.$value->satuan;
            if ($value->jenis_komisi == 'persentase') {
                $value->jenis_komisi = 'Persentase';
                $value->komisi = Main::format_decimal($value->komisi) . '%';
            }
            else{
                $value->jenis_komisi = 'Nominal';
                $value->komisi = Main::format_money($value->komisi);
            }
            $value->komisi_nominal = Main::format_money($value->komisi_nominal);
            if ($value->status == 'belum_terbayar') {
                $value->status = 'Belum Dibayar';
                $value->updated_at = null;
            } else {
                $value->status = 'Terbayar';
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $status = $request->input('status');
        $status = $status ? $status : 'all';
        $query = null;
        $start = null;
        $length = null;

        $komisi = new mKomisi();
        $list = $komisi->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $status);
        $count = $list->count();
        if ($status == 'terbayar') {
            $status_list = 'Terbayar';
        } elseif ($status == 'belum_terbayar') {
            $status_list = 'Belum Dibayar';
        } else {
            $status_list = 'Semua Data';
        }

        foreach ($list as $value) {
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->updated_at = Main::format_date($value->updated_at);
            $value->qty_beli = Main::format_decimal($value->qty_beli).' '.$value->satuan;
            $value->min_beli = Main::format_decimal($value->min_beli).' '.$value->satuan;

            if ($value->jenis_komisi == 'persentase') {
                $value->jenis_komisi = 'Persentase';
                $value->komisi = Main::format_decimal($value->komisi) . '%';
            }
            else{
                $value->jenis_komisi = 'Nominal';
                $value->komisi = Main::format_money($value->komisi);
            }
            $value->komisi_nominal = Main::format_money($value->komisi_nominal);
            if ($value->status == 'belum_terbayar') {
                $value->status = 'Belum Dibayar';
                $value->updated_at = null;
            } else {
                $value->status = 'Terbayar';
            }
        }

        $data = [
            'list' => $list,
            'count' => $count,
            'status_list' => $status_list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanKomisiLangganan/laporanKomisiLanggananPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Komisi Langganan ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);
        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);
        $status = $request->input('status');
        $status = $status ? $status : 'all';
        $query = null;
        $start = null;
        $length = null;

        $komisi = new mKomisi();
        $list = $komisi->list_laporan($start, $length, $query, $this->view, $date_start_db, $date_end_db, $status);
        $count = $list->count();
        if ($status == 'terbayar') {
            $status_list = 'Terbayar';
        } elseif ($status == 'belum_terbayar') {
            $status_list = 'Belum Dibayar';
        } else {
            $status_list = 'Semua Data';
        }

        foreach ($list as $value) {
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            $value->updated_at = Main::format_date($value->updated_at);
            $value->qty_beli = Main::format_decimal($value->qty_beli).' '.$value->satuan;
            $value->min_beli = Main::format_decimal($value->min_beli).' '.$value->satuan;

            if ($value->jenis_komisi == 'persentase') {
                $value->jenis_komisi = 'Persentase';
                $value->komisi = Main::format_decimal($value->komisi) . '%';
            }
            else{
                $value->jenis_komisi = 'Nominal';
                $value->komisi = Main::format_money($value->komisi);
            }
            $value->komisi_nominal = Main::format_money($value->komisi_nominal);
            if ($value->status == 'belum_terbayar') {
                $value->status = 'Belum Dibayar';
                $value->updated_at = null;
            } else {
                $value->status = 'Terbayar';
            }
        }

        $data = [
            'list' => $list,
            'count' => $count,
            'status_list' => $status_list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanKomisiLangganan/laporanKomisiLanggananExcel', $data);
    }
}
