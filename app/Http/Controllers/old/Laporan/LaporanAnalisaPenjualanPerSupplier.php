<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPenjualan;
use app\Models\Bayu\mSalesman;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanAnalisaPenjualanPerSupplier extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'supplier' => ["search_field" => "supplier"],
        'golongan' => ["search_field" => "golongan"],
        'barang' => ["search_field" => "barang"],
        'kode_barang' => ["search_field" => "kode_barang"],
        'qty_jual' => ["search_field" => "qty_jual"],
        'nilai_jual' => ["search_field" => "nilai_jual"],
        'nilai_ppn' => ["search_field" => "nilai_jual"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_analisa_penjualan_5'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['uri'] = '?' . $key . '=' . $value;
                } else {
                    $data['uri'] .= '&' . $key . '=' . $value;
                }
            }
        }
//        dd($data['uri']);

        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['golongan'] = mGolongan::all();
        $data['select_golongan'] = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $data['supplier'] = mSupplier::all();
        $data['select_supplier'] = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        return view('laporan/laporanAnalisaPenjualanPerSupplier/laporanAnalisaPenjualanPerSupplierList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian = new mPenjualan();

        $result['iTotalRecords'] = count($pembelian->count_all_analisa_per_supplier($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier));
        $result['iTotalDisplayRecords'] = count($pembelian->count_filter_analisa_per_supplier($query, $this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pembelian->list_analisa_per_supplier($start, $length, $query, $this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->qty_jual = Main::format_number($value->qty_jual);
            $value->nilai_jual = Main::format_money($value->nilai_jual);
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
        }

//        dd($data);
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian = new mPenjualan();

        $list = $pembelian->export_analisa_per_supplier($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = mLokasi::select('lokasi')->where('id', $select_lokasi)->first()->lokasi;
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = mGolongan::select('golongan')->where('id', $select_golongan)->first()->golongan;
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = mGolongan::select('supplier')->where('id', $select_supplier)->first()->supplier;
        }

        foreach ($list as $value) {
            $value->qty_jual = Main::format_number($value->qty_jual);
            $value->nilai_jual = Main::format_money($value->nilai_jual);
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);
//        dd($data);

        return view('laporan/laporanAnalisaPenjualanPerSupplier/laporanAnalisaPenjualanPerSupplierExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;
        $select_supplier = $request->input('id_supplier') ? $request->input('id_supplier') : null;

        $pembelian = new mPenjualan();

        $list = $pembelian->export_analisa_per_supplier($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier);

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = mLokasi::select('lokasi')->where('id', $select_lokasi)->first()->lokasi;
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = mGolongan::select('golongan')->where('id', $select_golongan)->first()->golongan;
        }

        if (empty($select_supplier)) {
            $nama_supplier = 'Seluruh Supplier';
        } else {
            $nama_supplier = mGolongan::select('supplier')->where('id', $select_supplier)->first()->supplier;
        }

        foreach ($list as $key => $value) {
            $value->qty_jual = Main::format_number($value->qty_jual);
            $value->nilai_jual = Main::format_money($value->nilai_jual);
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
        }


        $data = array_merge($data, [
            'list' => $list,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'nama_supplier' => $nama_supplier ? $nama_supplier : null,
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanAnalisaPenjualanPerSupplier/laporanAnalisaPenjualanPerSupplierPdf', $data);

//        return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Analisa Penjualan Per Supplier ' . $nama_supplier . ' ' . $tanggal_start . 'S/d' . $tanggal_end);
    }
}
