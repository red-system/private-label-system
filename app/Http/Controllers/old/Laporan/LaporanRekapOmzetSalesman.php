<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanRekapOmzetSalesman extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'salesman' => ["search_field" => "tb_salesman.salesman"],
        'qty' => ["search_field" => "tb_item_penjualan.jml_barang_penjualan"],
        'total' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'dibayar_tepat_waktu' => ["search_field" => "tb_penjualan.grand_total_penjualan"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_omzet_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);

        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_lokasi' => $id_lokasi,
        ];
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['params'] = $params;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanRekapOmzetSalesman/laporanRekapOmzetSalesmanList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $result['iTotalRecords'] = $penjualan->count_all_rekap_omzet_salesman();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_rekap_omzet_salesman($query, $this->detail, $date_start, $date_end, $id_lokasi)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_rekap_omzet_salesman($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        foreach ($data as $value) {
            $value->total = Main::format_money($value->total);
            $value->dibayar_tepat_waktu = Main::format_money($value->dibayar_tepat_waktu);
            $value->qty = Main::format_decimal($value->qty).' '.$value->satuan;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_rekap_omzet_salesman($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_lokasi);
        foreach ($list as $value) {
            $value->total = Main::format_money($value->total);
            $value->dibayar_tepat_waktu = Main::format_money($value->dibayar_tepat_waktu);
            $value->qty = Main::format_decimal($value->qty).' '.$value->satuan;
        }

        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanRekapOmzetSalesman/laporanRekapOmzetSalesmanPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Rekap Omzet Salesman '  .$date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_rekap_omzet_salesman($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_lokasi);
        foreach ($list as $value) {
            $value->total = Main::format_money($value->total);
            $value->dibayar_tepat_waktu = Main::format_money($value->dibayar_tepat_waktu);
            $value->qty = Main::format_decimal($value->qty).' '.$value->satuan;
        }
        $data = [
            'list' => $list,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanRekapOmzetSalesman/laporanRekapOmzetSalesmanExcel', $data);
    }
}
