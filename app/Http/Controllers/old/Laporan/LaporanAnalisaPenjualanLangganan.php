<?php

namespace app\Http\Controllers\Laporan;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mGolongan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mPiutangLangganan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LaporanAnalisaPenjualanLangganan extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'wilayah' => ["search_field" => "tb_wilayah.wilayah"],
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'grand_total_penjualan' => ["search_field" => "tb_penjualan.grand_total_penjualan"],
        'retur' => ["search_field" => "tb_payment_piutang_langganan.jumlah"],
        'net_penjualan' => ["search_field" => "tb_payment_piutang_langganan.jumlah"],
        'pembayaran' => ["search_field" => "tb_payment_piutang_langganan.jumlah"],


    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['sub_analisa_penjualan_9'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $id_golongan = $request->input('id_golongan');
        $id_golongan = $id_golongan ? $id_golongan : 'all';

        $id_barang = $request->input('id_barang');
        $id_barang = $id_barang ? $id_barang : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);

        $data = Main::data($this->breadcrumb);
        $detail = array();
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'id_barang' => $id_barang,
            'id_lokasi' => $id_lokasi,
            'id_golongan' => $id_golongan,
        ];
        $data['golongan'] = mGolongan::orderBy('golongan', 'ASC')->get();
        $data['lokasi'] = mLokasi::orderBy('lokasi', 'ASC')->get();
        $data['barang'] = mBarang::orderBy('nama_barang', 'ASC')->get();
        $data['params'] = $params;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['id_lokasi'] = $id_lokasi;
        $data['id_golongan'] = $id_golongan;
        $data['id_barang'] = $id_barang;
        $data['detail'] = json_encode($detail);
        return view('laporan/laporanAnalisaPenjualanLangganan/laporanAnalisaPenjualanLanggananList', $data);
    }

    function list(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $result['iTotalRecords'] = $penjualan->count_all_analisa_penjualan_langganan()->count();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter_analisa_penjualan_langganan($query, $this->detail, $date_start, $date_end, $id_lokasi)->count();
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list_analisa_penjualan_langganan($start, $length, $query, $this->detail, $date_start, $date_end, $id_lokasi);
        foreach ($data as $value) {
            $value->grand_total_penjualan = Main::format_money($value->grand_total_penjualan);
            $value->net_penjualan = Main::format_money($value->net_penjualan);
            if ($value->retur == 0) {
                $value->retur = null;
            } else {
                $value->retur = Main::format_money($value->retur);
            }
            if ($value->pembayaran == 0) {
                $value->pembayaran = null;
            } else {
                $value->pembayaran = Main::format_money($value->pembayaran);
            }

        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_analisa_penjualan_langganan($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_lokasi);

        $wilayah = DB::table('tb_penjualan')->leftjoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('tb_lokasi.id_wilayah')->get();
        $data = [
            'list' => $list,
            'wilayah' => $wilayah,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanAnalisaPenjualanLangganan/laporanAnalisaPenjualanLanggananPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laporan Analisa Penjualan Langganan ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';

        $date_start = $request->input('date_start');
        $date_start = $date_start ? $date_start : mPenjualan::orderBy('tgl_penjualan', 'ASC')->value('tgl_penjualan');
        $date_start = Main::format_date($date_start);
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $request->input('date_end');
        $date_end = $date_end ? $date_end : mPenjualan::orderBy('tgl_penjualan', 'DESC')->value('tgl_penjualan');
        $date_end = Main::format_date($date_end);
        $date_end_db = Main::format_date_db($date_end);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPiutangLangganan();
        $list = $penjualan->list_analisa_penjualan_langganan($start, $length, $query, $this->detail, $date_start_db, $date_end_db, $id_lokasi);

        $wilayah = DB::table('tb_penjualan')->leftjoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('tb_lokasi.id_wilayah')->get();
        $data = [
            'list' => $list,
            'wilayah' => $wilayah,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'company' => Main::companyInfo()
        ];

        return view('laporan/laporanAnalisaPenjualanLangganan/laporanAnalisaPenjualanLanggananExcel', $data);
    }
}
