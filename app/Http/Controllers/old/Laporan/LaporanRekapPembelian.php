<?php

namespace app\Http\Controllers\Laporan;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mHutangSupplier;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mPembelian;
use app\Models\Bayu\mPembelianBarang;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mGolongan;

use app\Models\Bayu\mSupplier;
use app\Models\Bayu\mWilayah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


use PDF;

class LaporanRekapPembelian extends Controller
{
//    TODO : tanyakan ke bli deva apakah benar data nilai yang diambil itu dari sub_total
    private $breadcrumb;
    private $menuActive;
    private $datetime;
    private $view = [
        'golongan' => ["search_field" => "golongan"],
        'qty' => ["search_field" => "qty"],
        'nilai' => ["search_field" => "nilai"],
        'nilai_persen' => ["search_field" => "nilai"],
        'nilai_ppn' => ["search_field" => "ppn"],
        'nilai_ppn_persen' => ["search_field" => "ppn"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['laporan'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => ''
            ],
            [
                'label' => $cons['sub_pembelian_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $data['view'] = json_encode($view);
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['uri'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['uri'] = '?' . $key . '=' . $value;
                } else {
                    if ($key == 'id_golongan') {
                        foreach ($value as $golongan) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $golongan;
                        }
                    } elseif ($key == 'id_lokasi') {
                        foreach ($value as $lokasi) {
                            $data['uri'] .= '&' . $key . '%5B%5D=' . $lokasi;
                        }
                    } else {
                        $data['uri'] .= '&' . $key . '=' . $value;
                    }
                }
            }
        }

        $data['date_start'] = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $data['date_end'] = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $data['lokasi'] = mLokasi::all();
        $data['select_lokasi'] = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $data['golongan'] = mGolongan::all();
        $data['select_golongan'] = $request->input('id_golongan') ? $request->input('id_golongan') : null;


        return view('laporan/laporanRekapPembelian/laporanRekapPembelianList', $data);
    }

    function list(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');

        $total_nilai = 0;
        $total_ppn = 0;

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;

        $pembelian = new mPembelian();

        $result['iTotalRecords'] = count($pembelian->count_all_rekap($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan));
        $result['iTotalDisplayRecords'] = count($pembelian->count_filter_rekap($query, $this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan));
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pembelian->list_rekap($start, $length, $query, $this->view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan);

        foreach ($data as $value) {
            $total_nilai += $value->nilai;
            $total_ppn += $value->ppn;
        }

        $total_nilai_ppn = $total_nilai + $total_ppn;

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->nilai_persen = Main::format_discount(Main::format_number(($value->nilai / $total_nilai) * 100));
            $value->nilai_ppn = $value->nilai + $value->ppn;
            $value->nilai_ppn_persen = Main::format_discount(Main::format_number(($value->nilai_ppn / $total_nilai_ppn) * 100));
            $value->nilai = Main::format_money($value->nilai);
            $value->qty = Main::format_number($value->qty);
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
        }

        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function excel(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_nilai = 0;
        $total_ppn = 0;
        $total_nilai_kirim = 0;
        $total_nilai_ppn_kirim = 0;
        $total_nilai_persen_kirim = 0;
        $total_nilai_ppn_persen_kirim = 0;

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;

        $pembelian = new mPembelian();

        $list = $pembelian->export_rekap($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan);

        foreach ($list as $value) {
            $total_nilai += $value->nilai;
            $total_ppn += $value->ppn;
        }

        $total_nilai_ppn = $total_nilai + $total_ppn;

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value) {
                if ($value == 0) {
                    array_push($nama_lokasi, 'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = array();
            foreach ($select_golongan as $value) {
                if ($value == 0) {
                    array_push($nama_golongan, 'Seluruh Golongan');
                } else {
                    $nama = mGolongan::where('id', $value)->value('golongan');
                    array_push($nama_golongan, $nama);
                }
            }
        }

        foreach ($list as $value) {
            $value->nilai_persen = Main::format_discount(Main::format_number(($value->nilai / $total_nilai) * 100));
            $value->nilai_ppn = $value->nilai + $value->ppn;
            $value->nilai_ppn_persen = Main::format_discount(Main::format_number(($value->nilai_ppn / $total_nilai_ppn) * 100));

            $total_nilai_kirim = floatval($total_nilai_kirim) + floatval($value->nilai);
            $total_nilai_ppn_kirim = floatval($total_nilai_ppn_kirim) + floatval($value->nilai_ppn);
            $total_nilai_persen_kirim = floatval($total_nilai_persen_kirim) + floatval($value->nilai_persen);
            $total_nilai_ppn_persen_kirim = floatval($total_nilai_ppn_persen_kirim) + floatval($value->nilai_ppn_persen);

            $value->nilai = Main::format_money($value->nilai);
            $value->qty = Main::format_number($value->qty);
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
        }

        $data = array_merge($data, [
            'list' => $list,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'total_nilai_kirim' => Main::format_money($total_nilai_kirim),
            'total_nilai_ppn_kirim' => Main::format_money($total_nilai_ppn_kirim),
            'total_nilai_persen_kirim' => Main::format_discount($total_nilai_persen_kirim),
            'total_nilai_ppn_persen_kirim' => Main::format_discount($total_nilai_ppn_persen_kirim),
            'company' => Main::companyInfo(),
        ]);

        return view('laporan/laporanRekapPembelian/laporanRekapPembelianExcel', $data);
    }

    function pdf(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $total_nilai = 0;
        $total_ppn = 0;
        $total_nilai_kirim = 0;
        $total_nilai_ppn_kirim = floatval(0);
        $total_nilai_persen_kirim = 0;
        $total_nilai_ppn_persen_kirim = 0;

        $date_start = $request->input('date_start') ? $request->input('date_start') : date('d-m-Y', strtotime('-30 days', strtotime(date('d-m-Y'))));
        $tanggal_start = Main::format_date_db($date_start);
        $date_end = $request->input('date_end') ? $request->input('date_end') : date('d-m-Y');
        $tanggal_end = Main::format_date_db($date_end);
        $select_lokasi = $request->input('id_lokasi') ? $request->input('id_lokasi') : null;
        $select_golongan = $request->input('id_golongan') ? $request->input('id_golongan') : null;

        $pembelian = new mPembelian();

        $list = $pembelian->export_rekap($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan);

        foreach ($list as $value) {
            $total_nilai += $value->nilai;
            $total_ppn += $value->ppn;
        }

        $total_nilai_ppn = $total_nilai + $total_ppn;

        if (empty($select_lokasi)) {
            $nama_lokasi = 'Seluruh Lokasi';
        } else {
            $nama_lokasi = array();
            foreach ($select_lokasi as $value) {
                if ($value == 0) {
                    array_push($nama_lokasi, 'Seluruh Lokasi');
                } else {
                    $nama = mLokasi::where('id', $value)->value('lokasi');
                    array_push($nama_lokasi, $nama);
                }
            }
        }

        if (empty($select_golongan)) {
            $nama_golongan = 'Seluruh Golongan';
        } else {
            $nama_golongan = array();
            foreach ($select_golongan as $value) {
                if ($value == 0) {
                    array_push($nama_golongan, 'Seluruh Golongan');
                } else {
                    $nama = mGolongan::where('id', $value)->value('golongan');
                    array_push($nama_golongan, $nama);
                }
            }
        }

        foreach ($list as $key => $value) {
            $value->nilai_persen = Main::format_number(($value->nilai / $total_nilai) * 100);
            $value->nilai_ppn = $value->nilai + $value->ppn;
            $value->nilai_ppn_persen = Main::format_number(($value->nilai_ppn / $total_nilai_ppn) * 100);

            $total_nilai_kirim = floatval($total_nilai_kirim) + floatval($value->nilai);
            $total_nilai_ppn_kirim = floatval($total_nilai_ppn_kirim) + floatval($value->nilai_ppn);
            $total_nilai_persen_kirim = floatval($total_nilai_persen_kirim) + $value->nilai_persen;
            $total_nilai_ppn_persen_kirim = floatval($total_nilai_ppn_persen_kirim) + $value->nilai_ppn_persen;

            $value->nilai = Main::format_money($value->nilai);
            $value->qty = Main::format_number($value->qty);
            $value->nilai_ppn = Main::format_money($value->nilai_ppn);
            $value->nilai_persen = Main::format_discount($value->nilai_persen);
            $value->nilai_ppn_persen = Main::format_discount($value->nilai_ppn_persen);
        }


        $data = array_merge($data, [
            'list' => $list,
            'tanggal_start' => Main::format_date($tanggal_start),
            'tanggal_end' => Main::format_date($tanggal_end),
            'nama_lokasi' => $nama_lokasi ? $nama_lokasi : null,
            'nama_golongan' => $nama_golongan ? $nama_golongan : null,
            'total_nilai_kirim' => Main::format_money($total_nilai_kirim),
            'total_nilai_ppn_kirim' => Main::format_money($total_nilai_ppn_kirim),
            'total_nilai_persen_kirim' => Main::format_discount($total_nilai_persen_kirim),
            'total_nilai_ppn_persen_kirim' => Main::format_discount($total_nilai_ppn_persen_kirim),
            'company' => Main::companyInfo(),
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('laporan/laporanRekapPembelian/laporanRekapPembelianPdf', $data);

//        return $pdf->setPaper('A4', 'landscape')->stream();

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Laporan Pembelian Jatuh Tempo ' . $tanggal_start . 'S/d' . $tanggal_end);
    }
}
