<?php

namespace app\Http\Controllers\Transaksi;

use app\Models\Widi\mGolongan;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSalesman;
use app\Models\Widi\mStok;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


use app\Models\Widi\mSO;
use app\Models\Widi\mItemSO;

class SO extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_so' => ["search_field" => "tb_so.no_so"],
        'tgl_so' => ["search_field" => "tb_so.tgl_so"],
        'pelanggan_sok' => ["search_field" => "tb_so.pelanggan_so"],
        'alamat_pengiriman' => ["search_field" => "tb_so.alamat_pengiriman"],
        'keterangan_so' => ["search_field" => "tb_so.keterangan_so"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('transaksi/salesOrder/soList', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * fungsi untuk menampilkan detail dari sales order
     */
    function detail($id)
    {
        $so = mSO::where('id', $id)->first();
        $salesman = mSalesman::where('id', $so->id_salesman)->value('salesman');
        $item_so = new mItemSO;
        $item = $item_so->list_item_so($id);
        $data = [
            'so' => $so,
            'item' => $item,
            'salesman' => $salesman,
        ];
        return view('transaksi/salesOrder/modalBodyDetailSo', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $so = new mSO();
        $result['iTotalRecords'] = $so->count_all();
        $result['iTotalDisplayRecords'] = $so->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $so->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $date = Main::format_date($value->tgl_so);
            $value->tgl_so = $date;
            $value->route['delete'] = route('salesOrderDelete', ['id' => $value->id]);
            $value->route['detail_view'] = route('salesOrderDetail', ['id' => $value->id]);
            $value->route['edit'] = route('salesOrderEdit', ['id' => $value->id]);
            $value->direct['edit'] = route('salesOrderUpdate', ['id' => $value->id]);
            $value->ignore['detail'] = true;
            if ($value->penjualan == "kosong") {
                $value->route['penjualan'] = route('penjualanCreate', ['id' => $value->id]);
                $value->direct['penjualan'] = route('penjualanCreate', ['id' => $value->id]);
            } else {
                $value->visibility['penjualan'] = 'none';
                $value->visibility['edit'] = 'none';
                $value->visibility['delete'] = 'none';
            };
            if ($value->surat_jalan == "kosong") {
                $value->route['surat_jalan'] = route('suratJalanCreate', ['id' => $value->id]);
                $value->direct['surat_jalan'] = route('suratJalanCreate', ['id' => $value->id]);
            } else {
                $value->label['surat_jalan'] = 'Cetak Surat Jalan';
                $value->icon['surat_jalan'] = 'fa fa-print';
                $value->target_blank['surat_jalan'] = 'ada';
                $value->route['surat_jalan'] = route('suratJalanPdf', ['id' => $value->id]);
                $value->visibility['edit'] = 'none';
                $value->visibility['delete'] = 'none';
            }


        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function create()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_1'],
                'route' => ''
            ],
            [
                'label' => 'Tambah Data',
                'route' => ''
            ]
        ];
        $data = Main::data($this->breadcrumb, 'sales_order');
        $pageTitle = 'Tambah Data';
        $langganan = mLangganan::orderBy('langganan', ' ASC')->get();
        $salesman = mSalesman::where('salesman_suspended', '0')->orWhere('salesman_suspended', '')->orderBy('salesman', ' ASC')->get();
        $lokasi = mLokasi::all();
        $barang = mBarang::withCount([
            'stok_barang AS total_stok_barang' => function ($query) {
                $query->select(DB::raw("SUM(jml_barang) AS qty_sum"));
            }
        ])
            ->orderBy('kode_barang', 'ASC')
            ->get();


        $data = array_merge($data, [
            'langganan' => $langganan,
            'salesman' => $salesman,
            'barang' => $barang,
            'pageTitle' => $pageTitle,
            'lokasi' => $lokasi
        ]);
        return view('transaksi/salesOrder/soCreate', $data);
    }

    /**
     * @param Request $id
     * @return array
     * fungsi untuk membuat nomor sales order dari sales_order.js
     */
    function no_so(Request $id)
    {
        $user = Session::get('user');
        $id_user = $user->id;
        $id_langganan = $id->id_langganan;
        $inisial_karyawan = $user->inisial_karyawan;
        $langganan = mLangganan::where('id', $id_langganan)->first();
        /**
         * proses menghitung urutan untuk nomor sales order
         * data didapat melalui fungsi countNoSo di model mSO dengan parameter $id_user dan $id_langganan
         */
        $so = new mSO;
        $no = $so->countNoSo($id_user, $id_langganan);
        $urutan = $no + 1;
        $year = date('y');
        $month = date('m');
        $date = date('d');
        $no_so = 'SO.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $langganan->kode_langganan . '.' . $urutan;

        $data_response = [
            'no_so' => $no_so,
            'urutan' => $urutan,
        ];
        /**
         * data di return ke sales_order.js
         */
        return $data_response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * proses untuk melihat stok dari data barang dari sales_order.js
     */
    function so_stok(Request $request)
    {
        $id_barang = $request->input('id_barang');
        $produkStok = mStok
            ::with('lokasi')
            ->where([
                'id_barang' => $id_barang,
            ])
            ->where('jml_barang', '>', 0)
            ->get();

        $data = [
            'produkStok' => $produkStok,
            'no' => 1
        ];

        return view('transaksi/salesOrder/tbodyProdukStok', $data);
    }


    function produk_harga(Request $request)
    {
        $id_produk = $request->input('id_barang');


        $barang = mBarang
            ::find($id_produk);

        $harga = $barang->harga_jual_barang;


        $data = [
            'number_raw' => $harga,
            'number_format' => Main::format_number($harga),
        ];

        return $data;
    }

    function check_stok_produk($id_stok_produk_arr, $qty_arr)
    {
        $valid = TRUE;
        foreach ($id_stok_produk_arr as $index => $id_stok_produk) {
            $stok_produk_qty = mStok::where('id', $id_stok_produk)->value('jml_barang');
            $jual_qty = $qty_arr[$index];

            if ($stok_produk_qty < $jual_qty) {
                $valid = FALSE;
                break;
            }
        }

        return $valid;
    }

    function produk_lokasi(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_barang = $request->input('id_barang');

        $lokasi = mStok
            ::with('lokasi')
            ->distinct()
            ->where([
                'id_barang' => $id_barang,
            ])
            ->get(['id', 'id_lokasi', 'jml_barang', 'satuan', 'id_satuan']);
        return $lokasi;

    }

    function insert(Request $request)
    {
        $rules = [
            'id_langganan' => 'required',
            'id_salesman' => 'required',
            'id_lokasi' => 'required',
            'id_lokasi.*' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'id_lokasi_penjualan' => 'required',
            'metode_pembayaran' => 'required'
        ];

        $attributes = [
            'id_langganan' => 'Langganan',
            'id_salesman' => 'Salesman',
            'id_barang' => 'Produk',
            'id_lokasi' => 'Lokasi Barang',
            'id_lokasi_penjualan' => 'Lokasi Order',
            'metode_pembayaran' => 'Metode Pembayaran'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Produk ke-' . $next;
            $attr['id_lokasi.' . $i] = 'Lokasi ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * data yang disimpan ke tabel sales order
         */
        $id_langganan = $request->input('id_langganan');
        $id_salesman = $request->input('id_salesman');
        $urutan = $request->input('urutan');
        $no_so = $request->input('no_so');
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $tanggal_kirim = $request->input('tanggal_kirim');
        $tanggal_kirim = date('Y-m-d', strtotime($tanggal_kirim));
        $langganan = $request->input('nama_langganan');
        $alamat = $request->input('alamat_pengiriman');
        $penerima = $request->input('penerima');
        $keterangan = $request->input('keterangan');
        $total = Main::string_to_number($request->input('total'));
        $diskon_persen_akhir = Main::string_to_number($request->input('diskon_persen_akhir'));
        $diskon_persen = $diskon_persen_akhir * $total / 100;
        $diskon_nominal = Main::string_to_number($request->input('potongan_akhir'));
        $pajak_persen = Main::string_to_number($request->input('pajak'));
        $pajak_nominal = $pajak_persen * ($total - $diskon_nominal - $diskon_persen) / 100;
        $ongkos_kirim = Main::string_to_number($request->input('ongkos_kirim'));
        $grand_total = Main::string_to_number($request->input('grand_total'));
        $dp = Main::string_to_number($request->input('bayar'));
        $id_lokasi_penjualan = $request->input('id_lokasi_penjualan');
        $metode_pembayaran = $request->input('metode_pembayaran');
        $user = Session::get('user');
        $id_user = $user->id;


        /**
         * data yang disimpan di tabel item sales order
         */
        $id_barang_arr = $request->input('id_barang');
        $harga_arr = $request->input('harga');
        $diskon_persen_satuan_arr = $request->input('diskon_persen');
        $diskon_nominal_satuan_arr = $request->input('diskon_nominal');
        $sub_total_arr = $request->input('sub_total');
        $satuan_arr = $request->input('satuan');
//        dd($satuan_arr);
        $id_satuan_arr = $request->input('id_satuan');
        $id_stok_produk_arr = $request->input('id_stok');
        $qty_arr = $request->input('qty');
        $id_lokasi_arr = $request->input('id_lokasi');
        $harga_net_arr = $request->input('harga_net');


        /**
         * Check stok produk dengan jumlah yang dijual
         */
        $check_stok_produk = $this->check_stok_produk($id_stok_produk_arr, $qty_arr);
        if (!$check_stok_produk) {
            return response([
                'message' => 'Stok Produk yang akan dijual <strong>Tidak Mencukupi</strong>'
            ], 422);
        }
        DB::beginTransaction();
        try {
            $data_so = [
                'urutan' => $urutan,
                'no_so' => $no_so,
                'tgl_so' => $tanggal,
                'tgl_kirim_so' => $tanggal_kirim,
                'pelanggan_so' => $langganan,
                'alamat_pengiriman' => $alamat,
                'penerima' => $penerima,
                'keterangan_so' => $keterangan,
                'total_so' => $total,
                'disc_persen_so' => $diskon_persen_akhir,
                'disc_nominal_so' => $diskon_nominal,
                'pajak_persen_so' => $pajak_persen,
                'pajak_nominal_so' => $pajak_nominal,
                'ongkos_kirim_so' => $ongkos_kirim,
                'grand_total_so' => $grand_total,
                'dp_so' => $dp,
                'metode_pembayaran_dp_so' => $metode_pembayaran,
                'id_lokasi' => $id_lokasi_penjualan,
                'id_user' => $id_user,
                'id_langganan' => $id_langganan,
                'id_salesman' => $id_salesman,
                'surat_jalan' => 'kosong',
                'penjualan' => 'kosong'
            ];

            $response = mSO::create($data_so);
            $id_so = $response->id;

            foreach ($id_barang_arr as $index => $id_produk) {
                $barang = mBarang::where('id', $id_produk)->first();
                $golongan = mGolongan::where('id', $barang->id_golongan)->first();
                $id_lokasi = $id_lokasi_arr[$index];
                $id_stok_produk = $id_stok_produk_arr[$index];
                $qty = Main::string_to_number($qty_arr[$index]);
                $harga = Main::string_to_number($harga_arr[$index]);
                $diskon_nominal_satuan = Main::string_to_number($diskon_nominal_satuan_arr[$index]);
                $diskon_persen_satuan = Main::string_to_number($diskon_persen_satuan_arr[$index]);
                $id_satuan = $id_satuan_arr[$index];
                $satuan = $satuan_arr[$index];
                $sub_total = Main::string_to_number($sub_total_arr[$index]);
                $harga_net = Main::string_to_number($harga_net_arr[$index]);

                /**
                 * data yang akan disimpan ke tabel item sales order
                 */
                $data_penjualan_produk = [
                    'id_so' => $id_so,
                    'id_barang' => $id_produk,
                    'id_stok' => $id_stok_produk,
                    'id_lokasi' => $id_lokasi,
                    'id_satuan' => $id_satuan,
                    'nama_barang_so' => $barang->nama_barang,
                    'jml_barang_so' => $qty,
                    'golongan_so' => $golongan->golongan,
                    'satuan_so' => $satuan,
                    'harga_so' => $harga,
                    'disc_persen_so' => $diskon_persen_satuan,
                    'disc_nominal_so' => $diskon_nominal_satuan,
                    'harga_net' => $harga_net,
                    'subtotal' => $sub_total
                ];

                mItemSO::create($data_penjualan_produk);
            }
            DB::commit();

        } catch (\ErrorException $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function delete($id)
    {
        mSO::where('id', $id)->delete();
        mItemSO::where('id_so', $id)->delete();
    }

    function edit($id)
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_1'],
                'route' => ''
            ],
            [
                'label' => 'Edit Data',
                'route' => ''
            ]
        ];
        $data = Main::data($this->breadcrumb, 'sales_order');
        $pageTitle = 'Tambah Data';
        $id_so = $id;
        $so = mSO::where('id', $id_so)->first();
        $pelanggan = mLangganan::where('id', $so->id_langganan)->first();
        $salesman = mSalesman::where('id', $so->id_salesman)->value('salesman');

        /**
         * join tabel item sales order dengan tabel barang dan stok
         */
        $item_so = mItemSO
            ::select(
                'tb_item_so.*',
                'tb_barang.nama_barang',
                'tb_barang.harga_jual_barang',
                'tb_stok.jml_barang'
            )
            ->leftJoin('tb_barang', 'tb_item_so.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_item_so.id_stok', '=', 'tb_stok.id')
            ->where('id_so', $id_so)
            ->get();

        foreach ($item_so as $item){
            $item->gudang = mStok
                ::with('lokasi')
                ->distinct()
                ->where([
                    'id_barang' => $item->id_barang,
                ])
                ->get(['id','id_lokasi','jml_barang','satuan', 'id_satuan']);
        }
        $langganan = mLangganan::orderBy('langganan', ' ASC')->get();
        $lokasi = mLokasi::all();

        /**
         * menjumlahkan jumlah barang
         */
        $barang = mBarang::withCount([
            'stok_barang AS total_stok_barang' => function ($query) {
                $query->select(DB::raw("SUM(jml_barang) AS qty_sum"));
            }
        ])
            ->orderBy('kode_barang', 'ASC')
            ->get();


        $data = array_merge($data, [
            'so' => $so,
            'item_so' => $item_so,
            'pelanggan' => $pelanggan,
            'langganan' => $langganan,
            'barang' => $barang,
            'pageTitle' => $pageTitle,
            'lokasi' => $lokasi,
            'salesman' => $salesman
        ]);

        return view('transaksi/salesOrder/edit/soEdit', $data);
    }

    function update(Request $request, $id)
    {
        $id_so = $id;
        $rules = [
            'nama_langganan' => 'required',
            'id_lokasi_penjualan' => 'required',
            'id_lokasi.*' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'metode_pembayaran' => 'required'
        ];

        $attributes = [
            'nama_langganan' => 'Langganan',
            'id_barang' => 'Produk',
            'id_lokasi' => 'Lokasi Barang',
            'lokasi_penjualan' => 'Lokasi Order',
            'metode_pembayaran' => 'Metode Pembayaran'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Produk ke-' . $next;
            $attr['id_lokasi.' . $i] = 'Lokasi ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        /**
         * data yang disimpan ke tabel sales order
         */
        $no_so = $request->input('no_so');
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $tanggal_kirim = $request->input('tanggal_kirim');
        $tanggal_kirim = date('Y-m-d', strtotime($tanggal_kirim));
        $langganan = $request->input('nama_langganan');
        $alamat = $request->input('alamat_pengiriman');
        $penerima = $request->input('penerima');
        $keterangan = $request->input('keterangan');
        $total = Main::string_to_number($request->input('total'));
        $diskon_persen_akhir = Main::string_to_number($request->input('diskon_persen_akhir'));
        $diskon_persen = $diskon_persen_akhir * $total / 100;
        $diskon_nominal = Main::string_to_number($request->input('potongan_akhir'));
        $pajak_persen = Main::string_to_number($request->input('pajak'));
        $pajak_nominal = $pajak_persen * ($total - $diskon_nominal - $diskon_persen) / 100;
        $ongkos_kirim = Main::string_to_number($request->input('ongkos_kirim'));
        $grand_total = Main::string_to_number($request->input('grand_total'));
        $dp = Main::string_to_number($request->input('bayar'));
        $metode_pembayaran = $request->input('metode_pembayaran');
        $id_lokasi_penjualan = $request->input('id_lokasi_penjualan');

        /**
         * data yang disimpan di tabel item sales order
         */
        $id_barang_arr = $request->input('id_barang');
        $harga_arr = $request->input('harga');
        $diskon_persen_satuan_arr = $request->input('diskon_persen');
        $diskon_nominal_satuan_arr = $request->input('diskon_nominal');
        $sub_total_arr = $request->input('sub_total');
        $satuan_arr = $request->input('satuan');
        $id_satuan_arr = $request->input('id_satuan');
        $id_stok_produk_arr = $request->input('id_stok');
        $qty_arr = $request->input('qty');
        $id_lokasi_arr = $request->input('id_lokasi');
        $harga_net_arr = $request->input('harga_net');


        /**
         * Check stok produk dengan yang dijual
         */
        $check_stok_produk = $this->check_stok_produk($id_stok_produk_arr, $qty_arr);
        if (!$check_stok_produk) {
            return response([
                'message' => 'Stok Produk yang akan dijual <strong>Tidak Mencukupi</strong>'
            ], 422);
        }
        DB::beginTransaction();
        try {

            /**
             * data yang akan disimpan ke tabel sales order
             */
            $data_so = [
                'no_so' => $no_so,
                'tgl_so' => $tanggal,
                'tgl_kirim_so' => $tanggal_kirim,
                'pelanggan_so' => $langganan,
                'alamat_pengiriman' => $alamat,
                'penerima' => $penerima,
                'keterangan_so' => $keterangan,
                'total_so' => $total,
                'disc_persen_so' => $diskon_persen_akhir,
                'disc_nominal_so' => $diskon_nominal,
                'pajak_persen_so' => $pajak_persen,
                'pajak_nominal_so' => $pajak_nominal,
                'ongkos_kirim_so' => $ongkos_kirim,
                'grand_total_so' => $grand_total,
                'dp_so' => $dp,
                'metode_pembayaran_dp_so' => $metode_pembayaran,
                'id_lokasi' => $id_lokasi_penjualan,
                'surat_jalan' => 'kosong',
                'penjualan' => 'kosong'
            ];

            /**
             * update data ke tabel sales order
             */
            mSO::where('id', $id_so)->update($data_so);

            /**
             * hapus data di tabel item sales order dengan id dari tabel sales order yang baru saja diupdate
             */
            mItemSO::where('id_so', $id_so)->delete();
            foreach ($id_barang_arr as $index => $id_produk) {
                $barang = mBarang::where('id', $id_produk)->first();
                $golongan = mGolongan::where('id', $barang->id_golongan)->first();
                $id_lokasi = $id_lokasi_arr[$index];
                $id_stok_produk = $id_stok_produk_arr[$index];
                $qty = Main::string_to_number($qty_arr[$index]);
                $harga = Main::string_to_number($harga_arr[$index]);
                $diskon_nominal_satuan = Main::string_to_number($diskon_nominal_satuan_arr[$index]);
                $diskon_persen_satuan = Main::string_to_number($diskon_persen_satuan_arr[$index]);
                $satuan = $satuan_arr[$index];
                $id_satuan = $id_satuan_arr[$index];
                $sub_total = Main::string_to_number($sub_total_arr[$index]);
                $harga_net = Main::string_to_number($harga_net_arr[$index]);

                /**
                 * data yang akan disimpan di tabel item sales order
                 */
                $data_penjualan_produk = [
                    'id_so' => $id_so,
                    'id_barang' => $id_produk,
                    'id_stok' => $id_stok_produk,
                    'id_lokasi' => $id_lokasi,
                    'id_satuan' => $id_satuan,
                    'nama_barang_so' => $barang->nama_barang,
                    'jml_barang_so' => $qty,
                    'golongan_so' => $golongan->golongan,
                    'satuan_so' => $satuan,
                    'harga_so' => $harga,
                    'disc_persen_so' => $diskon_persen_satuan,
                    'disc_nominal_so' => $diskon_nominal_satuan,
                    'harga_net' => $harga_net,
                    'subtotal' => $sub_total
                ];

                mItemSO::create($data_penjualan_produk);
            }
            DB::commit();

        } catch (\ErrorException $e) {
            throw $e;
            DB::rollBack();
        }
    }
}
