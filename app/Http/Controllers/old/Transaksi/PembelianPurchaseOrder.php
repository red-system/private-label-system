<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Models\Mahendra\mPembelian;
use app\Models\Mahendra\mPembelianBarang;
use app\Models\Mahendra\mPembelianReturn;
use app\Models\Mahendra\mPembelianReturnBarang;
use app\Models\Mahendra\mPO;
use app\Models\Mahendra\mPoBarang;
use app\Models\Widi\mHutangGiro;
use app\Models\Widi\mHutangSupplier;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mKartuStok;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPiutangLangganan;
use app\Models\Widi\mSalesman;
use app\Models\Widi\mBarang;
use app\Models\Widi\mItemSO;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mSO;
use app\Models\Widi\mStok;
use Carbon\Carbon;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PembelianPurchaseOrder extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_pembelian_label' => ["search_field" => "tb_pembelian.no_pembelian_label"],
        'tanggal_pembelian' => ["search_field" => "tb_pembelian.tanggal_pembelian"],
        'supplier' => ["search_field" => "tb_supplier.supplier"],
        'no_po_label' => ["search_field" => "tb_po.no_po_label"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'total_po' => ["search_field" => "tb_po.total_po"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('transaksi/pembelianPurchaseOrder/pembelianList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $pembelian = new mPembelian();
        $result['iTotalRecords'] = $pembelian->count_all();
        $result['iTotalDisplayRecords'] = $pembelian->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $pembelian->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->tanggal_pembelian = Main::format_date_label($value->tanggal_pembelian);
            $value->total_po = Main::format_money($value->total_po);
            $value->route['detail_view'] = route('purchaseOrderDetail', ['id' => Main::encrypt($value->id_po)]);
            $value->label['detail_view'] = 'Detail Pembelian';
            $value->route['return_pembelian'] = route('returnPembelianPurchaseOrderPage', ['id' => Main::encrypt($value->id_pembelian)]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function create($id)
    {
        $id_po = Main::decrypt($id);
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => 'Penjualan',
                'route' => ''
            ],
            [
                'label' => 'Pembelian Purchase Order',
                'route' => ''
            ]
        ];
        $user = Session::get('user');
        $id_user = $user->id;
        $inisial_karyawan = $user->inisial_karyawan;
        $year = date('y');
        $month = date('m');
        $date = date('d');
        $pembelian = new mPembelian();
        $no_pembelian = $pembelian->countNoPembelian($id_user);
        $no_pembelian = $no_pembelian + 1;
        $no_pembelian_label = 'PB.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $no_pembelian;

        $data = Main::data($this->breadcrumb, $cons['transaksi_5']);
        $pageTitle = 'Pembelian Purchase Order';
        $po = mPO::with(['supplier', 'po_barang', 'po_barang.satuan'])->where('id', $id_po)->first();
        $data = array_merge($data, [
            'no_pembelian' => $no_pembelian,
            'no_pembelian_label' => $no_pembelian_label,
            'po' => $po,
            'pageTitle' => $pageTitle,
        ]);
        return view('transaksi/pembelianPurchaseOrder/pembelianCreate', $data);
    }

    function insert(Request $request, $id)
    {

        $rules = [
            'tanggal_pembelian' => 'required',
            'no_surat_jalan' => 'required',
            'tanggal_surat_jalan' => 'required',
            'pajak_no_faktur' => 'required',
            'tanggal_jatuh_tempo' => 'required',
            'hpp_barang' => 'required',
            'hpp_barang.*' => 'required',
        ];

        $attributes = [
            'tanggal_pembelian' => 'Tanggal Pembelian',
            'no_surat_jalan' => 'Nomer Surat Jalan',
            'tanggal_surat_jalan' => 'Tanggal Surat Jalan',
            'pajak_no_faktur' => 'Pajak nomer Faktur',
            'tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
        ];

        if ($request->input('sisa_pembayaran_po') > 0) {
            $rules['metode_pembayaran_hutang'] = 'required';
            $attributes['metode_pembayaran_hutang'] = 'Metode Pembayaran Hutang';

            if ($request->input('metide_pembayaran_hutang') == 'hutang_giro') {
                $rules['hutang_giro_bank'] = 'required';
                $rules['hutang_giro_bank_cair'] = 'required';
                $attributes['hutang_giro_bank'] = 'Bank Hutang Giro';
                $attributes['hutang_giro_bank_cair'] = 'Tnaggal Pencairan Giro';
            }
        }


        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['hpp_barang.' . $i] = 'HPP Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        DB::beginTransaction();
        try {

            $id_po = Main::decrypt($id);
            $id_pembelian = NULL;
            $po = mPO::with(['lokasi', 'supplier'])->where('id', $id_po)->first();
            $po_barang = mPoBarang::with('satuan')->where('id_po', $id_po)->get();
            $sisa_pembayaran_po = $request->input('sisa_pembayaran_po');
            $no_pembelian = $request->input('no_pembelian');
            $no_pembelian_label = $request->input('no_pembelian_label');
            $tanggal_pembelian = Main::format_date_db($request->input('tanggal_pembelian'));
            $no_surat_jalan = $request->input('no_surat_jalan');
            $tanggal_surat_jalan = Main::format_date_db($request->input('tanggal_surat_jalan'));
            $keterangan_pembelian = $request->input('keterangan_pembelian');
            $pajak_no_faktur = $request->input('pajak_no_faktur');
            $tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));
            $metode_pembayaran_hutang = $request->input('metode_pembayaran_hutang');
            $hutang_giro_bank = $request->input('hutang_giro_bank');
            $hutang_giro_bank_cair = Main::format_date_db($request->input('hutang_giro_bank_cair'));

            $id_po_barang_arr = $request->input('id_po_barang');
            $id_barang_arr = $request->input('id_barang');
            $id_satuan_arr = $request->input('id_satuan');
            $qty_arr = $request->input('qty');
            $satuan_arr = $request->input('satuan');
            $hpp_barang_arr = $request->input('hpp_barang');

            $user = Session::get('user');
            $check_po_location_exist = TRUE;
            $id_user = $user->id;
            $inisial_karyawan = $user->inisial_karyawan;

            $data_pembelian = [
                'id_po' => $id_po,
                'id_user' => $id_user,
                'no_pembelian' => $no_pembelian,
                'no_pembelian_label' => $no_pembelian_label,
                'no_surat_jalan' => $no_surat_jalan,
                'tanggal_pembelian' => $tanggal_pembelian,
                'tanggal_surat_jalan' => $tanggal_surat_jalan,
                'keterangan_pembelian' => $keterangan_pembelian,
                'pajak_no_faktur' => $pajak_no_faktur,
                'tanggal_jatuh_tempo' => $tanggal_jatuh_tempo,
                'metode_pembayaran' => $metode_pembayaran_hutang,
                'hutang_giro_bank' => $hutang_giro_bank,
                'hutang_giro_bank_cair' => $hutang_giro_bank_cair,
                'status_return_pembelian' => 'no'
            ];
            $pembelian = mPembelian::create($data_pembelian);
            $id_pembelian = $pembelian->id;

            $data_po = [
                'status_pembelian' => 'yes',
                'status_pembelian_date' => Main::datetime()
            ];
            mPO::where('id', $id_po)->update($data_po);

            foreach ($hpp_barang_arr as $key => $hpp_barang) {
                $hpp_barang = Main::format_number_db($hpp_barang);
                $id_po_barang = $id_po_barang_arr[$key];
                $id_barang = $id_barang_arr[$key];
                $id_satuan = $id_satuan_arr[$key];
                $qty = $qty_arr[$key];
                $satuan = $satuan_arr[$key];
                $id_stok = NULL;
                $stok_awal = NULL;
                $hpp_awal = NULL;
                $stok_masuk = NULL;
                $stok_keluar = NULL;
                $hpp_keluar = NULL;
                $stok_akhir = NULL;
                $sst_sprl = NULL;
                $hpp_stok = NULL;


                /**
                 * 1. Check barang dan lokasi sudah ada di stok atau belum
                 */
                $check = mStok
                    ::where([
                        'id_barang' => $id_barang,
                        'id_lokasi' => $po->id_lokasi
                    ])
                    ->count();

                /**
                 * 2. jika tidak ada, maka membuat stok baru
                 */
                if ($check == 0) {
                    $check_po_location_exist = FALSE;
                    $data_stok_insert = [
                        'id_barang' => $id_barang,
                        'id_lokasi' => $po->id_lokasi,
                        'id_satuan' => $id_satuan,
                        'jml_barang' => $qty,
                        'satuan' => $satuan,
                        'hpp' => $hpp_barang,
                        'id_supplier' => $po->id_supplier
                    ];

                    $stok = mStok::create($data_stok_insert);
                    $id_stok = $stok->id;
                    $stok_awal = 0;
                    $hpp_awal = $hpp_barang;
                    $stok_masuk = $qty;
                    $stok_akhir = $qty;
                } else {
                    /**
                     * 3. Jika ada, maka update hpp stok yang sudah ada
                     */
                    $where_stok_update = [
                        'id_barang' => $id_barang,
                        'id_lokasi' => $po->id_lokasi
                    ];
                    $jml_barang_before = mStok::where($where_stok_update)->value('jml_barang');
                    $jml_barang_now = $jml_barang_before + $qty;
                    $data_stok_update = [
                        'hpp' => $hpp_barang,
                        'jml_barang' => $jml_barang_now
                    ];

                    $stok_awal = mStok::where($where_stok_update)->value('jml_barang');
                    $hpp_awal = mStok::where($where_stok_update)->value('hpp');
                    mStok::where($where_stok_update)->update($data_stok_update);
                    $id_stok = mStok::where($where_stok_update)->value('id');
                    $stok_masuk = $qty;
                    $stok_akhir = $jml_barang_now;
                }

                /**
                 * 4. Insert HPP Barang ke Pembelian barang
                 */

                $data_pembelian_barang = [
                    'id_po' => $id_po,
                    'id_po_barang' => $id_po_barang,
                    'id_pembelian' => $id_pembelian,
                    'id_barang' => $id_barang,
                    'id_stok' => $id_stok,
                    'hpp_barang' => $hpp_barang,
                ];
                mPembelianBarang::create($data_pembelian_barang);

                /**
                 * 5. Update hpp barang sesuai dengan pembelian barang yang sudah terjadi
                 */

                $barang = mStok::where('id_barang', $id_barang)->get();
                $total_hpp_barang = 0;
                $total_jml_barang = 0;
                foreach ($barang as $row) {
                    $total_hpp_barang += $row->hpp * $row->jml_barang;
                    $total_jml_barang += $row->jml_barang;
                }

                $hpp_barang_new = round($total_hpp_barang / $total_jml_barang, 2);

                mBarang
                    ::where([
                        'id' => $id_barang
                    ])
                    ->update([
                        'hpp' => $hpp_barang_new
                    ]);

                mPoBarang::where('id', $id_po_barang)->update(['id_stok' => $id_stok]);

                /**
                 * 6. Masuk ke kartu stok
                 */

                $data_kartu_stok = [
                    'id_barang' => $id_barang,
                    'id_lokasi' => $po->id_lokasi,
                    'id_stok' => $id_stok,
                    'lokasi' => $po->lokasi->lokasi,
                    'tgl_transit' => Main::date(),
                    'keterangan' => 'Pembelian barang dari PO ' . $po->no_po_label,
                    'stok_awal' => $stok_awal,
                    'hpp_awal' => $hpp_awal,
                    'stok_masuk' => $stok_masuk,
                    'stok_akhir' => $stok_akhir,
                    'id_pembelian' => $id_pembelian,
                    'id_po' => $id_po
                ];

                mKartuStok::create($data_kartu_stok);

            }


            /**
             * 7. Insert data, tergantung metode pembayaran
             */

            if ($metode_pembayaran_hutang == 'hutang_supplier') {
                $hutang_supplier = new mHutangSupplier();
                $no_hutang_supplier = $hutang_supplier->countNoPo($id_user) + 1;
                $year = date('y');
                $month = date('m');
                $date = date('d');
                $no_hutang_supplier_label = 'HS.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $no_hutang_supplier;

                $data_hutang_supplier = [
                    'id_supplier' => $po->id_supplier,
                    'id_user' => $id_user,
                    'id_po' => $id_po,
                    'id_pembelian' => $id_pembelian,
                    'supplier' => $po->supplier->supplier,
                    'no_hutang_supplier' => $no_hutang_supplier,
                    'no_hutang_supplier_label' => $no_hutang_supplier_label,
                    'no_faktur_pembelian' => $no_pembelian_label,
                    'tgl_faktur_pembelian' => $tanggal_pembelian,
                    'jatuh_tempo_hutang_supplier' => $tanggal_jatuh_tempo,
                    'total_hutang_supplier' => $po->sisa_pembayaran_po,
                    'sisa_hutang_supplier' => $po->sisa_pembayaran_po,
                    'keterangan_hutang_supplier' => 'Pembelian PO - ' . $po->no_po_label,
                    'status_hutang_supplier' => 'belum_lunas',
                ];

                mHutangSupplier::create($data_hutang_supplier);
            }

            if ($metode_pembayaran_hutang == 'hutang_giro') {
                $hutang_giro = new mHutangGiro();
                $no_hutang_giro = $hutang_giro->countNoPo($id_user) + 1;
                $year = date('y');
                $month = date('m');
                $date = date('d');
                $no_hutang_giro_label = 'HG.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $no_hutang_giro;

                $data_hutang_giro = [
                    'id_po' => $id_po,
                    'id_pembelian' => $id_pembelian,
                    'id_supplier_untuk' => $po->id_supplier,
                    'no_hutang_giro' => $no_hutang_giro,
                    'no_hutang_giro_label' => $no_hutang_giro_label,
                    'tgl_pencairan_hutang_giro' => $hutang_giro_bank_cair,
                    'jumlah_hutang_giro' => $sisa_pembayaran_po,
                    'no_faktur_pembelian' => $no_pembelian_label,
                    'tgl_faktur_pembelian' => $tanggal_pembelian,
                    'nama_bank_hutang_giro' => $hutang_giro_bank,
                    'keterangan_hutang_giro' => 'Hutang dari Pembelian Purchase Order ' . $no_hutang_giro_label,
                    'status_hutang_giro' => 'belum_lunas'
                ];

                mHutangGiro::create($data_hutang_giro);

            }


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function return_pembelian_page($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => 'Penjualan',
                'route' => ''
            ],
            [
                'label' => 'Pembelian Purchase Order',
                'route' => ''
            ]
        ];
        $user = Session::get('user');
        $id_user = $user->id;
        $inisial_karyawan = $user->inisial_karyawan;
        $year = date('y');
        $month = date('m');
        $date = date('d');
        $pembelian_return = new mPembelianReturn();
        $no_pembelian_return = $pembelian_return->countNoReturn($id_user);
        $no_pembelian_return = $no_pembelian_return + 1;
        $no_pembelian_return_label = 'PBR.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $no_pembelian_return;
        $pembelian = mPembelian
            ::with([
                'pembelian_barang.po_barang.barang',
                'pembelian_barang.po_barang.satuan',
            ])
            ->where('id', $id_pembelian)
            ->first();

        $data = Main::data($this->breadcrumb, $cons['transaksi_5']);
        $pageTitle = 'Return Pembelian Purchase Order';
        $po = mPO::with(['supplier', 'po_barang', 'po_barang.satuan'])->where('id', $pembelian->id_po)->first();
        $data = array_merge($data, [
            'no_pembelian_return' => $no_pembelian_return,
            'no_pembelian_return_label' => $no_pembelian_return_label,
            'po' => $po,
            'pembelian' => $pembelian,
            'pageTitle' => $pageTitle
        ]);
        return view('transaksi/pembelianPurchaseOrder/pembelianReturn', $data);

    }

    function return_pembelian_insert(Request $request)
    {
        $qty = $request->input('qty');
        $rules = [
            'keterangan_return' => 'required',
            'qty_return' => 'required',
            'qty_return.*' => 'required',
        ];

        $attributes = [
            'keterangan_return' => 'Keterangan Return',
            'qty_return' => 'Qty Return',
        ];

        /**
         * Untuk custom between value
         */
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $between_last = isset($qty[$i]) ? $qty[$i] : 1;

            $attr['qty_return.' . $i] = 'Qty Return ke-' . $next;

            $rules['qty_return.' . $i] = 'numeric|between:0,' . $between_last;
        }
        $attributes = array_merge($attributes, $attr);

        $request_data = $request->all();
        foreach ($request_data['qty_return'] as $index => $row) {
            $request_data['qty_return'][$index] = $row ? Main::format_number_db($row) : $row;
        }
        $validator = Validator::make($request_data, $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $id_pembelian = $request->input('id_pembelian');
            $no_pembelian_return = $request->input('no_pembelian_return');
            $no_pembelian_return_label = $request->input('no_pembelian_return_label');
            $id_po = $request->input('id_po');
            $id_supplier = $request->input('id_supplier');
            $id_lokasi = $request->input('id_lokasi');
            $keterangan_return = $request->input('keterangan_return');

            $id_pembelian_barang_arr = $request->input('id_pembelian_barang');
            $id_po_barang_arr = $request->input('id_po_barang');
            $id_barang_arr = $request->input('id_barang');
            $id_stok_arr = $request->input('id_stok');
            $qty_arr = $request->input('qty');
            $hpp_barang_arr = $request->input('hpp_barang_arr');
            $qty_return_arr = $request->input('qty_return');
            $po_barang_harga_arr = $request->input('po_barang_harga');

            $pembelian = mPembelian::where('id', $id_pembelian)->first();
            $total_harga_return = 0;

            $user = Session::get('user');
            $check_return_all = TRUE;
            $id_user = $user->id;
            $inisial_karyawan = $user->inisial_karyawan;

            /**
             * Insert data Pembelian Return
             */
            $data_pembelian_return = [
                'id_user' => $id_user,
                'id_po' => $id_po,
                'id_pembelian' => $id_pembelian,
                'id_supplier' => $id_supplier,
                'id_lokasi' => $id_lokasi,
                'no_pembelian_return' => $no_pembelian_return,
                'no_pembelian_return_label' => $no_pembelian_return_label,
                'tanggal_return' => Main::datetime(),
                'keterangan_return' => $keterangan_return
            ];

            $pembelian_return = mPembelianReturn::create($data_pembelian_return);
            $id_pembelian_return = $pembelian_return->id;

            /**
             * Insert data Pembelian Return Barang
             */
            foreach ($id_po_barang_arr as $key => $id_po_barang) {
                $id_barang = $id_barang_arr[$key];
                $qty = $qty_arr[$key];
                $qty_return = $qty_return_arr[$key];
                $id_stok = $id_stok_arr[$key];
                $hpp_barang = $hpp_barang_arr[$key];
                $po_barang_harga = $po_barang_harga_arr[$key];
                $stok = mStok::where('id', $id_stok)->first();
                $po_barang = mPoBarang::where('id', $id_po_barang)->first();
                $id_pembelian_barang = $id_pembelian_barang_arr[$key];
                $stok_jml_barang_now = $stok->jml_barang - $qty_return;
                $qty_po_barang_now = $po_barang->qty - $qty_return;

                $total_harga_return += $qty_return * $po_barang_harga;

                $data_pembelian_return_barang = [
                    'id_po' => $id_po,
                    'id_pembelian' => $id_pembelian,
                    'id_pembelian_barang' => $id_pembelian_barang,
                    'id_pembelian_return' => $id_pembelian_return,
                    'id_barang' => $id_barang,
                    'qty_return' => $qty_return,
                    'qty_harga_return' => $qty_return * $po_barang_harga
                ];

                mPembelianReturnBarang::create($data_pembelian_return_barang);

                /**
                 * Proses pengurangan qty pembelian barang purchase order
                 */
                mPoBarang::where('id', $id_po_barang)->update(['qty' => $qty_po_barang_now]);


                /**
                 * Proses Pengembalian Stok Barang
                 */

                $data_stok_update = [
                    'jml_barang' => $stok_jml_barang_now
                ];
                mStok::where('id', $id_stok)->update($data_stok_update);

                /**
                 * Mencatat returnn barang yang sudah di beli di kartu stok
                 */

                $data_kartu_stok = [
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_stok' => $id_stok,
                    'tgl_transit' => Main::datetime(),
                    'keterangan' => 'Return Pembelian Purchase Order - No Return : ' . $no_pembelian_return_label,
                    'stok_awal' => $stok->jml_barang,
                    'hpp_awal' => $stok->hpp_awal,
                    'stok_keluar' => $qty,
                    'hpp_keluar' => $hpp_barang,
                    'stok_akhir' => $stok_jml_barang_now,
                    'id_pembelian' => $id_pembelian,
                    'id_po' => $id_po,
                    'id_pembelian_return' => $id_pembelian_return,
                ];

                mKartuStok::create($data_kartu_stok);

            }

            /**
             * Check Return barang semua qty dilakukan atau tidak
             */

            foreach ($qty_arr as $key => $qty) {
                $qty_return = $qty_return_arr[$key];

                if ($qty_return < $qty) {
                    $check_return_all = FALSE;
                    break;
                }
            }

            /**
             * Proses Return semua barang jika semua barang dilakukan proses Return
             */

            $data_pembelian_update = [
                'status_return_pembelian' => 'yes',
                'status_return_pembelian_date' => Main::datetime()
            ];
            mPembelian::where('id', $id_pembelian)->update($data_pembelian_update);

            if ($check_return_all) {
                $data_pembelian_update = [
                    'status_return_pembelian_semua' => 'yes',
                    'status_return_pembelian_semua_date' => Main::datetime()
                ];
                mPembelian::where('id', $id_pembelian)->update($data_pembelian_update);
            }

            if ($pembelian->metode_pembayaran == 'hutang_supplier') {
                $hutang_supplier = mHutangSupplier::where('id_pembelian', $id_pembelian)->first();
                $total_hutang_supplier_now = $hutang_supplier->total_hutang_supplier - $total_harga_return;
                $total_hutang_supplier_now = $total_hutang_supplier_now < 0 ? 0 : $total_hutang_supplier_now;
                $sisa_hutang_supplier_now = $hutang_supplier->sisa_hutang_supplier_now - $total_harga_return;
                $sisa_hutang_supplier_now = $sisa_hutang_supplier_now < 0 ? 0 : $sisa_hutang_supplier_now;

                mHutangSupplier
                    ::where('id_pembelian', $id_pembelian)
                    ->update([
                        'total_hutang_supplier' => $total_hutang_supplier_now,
                        'sisa_hutang_supplier' => $sisa_hutang_supplier_now
                    ]);
            }

            if ($pembelian->metode_pembayaran == 'hutang_giro') {
                $hutang_giro = mHutangGiro::where('id_pembelian', $id_pembelian)->first();
                $jumlah_hutang_giro_now = $hutang_giro->jumlah_hutang_giro - $total_harga_return;
                $jumlah_hutang_giro_now = $jumlah_hutang_giro_now < 0 ? 0 : $jumlah_hutang_giro_now;

                mHutangGiro
                    ::where('id_pembelian', $id_pembelian)
                    ->update([
                        'jumlah_hutang_giro' => $jumlah_hutang_giro_now
                    ]);
            }


            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }
}
