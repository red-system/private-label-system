<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mItemSO;
use app\Models\Widi\mItemSuratJalan;
use app\Models\Widi\mKartuStok;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSalesman;
use app\Models\Widi\mSO;
use app\Models\Widi\mStok;
use app\Models\Widi\mSuratJalan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SuratJalan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_1'],
                'route' => ''
            ]
        ];
    }

    function create($id)
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => 'Surat Jalan',
                'route' => ''
            ],
            [
                'label' => 'Tambah Data',
                'route' => ''
            ]
        ];
        $user = Session::get('user');
        $id_user = $user->id;
        $inisial_karyawan = $user->inisial_karyawan;
        $year = date('y');
        $month = date('m');
        $date = date('d');
        $tanggal = now();
        $dates = Main::format_date_db($tanggal);
        $data = DB::table('tb_surat_jalan')
            ->where('date', $dates)
            ->where('id_user', $id_user)
            ->count();
        $urutan = $data + 1;
        $no_sj = 'SJ.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $urutan;
        $staff = $user->username;
        $data = Main::data($this->breadcrumb);
        $pageTitle = 'Tambah Data';
        $id_so = $id;
        $so = mSO::where('id', $id_so)->first();
        $pelanggan = mLangganan::where('langganan', $so->pelanggan_so)->first();
        $salesman = mSalesman::where('id', $so->id_salesman)->value('salesman');
        $item_so = mItemSO
            ::select(
                'tb_item_so.*',
                'tb_barang.nama_barang',
                'tb_barang.harga_jual_barang',
                'tb_stok.jml_barang',
                'tb_lokasi.lokasi',
                DB::raw('(tb_item_so.harga_so * tb_item_so.jml_barang_so - tb_item_so.subtotal) as diskon')
            )
            ->leftJoin('tb_barang', 'tb_item_so.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_item_so.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->where('id_so', $id_so)
            ->get();
        $langganan = mLangganan::orderBy('langganan', ' ASC')->get();

        $barang = mBarang::withCount([
            'stok_barang AS total_stok_barang' => function ($query) {
                $query->select(DB::raw("SUM(jml_barang) AS qty_sum"));
            }
        ])
            ->orderBy('kode_barang', 'ASC')
            ->get();

        $data = Main::data($this->breadcrumb, 'sales_order');

        $data = array_merge($data, [
            'no_sj' => $no_sj,
            'so' => $so,
            'item_so' => $item_so,
            'salesman' => $salesman,
            'pelanggan' => $pelanggan,
            'langganan' => $langganan,
            'barang' => $barang,
            'staff' => $staff,
            'pageTitle' => $pageTitle
        ]);
        return view('transaksi/suratJalan/suratJalanCreate', $data);
    }

    function insert(Request $request, $id)
    {

        /**
         * data yang disimpan ke tabel sales order
         */
        $id_so = $id;
        $no_surat_jalan = $request->input('no_surat_jalan');
        $staff = $request->input('staff');
        $tanggal_order = Main::format_date_db($request->input('tanggal_order'));
        $grand_total = $request->input('grand_total');
        $user = Session::get('user');
        $id_user = $user->id;

        /**
         * data yang disimpan di tabel item surat jalan
         */
        $item_so = mItemSO::where('id_so', $id_so)->get();
        $id_barang_arr = $request->input('id_barang');
        $satuan_arr = $request->input('satuan');
        $id_stok_arr = $request->input('id_stok');
        $qty_kirim_arr = $request->input('qty_kirim');
        $qty_kali_arr = $request->input('qty_kali');
        $sub_total_arr = $request->input('sub_total');
        $diskon_arr = $request->input('diskon');

        /**
         * Check stok produk dengan yang dijual
         */
        $check_stok_produk = $this->check_stok_produk($id_stok_arr, $qty_kirim_arr);
        if (!$check_stok_produk) {
            return response([
                'message' => 'Stok Produk yang akan dijual <strong>Tidak Mencukupi</strong>'
            ], 422);
        }

        /**
         * Check Nilai Timbangan yang akan dikirim
         */
        $ke = 0;
        foreach ($qty_kirim_arr as $kirim) {
            $ke += 1;
            if ($kirim == 0) {
                return response([
                    'message' => '<strong>Qty Kirim Barang Ke-'.$ke.' Tidak Boleh 0</strong>'
                ], 422);
            }
        }


        /**
         * proses simpan surat jalan
         */
        $data_surat_jalan = [
            'no_surat_jalan' => $no_surat_jalan,
            'id_so' => $id_so,
            'staff_surat_jalan' => $staff,
            'grand_total_surat_jalan' => $grand_total,
            'date' => now(),
            'id_user' => $id_user,
            'tanggal_order' => $tanggal_order
        ];

        $data_so = [
            'surat_jalan' => 'ada',
        ];

        $sales_order = mSO::where('id', $id_so)->update($data_so);
        $surat_jalan = mSuratJalan::create($data_surat_jalan);
        $id_sj = $surat_jalan->id;

        /**
         * proses item surat jalan
         */
        foreach ($item_so as $index) {
            $id_barang = $id_barang_arr[$index->id];
            $id_stok = $id_stok_arr[$index->id];
            $satuan = $satuan_arr[$index->id];
            $qty_kirim = $qty_kirim_arr[$index->id];
            $qty_kali = $qty_kali_arr[$index->id];
            $sub_total = $sub_total_arr[$index->id];
            $diskon = $diskon_arr[$index->id];

            $stok_awal = mStok::where('id', $id_stok)->with('lokasi')->first();

            /**
             * proses simpan item surat jalan
             */

            $data_item_sj = [
                'id_surat_jalan' => $id_sj,
                'id_barang' => $id_barang,
                'id_stok' => $id_stok,
                'diskon' => $diskon,
                'qty_kirim_surat_jalan' => $qty_kirim,
                'satuan_surat_jalan' => $satuan,
                'kali_surat_jalan' => $qty_kali,
                'subtotal_surat_jalan' => $sub_total,
            ];

            $response = mItemSuratJalan::create($data_item_sj);

            $stok_baru = $stok_awal->jml_barang - $qty_kirim;

            /**
             * proses update stok
             */
            $stoks = mStok::where('id', $id_stok)->update([
                'jml_barang' => $stok_baru]);

            /**
             * proses kartu stok
             */
            $hpp = mBarang::where('id', $id_barang)->value('hpp');
            $stok = mStok::where('id_barang', $id_barang)->get();

            $stok_lama = $stok_baru + $qty_kirim;

            mKartuStok::create([
                'id_barang' => $id_barang,
                'id_stok' => $id_stok,
                'id_lokasi' => $stok_awal->id_lokasi,
                'lokasi' => $stok_awal->lokasi->lokasi,
                'tgl_transit' => now(),
                'keterangan' => 'Stok Out (' . $no_surat_jalan . ')',
                'stok_awal' => $stok_lama,
                'hpp_awal' => $hpp,
                'stok_keluar' => $qty_kirim,
                'hpp_keluar' => $hpp,
                'stok_akhir' => $stok_baru,
                'hpp_stok' => $hpp
            ]);
        }

    }

    /**
     * @param $id_stok_produk_arr
     * @param $qty_arr
     * @return bool
     *
     * proses untuk mengecek apakah produk yang akan dijual tidak melebihi jumlah stok pada lokasi yang dipesan
     */
    function check_stok_produk($id_stok_arr, $qty_kirim_arr)
    {
        $valid = TRUE;
        foreach ($id_stok_arr as $index => $id_stok_produk) {
            $stok_produk_qty = mStok::where('id', $id_stok_produk)->value('jml_barang');
            $jual_qty = $qty_kirim_arr[$index];

            if ($stok_produk_qty < $jual_qty) {
                $valid = FALSE;
                break;
            }
        }
        return $valid;
    }

    function pdf($id_so)
    {
        ini_set("memory_limit", "-1");
        $user = Session::get('user');
        $lokasi = mLokasi::find($user->id_lokasi);
        $sales_order = mSO::find($id_so);
        $surat_jalan = mSuratJalan::where('id_so', $id_so)->first();
        $item = new mItemSuratJalan();
        $item_sj = $item->pdf($surat_jalan->id);
        $langganan = mLangganan::find($sales_order->id_langganan);
        $diskon_persen = $sales_order->disc_persen_so * $sales_order->total_so / 100;
        $data = [
            'sales_order' => $sales_order,
            'surat_jalan' => $surat_jalan,
            'item_sj' => $item_sj,
            'lokasi' => $lokasi,
            'langganan' => $langganan,
            'diskon_persen' => $diskon_persen,
        ];
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('transaksi/suratJalan/suratJalanPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Surat Jalan ' . $sales_order->pelanggan_so . '-' . Main::format_date($surat_jalan->tanggal_order));
    }
}
