<?php

namespace app\Http\Controllers\Transaksi;

use app\Models\Mahendra\mPoBarang;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLokasi;
use app\Models\Mahendra\mPO;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


use app\Models\Widi\mSO;
use app\Models\Widi\mItemSO;

class PurchaseOrder extends Controller
{
    private $breadcrumb;
    private $cons;
    private $view = [
        'no_po_label' => ["search_field" => "tb_po.no_po_label"],
        'tgl_po' => ["search_field" => "tb_po.tgl_po"],
        'supplier' => ["search_field" => "tb_supplier.supplier"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'total_po' => ["search_field" => "tb_po.total_po"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_4'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
//         dd($data);
        return view('transaksi/purchaseOrder/POList', $data);
    }

    function detail($id_po)
    {
        $id_po = Main::decrypt($id_po);
        $po = mPO::with(['lokasi', 'supplier'])->where('id', $id_po)->first();
        $po_barang = mPoBarang::with(['satuan', 'barang'])->where('id_po', $id_po)->get();
        $data = [
            'po' => $po,
            'po_barang' => $po_barang,
        ];
        return view('transaksi/purchaseOrder/modalBodyDetailPo', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $po = new mPO();
        $result['iTotalRecords'] = $po->count_all();
        $result['iTotalDisplayRecords'] = $po->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $po->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
//            $date = Carbon::parse($value->tgl_po)->format('d-m-Y');
            $value->tgl_po = Main::format_date_label($value->tgl_po);
            $value->total_po = Main::format_number($value->total_po);

            $value->route['delete'] = route('purchaseOrderDelete', ['id' => Main::encrypt($value->id)]);
            $value->route['edit_direct'] = route('purchaseOrderEdit', ['id' => Main::encrypt($value->id)]);
            $value->route['detail_view'] = route('purchaseOrderDetail', ['id' => Main::encrypt($value->id)]);
            $value->route['pembelian'] = route('pembelianPurchaseOrderCreate', ['id' => Main::encrypt($value->id)]);


        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function create()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_4'],
                'route' => ''
            ],
            [
                'label' => 'Tambah Data',
                'route' => ''
            ]
        ];
        $data = Main::data($this->breadcrumb, $this->cons['transaksi_4']);
        $pageTitle = 'Tambah Data';
        $supplier = mSupplier::orderBy('supplier', ' ASC')->get();
        $lokasi = mLokasi::all();
        $satuan = mSatuan::orderBy('satuan')->get();
        $barang = mBarang::withCount([
            'stok_barang AS total_stok_barang' => function ($query) {
                $query->select(DB::raw("SUM(jml_barang) AS qty_sum"));
            }
        ])
            ->orderBy('kode_barang', 'ASC')
            ->get();
        $user = Session::get('user');
        $id_user = $user->id;
        $inisial_karyawan = $user->inisial_karyawan;
        $po = new mPO();
        $no = $po->countNoPo($id_user);
        $no_po = $no + 1;
        $year = date('y');
        $month = date('m');
        $date = date('d');
        $no_po_label = 'PO.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $no_po;

        $data = array_merge($data, [
            'supplier' => $supplier,
            'barang' => $barang,
            'pageTitle' => $pageTitle,
            'lokasi' => $lokasi,
            'no_po_label' => $no_po_label,
            'no_po' => $no_po,
            'satuan' => $satuan
        ]);
        return view('transaksi/purchaseOrder/POCreate', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'id_lokasi' => 'required',
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'id_satuan' => 'required',
            'id_satuan.*' => 'required',
            'harga' => 'required',
            'harga.*' => 'required',
            'discount' => 'required',
            'discount.*' => 'required',
            'metode_pembayaran_dp_po' => 'required',
        ];

        $attributes = [
            'id_lokasi' => 'Lokasi',
            'id_supplier' => 'Supplier',
            'id_barang' => 'Barang',
            'qty' => 'Jumlah Barang',
            'id_satuan' => 'Satuan Barang',
            'harga' => 'Harga',
            'discount' => 'Diskon',
            'metode_pembayaran_dp_po' => 'Metode Pembayaran',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['qty.' . $i] = 'Qty ke-' . $next;
            $attr['id_satuan.' . $i] = 'Satuan ke-' . $next;
            $attr['harga.' . $i] = 'Harga ke-' . $next;
            $attr['discount.' . $i] = 'Diskon ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * data yang disimpan ke tabel purchase order
         */
        $id_supplier = $request->input('id_supplier');
        $grand_total_po = $request->input('grand_total_po');
        $total_po = $request->input('total_po');
        $no_po = $request->input('no_po');
        $no_po_label = $request->input('no_po_label');
        $tgl_po = $request->input('tgl_po');
        $id_lokasi = $request->input('id_lokasi');
        $tgl_kirim_po = $request->input('tgl_kirim_po');
        $keterangan_po = $request->input('keterangan_po');
        $discount_persen_po = $request->input('discount_persen_po');
        $discount_nominal_po = $request->input('discount_nominal_po');
        $pajak_persen_po = $request->input('pajak_persen_po');
        $pajak_nominal_po = $request->input('pajak_nominal_po');
        $ongkos_kirim_po = $request->input('ongkos_kirim_po');
        $dp_po = $request->input('dp_po');
        $metode_pembayaran_dp_po = $request->input('metode_pembayaran_dp_po');
        $sisa_pembayaran_po = $request->input('sisa_pembayaran_po');
        $user = Session::get('user');
        $id_user = $user->id;
        $datetime = Main::datetime();


        /**
         * data yang disimpan di tabel item purchase order
         */
        $id_barang_arr = $request->input('id_barang');
        $discount_nominal_arr = $request->input('discount_nominal');
        $sub_total_arr = $request->input('sub_total');
        $id_satuan_arr = $request->input('id_satuan');
        $harga_arr = $request->input('harga');
        $qty_arr = $request->input('qty');
        $discount_arr = $request->input('discount');


        DB::beginTransaction();
        try {

            $data_po = [
                'id_supplier' => $id_supplier,
                'id_lokasi' => $id_lokasi,
                'id_user' => $id_user,
                'no_po' => $no_po,
                'no_po_label' => $no_po_label,
                'tgl_po' => Main::format_date_db($tgl_po),
                'tgl_kirim_po' => Main::format_date_db($tgl_kirim_po),
                'keterangan_po' => $keterangan_po,
                'total_po' => $total_po,
                'discount_persen_po' => Main::format_number_db($discount_persen_po),
                'discount_nominal_po' => Main::format_number_db($discount_nominal_po),
                'pajak_persen_po' => Main::format_number_db($pajak_persen_po),
                'pajak_nominal_po' => $pajak_nominal_po,
                'ongkos_kirim_po' => Main::format_number_db($ongkos_kirim_po),
                'grand_total_po' => $grand_total_po,
                'dp_po' => Main::format_number_db($dp_po),
                'metode_pembayaran_dp_po' => $metode_pembayaran_dp_po,
                'sisa_pembayaran_po' => $sisa_pembayaran_po
            ];

            $po = mPO::create($data_po);
            $id_po = $po->id;

            $data_po_barang = [];
            foreach ($id_barang_arr as $index => $id_barang) {

                $discount_nominal = $discount_nominal_arr[$index];
                $sub_total = $sub_total_arr[$index];
                $id_satuan = $id_satuan_arr[$index];
                $harga = $harga_arr[$index];
                $qty = $qty_arr[$index];
                $discount = $discount_arr[$index];

                $data_po_barang[] = [
                    'id_po' => $id_po,
                    'id_barang' => $id_barang,
                    'id_satuan' => $id_satuan,
                    'qty' => Main::format_number_db($qty),
                    'harga' => Main::format_number_db($harga),
                    'discount' => Main::format_number_db($discount),
                    'discount_nominal' => Main::format_number_db($discount_nominal),
                    'sub_total' => $sub_total,
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                ];
            }
            mPoBarang::insert($data_po_barang);


            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mPO::where('id', $id)->delete();
        mPoBarang::where('id_po', $id)->delete();
    }

    function edit($id_po)
    {
        $id_po = Main::decrypt($id_po);
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_4'],
                'route' => ''
            ],
            [
                'label' => 'Edit Data',
                'route' => ''
            ]
        ];
        $data = Main::data($this->breadcrumb, $this->cons['transaksi_4']);
        $pageTitle = 'Tambah Data';
        $supplier = mSupplier::orderBy('supplier', ' ASC')->get();
        $lokasi = mLokasi::all();
        $satuan = mSatuan::orderBy('satuan')->get();
        $barang = mBarang::withCount([
            'stok_barang AS total_stok_barang' => function ($query) {
                $query->select(DB::raw("SUM(jml_barang) AS qty_sum"));
            }
        ])
            ->orderBy('kode_barang', 'ASC')
            ->get();
        $po = mPO::with('supplier')->where('id', $id_po)->first();
        $po_barang = mPoBarang::with('barang')->where('id_po', $id_po)->orderBy('id', 'ASC')->get();

        $data = array_merge($data, [
            'supplier' => $supplier,
            'barang' => $barang,
            'pageTitle' => $pageTitle,
            'lokasi' => $lokasi,
            'satuan' => $satuan,
            'po' => $po,
            'po_barang' => $po_barang
        ]);
        return view('transaksi/purchaseOrder/POEdit', $data);
    }

    function update(Request $request, $id_po)
    {
        $id_po = Main::decrypt($id_po);
        $rules = [
            'id_lokasi' => 'required',
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'id_satuan' => 'required',
            'id_satuan.*' => 'required',
            'harga' => 'required',
            'harga.*' => 'required',
            'discount' => 'required',
            'discount.*' => 'required',
            'metode_pembayaran_dp_po' => 'required',
        ];

        $attributes = [
            'id_lokasi' => 'Lokasi',
            'id_supplier' => 'Supplier',
            'id_barang' => 'Barang',
            'qty' => 'Jumlah Barang',
            'id_satuan' => 'Satuan Barang',
            'harga' => 'Harga',
            'discount' => 'Diskon',
            'metode_pembayaran_dp_po' => 'Metode Pembayaran',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['qty.' . $i] = 'Qty ke-' . $next;
            $attr['id_satuan.' . $i] = 'Satuan ke-' . $next;
            $attr['harga.' . $i] = 'Harga ke-' . $next;
            $attr['discount.' . $i] = 'Diskon ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * data yang disimpan ke tabel purchase order
         */
        $id_supplier = $request->input('id_supplier');
        $grand_total_po = $request->input('grand_total_po');
        $total_po = $request->input('total_po');
        $discount_nominal_po = $request->input('discount_nominal_po');
        $no_po = $request->input('no_po');
        $no_po_label = $request->input('no_po_label');
        $tgl_po = $request->input('tgl_po');
        $id_lokasi = $request->input('id_lokasi');
        $tgl_kirim_po = $request->input('tgl_kirim_po');
        $keterangan_po = $request->input('keterangan_po');
        $discount_persen_po = $request->input('discount_persen_po');
        $pajak_persen_po = $request->input('pajak_persen_po');
        $pajak_nominal_po = $request->input('pajak_nominal_po');
        $ongkos_kirim_po = $request->input('ongkos_kirim_po');
        $dp_po = $request->input('dp_po');
        $metode_pembayaran_dp_po = $request->input('metode_pembayaran_dp_po');
        $sisa_pembayaran_po = $request->input('sisa_pembayaran_po');
        $user = Session::get('user');
        $id_user = $user->id;
        $datetime = Main::datetime();

        /**
         * data yang disimpan di tabel item purchase order
         */
        $id_barang_arr = $request->input('id_barang');
        $discount_nominal_arr = $request->input('discount_nominal');
        $sub_total_arr = $request->input('sub_total');
        $id_satuan_arr = $request->input('id_satuan');
        $harga_arr = $request->input('harga');
        $qty_arr = $request->input('qty');
        $discount_arr = $request->input('discount');


        DB::beginTransaction();
        try {

            $data_po = [
                'id_supplier' => $id_supplier,
                'id_lokasi' => $id_lokasi,
                'id_user' => $id_user,
                'no_po' => $no_po,
                'no_po_label' => $no_po_label,
                'tgl_po' => Main::format_date_db($tgl_po),
                'tgl_kirim_po' => Main::format_date_db($tgl_kirim_po),
                'keterangan_po' => $keterangan_po,
                'total_po' => $total_po,
                'discount_persen_po' => Main::format_number_db($discount_persen_po),
                'discount_nominal_po' => Main::format_number_db($discount_nominal_po),
                'pajak_persen_po' => Main::format_number_db($pajak_persen_po),
                'pajak_nominal_po' => $pajak_nominal_po,
                'ongkos_kirim_po' => Main::format_number_db($ongkos_kirim_po),
                'grand_total_po' => $grand_total_po,
                'dp_po' => Main::format_number_db($dp_po),
                'metode_pembayaran_dp_po' => $metode_pembayaran_dp_po,
                'sisa_pembayaran_po' => $sisa_pembayaran_po
            ];

            mPO::where('id', $id_po)->update($data_po);

            $data_po_barang = [];
            foreach ($id_barang_arr as $index => $id_barang) {

                $discount_nominal = $discount_nominal_arr[$index];
                $sub_total = $sub_total_arr[$index];
                $id_satuan = $id_satuan_arr[$index];
                $harga = $harga_arr[$index];
                $qty = $qty_arr[$index];
                $discount = $discount_arr[$index];

                $data_po_barang[] = [
                    'id_po' => $id_po,
                    'id_barang' => $id_barang,
                    'id_satuan' => $id_satuan,
                    'qty' => Main::format_number_db($qty),
                    'harga' => Main::format_number_db($harga),
                    'discount' => Main::format_number_db($discount),
                    'discount_nominal' => Main::format_number_db($discount_nominal),
                    'sub_total' => $sub_total,
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                ];
            }


            mPoBarang::where('id_po', $id_po)->delete();
            mPoBarang::insert($data_po_barang);


            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }
}
