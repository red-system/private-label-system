<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Models\Widi\mKomisi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class Komisi extends Controller
{
    private $breadcrumb;
    private $view = [
        'langganan' => ["search_field" => "tb_langganan.langganan"],
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'min_beli' => ["search_field" => "tb_komisi.min_beli"],
        'qty_beli' => ["search_field" => "tb_komisi.qty_beli"],
        'jenis_komisi' => ["search_field" => "tb_komisi.jenis_komisi"],
        'komisi' => ["search_field" => "tb_komisi.komisi"],
        'komisi_nominal' => ["search_field" => "tb_komisi.komisi_nominal"],
        'status' => ["search_field" => "tb_komisi.status"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_6'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('transaksi/komisi/komisiList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $komisi = new mKomisi();
        $result['iTotalRecords'] = $komisi->count_all();
        $result['iTotalDisplayRecords'] = $komisi->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $komisi->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->tgl_penjualan = Main::format_date($value->tgl_penjualan);
            if ($value->status == 'terbayar') {
                $value->status = 'Terbayar';
                $value->route['accept'] = route('komisiBayar', ['id' => $value->id]);
                $value->visibility['accept'] = 'none';
            } else {
                $value->status = 'Belum Terbayar';
                $value->label['accept'] = 'Bayar';
                $value->icon['accept'] = 'fa fa-money-bill';
                $value->route['accept'] = route('komisiBayar', ['id' => $value->id]);
                $value->direct['accept'] = route('komisiBayar', ['id' => $value->id]);
            }
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function bayar($id)
    {
        mKomisi::where('id', $id)->update(['status' => 'terbayar']);
        return redirect()->route('komisiPage');
    }
}
