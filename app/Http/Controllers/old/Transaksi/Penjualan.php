<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Models\Bayu\mKomisiLangganan;
use app\Models\Widi\mGolongan;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mItemReturPenjualan;
use app\Models\Widi\mKomisi;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mPaymentPiutangLangganan;
use app\Models\Widi\mPiutangLangganan;
use app\Models\Widi\mSalesman;
use app\Models\Widi\mBarang;
use app\Models\Widi\mItemSO;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mSO;
use app\Models\Widi\mStok;
use Carbon\Carbon;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Penjualan extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_penjualan' => ["search_field" => "tb_penjualan.no_penjualan"],
        'tgl_penjualan' => ["search_field" => "tb_penjualan.tgl_penjualan"],
        'pelanggan_penjualan' => ["search_field" => "tb_penjualan.pelanggan_penjualan"],
        'salesman' => ["search_field" => "tb_salesman.salesman"],
        'alamat_pengiriman' => ["search_field" => "tb_penjualan.alamat_pengiriman"],
        'keterangan_penjualan' => ["search_field" => "tb_penjualan.keterangan_penjualan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('transaksi/penjualan/penjualanList', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * fungsi untuk menampilkan detail dari data penjualan
     */
    function detail($id)
    {
        $penjualan = mPenjualan::leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->find($id);

        $item = mItemPenjualan::where('id_penjualan', $id)->get();
        $sisa = $penjualan->grand_total_penjualan - $penjualan->dp_penjualan;
        $history = mItemReturPenjualan::leftjoin('tb_retur_penjualan', 'tb_item_retur_penjualan.id_retur_penjualan', '=', 'tb_retur_penjualan.id')
            ->where('tb_retur_penjualan.id_penjualan', $id)->get();
        $data = [
            'penjualan' => $penjualan,
            'history' => $history,
            'item' => $item,
            'sisa' => $sisa,
        ];
        return view('transaksi/penjualan/modalBodyDetailPenjualan', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $penjualan = new mPenjualan();
        $result['iTotalRecords'] = $penjualan->count_all();
        $result['iTotalDisplayRecords'] = $penjualan->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $penjualan->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $date = Carbon::parse($value->tgl_penjualan)->format('d-m-Y');
            $value->tgl_penjualan = $date;
            $value->route['detail_view'] = route('penjualanDetail', ['id' => $value->id]);
            $value->ignore['detail_view'] = true;
            $value->route['retur_penjualan'] = route('returPenjualanCreate', ['id' => $value->id]);
            $value->direct['retur_penjualan'] = route('returPenjualanCreate', ['id' => $value->id]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * proses menampilkan form tambah data penjualan dengan data yang berasal dari sales order
     */
    function create($id)
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => 'Penjualan',
                'route' => ''
            ],
            [
                'label' => 'Tambah Data',
                'route' => ''
            ]
        ];
        $user = Session::get('user');
        $id_user = $user->id;
        $inisial_karyawan = $user->inisial_karyawan;
        $year = date('y');
        $month = date('m');
        $date = date('d');

        /**
         * proses menghitung nomor faktur penjualan melalui model mItemPenjualan
         */
        $penjualan = new mPenjualan();
        $data = $penjualan->countNoPenjualan($id_user);
        $urutan = $data + 1;
        $no_faktur = 'PL.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $urutan;
        /**
         * nomor faktur telah siap
         */

        /**
         * proses pemilahan data apa saja yang bisa diakses di form penjualan
         */
        $data = Main::data($this->breadcrumb);
        $pageTitle = 'Tambah Data';
        $id_so = $id;
        $so = mSO::where('id', $id_so)->first();
        $lokasi_penjualan = mLokasi::where('id', $so->id_lokasi)->value('lokasi');
        $pelanggan = mLangganan::where('langganan', $so->pelanggan_so)->first();
        $salesman = mSalesman::where('id', $so->id_salesman)->value('salesman');
        $item_so = mItemSO
            ::select(
                'tb_item_so.*',
                'tb_barang.kode_barang',
                'tb_barang.nama_barang',
                'tb_barang.harga_jual_barang',
                'tb_stok.jml_barang',
                'tb_lokasi.lokasi'
            )
            ->leftJoin('tb_barang', 'tb_item_so.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_item_so.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->where('id_so', $id_so)
            ->get();
        $langganan = mLangganan::orderBy('langganan', ' ASC')->get();
        $lokasi = mLokasi::all();
        $barang = mBarang::withCount([
            'stok_barang AS total_stok_barang' => function ($query) {
                $query->select(DB::raw("SUM(jml_barang) AS qty_sum"));
            }
        ])
            ->orderBy('kode_barang', 'ASC')
            ->get();

        $data = Main::data($this->breadcrumb, 'penjualan');

        /**
         * data-data dimasukan ke variabel array
         */
        $data = array_merge($data, [
            'no_faktur' => $no_faktur,
            'so' => $so,
            'item_so' => $item_so,
            'pelanggan' => $pelanggan,
            'salesman' => $salesman,
            'langganan' => $langganan,
            'barang' => $barang,
            'pageTitle' => $pageTitle,
            'lokasi' => $lokasi,
            'urutan' => $urutan,
            'lokasi_penjualan' => $lokasi_penjualan
        ]);

        /**
         * return data ke view
         */
        return view('transaksi/penjualan/penjualanCreate', $data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     *
     * proses penyimpanan data ke tabel penjualan dan item penjualan
     */
    function insert(Request $request, $id)
    {
        $id_so = $id;

        /**
         * membuat rules data apa saja yang wajib di isi pada form penjualan
         */
        $rules = [
            'id_langganan' => 'required',
            'id_salesman' => 'required',
            'id_lokasi' => 'required',
            'id_lokasi.*' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'id_stok' => 'required',
            'id_stok.*' => 'required',
            'lokasi_penjualan' => 'required',
            'metode_pembayaran' => 'required'
        ];

        /**
         * proses penamaan atribut suapa pengguna lebih mudah memahami maksud dari alert
         */
        $attributes = [
            'id_langganan' => 'Langganan',
            'id_salesman' => 'SO',
            'id_barang' => 'Produk',
            'id_lokasi' => 'Lokasi',
            'lokasi_penjualan' => 'Lokasi Penjualan',
            'metode_pembayaran' => 'Metode Pembayaran'
        ];

        /**
         * proses perulangan untuk data yang bersifat array
         */
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Produk ke-' . $next;
            $attr['id_lokasi.' . $i] = 'Lokasi ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        /**
         * jika data yang wjib di isi ternyata kosong, maka akan ada pesan error
         */
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        /**
         * data yang disimpan ke tabel sales order
         */
        $id_langganan = $request->input('id_langganan');
        $id_salesman = $request->input('id_salesman');
        $no_faktur = $request->input('no_penjualan');
        $urutan = $request->input('urutan');
        $tanggal = $request->input('tanggal');
        $tanggal = date('Y-m-d', strtotime($tanggal));
        $tanggal_tempo = $request->input('tanggal_tempo');
        $tanggal_tempo = date('Y-m-d', strtotime($tanggal_tempo));
        $langganan = $request->input('pelanggan_penjualan');
        $alamat = $request->input('alamat_pengiriman');
        $penerima = $request->input('penerima');
        $keterangan = $request->input('keterangan');
        $total = $request->input('total');
        $diskon_persen = $request->input('diskon_persen');
        $diskon_nominal = $request->input('potongan_akhir');
        $pajak_persen = $request->input('pajak');
        $pajak_nominal = $request->input('pajak_nominal');
        $ongkos_kirim = $request->input('biaya_tambahan');
        $grand_total = $request->input('grand_total');
        $dp = $request->input('bayar');
        $metode_pembayaran = $request->input('metode_pembayaran');
        $lokasi_penjualan = $request->input('lokasi_penjualan');
        $id_lokasi_penjualan = mLokasi::where('lokasi', $lokasi_penjualan)->value('id');
        $user = Session::get('user');
        $id_user = $user->id;

        /**
         * data yang disimpan di tabel item sales order
         */
        $id_barang_arr = $request->input('id_barang');
        $harga_arr = $request->input('harga');
        $diskon_persen_satuan_arr = $request->input('diskon_persen_satuan');
        $diskon_nominal_satuan_arr = $request->input('diskon_nominal_satuan');
        $sub_total_arr = $request->input('sub_total');
        $satuan_arr = $request->input('satuan');
        $id_stok_produk_arr = $request->input('id_stok');
        $qty_arr = $request->input('qty');
        $id_lokasi_arr = $request->input('id_lokasi');
        $harga_net_arr = $request->input('harga_net');

        $year = date('y');
        $month = date('m');
        $date = date('d');
        $tanggal = now();
        $dates = Main::format_date_db($tanggal);


        /**
         * proses penggabungan data yang akan disimpan ke tabel penjualan
         */
        $data_penjualan = [
            'id_so' => $id_so,
            'id_salesman' => $id_salesman,
            'id_langganan' => $id_langganan,
            'no_penjualan' => $no_faktur,
            'urutan' => $urutan,
            'tgl_penjualan' => $tanggal,
            'tgl_kirim_penjualan' => $tanggal_tempo,
            'pelanggan_penjualan' => $langganan,
            'alamat_pengiriman' => $alamat,
            'penerima' => $penerima,
            'keterangan_penjualan' => $keterangan,
            'total_penjualan' => $total,
            'disc_persen_penjualan' => $diskon_persen,
            'disc_nominal_penjualan' => $diskon_nominal,
            'pajak_persen_penjualan' => $pajak_persen,
            'pajak_nominal_penjualan' => $pajak_nominal,
            'ongkos_kirim_penjualan' => $ongkos_kirim,
            'grand_total_penjualan' => $grand_total,
            'dp_penjualan' => $dp,
            'metode_pembayaran_dp_penjualan' => $metode_pembayaran,
            'id_lokasi' => $id_lokasi_penjualan,
            'date' => $dates,
            'id_user' => $id_user,
        ];

        /**
         * proses penyimpanan data ke tabel penjualan
         */
        $response = mPenjualan::create($data_penjualan);

        /**
         * mengambil id dari data yang baru saja disimpan
         */
        $id_penjualan = $response->id;

        /**
         * proses menghitung sisa piutang
         */
        $sisa_piutang = $grand_total - $dp;

        /**
         * menghitung nomonr piutang
         */

        $piutang = new mPiutangLangganan();
        $datas = $piutang->countNoPiutangLangganan($id_user);

        /**
         * jika sisa piutang lebih dari nol maka simpan data tersebut ke piutang langganan
         */
        if ($metode_pembayaran == 'piutang') {
            if ($sisa_piutang > 0) {
                /**
                 * proses membuat nomor piutang melalui model mPiutangLangganan
                 */
                $count = $datas + 1;
                $no_piutang_langganan =
                    'PIL.' . $date . $month . $year . '.' . $count;
                /**
                 * nomor piutang siap
                 */

                /**
                 * proses menyatukan data piutang
                 */
                $data_piutang_langganan = [
                    'id_penjualan' => $id_penjualan,
                    'no_piutang_langganan' => $no_piutang_langganan,
                    'no_faktur_penjualan' => $no_faktur,
                    'tgl_faktur_penjualan' => $tanggal,
                    'jatuh_tempo' => $tanggal_tempo,
                    'id_langganan' => $id_langganan,
                    'total_piutang' => $grand_total,
                    'sisa_piutang' => $grand_total,
                    'terbayar' => 0,
                    'keterangan' => $keterangan,
                    'status' => 'belum_lunas',
                    'lokasi_penjualan' => $lokasi_penjualan,
                    'date' => Carbon::now()
                ];

                /**
                 * data piutang disimpan
                 */
                $piutang_langganan = mPiutangLangganan::create($data_piutang_langganan);
                $id_piutang = $piutang_langganan->id;
            }
        } else {
            if ($sisa_piutang > 0) {
                /**
                 * proses membuat nomor piutang melalui model mPiutangLangganan
                 */

                $count = $datas + 1;
                $no_piutang_langganan =
                    'PIL.' . $date . $month . $year . '.' . $count;
                /**
                 * nomor piutang siap
                 */

                /**
                 * proses menghitung piutang yang sudah terbayar
                 */
                $terbayar = $grand_total - $sisa_piutang;

                /**
                 * proses menyatukan data piutang
                 */
                $data_piutang_langganan = [
                    'id_penjualan' => $id_penjualan,
                    'no_piutang_langganan' => $no_piutang_langganan,
                    'no_faktur_penjualan' => $no_faktur,
                    'tgl_faktur_penjualan' => $tanggal,
                    'jatuh_tempo' => $tanggal_tempo,
                    'id_langganan' => $id_langganan,
                    'total_piutang' => $grand_total,
                    'sisa_piutang' => $sisa_piutang,
                    'terbayar' => $terbayar,
                    'keterangan' => $keterangan,
                    'status' => 'belum_lunas',
                    'lokasi_penjualan' => $lokasi_penjualan,
                    'date' => Carbon::now()
                ];

                /**
                 * data piutang disimpan
                 */
                $piutang_langganan = mPiutangLangganan::create($data_piutang_langganan);
                $id_piutang = $piutang_langganan->id;

                /**
                 * jika DP lebih besar dar nol maka data pembayaran disimpan di tabel payment
                 */
                if ($terbayar > 0) {
                    /**
                     * data pembayaran DP
                     */
                    $payment = new mPaymentPiutangLangganan();
                    $data = $payment->countNoPayment();
                    $urutan_payment = $data + 1;
                    $no_bukti = $date . $month . $year . '.' . $urutan_payment;

                    $data_payment = [
                        'id_piutang' => $piutang_langganan->id,
                        'id_langganan' => $id_langganan,
                        'urutan' => $urutan_payment,
                        'no_bukti_pembayaran' => $no_bukti,
                        'tgl_pembayaran' => now(),
                        'metode_pembayaran' => $metode_pembayaran,
                        'jumlah' => $terbayar,
                        'keterangan' => 'DP',
                        'date' => now(),
                        'jenis_pembayaran' => 'bayar'
                    ];

                    mPaymentPiutangLangganan::create($data_payment);
                }
            }
        }

        foreach ($id_barang_arr as $index => $id_produk) {
            $barang = mBarang::where('id', $id_produk)->first();
            $golongan = mGolongan::where('id', $barang->id_golongan)->first();
            $id_lokasi = $id_lokasi_arr[$index];
            $id_stok_produk = $id_stok_produk_arr[$index];
            $qty = $qty_arr[$index];
            $harga = $harga_arr[$index];
            $diskon_nominal_satuan = $diskon_nominal_satuan_arr[$index];
            $diskon_persen_satuan = $diskon_persen_satuan_arr[$index];
            $satuan = $satuan_arr[$index];
            $sub_total = $sub_total_arr[$index];
            $harga_net = $harga_net_arr[$index];

            $data_penjualan_produk = [
                'id_penjualan' => $id_penjualan,
                'id_barang' => $id_produk,
                'id_stok' => $id_stok_produk,
                'nama_barang_penjualan' => $barang->nama_barang,
                'jml_barang_penjualan' => $qty,
                'golongan_penjualan' => $golongan->golongan,
                'satuan_penjualan' => $satuan,
                'harga_net' => $harga_net,
                'harga_penjualan' => $harga,
                'disc_persen_penjualan' => $diskon_persen_satuan,
                'disc_nominal' => $diskon_nominal_satuan,
                'subtotal' => $sub_total
            ];

            $item_penjualan = mItemPenjualan::create($data_penjualan_produk);
            $komisi = mKomisiLangganan::where('id_langganan', $id_langganan)->where('id_barang', $id_produk)->first();
//            $test = 'gagal';
            if ($komisi != null) {
                if ($qty >= $komisi->minimal_beli_komisi_langganan) {
//                $test = 'berhasil';
                    if ($komisi->tipe_komisi_langganan == 'persentase') {
                        $komisi_nominal = $komisi->komisi_langganan * $sub_total / 100;
                    } else {
                        $komisi_nominal = $komisi->komisi_langganan;
                    }
                    $komisi = [
                        'id_langganan' => $id_langganan,
                        'id_piutang_langganan' => $id_piutang,
                        'id_item_penjualan' => $item_penjualan->id,
                        'id_komisi_langganan' => $komisi->id,
                        'min_beli' => $komisi->minimal_beli_komisi_langganan,
                        'qty_beli' => $qty,
                        'jenis_komisi' => $komisi->tipe_komisi_langganan,
                        'komisi' => $komisi->komisi_langganan,
                        'komisi_nominal' => $komisi_nominal,
                        'status' => 'belum_terbayar',
                    ];
                    mKomisi::create($komisi);
                }
            }
        }

        mSO::where('id', $id_so)->update(['penjualan' => 'ada']);
    }

    function penjualan_stok(Request $request)
    {
        $id_barang = $request->input('id_barang');
        $produkStok = mStok
            ::with('lokasi')
            ->where([
                'id_barang' => $id_barang,
            ])
            ->where('jml_barang', '>', 0)
            ->get();

        $data = [
            'produkStok' => $produkStok,
            'no' => 1
        ];

        return view('transaksi/penjualan/tbodyProdukStok', $data);
    }


    function produk_harga(Request $request)
    {
        $id_produk = $request->input('id_barang');
        $qty = $request->input('jml_barang');


        $barang = mBarang
            ::where('id', $id_produk)->first();

        $harga = $barang->harga_jual_barang;


        $data = [
            'number_raw' => $harga,
            'number_format' => Main::format_number($harga)
        ];

        return $data;
    }

    function produk_lokasi(Request $request)
    {
        $id_lokasi = $request->input('id_lokasi');
        $id_barang = $request->input('id_barang');

        $lokasi = mStok
            ::with('lokasi')
            ->distinct()
            ->where([
                'id_barang' => $id_barang,
            ])
            ->get(['id_lokasi', 'jml_barang']);
        return $lokasi;

    }
}
