<?php

namespace app\Http\Controllers\Transaksi;

use app\Helpers\Main;
use app\Mail\sendMail;
use app\Models\Widi\mBarang;
use app\Models\Widi\mItemPenjualan;
use app\Models\Widi\mItemReturPenjualan;
use app\Models\Widi\mKartuStok;
use app\Models\Widi\mLangganan;
use app\Models\Widi\mPaymentHutang;
use app\Models\Widi\mPaymentPiutangLangganan;
use app\Models\Widi\mPenjualan;
use app\Models\Widi\mPiutangLangganan;
use app\Models\Widi\mReturPenjualan;
use app\Models\Widi\mStok;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

class ReturPenjualan extends Controller
{

    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => $cons['transaksi_2'],
                'route' => ''
            ]
        ];
    }

    function create($id)
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transaksi'],
                'route' => ''
            ],
            [
                'label' => 'Retur Penjualan',
                'route' => ''
            ],
            [
                'label' => 'Tambah Data',
                'route' => ''
            ]
        ];
        $user = Session::get('user');
        $id_user = $user->id;
        $inisial_karyawan = $user->inisial_karyawan;
        $year = date('y');
        $month = date('m');
        $date = date('d');
        $penjualan = new mReturPenjualan();
        $data = $penjualan->countNoReturPenjualan($id_user);
        $urutan = $data + 1;
        $no_retur = 'RP.' . $date . $month . $year . '.' . $inisial_karyawan . '.' . $urutan;
        $data = Main::data($this->breadcrumb);
        $pageTitle = 'Tambah Data';
        $id_penjualan = $id;
        $penjualan = mPenjualan::where('id', $id_penjualan)->first();
        $item_penjualan = mItemPenjualan
            ::select([
                'tb_item_penjualan.*',
                'tb_barang.kode_barang',
                'tb_barang.nama_barang',
                'tb_stok.jml_barang',
                'tb_lokasi.lokasi'
            ])
            ->leftJoin('tb_barang', 'tb_item_penjualan.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_item_penjualan.id_stok', '=', 'tb_stok.id')
            ->leftjoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->where('id_penjualan', $id_penjualan)
            ->get();

        /**
         * mencari apakah data penjualan yang dipilih sudah pernah melakukan retur
         */
        $retur = DB::table('tb_item_retur_penjualan')->select('tb_item_retur_penjualan.id_barang',
            DB::raw('sum(tb_item_retur_penjualan.jml_retur_penjualan) as jumlah_barang_retur,
            sum(tb_item_retur_penjualan.subtotal_retur) as total_nilai_retur'))
            ->leftjoin('tb_retur_penjualan', 'tb_retur_penjualan.id', '=', 'tb_item_retur_penjualan.id_retur_penjualan')
            ->where('tb_retur_penjualan.id_penjualan', $id_penjualan)->groupBy('id_barang')->get();

        $total_sebelumnya = 0;
        foreach ($retur as $value) {
            $total_sebelumnya += $value->total_nilai_retur;
        }
        $data = Main::data($this->breadcrumb, 'penjualan');
        $data = array_merge($data, [
            'no_retur' => $no_retur,
            'total_sebelumnya' => $total_sebelumnya,
            'penjualan' => $penjualan,
            'item_penjualan' => $item_penjualan,
            'pageTitle' => $pageTitle,
            'retur' => $retur
        ]);
        return view('transaksi/returPenjualan/returPenjualanCreate', $data);
    }


    function insert(request $request, $id)
    {
        $user = Session::get('user');
        $id_penjualan = $id;
        $tgl_retur = Main::format_date_db($request->input('tgl_retur'));
        $no_retur = $request->input('no_retur');
        $keterangan_retur = $request->input('keterangan_retur');

        if($keterangan_retur == ""){
            $keterangan_retur = "Retur Penjualan No : ".$no_retur;
        }
        $total_retur = $request->input('total_retur');
        $id_user = $user->id;
        $date = now();

        $id_item_penjualan_arr = $request->input('id_item_penjualan');
        $id_barang_arr = $request->input('id_barang');
        $id_stok_arr = $request->input('id_stok');
        $jml_barang_penjualan_arr = $request->input('jml_barang_penjualan');
        $satuan_penjualan_arr = $request->input('satuan_penjualan');
        $harga_penjualan_arr = $request->input('harga_penjualan');
        $disc_persen_penjualan_arr = $request->input('diskon_persen');
        $disc_nominal_arr = $request->input('diskon_nominal');
        $jml_retur_penjualan_arr = $request->input('qty_retur');
        $subtotal_retur_arr = $request->input('subtotal_retur');

        $data_retur = [
            'tgl_retur' => $tgl_retur,
            'no_retur' => $no_retur,
            'id_penjualan' => $id_penjualan,
            'keterangan_retur' => $keterangan_retur,
            'total_retur' => $total_retur,
            'id_user' => $id_user,
            'date' => $date

        ];
        $retur = mReturPenjualan::create($data_retur);
        $id_retur = $retur->id;
        $item_retur = [];
        $count = 0;
        foreach ($id_item_penjualan_arr as $index) {
            $count++;
            $id_barang = $id_barang_arr[$index];
            $id_stok = $id_stok_arr[$index];
            $jml_barang_penjualan = $jml_barang_penjualan_arr[$index];
            $satuan_penjualan = $satuan_penjualan_arr[$index];
            $harga_penjualan = $harga_penjualan_arr[$index];
            $disc_persen_penjualan = $disc_persen_penjualan_arr[$index];
            $disc_nominal = $disc_nominal_arr[$index];
            $jml_retur_penjualan = $jml_retur_penjualan_arr[$index];
            $subtotal_retur = $subtotal_retur_arr[$index];

            if($jml_retur_penjualan > 0){
            $item_retur[$index] = [
                'nama_barang' => mBarang::where('id', $id_barang)->value('nama_barang'),
                'jml_retur' => $jml_retur_penjualan,
                'stok' => DB::table('tb_stok')->select('tb_satuan.satuan', 'tb_lokasi.lokasi')
                    ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
                    ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
                    ->where('tb_stok.id', $id_stok)->first(),
                'kode_barang' => mBarang::where('id', $id_barang)->value('kode_barang'),
                'subtotal' => $subtotal_retur
            ];
            $data_item_retur = [
                'id_barang' => $id_barang,
                'id_stok' => $id_stok,
                'id_retur_penjualan' => $id_retur,
                'jml_barang_penjualan' => $jml_barang_penjualan,
                'satuan_penjualan' => $satuan_penjualan,
                'harga_penjualan' => $harga_penjualan,
                'disc_persen_penjualan' => $disc_persen_penjualan,
                'disc_nominal' => $disc_nominal,
                'jml_retur_penjualan' => $jml_retur_penjualan,
                'subtotal_retur' => $subtotal_retur,
                'id_user' => $id_user
            ];

            $response = mItemReturPenjualan::create($data_item_retur);

            $barang = mBarang::where('id', $id_barang)->first();

            $stoks = mStok::where('id', $id_stok)->with('lokasi')->first();

            $stok_baru = $stoks->jml_barang + $jml_retur_penjualan;

            $stok = mStok::where('id_barang', $id_barang)->get();


            /**
             * proses mencari hpp rata rata baru
             * mencari total keseluruhan stok awal
             */
            $totalstok = 0;
            foreach ($stok as $item) {
                $totalstok += $item->jml_barang;
            }
            /**
             * mencari hpp lama
             */
            $hpp_lama = $totalstok * $barang->hpp;
            /**
             * mencari hpp dari barang yang retur
             */
            $hpp_baru = $jml_retur_penjualan * $stoks->hpp;
            /**
             * mencari  total keseluruhan stok termasuk yang di retur
             */
            $total_keseluruhan_stok = $totalstok + $jml_retur_penjualan;
            /**
             * menggabungkan nilai hpp lama dan retur
             */
            $hpp_gabung = $hpp_lama + $hpp_baru;
            /**
             * mencari hpp rata-rata
             */
            $hpp_rata_rata = $hpp_gabung / $total_keseluruhan_stok;

            $stok_awal = $stok_baru - $jml_retur_penjualan;

                mStok::where('id', $id_stok)->update([
                    'jml_barang' => $stok_baru]);

                mKartuStok::create([
                    'id_barang' => $id_barang,
                    'id_lokasi' => $stoks->id_lokasi,
                    'lokasi' => $stoks->lokasi->lokasi,
                    'tgl_transit' => now(),
                    'keterangan' => 'Retur Stok',
                    'stok_awal' => $stok_awal,
                    'hpp_awal' => $barang->hpp,
                    'stok_masuk' => $jml_retur_penjualan,
                    'hpp_masuk' => $stoks->hpp,
                    'stok_akhir' => $stok_baru,
                    'hpp_stok' => $hpp_rata_rata
                ]);
            }
        }


        $penjualan = mPenjualan::where('id', $id_penjualan)->first();
        $piutang = mPiutangLangganan::where('id_penjualan', $id_penjualan)->first();

        if ($piutang != null) {
            $payment = new mPaymentHutang();
            $data = $payment->countNoPayment();
            $urutan = $data + 1;
            $tanggal = date('d');
            $month = date('m');
            $year = date('y');
            $no_bukti = $tanggal . $month . $year . '.' . $urutan;

            $sisa_piutang = $piutang->sisa_piutang - $total_retur;
            $terbayar = $piutang->total_piutang - $sisa_piutang;

            if ($sisa_piutang > 0) {
                $status = 'belum_lunas';
            } else {
                $status = 'lunas';
            }

            $data_piutang = [
                'sisa_piutang' => $sisa_piutang,
                'terbayar' => $terbayar,
                'status' => $status
            ];

            mPiutangLangganan::where('id', $piutang->id)->update($data_piutang);

            $data_payment = [
                'id_piutang' => $piutang->id,
                'id_langganan' => $penjualan->id_langganan,
                'no_bukti_pembayaran' => $no_bukti,
                'urutan' => $urutan,
                'tgl_pembayaran' => now(),
                'metode_pembayaran' => 'Retur Penjualan',
                'jumlah' => $total_retur,
                'keterangan' => 'Retur Penjualan no : ' . $no_retur,
                'date' => now(),
                'jenis_pembayaran' => 'retur'
            ];

            mPaymentPiutangLangganan::create($data_payment);
        }
        $langganan = mLangganan::where('id', $penjualan->id_langganan)->value('langganan');
        $company = Main::companyInfo();
        $data = [
            'data_retur' => $data_retur,
            'item_retur' => $item_retur,
            'langganan' => $langganan,
            'count' => $count
        ];
        Mail::to('deva.yogi25@gmail.com')->send(new sendMail($data));
//        return view('transaksi/returPenjualan/returEmail', compact('data'));
    }
}
