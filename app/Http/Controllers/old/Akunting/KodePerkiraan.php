<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use Barryvdh\DomPDF\Facade as PDF;
use DB;

class KodePerkiraan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_1'],
                'route' => ''
            ]
        ];
    }

    /**
     * @return array|mixed
     * fungsi untuk mendapat data list untuk ditampilkan
     */
    function list(){
        /**
         * varibel untuk mengatur jarak teks dari hAkunting
         */
        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);

        $data = Main::data($this->breadcrumb);

        /**
         * url API tujuan
         */
        $endpoint = $data["urlCoa"];
        $client = new \GuzzleHttp\Client();

        /**
         * request data ke API
         */
        $response = $client->request('GET', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);
        /**
         * respon data
         */
        $result = (json_decode($response->getBody(),true));
        $data = array_merge($data, [
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'perkiraan' => $result['data'],
            'company' => Main::companyInfo()
        ]);

        return $data;
    }

    function index()
    {
        $data = $this->list();
        return view('akunting/kodePerkiraan/kodePerkiraanList', $data);
    }

    function rules($request)
    {
        $rules = [
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ];
        $customeMessage = [
            'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request)
    {
        $request->validate([
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ]);

        $data = Main::data($this->breadcrumb);
        $endpoint = $data["urlCoaInsert"];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $endpoint, [
            'form_params' => [
                'mst_master_id' => $request->input('mst_master_id'),
                'mst_kode_rekening' => $request->input('mst_kode_rekening'),
                'mst_nama_rekening' => $request->input('mst_nama_rekening'),
                'mst_posisi' => $request->input('mst_posisi'),
                'mst_normal' => $request->input('mst_normal'),
                'mst_tipe_laporan' => $request->input('mst_tipe_laporan'),
                'mst_tipe_nominal' => $request->input('mst_tipe_nominal'),
                'mst_neraca_tipe' => $request->input('mst_neraca_tipe'),
                'mst_kas_status' => $request->input('mst_kas_status'),
                'mst_pembayaran' => $request->input('mst_pembayaran'),
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);
        $result = (json_decode($response->getBody(),true));
        return $result;
    }

    /**
     * @param $master_id
     * @return mixed
     * fungsi untuk mendapat data yang akan di edit
     */
    function list_edit($master_id){
        $data = Main::data($this->breadcrumb);
        $endpoint = $data["urlCoa"];
        $client = new \GuzzleHttp\Client();
        $list = $client->request('POST', $endpoint.'/detail', [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ],
            'form_params' => [
                'master_id' => $master_id
            ],
        ]);
        $list = (json_decode($list->getBody(),true));
        return $list['data'];
    }

    /**
     * @param $master_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * fungsi edit
     */
    function edit($master_id)
    {
        $list = $this->list_edit($master_id);
        $data = $this->list();
        $data['list'] = $list;
        $data['master_id'] = $master_id;
        return view('akunting/kodePerkiraan/kodePerkiraanDetail', $data);
    }

    /**
     * fungsi update
     */
    function update(Request $request, $master_id)
    {
        $request->validate([
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ]);

        $data = Main::data($this->breadcrumb);
        $endpoint = $data["urlCoa"];
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', $endpoint.'/update', [
            'form_params' => [
                'master_id' => $master_id,
                'mst_master_id' => $request->input('mst_master_id'),
                'mst_kode_rekening' => $request->input('mst_kode_rekening'),
                'mst_nama_rekening' => $request->input('mst_nama_rekening'),
                'mst_posisi' => $request->input('mst_posisi'),
                'mst_normal' => $request->input('mst_normal'),
                'mst_tipe_laporan' => $request->input('mst_tipe_laporan'),
                'mst_tipe_nominal' => $request->input('mst_tipe_nominal'),
                'mst_neraca_tipe' => $request->input('mst_neraca_tipe'),
                'mst_kas_status' => $request->input('mst_kas_status'),
                'mst_pembayaran' => $request->input('mst_pembayaran'),
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);

        return redirect(route('perkiraanPage'));
    }

    /**
     * @param $master_id
     * fungsi delete
     */
    function delete($master_id)
    {
        $data = Main::data($this->breadcrumb);
        $endpoint = $data["urlCoa"];
        $client = new \GuzzleHttp\Client();

        $response = $client->post($endpoint.'/delete?master_id='.$master_id, [
            'form_params' => [
                'master_id' => $master_id,
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);
    }

    function pdf()
    {
        $data = $this->list();
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                    ->loadView('akunting/kodePerkiraan/kodePerkiraanPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Kode Perkiraan ' . date('d-m-Y'));
    }

    function excel()
    {

        $data = $this->list();
        return view('akunting/kodePerkiraan/kodePerkiraanExcel', $data);
    }


}