<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use app\Models\mPerkiraan;
use app\Models\mDetailPerkiraan;

use DB,
    PDF;

class ArusKas extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_3'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);


//        $transaksi = mAcTransaksi::whereBetween('tgl_transaksi', [$date_start, $date_end]);
//
//        $debet = mAcTransaksi
//            ::where('tgl_transaksi', '<', $date_start)
//            ->where('trs_kode_rekening', '1101')
//            ->sum('trs_debet');
//        $kredit = mAcTransaksi
//            ::where('tgl_transaksi', '<', $date_start)
//            ->where('trs_kode_rekening', '1101')
//            ->sum('trs_kredit');
//        $kas_kecil = $debet - $kredit;
//
//        //saldoawalkasbesar
//        $debet_kas_besar = mAcTransaksi
//            ::where('tgl_transaksi', '<', $date_start)
//            ->where('trs_kode_rekening', '1102')
//            ->sum('trs_debet');
//        $kredit_kas_besar = mAcTransaksi
//            ::where('tgl_transaksi', '<', $date_start)
//            ->where('trs_kode_rekening', '1102')
//            ->sum('trs_kredit');
//        $kas_besar = $debet_kas_besar - $kredit_kas_besar;
//        $kas_awal = $kas_kecil + $kas_besar;
//
//        $transaksi = $transaksi->where('trs_kode_rekening', '=', '1101')->get();
//        $transaksi_kas_besar = mAcTransaksi
//            ::whereBetween('tgl_transaksi', [$date_start, $date_end])
//            ->where('trs_kode_rekening', '=', '1102')
//            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
//            'kas_awal' => $kas_awal,
//            'transaksi' => $transaksi,
//            'transaksi_kas_besar' => $transaksi_kas_besar
        ]);

        return view('akunting/arusKas/arusKasList', $data);
    }

    function pdf(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);


        $transaksi = mAcTransaksi::whereBetween('tgl_transaksi', [$date_start, $date_end]);

        $debet = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1101')
            ->sum('trs_debet');
        $kredit = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1101')
            ->sum('trs_kredit');
        $kas_kecil = $debet - $kredit;

        //saldoawalkasbesar
        $debet_kas_besar = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1102')
            ->sum('trs_debet');
        $kredit_kas_besar = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1102')
            ->sum('trs_kredit');
        $kas_besar = $debet_kas_besar - $kredit_kas_besar;
        $kas_awal = $kas_kecil + $kas_besar;

        $transaksi = $transaksi->where('trs_kode_rekening', '=', '1101')->get();
        $transaksi_kas_besar = mAcTransaksi
            ::whereBetween('tgl_transaksi', [$date_start, $date_end])
            ->where('trs_kode_rekening', '=', '1102')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'kas_awal' => $kas_awal,
            'transaksi' => $transaksi,
            'transaksi_kas_besar' => $transaksi_kas_besar,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('akunting/arusKas/arusKasPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->download('Arus Kas ' . date('d-m-Y') . '.pdf');
    }

    function excel(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);


        $transaksi = mAcTransaksi::whereBetween('tgl_transaksi', [$date_start, $date_end]);

        $debet = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1101')
            ->sum('trs_debet');
        $kredit = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1101')
            ->sum('trs_kredit');
        $kas_kecil = $debet - $kredit;

        //saldoawalkasbesar
        $debet_kas_besar = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1102')
            ->sum('trs_debet');
        $kredit_kas_besar = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1102')
            ->sum('trs_kredit');
        $kas_besar = $debet_kas_besar - $kredit_kas_besar;
        $kas_awal = $kas_kecil + $kas_besar;

        $transaksi = $transaksi->where('trs_kode_rekening', '=', '1101')->get();
        $transaksi_kas_besar = mAcTransaksi
            ::whereBetween('tgl_transaksi', [$date_start, $date_end])
            ->where('trs_kode_rekening', '=', '1102')
            ->get();

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'kas_awal' => $kas_awal,
            'transaksi' => $transaksi,
            'transaksi_kas_besar' => $transaksi_kas_besar,
            'company' => Main::companyInfo()
        ];

        return view('akunting/arusKas/arusKasExcel', $data);
    }


}