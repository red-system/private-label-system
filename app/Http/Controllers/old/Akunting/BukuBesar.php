<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

use DB,
    PDF;

class BukuBesar extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_8'],
                'route' => ''
            ]
        ];
    }

    /**
     * proses request data Buku Besar ke API
     */
    function list($date_start, $date_end, $master_id){
        $data = Main::data($this->breadcrumb);

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);

        /**
         * membuat kondisi array yang akan dikirim ke API
         *
         * jika variabel master_id benilai null, atau all, atau di dalam arraynya terdapat nilai all, atau variabel array master id kososng
         * maka variabel master_id_send berisi all
         */
        if($master_id == null || $master_id == 'all' || in_array('all', $master_id) || empty($master_id)){
            $master_id_send = 'all';
        }
        /**
         * jika tidak variabel master_id_send sama dengan nilai variabel master_id yang di json encode
         */
        else{
            $master_id_send = (json_encode($master_id));
        }

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlBukuBesar"];
        $client = new \GuzzleHttp\Client();

        /**
         * proses pengiriman request dengan parameter tanggal
         */
        $response = $client->request('POST', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ],
            'form_params'  => [
                'date_start' => $date_start,
                'date_end' => $date_end,
                'master_id' => $master_id_send
            ]
        ]);
        /**
         * respon berupa data
         */
        $result = (json_decode($response->getBody(),true));

        $endpointCoa = $data["urlCoa"];
        $list = $client->request('GET', $endpointCoa, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);
        $list = (json_decode($list->getBody(),true));

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'master_id' => $master_id
        ];
        $data = array_merge($data, $result['data']);
        $data = array_merge($data, [
            'perkiraan' => $list['data'],
            'master_id' => $master_id,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'company' => Main::companyInfo()
        ]);

        return $data;
    }

    function index(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $master_id = $request->input('master_id');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();
        $master_id = $master_id ? $master_id : ['all'];

        $data = $this->list($date_start,$date_end, $master_id);

        return view('akunting/bukuBesar/bukuBesarList', $data);
    }

    function pdf(Request $request)
    {

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 30000000);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $master_id = $request->input('master_id');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');
        $master_id = $master_id ? $master_id : ['all'];

        $data = $this->list($date_start,$date_end, $master_id);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                    ->loadView('akunting/bukuBesar/bukuBesarPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Buku Besar ' . date('d-m-Y') );
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 30000000);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $master_id = $request->input('master_id');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');
        $master_id = $master_id ? $master_id : ['all'];

        $data = $this->list($date_start,$date_end, $master_id);


        return view('akunting/bukuBesar/bukuBesarExcel', $data);
    }
}
