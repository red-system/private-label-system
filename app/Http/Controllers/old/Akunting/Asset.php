<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Models\mKategoriAsset;
use app\Models\mAsset;
use app\Models\mPenyusutanAsset;

use DB;

class Asset extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_6'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_6'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

//        $list = mAsset::with('kategori_asset:id,nama')->orderBy('id', 'ASC')->get();

        $data = array_merge($data, [
//            'list' => $list,
        ]);

        return view('akunting/asset/assetList', $data);
    }

    function create()
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $kategori_asset = mKategoriAsset::orderBy('nama')->get();
        $kode_asset = hAkunting::kode_asset();
        $kode_perkiraan_option_list = hAkunting::kode_perkiraan_select_list();

        $data = array_merge($data, [
            'pageTitle' => 'Asset Baru',
            'kode_asset' => $kode_asset,
            'kategori_asset' => $kategori_asset,
            'kode_perliraan_option_list' => $kode_perkiraan_option_list
        ]);

        return view('akunting/asset/assetCreate', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_kategori_asset' => 'required',
        ]);

        $id_kategori_asset = $request->input('id_kategori_asset');
        $penyusutan_status = mKategoriAsset::where('id', $id_kategori_asset)->value('penyusutan_status');
        if ($penyusutan_status == 'yes') {
            $request->validate([
                'nama' => 'required',
                'tanggal_beli' => 'required',
                'id_kategori_asset' => 'required',
                'qty' => 'required',
                'agencies' => 'required',
                'metode' => 'required',
                'tanggal_pensiun' => 'required',
                'akumulasi_beban' => 'required',
                'terhitung_tanggal' => 'required',
                'nilai_buku' => 'required',
                'beban_perbulan' => 'required',
                'master_id_asset' => 'required',
                'master_id_akumulasi' => 'required',
                //'master_id_depresiasi' => 'required',
                'master_id_jenis_pembayaran' => 'required',
            ]);
        } else {
            $request->validate([
                'nama' => 'required',
                'tanggal_beli' => 'required',
                'id_kategori_asset' => 'required',
                'qty' => 'required',
                'agencies' => 'required',
                'metode' => 'required',
                'tanggal_pensiun' => 'required',
                //'akumulasi_beban' => 'required',
                'terhitung_tanggal' => 'required',
                'nilai_buku' => 'required',
                //'beban_perbulan' => 'required',
                'master_id_asset' => 'required',
                'master_id_akumulasi' => 'required',
                'master_id_depresiasi' => 'required',
                'master_id_jenis_pembayaran' => 'required',
            ]);
        }

        DB::beginTransaction();
        try {


            $kode_asset = $request->input('kode_asset');
            $nama = strtoupper($request->input('nama'));
            $id_kategori_asset = $request->input('id_kategori_asset');
            $tanggal_beli = Main::format_date_db($request->input('tanggal_beli_2'));
            $tanggal_beli_2 = Main::format_date_db($request->input('tanggal_beli'));
            $qty = $request->input('qty');
            $harga_beli = $request->input('harga_beli');
            $total_beli = $request->input('total_beli');
            $nilai_residu = $request->input('nilai_residu');
            $umur_ekonomis = $request->input('umur_ekonomis');
            $lokasi = strtoupper($request->input('agencies'));
            $akumulasi_beban = $request->input('akumulasi_beban');
            $terhitung_tanggal = Main::format_date_db($request->input('terhitung_tanggal'));
            $nilai_buku = $request->input('nilai_buku');
            $provisi = $request->input('provisi');
            $beban_perbulan = $request->input('beban_perbulan');
            $metode = $request->input('metode');
            $tanggal_pensiun = Main::format_date_db($request->input('tanggal_pensiun'));
            $master_id_asset = $request->input('master_id_asset');
            $master_id_akumulasi = $request->input('master_id_akumulasi');
            $master_id_depresiasi = $request->input('master_id_depresiasi');
            $master_id_jenis_pembayaran = $request->input('master_id_jenis_pembayaran');
            $asset_profisi = $request->input('asset_profisi');
            $harga_beli_2 = $request->input('harga_beli_2');

            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);


            $data_asset = [
                'kode_asset' => $kode_asset,
                'nama' => $nama,
                'id_kategori_asset' => $id_kategori_asset,
                'tanggal_beli' => $tanggal_beli,
                'tanggal_beli_2' => $tanggal_beli_2,
                'qty' => $qty,
                'harga_beli' => $harga_beli,
                'total_beli' => $total_beli,
                'nilai_residu' => $nilai_residu,
                'umur_ekonomis' => $umur_ekonomis,
                'agencies' => $lokasi,
                'akumulasi_beban' => $akumulasi_beban,
                'terhitung_tanggal' => $terhitung_tanggal,
                'nilai_buku' => $nilai_buku,
                'provisi' => $provisi,
                'beban_perbulan' => $beban_perbulan,
                'metode' => $metode,
                'tanggal_pensiun' => $tanggal_pensiun,
                'master_id_asset' => $master_id_asset,
                'master_id_akumulasi' => $master_id_akumulasi,
                'master_id_depresiasi' => $master_id_depresiasi,
                'master_id_jenis_pembayaran' => $master_id_jenis_pembayaran
            ];

            //return response($data_asset, 422);

            $response = mAsset::create($data_asset);
            $id_asset = $response->id;

            $data_jurnal_umum = [
                'id_asset' => $id_asset,
                'no_invoice' => $kode_asset,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => 'Pengadaan Asset No : ' . $kode_asset,
            ];

            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $coa = [$master_id_asset];
            foreach ($coa as $master_id) {
                $master = mAcMaster::where('master_id', $master_id)->first();
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;

                $debet = $harga_beli_2;

                $data_transaksi_piutang = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'id_asset' => $id_asset,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet' => $debet,
                    'trs_kredit' => 0,
                    'user_id' => 0,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => 'Investasi',
                    'trs_catatan' => 'Pengadaan Asset',
                    'trs_charge' => 0,
                    'trs_no_check_bg' => 0,
                    'trs_tgl_pencairan' => $this->datetime,
                    'trs_setor' => 0,
                    'tgl_transaksi' => $this->datetime
                ];
                mAcTransaksi::create($data_transaksi_piutang);
            }

            $master_jenis_pembayaran = mAcMaster::where('master_id', $master_id_jenis_pembayaran)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_nama_rekening = $master_jenis_pembayaran->mst_kode_rekening;
            $trs_kode_rekening = $master_jenis_pembayaran->mst_nama_rekening;
            $jns_pembayaran = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'id_asset' => $id_asset,
                'master_id' => $master_id_jenis_pembayaran,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet' => 0,
                'trs_kredit' => $harga_beli,
                'user_id' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Investasi',
                'trs_catatan' => 'Pengadaan Asset',
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => 0,
                'tgl_transaksi' => $this->datetime
            ];
            mAcTransaksi::create($jns_pembayaran);

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }


    }

    function penyusutan_form($id_asset)
    {
        $asset = mAsset::find($id_asset);

        $data = [
            'asset' => $asset
        ];

        return view('akunting.asset.modalPenyusutanForm', $data);
    }

    function penyusutan_insert(Request $request)
    {
        $request->validate([
            'tanggal_transaksi' => 'required',
            'keterangan_depresiasi' => 'required',
            'keterangan_akumulasi' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $id_asset = $request->input('id_asset');
            $beban_perbulan = $request->input('beban_perbulan');
            $akumulasi_beban = $request->input('akumulasi_beban');
            $nilai_buku = $request->input('nilai_buku');
            $nama = $request->input('nama');
            $master_id_asset = $request->input('master_id_asset');
            $master_id_depresiasi = $request->input('master_id_depresiasi');
            $master_id_akumulasi = $request->input('master_id_akumulasi');
            $tanggal_transaksi = $request->input('tanggal_transaksi');
            $no_transaksi = $request->input('no_transaksi');
            $keterangan_depresiasi = $request->input('keterangan_depresiasi');
            $keterangan_akumulasi = $request->input('keterangan_akumulasi');
            $year = date('Y', strtotime($tanggal_transaksi));
            $month = date('m', strtotime($tanggal_transaksi));
            $day = date('d', strtotime($tanggal_transaksi));
            $jmu_no = hAkunting::jmu_no($year, $month, $day);

            $master_asset = mAcMaster::where('master_id', $master_id_asset)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $master_depresiasi = mAcMaster::where('master_id', $master_id_depresiasi)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $master_akumulasi = mAcMaster::where('master_id', $master_id_akumulasi)->first(['mst_kode_rekening', 'mst_nama_rekening']);

            //data jurnal umum

            // General

            $data_jurnal = [
                'no_invoice' => $no_transaksi,
                'id_asset' => $id_asset,
                'jmu_tanggal' => Main::format_date_db($tanggal_transaksi),
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => 'Akumulasi Penyusutan ' . $master_asset->mst_nama_rekening . ' Asset No : ' . $no_transaksi,
            ];
            $response = mAcJurnalUmum::create($data_jurnal);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $data_transaksi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'id_asset' => $id_asset,
                'master_id' => $master_id_depresiasi,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $beban_perbulan,
                'trs_kredit' => 0,
                'user_id' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $master_depresiasi->mst_kode_rekening,
                'trs_nama_rekening' => $master_depresiasi->mst_nama_rekening,
                'trs_tipe_arus_kas' => 'Operasi',
                'trs_catatan' => $keterangan_depresiasi . ' : ' . $master_depresiasi->mst_nama_rekening . ' ' . $nama,
                'trs_charge' => 0,
                'trs_no_check_bg' => '',
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => 0,
                'tgl_transaksi' => Main::format_date_db($tanggal_transaksi),
            ];

            mAcTransaksi::create($data_transaksi);

            $data_transaksi_akumulasi = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'id_asset' => $id_asset,
                'master_id' => $master_id_akumulasi,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet' => 0,
                'trs_kredit' => $beban_perbulan,
                'user_id' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $master_akumulasi->mst_kode_rekening,
                'trs_nama_rekening' => $master_akumulasi->mst_nama_rekening,
                'trs_tipe_arus_kas' => 'operasi',
                'trs_catatan' => $keterangan_akumulasi . ' : ' . $master_akumulasi->mst_nama_rekening . ' ' . $nama,
                'trs_charge' => 0,
                'trs_no_check_bg' => '',
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => 0,
                'tgl_transaksi' => Main::format_date_db($tanggal_transaksi),
            ];

            mAcTransaksi::create($data_transaksi_akumulasi);


            // return Redirect::to('/jurnalUmum')->with('success', 'Posting Asset ke Jurnal Umum Berhasil');
            $data_asset = [
                'akumulasi_beban' => $akumulasi_beban,
                'nilai_buku' => $nilai_buku
            ];

            mAsset::where('id', $id_asset)->update($data_asset);


            //insert ke tb_penyusutan_asset
            $data_penyusutan = [
                'id_asset' => $id_asset,
                'penyusutan_perbulan' => $beban_perbulan,
                'bulan' => $month,
                'tahun' => $year
            ];
            mPenyusutanAsset::create($data_penyusutan);

            \DB::commit();


        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }
    }

    function edit($jurnal_umum_id)
    {
        $jurnal_umum_id = Main::decrypt($jurnal_umum_id);

        $data = Main::data($this->breadcrumb, $this->menuActive);
        $jurnal_umum = mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->first();
        $transaksi = mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->orderBy('transaksi_id', 'ASC')->get();
        $master = mAcMaster::orderBy('master_id', 'ASC')->get();


        $data = array_merge($data, [
            'pageTitle' => 'Edit Jurnal Umum',
            'jurnal_umum' => $jurnal_umum,
            'transaksi' => $transaksi,
            'master' => $master
        ]);

        return view('akunting/jurnalUmum/jurnalUmumEdit', $data);

    }

    function update(Request $request, $jurnal_umum_id)
    {
        $request->validate([
            'no_invoice' => 'required',
            'master_id.*' => 'required',
            'trs_debet.*' => 'required',
            'trs_kredit.*' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $jurnal_umum_id = Main::decrypt($jurnal_umum_id);
            $jmu_tanggal = $request->input('jmu_tanggal');
            $no_invoice = $request->input('no_invoice');
            $year = date('Y');
            $month = date('m');
            $jmu_keterangan = $request->input('jmu_keterangan');

            $master_id_arr = $request->input('master_id');
            $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
            $trs_debet_arr = $request->input('trs_debet');
            $trs_kredit_arr = $request->input('trs_kredit');
            $tipe_arus_kas_arr = $request->input('tipe_arus_kas');
            $trs_catatan_arr = $request->input('trs_catatan');

            $data_jurnal_umum = [
                'no_invoice' => $no_invoice,
                'jmu_tanggal' => Main::format_date_db($jmu_tanggal),
                'jmu_keterangan' => $jmu_keterangan,
            ];

            mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->update($data_jurnal_umum);

            /**
             * Delete semua transaksi, lalu masukkan data transaksi kembali
             */

            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();

            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first([
                        'mst_kode_rekening',
                        'mst_nama_rekening'
                    ]);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_jenis_transaksi = $trs_jenis_transaksi_arr[$index];
                $trs_debet = $trs_debet_arr[$index];
                $trs_kredit = $trs_kredit_arr[$index];
                $tipe_arus_kas = $tipe_arus_kas_arr[$index];
                $trs_catatan = $trs_catatan_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => $this->datetime,
                    'trs_setor' => 0,
                    'tgl_transaksi' => Main::format_date_db($jmu_tanggal)
                ];

                mAcTransaksi::create($data_transaksi);
            }

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }

    function delete($id_asset)
    {
        DB::beginTransaction();
        try {
            mAsset::where('id', $id_asset)->delete();
            mPenyusutanAsset::where('id_asset', $id_asset)->delete();
            mAcJurnalUmum::where('id_asset', $id_asset)->delete();
            mAcTransaksi::where('id_asset', $id_asset)->delete();

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }
    }


}
