<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

use DB,
    PDF;

class JurnalUmum extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_2'],
                'route' => ''
            ]
        ];
    }

    /**
     * @param $date_start
     * @param $date_end
     * @return array|mixed
     *
     * proses request data Jurnal Umum ke API
     */
    function list($date_start, $date_end){
        $data = Main::data($this->breadcrumb);

        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlJurnalUmum"];
        $client = new \GuzzleHttp\Client();

        /**
         * proses pengiriman request dengan parameter tanggal
         */
        $response = $client->request('POST', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ],
            'form_params'  => [
                'date_start' => $date_start,
                'date_end' => $date_end,
            ]
        ]);
        /**
         * respon berupa data
         */
        $result = (json_decode($response->getBody(),true));

        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $data = array_merge($data, $result['data']);
        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'company' => Main::companyInfo()
        ]);

        return $data;
    }

    function index(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('MAX_EXECUTION_TIME', -1);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $data = $this->list($date_start,$date_end);

        return view('akunting/jurnalUmum/jurnalUmumList', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * proses create
     */
    function create()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);
        $data = Main::data($this->breadcrumb, $this->menuActive);

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlJurnalUmum"];
        $client = new \GuzzleHttp\Client();

        /**
         * proses request data
         */
        $response = $client->request('GET', $endpoint.'/create', [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);

        /**
         * respo data
         */
        $result = (json_decode($response->getBody(),true));


        $list = $result['data'];
        $kode_perkiraan_option_list = $list['kode_perkiraan_option_list'];
        $data['pageTitle'] = 'Tambah Jurnal Umum';
        $data['no_invoice'] = $list['no_invoice'];
        $data['kode_perkiraan_option_list'] = $kode_perkiraan_option_list;

        return view('akunting/jurnalUmum/jurnalUmumCreate', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * proses insert
     */
    function insert(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);
        /**
         * membuat rules data yang wajib di isi
         */
        $rules = [
            'jmu_keterangan' => 'required',
            'no_invoice' => 'required',
            'master_id' => 'required',
            'master_id.*' => 'required',
            'trs_debet' => 'required',
            'trs_debet.*' => 'required',
            'trs_kredit' => 'required',
            'trs_kredit.*' => 'required',
        ];

        $attributes = [
            'jmu_keterangan' => 'Keterangan',
            'no_invoice' => 'No Invoice',
            'master_id' => 'Kode Perkiraan',
            'trs_debet' => 'Nominal Debet',
            'trs_kredit' => 'Nominal Kredit',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan';
            $attr['trs_debet.' . $i] = 'Nominal Debet';
            $attr['trs_kredit.' . $i] = 'Nominal Kredit';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        /**
         *membuat kondisi jika validator gagal, maka return respon erorrs
         */
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * memasukan data request kedalam variabel
         */
        $jmu_tanggal = $request->input('jmu_tanggal');
        $no_invoice = $request->input('no_invoice');
        $jmu_keterangan = $request->input('jmu_keterangan');

        $master_id_arr = $request->input('master_id');
        $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
        $trs_debet_arr = $request->input('trs_debet');
        $trs_kredit_arr = $request->input('trs_kredit');
        $tipe_arus_kas_arr = $request->input('tipe_arus_kas');
        $trs_catatan_arr = $request->input('trs_catatan');

        /**
         * merubah format data angka dengan class input-numeral menjadi angka biasa
         */
        foreach($trs_debet_arr as $index=>$debet){
            $trs_debet_arr[$index] = Main::string_to_number($debet);
        }
        foreach($trs_kredit_arr as $index=>$kredit){
            $trs_kredit_arr[$index] = Main::string_to_number($kredit);
        }

        /**
         * menjumlahkan data array dengan tipe angka
         */
        $total_debet = array_sum($trs_debet_arr);
        $total_kredit = array_sum($trs_kredit_arr);

        /**
         * mengatur kondisi jika data tidak balance (bernilai sama), maka return respon data tidak balance
         */
        if ($total_debet !== $total_kredit) {
            return response([
                'message' => 'Tidak Balance antara Total Debet dengan Total Kredit'
            ], 422);
        }

        /**
         * GET data User dari session
         */
        $user = session('user');
        $data = Main::data($this->breadcrumb, $this->menuActive);

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlJurnalUmum"];
        $client = new \GuzzleHttp\Client();

        /**
         * proses mengirim data untuk disimpan di API
         */
        $response = $client->request('POST', $endpoint . '/insert', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $data['acctoken'],
            ],
            'form_params' => [
                'user_id' => $user['id'],
                'user_name' => $user['username'],
                'user_table_name' => 'tb_user',
                'jmu_tipe_input' => 'jurnal_umum',
                'jmu_tanggal' => Main::format_date_db($jmu_tanggal),
                'jmu_keterangan' => $jmu_keterangan,
                'no_invoice' => $no_invoice,
                'master_id' => $master_id_arr,
                'trs_debet' => $trs_debet_arr,
                'trs_kredit' => $trs_kredit_arr,
                'trs_jenis_transaksi' => $trs_jenis_transaksi_arr,
                'trs_tipe_arus_kas' => $tipe_arus_kas_arr,
                'trs_catatan' => $trs_catatan_arr,
            ]
        ]);
    }

    /**
     * @param $jurnal_umum_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * proses edit
     */
    function edit($jurnal_umum_id)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);

        /**
         * melakukanj decrypt pada variabel yang ter enkripsi
         */
        $jurnal_umum_id = Main::decrypt($jurnal_umum_id);

        $data = Main::data($this->breadcrumb, $this->menuActive);
        /**
         * url API yang dituju
         */
        $endpoint = $data["urlJurnalUmum"];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $endpoint.'/edit', [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ],
            'form_params' => [
                'jurnal_umum_id' => $jurnal_umum_id
            ]
        ]);

        /**
         * respon data dalam 2 bentuk
         */
        $result = json_decode($response->getBody());
        $result_true = (json_decode($response->getBody(),true));


        $data = array_merge($data, [
            'pageTitle' => 'Edit Jurnal Umum',
            'jurnal_umum' => $result->data->jurnal_umum[0],
            'jurnal_umum_id' => $jurnal_umum_id,
            'transaksi' => $result->data->jurnal_umum[0]->transaksi,
            'kode_perkiraan_option_list' => $result_true['data']['kode_perkiraan_option_list'],
            'total_debet' => $result->data->total_debet,
            'total_kredit' => $result->data->total_kredit
        ]);
//        dd($data);

        return view('akunting/jurnalUmum/jurnalUmumEdit', $data);

    }

    function update(Request $request, $jurnal_umum_id)
    {
        /**
         * membuat rules data yang wajib di isi
         */
        $rules = [
            'jmu_keterangan' => 'required',
            'no_invoice' => 'required',
            'master_id' => 'required',
            'master_id.*' => 'required',
            'trs_debet' => 'required',
            'trs_debet.*' => 'required',
            'trs_kredit' => 'required',
            'trs_kredit.*' => 'required',
        ];

        $attributes = [
            'jmu_keterangan' => 'Keterangan',
            'no_invoice' => 'No Invoice',
            'master_id' => 'Kode Perkiraan',
            'trs_debet' => 'Nominal Debet',
            'trs_kredit' => 'Nominal Kredit',
        ];
        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan';
            $attr['trs_debet.' . $i] = 'Nominal Debet';
            $attr['trs_kredit.' . $i] = 'Nominal Kredit';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        /**
         *membuat kondisi jika validator gagal, maka return respon erorrs
         */
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $jurnal_umum_id = Main::decrypt($jurnal_umum_id);
        $jmu_tanggal = $request->input('jmu_tanggal');
        $no_invoice = $request->input('no_invoice');
        $jmu_keterangan = $request->input('jmu_keterangan');

        $master_id_arr = $request->input('master_id');
        $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
        $trs_debet_arr = $request->input('trs_debet');
        $trs_kredit_arr = $request->input('trs_kredit');
        $tipe_arus_kas_arr = $request->input('tipe_arus_kas');
        $trs_catatan_arr = $request->input('trs_catatan');

        /**
         * merubah format data angka dengan class input-numeral menjadi angka biasa
         */
        foreach($trs_debet_arr as $index=>$debet){
            $trs_debet_arr[$index] = Main::string_to_number($debet);
        }
        foreach($trs_kredit_arr as $index=>$kredit){
            $trs_kredit_arr[$index] = Main::string_to_number($kredit);
        }

        /**
         * menjumlahkan data array dengan tipe angka
         */
        $total_debet = array_sum($trs_debet_arr);
        $total_kredit = array_sum($trs_kredit_arr);

        /**
         * mengatur kondisi jika data tidak balance (bernilai sama), maka return respon data tidak balance
         */
        if ($total_debet !== $total_kredit) {
            return response([
                'message' => 'Tidak Balance antara Total Debet dengan Total Kredit'
            ], 422);
        }

        /**
         * get data user dari session
         */
        $user = session('user');

        $data = Main::data($this->breadcrumb, $this->menuActive);

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlJurnalUmum"];

        $client = new \GuzzleHttp\Client();

        /**
         * proses update data di API
         */
        $response = $client->request('POST', $endpoint . '/update', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $data['acctoken'],
            ],
            'form_params' => [
                'jurnal_umum_id' => $jurnal_umum_id,
                'user_id' => $user['id'],
                'user_name' => $user['username'],
                'user_table_name' => 'tb_user',
                'jmu_tipe_input' => 'jurnal_umum',
                'jmu_tanggal' => Main::format_date_db($jmu_tanggal),
                'jmu_keterangan' => $jmu_keterangan,
                'no_invoice' => $no_invoice,
                'master_id' => $master_id_arr,
                'trs_debet' => $trs_debet_arr,
                'trs_kredit' => $trs_kredit_arr,
                'trs_jenis_transaksi' => $trs_jenis_transaksi_arr,
                'trs_tipe_arus_kas' => $tipe_arus_kas_arr,
                'trs_catatan' => $trs_catatan_arr,
            ]
        ]);


    }

    function delete($jurnal_umum_id)
    {
        /**
         * melakukan decrypt pada data yang di eknripsi
         */
        $jurnal_umum_id = Main::decrypt($jurnal_umum_id);
        $data = Main::data($this->breadcrumb);
        $endpoint = $data["urlJurnalUmum"];
        $client = new \GuzzleHttp\Client();

        /**
         * mengirim request untuk mnghapus data ke API
         */
        $response = $client->post($endpoint.'/delete', [
            'form_params' => [
                'jurnal_umum_id' => $jurnal_umum_id,
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);
    }


    function pdf(Request $request)
    {

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 30000000);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $data = $this->list($date_start,$date_end);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                    ->loadView('akunting/jurnalUmum/jurnalUmumPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Jurnal Umum ' . date('d-m-Y') );
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 30000000);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : date('Y-m-d');
        $date_end = $date_end ? $date_end : date('Y-m-d');

        $data = $this->list($date_start,$date_end);


        return view('akunting/jurnalUmum/jurnalUmumExcel', $data);
    }
}
