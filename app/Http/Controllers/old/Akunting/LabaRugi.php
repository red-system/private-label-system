<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\Main;
use app\Helpers\hAkunting;
use app\Models\mAcJurnalUmum;
use app\Models\mAcMaster;
use app\Models\mAcTransaksi;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use DB,
    PDF;

class LabaRugi extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $datetime;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_2'];
        $this->datetime = date('Y-m-d H:i:s');
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        ini_set('max_execution_time', 300);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();



        $data = $this->list($date_start, $date_end);


        return view('akunting/labaRugi/labaRugiList', $data);
    }

    /**
     * @param $date_start
     * @param $date_end
     * @return array|mixed
     * fungsi untuk mendapat list data yang akan ditampilkan
     */
    function list($date_start, $date_end){
        $data = Main::data($this->breadcrumb);
        $date_end = Main::format_date_db($date_end);
        $date_start = Main::format_date_db($date_start);
        $endpoint = $data["urlLabaRugi"];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ],
            'form_params'  => [
                'date_start' => $date_start,
                'date_end' => $date_end,
            ]
        ]);
        $result = (json_decode($response->getBody(), true));
        $list = $result['data'];
        $params = [
            'date_start' => $date_start,
            'date_end' => $date_end
        ];
        $data = array_merge($data, $list);
        $data = array_merge($data, [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'params' => $params,
            'space1' => hAkunting::perkiraan_space(1),
            'space2' => hAkunting::perkiraan_space(2),
            'space3' => hAkunting::perkiraan_space(3),
            'space4' => hAkunting::perkiraan_space(4),
            'company' => Main::companyInfo()

        ]);

        return $data;
    }

    function pdf(Request $request)
    {
        ini_set('max_execution_time', 300);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();

        $data = $this->list($date_start, $date_end);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                    ->loadView('akunting/labaRugi/labaRugiPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Laba Rugi ' . date('d-m-Y'));
    }

    function excel(Request $request)
    {
        ini_set('max_execution_time', 300);
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $date_start = $date_start ? $date_start : now();
        $date_end = $date_end ? $date_end : now();

        $data = $this->list($date_start, $date_end);

        return view('akunting/labaRugi/labaRugiExcel', $data);
    }

}