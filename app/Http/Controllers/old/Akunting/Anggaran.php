<?php

namespace app\Http\Controllers\Akunting;

use app\Helpers\hAkunting;
use app\Helpers\Main;
use app\Models\Widi\mAnggaran;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

use DB,
    PDF;

class Anggaran extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['akunting_9'];
        $this->breadcrumb = [
            [
                'label' => $cons['akunting'],
                'route' => ''
            ],
            [
                'label' => $cons['akunting_9'],
                'route' => ''
            ]
        ];
    }

    function index(){
        $data = Main::data($this->breadcrumb);

        /**
         * url API tujuan
         */
        $endpoint = $data["urlAnggaran"];
        $client = new \GuzzleHttp\Client();

        /**
         * request data ke API
         */
        $response = $client->request('GET', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);


        /**
         * respon data
         */
        $result = (json_decode($response->getBody(),true));

        $data = array_merge($data, [
            'anggaran' => $result['data']
        ]);

        return view('akunting/anggaran/anggaranList', $data);
    }

    function create()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);
        $data = Main::data($this->breadcrumb, $this->menuActive);

        /**
         * url API yang dituju
         */
        $client = new \GuzzleHttp\Client();

        $coa = $data["urlCoa"];

        /**
         * request data ke API
         */
        $response = $client->request('GET', $coa, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);


        /**
         * respo data
         */
        $result = (json_decode($response->getBody(),true));


        $list = $result['data'];
        $data['pageTitle'] = 'Tambah Data';
        $data['kode_perkiraan'] = $list;

        return view('akunting/anggaran/anggaranCreate', $data);
    }

    function insert(Request $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);
        /**
         * membuat rules data yang wajib di isi
         */
        $rules = [
            'anggaran_tahun' => 'required',
            'anggaran.*' => 'required',
        ];

        $attributes = [
            'anggaran_tahun' => 'Tahun Anggaran'
        ];
        $attr = [];
        for ($i = 0; $i <= 300; $i++) {
            $next = $i + 1;
            $attr['anggaran.' . $i] = 'Anggaran';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        /**
         *membuat kondisi jika validator gagal, maka return respon erorrs
         */
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * memasukan data request kedalam variabel
         */
        $anggaran_tahun = $request->input('anggaran_tahun');

        $master_id_arr = $request->input('master_id');
        $anggaran_arr = $request->input('anggaran');


        /**
         * merubah format data angka dengan class input-numeral menjadi angka biasa
         */
        $anggaran=[];
        foreach($master_id_arr as $index=>$id){
            $anggaran[$index]['master_id'] = $id;
        }
        foreach($anggaran_arr as $key=>$nilai){
            $anggaran[$key]['nominal'] = Main::string_to_number($nilai);
        }
        $anggaran = json_encode($anggaran);
        $anggaran = (json_decode($anggaran,true));


//        return $anggaran;
//        dd($anggaran);

        /**
         * GET data User dari session
         */
        $user = session('user');
        $data = Main::data($this->breadcrumb);

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlAnggaran"];
        $client = new \GuzzleHttp\Client();

        $tahun = $client->request('GET', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);

        $tahun_list = (json_decode($tahun->getBody(),true));
        $tahun=[];
        foreach ($tahun_list['data'] as $index=>$list){
            $tahun[$index] = $list['anggaran_tahun'];
        }

        if(in_array($anggaran_tahun, $tahun)){
            return response([
                'message' => 'Tahun Anggaran Sudah Ada'
            ], 422);
        }

        /**
         * proses mengirim data untuk disimpan di API
         */
        $response = $client->request('POST', $endpoint . '/insert', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $data['acctoken'],
            ],
            'form_params' => [
                'anggaran_tahun' => $anggaran_tahun,
                'user_id_created' => $user['id'],
                'user_name_created' => $user['username'],
                'user_table_name' => 'tb_user',
                'anggaran' => $anggaran,
            ]
        ]);

    }

    function edit($anggaran_id)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);

        $anggaran_id = Main::decrypt($anggaran_id);
        $data = Main::data($this->breadcrumb, $this->menuActive);

        /**
         * url API yang dituju
         */
        $client = new \GuzzleHttp\Client();

        $coa = $data["urlAnggaran"];

        /**
         * request data ke API
         */
        $response = $client->request('POST', $coa.'/edit', [
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ],
            'form_params' => [
                'anggaran_id' => $anggaran_id
            ]
        ]);


        /**
         * respo data
         */
        $result = (json_decode($response->getBody(),true));


        $list = $result['data'];
        $data['pageTitle'] = 'Edit Data';
        $data['anggaran_id'] = $list['anggaran_id'];
        $data['anggaran_tahun'] = $list['anggaran_tahun'];
        $data['anggaran_total'] = $list['anggaran_total'];
        $data['anggaran_detail'] = $list['anggaran_detail'];

        return view('akunting/anggaran/anggaranEdit', $data);
    }

    function update(Request $request, $anggaran_id)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 180);
        /**
         * membuat rules data yang wajib di isi
         */
        $rules = [
            'anggaran_tahun' => 'required',
            'anggaran.*' => 'required',
        ];

        $attributes = [
            'anggaran_tahun' => 'Tahun Anggaran'
        ];
        $attr = [];
        for ($i = 0; $i <= 300; $i++) {
            $next = $i + 1;
            $attr['anggaran.' . $i] = 'Anggaran';
        }
        $attributes = array_merge($attributes, $attr);

        $validator = Validator::make($request->all(), $rules, [], $attributes);

        /**
         *membuat kondisi jika validator gagal, maka return respon erorrs
         */
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        /**
         * memasukan data request kedalam variabel
         */
        $anggaran_id = Main::decrypt($anggaran_id);
        $anggaran_tahun = $request->input('anggaran_tahun');

        $master_id_arr = $request->input('master_id');
        $anggaran_arr = $request->input('anggaran');


        /**
         * merubah format data angka dengan class input-numeral menjadi angka biasa
         */
        $anggaran=[];
        foreach($master_id_arr as $index=>$id){
            $anggaran[$index]['master_id'] = $id;
        }
        foreach($anggaran_arr as $key=>$nilai){
            $anggaran[$key]['nominal'] = Main::string_to_number($nilai);
        }
        $anggaran = json_encode($anggaran);
        $anggaran = (json_decode($anggaran,true));


//        return $anggaran;
//        dd($anggaran);

        /**
         * GET data User dari session
         */
        $user = session('user');
        $data = Main::data($this->breadcrumb);

        /**
         * url API yang dituju
         */
        $endpoint = $data["urlAnggaran"];
        $client = new \GuzzleHttp\Client();

        /**
         * proses mengirim data untuk disimpan di API
         */
        $response = $client->request('POST', $endpoint . '/update', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $data['acctoken'],
            ],
            'form_params' => [
                'anggaran_id' => $anggaran_id,
                'anggaran_tahun' => $anggaran_tahun,
                'user_id_updated' => $user['id'],
                'user_name_updated' => $user['username'],
                'user_table_name' => 'tb_user',
                'anggaran' => $anggaran,
            ]
        ]);
    }

    function list($anggaran_id){

        $anggaran_id = Main::decrypt($anggaran_id);
        $data = Main::data($this->breadcrumb, $this->menuActive);
        $endpoint = $data["urlAnggaran"];
        $client = new \GuzzleHttp\Client();

        /**
         * mengirim request untuk mnghapus data ke API
         */
        $response = $client->request('POST',$endpoint.'/detail', [
            'form_params' => [
                'anggaran_id' => $anggaran_id,
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);

        $result = (json_decode($response->getBody(),true));
        $list = $result['data'];

        $data['pageTitle'] = 'Detail';
        $data['anggaran_id'] = $list['anggaran_id'];
        $data['anggaran_tahun'] = $list['anggaran_tahun'];
        $data['anggaran_total'] = $list['anggaran_total'];
        $data['budget_status'] = $list['budget_status'];
        $data['realisasi_anggaran_total'] = $list['realisasi_anggaran_total'];
        $data['anggaran_detail'] = $list['anggaran_detail'];
        $data['company'] = Main::companyInfo();

        return $data;
    }

    function detail($anggaran_id){

        $data = $this->list($anggaran_id);

        return view('akunting/anggaran/anggaranDetail', $data);
    }

    function pdf($anggaran_id){

        $data = $this->list($anggaran_id);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('akunting/anggaran/anggaranPdf', $data);
        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Anggaran ' . $data['anggaran_tahun'] );
    }

    function excel($anggaran_id){

        $data = $this->list($anggaran_id);

        return view('akunting/anggaran/anggaranExcel', $data);
    }

    function delete($anggaran_id)
    {
        /**
         * melakukan decrypt pada data yang di eknripsi
         */
        $anggaran_id = Main::decrypt($anggaran_id);
        $data = Main::data($this->breadcrumb);
        $endpoint = $data["urlAnggaran"];
        $client = new \GuzzleHttp\Client();

        /**
         * mengirim request untuk mnghapus data ke API
         */
        $response = $client->request('POST',$endpoint.'/delete', [
            'form_params' => [
                'anggaran_id' => $anggaran_id,
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$data['acctoken'],
            ]
        ]);
    }
}
