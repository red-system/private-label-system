<?php

namespace app\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Http\Controllers\Inventory\PemindahanStok;
use app\Helpers\Main;

use app\Models\Bayu\mPemakaian;
use app\Models\Bayu\mItemPemakaian;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mSatuan;
use Illuminate\Support\Facades\DB;

class Pemakaian extends Controller
{
//    TODO : perbaiki penanggalan kartu stok agar include dengan jam juga
    private $breadcrumb;
    private $view = [
        'no_pemakaian' => ["search_field" => "tb_pemakaian.no_pemakaian"],
        'tgl_pemakaian' => ["search_field" => "tb_pemakaian.tgl_pemakaian"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'keterangan' => ["search_field" => "tb_pemakaian.keterangan"],
    ];
    private $produkList = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'golongan' => ["search_field" => "tb_golongan.golongan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_7'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('inventory/pemakaian/pemakaianList', $data);
    }


    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $pemakaian = new mPemakaian();
        $result['iTotalRecords'] = $pemakaian->count_all();
        $result['iTotalDisplayRecords'] = $pemakaian->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pemakaian->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->tgl_pemakaian = date('d-m-Y', strtotime($value->tgl_pemakaian));
            $value->route['delete'] = route('pemakaianDelete', ['id' => $value->id]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function tambah()
    {
        $pemakaian = new mPemakaian();
        $data = Main::data($this->breadcrumb, 'pemakaian');
        $dataproduk = array();
        // array_push($dataproduk,array("data"=>"no"));
        foreach ($this->produkList as $key => $value) {
            array_push($dataproduk, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        session()->forget('id_location');
        $data['actionButton'] = json_encode($action);
        $data['produkList'] = json_encode($dataproduk);
        $data['lokasi'] = mLokasi::all();
        $data['satuan'] = mSatuan::all();
        $data['json_satuan'] = json_encode($data['satuan']);
        // $data['no_pemakaian'] = $pemakaian->noTerakhir() + 1;
        // dd($data);
        return view('inventory/pemakaian/pemakaianTambah', $data);
    }

    function insertTambah(Request $request)
    {
        $request->validate([
            'no_pemakaian' => 'required',
            'tgl_pemakaian' => 'required',
            'lokasi_barang' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $no_pemakaian = $request->input('no_pemakaian');
            $tgl_pemakaian = Main::format_date_db($request->input('tgl_pemakaian'));
            $lokasi_barang = $request->input('lokasi_barang');
            $keterangan = $request->input('keterangan');
            $item_id_stok = $request->input('item_id_stok');
            $item_id_barang = $request->input('item_id_barang');
            $item_id_supplier = $request->input('item_id_supplier');
            $item_qty = $request->input('item_qty');
            $item_satuan = $request->input('item_satuan');

            $lokasi = new mLokasi();
            $pemakaian = new mPemakaian();
            $stok = new mStok();
            $pemindahanController = new PemindahanStok();

            if ($keterangan == null || $keterangan == '') {
                $keterangan = 'Pemakaian Barang Dari '.$lokasi->getNameById($request->lokasi_barang)->lokasi;
            }

            // Simpan ke tb_pemakaian
            $new_pemakaian = [
                'no_pemakaian' => $no_pemakaian,
                'tgl_pemakaian' => $tgl_pemakaian,
                'id_lokasi' => $lokasi_barang,
                'keterangan' => $keterangan
            ];
            $id_pemakaian = mPemakaian::create($new_pemakaian)->id;

            // Simpan Item Pemakaian
            foreach ($item_id_stok as $key => $value) {
                $itemIdStok = $item_id_stok[$key];
                $itemIdBarang = $item_id_barang[$key];
                $itemIdSupplier = $item_id_supplier[$key];
                $itemQty = $item_qty[$key];
                $itemSatuan = $item_satuan[$key];

                $newItemPemakaian = new mItemPemakaian;
                $newItemPemakaian -> id_pemakaian = $id_pemakaian;
                $newItemPemakaian -> id_stok = $itemIdStok;
                $newItemPemakaian -> jml_barang = $itemQty;
                $newItemPemakaian -> id_satuan = $itemSatuan;
                $newItemPemakaian -> save();

                // Kartu Stok
                $hasilPengurangan = $stok->penguranganJml($itemIdStok,$itemQty)->pengurangan;
                // dd($hasilPengurangan);
                $pemakaian = new mPemakaian();
                $pemindahanController->kartuStok($request->lokasi_barang, null, $itemIdBarang, $itemIdStok, null, $request->keterangan, $itemQty, $hasilPengurangan, null, $id_pemakaian, 'pemakaian', false, false, true);

                // Update Stok
                $updateStok = mStok::find($itemIdStok);
                $updateStok -> jml_barang = $hasilPengurangan;
                $updateStok -> save();
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

        return redirect(route('pemakaianPage'));
    }

    function delete($id)
    {
        $itemPemakaian = new mItemPemakaian();
        $countItemPemakaian = $itemPemakaian->countIdPemakaian($id);
        for ($i = 0; $i < $countItemPemakaian; $i++) {
            mItemPemakaian::where('id_pemakaian', $id)->delete();
        }
        mPemakaian::where('id', $id)->delete();
    }
}
