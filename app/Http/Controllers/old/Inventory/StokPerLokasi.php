<?php

namespace app\Http\Controllers\Inventory;

use Barryvdh\DomPDF\Facade as PDF;
use app\Helpers\Main;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class StokPerlokasi extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'wilayah' => ["search_field" => "tb_wilayah.wilayah"],
        'jml_barang' => ["search_field" => "tb_stok.jml_barang"],
        'satuan' => ["search_field" => "tb_stok.satuan"],
        'supplier' => ["search_field" => "tb_supplier.supplier"],
        'date_expired' => ["search_field" => "tb_stok.date_expired"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_2'],
                'route' => ''
            ],
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $detail = array();
//        $satuan = mSatuan::all();
        $lokasi = mLokasi::orderBy('lokasi')->get();
//        $supplier = mSupplier::all();

        $all_request = $request->input();

        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $data['url'] = '?' . 'id_lokasi' . '=' . $id_lokasi;
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }
        $params = [
            'id_lokasi' => $id_lokasi,
        ];
        $data['params'] = $params;
        $data['id_lokasi'] = $id_lokasi;
        $data['detail'] = json_encode($detail);
//        $data['satuan'] = $satuan;
        $data['lokasi'] = $lokasi;
//        $data['supplier'] = $supplier;
        return view('inventory/stokPerlokasi/stokList', $data);
    }

    public function pdf(Request $request)
    {

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_lokasi = $request->input('id_lokasi');
        $stok = new mStok;
        $list = $stok->list($start, $length, $query, $this->detail, $id_lokasi);
        foreach ($list as $l) {
            $l->date_expired = Main::format_date($l->date_expired);
            $l->jml_barang = Main::format_number($l->jml_barang);
        }
        if ($id_lokasi == 'all') {
            $lokasi = 'Semua Lokasi';
        } else {
            $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');

        }
        $lokasi_all = mLokasi::orderBy('lokasi')->get();

        $data['lokasi'] = $lokasi;
        $data['lokasi_all'] = $lokasi_all;
        $data['list'] = $list;
        $data['company'] = Main::companyInfo();

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadview('inventory.stokPerlokasi.print', $data);
        return $pdf->stream('Data Stok di ' . $lokasi);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $id_lokasi = $request->input('id_lokasi');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $stok = new mStok;
        $result['iTotalRecords'] = $stok->count_all();
        $result['iTotalDisplayRecords'] = $stok->count_filter($query, $this->detail, $id_lokasi);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list($start, $length, $query, $this->detail, $id_lokasi);
        $no = $start + 1;
        foreach ($data as $value) {
            $tanggal = $value->date_expired;
            $date = Main::format_date($value->date_expired);
            if ($tanggal == null) {
                $value->date_expired = $tanggal;
            } else {
                $value->date_expired = $date;
            }
            $value->no = $no;
            $no++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
