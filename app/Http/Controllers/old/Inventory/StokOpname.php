<?php

namespace app\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Http\Controllers\Inventory\PemindahanStok;
use app\Helpers\Main;


use app\Models\Bayu\mStok;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mGolongan;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mStokOpname;

class StokOpname extends Controller
{
    private $breadcrumb;
    private $view = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'golongan' => ["search_field" => "golongan"],
        'harga_jual_barang' => ["search_field" => "harga_jual_barang"],
        'hpp' => ["search_field" => "hpp"],
        'harga_beli_terakhir_barang' => ["search_field" => "harga_beli_terakhir_barang"],
        'total_stok' => ["search_field" => "total_stok"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_10'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['lokasi'] = mLokasi::all();
        $data['view'] = json_encode($view);
        return view('inventory/stokOpname/stokOpnameList', $data);
    }


    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok();
        $result['iTotalRecords'] = $stok->count_all_stokOpname();
        $result['iTotalDisplayRecords'] = $stok->count_filter_stokOpname($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        // $data = $stokOpname->list($start,$length,$query,$this->view);
        $data = $stok->list_stokOpname($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            // $value->route['delete'] = route('stokOpnameDelete',['id'=>$value->id]);

            $value->route['tambah_stok_opname'] = route('stokOpnameTambahStok',['id'=>$value->id_barang]);
            $value->visibility['delete'] = 'none';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'id_lokasi' => 'required',
            'tgl_opname' => 'required',
            'qty_data' => 'required',
            'satuan_qty_data' => 'required',
            'qty_fisik' => 'required',
            'satuan_qty_fisik' => 'required',
            'selisih_qty' => 'required',
            'selisih_satuan' => 'required',

        ]);

        $stok = new mStok();
        $lokasi = new mLokasi();
        $pemindahanStok = new PemindahanStok();
        $stokOpname = new mStokOpname();

        // ====== START MAKE STOK OPNAME ======
        if ($request->keterangan == null || $request->keterangan == '') {
            $lokasiAsal = $lokasi->getNameById($request->id_lokasi);
            $request->keterangan = 'Stok Opname Dari ' . $lokasiAsal->lokasi;
        }
        $request->tgl_opname = date('Y-m-d h:i:s', strtotime($request->tgl_opname));

        // $data = $request->except(['_token']);
        // mStokOpname::create($data);
        $opname = new mStokOpname;
        $opname->id_barang = $request->id;
        $opname->id_lokasi = $request->id_lokasi;
        $opname->kode_barang = $request->kode_barang;
        $opname->nama_barang = $request->nama_barang;
        $opname->tgl_opname = $request->tgl_opname;
        $opname->qty_data = $request->qty_data;
        $opname->satuan_qty_data = $request->satuan_qty_data;
        $opname->qty_fisik = $request->qty_fisik;
        $opname->satuan_qty_fisik = $request->satuan_qty_fisik;
        $opname->selisih_qty = $request->selisih_qty;
        $opname->selisih_satuan = $request->selisih_satuan;
        $opname->keterangan = $request->keterangan;
        $opname->save();

        // dd('kode barang:'.$request->kode_barang.' idLokasi:'.$request->id_lokasi.' tgl_opname:'.$request->tgl_opname.' selisih_qty:'.$request->selisih_qty);
        $newOpname = $stokOpname->getNewId($request->kode_barang, $request->id_lokasi, $request->tgl_opname)->id;
        // ====== END MAKE STOK OPNAME ======


        // ======= START MAKE KARTU STOK AND UPDATE STOK ========
        $qtyData = $request->qty_data;
        $qtyFisik = $request->qty_fisik;
        if ($qtyFisik > $qtyData) {
            // STOK MASUK
            // TODO : BERIKAN KONDISI PENGECEKAN APAKAH BARANG MERUPAKAN BARANG ASSEMBLY
            //      JIKA BARANG MERUPAKAN BARANG ASSEMBLY, MAKA LAKUKAN PROSES PENAMBAHAN STOK ASSEMBLY
            //      SETELAH ITU, BARU LAKUKAN PENAMBAHAN STOK BIASA PADA BARANG ASSEMBLY
            $hasilPenjumlahan = $stok->penjumlahanJml($request->id_stok, $request->selisih_qty)->penjumlahan;
            $hasilHppBarang = $pemindahanStok->kartuStok(null, $request->id_lokasi, $request->id, null, $request->id_stok, $request->keterangan, $request->selisih_qty, null, $hasilPenjumlahan, $newOpname, 'stokOpname', false, true, false);

            $updateStok = mStok::find($request->id_stok);
            $updateStok->jml_barang = $hasilPenjumlahan;
            $updateStok->save();

            $updateBarang = mBarang::find($request->id);
            $updateBarang->hpp = $hasilHppBarang;
            $updateBarang->save();
        } else if ($qtyFisik < $qtyData) {
            // STOK KELUAR 
            // TODO : BERIKAN KONDISI PENGECEKAN APAKAH BARANG MERUPAKAN BARANG ASSEMBLY
            //      JIKA BARANG MERUPAKAN BARANG ASSEMBLY, MAKA LAKUKAN PROSES PENGURANGAN STOK ASSEMBLY
            //      SETELAH ITU, BARU LAKUKAN PENGURANGAN STOK BIASA PADA BARANG ASSEMBLY
            $hasilPengurangan = $stok->penguranganJml($request->id_stok, $request->selisih_qty)->pengurangan;
            $hasilHppBarang = $pemindahanStok->kartuStok($request->id_lokasi, null, $request->id, $request->id_stok, null, $request->keterangan, $request->selisih_qty, $hasilPengurangan, null, $newOpname, 'stokOpname', false, false, true);

            $updateStok = mStok::find($request->id_stok);
            $updateStok->jml_barang = $hasilPengurangan;
            $updateStok->save();

            $updateBarang = mBarang::find($request->id);
            $updateBarang->hpp = $hasilHppBarang;
            $updateBarang->save();
        }
        // ======= END MAKE KARTU STOK AND UPDATE STOK ========
    }

    function filterLokasi(Request $request)
    {
        $stok = new mStok();
        $data = $stok->filterLokasiStokOpname($request->id_barang);

        return $data;
    }
}
