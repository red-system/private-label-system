<?php

namespace app\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use app\Http\Controllers\Inventory\PemindahanStok;

use app\Models\Bayu\mBarang;
use app\Models\Bayu\mGolongan;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mSatuan;
use app\Models\Bayu\mAssembly;
use app\Models\Bayu\mItemAssembly;
use app\Models\Bayu\mStok;

use Illimunate\Support\Facades\Validator;

class Assembly extends Controller
{
    private $breadcrumb;
    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'golongan' => ["search_field" => "tb_golongan.golongan"],
        'hpp' => ["search_field" => "tb_barang.hpp"],
        'harga_beli_terakhir_barang' => ["search_field" => "tb_barang.harga_beli_terakhir_barang"],
        'harga_jual_barang' => ["search_field" => "tb_barang.harga_jual_barang"],
        'total_stok' => ["search_field" => "perhitungan.total_stok"],
    ];
    private $produkList = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'golongan' => ["search_field" => "golongan"],
        'satuan' => ["search_field" => "satuan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_1'],
                'route' => ''
            ]
        ];
    }

    function assembly()
    {
        $data = Main::data($this->breadcrumb, 'barang');
        $produkList = array();
        foreach ($this->produkList as $key => $value) {
            array_push($produkList, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['produkList'] = json_encode($produkList);
        $data['golongan'] = mGolongan::all();;
        $data['barang'] = mBarang::all();
        $data['json_satuan'] = mSatuan::all();

        return view('inventory/barang/barangAssembly', $data);
    }

    function assemblyBarangAll(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();

        $result['iTotalRecords'] = $barang->count_all_assembly();
        $result['iTotalDisplayRecords'] = $barang->count_filter_assembly($query, $this->produkList);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $barang->list_all_assembly($start, $length, $query, $this->produkList);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function assembly_insert(Request $request)
    {
        $request->validate([
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'id_golongan' => 'required',
            'harga_beli_terakhir_barang' => 'required',
            'harga_jual_barang' => 'required|gt:0',
            'hpp' => 'required|gt:0',
            'total_item' => 'gt:0',
            'item_qty' => 'required|array',
            'item_qty.*' => 'required|numeric|gt:0',
        ]);

        DB::beginTransaction();
        try {
            // === DATA INPUT ===
            $kode_barang = $request->input('kode_barang');
            $nama_barang = $request->input('nama_barang');
            $id_golongan = $request->input('id_golongan');
            $harga_beli_terakhir_barang = $request->input('harga_beli_terakhir_barang');
            $harga_jual_barang = $request->input('harga_jual_barang');
            $hpp = $request->input('hpp');
            $total_item = $request->input('total_item');
            $kode_barang_array = $request->input('item_kode');
            $id_barang_array = $request->input('item_id_barang');
            $nama_array = $request->input('item_nama');
            $qty_array = $request->input('item_qty');
            $satuan_array = $request->input('item_satuan');

            // START CREATE PRODUK
            $dataProduk = [
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama_barang,
                'id_golongan' => $id_golongan,
                'harga_beli_terakhir_barang' => Main::string_to_number($harga_beli_terakhir_barang),
                'harga_jual_barang' => $harga_jual_barang,
                'hpp' => $hpp,
            ];
            $idProduk = mBarang::create($dataProduk)->id;
            // END CREATE PRODUK


            // START CREATE ASSEMBLY
            $dataAssembly = [
                'id_barang' => $idProduk,
            ];
            $idAssembly = mAssembly::create($dataAssembly)->id;
            // END CREATE ASSEMBLY


            // START CREATE ITEM ASSEMBLY
            foreach ($request->item_id_barang as $key => $value) {
                $satuan = mSatuan::where('id', $request->item_satuan[$key])->value('satuan');
                // dd($satuan);
                $dataItemAssembly = [
                    'id_assembly' => $idAssembly,
                    'id_barang' => $id_barang_array[$key],
                    'jumlah' => $qty_array[$key],
                    'id_satuan' => $satuan_array[$key],
                    'satuan' => $satuan
                ];
                $itemAssembly = mItemAssembly::create($dataItemAssembly);
//            dd($itemAssembly);
            }
            // END CREATE ITEM ASSEMBLY
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function edit_assembly($id)
    {
        $data = Main::data($this->breadcrumb, 'barang');
        $produkList = array();
        foreach ($this->produkList as $key => $value) {
            array_push($produkList, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['produkList'] = json_encode($produkList);

        $barang = mBarang::where('id', $id)->first();
        $assembly = mAssembly::where('id_barang', $id)->first();
        $itemAssembly = mItemAssembly::where('id_assembly', $assembly->id)->with('barang')->get();

        $data['barang'] = $barang;
        $data['assembly'] = $assembly;
        $data['itemAssembly'] = $itemAssembly;
        $data['golongan'] = mGolongan::all();
        return view('inventory/barang/barangEditAssembly', $data);
    }

    /**
     * Proses :
     *      1. Update Produk
     *      2. Kembalikan Stok ke Produk sesuai dengan itemAssembly sebelum terupdate
     *      3. Update Stok Produk
     *      4. Update Item Assembly
     *
     * @param Request $request
     * @param $id
     * @throws \Exception
     */
    function assembly_update(Request $request, $id)
    {
        $request->validate([
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'id_golongan' => 'required',
            'harga_beli_terakhir_barang' => 'required',
            'harga_jual_barang' => 'required|gt:0',
            'hpp' => 'required|gt:0',
            'total_item' => 'required|gt:0',
            'item_qty' => 'required',
            'item_qty.*' => 'required|gt:0',
        ]);

        DB::beginTransaction();
        try {
            // === DATA INPUT ====
            $kode_barang = $request->input('kode_barang');
            $nama_barang = $request->input('nama_barang');
            $id_golongan = $request->input('id_golongan');
            $harga_beli_terakhir_barang = $request->input('harga_beli_terakhir_barang');
            $harga_jual_barang = $request->input('harga_jual_barang');
            $hpp = $request->input('hpp');
            $total_item = $request->input('total_item');
            $kode_barang_array = $request->input('item_kode');
            $id_barang_array = $request->input('item_id_barang');
            $nama_array = $request->input('item_nama');
            $qty_array = $request->input('item_qty');
            $satuan_array = $request->input('item_satuan');


            // === PROCESS ===

            // START GET ASSEMBLY
            $idAssembly = mAssembly::where('id_barang', $id)->value('id');
            $getItemAssembly = mItemAssembly::where('id_assembly', $idAssembly)->get();
            $getStok = mStok::where('id_barang', $id)->get();
            // END GET ASSEMBLY


            // START MENGEMBALIKAN STOK
            $this->kurangStokAssembly($getItemAssembly, $getStok);
            // END MENGEMBALIKAN STOK


            // START UPDATE STOK PRODUK
            foreach ($getStok as $key => $stok) {
                mStok::where('id', $stok->id)->update(['jml_barang' => '0']);
            }
            // END UPDATE STOK PRODUK


            // START UPDATE PRODUK
            $dataProduk = [
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama_barang,
                'id_golongan' => $id_golongan,
                'harga_beli_terakhir_barang' => Main::string_to_number($harga_beli_terakhir_barang),
                'harga_jual_barang' => $harga_jual_barang,
                'hpp' => $hpp,
            ];
            mBarang::where('id', $id)->update($dataProduk);
            // END UPDATE PRODUK


            // START UPDATE ITEM ASSEMBLY
            foreach ($getItemAssembly as $key => $value) {
                $satuan = mSatuan::where('id', $satuan_array[$key])->value('satuan');
                // dd($satuan);
                $dataItemAssembly = [
                    'id_assembly' => $idAssembly,
                    'id_barang' => $id_barang_array[$key],
                    'jumlah' => $qty_array[$key],
                    'id_satuan' => $satuan_array[$key],
                    'satuan' => $satuan
                ];
                mItemAssembly::where('id', $value->id)->update($dataItemAssembly);
            }
            // END UPDATE ITEM ASSEMBLY
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    function delete($id)
    {
        $idAssembly = mAssembly::where('id_barang', $id)->values('id');
        $itemAssembly = mItemAssembly::where('id_assembly', $idAssembly)->get();
        $stokAssembly = mStok::where('id_barang', $id)->get();
        $this->kurangStokAssembly($itemAssembly, $stokAssembly);

        foreach ($itemAssembly as $key => $item) {
            mItemAssembly::where('id', $item->id)->delete();
        }

        foreach ($stokAssembly as $key => $stok) {
            mStok::where('id', $stok->id)->delete();
        }

        mAssembly::where('id', $idAssembly)->delete();
        mBarang::where('id', $id)->delete();
    }

    /**
     * Menambahkan jumlah stok
     * Proses ini hanya mengcover untuk pengurangan stok bahan dan pembuatan kartu stok keluar dari stok bahan
     *
     * @param array $itemAssembly
     * @param int $id_lokasi
     * @param int $jml_barang
     * @throws \Exception
     */
    function tambahStokAssembly($itemAssembly, $id_lokasi, $jml_barang)
    {
        DB::beginTransaction();
        try {
            $pemindahanController = new PemindahanStok();
            $namaLokasi = mLokasi::where('id', $id_lokasi)->values('lokasi');
            foreach ($itemAssembly as $key => $item) {
                $jmlStok = mStok::where([
                    ['id_barang', $item->id_barang],
                    ['id_lokasi', $id_lokasi]
                ])->value('jml_barang');
                $idStok = mStok::where([
                    ['id_barang', $item->id_barang],
                    ['id_lokasi', $id_lokasi],
                ])->value('id');

                $namaBarang = mBarang::where('id', $item->id_barang)->values('nama_barang');
                $totalDibutuhkan = $item->jumlah * $jml_barang;
                $hasilPengurangan = $jmlStok - $totalDibutuhkan;
                $updateStok = mStok::where('id', $idStok)->update(['jml_barang' => $hasilPengurangan]);
                $keterangan = 'Stok Keluar Assembly, Nama Barang : ' . $namaBarang . ' Pada Lokasi : ' . $namaLokasi;
                // dd($keterangan);

                $pemindahanController->kartuStok($id_lokasi, null, $item->id_barang, $idStok, null, $keterangan, $totalDibutuhkan, $hasilPengurangan, null, null, null, false, false, true);
            }
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    /**
     * Mengurangi jumlah stok
     * Proses ini hanya mengcover untuk penambahan stok bahan dan pembuatan kartu stok masuk dari stok bahan
     *
     * @param array $itemAssembly
     * @param array $stokAssembly
     */
    function kurangStokAssembly($itemAssembly, $stokAssembly)
    {
        $pemindahanController = new PemindahanStok();

        DB::beginTransaction();
        try {
            foreach ($stokAssembly as $keyStok => $stok) {
                $namaLokasi = mLokasi::where('id', $stok->id_lokasi)->value('lokasi');
                foreach ($itemAssembly as $keyAssembly => $item) {
                    $hasilPerkalian = $stok->jml_barang * $item->jumlah;
                    //                $getStok = mStok::where([
                    //                                        ['id_barang', $itemAssembly->id_barang],
                    //                                        ['id_lokasi', $stokAssembly->id_lokasi],
                    //                                    ])->first();
                    $namaBarang = mBarang::where('id', $item->id_barang)->value('nama_barang');
                    $hasilPenjumlahan = $stok->jml_barang + $hasilPerkalian;
                    $updateStok = mStok::where('id', $stok->id)->update(['jml_barang' => $hasilPenjumlahan]);
                    $keterangan = 'Stok Masuk Assembly, Nama Barang : ' . $namaBarang . ' Pada Lokasi : ' . $namaLokasi;

                    $pemindahanController->kartuStok(null, $stok->id_lokasi, $item->id_barang, null, $stok->id, $keterangan, $hasilPerkalian, null, $hasilPenjumlahan, null, null, false, true, false);
                }
            }
            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }
}
