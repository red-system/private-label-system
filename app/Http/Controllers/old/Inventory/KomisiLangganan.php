<?php

namespace app\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;


use app\Models\Bayu\mStok;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mGolongan;
use app\Models\Bayu\mKomisiLangganan;
use app\Models\Bayu\mLangganan;

class KomisiLangganan extends Controller
{
//    TODO: tambahkan lambang % dan Rp di komisi langganan list, dan jenis satuan nya
    private $breadcrumb;
    private $view = [
        'kode_langganan' => ["search_field" => "kode_langganan"],
        'langganan' => ["search_field" => "langganan"],
        'langganan_alamat' => ["search_field" => "langganan_alamat"],
        'langganan_kontak' => ["search_field" => "langganan_kontak"],
    ];
    private $komisiView = [
        'kode_barang' => ["search_field" => "kode_barang"],
        'nama_barang' => ["search_field" => "nama_barang"],
        'golongan' => ["search_field" => "golongan"],
        'minimal_beli_komisi_langganan' => ["search_field" => "minimal_beli_komisi_langganan"],
        'satuan' => ["search_field" => "satuan"],
        'tipe_komisi_langganan' => ["search_field" => "tipe_komisi_langganan"],
        'komisi_langganan' => ["search_field" => "komisi_langganan"],
    ];


    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_12'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value && $key != 'edit' && $key != 'delete') {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('inventory/komisiLangganan/komisiLanggananList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $langganan = new mLangganan();
        $result['iTotalRecords'] = $langganan->count_all();
        $result['iTotalDisplayRecords'] = $langganan->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $langganan->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            // $value->route['delete'] = route('stokOpnameDelete',['id'=>$value->id]);
            $value->route['komisi_langganan'] = route('komisiLanggananKomisiPage', ['id' => $value->id]);
            $value->direct['komisi_langganan'] = route('komisiLanggananKomisiPage', ['id' => $value->id]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function komisi(Request $request)
    {
        $data = Main::data($this->breadcrumb, 'komisi_langganan');
        $komisiView = array();
        foreach ($this->komisiView as $key => $value) {
            array_push($komisiView, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value && $key != 'komisi_langganan') {
                array_push($action, $key);
            }
        }

        $barang = new mBarang();

        $data['actionButton'] = json_encode($action);
        $data['komisiView'] = json_encode($komisiView);
        $data['id'] = $request->id;
        $data['barang'] = $barang->barangDanSatuan();
        return view('inventory/komisiLangganan/komisiLanggananKomisi', $data);
    }

    function komisiList(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $komisiLangganan = new mKomisiLangganan();
        $result['iTotalRecords'] = $komisiLangganan->count_all_by_langganan($request->id);
        $result['iTotalDisplayRecords'] = $komisiLangganan->count_filter_by_langganan($query, $this->komisiView, $request->id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $komisiLangganan->list_by_langganan($start, $length, $query, $this->komisiView, $request->id);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->route['delete'] = route('komisiLanggananDeleteKomisi', ['idLangganan' => $value->id_langganan, 'id' => $value->id]);
            $value->route['edit'] = route('komisiLanggananUpdateKomisi', ['idLangganan' => $value->id_langganan, 'id' => $value->id]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function insertKomisi(Request $request)
    {
        $request->validate([
            'id_barang' => 'required',
            'minimal_beli_komisi_langganan' => 'required',
            'tipe_komisi_langganan' => 'required',
            'komisi_langganan' => 'required',
        ]);

        $newKomisiLangganan = new mKomisiLangganan;
        $newKomisiLangganan->id_langganan = $request->id_langganan;
        $newKomisiLangganan->id_barang = $request->id_barang;
        $newKomisiLangganan->id_satuan = $request->id_satuan;
        $newKomisiLangganan->minimal_beli_komisi_langganan = $request->minimal_beli_komisi_langganan;
        $newKomisiLangganan->tipe_komisi_langganan = $request->tipe_komisi_langganan;
        $newKomisiLangganan->komisi_langganan = $request->komisi_langganan;
        $newKomisiLangganan->save();
    }

    function updateKomisi(Request $request)
    {
        $request->validate([
            'id_barang' => 'required',
            'minimal_beli_komisi_langganan' => 'required',
            'tipe_komisi_langganan' => 'required',
            'komisi_langganan' => 'required',
        ]);

        $updateKomisiLangganan = mKomisiLangganan::find($request->id);
        $updateKomisiLangganan->id_barang = $request->id_barang;
        $updateKomisiLangganan->id_satuan = $request->id_satuan;
        $updateKomisiLangganan->minimal_beli_komisi_langganan = $request->minimal_beli_komisi_langganan;
        $updateKomisiLangganan->tipe_komisi_langganan = $request->tipe_komisi_langganan;
        $updateKomisiLangganan->komisi_langganan = $request->komisi_langganan;
        $updateKomisiLangganan->save();
    }

    function deleteKomisi($id)
    {
        mKomisiLangganan::where('id', $id)->delete();
    }
}
