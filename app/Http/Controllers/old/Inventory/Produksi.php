<?php

namespace app\Http\Controllers\Inventory;

use app\Helpers\hProduk;
use app\Models\Mahendra\mKartuStok;
use app\Models\Mahendra\mProduksi;
use app\Models\Mahendra\mProduksiBaku;
use app\Models\Mahendra\mProduksiHasil;
use Barryvdh\DomPDF\Facade as PDF;
use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Nexmo\Response;
use Illuminate\Support\Facades\DB;

class Produksi extends Controller
{
    private $breadcrumb;
    private $cons;

    private $view = [
        'no_produksi_label' => ["search_field" => "tb_produksi.no_produksi_label"],
        'tgl_produksi' => ["search_field" => "tb_produksi.tgl_produksi"],
        'tanggal_expired' => ["search_field" => "tb_produksi.tanggal_expired"],
        'keterangan' => ["search_field" => "tb_produksi.keterangan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->cons = $cons;
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_6'],
                'route' => ''
            ],
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }

        $data['view'] = json_encode($view);
        $data['actionButton'] = json_encode($action);
        return view('inventory/produksi/produksiList', $data);
    }

    function create()
    {
        $data = Main::data($this->breadcrumb, $this->cons['inventory_6']);
        $urutan_produksi = $this->urutan_produksi();
        $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();
        $barang = mBarang::orderBy('nama_barang', 'ASC')->get();
        $satuan = mSatuan::orderBy('satuan', 'ASC')->get();

        $data = array_merge($data, [
            'urutan_produksi' => $urutan_produksi,
            'lokasi' => $lokasi,
            'barang' => $barang,
            'satuan' => $satuan
        ]);

        return view('inventory/produksi/produksiCreate', $data);
    }

    function edit($id)
    {
        $data = Main::data($this->breadcrumb, $this->cons['inventory_6']);
        $lokasi = mLokasi::orderBy('lokasi', 'ASC')->get();
        $barang = mBarang::orderBy('nama_barang', 'ASC')->get();
        $satuan = mSatuan::orderBy('satuan', 'ASC')->get();
        $id = Main::decrypt($id);
        $produksi = mProduksi::where('id', $id)->first();
        $produksi_baku = mProduksiBaku
            ::with([
                'stok',
                'stok.lokasi'
            ])
            ->where(['id_produksi' => $id])
            ->get();
        $produksi_hasil = mProduksiHasil
            ::with([
                'stok',
                'stok.lokasi'
            ])
            ->where(['id_produksi' => $id])
            ->get();

        $data = array_merge($data, [
            'lokasi' => $lokasi,
            'barang' => $barang,
            'satuan' => $satuan,
            'produksi' => $produksi,
            'produksi_baku' => $produksi_baku,
            'produksi_hasil' => $produksi_hasil
        ]);

        return view('inventory/produksi/produksiEdit', $data);
    }


    /**
     * Alur proses
     * 1. Menyimpan data produksi
     * 2. Mengurangsi stok barang dan menambah stok barang
     * 3. Memasukkan ke dalam stok opname
     *
     * @param Request $request
     * @throws \Exception
     */
    function insert(Request $request)
    {
        $request->validate([
            'tgl_produksi' => 'required',
            'tanggal_expired' => 'required',
            'id_barang_baku' => 'required|array',
            'id_stok_baku' => 'required|array',
            'qty_baku' => 'required|array',
            'id_barang_produksi' => 'required|array',
            'id_lokasi_produksi' => 'required|array',
            'qty_produksi' => 'required|array',
            'id_satuan_produksi' => 'required|array',
            'hpp_produksi' => 'required|array',
        ]);

        DB::beginTransaction();
        try {
            $no_produksi = $request->input('no_produksi');
            $no_produksi_label = $request->input('no_produksi_label');
            $tgl_produksi = $request->input('tgl_produksi');
            $tanggal_expired = $request->input('tanggal_expired');
            $keterangan = $request->input('keterangan');

            $nama_barang_baku_array = $request->input('nama_barang_baku');
            $harga_barang_baku_array = $request->input('harga_barang_baku');
            $nilai_bahan_baku_array = $request->input('nilai_bahan_baku');
            $id_barang_baku_array = $request->input('id_barang_baku');
            $id_stok_baku_array = $request->input('id_stok_baku');
            $qty_baku_array = $request->input('qty_baku');

            $nama_barang_produksi_array = $request->input('nama_barang_produksi');
            $id_barang_produksi_array = $request->input('id_barang_produksi');
            $id_lokasi_produksi_array = $request->input('id_lokasi_produksi');
            $id_satuan_produksi_array = $request->input('id_satuan_produksi');
            $qty_produksi_array = $request->input('qty_produksi');
            $hpp_produksi_array = $request->input('hpp_produksi');

            $data_barang_baku = [];
            $data_barang_hasil = [];
            $datetime = Main::datetime();

            $data_produksi = [
                'no_produksi' => $no_produksi,
                'no_produksi_label' => $no_produksi_label,
                'tgl_produksi' => Main::format_date_db($tgl_produksi),
                'tanggal_expired' => Main::format_date_db($tanggal_expired),
                'keterangan' => $keterangan
            ];

            $produksi = mProduksi::create($data_produksi);
            $id_produksi = $produksi->id;

            foreach ($nama_barang_baku_array as $key => $nama_barang_baku) {

                $id_barang = $id_barang_baku_array[$key];
                $id_stok = $id_stok_baku_array[$key];
                $harga_barang = $harga_barang_baku_array[$key];
                $qty = $qty_baku_array[$key];
                $nilai_bahan_baku = $nilai_bahan_baku_array[$key];

                $stok = mStok::where('id', $id_stok)->first(['jml_barang', 'id_lokasi', 'hpp']);
                $jml_barang = $stok->jml_barang;
                $id_lokasi = $stok->id_lokasi;
                $hpp = $stok->hpp;
                $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
                $stok_now = $jml_barang - $qty;

                $data_barang_baku[] = [
                    'id_produksi' => $id_produksi,
                    'id_barang' => $id_barang,
                    'id_stok' => $id_stok,
                    'id_lokasi' => $id_lokasi,
                    'nama_barang' => $nama_barang_baku,
                    'harga_barang' => $harga_barang,
                    'qty' => $qty,
                    'nilai_bahan_baku' => $nilai_bahan_baku,
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                ];

                /**
                 * Proses pengurangan/update stok terkini
                 */
                mStok
                    ::where('id', $id_stok)
                    ->update([
                        'jml_barang' => $stok_now
                    ]);

                /**
                 * Menambah data ke kartu stok keluar
                 */
                $data_kartu_stok = [
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_stok' => $id_stok,
                    'lokasi' => $lokasi,

                    /**
                     * Tanggal Produksi Salah Format
                     */
                    'tgl_transit' => Main::format_date_db($tgl_produksi),
                    'keterangan' => 'Proses pemakaian barang pada produksi ' . $no_produksi_label,
//                    'saldo_awal' => $jml_barang,
                    'hpp_awal' => $hpp,
                    'stok_keluar' => $qty,
                    'hpp_keluar' => $hpp,
//                    'saldo_terakhir' => $stok_now
                ];

                mKartuStok::create($data_kartu_stok);

            }

            foreach ($nama_barang_produksi_array as $key => $nama_barang_produksi) {

                $id_barang = $id_barang_produksi_array[$key];
                $id_lokasi = $id_lokasi_produksi_array[$key];
                $id_satuan = $id_satuan_produksi_array[$key];
                $qty = $qty_produksi_array[$key];
                $hpp = $hpp_produksi_array[$key];
                $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');

                $data_barang_hasil[] = [
                    'id_produksi' => $id_produksi,
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_satuan' => $id_satuan,
                    'nama_barang' => $nama_barang_produksi,
                    'qty' => $qty,
                    'hpp' => $hpp,
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                ];

                /**
                 * Proses penambahan stok setelah di produksi
                 */

                $check_stok = mStok
                    ::where([
                        'id_barang' => $id_barang,
                        'id_lokasi' => $id_lokasi
                    ])
                    ->count();
                $id_stok = '';
                $saldo_awal = 0;
                $hpp_awal = 0;
                $saldo_terakhir = 0;

                if ($check_stok == 0) {
                    $data_stok_insert = [
                        'id_barang' => $id_barang,
                        'id_lokasi' => $id_lokasi,
                        'id_satuan' => $id_satuan,
                        'jml_barang' => $qty,
                        'hpp' => $hpp
                    ];

                    $stok = mStok::create($data_stok_insert);
                    $id_stok = $stok->id;
                    $saldo_awal = $qty;
                    $hpp_awal = $hpp;
                    $saldo_terakhir = $qty;
                } else {
                    $stok = mStok
                        ::where([
                            'id_barang' => $id_barang,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->first(['hpp', 'jml_barang']);
                    $jml_barang_before = $stok->jml_barang;
                    $jml_barang_now = $jml_barang_before + $qty;
                    $saldo_awal = $jml_barang_before;
                    $hpp_awal = $stok->hpp;
                    $saldo_terakhir = $jml_barang_now;
                    mStok
                        ::where([
                            'id_barang' => $id_barang,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->update([
                            'jml_barang' => $jml_barang_now
                        ]);
                    $id_stok = mStok
                        ::where([
                            'id_barang' => $id_barang,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->value('id');
                }


                /**
                 * Proses pembuatan kartu stok masuk
                 */

                $data_kartu_stok = [
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_stok' => $id_stok,
                    'id_produksi' => $id_produksi,
                    'lokasi' => $lokasi,
                    'tgl_transit' => $tgl_produksi,
                    'keterangan' => 'Proses pemasukan barang pada produksi ' . $no_produksi_label,
//                    'saldo_awal' => $saldo_awal,
                    'hpp_awal' => $hpp_awal,
                    'stok_masuk' => $qty,
                    'hpp_masuk' => $hpp,
//                    'saldo_terakhir' => $saldo_terakhir
                ];

                mKartuStok::create($data_kartu_stok);

            }

            mProduksiBaku::insert($data_barang_baku);
            mProduksiHasil::insert($data_barang_hasil);

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    /**
     * Disini banyak ada kondisi
     *
     *
     *
     *
     * @param Request $request
     * @param $id_produksi
     * @throws \Exception
     */
    public function update(Request $request, $id_produksi = array())
    {
        $request->validate([
            'tgl_produksi' => 'required',
            'tanggal_expired' => 'required',
            'id_barang_baku' => 'required|array',
            'id_stok_baku' => 'required|array',
            'qty_baku' => 'required|array',
            'id_barang_produksi' => 'required|array',
            'id_lokasi_produksi' => 'required|array',
            'qty_produksi' => 'required|array',
            'id_satuan_produksi' => 'required|array',
            'hpp_produksi' => 'required|array',
        ]);

        DB::beginTransaction();
        try {

            $no_produksi = $request->input('no_produksi');
            $no_produksi_label = $request->input('no_produksi_label');
            $tgl_produksi = $request->input('tgl_produksi');
            $tanggal_expired = $request->input('tanggal_expired');
            $keterangan = $request->input('keterangan');

            $id_produksi_baku_array = $request->input('id_produksi_baku');
            $nama_barang_baku_array = $request->input('nama_barang_baku');
            $harga_barang_baku_array = $request->input('harga_barang_baku');
            $nilai_bahan_baku_array = $request->input('nilai_bahan_baku');
            $id_barang_baku_array = $request->input('id_barang_baku');
            $id_stok_baku_array = $request->input('id_stok_baku');
            $qty_baku_array = $request->input('qty_baku');

            $id_produksi_hasil_array = $request->input('id_produksi_hasil');
            $nama_barang_produksi_array = $request->input('nama_barang_produksi');
            $id_barang_produksi_array = $request->input('id_barang_produksi');
            $id_lokasi_produksi_array = $request->input('id_lokasi_produksi');
            $id_satuan_produksi_array = $request->input('id_satuan_produksi');
            $qty_produksi_array = $request->input('qty_produksi');
            $hpp_produksi_array = $request->input('hpp_produksi');

            $datetime = Main::datetime();

            $data_produksi = [
                'no_produksi' => $no_produksi,
                'no_produksi_label' => $no_produksi_label,
                'tgl_produksi' => Main::format_date_db($tgl_produksi),
                'tanggal_expired' => Main::format_date_db($tanggal_expired),
                'keterangan' => $keterangan
            ];

            mProduksi::where('id', $id_produksi)->update($data_produksi);

            $produksi_baku = mProduksiBaku::where('id_produksi', $id_produksi)->get();
            $produksi_hasil = mProduksiHasil::where('id_produksi', $id_produksi)->get();

            foreach ($produksi_baku as $row) {
                $jml_barang_before = mStok::where('id', $row->id_stok)->value('jml_barang');
                $jml_barang_now = $jml_barang_before + $row->qty;
                mStok
                    ::where('id', $row->id_stok)
                    ->update([
                        'jml_barang' => $jml_barang_now
                    ]);
            }

            foreach ($produksi_hasil as $row) {
                $jml_barang_before = mStok
                    ::where([
                        'id_barang' => $row->id_barang,
                        'id_lokasi' => $row->id_lokasi
                    ])
                    ->value('jml_barang');
                $jml_barang_now = $jml_barang_before - $row->qty;
                mStok
                    ::where([
                        'id_barang' => $row->id_barang,
                        'id_lokasi' => $row->id_lokasi
                    ])
                    ->update([
                        'jml_barang' => $jml_barang_now
                    ]);
            }

            mProduksiBaku::where('id_produksi', $id_produksi)->delete();
            mProduksiHasil::where('id_produksi', $id_produksi)->delete();


            foreach ($nama_barang_baku_array as $key => $nama_barang_baku) {

                $id_barang = $id_barang_baku_array[$key];
                $id_stok = $id_stok_baku_array[$key];
                $harga_barang = $harga_barang_baku_array[$key];
                $qty = $qty_baku_array[$key];
                $nilai_bahan_baku = $nilai_bahan_baku_array[$key];

                $stok = mStok::where('id', $id_stok)->first(['jml_barang', 'id_lokasi', 'hpp']);
                $jml_barang = $stok->jml_barang;
                $id_lokasi = $stok->id_lokasi;
                $hpp = $stok->hpp;
                $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');
                $stok_now = $jml_barang - $qty;

                $data_barang_baku[] = [
                    'id_produksi' => $id_produksi,
                    'id_barang' => $id_barang,
                    'id_stok' => $id_stok,
                    'id_lokasi' => $id_lokasi,
                    'nama_barang' => $nama_barang_baku,
                    'harga_barang' => $harga_barang,
                    'qty' => $qty,
                    'nilai_bahan_baku' => $nilai_bahan_baku,
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                ];

                /**
                 * Proses pengurangan/update stok terkini
                 */
                mStok
                    ::where('id', $id_stok)
                    ->update([
                        'jml_barang' => $stok_now
                    ]);

                /**
                 * Menambah data ke kartu stok keluar
                 */
                $data_kartu_stok = [
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_stok' => $id_stok,
                    'id_produksi' => $id_produksi,
                    'lokasi' => $lokasi,
                    'tgl_transit' => $tgl_produksi,
                    'keterangan' => 'Proses pemakaian barang pada produksi ' . $no_produksi_label,
//                    'saldo_awal' => $jml_barang,
                    'hpp_awal' => $hpp,
                    'stok_keluar' => $qty,
                    'hpp_keluar' => $hpp,
//                    'saldo_terakhir' => $stok_now
                ];

                mKartuStok::create($data_kartu_stok);

            }

            foreach ($nama_barang_produksi_array as $key => $nama_barang_produksi) {

                $id_barang = $id_barang_produksi_array[$key];
                $id_lokasi = $id_lokasi_produksi_array[$key];
                $id_satuan = $id_satuan_produksi_array[$key];
                $qty = $qty_produksi_array[$key];
                $hpp = $hpp_produksi_array[$key];
                $lokasi = mLokasi::where('id', $id_lokasi)->value('lokasi');

                $data_barang_hasil[] = [
                    'id_produksi' => $id_produksi,
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_satuan' => $id_satuan,
                    'nama_barang' => $nama_barang_produksi,
                    'qty' => $qty,
                    'hpp' => $hpp,
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                ];

                /**
                 * Proses penambahan stok setelah di produksi
                 */

                $check_stok = mStok
                    ::where([
                        'id_barang' => $id_barang,
                        'id_lokasi' => $id_lokasi
                    ])
                    ->count();

                if ($check_stok == 0) {
                    $data_stok_insert = [
                        'id_barang' => $id_barang,
                        'id_lokasi' => $id_lokasi,
                        'id_satuan' => $id_satuan,
                        'jml_barang' => $qty,
                        'hpp' => $hpp
                    ];

                    $stok = mStok::create($data_stok_insert);
                    $id_stok = $stok->id;
                    $saldo_awal = $qty;
                    $hpp_awal = $hpp;
                    $saldo_terakhir = $qty;
                } else {
                    $stok = mStok
                        ::where([
                            'id_barang' => $id_barang,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->first(['hpp', 'jml_barang']);
                    $jml_barang_before = $stok->jml_barang;
                    $jml_barang_now = $jml_barang_before + $qty;
                    $saldo_awal = $jml_barang_before;
                    $hpp_awal = $stok->hpp;
                    $saldo_terakhir = $jml_barang_now;
                    mStok
                        ::where([
                            'id_barang' => $id_barang,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->update([
                            'jml_barang' => $jml_barang_now
                        ]);
                    $id_stok = mStok
                        ::where([
                            'id_barang' => $id_barang,
                            'id_lokasi' => $id_lokasi
                        ])
                        ->value('id');
                }


                /**
                 * Proses pembuatan kartu stok masuk
                 */

                $data_kartu_stok = [
                    'id_barang' => $id_barang,
                    'id_lokasi' => $id_lokasi,
                    'id_stok' => $id_stok,
                    'id_produksi' => $id_produksi,
                    'lokasi' => $lokasi,
                    'tgl_transit' => $tgl_produksi,
                    'keterangan' => 'Proses pemasukan barang pada produksi ' . $no_produksi_label,
//                    'saldo_awal' => $saldo_awal,
                    'hpp_awal' => $hpp_awal,
                    'stok_masuk' => $qty,
                    'hpp_masuk' => $hpp,
//                    'saldo_terakhir' => $saldo_terakhir
                ];

                mKartuStok::create($data_kartu_stok);

            }

            mProduksiBaku::insert($data_barang_baku);
            mProduksiHasil::insert($data_barang_hasil);

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
    }

    public function delete($id_produksi)
    {
        $id_produksi = Main::decrypt($id_produksi);
        mProduksi::where('id', $id_produksi)->delete();

        $produksi_baku = mProduksiBaku::where('id_produksi', $id_produksi)->get();
        $produksi_hasil = mProduksiHasil::where('id_produksi', $id_produksi)->get();

        foreach ($produksi_baku as $row) {
            $jml_barang_before = mStok::where('id', $row->id_stok)->value('jml_barang');
            $jml_barang_now = $jml_barang_before + $row->qty;
            mStok
                ::where('id', $row->id_stok)
                ->update([
                    'jml_barang' => $jml_barang_now
                ]);
        }

        foreach ($produksi_hasil as $row) {
            $jml_barang_before = mStok
                ::where([
                    'id_barang' => $row->id_barang,
                    'id_lokasi' => $row->id_lokasi
                ])
                ->value('jml_barang');
            $jml_barang_now = $jml_barang_before - $row->qty;
            mStok
                ::where([
                    'id_barang' => $row->id_barang,
                    'id_lokasi' => $row->id_lokasi
                ])
                ->update([
                    'jml_barang' => $jml_barang_now
                ]);
        }

        mProduksiBaku::where('id_produksi', $id_produksi)->delete();
        mProduksiHasil::where('id_produksi', $id_produksi)->delete();
    }

    public function detail($id_produksi)
    {
        $id_produksi = Main::decrypt($id_produksi);
        $produksi = mProduksi
            ::where('id', $id_produksi)
            ->first();
        $produksi_baku = mProduksiBaku
            ::with([
                'stok',
                'stok.lokasi'
            ])
            ->where('id_produksi', $id_produksi)
            ->get();
        $produksi_hasil = mProduksiHasil
            ::with([
                'stok',
                'lokasi'
            ])
            ->where('id_produksi', $id_produksi)
            ->get();

        $data = [
            'produksi' => $produksi,
            'produksi_baku' => $produksi_baku,
            'produksi_hasil' => $produksi_hasil
        ];

        return view('inventory/produksi/produksiDetail', $data);
    }


    public function data(Request $request)
    {
        $id = $request->input('id_lokasi');
        return redirect()->route('stokPerlokasiPrint', ['id' => $id]);
    }

    public function print($id)
    {

        if ($id == 0) {
            $lokasi = 'Semua Lokasi';
            $stok = mStok::join('tb_barang', 'tb_barang.id', '=', 'tb_stok.id_barang')
                ->join('tb_supplier', 'tb_supplier.id', '=', 'tb_stok.id_supplier')
                ->get();
        } else {
            $mlokasi = mLokasi::where('id', $id)->first();
            $lokasi = $mlokasi->lokasi;
            $stok = mStok::join('tb_barang', 'tb_barang.id', '=', 'tb_stok.id_barang')
                ->join('tb_supplier', 'tb_supplier.id', '=', 'tb_stok.id_supplier')
                ->where('id_lokasi', $id)
                ->get();
        }

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadview('inventory.stokPerlokasi.print', compact('stok', 'lokasi'));
        return $pdf->stream('Data Stok di ' . $lokasi);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $data = new mProduksi;
        $result['iTotalRecords'] = $data->count_all();
        $result['iTotalDisplayRecords'] = $data->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $data->list_all($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $value->tgl_produksi = Main::format_date($value->tgl_produksi);
            $value->route['delete'] = route('produksiDelete', ['id' => Main::encrypt($value->id)]);
            $value->route['edit_direct'] = route('produksiEdit', ['id' => Main::encrypt($value->id)]);
            $value->route['detail_view'] = route('produksiDetail', ['id' => Main::encrypt($value->id)]);

            $no++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function no_seri_produk(Request $request)
    {
        $request->validate([
            'id_produk' => 'required',
            'id_lokasi' => 'required'
        ]);

        $month = date('m');
        $year = date('y');
        $id_produk = $request->id_produk;
        $id_lokasi = $request->id_lokasi;

        $no_seri_produk = hProduk::no_seri_produk($month, $year, $id_produk, $id_lokasi);

        return [
            'no_seri_produk' => $no_seri_produk
        ];
    }

    function urutan_produksi()
    {
        $last_urutan = mProduksi::orderBy('no_produksi', 'DESC')->value('no_produksi');
        $next_urutan = $last_urutan + 1;

        return $next_urutan;
    }

    function _barang_lokasi(Request $request)
    {
        $request->validate([
            'id_barang' => 'required'
        ]);

        $id_barang = $request->input('id_barang');
        $stok = \app\Models\Mahendra\mStok
            ::where('id_barang', $id_barang)
            ->leftJoin('tb_lokasi', 'tb_lokasi.id', '=', 'tb_stok.id_lokasi')
            ->leftJoin('tb_satuan', 'tb_satuan.id', '=', 'tb_stok.id_satuan')
            ->get([
                'tb_stok.id',
                'tb_lokasi.lokasi',
                'tb_satuan.satuan',
                'tb_stok.jml_barang'
            ]);
        return $stok;

    }
}
