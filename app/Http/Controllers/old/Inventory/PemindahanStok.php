<?php

namespace app\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
//use app\Http\Controllers\Inventory\PemindahanStok;
use Illuminate\Support\Facades\DB;
use DateTime;
use DateTimeZone;

use app\Models\Bayu\mPemindahanStok;
use app\Models\Bayu\mItemPemindahanStok;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use app\Models\Bayu\mStok;
use app\Models\Bayu\mKartuStok;
use app\Models\Bayu\mSatuan;
use app\Models\Bayu\mGolongan;

class PemindahanStok extends Controller
{
    private $breadcrumb;
    private $view = [
        'no_pemindahan' => ["search_field" => "tb_pemindahan.no_pemindahan"],
        'tgl_pemindahan' => ["search_field" => "tb_pemindahan.tgl_pemindahan"],
        'asal' => ["search_field" => "l1.lokasi"],
        'tujuan' => ["search_field" => "l2.lokasi"],
        'keterangan' => ["search_field" => "tb_pemindahan.keterangan"],
        'status' => ["search_field" => "tb_pemindahan.status"],
    ];
    private $produkList = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'golongan' => ["search_field" => "tb_golongan.golongan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_5'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        return view('inventory/pemindahanStok/pemindahanStokList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $pemindahan = new mPemindahanStok();
        $result['iTotalRecords'] = $pemindahan->count_all();
        $result['iTotalDisplayRecords'] = $pemindahan->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $pemindahan->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
            $value->tgl_pemindahan = date('d-m-Y', strtotime($value->tgl_pemindahan));
            if ($value->status != 'belum diterima') {
                $value->route['detail'] = route('pemindahanStokDetail', ['id' => $value->id]);
                $value->redirect['detail'] = route('pemindahanStokDetail', ['id' => $value->id]);
                $value->visibility['accept'] = 'none';
            } else {
                $value->route['accept'] = route('pemindahanStokAccept', ['id' => $value->id]);
                $value->redirect['accept'] = route('pemindahanStokAccept', ['id' => $value->id]);
                $value->visibility['detail'] = 'none';
            }

            $value->route['delete'] = route('pemindahanStokDelete', ['id' => $value->id]);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function tambah()
    {
        $pemindahan = new mPemindahanStok();
        $data = Main::data($this->breadcrumb, 'pemindahan_stok');
        $dataproduk = array();
        // array_push($dataproduk,array("data"=>"no"));
        foreach ($this->produkList as $key => $value) {
            array_push($dataproduk, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        session()->forget('id_location');
        $data['actionButton'] = json_encode($action);
        $data['produkList'] = json_encode($dataproduk);
        $data['lokasi'] = mLokasi::all();
        $data['satuan'] = mSatuan::all();
        $data['json_satuan'] = json_encode($data['satuan']);
        // $data['no_pemindahan'] = $pemindahan->noTerakhir() + 1;
        // dd($data);
        return view('inventory/pemindahanStok/pemindahanStokTambah', $data);
    }

    function session_change(Request $request)
    {
        session()->put('id_location', $request->input('lokasi_id'));
    }

    function tambahBarangAll(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok();

        $result['iTotalRecords'] = $stok->count_all();
        $result['iTotalDisplayRecords'] = $stok->count_filter($query, $this->produkList);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';

        $data = $stok->list_all($start, $length, $query, $this->produkList);

        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            $value->password = '';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
        // echo "<script>console.log(".session()->get('idLokasi').");</script>";
    }

    function insert(Request $request)
    {
        $request->validate([
            'no_pemindahan' => 'required',
            'tgl_pemindahan' => 'required',
            'asal_barang' => 'required',
            'tujuan_barang' => 'required',
        ]);

        $pemindahan = new mPemindahanStok();
        $lokasi = new mLokasi();

        DB::beginTransaction();
        try{
            $no_pemindahan = $request->input('no_pemindahan');
            $tgl_pemindahan = Main::format_date_db($request->input('tgl_pemindahan'));
            $asal_barang = $request->input('asal_barang');
            $tujuan_barang = $request->input('tujuan_barang');
            $total_item = $request->input('total_item');
            $item_id_stok = $request->input('item_id_stok');
            $item_qty = $request->input('item_qty');
            $item_satuan = $request->input('item_satuan');
            if ($request->keterangan == null || $request->keterangan == '') {
                $lokasiAsal = $lokasi->getNameById($request->asal_barang);
                $lokasiTujuan = $lokasi->getNameById($tujuan_barang);
                $request->keterangan = 'Pemindahan Barang Dari '.$lokasiAsal->lokasi.' Menuju '.$lokasiTujuan->lokasi;
            }
            $keterangan = $request->keterangan;
            $status = 'belum diterima';

            // START CREATE PEMINDAHAN STOK
            $dataPemindahan = [
                'no_pemindahan' => $no_pemindahan,
                'tgl_pemindahan' => $tgl_pemindahan,
                'asal_barang' => $asal_barang,
                'tujuan_barang' => $tujuan_barang,
                'keterangan' => $keterangan,
                'status' => $status,
            ];
            $id_pemindahan = mPemindahanStok::create($dataPemindahan)->id;
            // END CREATE PEMINDAHAN STOK

            // START CREATE ITEM PEMINDAHAN STOK
            foreach ($item_id_stok as $key => $value){
                $dataItemPemindahan = [
                    'id_pemindahan' => $id_pemindahan,
                    'id_stok' => $item_id_stok[$key],
                    'jml_barang' => $item_qty[$key],
                    'id_satuan' => $item_satuan[$key],
                ];
                $itemAssembly = mItemPemindahanStok::create($dataItemPemindahan);
            }
            // END CREATE ITEM PEMINDAHAN STOK

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }
        // mPemindahanStok::create($data);
        return redirect(route('pemindahanStokPage'));
    }

    function delete($id)
    {
        mItemPemindahanStok::where('id_pemindahan',$id)->delete();
        mPemindahanStok::where('id', $id)->delete();
    }

    function accept(Request $request)
    {
        $pemindahan = new mPemindahanStok();
        $itemPemindahan = new mItemPemindahanStok();
        $data = Main::data($this->breadcrumb, 'pemindahan_stok');
        $dataproduk = array();
        // array_push($dataproduk,array("data"=>"no"));
        foreach ($this->produkList as $key => $value) {
            array_push($dataproduk, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        session()->forget('id_location');
        $data['actionButton'] = json_encode($action);
        $data['produkList'] = json_encode($dataproduk);
        $data['lokasi'] = mLokasi::all();
        $data['satuan'] = mSatuan::all();
        $data['pemindahan'] = $pemindahan->list_by_pemindahan_stok($request->id);
        $data['pemindahan']->tgl_pemindahan = date('d-m-Y', strtotime($data['pemindahan']->tgl_pemindahan));
        $data['pemindahan']->nama_asal_barang = mLokasi::select('lokasi')->where('id',$data['pemindahan']->asal_barang)->first()->lokasi;
        $data['pemindahan']->nama_tujuan_barang = mLokasi::select('lokasi')->where('id',$data['pemindahan']->tujuan_barang)->first()->lokasi;
        $data['itemPemindahan'] = $itemPemindahan->getDataItemPemindahan($request->id);
//        dd($data['itemPemindahan']);
        $data['json_satuan'] = json_encode($data['satuan']);

        return view('inventory/pemindahanStok/pemindahanStokAccept', $data);
    }

    function insertAccept(Request $request)
    {
        $request->validate([
            'no_pemindahan' => 'required',
            'tgl_pemindahan' => 'required',
            'asal_barang' => 'required',
            'tujuan_barang' => 'required',
        ]);

        $stok = new mStok();
        $satuan = new mSatuan();
        $lokasi = new mLokasi();
        $pemindahanController = new PemindahanStok();

        DB::beginTransaction();
        try{
            $no_pemindahan = $request->input('no_pemindahan');
            $tgl_pemindahan = $request->input('tgl_pemindahan');
            $asal_barang = $request->input('asal_barang');
            $tujuan_barang = $request->input('tujuan_barang');
            $keterangan = $request->input('keterangan');
            $total_item = $request->input('totalItem');
            $item_id_stok = $request->input('item_id_stok');
            $item_id_barang = $request->input('item_id_barang');
            $item_id_supplier = $request->input('item_id_supplier');
            $item_qty = $request->input('item_qty');
            $item_satuan = $request->input('item_satuan');

            $updatePemindahan = mPemindahanStok::find($request->id);
            $updatePemindahan -> status = 'diterima';
            $updatePemindahan -> save();

            // ========= Start Bagian Tambah dan Kurang Stok ==========
            foreach($item_id_stok as $key => $value) {
                $itemIdStok = $item_id_stok[$key];
                $itemIdBarang = $item_id_barang[$key];
                $itemIdSupplier =$item_id_supplier[$key];
                $itemQty = $item_qty[$key];
                $itemSatuan = $item_satuan[$key];

                $hasilPengurangan = $stok->penguranganJml($itemIdStok,$itemQty)->pengurangan;
                $countDataPenjumlahan = $stok->countStokByBarangAndLocation($itemIdBarang,$tujuan_barang);

                if ($countDataPenjumlahan == 0) {

                    //===== Start Buat Data Baru, Karena Stok di Lokasi Tujuan Tidak Belum Pernah Terdapat Stok Tersebut =====
                    $new_stok = [
                        'id_barang' => $itemIdBarang,
                        'id_lokasi' => $tujuan_barang,
                        'id_satuan' => $itemSatuan,
                        'jml_barang' => $itemQty,
                        'satuan' => mSatuan::select('satuan')->where('id',$itemSatuan)->first()->satuan,
                        'hpp' => mStok::select('hpp')->where('id',$itemIdStok)->first()->hpp,
                        'id_supplier' => $itemIdSupplier
                    ];
                    $id_new_stok = mStok::create($new_stok)->id;

                    $hasilPenjumlahan = $itemQty;
                    //===== End Buat Data Baru, Karena Stok di Lokasi Tujuan Tidak Belum Pernah Terdapat Stok Tersebut =====

                    // Buat Kartu Stok dan Hitung HPP Stok di Stok Asal
                    $hasilHppBarang = $pemindahanController->kartuStok($asal_barang, $tujuan_barang, $itemIdBarang, $itemIdStok, $id_new_stok, $keterangan, $itemQty, $hasilPengurangan, $hasilPenjumlahan, $request->id, 'pemindahan', true, true, true);

                    // Update Stok Keluar
                    $updateStok = mStok::find($itemIdStok);
                    $updateStok -> jml_barang = $hasilPengurangan;
                    $updateStok -> save();

                    // Update Hpp Barang
                    $updateBarang = mBarang::find($itemIdBarang);
                    $updateBarang -> hpp = $hasilHppBarang;
                    $updateBarang -> save();

                } else {
                    // ===== Start Kartu Stok =====
                    $idStokKeluar = $itemIdStok;
                    $idStokMasuk = $stok->idTujuan($itemIdBarang, $tujuan_barang)->id;
                    $hasilPenjumlahan = $stok->penjumlahanJml($idStokMasuk, $itemQty)->penjumlahan;
                    $hasilHppBarang = $pemindahanController->kartuStok($asal_barang, $tujuan_barang, $itemIdBarang, $idStokKeluar, $idStokMasuk, $keterangan, $itemQty, $hasilPengurangan, $hasilPenjumlahan, $request->id, 'pemindahan', false, true, true);
                    // ===== End Kartu Stok =====


                    // ===== Start Stok Keluar =====
                    $dataStokKeluar = $stok->getDataById($idStokKeluar);

                    // Update Stok Keluar
                    $updateStokKeluar = mStok::find($idStokKeluar);
                    $updateStokKeluar -> jml_barang = $hasilPengurangan;
                    $updateStokKeluar -> save();
                    // ===== End Stok Keluar =====


                    // ===== Start Stok Masuk =====
                    $dataStokMasuk = $stok->getDataById($idStokMasuk);
                    $hasilHppStokMasuk = $pemindahanController->hitungHppStokBaru($dataStokMasuk->jml_barang, $dataStokMasuk->hpp, $itemQty, $dataStokKeluar->hpp, $hasilPenjumlahan);

                    // Update Stok Masuk
                    $updateStokMasuk = mStok::find($idStokMasuk);
                    $updateStokMasuk -> jml_barang = $hasilPenjumlahan;
                    $updateStokMasuk -> hpp = $hasilHppStokMasuk;
                    $updateStokMasuk -> save();
                    // ===== End Stok Masuk =====


                    // Update Hpp Barang
                    $updateBarang = mBarang::find($itemIdBarang);
                    $updateBarang -> hpp = $hasilHppBarang;
                    $updateBarang -> save();

                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

        return redirect(route('pemindahanStokPage'));
    }
    // ========= End Bagian Tambah dan Kurang Stok ==========

    // ========= Start Bagian Kartu Stok ==========

    public function kartuStok($lokasiStokKeluar, $lokasiStokMasuk, $idBarang, $idStokKeluar, $idStokMasuk, $keterangan, $qty, $hasilPengurangan, $hasilPenjumlahan, $idAksi, $aksi, $isNew, $stokMasuk, $stokKeluar){
        // set Tanggal
        $tz = 'Asia/Hong_Kong';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp

        // Get Required Data
        $stok = new mStok();
        $barang = new mBarang();
        $lokasi = new mLokasi();
        $dataBarang = $barang->getDataById($idBarang);

        DB::beginTransaction();
        try {
            if ($stokKeluar) {

                // ======= Start Stok Keluar ========
                $dataStokKeluar = $stok->getDataById($idStokKeluar);

                $newKartuStokKeluar = new mKartuStok;
                $newKartuStokKeluar -> id_barang = $idBarang;
                $newKartuStokKeluar -> id_lokasi = $lokasiStokKeluar;
                $newKartuStokKeluar -> id_stok = $idStokKeluar;
                $newKartuStokKeluar -> lokasi = $lokasi->getNameById($lokasiStokKeluar)->lokasi;
                $newKartuStokKeluar -> tgl_transit = $dt->format('Y-m-d');
                $newKartuStokKeluar -> keterangan = $keterangan;
                $newKartuStokKeluar -> stok_awal = $dataStokKeluar->jml_barang;
                $newKartuStokKeluar -> hpp_awal = $dataBarang->hpp;
                $newKartuStokKeluar -> stok_keluar = $qty;
                $newKartuStokKeluar -> hpp_keluar = $dataStokKeluar->hpp;
                $newKartuStokKeluar -> stok_akhir = $hasilPengurangan;
                $newKartuStokKeluar -> sst_sprl = null;
                $newKartuStokKeluar -> hpp_stok = $dataBarang->hpp;
                if ($aksi == 'pemindahan') {
                    $newKartuStokKeluar -> id_pemindahan = $idAksi;
                } else if($aksi == 'pemakaian'){
                    $newKartuStokKeluar -> id_pemakaian = $idAksi;
                } else if($aksi == 'stokOpname'){
                    $newKartuStokKeluar -> id_stokOpname = $idAksi;
                }
                $newKartuStokKeluar -> save();
                // ======= End Stok Keluar ========

            }


            if ($stokMasuk) {

                // ======= Start Stok Masuk ========
                $dataStokMasuk = $stok->getDataById($idStokMasuk);

                if ($isNew) {
                    $stok_awal = 0;
                } else {
                    $stok_awal = $dataStokMasuk->jml_barang;
                }

                $hasilHppStok = (($dataBarang->hpp*$stok_awal)+($qty*$dataStokMasuk->hpp))/($stok_awal+$qty);

                $newKartuStokMasuk = new mKartuStok;
                $newKartuStokMasuk -> id_barang = $idBarang;
                $newKartuStokMasuk -> id_lokasi = $lokasiStokMasuk;
                $newKartuStokMasuk -> id_stok = $idStokMasuk;
                $newKartuStokMasuk -> lokasi = $lokasi->getNameById($lokasiStokMasuk)->lokasi;
                $newKartuStokMasuk -> tgl_transit = $dt->format('Y-m-d');
                $newKartuStokMasuk -> keterangan = $keterangan;
                $newKartuStokMasuk -> stok_awal = $stok_awal;
                $newKartuStokMasuk -> hpp_awal = $dataBarang->hpp;
                $newKartuStokMasuk -> stok_masuk = $qty;
                $newKartuStokMasuk -> hpp_masuk = $dataStokMasuk->hpp;
                $newKartuStokMasuk -> stok_akhir = $hasilPenjumlahan;
                $newKartuStokMasuk -> sst_sprl = null;
                $newKartuStokMasuk -> hpp_stok = $hasilHppStok;
                if ($aksi == 'pemindahan') {
                    $newKartuStokMasuk->id_pemindahan = $idAksi;;
                } else if ($aksi == 'pemakaian') {
                    $newKartuStokMasuk->id_pemakaian = $idAksi;
                } else if ($aksi == 'stokOpname') {
                    $newKartuStokMasuk->id_stokOpname = $idAksi;
                }
                $newKartuStokMasuk -> save();
                // ======= End Stok Masuk ========

            }

            DB::commit();

            if (isset($hasilHppStok)){
                return $hasilHppStok ? $hasilHppStok : 0;
            }
        } catch (\Exception $exception) {
            throw $exception;

            DB::rollBack();
        }

    }

    // Perhitungan HPP Barang (Hpp_stok pada field) yang baru
    // Parameter :
    //          1. jmlBarang        : Jumlah barang sebelum ditambahkan/dikurangi dari table stok
    //          2. hppBarang        : HPP barang sebelum ditambahkan/dikurangi dari table stok
    //          3. jmlBarangPindah  : Jumlah barang yang pindah
    //          4. hppBarangPindah  : HPP stok dari barang yang pindah, sebelum ditambahkan/dikurangi
    //          5. totalStokBaru    : Total keseluruhan stok yang baru setelah ditambahkan/dikurangi
    function hitungHppStokBaru($jmlBarang, $hppBarang, $jmlBarangPindah, $hppBarangPindah, $totalStokBaru)
    {
        $hppAsal = $jmlBarang * $hppBarang;
        $hppBaru = $jmlBarangPindah * $hppBarangPindah;
        $jumlahHpp = $hppAsal + $hppBaru;
        $hppAkhir = $jumlahHpp / $totalStokBaru;
        return $hppAkhir;
    }

    // ========= End Bagian Kartu Stok ==========

    function declining(Request $request)
    {
        $updatePemindahan = mPemindahanStok::find($request->id);
        $updatePemindahan->status = 'ditolak';
        $updatePemindahan->save();

        return redirect(route('pemindahanStokPage'));
    }

}
