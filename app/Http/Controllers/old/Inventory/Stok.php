<?php

namespace app\Http\Controllers\Inventory;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mKartuStok;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use app\Models\Widi\mStok;
use app\Models\Widi\mSupplier;
use Illuminate\Http\Request;

use app\Models\Bayu\mAssembly;
use app\Models\Bayu\mItemAssembly;

use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use app\Http\Controllers\Inventory\PemindahanStok;
use app\Http\Controllers\Inventory\Assembly;

class Stok extends Controller
{
    private $breadcrumb;
    private $cons;

    private $detail = [
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'wilayah' => ["search_field" => "tb_wilayah.wilayah"],
        'jml_barang' => ["search_field" => "tb_stok.jml_barang"],
        'satuan' => ["search_field" => "tb_stok.satuan"],
        'supplier' => ["search_field" => "tb_supplier.supplier"],
        'date_expired' => ["search_field" => "tb_stok.date_expired"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_1'],
                'route' => ''
            ]
            ,
            [
                'label' => $cons['inventory_2'],
                'route' => ''
            ]
        ];
    }

    function index($id)
    {
        $data = Main::data($this->breadcrumb, 'barang');
        $detail = array();
        $produk = mBarang::where('id', $id)->first();
        $satuan = mSatuan::all();
        $lokasi = mLokasi::all();
        $supplier = mSupplier::all();
        array_push($detail, array("data" => "no"));
        foreach ($this->detail as $key => $value) {
            array_push($detail, array("data" => $key));
        }

        $data['detail'] = json_encode($detail);
        $data['id'] = $id;
        $data['produk'] = $produk;
        $data['satuan'] = $satuan;
        $data['lokasi'] = $lokasi;
        $data['supplier'] = $supplier;


        return view('inventory/stok/stokList', $data);
    }

    function insert(Request $request, $id)
    {
        $request->validate([
            'id_lokasi' => 'required',
            'jml_barang' => 'required',
            'id_satuan' => 'required',
            'hpp' => 'required',
            'id_supplier' => 'required',
            'date_expired' => 'required'
        ]);


        $pemindahanController = new PemindahanStok();

        // check jika barang merupakan barang assembly
        $countAssembly = mAssembly::where('id_barang', $id)->count();
        if ($countAssembly > 0) {
            $idAssembly = mAssembly::where('id_barang', $id)->value('id');
            $itemAssembly = mItemAssembly::where('id_assembly', $idAssembly)->get();
            $itemDiffer = array();
            foreach ($itemAssembly as $key => $item) {
                // check apakah stok barang mencukupi
                $totalDibutuhkan = $item->jumlah * Main::string_to_number($request->jml_barang);
                $jmlStok = mStok::where([
                    ['id_barang', $item->id_barang],
                    ['id_lokasi', $request->id_lokasi]
                ])->value('jml_barang');
                if ($jmlStok < $totalDibutuhkan) {
                    array_push($itemDiffer, 'break');
                } else {
                    array_push($itemDiffer, 'run');
                }
            }

            // CHECK JIKA ADA BREAK DI DALAM ARRAY ATAU TIDAK,
            // JIKA TIDAK MAKA JALANKAN PENGURANGAN JUMLAH STOK ITEM ASSEMBLY
            if (in_array('break', $itemDiffer)) {
                return response()->json([
                    'message' => 'Stok Produk <strong>Tidak Mencukupi</strong>'
                ], 422);
            } else {
                $assembly = new Assembly();
                $assembly->tambahStokAssembly($itemAssembly, $request->id_lokasi, Main::string_to_number($request->jml_barang));
            }
        }

        // START PERHITUNGAN HPP
        $id_lokasi = $request->input('id_lokasi');
        $id_satuan = $request->input('id_satuan');
        $satuan = mSatuan::where('id', $id_satuan)->first();
        $stok = mStok::where('id_barang', $id)->get();
        $totalstok = 0;
        $barang = mBarang::where('id', $id)->first();

        /**
         * pertama mencari jumlah keseluruhan stok barang yang akan disimpan
         */
        foreach ($stok as $item) {
            $totalstok += $item->jml_barang;
        }

        /**
         * kemudian hitung hpp stok tersebut sebagai hpp lama
         */
        $hpp_lama = $totalstok * $barang->hpp;

        /**
         * kali kan jumlah stok barang yang baru insert dengan hpp nya sebagai hpp brang baru
         */
        $hpp_baru = Main::string_to_number($request->jml_barang) * Main::string_to_number($request->hpp);

        /**
         * jumlahkan total stok lama dan baru sebagai total keseluruhan stok
         */
        $total_keseluruhan_stok = $totalstok + Main::string_to_number($request->jml_barang);

        /**
         * jumlahkan hpp lama dengan hpp baru untuk mendapat hpp gabungan
         */
        $hpp_gabung = $hpp_lama + $hpp_baru;

        /**
         * cari hpp rata-rata dengan cara membagi hpp gabung dengan jumlah kesluruhan stok
         */
        $hpp_rata_rata = $hpp_gabung / $total_keseluruhan_stok;
        // END PERHITUNGAN HPP

        mBarang::where('id', $id)->update([
            'hpp' => $hpp_rata_rata,
        ]);

        $request['id_barang'] = $id;
        $lokasi = mLokasi::where('id', $request->input('id_lokasi'))->first();
        $id_supplier = $request->input('id_supplier');
        $date = $request->input('date_expired');
        $date_expired = Main::format_date_db($date);
        $jml_barang = Main::string_to_number($request->input('jml_barang'));
        $hpp = Main::string_to_number($request->input('hpp'));
        $count = DB::table('tb_stok')
            ->where('id_lokasi', $id_lokasi)
            ->where('id_barang', $id)
            ->count();
        $id_barang = $id;
        $data = [
            'id_barang' => $id_barang,
            'id_lokasi' => $id_lokasi,
            'id_satuan' => $id_satuan,
            'jml_barang' => $jml_barang,
            'satuan' => $satuan->satuan,
            'hpp' => $hpp,
            'id_supplier' => $id_supplier,
            'date_expired' => $date_expired
        ];
        if ($count > 0) {
            $stok_ada = mStok::where('id_lokasi', $id_lokasi)
                ->where('id_barang', $id)
                ->first();
            // dd($stok_ada);

            /**
             * proses mencari nilai hpp stok
             * pertama cari nilai hpp stok yang ada/lama
             */
            $hpp_stok_yang_ada = $stok_ada->jml_barang * $stok_ada->hpp;

            /**
             * kemudian kalikan jumlah stok baru dengan hpp baru
             */
            $hpp_stok_baru = $jml_barang * $hpp;

            /**
             * jumlahkan hpp stok awal dengan hpp stok baru
             */
            $hpp_gabungan = $hpp_stok_yang_ada + $hpp_stok_baru;

            /**
             * jumlahkan stok awal dengan stok baru
             */
            $stok_gabungan = $stok_ada->jml_barang + $jml_barang;

            /**
             * bagi hpp gabungan dengan stok gabungan untuk mendapatkan hpp terbaru
             */
            $hpp_terbaru = $hpp_gabungan / $stok_gabungan;

            $stoks = mStok::where('id_lokasi', $stok_ada->id_lokasi)->where('id_barang', $id)->update([
                'jml_barang' => $stok_gabungan,
                'hpp' => $hpp_terbaru,
                'id_supplier' => $id_supplier, 'date_expired' => $date_expired]);
            $stok_awal = $stok_gabungan - $jml_barang;
            mKartuStok::create([
                'id_barang' => $id,
                'id_lokasi' => $lokasi->id,
                'id_stok' => $stok_ada->id,
                'lokasi' => $lokasi->lokasi,
                'tgl_transit' => Carbon::now(),
                'keterangan' => 'Stok Masuk',
                'stok_awal' => $stok_awal,
                'hpp_awal' => $barang->hpp,
                'stok_masuk' => $jml_barang,
                'hpp_masuk' => $hpp,
                'stok_akhir' => $stok_gabungan,
                'hpp_stok' => $hpp_rata_rata
            ]);
        } else {
            $newStok = mStok::create($data);

            mKartuStok::create([
                'id_barang' => $id,
                'id_lokasi' => $lokasi->id,
                'id_stok' => $newStok->id,
                'lokasi' => $lokasi->lokasi,
                'tgl_transit' => Carbon::now(),
                'keterangan' => 'Stok Masuk',
                'stok_awal' => 0,
                'hpp_awal' => $barang->hpp,
                'stok_masuk' => $jml_barang,
                'hpp_masuk' => $hpp,
                'stok_akhir' => $newStok->jml_barang,
                'hpp_stok' => $hpp_rata_rata
            ]);
        }
        return redirect()->route('stokPage', $id);
    }

    function list(Request $request, $id)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $stok = new mStok;
        $result['iTotalRecords'] = $stok->count_all_by_product($id);
        $result['iTotalDisplayRecords'] = $stok->count_filter_by_product($id, $query, $this->detail);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $stok->list_by_product($id, $start, $length, $query, $this->detail);
        $no = $start + 1;
        foreach ($data as $value) {
            $tanggal = $value->date_expired;
            $date = Main::format_date($value->date_expired);
            if ($tanggal == null) {
                $value->date_expired = $tanggal;
            } else {
                $value->date_expired = $date;
            }
            $value->no = $no;
            $no++;
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }
}
