<?php

namespace app\Http\Controllers\Inventory;

use app\Models\Widi\mStok;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;

use app\Models\Widi\mBarang;
use app\Models\Widi\mGolongan;


use app\Models\Bayu\mAssembly;
use app\Models\Bayu\mItemAssembly;
use Milon\Barcode\DNS1D;
use Milon\Barcode\Facades\DNS1DFacade;
use Dompdf\Options as Options;

class Barang extends Controller
{
    private $breadcrumb;
    private $view = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'golongan' => ["search_field" => "tb_golongan.golongan"],
        'hpp' => ["search_field" => "tb_barang.hpp"],
        'harga_beli_terakhir_barang' => ["search_field" => "tb_barang.harga_beli_terakhir_barang"],
        'harga_jual_barang' => ["search_field" => "tb_barang.harga_jual_barang"],
        'minimal_stok' => ["search_field" => "tb_barang.minimal_stok"],
        'total_stok' => ["search_field" => "perhitungan.total_stok"],
    ];
    private $produkList = [
        'kode_barang' => ["search_field" => "tb_barang.kode_barang"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'golongan' => ["search_field" => "tb_golongan.golongan"],
    ];

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $golongan = mGolongan::all();
        $data = Main::data($this->breadcrumb);
        $view = array();
        array_push($view, array("data" => "no"));
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $action = array();
        foreach ($data['menuAction'] as $key => $value) {
            if ($value) {
                array_push($action, $key);
            }
        }
        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);
        $data['golongan'] = $golongan;

        return view('inventory/barang/barangList', $data);
    }

    function list(Request $request)
    {
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $barang = new mBarang();
        $result['iTotalRecords'] = $barang->count_all();
        $result['iTotalDisplayRecords'] = $barang->count_filter($query, $this->view);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $barang->list($start, $length, $query, $this->view);
        $no = $start + 1;
        foreach ($data as $value) {
            $value->no = $no;
            $no++;
            // CHECK APAKAH BARANG MERUPAKAN BARANG ASSEMBLY
            // JIKA YA, MAKA BEDAKAN PROSES TOMBOL" YANG ADA
            $countAssembly = mAssembly::where('id_barang', $value->id)->count();
            if ($countAssembly > 0) {
                $value->route['delete'] = route('assemblyDelete', ['id' => $value->id]);
                $value->route['edit_assembly'] = route('assemblyEdit', ['id' => $value->id]);
                $value->direct['edit_assembly'] = route('assemblyEdit', ['id' => $value->id]);
                $value->check['edit_assembly'] = $value->total_stok;
                $value->ignore['edit_assembly'] = true;
                $value->visibility['edit'] = 'none';
                $value->label['detail'] = 'Stok';
                $value->route['detail'] = route('stokPage', ['id' => $value->id]);
                $value->direct['detail'] = route('stokPage', ['id' => $value->id]);
                $value->ignore['detail'] = true;
            } else {
                $value->route['delete'] = route('produkDelete', ['id' => $value->id]);
                $value->route['edit'] = route('produkUpdate', ['id' => $value->id]);
                $value->visibility['edit_assembly'] = 'none';
                $value->label['detail'] = 'Stok';
                $value->route['detail'] = route('stokPage', ['id' => $value->id]);
                $value->direct['detail'] = route('stokPage', ['id' => $value->id]);
                $value->ignore['detail'] = true;
            }
            $value->route['barcode'] = route('barcodePage', ['id' => $value->id]);
            $value->target_blank['barcode'] = 'ada';
            $value->ignore['barcode'] = true;
            $value->hpp = Main::format_money($value->hpp);
            $value->harga_beli_terakhir_barang = Main::format_money($value->harga_beli_terakhir_barang);
            $value->harga_jual_barang = Main::format_money($value->harga_jual_barang);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function insert(Request $request)
    {
        $request->validate([
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'id_golongan' => 'required',
            'hpp' => 'required',
            'harga_beli_terakhir_barang' => 'required',
            'harga_jual_barang' => 'required',

        ]);
        $kode_barang = $request->input('kode_barang');
        $nama_barang = $request->input('nama_barang');
        $id_golongan = $request->input('id_golongan');
        $hpp = Main::string_to_number($request->input('hpp'));
        $harga_beli_terakhir_barang = Main::string_to_number($request->input('harga_beli_terakhir_barang'));
        $harga_jual_barang = Main::string_to_number($request->input('harga_jual_barang'));
        $minimal_stok = Main::string_to_number($request->input('minimal_stok'));

        $data = [
            'kode_barang' => $kode_barang,
            'nama_barang' => $nama_barang,
            'id_golongan' => $id_golongan,
            'hpp' => $hpp,
            'harga_beli_terakhir_barang' => $harga_beli_terakhir_barang,
            'harga_jual_barang' => $harga_jual_barang,
            'minimal_stok' => $minimal_stok
        ];
        mBarang::create($data);
    }

    function barcode($id){
        $barang =  mBarang::find($id);
        $barcode = new DNS1D();
        $print = $barcode->getBarcodeHTML($barang->kode_barang, 'MSI+', 1.5, 40);
        $path = $barcode->getBarcodeSVG($barang->kode_barang, 'MSI+', 1.5, 40);

        $data = [
            'barang' => $barang,
            'barcode' => $print,
            'path' => $path
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('inventory/barang/barcode', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Barcode ' . $barang->nama_barang);
//        return view('inventory/barang/barcode', $data);

    }

    function delete($id)
    {
        // CHECK JIKA BARANG MASIH MEMILIKI STOK
        /**
         * Fixing Hapus Stok
         **/
        $getStok = mStok::where('id_barang', $id);
        $countStok = count($getStok->get());
        if ($countStok > 0) {
            $getStok->delete();
        }

        mBarang::where('id', $id)->delete();
    }


    function update(Request $request, $id)
    {
        $request->validate([
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'id_golongan' => 'required',
            'hpp' => 'required',
            'harga_beli_terakhir_barang' => 'required',
            'harga_jual_barang' => 'required',
        ]);

        $kode_barang = $request->input('kode_barang');
        $nama_barang = $request->input('nama_barang');
        $id_golongan = $request->input('id_golongan');
        $hpp = Main::string_to_number($request->input('hpp'));
        $harga_beli_terakhir_barang = Main::string_to_number($request->input('harga_beli_terakhir_barang'));
        $harga_jual_barang = Main::string_to_number($request->input('harga_jual_barang'));
        $minimal_stok = Main::string_to_number($request->input('minimal_stok'));

        $data = [
            'kode_barang' => $kode_barang,
            'nama_barang' => $nama_barang,
            'id_golongan' => $id_golongan,
            'hpp' => $hpp,
            'harga_beli_terakhir_barang' => $harga_beli_terakhir_barang,
            'harga_jual_barang' => $harga_jual_barang,
            'minimal_stok' => $minimal_stok
        ];

        mBarang::where(['id' => $id])->update($data);
    }


}
