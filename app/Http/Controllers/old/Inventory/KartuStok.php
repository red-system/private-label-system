<?php

namespace app\Http\Controllers\Inventory;

use app\Helpers\Main;
use app\Models\Widi\mKartuStok;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Barryvdh\DomPDF\Facade as PDF;

class KartuStok extends Controller
{
    private $breadcrumb;
    private $view = [
        'lokasi' => ["search_field" => "tb_lokasi.lokasi"],
        'nama_barang' => ["search_field" => "tb_barang.nama_barang"],
        'tgl_transit' => ["search_field" => "tb_kartu_stok.tgl_transit"],
        'keterangan' => ["search_field" => "tb_kartu_stok.keterangan"],
        'stok_awal' => ["search_field" => "tb_kartu_stok.stok_awal"],
        'hpp_awal' => ["search_field" => "tb_kartu_stok.hpp_awal"],
        'stok_masuk' => ["search_field" => "tb_kartu_stok.stok_masuk"],
        'hpp_masuk' => ["search_field" => "tb_kartu_stok.hpp_masuk"],
        'stok_keluar' => ["search_field" => "tb_kartu_stok.stok_keluar"],
        'hpp_keluar' => ["search_field" => "tb_kartu_stok.hpp_keluar"],
        'stok_akhir' => ["search_field" => "tb_kartu_stok.stok_akhir"],
        'hpp_stok' => ["search_field" => "tb_kartu_stok.hpp_stok"],
        'sst_sprl' => ["search_field" => "tb_kartu_stok.sst_sprl"],
    ];
    private $cons;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->breadcrumb = [
            [
                'label' => $cons['inventory'],
                'route' => ''
            ],
            [
                'label' => $cons['inventory_4'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
        $all_request = $request->input();
        if (empty($all_request)) {
            $data['url'] = null;
        } else {
            foreach ($all_request as $key => $value) {
                if ($key == 'date_start') {
                    $data['url'] = '?' . $key . '=' . $value;
                } else {
                    $data['url'] .= '&' . $key . '=' . $value;

                }
            }
        }

        $lokasi = mLokasi::orderBy('lokasi', 'ASC');
        $lokasi = $lokasi->get();
        $barang = mBarang::orderBy('kode_barang', 'ASC');
        $barang = $barang->get();
        $id_lokasi = $request->input('id_lokasi');
        $id_barang = $request->input('id_barang');
        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_barang = $id_barang ? $id_barang : 'all';
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start = $date_start ? $date_start : mKartuStok::orderBy('tgl_transit', 'ASC')->value('tgl_transit');
        $date_start = Main::format_date($date_start);
        $date_end = $date_end ? $date_end : mKartuStok::orderBy('tgl_transit', 'DESC')->value('tgl_transit');
        $date_end = Main::format_date($date_end);
        $params = [
            'id_lokasi' => $id_lokasi,
            'id_barang' => $id_barang,
            'date_start' => $date_start,
            'date_end' => $date_end
        ];

        $data['tab'] = 'trasfer';
        $data['lokasi'] = $lokasi;
        $data['barang'] = $barang;
        $data['id_lokasi'] = $id_lokasi;
        $data['id_barang'] = $id_barang;
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;
        $data['params'] = $params;
        $data['view'] = json_encode($view);
        return view('inventory/kartuStok/kartuStokList', $data);
    }

    function list(Request $request)
    {
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $id_lokasi = $request->input('id_lokasi');
        $id_barang = $request->input('id_barang');

//        dd($id_barang);
        $date_start = $date_start ? $date_start : mKartuStok::orderBy('tgl_transit', 'ASC')->value('tgl_transit');
        $date_start_db = Main::format_date_db($date_start);

        $date_end = $date_end ? $date_end : mKartuStok::orderBy('tgl_transit', 'DESC')->value('tgl_transit');
        $date_end_db = Main::format_date_db($date_end);

        $id_lokasi = $id_lokasi ? $id_lokasi : 'all';
        $id_barang = $id_barang ? $id_barang : 'all';
//        dd($id_lokasi);

        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $package = new mKartuStok();
        $result['iTotalRecords'] = $package->count_all();
        $result['iTotalDisplayRecords'] = $package->count_filter($query, $this->view, $id_barang, $id_lokasi, $date_start_db, $date_end_db);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $package->list($start, $length, $query, $this->view, $id_barang, $id_lokasi, $date_start_db, $date_end_db);
//        dd($data);
        foreach ($data as $value) {
            $value->tgl_transit = Main::format_date($value->tgl_transit);
            $value->stok_awal = Main::format_number_system($value->stok_awal);
            $value->hpp_awal = Main::format_number_system($value->hpp_awal);
            if ($value->stok_masuk != null) {
                $value->stok_masuk = Main::format_number_system($value->stok_masuk);
                $value->hpp_masuk = Main::format_number_system($value->hpp_masuk);
            };
            if ($value->stok_keluar != null) {
                $value->stok_keluar = Main::format_number_system($value->stok_keluar);
                $value->hpp_keluar = Main::format_number_system($value->hpp_keluar);
            }
            $value->stok_akhir = Main::format_number_system($value->stok_akhir);
            $value->hpp_stok = Main::format_number_system($value->hpp_stok);
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function pdf(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_barang = $request->input('id_barang');
        $id_lokasi = $request->input('id_lokasi');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start_db = Main::format_date_db($date_start);
        $date_end_db = Main::format_date_db($date_end);

        $list = mKartuStok
            ::select([
                'tb_kartu_stok.*',
                'tb_barang.kode_barang',
                'tb_barang.nama_barang',
                'tb_lokasi.lokasi',
            ])
            ->leftJoin('tb_barang', 'tb_kartu_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'tb_kartu_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->whereBetween('tgl_transit', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_kartu_stok.tgl_transit', 'ASC');

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_kartu_stok.lokasi', $id_lokasi);
            $lokasi = mLokasi::where('lokasi', $id_lokasi)->first();
            $nama_lokasi = '(' . $lokasi->kode_lokasi . ') ' . $lokasi->lokasi;
        } else {
            $nama_lokasi = 'Semua Lokasi';
            $view_lokasi = 'all';
        }

        if ($id_barang != 'all') {
            $list = $list->where('tb_barang.id', $id_barang);
            $barang = mBarang::where('id', $id_barang)->first(['kode_barang', 'nama_barang']);
            $nama_barang = '(' . $barang->kode_barang . ') ' . $barang->nama_barang;
            $view_barang = 'solo';
        } else {
            $nama_barang = 'Semua Produk';
            $view_barang = 'all';
        }

        $list = $list->get();
        $data = [
            'list' => $list,
            'nama_barang' => $nama_barang,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'nama_lokasi' => $nama_lokasi,
            'view_barang' => $view_barang,
            'view_lokasi' => $view_lokasi,
            'company' => Main::companyInfo()
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('inventory/kartuStok/kartuStokPdf', $data);

        return $pdf
            ->setPaper('A4', 'landscape')
            ->stream('Kartu Stok Produk ' . $date_start.' s/d '.$date_end);
    }

    function excel(Request $request)
    {
        ini_set("memory_limit", "-1");
        $id_barang = $request->input('id_barang');
        $id_lokasi = $request->input('id_lokasi');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $date_start_db = Main::format_date_db($date_start);
        $date_end_db = Main::format_date_db($date_end);

        $list = mKartuStok
            ::select([
                'tb_kartu_stok.*',
                'tb_barang.kode_barang',
                'tb_barang.nama_barang',
                'tb_lokasi.lokasi',
            ])
            ->leftJoin('tb_barang', 'tb_kartu_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'tb_kartu_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->whereBetween('tgl_transit', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_kartu_stok.tgl_transit', 'ASC');

        if ($id_lokasi != 'all') {
            $list = $list->where('tb_kartu_stok.lokasi', $id_lokasi);
            $lokasi = mLokasi::where('lokasi', $id_lokasi)->first();
            $nama_lokasi = '(' . $lokasi->kode_lokasi . ') ' . $lokasi->lokasi;
        } else {
            $nama_lokasi = 'Semua Lokasi';
            $view_lokasi = 'all';
        }

        if ($id_barang != 'all') {
            $list = $list->where('tb_barang.id', $id_barang);
            $barang = mBarang::where('id', $id_barang)->first(['kode_barang', 'nama_barang']);
            $nama_barang = '(' . $barang->kode_barang . ') ' . $barang->nama_barang;
            $view_barang = 'solo';
        } else {
            $nama_barang = 'Semua Produk';
            $view_barang = 'all';
        }

        $list = $list->get();
        $data = [
            'list' => $list,
            'nama_barang' => $nama_barang,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'nama_lokasi' => $nama_lokasi,
            'view_barang' => $view_barang,
            'view_lokasi' => $view_lokasi,
            'company' => Main::companyInfo()
        ];
        return view('inventory/kartuStok/kartuStokExcel', $data);
    }
}
