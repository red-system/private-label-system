<?php

namespace app\Http\Controllers\Client;

use app\Helpers\Main;
use app\Models\mCompany;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ClientCompany extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['client'],
                'route' => ''
            ],
            [
                'label' => $cons['client_2'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $count = mCompany::where('client_id', $from[1])->count();
        $company = mCompany::where('client_id', $from[1])->first();
        $data['company'] = $company;
        $data['from'] = $from;
        $data['user'] = $user;
        if($count > 0){
            return view('client/myCompany/myCompany', $data);
        }
        else{
            return redirect(route('companyNullPage'));
        }
    }

    function update(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $rules = [
            'company_name' => 'required',
            'company_phone' => 'required',
            'company_email' => 'required',
            'company_address' => 'required',
            'company_overview' => 'required',
            'company_data' => 'required',
        ];

        $attributes = [
            'company_name' => 'Company Name',
            'company_phone' => 'Company Phone',
            'company_email' => 'Company Email',
            'company_address' => 'Company Address',
            'company_overview' => 'Company Overview',
            'company_data' => 'Company Data',
        ];

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $company_id = $request->input('company_id');
        $company_name = $request->input('company_name');
        $company_phone = $request->input('company_phone');
        $company_email = $request->input('company_email');
        $company_address = $request->input('company_address');
        $company_overview = $request->input('company_overview');
        $company_data = $request->input('company_data');
        $data = [
            'company_name' => $company_name,
            'company_phone' => $company_phone,
            'company_email' => $company_email,
            'company_overview' => $company_overview,
            'company_address' => $company_address,
            'company_data' => $company_data,
        ];
        mCompany::where(['company_id' => $company_id])->update($data);
    }
}
