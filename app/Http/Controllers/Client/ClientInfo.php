<?php

namespace app\Http\Controllers\Client;

use app\Helpers\Main;
use app\Models\mClient;
use app\Models\mUser;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ClientInfo extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['client'],
                'route' => ''
            ],
            [
                'label' => $cons['client_1'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $client = mClient::where('client_id', $from[1])->first();
        $data['client'] = $client;
        $data['from'] = $from;
        $data['user'] = $user;

        return view('client/myInfo/myInfo', $data);
    }

    function update(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $rules = [
            'client_name' => 'required',
            'client_contact' => 'required',
            'client_email' => 'required',
            'client_desc' => 'required',
            'client_data' => 'required',
        ];

        $attributes = [
            'client_name' => 'Client Name',
            'client_contact' => 'Client Contact',
            'client_email' => 'Client Email',
            'client_desc' => 'Client Description',
            'client_data' => 'Client Data',
        ];

        /**
         * proses validasi
         */
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $client_id = $request->input('client_id');
        $client_name = $request->input('client_name');
        $client_contact = $request->input('client_contact');
        $client_email = $request->input('client_email');
        $client_desc = $request->input('client_desc');
        $client_data = $request->input('client_data');
        $data = [
            'client_name' => $client_name,
            'client_contact' => $client_contact,
            'client_email' => $client_email,
            'client_desc' => $client_desc,
            'client_data' => $client_data,
        ];
        mClient::where(['client_id' => $client_id])->update($data);
    }
}
