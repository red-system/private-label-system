<?php

namespace app\Http\Controllers\Client;

use app\Helpers\Main;
use app\Models\mAgency;
use app\Models\mCompany;
use app\Models\mFile;
use app\Models\mProject;
use app\Models\mProjectStatus;
use app\Models\mProjectType;
use app\Models\mService;
use app\Models\mTasks;
use PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ClientProject extends Controller
{
    private $view = [
        'project_order_number_label' => ["search_field" => "tb_project.project_order_number"],
        'channel_name_label' => ["search_field" => "tb_channel.channel_name"],
        'project_order_title_label' => ["search_field" => "tb_project.project_order_title"],
        'project_order_date_label' => ["search_field" => "tb_project.project_order_date"],
        'project_status_name_label' => ["search_field" => "tb_project_status.project_status_name"],
    ];
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['client'],
                'route' => ''
            ],
            [
                'label' => $cons['client_3'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        /**
         * mengambil data dari main berdasarkan breadcrumb menu yang dibuka
         */
        $data = Main::data($this->breadcrumb);
        $user = Session::get('user');
        $tabel = explode('|', $user->from);
        $view = array();
        foreach ($this->view as $key => $value) {
            array_push($view, array("data" => $key));
        }
//        $action = array();
//        foreach ($data['menuAction'] as $key => $value) {
//            if ($value) {
//                array_push($action, $key);
//            }
//        }
//        $data['actionButton'] = json_encode($action);
        $data['view'] = json_encode($view);


        return view('client/myProject/projectList', $data);
    }

    function list(Request $request)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $query = $request->input('search')["value"];
        $start = $request->input('start');
        $length = $request->input('length');
        $project = new mProject();
        $project_status_id = 'all';
        $result['iTotalRecords'] = $project->count_all($from[0], $from[1], $project_status_id);
        $result['iTotalDisplayRecords'] = $project->count_filter($query, $this->view, $from[0], $from[1], $project_status_id);
        $result['sEcho'] = 0;
        $result['sColumns'] = '';
        $data = $project->list($start, $length, $query, $this->view, $from[0], $from[1], $project_status_id);
        $no = $start + 1;
        foreach ($data as $value) {
//            $value->route['detail_view'] = route('clientProjectDetail', ['id' => $value->project_id]);
//            $value->route['tasks'] = route('clientProjectTasks', ['id' => $value->project_id]);
//            $value->route['print'] = route('clientProjectReport', ['id' => $value->project_id]);
            $value->target_blank['print'] = 'ada';
            $value->project_order_date = Main::format_date($value->project_order_date);
            $value->project_timeline = $value->project_timeline.' Days';

            $value->project_cost = Main::format_number($value->project_cost);
            $value->project_order_number_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => $value->project_id]) . '">' . $value->project_order_number . '</a>';
            $value->channel_name_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => $value->project_id]) . '">' . $value->channel_name . '</a>';
            $value->client_name_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => $value->project_id]) . '">' . $value->client_name . '</a>';
            $value->project_order_title_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => $value->project_id]) . '">' . $value->project_order_title . '</a>';
            $value->project_order_date_label = '<a style="color : #000" href="' . route('projectListDetail', ['id' => $value->project_id]) . '">' . Main::format_date($value->project_order_date) . '</a>';
            $value->project_status_name_label = '<span class="label font-weight-bold label-lg label-inline" style="color: ' . $value->project_status_text_color . '; background-color: ' . $value->project_status_bc_color . ';">' . $value->project_status_name . '</span>';
        }
        $result['aaData'] = $data;
        echo json_encode($result);
    }

    function detail($id)
    {
        $data['project'] = mProject::where('project_id', $id)->first();
        return view('client/myProject/projectBodyDetail', $data);
    }

    function tasks($id)
    {
        $data['project'] = mProject::where('project_id', $id)->first();

        $data['next_tasks'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Next Tasks')->get();
        $data['count_next_tasks'] = $data['next_tasks']->count();
        $data['completed'] = mTasks::where('project_id', $id)->where('tasks_status', '=', 'Completed')->get();
        $data['count_completed'] = $data['completed']->count();
        return view('client/myProject/modalBodyTasks', $data);
    }

    function print($id)
    {
        $data = Main::data($this->breadcrumb);

        $list = mProject::find($id);
        /**
         * mencari data tasks
         */
        $next_tasks = mTasks::where('project_id', '=', $id)->where('tasks_status', '=', 'Next Tasks')->get();
        $completed = mTasks::where('project_id', '=', $id)->where('tasks_status', '=', 'Completed')->get();

        /**
         * mencari data file
         */
        $file = mFile::where('project_id', '=', $id)->get();

        $data = array_merge($data, [
            'list' => $list,
            'next_tasks' => $next_tasks,
            'completed' => $completed,
            'file' => $file,
        ]);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('client/myProject/projectReportPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Project Report (' . $list->project_order_title . ')');
    }
}
