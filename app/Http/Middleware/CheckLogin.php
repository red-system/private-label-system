<?php

namespace app\Http\Middleware;

use app\Helpers\Main;
use Closure;
use Illuminate\Support\Facades\Session;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $login = Session::get('login');
        $link = Session::get('link');
        $redirect = null;

        if($link){
            $redirect = Main::decrypt($link);
        }
        if($login) {
            if($redirect == null){
                return redirect()->route('dashboardPage');
            }else{
                $request->session()->forget('link');
                return redirect($redirect);
            }
        }
        $session = $request->session();
//        dd($request->session()->get('_previous')['url']);
        return $next($request);
    }
}
