<?php

namespace app\Http\Middleware;

use app\Helpers\Main;
use Closure;
use Illuminate\Support\Facades\Session;

class AuthLogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $login = Session::get('login');
        $level = Session::get('level');
        $link = $request->getPathInfo();
//        dd($request);
        if (!$login) {
            if ($link == "" || $link == 'label/public/') {
                return redirect()->route('loginPage');
            } else {
                return redirect()->route('loginPageRedirect', Main::encrypt($link));
            }
        }

        $role = strpos($role, '|') ? explode('|', $role) : array($role);
        //echo json_encode([$level, $role]);

        if (!in_array($level, $role)) {
            $request->session()->regenerate();
            return redirect()->route('dashboardPage');
        }

        return $next($request);
    }
}
