<?php

namespace app\Imports;

use app\Models\mAgency;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class importExcel implements ToModel
{
    /**
    * @param Collection $collection
    */
    public function model(array $collection)
    {
        $count = count($collection);
        dd($collection, $count);
        return new mAgency([
            'agency_name' => $collection[0],
            'agency_type_id' => $collection[1],
            'agency_data' => $collection[2],
            'agency_desc' => $collection[3],
            'user_status' => $collection[4],
        ]);
    }
}
