<?php

namespace app\Imports;


use app\Models\mAgency;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class importAgency implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
//        dd($row);
//        if($row[0] != 'Judul'){
            return new mAgency([
                'agency_name' => $row['agency_name'],
                'agency_type_id' => $row['agency_type_id'],
                'agency_data' => $row['agency_data'],
                'agency_desc' => $row['agency_desc'],
                'user_status' => $row['user_status'],
            ]);
//        }
    }
}
