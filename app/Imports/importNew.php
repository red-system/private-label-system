<?php

namespace app\Imports;

use app\Models\mAgency;
use app\Models\mGrowth;
use app\Models\mUser;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\ToCollection;

class importNew implements ToCollection
{
    /**
     * @param Collection $collection
     */

    private $id = 0;
    public function collection(Collection $collection)
    {
        $user = Session::get('user');
        $from = explode('|', $user->from);
        $bulan = [];
        $fullfillment = [];
        $profit = [];
        $revenue = [];
        foreach ($collection as $value) {
            if($collection[0][0] != $value[0]){
                if ($value[0]) {
                    array_push($bulan, $value[0]);
                }
                if ($value[1]) {
                    array_push($fullfillment, $value[1]);
                }
                if ($value[2]) {
                    array_push($profit, $value[2]);
                }
                if ($value[3]) {
                    array_push($revenue, $value[3]);
                }
            }
        }
        $bulan = json_encode($bulan);
        $profit = json_encode($profit);
        $fullfillment = json_encode($fullfillment);
        $revenue = json_encode($revenue);

       $excel = mGrowth::create([
            'bulan' => $bulan,
            'fullfillment' => $fullfillment,
            'profit' => $profit,
            'revenue' => $revenue,
        ]);
//            'agency_name' => $collection,
//            'agency_type_id' => $collection,
//            'agency_data' => $collection,
//            'agency_desc' => $collection,
//            'user_status' => $collection,
//        ]);
        $this->id = $excel->growth_id;
    }

    public function getId()
    {
        return $this->id;
    }
}
