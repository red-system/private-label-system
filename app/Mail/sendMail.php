<?php

namespace app\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $notif = [
            0 => 'jordan@privatelabel.id',
            1 => 'laya@privatelabel.id',
            2 => 'siva@privatelabel.id'
        ];
        return $this->view('project.projectList.notifEmail', $this->data)->subject('New Message')->bcc($notif);
    }
}
