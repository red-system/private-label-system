<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;

class mUserRole extends Model
{
    protected $table = 'tb_user_role';
    protected $primaryKey = 'id';
    protected $fillable = [
        'role_name',
        'role_keterangan',
        'role_akses'
    ];
}
