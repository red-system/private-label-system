<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPemindahanStok extends Model
{
    protected $table = 'tb_pemindahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_pemindahan',
        'tgl_pemindahan',
        'asal_barang',
        'tujuan_barang',
        'keterangan',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all()
    {
        $count = DB::table($this->table);
        return $count->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table);

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*");
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function noTerakhir()
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id")
            ->orderBy($this->table . ".id", 'desc');
        return $data->first();
    }
}
