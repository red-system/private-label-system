<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mHutangSupplier extends Model
{
    protected $table = 'tb_hutang_supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_supplier',
        'id_po',
        'id_pembelian',
        'id_user',
        'no_hutang_supplier',
        'no_hutang_supplier_label',
        'no_faktur_pembelian',
        'tgl_faktur_pembelian',
        'jatuh_tempo_hutang_supplier',
        'supplier',
        'id_supplier',
        'total_hutang_supplier',
        'sisa_hutang_supplier',
        'keterangan_hutang_supplier',
        'status_hutang_supplier'
    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->orderBy($this->table . ".jatuh_tempo_hutang_supplier", 'ASC')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->orderBy($this->table . ".jatuh_tempo_hutang_supplier", 'ASC');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*")
            ->orderBy($this->table . ".jatuh_tempo_hutang_supplier", 'ASC');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countNoPo($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->where($this->table . '.tgl_faktur_pembelian', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->orderBy('no_hutang_supplier', 'DESC')->value('no_hutang_supplier');
        }

        return $no;
    }
}
