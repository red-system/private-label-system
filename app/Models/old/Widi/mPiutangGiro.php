<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPiutangGiro extends Model
{
    protected $table = 'tb_piutang_giro';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_piutang_langganan',
        'no_piutang_giro',
        'tgl_pencairan_piutang_giro',
        'jumlah_piutang_giro',
        'id_langganan',
        'no_faktur_penjualan',
        'tgl_faktur_penjualan',
        'nama_bank_piutang_giro',
        'keterangan_piutang_giro',
        'status_piutang_giro'
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".tgl_pencairan_piutang_giro", 'ASC')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".tgl_pencairan_piutang_giro", 'ASC');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_langganan.langganan")
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".tgl_pencairan_piutang_giro", 'ASC');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
