<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use app\Models\Widi\mStok;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\General;
use function foo\func;

class mBarang extends Model
{
    protected $table = 'tb_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_barang',
        'nama_barang',
        'id_golongan',
        'hpp',
        'harga_beli_terakhir_barang',
        'harga_jual_barang',
        'minimal_stok',
        'created_at',
        'updated_at',
    ];

    function stok_barang()
    {
        return $this->hasMany(mStok::class, 'id_barang', 'id');
    }

    function golongan()
    {
        return $this->belongsTo(mGolongan::class, 'id_golongan');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all()
    {
        $count = DB::table($this->table)
            ->select($this->table . ".*", 'perhitungan.total_stok', 'tb_golongan.golongan')
            ->leftjoin('tb_golongan', $this->table . '.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('perhitungan', $this->table . '.id', '=', 'perhitungan.id_barang');
        return $count->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->select($this->table . ".*", 'perhitungan.total_stok', 'tb_golongan.golongan')
            ->leftJoin('tb_golongan', $this->table . '.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('perhitungan', $this->table . '.id', '=', 'perhitungan.id_barang');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", DB::raw('if((perhitungan.total_stok is null),0,perhitungan.total_stok) as total_stok'), 'tb_golongan.golongan')
            ->leftjoin('tb_golongan', $this->table . '.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('perhitungan', $this->table . '.id', '=', 'perhitungan.id_barang');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function list_margin($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", DB::raw('if((perhitungan.total_stok is null),0,perhitungan.total_stok) as total_stok, (tb_barang.harga_jual_barang - tb_barang.hpp) as margin'), 'tb_golongan.golongan')
            ->leftjoin('tb_golongan', $this->table . '.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('perhitungan', $this->table . '.id', '=', 'perhitungan.id_barang');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function stokBarang($idBarang)
    {
        $data = DB::table('tb_stok')
            ->where('id_barang', '=', $idBarang);

        return $data->count();
    }

    public function margin()
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", DB::raw('if((perhitungan.total_stok is null),0,perhitungan.total_stok) as total_stok, (tb_barang.harga_jual_barang - tb_barang.hpp) as margin'), 'tb_golongan.golongan')
            ->leftjoin('tb_golongan', $this->table . '.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('perhitungan', $this->table . '.id', '=', 'perhitungan.id_barang');
        return $data->get();
    }

    public function list_pergerakan($start, $length, $query, $view, $date_start_db, $date_end_db, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".nama_barang", $this->table . ".kode_barang", "tb_penjualan.tgl_penjualan",
                "tb_item_penjualan.subtotal", "tb_item_penjualan.satuan_penjualan", "tb_item_penjualan.jml_barang_penjualan", "tb_po_barang.qty", "tb_po_barang.sub_total",
                "tb_pembelian.tanggal_pembelian", "lokasi_so.lokasi as so_lokasi", "lokasi_so.id as so_id_lokasi",
                "lokasi_po.lokasi as po_lokasi", "lokasi_po.id as po_id_lokasi", "tb_satuan.satuan as satuan_pembelian",
                DB::raw('SUM(tb_item_penjualan.jml_barang_penjualan) as qty_jual, SUM(tb_item_penjualan.subtotal) as total_jual, 
            SUM(tb_po_barang.sub_total) as total_beli, SUM(tb_po_barang.qty) as qty_beli'))
            ->leftjoin('tb_item_penjualan', $this->table . '.id', '=', 'tb_item_penjualan.id_barang')
            ->leftjoin('tb_pembelian_barang', $this->table . '.id', '=', 'tb_pembelian_barang.id_barang')
            ->leftjoin('tb_pembelian', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftjoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_penjualan', 'tb_item_penjualan.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_so', 'tb_penjualan.id_so', '=', 'tb_so.id')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_satuan', 'tb_po_barang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_lokasi as lokasi_so', 'tb_penjualan.id_lokasi', '=', 'lokasi_so.id')
            ->leftJoin('tb_lokasi as lokasi_po', 'tb_po.id_lokasi', '=', 'lokasi_po.id')
            ->groupBy($this->table . ".id")
            ->groupBy("tb_so.id_lokasi")
            ->orderBy($this->table . ".id", 'ASC')
            ->orderBy('tb_penjualan.tgl_penjualan', 'DESC');

        if ($id_lokasi != 'all') {
            $data = $data->Where('tb_so.id_lokasi', $id_lokasi)->OrWhere('tb_po.id_lokasi', $id_lokasi)
                ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start_db, $date_end_db])
                ->WhereBetween('tb_pembelian.tanggal_pembelian', [$date_start_db, $date_end_db]);
        } else {
            $data = $data->whereBetween('tb_penjualan.tgl_penjualan', [$date_start_db, $date_end_db])
                ->orWhereBetween('tb_pembelian.tanggal_pembelian', [$date_start_db, $date_end_db]);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_filter_pergerakan($query, $view, $date_start_db, $date_end_db, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".nama_barang", $this->table . ".kode_barang", "tb_penjualan.tgl_penjualan",
                "tb_item_penjualan.subtotal", "tb_item_penjualan.jml_barang_penjualan", "tb_po_barang.qty", "tb_po_barang.sub_total",
                "tb_pembelian.tanggal_pembelian", "lokasi_so.lokasi as so_lokasi", "lokasi_so.id as so_id_lokasi",
                "lokasi_po.lokasi as po_lokasi", "lokasi_po.id as po_id_lokasi",
                DB::raw('SUM(tb_item_penjualan.jml_barang_penjualan) as qty_jual, SUM(tb_item_penjualan.subtotal) as total_jual, 
            SUM(tb_po_barang.sub_total) as total_beli, SUM(tb_po_barang.qty) as qty_beli'))
            ->leftjoin('tb_item_penjualan', $this->table . '.id', '=', 'tb_item_penjualan.id_barang')
            ->leftjoin('tb_pembelian_barang', $this->table . '.id', '=', 'tb_pembelian_barang.id_barang')
            ->leftjoin('tb_pembelian', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftjoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_penjualan', 'tb_item_penjualan.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_so', 'tb_penjualan.id_so', '=', 'tb_so.id')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_lokasi as lokasi_so', 'tb_penjualan.id_lokasi', '=', 'lokasi_so.id')
            ->leftJoin('tb_lokasi as lokasi_po', 'tb_po.id_lokasi', '=', 'lokasi_po.id')
            ->groupBy($this->table . ".id")
            ->groupBy("tb_so.id_lokasi")
            ->orderBy($this->table . ".id", 'ASC')
            ->orderBy('tb_penjualan.tgl_penjualan', 'DESC');

        if ($id_lokasi != 'all') {
            $data = $data->Where('tb_so.id_lokasi', $id_lokasi)->OrWhere('tb_po.id_lokasi', $id_lokasi)
                ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start_db, $date_end_db])
                ->WhereBetween('tb_pembelian.tanggal_pembelian', [$date_start_db, $date_end_db]);
        } else {
            $data = $data->whereBetween('tb_penjualan.tgl_penjualan', [$date_start_db, $date_end_db])
                ->orWhereBetween('tb_pembelian.tanggal_pembelian', [$date_start_db, $date_end_db]);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->get();
    }

    public function count_all_pergerakan()
    {
        $count = DB::table($this->table)
            ->leftjoin('tb_item_penjualan', $this->table . '.id', '=', 'tb_item_penjualan.id_barang')
            ->leftjoin('tb_pembelian_barang', $this->table . '.id', '=', 'tb_pembelian_barang.id_barang')
            ->leftjoin('tb_pembelian', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftjoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_penjualan', 'tb_item_penjualan.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_so', 'tb_penjualan.id_so', '=', 'tb_so.id')
            ->leftJoin('tb_lokasi as lokasi_so', 'tb_so.id_lokasi', '=', 'lokasi_so.id')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_lokasi as lokasi_po', 'tb_po.id_lokasi', '=', 'lokasi_po.id')
            ->where('tb_penjualan.tgl_penjualan', '!=', null)
            ->orWhere('tb_pembelian.tanggal_pembelian', '!=', null)
            ->groupBy($this->table . ".id")
            ->groupBy("tb_so.id_lokasi");
        return $count->get();
    }
}
