<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mUser extends Model
{
    protected $table = 'tb_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_karyawan',
        'id_user_role',
        'username',
        'password',
        'inisial_karyawan',
        'id_lokasi'
    ];
    protected $hidden = [
        'password'
    ];

    function karyawan()
    {
        return $this->belongsTo(mKaryawan::class, 'id_karyawan');
    }

    function user_role()
    {
        return $this->belongsTo(mUserRole::class, 'id_user_role');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->join('tb_karyawan', $this->table . '.id_karyawan', '=', 'tb_karyawan.id')
//            ->join('tb_user_role',$this->table.'.id_user_role','=','tb_user_role.id')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->join('tb_karyawan', $this->table . '.id_karyawan', '=', 'tb_karyawan.id');
//            ->join('tb_user_role',$this->table.'.id_user_role','=','tb_user_role.id');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select('tb_user.*','tb_karyawan.nama_karyawan','tb_karyawan.posisi_karyawan')
            ->join('tb_karyawan',$this->table.'.id_karyawan','=','tb_karyawan.id');
//            ->join('tb_user_role',$this->table.'.id_user_role','=','tb_user_role.id');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
