<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Main;
use function foo\func;

class mItemPemindahanStok extends Model
{
    protected $table = 'tb_item_pemindahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_pemindahan',
        'id_barang',
        'jml_barang',
        'id_satuan',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all()
    {
        $count = DB::table($this->table);
        return $count->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table);

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*");
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_laporan()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_stok', $this->table . '.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_barang', 'tb_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_pemindahan', $this->table . '.id_pemindahan', '=', 'tb_pemindahan.id')
            ->leftJoin('tb_satuan', $this->table . '.id_satuan', '=', 'tb_satuan.id')
            ->count(DB::raw('DISTINCT id_stok'));

        return $count;
    }

    public function count_filter_laporan($query, $view, $date_start_db, $date_end_db, $lokasi)
    {

        $count = DB::table($this->table)
            ->select("tb_satuan.satuan", "tb_lokasi.lokasi", "tb_barang.nama_barang", "tb_barang.kode_barang",
                "tb_stok.id_barang", "tb_stok.id_lokasi",
                DB::raw('SUM(tb_item_pemindahan.jml_barang) as total_pindah'))
            ->leftJoin('tb_stok', $this->table . '.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_barang', 'tb_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_pemindahan', $this->table . '.id_pemindahan', '=', 'tb_pemindahan.id')
            ->leftJoin('tb_satuan', $this->table . '.id_satuan', '=', 'tb_satuan.id')
            ->whereBetween('tb_pemindahan.tgl_pemindahan', [$date_start_db, $date_end_db])
            ->groupBy('tb_stok.id')
            ->orderBy('tb_lokasi.lokasi', 'ASC')
            ->orderBy('tb_barang.nama_barang', 'ASC');

        if ($lokasi != null) {
            if (!empty($lokasi)) {
                if (!in_array(0, $lokasi)) {
                    $count = $count->whereIn('tb_lokasi.id', $lokasi);
                }
            }
        }
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->get();
    }

    public function list_laporan($start, $length, $query, $view, $date_start_db, $date_end_db, $lokasi)
    {
        $data = DB::table($this->table)
            ->select("tb_satuan.satuan", "tb_lokasi.lokasi", "tb_barang.nama_barang", "tb_barang.kode_barang",
                "tb_stok.id_barang", "tb_stok.id_lokasi",
                DB::raw('SUM(IF(tb_pemindahan.tgl_pemindahan BETWEEN "' . $date_start_db . '" AND  "' . $date_end_db . '" , 
                tb_item_pemindahan.jml_barang, 0)) as total_pemindahan'))
            ->leftJoin('tb_stok', $this->table . '.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_barang', 'tb_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_pemindahan', $this->table . '.id_pemindahan', '=', 'tb_pemindahan.id')
            ->leftJoin('tb_satuan', $this->table . '.id_satuan', '=', 'tb_satuan.id')
            ->whereBetween('tb_pemindahan.tgl_pemindahan', [$date_start_db, $date_end_db])
            ->groupBy('tb_stok.id')
            ->orderBy('tb_lokasi.lokasi', 'ASC')
            ->orderBy('tb_barang.nama_barang', 'ASC');

        if ($lokasi != null) {
            if (!empty($lokasi)) {
                if (!in_array(0, $lokasi)) {
                    $data = $data->whereIn('tb_lokasi.id', $lokasi);
                }
            }
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
