<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mLangganan extends Model
{
    protected $table = 'tb_langganan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_wilayah',
        'kode_langganan',
        'langganan',
        'langganan_alamat',
        'langganan_kontak',
        'created_at',
        'updated_at',
    ];

    public function count_all()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_wilayah', 'tb_langganan.id_wilayah', '=', 'tb_wilayah.id');
        return $count->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_wilayah', 'tb_langganan.id_wilayah', '=', 'tb_wilayah.id');

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_wilayah.wilayah")
            ->leftJoin('tb_wilayah', 'tb_langganan.id_wilayah', '=', 'tb_wilayah.id');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
