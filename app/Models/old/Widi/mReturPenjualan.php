<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mReturPenjualan extends Model
{
    protected $table = 'tb_retur_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_retur',
        'tgl_retur',
        'id_penjualan',
        'keterangan_retur',
        'total_retur',
        'id_user',
        'date'
    ];

    public function countNoReturPenjualan($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.date', $date)
            ->where($this->table . '.id_user', $id_user);
        return $data->count();
    }
}
