<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mStok extends Model
{
    protected $table = 'tb_stok';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_satuan',
        'id_lokasi',
        'jml_barang',
        'satuan',
        'hpp',
        'id_supplier',
        'date_expired'
    ];

    function lokasi()
    {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all_by_product($id)
    {
        return DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->where('id_barang', '=', $id)
            ->count();
    }

    public function count_filter_by_product($id, $query, $view)
    {

        $count = DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->where('id_barang', '=', $id);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_by_product($id, $start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.nama_barang", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_supplier.supplier")
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->where('id_barang', '=', $id);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->count();
    }

    public function count_filter($query, $view, $id_lokasi)
    {

        $count = DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id');

        if ($id_lokasi != 'all') {
            $count = $count->where('id_lokasi', $id_lokasi);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.*", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_supplier.supplier")
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->orderBy('id_lokasi', 'ASC')
            ->orderBy('id_barang', 'ASC');
        if ($id_lokasi != 'all') {
            $data = $data->where('id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function list_barang($start, $length, $query, $view, $id_lokasi, $id_golongan)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.*", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_supplier.supplier", "tb_golongan.golongan")
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->orderBy('id_barang', 'ASC')
            ->orderBy('id_lokasi', 'ASC');

        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }
        if ($id_golongan != 'all') {
            $data = $data->where('tb_barang.id_golongan', $id_golongan);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_barang()
    {
        return DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->count();
    }

    public function count_filter_barang($query, $view, $id_lokasi, $id_golongan)
    {

        $count = DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id');

        if ($id_lokasi != 'all') {
            $count = $count->where($this->table . '.id_lokasi', $id_lokasi);
        }
        if ($id_golongan != 'all') {
            $count = $count->where('tb_barang.id_golongan', $id_golongan);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function print()
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.*", "tb_lokasi.lokasi")
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->orderBy('id_lokasi', 'ASC')
            ->orderBy('id_barang', 'ASC');
        return $data->get();
    }

    public function print_barang()
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.*", "tb_lokasi.lokasi",
                DB::raw('if(tb_stok.satuan is null,tb_satuan.satuan,tb_stok.satuan) as satuan'))
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_satuan', $this->table . '.id_satuan', '=', 'tb_satuan.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->orderBy('id_lokasi', 'ASC');
        return $data->get();
    }

    public function count_all_nol()
    {
        return DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->where('jml_barang', '=', 0)
            ->count();
    }

    public function count_filter_nol($query, $view)
    {

        $count = DB::table($this->table)
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->where('jml_barang', '=', 0);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_nol($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.*", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_supplier.supplier")
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->orderBy('id_lokasi', 'ASC')
            ->orderBy('id_barang', 'ASC')
            ->where('jml_barang', '=', 0);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function print_nol()
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_barang.nama_barang", "tb_barang.kode_barang", "tb_lokasi.lokasi")
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->join('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->join('tb_wilayah', 'tb_lokasi' . '.id_wilayah', '=', 'tb_wilayah.id')
            ->where('jml_barang', '=', 0)
            ->orderBy('id_lokasi', 'ASC')
            ->orderBy('id_barang', 'ASC');
        return $data->get();
    }
}
