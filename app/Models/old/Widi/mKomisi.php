<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mKomisi extends Model
{
    protected $table = 'tb_komisi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_langganan',
        'id_piutang_langganan',
        'id_item_penjualan',
        'id_komisi_langganan',
        'min_beli',
        'qty_beli',
        'jenis_komisi',
        'komisi',
        'komisi_nominal',
        'status'
    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->leftJoin('tb_item_penjualan', $this->table . ".id_item_penjualan", '=', 'tb_item_penjualan.id')
            ->leftJoin('tb_penjualan', "tb_item_penjualan.id_penjualan", '=', 'tb_penjualan.id')
            ->leftJoin('tb_barang', "tb_item_penjualan.id_barang", '=', 'tb_barang.id')
            ->leftJoin('tb_piutang_langganan', $this->table . ".id_piutang_langganan", '=', 'tb_piutang_langganan.id')
            ->leftJoin('tb_komisi_langganan', $this->table . ".id_komisi_langganan", '=', 'tb_komisi_langganan.id')
            ->where('tb_piutang_langganan.sisa_piutang', '<=', 0)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->leftJoin('tb_item_penjualan', $this->table . ".id_item_penjualan", '=', 'tb_item_penjualan.id')
            ->leftJoin('tb_penjualan', "tb_item_penjualan.id_penjualan", '=', 'tb_penjualan.id')
            ->leftJoin('tb_barang', "tb_item_penjualan.id_barang", '=', 'tb_barang.id')
            ->leftJoin('tb_piutang_langganan', $this->table . ".id_piutang_langganan", '=', 'tb_piutang_langganan.id')
            ->leftJoin('tb_komisi_langganan', $this->table . ".id_komisi_langganan", '=', 'tb_komisi_langganan.id')
            ->where('tb_piutang_langganan.sisa_piutang', '<=', 0);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id", $this->table . ".min_beli", $this->table . ".qty_beli", $this->table . ".jenis_komisi",
                $this->table . ".komisi", $this->table . ".komisi_nominal", $this->table . ".status",
                "tb_langganan.langganan", "tb_penjualan.no_penjualan", "tb_penjualan.tgl_penjualan", "tb_barang.nama_barang")
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->leftJoin('tb_item_penjualan', $this->table . ".id_item_penjualan", '=', 'tb_item_penjualan.id')
            ->leftJoin('tb_penjualan', "tb_item_penjualan.id_penjualan", '=', 'tb_penjualan.id')
            ->leftJoin('tb_barang', "tb_item_penjualan.id_barang", '=', 'tb_barang.id')
            ->leftJoin('tb_piutang_langganan', $this->table . ".id_piutang_langganan", '=', 'tb_piutang_langganan.id')
            ->leftJoin('tb_komisi_langganan', $this->table . ".id_komisi_langganan", '=', 'tb_komisi_langganan.id')
            ->where('tb_piutang_langganan.sisa_piutang', '<=', 0);

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_laporan()
    {
        return DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->leftJoin('tb_item_penjualan', $this->table . ".id_item_penjualan", '=', 'tb_item_penjualan.id')
            ->leftJoin('tb_penjualan', "tb_item_penjualan.id_penjualan", '=', 'tb_penjualan.id')
            ->leftJoin('tb_barang', "tb_item_penjualan.id_barang", '=', 'tb_barang.id')
            ->leftJoin('tb_piutang_langganan', $this->table . ".id_piutang_langganan", '=', 'tb_piutang_langganan.id')
            ->leftJoin('tb_komisi_langganan', $this->table . ".id_komisi_langganan", '=', 'tb_komisi_langganan.id')
            ->where('tb_piutang_langganan.sisa_piutang', '<=', 0)
            ->count();
    }

    public function count_filter_laporan($query, $view, $date_start, $date_end, $status)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->leftJoin('tb_item_penjualan', $this->table . ".id_item_penjualan", '=', 'tb_item_penjualan.id')
            ->leftJoin('tb_penjualan', "tb_item_penjualan.id_penjualan", '=', 'tb_penjualan.id')
            ->leftJoin('tb_barang', "tb_item_penjualan.id_barang", '=', 'tb_barang.id')
            ->leftJoin('tb_piutang_langganan', $this->table . ".id_piutang_langganan", '=', 'tb_piutang_langganan.id')
            ->leftJoin('tb_komisi_langganan', $this->table . ".id_komisi_langganan", '=', 'tb_komisi_langganan.id')
            ->where('tb_piutang_langganan.sisa_piutang', '<=', 0)
            ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        if ($status != 'all') {
            $count = $count->where($this->table . ".status", $status);
        }
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_laporan($start, $length, $query, $view, $date_start, $date_end, $status)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id", $this->table . ".min_beli", $this->table . ".qty_beli", $this->table . ".jenis_komisi",
                $this->table . ".komisi", $this->table . ".komisi_nominal", $this->table . ".status", $this->table . ".updated_at",
                "tb_langganan.langganan", "tb_penjualan.no_penjualan", "tb_penjualan.tgl_penjualan", "tb_barang.nama_barang",
                "tb_item_penjualan.satuan_penjualan as satuan")
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->leftJoin('tb_item_penjualan', $this->table . ".id_item_penjualan", '=', 'tb_item_penjualan.id')
            ->leftJoin('tb_penjualan', "tb_item_penjualan.id_penjualan", '=', 'tb_penjualan.id')
            ->leftJoin('tb_barang', "tb_item_penjualan.id_barang", '=', 'tb_barang.id')
            ->leftJoin('tb_piutang_langganan', $this->table . ".id_piutang_langganan", '=', 'tb_piutang_langganan.id')
            ->leftJoin('tb_komisi_langganan', $this->table . ".id_komisi_langganan", '=', 'tb_komisi_langganan.id')
            ->where('tb_piutang_langganan.sisa_piutang', '<=', 0)
            ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        if ($status != 'all') {
            $data = $data->where($this->table . ".status", $status);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
