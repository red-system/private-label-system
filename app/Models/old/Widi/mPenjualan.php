<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPenjualan extends Model
{
    protected $table = 'tb_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_so',
        'id_langganan',
        'id_user',
        'id_salesman',
        'no_penjualan',
        'urutan',
        'tgl_penjualan',
        'lokasi_penjualan',
        'tgl_kirim_penjualan',
        'pelanggan_penjualan',
        'alamat_pengiriman',
        'penerima',
        'keterangan_penjualan',
        'total_penjualan',
        'disc_persen_penjualan',
        'disc_nominal_penjualan',
        'pajak_persen_penjualan',
        'pajak_nominal_penjualan',
        'ongkos_kirim_penjualan',
        'grand_total_penjualan',
        'dp_penjualan',
        'metode_pembayaran_dp_penjualan',
        'id_lokasi',
        'date'
    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->leftjoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftjoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id');
        foreach ($view as $value) {
            $count->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", 'tb_salesman.salesman')
            ->leftjoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id');
        foreach ($view as $value) {
            $data->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countNoPenjualan($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.date', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->max('urutan');
        }

        return $no;
    }

    public function count_all_kartu_langganan()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_item_penjualan', $this->table . '.id', '=', 'tb_item_penjualan.id_penjualan')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_so', $this->table . '.id_so', '=', 'tb_so.id')
            ->leftJoin('tb_retur_penjualan', $this->table . '.id', '=', 'tb_retur_penjualan.id_penjualan')
            ->groupBy($this->table . '.id')
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        return $count->count();
    }

    public function count_filter_kartu_langganan($query, $view, $id_langganan, $date_start_db, $date_end_db)
    {

        $data = DB::table($this->table)
            ->leftJoin('tb_item_penjualan', $this->table . '.id', '=', 'tb_item_penjualan.id_penjualan')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_so', $this->table . '.id_so', '=', 'tb_so.id')
            ->leftJoin('tb_retur_penjualan', $this->table . '.id', '=', 'tb_retur_penjualan.id_penjualan')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->groupBy($this->table . '.id')
            ->orderBy($this->table . '.no_penjualan', 'ASC');

        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count();
    }

    public function list_kartu_langganan($start, $length, $query, $view, $id_langganan, $date_start_db, $date_end_db)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id", $this->table . ".id_langganan", $this->table . ".no_penjualan", $this->table . ".tgl_penjualan",
                $this->table . ".grand_total_penjualan",
                $this->table . ".keterangan_penjualan", $this->table . '.pajak_nominal_penjualan', 'tb_piutang_langganan.updated_at as tgl_terbayar',
                'tb_piutang_langganan.jatuh_tempo', 'tb_piutang_langganan.status', 'tb_piutang_langganan.terbayar',
                'tb_piutang_langganan.sisa_piutang', 'tb_retur_penjualan.total_retur')
            ->leftJoin('tb_item_penjualan', $this->table . '.id', '=', 'tb_item_penjualan.id_penjualan')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_so', $this->table . '.id_so', '=', 'tb_so.id')
            ->leftJoin('tb_retur_penjualan', $this->table . '.id', '=', 'tb_retur_penjualan.id_penjualan')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->groupBy($this->table . '.id')
            ->orderBy($this->table . '.no_penjualan', 'ASC');

        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function count_all_laporan_penjualan()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        return $count->count();
    }

    public function count_filter_laporan_penjualan($query, $view, $date_start, $date_end, $id_langganan, $id_lokasi)
    {

        $data = DB::table($this->table)
            ->select($this->table . ".no_penjualan", $this->table . ".tgl_penjualan", $this->table . ".total_penjualan",
                'tb_lokasi.lokasi', 'tb_langganan.langganan', 'tb_salesman.salesman', 'tb_piutang_langganan.terbayar',
                'tb_piutang_langganan.sisa_piutang', 'tb_piutang_langganan.jatuh_tempo')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count();
    }

    public function list_laporan_penjualan($start, $length, $query, $view, $date_start, $date_end, $id_langganan, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_penjualan", $this->table . ".tgl_penjualan", $this->table . ".total_penjualan", $this->table . ".grand_total_penjualan",
                'tb_lokasi.lokasi', 'tb_langganan.langganan', 'tb_salesman.salesman', 'tb_piutang_langganan.terbayar',
                'tb_piutang_langganan.sisa_piutang', 'tb_piutang_langganan.jatuh_tempo')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function count_all_laporan_penjualan_laba()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_user', $this->table . '.id_user', '=', 'tb_user.id')
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        return $count->count();
    }

    public function count_filter_laporan_penjualan_laba($query, $view, $date_start, $date_end, $id_langganan, $id_lokasi)
    {

        $data = DB::table($this->table)
            ->select($this->table . ".no_penjualan", $this->table . ".tgl_penjualan", $this->table . ".total_penjualan",
                'tb_lokasi.lokasi', 'tb_langganan.langganan', 'tb_salesman.salesman', 'tb_piutang_langganan.terbayar',
                'tb_piutang_langganan.sisa_piutang', 'tb_piutang_langganan.jatuh_tempo')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_user', $this->table . '.id_user', '=', 'tb_user.id')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count();
    }

    public function list_laporan_penjualan_laba($start, $length, $query, $view, $date_start, $date_end, $id_langganan, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_penjualan", $this->table . ".tgl_penjualan", $this->table . ".total_penjualan", $this->table . ".grand_total_penjualan",
                'tb_lokasi.lokasi', 'tb_langganan.langganan', 'tb_salesman.salesman', 'tb_piutang_langganan.terbayar',
                'tb_piutang_langganan.sisa_piutang', 'tb_piutang_langganan.jatuh_tempo', 'tb_user.username')
            ->leftJoin('tb_piutang_langganan', $this->table . '.id', '=', 'tb_piutang_langganan.id_penjualan')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_user', $this->table . '.id_user', '=', 'tb_user.id')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy($this->table . '.no_penjualan', 'ASC');
        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function count_all_laporan_laba()
    {
        $count = DB::table('laba_penjualan')
            ->orderBy('laba_penjualan.jatuh_tempo', 'ASC');
        return $count->count();
    }

    public function count_filter_laporan_laba($query, $view, $date_start, $date_end)
    {

        $data = DB::table('laba_penjualan')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('laba_penjualan.jatuh_tempo', 'ASC');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWHere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->get();
    }

    public function list_laporan_laba($start, $length, $query, $view, $date_start, $date_end)
    {
        $data = DB::table('laba_penjualan')
            ->select("laba_penjualan.*", DB::raw('laba_penjualan.netto - laba_penjualan.hpp_penjualan as laba, 
            ((laba_penjualan.netto - laba_penjualan.hpp_penjualan)/laba_penjualan.netto) * 100 as margin'))
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('laba_penjualan.jatuh_tempo', 'ASC');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function count_all_omzet_salesman_per_barang()
    {
        $count = DB::table('analisa_penjualan_termasuk_ppn')
            ->groupBy('analisa_penjualan_termasuk_ppn' . '.id_salesman')
            ->orderBy('analisa_penjualan_termasuk_ppn' . '.id_barang', 'ASC');
        return $count->count();
    }

    public function count_filter_omzet_salesman_per_barang($query, $view, $date_start_db, $date_end_db, $id_barang, $id_lokasi, $id_golongan)
    {

        $data = DB::table('analisa_penjualan_termasuk_ppn')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59']);
        if ($id_lokasi != 'all') {
            $data = $data->where('id_lokasi', $id_lokasi);
        }
        if ($id_golongan != 'all') {
            $data = $data->where('id_golongan', $id_golongan);
        }
        if ($id_barang != 'all') {
            $data = $data->where('id_barang', $id_barang);
        }
        $data = $data->groupBy('analisa_penjualan_termasuk_ppn' . '.id_barang')
            ->groupBy('analisa_penjualan_termasuk_ppn' . '.id_salesman')
            ->orderBy('analisa_penjualan_termasuk_ppn' . '.id_barang', 'ASC');
//        $data->where(function($qry) use ($view,$query){
        foreach ($view as $value) {
            $data = $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
//        });
        return $data
            ->get();
    }

    public function list_omzet_salesman_per_barang($start, $length, $query, $view, $date_start_db, $date_end_db, $id_barang, $id_lokasi, $id_golongan)
    {
        $data = DB::table('analisa_penjualan_termasuk_ppn')
            ->select('analisa_penjualan_termasuk_ppn.*', DB::raw('SUM(dpp) as subtotal, SUM(qty) as total_qty'), 'tb_item_penjualan.satuan_penjualan as satuan')
            ->leftJoin('tb_item_penjualan', 'analisa_penjualan_termasuk_ppn.id_item_penjualan', '=', 'tb_item_penjualan.id')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59']);
        if ($id_lokasi != 'all') {
            $data = $data->where('id_lokasi', $id_lokasi);
        }
        if ($id_golongan != 'all') {
            $data = $data->where('id_golongan', $id_golongan);
        }
        if ($id_barang != 'all') {
            $data = $data->where('id_barang', $id_barang);
        }
        $data = $data->groupBy('analisa_penjualan_termasuk_ppn' . '.id_barang')
            ->groupBy('analisa_penjualan_termasuk_ppn' . '.id_salesman')
            ->orderBy('analisa_penjualan_termasuk_ppn' . '.id_barang', 'ASC');
//        $data->where(function($qry) use ($view,$query){
//            foreach ($view as $value){
//                $qry->orHaving($value['search_field'],'like','%'.$query.'%');
//            }
//        });
        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function count_all_omzet_salesman_per_faktur()
    {
        return DB::table($this->table)
            ->leftjoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->count();
    }

    public function count_filter_omzet_salesman_per_faktur($query, $view, $date_start_db, $date_end_db, $id_lokasi)
    {

        $count = DB::table($this->table)
            ->select($this->table . ".*", 'tb_salesman.salesman')
            ->leftjoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59']);

        if ($id_lokasi != 'all') {
            $count = $count->where('id_lokasi', $id_lokasi);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_omzet_salesman_per_faktur($start, $length, $query, $view, $date_start_db, $date_end_db, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", 'tb_salesman.salesman')
            ->leftjoin('tb_salesman', $this->table . '.id_salesman', '=', 'tb_salesman.id')
            ->whereBetween('tgl_penjualan', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59']);

        if ($id_lokasi != 'all') {
            $data = $data->where('id_lokasi', $id_lokasi);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_umur_penjualan()
    {
        return DB::table($this->table)
            ->leftjoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->groupBy('tb_penjualan.id_langganan')
            ->orderBy('tb_langganan.langganan', 'ASC')
            ->get();
    }

    public function count_filter_umur_penjualan($query, $view, $date)
    {

        $count = DB::table($this->table)
            ->select('tb_langganan.langganan',
                DB::raw('SUM(IF(tb_penjualan.tgl_penjualan < "' . $date . '", tb_penjualan.grand_total_penjualan, 0)) as sebelumnya,
            SUM(IF(tb_penjualan.tgl_penjualan BETWEEN "' . $date . '" AND  ("' . $date . '" + INTERVAL 7 DAY) , tb_penjualan.grand_total_penjualan, 0)) as minggu_pertama,
            SUM(IF(tb_penjualan.tgl_penjualan BETWEEN ("' . $date . '" + INTERVAL 7 DAY) AND ("' . $date . '" + INTERVAL 14 DAY) , tb_penjualan.grand_total_penjualan, 0)) as minggu_kedua,
            SUM(IF(tb_penjualan.tgl_penjualan BETWEEN ("' . $date . '" + INTERVAL 14 DAY) AND ("' . $date . '" + INTERVAL 21 DAY) , tb_penjualan.grand_total_penjualan, 0)) as minggu_ketiga,
            SUM(IF(tb_penjualan.tgl_penjualan > ("' . $date . '" + INTERVAL 21 DAY) , tb_penjualan.grand_total_penjualan, 0)) as seterusnya,
            SUM(tb_penjualan.grand_total_penjualan) as jumlah'))
            ->leftjoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->groupBy('tb_penjualan.id_langganan')
            ->orderBy('tb_langganan.langganan', 'ASC');

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->get();
    }

    public function list_umur_penjualan($start, $length, $query, $view, $date)
    {
        $data = DB::table($this->table)
            ->select('tb_langganan.langganan',
                DB::raw('SUM(IF(tb_penjualan.tgl_penjualan < "' . $date . '", tb_penjualan.grand_total_penjualan, 0)) as sebelumnya,
            SUM(IF(tb_penjualan.tgl_penjualan BETWEEN "' . $date . '" AND  ("' . $date . '" + INTERVAL 7 DAY) , tb_penjualan.grand_total_penjualan, 0)) as minggu_pertama,
            SUM(IF(tb_penjualan.tgl_penjualan BETWEEN ("' . $date . '" + INTERVAL 7 DAY) AND ("' . $date . '" + INTERVAL 14 DAY) , tb_penjualan.grand_total_penjualan, 0)) as minggu_kedua,
            SUM(IF(tb_penjualan.tgl_penjualan BETWEEN ("' . $date . '" + INTERVAL 14 DAY) AND ("' . $date . '" + INTERVAL 21 DAY) , tb_penjualan.grand_total_penjualan, 0)) as minggu_ketiga,
            SUM(IF(tb_penjualan.tgl_penjualan > ("' . $date . '" + INTERVAL 21 DAY) , tb_penjualan.grand_total_penjualan, 0)) as seterusnya,
            SUM(tb_penjualan.grand_total_penjualan) as jumlah'))
            ->leftjoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->groupBy('tb_penjualan.id_langganan')
            ->orderBy('tb_langganan.langganan', 'ASC');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
