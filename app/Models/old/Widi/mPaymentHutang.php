<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPaymentHutang extends Model
{
    protected $table = 'tb_payment_hutang_supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_hutang',
        'id_supplier',
        'urutan',
        'no_bukti_pembayaran',
        'tgl_pembayaran',
        'metode_pembayaran',
        'jumlah',
        'no_cek_gyro',
        'tgl_pencairan_gyro',
        'bank',
        'keterangan',
        'date'
    ];

    public function countNoPayment()
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.date', $date);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->max('urutan');
        }

        return $no;
    }
}
