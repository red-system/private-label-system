<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPaymentPiutangLangganan extends Model
{
    protected $table = 'tb_payment_piutang_langganan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_piutang',
        'id_langganan',
        'urutan',
        'no_bukti_pembayaran',
        'tgl_pembayaran',
        'metode_pembayaran',
        'jumlah',
        'no_cek_gyro',
        'tgl_pencairan_gyro',
        'bank',
        'keterangan',
        'date',
        'jenis_pembayaran'
    ];

    public function countNoPayment()
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.date', $date);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->max('urutan');
        }

        return $no;
    }
}
