<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mSO extends Model
{
    use SoftDeletes;
    protected $table = 'tb_so';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_user',
        'id_langganan',
        'id_salesman',
        'urutan',
        'no_so',
        'tgl_so',
        'lokasi_so',
        'tgl_kirim_so',
        'pelanggan_so',
        'alamat_pengiriman',
        'penerima',
        'keterangan_so',
        'total_so',
        'disc_persen_so',
        'disc_nominal_so',
        'pajak_persen_so',
        'pajak_nominal_so',
        'ongkos_kirim_so',
        'grand_total_so',
        'dp_so',
        'metode_pembayaran_dp_so',
        'id_lokasi',
        'surat_jalan',
        'penjualan'
    ];

//    public function getCreatedAtAttribute()
//    {
//        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
//    }
//
//    public function getUpdatedAtAttribute()
//    {
//        return \Carbon\Carbon::parse($this->attributes['updated_at'])
//            ->diffForHumans();
//    }

    public function count_all()
    {
        return DB::table($this->table)
            ->select($this->table . ".*",
                DB::raw('if(tb_so.id_langganan is null,tb_so.pelanggan_so,tb_langganan.langganan) as pelanggan_sok'))
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->where('deleted_at', null)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->select($this->table . ".*",
                DB::raw('if(tb_so.id_langganan is null,tb_so.pelanggan_so,tb_langganan.langganan) as pelanggan_sok'))
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id')
            ->where('deleted_at', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*",
                DB::raw('if(tb_so.id_langganan is null,tb_so.pelanggan_so,tb_langganan.langganan) as pelanggan_sok'))
            ->leftJoin('tb_langganan', $this->table . ".id_langganan", '=', 'tb_langganan.id');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
//
//    function ahay(){
//        $data = DB::table($this->table)
//            ->select($this->table.".*","tb_vendor.nama_vendor",
//                DB::raw('if(tb_service.nama_service is null,tb_hutang_supplier.nama_service,tb_service.nama_service) as nama_service'),
//                'no_booking','nama_agen')
//            ->join('tb_vendor',$this->table.".id_vendor",'=','tb_vendor.id')
//            ->join('tb_booking',$this->table.".id_booking",'=','tb_booking.id')
//            ->join('tb_agen',"tb_booking.id_agent",'=','tb_agen.id')
//
//            ->leftJoin('tb_service',$this->table.".id_service",'=','tb_service.id')
//        ;
//    }

    public function countNoSo($id_user, $id)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.tgl_so', $date)
            ->where($this->table . '.id_user', $id_user)
            ->where($this->table . '.id_langganan', $id);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->max('urutan');
        }

        return $no;
    }

    public function count_all_laporan()
    {
        return DB::table($this->table)
            ->select($this->table . ".*",
                DB::raw('if(tb_so.id_langganan is null,tb_so.pelanggan_so,tb_langganan.langganan) as pelanggan_sok'))
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.penjualan', '=', 'kosong')
            ->where($this->table . '.surat_jalan', '=', 'kosong')
            ->where('deleted_at', null)
            ->count();
    }

    public function count_filter_laporan($query, $view)
    {

        $count = DB::table($this->table)
            ->select($this->table . ".*",
                DB::raw('if(tb_so.id_langganan is null,tb_so.pelanggan_so,tb_langganan.langganan) as pelanggan_sok'))
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.penjualan', '=', 'kosong')
            ->where($this->table . '.surat_jalan', '=', 'kosong')
            ->where('deleted_at', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_laporan($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", 'tb_lokasi.lokasi',
                DB::raw('if(tb_so.id_langganan is null,tb_so.pelanggan_so,tb_langganan.langganan) as pelanggan_sok'))
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.penjualan', '=', 'kosong')
            ->where($this->table . '.surat_jalan', '=', 'kosong')
            ->where('deleted_at', null);

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_laporan_so()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . '.no_so', 'ASC');
        $count->where(function ($q) {
            $q->Where('penjualan', null)->orWhere('penjualan', '=', 'kosong');
        });
        return $count->count();
    }

    public function count_filter_laporan_so($query, $view, $date_start, $date_end, $id_langganan, $id_lokasi)
    {

        $data = DB::table($this->table)
            ->select($this->table . ".no_so", $this->table . ".tgl_so", $this->table . ".total_so",
                'tb_lokasi.lokasi', 'tb_langganan.langganan')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->whereBetween('tgl_so', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy($this->table . '.no_so', 'ASC');
        $data->where(function ($q) {
            $q->Where('penjualan', null)->orWhere('penjualan', '=', 'kosong');
        });
        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count();
    }

    public function list_laporan_so($start, $length, $query, $view, $date_start, $date_end, $id_langganan, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_so", $this->table . ".tgl_so", $this->table . ".total_so", $this->table . ".grand_total_so",
                'tb_lokasi.lokasi', 'tb_langganan.langganan')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->whereBetween('tgl_so', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy($this->table . '.no_so', 'ASC');
        $data->where(function ($q) {
            $q->Where('penjualan', null)->orWhere('penjualan', '=', 'kosong');
        });
        if ($id_langganan != 'all') {
            $data = $data->where($this->table . '.id_langganan', $id_langganan);
        }
        if ($id_lokasi != 'all') {
            $data = $data->where($this->table . '.id_lokasi', $id_lokasi);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }
}
