<?php

namespace app\Models\Widi;


use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mServiceContract extends Model
{
    protected $table = 'tb_service_contract';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_date',
        'id_service',
        'file_name',
        'file',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    protected $appends = ['file_url'];

    public function getFileUrlAttribute()
    {
        return url(env('PATH_SERVICE_CONTRACT') . $this->file);
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all($id = null)
    {
        $data = DB::table($this->table)
            ->join('tb_service', $this->table . '.id_service', '=', 'tb_service.id');
        if ($id != null) {
            $data->where(['id_service' => $id]);
        }
        return $data->count();
    }

    public function count_filter($query, $view, $id = null)
    {

        $count = DB::table($this->table)
            ->join('tb_service', $this->table . '.id_service', '=', 'tb_service.id');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($id != null) {
            $count->where(['id_service' => $id]);
        }
        return $count->count();
    }

    public function list($start, $length, $query, $view, $id = null)
    {

        $data = DB::table($this->table)
            ->select($this->table . ".*")
            ->join('tb_service', $this->table . '.id_service', '=', 'tb_service.id');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($id != null) {
            $data->where(['id_service' => $id]);
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
