<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mAnggaran extends Model
{
    protected $table = 'tb_anggaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tahun_anggaran',
        'total_anggaran'
    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->orderBy($this->table . ".tahun_anggaran", 'ASC')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->orderBy($this->table . ".tahun_anggaran", 'ASC');
        foreach ($view as $value) {
            $count->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*")
            ->orderBy($this->table . ".tahun_anggaran", 'ASC');
        foreach ($view as $value) {
            $data->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
