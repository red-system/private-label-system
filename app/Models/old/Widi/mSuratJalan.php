<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;

class mSuratJalan extends Model
{
    protected $table = 'tb_surat_jalan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_user',
        'no_surat_jalan',
        'id_so',
        'staff_surat_jalan',
        'tanggal_order',
        'grand_total_surat_jalan',
        'date'
    ];
}
