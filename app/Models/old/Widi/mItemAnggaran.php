<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;

class mItemAnggaran extends Model
{
    protected $table = 'tb_item_anggaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_anggaran',
        'kode_perkiraan',
        'item',
        'anggaran',
        'realisasi',
        'status'
    ];
}
