<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPaymentPiutangGiro extends Model
{
    protected $table = 'tb_payment_piutang_giro';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_piutang_giro',
        'id_langganan',
        'urutan',
        'no_bukti_pembayaran',
        'tgl_pembayaran',
        'metode_pembayaran',
        'jumlah',
        'bank',
        'keterangan',
        'date'
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function countNoPayment()
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.date', $date);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->max('urutan');
        }

        return $no;
    }
}
