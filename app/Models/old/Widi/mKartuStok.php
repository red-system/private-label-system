<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mKartuStok extends Model
{
    protected $table = 'tb_kartu_stok';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_lokasi',
        'id_stok',
        'lokasi',
        'tgl_transit',
        'keterangan',
        'stok_awal',
        'hpp_awal',
        'stok_masuk',
        'hpp_masuk',
        'stok_keluar',
        'hpp_keluar',
        'stok_akhir',
        'sst_sprl',
        'hpp_stok',
        'id_produksi',
        'id_pemindahan',
        'id_pemakaian',
        'id_stokOpname',
        'id_pembelian',
        'id_po',
        'id_pembelian_return',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function count_all()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_barang', 'tb_kartu_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'tb_kartu_stok.id_lokasi', '=', 'tb_lokasi.id');
        return $count->count();
    }

    public function count_filter($query, $view, $id_barang, $id_lokasi, $date_start_db, $date_end_db)
    {

        $data = DB::table($this->table)
            ->select($this->table . '.*', 'tb_barang.*', 'tb_lokasi.lokasi')
            ->leftJoin('tb_barang', 'tb_kartu_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'tb_kartu_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->whereBetween('tgl_transit', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_kartu_stok.id', 'ASC');

        if ($id_lokasi != 'all') {
            $data = $data->where('tb_kartu_stok.id_lokasi', $id_lokasi);
        }

        if ($id_barang != 'all') {
            $data = $data->where('tb_kartu_stok.id_barang', $id_barang);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count();
    }

    public function list($start, $length, $query, $view, $id_barang, $id_lokasi, $date_start_db, $date_end_db)
    {
        $data = DB::table($this->table)
            ->select($this->table . '.*', 'tb_barang.*', 'tb_lokasi.lokasi')
            ->leftJoin('tb_barang', 'tb_kartu_stok.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'tb_kartu_stok.id_lokasi', '=', 'tb_lokasi.id')
            ->whereBetween('tgl_transit', [$date_start_db . ' 00:00:00', $date_end_db . ' 23:59:59'])
            ->orderBy('tb_kartu_stok.id', 'ASC');

        if ($id_lokasi != 'all') {
            $data = $data->where('tb_kartu_stok.id_lokasi', $id_lokasi);
        }

        if ($id_barang != 'all') {
            $data = $data->where('tb_kartu_stok.id_barang', $id_barang);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function list_mutasi($start, $length, $query, $view, $view_awal, $view_akhir, $view_keluar_masuk, $date_start, $date_end)
    {
        $akhir = DB::table(DB::raw('(select DISTINCT id_stok, tgl_transit, id_barang, id_lokasi,
                if(tgl_transit >= "'.$date_start.' 00.00.00" AND tgl_transit <= "'.$date_end.' 23.59.00",stok_akhir, if(tgl_transit < "'.$date_start.' 00.00.00", stok_akhir, 0)) as saldo_akhir,
                if(tgl_transit >= "'.$date_start.' 00.00.00",hpp_stok, if(tgl_transit <= "'.$date_start.' 00.00.00",hpp_stok,0)) as hpp_akhir
                from tb_kartu_stok 
                WHERE tgl_transit <= "'.$date_end.' 23.59.00"
                order by tgl_transit DESC) as sub 
                GROUP BY sub.id_stok'))
            ->select('*');

        $filter_akhir = $akhir;
//        foreach ($view_akhir as $value){
//            $filter_akhir->orHaving($value['search_field'],'like','%'.$query.'%');
//        }
        if($length != null){
            $akhir
                ->offset($start)
                ->limit($length);
        }

        $filter_akhir = $filter_akhir->get();
        $akhir = $akhir->get();

        $awal = DB::table(DB::raw('(select DISTINCT id_stok, tgl_transit, id_barang, id_lokasi,
                if(tgl_transit >= "'.$date_start.' 00.00.00" AND tgl_transit <= "'.$date_end.' 23.59.00",stok_awal, if(tgl_transit < "'.$date_end.' 23.59.00", stok_akhir, stok_awal)) as saldo_awal,
                if(tgl_transit >= "'.$date_start.' 00.00.00",hpp_awal, if(tgl_transit <= "'.$date_start.' 00.00.00",hpp_stok, 0)) as hpp_awal
                from tb_kartu_stok 
                WHERE tgl_transit <= "'.$date_end.' 23.59.00"
                order by tgl_transit ASC) as sub 
                GROUP BY sub.id_stok'))
            ->select('*');

        $filter_awal = $awal;

//        foreach ($view_awal as $value){
//            $filter_awal->orHaving($value['search_field'],'like','%'.$query.'%');
//        }
        if($length != null){
            $awal
                ->offset($start)
                ->limit($length);
        }
        $filter_awal = $filter_awal->get();
        $awal = $awal->get();

        $keluar_masuk = DB::table($this->table)
            ->select($this->table.'.id_stok', $this->table.'.stok_masuk', $this->table.'.stok_keluar', 'tb_lokasi.lokasi', 'tb_barang.nama_barang', 'tb_barang.kode_barang',
                DB::raw('sum(if(tgl_transit >= "'.$date_start.' 00.00.00" and tgl_transit <= "'.$date_end.' 23.59.00", if(stok_masuk is null, 0, stok_masuk), 0)) as total_masuk,
            sum(if(tgl_transit >= "'.$date_start.' 00.00.00" and tgl_transit <= "'.$date_end.' 23.59.00", if(stok_keluar is null, 0, stok_keluar), 0)) as total_keluar'), 'tb_stok.satuan')
            ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
            ->where('tgl_transit', '<=', $date_end.' 23.59.00')
            ->groupBy('id_stok');

        $filter_keluar_masuk = $keluar_masuk;
        foreach ($view_keluar_masuk as $value){
            $filter_keluar_masuk->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if($length != null){
            $keluar_masuk
                ->offset($start)
                ->limit($length);
        }
        $keluar_masuk = $keluar_masuk->get();
        $filter_keluar_masuk = $filter_keluar_masuk->get();

        $merge = array();
//        if($filter_keluar_masuk->count() > 0){
            foreach ($filter_keluar_masuk as $km){
                $arr_data['id_stok'] = $km->id_stok;
                $arr_data['lokasi'] = $km->lokasi;
                $arr_data['kode_barang'] = $km->kode_barang;
                $arr_data['nama_barang'] = $km->nama_barang;
                $arr_data['total_masuk'] = $km->total_masuk;
                $arr_data['total_keluar'] = $km->total_keluar;
                $arr_data['satuan'] = $km->satuan;
                $arr_data['saldo_akhir'] = null;
                $arr_data['saldo_awal'] = null;
                array_push($merge, $arr_data);
            }
//        }
//        elseif ($filter_awal->count() > 0){
//            foreach ($filter_awal as $a){
//                $arr_data['id_stok'] = $a->id_stok;
//                $arr_data['id_lokasi'] = $a->id_lokasi;
//                $arr_data['id_barang'] = $a->id_barang;
//                $arr_data['saldo_awal'] = $a->saldo_awal;
//                $arr_data['hpp_awal'] = $a->hpp_awal;
//                $arr_data['saldo_akhir'] = null;
//                $arr_data['lokasi'] = null;
//                array_push($merge, $arr_data);
//            }
//        }
//        elseif ($filter_akhir->count() > 0){
//            foreach ($filter_akhir as $ak){
//                $arr_data['id_stok'] = $ak->id_stok;
//                $arr_data['saldo_akhir'] = $ak->saldo_akhir;
//                $arr_data['hpp_akhir'] = $ak->hpp_akhir;
//                $arr_data['saldo_awal'] = null;
//                $arr_data['lokasi'] = null;
//                array_push($merge, $arr_data);
//            }
//        }


        foreach ($merge as $key => $m) {
            if($m['saldo_akhir'] == null) {
                foreach ($akhir as $ak) {
                    if ($ak->id_stok == $m['id_stok']) {
                        array_set($merge[$key], 'saldo_akhir', $ak->saldo_akhir);
                        array_set($merge[$key], 'hpp_akhir', $ak->hpp_akhir);
                        array_set($merge[$key], 'tanggal_akhir', $ak->tgl_transit);
                    }
                }
            }
            if($m['saldo_awal'] == null) {
                foreach ($awal as $aw) {
                    if ($aw->id_stok == $m['id_stok']) {
                        array_set($merge[$key], 'id_lokasi', $aw->id_lokasi);
                        array_set($merge[$key], 'id_barang', $aw->id_barang);
                        array_set($merge[$key], 'saldo_awal', $aw->saldo_awal);
                        array_set($merge[$key], 'hpp_awal', $aw->hpp_awal);
                    }
                }
            }
//            if($m['lokasi'] == null) {
//                foreach ($keluar_masuk as $kms) {
//                    if ($kms->id_stok == $m['id_stok']) {
//                        array_set($merge[$key], 'lokasi', $kms->lokasi);
//                        array_set($merge[$key], 'kode_barang', $kms->kode_barang);
//                        array_set($merge[$key], 'nama_barang', $kms->nama_barang);
//                        array_set($merge[$key], 'total_masuk', $kms->total_masuk);
//                        array_set($merge[$key], 'total_keluar', $kms->total_keluar);
//                        array_set($merge[$key], 'satuan', $kms->satuan);
//                    }
//                }
//            }
        }

        return json_decode(json_encode($merge), FALSE);

    }

    public function total($id_lokasi, $id_barang, $date_start, $date_end)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id_barang", $this->table . ".id_lokasi",
                DB::raw('SUM(stok_masuk) AS total_stok_masuk, SUM(stok_keluar) AS total_stok_keluar'))
            ->where('tgl_transit', '<=', $date_end)
            ->where('tgl_transit', '>=', $date_start)
            ->where('id_lokasi', $id_lokasi)
            ->where('id_barang', $id_barang);
        return $data->first();
    }

}
