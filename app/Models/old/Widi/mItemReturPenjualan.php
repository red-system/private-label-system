<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;

class mItemReturPenjualan extends Model
{
    protected $table = 'tb_item_retur_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_stok',
        'id_retur_penjualan',
        'jml_barang_penjualan',
        'satuan_penjualan',
        'harga_penjualan',
        'disc_persen_penjualan',
        'disc_nominal',
        'jml_retur_penjualan',
        'subtotal_retur',
        'id_user'
    ];
}
