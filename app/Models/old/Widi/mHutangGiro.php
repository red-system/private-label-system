<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mHutangGiro extends Model
{
    protected $table = 'tb_hutang_giro';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_user',
        'id_hutang_supplier',
        'id_supplier_untuk',
        'id_pembelian',
        'no_hutang_giro',
        'no_hutang_giro_label',
        'tgl_pencairan_hutang_giro',
        'jumlah_hutang_giro',
        'no_faktur_pembelian',
        'tgl_faktur_pembelian',
        'nama_bank_hutang_giro',
        'keterangan_hutang_giro',
        'status_hutang_giro'
    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_supplier', $this->table . '.id_supplier_untuk', '=', 'tb_supplier.id')
            ->orderBy($this->table . ".tgl_pencairan_hutang_giro", 'ASC')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_supplier', $this->table . '.id_supplier_untuk', '=', 'tb_supplier.id')
            ->orderBy($this->table . ".tgl_pencairan_hutang_giro", 'ASC');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_supplier.supplier")
            ->leftJoin('tb_supplier', $this->table . '.id_supplier_untuk', '=', 'tb_supplier.id')
            ->orderBy($this->table . ".tgl_pencairan_hutang_giro", 'ASC');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countNoPo($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->where($this->table . '.tgl_faktur_pembelian', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->orderBy('no_hutang_giro', 'DESC')->value('no_hutang_giro');
        }

        return $no;
    }
}
