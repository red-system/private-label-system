<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mLokasi extends Model
{
    protected $table = 'tb_lokasi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_lokasi',
        'lokasi',
        'alamat',
        'kontak',
        'id_wilayah',
        'created_at',
        'updated_at',
    ];

    // function wilayah() {
    //     return $this->belongsTo(mWilayah::class, 'id_wilayah');
    // }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all()
    {
        $count = DB::table($this->table)
            ->join('tb_wilayah', $this->table . '.id_wilayah', '=', 'tb_wilayah.id');
        return $count->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->join('tb_wilayah', $this->table . '.id_wilayah', '=', 'tb_wilayah.id');

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", 'tb_wilayah.wilayah')
            ->join('tb_wilayah', $this->table . '.id_wilayah', '=', 'tb_wilayah.id');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
