<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mItemPenjualan extends Model
{
    protected $table = 'tb_item_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'id_barang',
        'id_stok',
        'nama_barang_penjualan',
        'jml_barang_penjualan',
        'golongan_penjualan',
        'satuan_penjualan',
        'harga_penjualan',
        'harga_net',
        'disc_persen_penjualan',
        'disc_nominal',
        'subtotal'
    ];

    function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function count_all_rekap_penjualan()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan');
        return $count->groupBy('tb_golongan.id')->get();
    }

    public function count_filter_rekap_penjualan($query, $view, $date_start, $date_end, $id_lokasi)
    {

        $data = DB::table($this->table)
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        if ($id_lokasi != 'all') {
            $data = $data->where('tb_penjualan.id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        $data->groupBy('tb_golongan.id');
        return $data->get();
    }

    public function list_rekap_penjualan($start, $length, $query, $view, $date_start, $date_end, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id", $this->table . ".satuan_penjualan as satuan", 'tb_golongan.golongan', DB::raw(
                'SUM(tb_item_penjualan.jml_barang_penjualan * tb_item_penjualan.harga_penjualan) as nilai,
                SUM(tb_item_penjualan.jml_barang_penjualan * tb_barang.hpp) hpp_total,
                SUM(tb_item_penjualan.jml_barang_penjualan) as jumlah_barang'))
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_golongan.id', '=', 'tb_barang.id_golongan')
            ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        if ($id_lokasi != 'all') {
            $data = $data->where('tb_penjualan.id_lokasi', $id_lokasi);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orHaving($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->groupBy('tb_golongan.id')
            ->get();
    }

    public function count_all_analisa_penjualan_semua_langganan()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_langganan', 'tb_langganan.id', '=', 'tb_penjualan.id_langganan');
        return $count->count();
    }

    public function count_filter_analisa_penjualan_semua_langganan($query, $view, $date_start, $date_end)
    {

        $data = DB::table($this->table)
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_langganan', 'tb_langganan.id', '=', 'tb_penjualan.id_langganan')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count();
    }

    public function list_analisa_penjualan_semua_langganan($start, $length, $query, $view, $date_start, $date_end)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".id_barang", $this->table . ".jml_barang_penjualan", $this->table . ".subtotal", 'tb_langganan.langganan',
                'tb_langganan.kode_langganan', 'tb_barang.kode_barang', 'tb_barang.nama_barang', $this->table.".satuan_penjualan as satuan")
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_langganan', 'tb_langganan.id', '=', 'tb_penjualan.id_langganan')
            ->whereBetween('tb_penjualan.tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orHaving($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data
            ->get();
    }

    public function count_all_analisa_penjualan_termasuk_ppn()
    {
        $count = DB::table('analisa_penjualan_termasuk_ppn')->groupBy('id_barang')->get();
        return $count;
    }

    public function count_filter_analisa_penjualan_termasuk_ppn($query, $view, $date_start, $date_end, $id_lokasi)
    {

        $data = DB::table('analisa_penjualan_termasuk_ppn');
        if ($id_lokasi != 'all') {
            $data = $data->where('id_lokasi', $id_lokasi);
        }
        $data = $data->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->groupBy('id_barang')->get();
    }

    public function list_analisa_penjualan_termasuk_ppn($start, $length, $query, $view, $date_start, $date_end, $id_lokasi)
    {
        $data = DB::table('analisa_penjualan_termasuk_ppn')
            ->select("analisa_penjualan_termasuk_ppn.*",
                DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) as total_qty, SUM(analisa_penjualan_termasuk_ppn.dpp) as total_dpp, SUM(analisa_penjualan_termasuk_ppn.ppn) as total_ppn, SUM(analisa_penjualan_termasuk_ppn.total) as total_nilai, 
                SUM(analisa_penjualan_termasuk_ppn.nominal) as total_nominal'), 'tb_item_penjualan.satuan_penjualan as satuan')
            ->leftJoin('tb_item_penjualan', 'analisa_penjualan_termasuk_ppn.id_item_penjualan', '=', 'tb_item_penjualan.id');
        if ($id_lokasi != 'all') {
            $data = $data->where('id_lokasi', $id_lokasi);
        }
        $data = $data->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->groupBy('id_barang')
            ->get();
    }

    /**
     * terapkan query ini untuk laporan analisa penjualan semua langganan
     */
//SELECT tb_item_penjualan.id_barang, tb_barang.kode_barang, tb_barang.nama_barang,
//SUM(tb_item_penjualan.jml_barang_penjualan) as qty,
//SUM(tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100)) as setelah_diskon_persen,
//
//SUM(tb_penjualan.disc_nominal_penjualan / (tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*100) as persen_nominal,
//
//SUM((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*(tb_penjualan.disc_nominal_penjualan / (tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*100)/100) as nominal,
//
//SUM((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100)) - ((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*(tb_penjualan.disc_nominal_penjualan / (tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*100)/100)) as dpp,
//
//SUM(tb_penjualan.pajak_persen_penjualan*((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100)) - ((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*(tb_penjualan.disc_nominal_penjualan / (tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*100)/100))/100) as ppn,
//
//SUM(((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100)) - ((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*(tb_penjualan.disc_nominal_penjualan / (tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*100)/100))+(tb_penjualan.pajak_persen_penjualan*((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100)) - ((tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*(tb_penjualan.disc_nominal_penjualan / (tb_item_penjualan.subtotal - (tb_item_penjualan.subtotal * tb_penjualan.disc_persen_penjualan/100))*100)/100))/100)) as total
//
//FROM tb_item_penjualan
//LEFT JOIN tb_penjualan ON tb_penjualan.id = tb_item_penjualan.id_penjualan
//LEFT JOIN tb_barang ON tb_item_penjualan.id_barang = tb_barang.id
//GROUP BY tb_item_penjualan.id_barang
}
