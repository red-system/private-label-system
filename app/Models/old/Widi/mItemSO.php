<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mItemSO extends Model
{
    protected $table = 'tb_item_so';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_so',
        'id_barang',
        'id_lokasi',
        'id_stok',
        'id_satuan',
        'nama_barang_so',
        'jml_barang_so',
        'golongan_so',
        'satuan_so',
        'harga_so',
        'disc_persen_so',
        'disc_nominal_so',
        'harga_net',
        'subtotal'
    ];

    function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function list_item_so($id)
    {
        $data = DB::table($this->table)
            ->leftJoin('tb_barang', 'tb_item_so.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_item_so.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi', 'tb_item_so.id_lokasi', '=', 'tb_lokasi.id')
            ->where('id_so', $id)->get();
        return $data;
    }
}
