<?php

namespace app\Models\Widi;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPiutangLangganan extends Model
{
    protected $table = 'tb_piutang_langganan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'no_piutang_langganan',
        'no_faktur_penjualan',
        'tgl_faktur_penjualan',
        'jatuh_tempo',
        'id_langganan',
        'total_piutang',
        'sisa_piutang',
        'terbayar',
        'keterangan',
        'status',
        'lokasi_penjualan',
        'date'
    ];

//    public function getCreatedAtAttribute()
//    {
//        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
//    }
//
//    public function getUpdatedAtAttribute()
//    {
//        return \Carbon\Carbon::parse($this->attributes['updated_at'])
//            ->diffForHumans();
//    }

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".jatuh_tempo", 'ASC')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".jatuh_tempo", 'ASC');
        foreach ($view as $value) {
            $count->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", "tb_langganan.langganan")
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".jatuh_tempo", 'ASC');
        foreach ($view as $value) {
            $data->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countNoPiutangLangganan($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table . '.date', $date);

        return $data->count();
    }

    public function count_all_saldo_piutang()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_langganan', 'tb_langganan.id', '=', $this->table . '.id_langganan')
            ->leftJoin('tb_wilayah', 'tb_wilayah.id', '=', 'tb_langganan.id_wilayah')
            ->orderBy('tb_langganan.kode_langganan', 'ASC');
        return $count->count(DB::raw('DISTINCT tb_piutang_langganan.id_langganan'));
    }

    public function count_filter_saldo_piutang($query, $view, $id_langganan)
    {

        $data = DB::table($this->table)
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_langganan', 'tb_langganan.id', '=', $this->table . '.id_langganan')
            ->leftJoin('tb_wilayah', 'tb_wilayah.id', '=', 'tb_langganan.id_wilayah')
            ->orderBy('tb_langganan.kode_langganan', 'ASC');

        if ($id_langganan != null) {
            if (!empty($id_langganan)) {
                if (!in_array(0, $id_langganan)) {
                    $data = $data->whereIn('tb_langganan.id', $id_langganan);
                }
            }
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $data->count(DB::raw('DISTINCT tb_piutang_langganan.id_langganan'));
    }

    public function list_saldo_piutang($start, $length, $query, $view, $id_langganan)
    {
        $data = DB::table($this->table)
            ->select("tb_langganan.kode_langganan", "tb_langganan.langganan", "tb_wilayah.wilayah",
                DB::raw('SUM(tb_piutang_langganan.sisa_piutang) as netto, SUM(tb_piutang_langganan.sisa_piutang) as netto_rp'))
            ->leftJoin('tb_penjualan', 'tb_penjualan.id', '=', $this->table . '.id_penjualan')
            ->leftJoin('tb_langganan', 'tb_langganan.id', '=', $this->table . '.id_langganan')
            ->leftJoin('tb_wilayah', 'tb_wilayah.id', '=', 'tb_langganan.id_wilayah')
            ->groupBy('tb_langganan.id')
            ->orderBy('tb_langganan.kode_langganan', 'ASC');

        if ($id_langganan != null) {
            if (!empty($id_langganan)) {
                if (!in_array(0, $id_langganan)) {
                    $data = $data->whereIn('tb_langganan.id', $id_langganan);
                }
            }
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function list_nota($id_langganan)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_faktur_penjualan", $this->table . ".tgl_faktur_penjualan", $this->table . ".jatuh_tempo"
                , $this->table . ".sisa_piutang")
            ->where('status', '=', 'belum_lunas')
            ->where('id_langganan', '=', $id_langganan)
            ->orderBy($this->table . '.jatuh_tempo', 'ASC');
        return $data->get();
    }

    public function count_all_mutasi_langganan()
    {
        return DB::table('mutasiLangganan')
            ->groupBy('id_langganan')
            ->get();
    }

    public function count_filter_mutasi_langganan($query, $view, $date_start, $date_end, $id_lokasi)
    {
        $count = DB::table('mutasiLangganan')
            ->select('mutasiLangganan.id_piutang_langganan', 'mutasiLangganan.id_langganan', 'mutasiLangganan.kode_langganan', 'mutasiLangganan.langganan',
                DB::raw('SUM(IF( mutasiLangganan.status_piutang_langganan = "lunas", 
				IF(mutasiLangganan.tgl_payment_piutang_langganan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '" AND mutasiLangganan.tanggal_piutang_langganan 
				NOT BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '",mutasiLangganan.jumlah_payment, 0) , 
				IF(mutasiLangganan.tgl_penjualan < "' . $date_start . ' 00:00:00' . '" AND mutasiLangganan.status_piutang_langganan = "belum_lunas", mutasiLanggananLanjut.rata, 0)
	            )) - SUM(IF(mutasiLangganan.tgl_payment_piutang_langganan < "2020-02-12" AND mutasiLangganan.status_piutang_langganan = "belum_lunas",mutasiLangganan.jumlah_payment, 0)) AS saldo_awal,
	            SUM( IF(mutasiLangganan.tgl_penjualan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '", mutasiLanggananLanjut.rata, 0)) AS penjualan,
	            SUM( IF(mutasiLangganan.tgl_payment_piutang_langganan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '"AND mutasiLangganan.jenis_pembayaran = "bayar",jumlah_payment, 0)) AS bayar,
	            SUM( IF(mutasiLangganan.tgl_payment_piutang_langganan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '"AND mutasiLangganan.jenis_pembayaran = "retur",jumlah_payment, 0)) AS retur'))
            ->leftJoin('mutasiLanggananLanjut', 'mutasiLangganan.id_piutang_langganan', '=', 'mutasiLanggananLanjut.id_piutang_langganan')
            ->groupBy('id_langganan')
            ->orderBy('kode_langganan', 'ASC');
        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($id_lokasi != 'all') {
            $count->where('id_lokasi', $id_lokasi);
        }
        return $count->get();
    }

    public function list_mutasi_langganan($start, $length, $query, $view, $date_start, $date_end, $id_lokasi)
    {
        $data = DB::table('mutasiLangganan')
            ->select('mutasiLangganan.id_piutang_langganan', 'mutasiLangganan.id_langganan', 'mutasiLangganan.kode_langganan', 'mutasiLangganan.langganan',
                DB::raw('SUM(IF( mutasiLangganan.status_piutang_langganan = "lunas", 
				IF(mutasiLangganan.tgl_payment_piutang_langganan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '" AND mutasiLangganan.tanggal_piutang_langganan 
				NOT BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '",mutasiLangganan.jumlah_payment, 0) , 
				IF(mutasiLangganan.tgl_penjualan < "' . $date_start . ' 00:00:00' . '" AND mutasiLangganan.status_piutang_langganan = "belum_lunas", mutasiLanggananLanjut.rata, 0)
	            )) - SUM(IF(mutasiLangganan.tgl_payment_piutang_langganan < "2020-02-12" AND mutasiLangganan.status_piutang_langganan = "belum_lunas",mutasiLangganan.jumlah_payment, 0)) AS saldo_awal,
	            SUM( IF(mutasiLangganan.tgl_penjualan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '", mutasiLanggananLanjut.rata, 0)) AS penjualan,
	            SUM( IF(mutasiLangganan.tgl_payment_piutang_langganan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '"AND mutasiLangganan.jenis_pembayaran = "bayar",jumlah_payment, 0)) AS bayar,
	            SUM( IF(mutasiLangganan.tgl_payment_piutang_langganan BETWEEN "' . $date_start . ' 00:00:00' . '" AND "' . $date_end . ' 23:59:00' . '"AND mutasiLangganan.jenis_pembayaran = "retur",jumlah_payment, 0)) AS retur'))
            ->leftJoin('mutasiLanggananLanjut', 'mutasiLangganan.id_piutang_langganan', '=', 'mutasiLanggananLanjut.id_piutang_langganan')
            ->groupBy('id_langganan')
            ->orderBy('kode_langganan', 'ASC');

        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($id_lokasi != 'all') {
            $data->where('id_lokasi', $id_lokasi);
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_tagihan()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".jatuh_tempo", 'ASC');
        $count->where(function ($a) {
            $a->where($this->table . '.status', '=', 'belum_lunas')
                ->orWhere($this->table . '.status', '=', '')
                ->orWhere($this->table . '.status', '=', null);
        });

        return $count->count();
    }

    public function count_filter_tagihan($query, $view, $date)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . ".jatuh_tempo", 'ASC');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        $count->where(function ($a) {
            $a->where($this->table . '.status', '=', 'belum_lunas')
                ->orWhere($this->table . '.status', '=', '')
                ->orWhere($this->table . '.status', '=', null);
        });
        return $count->count();
    }

    public function list_tagihan($start, $length, $query, $view, $date)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_faktur_penjualan", $this->table . ".jatuh_tempo"
                , $this->table . ".sisa_piutang", "tb_langganan.langganan", "tb_salesman.salesman", DB::raw("DATEDIFF('" . $date . "', " . $this->table . ".jatuh_tempo) as telat"))
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->orderBy($this->table . '.jatuh_tempo', 'ASC');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        $data->where(function ($a) {
            $a->where($this->table . '.status', '=', 'belum_lunas')
                ->orWhere($this->table . '.status', '=', '')
                ->orWhere($this->table . '.status', '=', null);
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_laporan_jatuh_tempo()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.jatuh_tempo', '<', now())
            ->orderBy($this->table . ".jatuh_tempo", 'ASC');
        $count->where(function ($a) {
            $a->where($this->table . '.status', '=', 'belum_lunas')
                ->orWhere($this->table . '.status', '=', '')
                ->orWhere($this->table . '.status', '=', null);
        });

        return $count->count();
    }

    public function count_filter_laporan_jatuh_tempo($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.jatuh_tempo', '<', now())
            ->orderBy($this->table . ".jatuh_tempo", 'ASC');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        $count->where(function ($a) {
            $a->where($this->table . '.status', '=', 'belum_lunas')
                ->orWhere($this->table . '.status', '=', '')
                ->orWhere($this->table . '.status', '=', null);
        });
        return $count->count();
    }

    public function list_laporan_jatuh_tempo($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_faktur_penjualan", $this->table . ".jatuh_tempo", $this->table . ".tgl_faktur_penjualan"
                , $this->table . ".sisa_piutang", "tb_langganan.langganan", "tb_lokasi.lokasi")
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.jatuh_tempo', '<', now())
            ->orderBy($this->table . '.jatuh_tempo', 'ASC');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        $data->where(function ($a) {
            $a->where($this->table . '.status', '=', 'belum_lunas')
                ->orWhere($this->table . '.status', '=', '')
                ->orWhere($this->table . '.status', '=', null);
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_tepat_waktu()
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->where($this->table . '.status', '=', 'lunas')
            ->orderBy($this->table . '.jatuh_tempo', 'ASC');

        return $count->count();
    }

    public function count_filter_tepat_waktu($query, $view, $date_start, $date_end)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->whereBetween($this->table . '.jatuh_tempo', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->where($this->table . '.status', '=', 'lunas')
            ->orderBy($this->table . '.jatuh_tempo', 'ASC');
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_tepat_waktu($start, $length, $query, $view, $date_start, $date_end)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".no_faktur_penjualan", $this->table . ".jatuh_tempo", $this->table . ".terbayar", $this->table . ".total_piutang",
                $this->table . ".sisa_piutang", $this->table . ".updated_at", "tb_langganan.langganan")
            ->leftJoin('tb_langganan', $this->table . '.id_langganan', '=', 'tb_langganan.id')
            ->whereBetween($this->table . '.jatuh_tempo', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->where($this->table . '.status', '=', 'lunas')
            ->orderBy($this->table . '.jatuh_tempo', 'ASC');

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_rekap_omzet_salesman()
    {
        return DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_item_penjualan', 'tb_penjualan.id', '=', 'tb_item_penjualan.id_penjualan')
            ->groupBy('tb_penjualan.id_salesman')
            ->orderBy('tb_salesman.salesman', 'ASC')
            ->count();
    }

    public function count_filter_rekap_omzet_salesman($query, $view, $date_start, $date_end, $id_lokasi)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_item_penjualan', 'tb_penjualan.id', '=', 'tb_item_penjualan.id_penjualan')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        if ($id_lokasi != 'all') {
            $count = $count->where('tb_penjualan.id_lokasi', $id_lokasi);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->groupBy('tb_penjualan.id_salesman')
            ->orderBy('tb_salesman.salesman', 'ASC')->get();
    }

    public function list_rekap_omzet_salesman($start, $length, $query, $view, $date_start, $date_end, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select(DB::raw('tb_salesman.salesman, SUM(tb_item_penjualan.jml_barang_penjualan) as qty, 
                SUM(tb_penjualan.grand_total_penjualan) as total,
                SUM(IF(tb_piutang_langganan.status = "lunas" and tb_piutang_langganan.updated_at <= tb_piutang_langganan.jatuh_tempo, total_piutang,0)) as dibayar_tepat_waktu')
            , 'tb_item_penjualan.satuan_penjualan as satuan')
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_salesman', 'tb_penjualan.id_salesman', '=', 'tb_salesman.id')
            ->leftJoin('tb_item_penjualan', 'tb_penjualan.id', '=', 'tb_item_penjualan.id_penjualan')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59']);

        if ($id_lokasi != 'all') {
            $data = $data->where('tb_penjualan.id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->groupBy('tb_penjualan.id_salesman')
            ->orderBy('tb_salesman.salesman', 'ASC')
            ->get();
    }

    public function count_all_analisa_penjualan_langganan()
    {
        return DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_payment_piutang_langganan', $this->table . '.id', '=', 'tb_payment_piutang_langganan.id_piutang')
            ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_wilayah', 'tb_lokasi.id_wilayah', '=', 'tb_wilayah.id')
            ->groupBy('tb_penjualan.id')
            ->get();
    }

    public function count_filter_analisa_penjualan_langganan($query, $view, $date_start, $date_end, $id_lokasi)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_payment_piutang_langganan', $this->table . '.id', '=', 'tb_payment_piutang_langganan.id_piutang')
            ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_wilayah', 'tb_lokasi.id_wilayah', '=', 'tb_wilayah.id')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('tb_wilayah.wilayah', 'ASC');

        if ($id_lokasi != 'all') {
            $count = $count->where('tb_penjualan.id_lokasi', $id_lokasi);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->groupBy('tb_penjualan.id')->get();
    }

    public function list_analisa_penjualan_langganan($start, $length, $query, $view, $date_start, $date_end, $id_lokasi)
    {
        $data = DB::table($this->table)
            ->select('tb_lokasi.id_wilayah', 'tb_wilayah.wilayah', 'tb_langganan.langganan', 'tb_penjualan.grand_total_penjualan',
                DB::raw('SUM(IF(tb_payment_piutang_langganan.jenis_pembayaran = "retur",tb_payment_piutang_langganan.jumlah,0)) as retur,
                tb_penjualan.grand_total_penjualan - SUM(IF(tb_payment_piutang_langganan.jenis_pembayaran = "retur",tb_payment_piutang_langganan.jumlah,0)) as net_penjualan,
                SUM(IF(tb_payment_piutang_langganan.jenis_pembayaran = "bayar",tb_payment_piutang_langganan.jumlah,0)) as pembayaran'))
            ->leftJoin('tb_penjualan', $this->table . '.id_penjualan', '=', 'tb_penjualan.id')
            ->leftJoin('tb_payment_piutang_langganan', $this->table . '.id', '=', 'tb_payment_piutang_langganan.id_piutang')
            ->leftJoin('tb_langganan', 'tb_penjualan.id_langganan', '=', 'tb_langganan.id')
            ->leftJoin('tb_lokasi', 'tb_penjualan.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_wilayah', 'tb_lokasi.id_wilayah', '=', 'tb_wilayah.id')
            ->whereBetween('tgl_penjualan', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('tb_wilayah.wilayah', 'ASC');

        if ($id_lokasi != 'all') {
            $data = $data->where('tb_penjualan.id_lokasi', $id_lokasi);
        }
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->groupBy('tb_penjualan.id')
            ->get();
    }
}
