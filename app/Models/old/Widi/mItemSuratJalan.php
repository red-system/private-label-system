<?php

namespace app\Models\Widi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mItemSuratJalan extends Model
{
    protected $table = 'tb_item_surat_jalan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_surat_jalan',
        'id_barang',
        'id_stok',
        'qty_kirim_surat_jalan',
        'satuan_surat_jalan',
        'kali_surat_jalan',
        'subtotal_surat_jalan',
        'diskon'
    ];

    public function pdf($id)
    {
        $data = DB::table($this->table)
            ->select($this->table . ".*", 'tb_barang.*', 'tb_stok.*')
            ->join('tb_barang', $this->table . '.id_barang', '=', 'tb_barang.id')
            ->join('tb_stok', $this->table . '.id_stok', '=', 'tb_stok.id')
            ->where('id_surat_jalan', $id);
        return $data->get();
    }
}
