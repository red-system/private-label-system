<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use app\Models\Mahendra\mPembelian;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mHutangSupplier extends Model
{
    protected $table = 'tb_hutang_supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_supplier',
        'id_po',
        'id_pembelian',
        'id_user',
        'no_hutang_supplier',
        'no_hutang_supplier_label',
        'no_faktur_pembelian',
        'tgl_faktur_pembelian',
        'jatuh_tempo_hutang_supplier',
        'supplier',
        'id_supplier',
        'total_hutang_supplier',
        'sisa_hutang_supplier',
        'keterangan_hutang_supplier',
        'status_hutang_supplier'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function pembelian()
    {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function count_all_mutasi_supplier($tanggal_start, $tanggal_end, $wilayah, $supplier){
        $count = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier',
                        DB::raw('SUM(IF( mutasiSupplier.`status_hutang` = "lunas", 
                                            IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'" AND mutasiSupplier.`tanggal_hutang` NOT BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) , 
                                            IF(mutasiSupplier.tgl_po < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)
                                        )) - SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) AS saldo_awal'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_po BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier2.rata, 0)) AS pembelian'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) ) AS bayar'),
                        DB::raw('SUM( IF(mutasiSupplier.`tanggal_return` BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier.`harga_return`, 0) ) AS retur')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->groupBy('mutasiSupplier.id_supplier');

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $count->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $count->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }

        return $count->get();
    }
    public function count_filter_mutasi_supplier($query,$view, $tanggal_start, $tanggal_end, $wilayah, $supplier){

        $count = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier',
                        DB::raw('SUM(IF( mutasiSupplier.`status_hutang` = "lunas", 
                                            IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'" AND mutasiSupplier.`tanggal_hutang` NOT BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) , 
                                            IF(mutasiSupplier.tgl_po < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)
                                        )) - SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) AS saldo_awal'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_po BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier2.rata, 0)) AS pembelian'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) ) AS bayar'),
                        DB::raw('SUM( IF(mutasiSupplier.`tanggal_return` BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier.`harga_return`, 0) ) AS retur')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->groupBy('mutasiSupplier.id_supplier');
        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $count->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $count->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }

        return $count->get();
    }

    public function list_mutasi_supplier($start,$length,$query,$view, $tanggal_start, $tanggal_end, $wilayah, $supplier){
        $data = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier',
                        DB::raw('SUM(IF( mutasiSupplier.`status_hutang` = "lunas", 
                                            IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'" AND mutasiSupplier.`tanggal_hutang` NOT BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) , 
                                            IF(mutasiSupplier.tgl_po < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)
                                        )) - SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) AS saldo_awal'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_po BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier2.rata, 0)) AS pembelian'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) ) AS bayar'),
                        DB::raw('SUM( IF(mutasiSupplier.`tanggal_return` BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier.`harga_return`, 0) ) AS retur')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->groupBy('mutasiSupplier.id_supplier');
        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $data->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $data->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }

        return $data->get();
    }

    public function export_mutasi_supplier($tanggal_start, $tanggal_end, $wilayah, $supplier){
        $data = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier',
                        DB::raw('SUM(IF( mutasiSupplier.`status_hutang` = "lunas", 
                                            IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'" AND mutasiSupplier.`tanggal_hutang` NOT BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) , 
                                            IF(mutasiSupplier.tgl_po < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)
                                        )) - SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_start.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) AS saldo_awal'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_po BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier2.rata, 0)) AS pembelian'),
                        DB::raw('SUM( IF(mutasiSupplier.tgl_payment_hutang BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'",mutasiSupplier.jumlah_payment, 0) ) AS bayar'),
                        DB::raw('SUM( IF(mutasiSupplier.`tanggal_return` BETWEEN "'.$tanggal_start.'" AND "'.$tanggal_end.'", mutasiSupplier.`harga_return`, 0) ) AS retur')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->groupBy('mutasiSupplier.id_supplier');

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $data->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $data->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }

        return $data->get();
    }

    public function count_all_saldo_supplier($tanggal_filter, $wilayah, $supplier){
         $count = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier', 'tb_wilayah.wilayah',
                        DB::raw('SUM(IF(mutasiSupplier.tgl_po < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)) 
                                       - 
                                       SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) 
                                       AS saldo')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->leftJoin('tb_wilayah', 'mutasiSupplier.wilayah', '=', 'tb_wilayah.id')
                    ->groupBy('mutasiSupplier.id_supplier');

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $count->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $count->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }

        return $count->get();
    }
    public function count_filter_saldo_supplier($query,$view, $tanggal_filter, $wilayah, $supplier){

        $count = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier', 'tb_wilayah.wilayah',
                        DB::raw('SUM(IF(mutasiSupplier.tgl_po < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)) 
                                       - 
                                       SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) 
                                       AS saldo')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->leftJoin('tb_wilayah', 'mutasiSupplier.wilayah', '=', 'tb_wilayah.id')
                    ->groupBy('mutasiSupplier.id_supplier');
        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $count->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $count->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }
        return $count->get();
    }

    public function list_saldo_supplier($start,$length,$query,$view, $tanggal_filter, $wilayah, $supplier){
        $data = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier', 'tb_wilayah.wilayah',
                        DB::raw('SUM(IF(mutasiSupplier.tgl_po < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)) 
                                       - 
                                       SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) 
                                       AS saldo')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->leftJoin('tb_wilayah', 'mutasiSupplier.wilayah', '=', 'tb_wilayah.id')
                    ->groupBy('mutasiSupplier.id_supplier');
        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $data->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $data->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_saldo_supplier($tanggal_filter, $wilayah, $supplier){
        $data = DB::table('mutasiSupplier')
                    ->select('mutasiSupplier.id_hutang', 'mutasiSupplier.id_supplier', 'mutasiSupplier.kode_supplier', 'mutasiSupplier.supplier', 'tb_wilayah.wilayah',
                        DB::raw('SUM(IF(mutasiSupplier.tgl_po < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas", mutasiSupplier2.`rata`, 0)) 
                                       - 
                                       SUM(IF(mutasiSupplier.tgl_payment_hutang < "'.$tanggal_filter.'" AND mutasiSupplier.`status_hutang` = "belum_lunas",mutasiSupplier.jumlah_payment, 0)) 
                                       AS saldo')
                    )
                    ->leftJoin('mutasiSupplier2', 'mutasiSupplier.id_hutang','=','mutasiSupplier2.id_hutang')
                    ->leftJoin('tb_wilayah', 'mutasiSupplier.wilayah', '=', 'tb_wilayah.id')
                    ->groupBy('mutasiSupplier.id_supplier');

        if($wilayah != null){
            if (!empty($wilayah)){
                if (!in_array(0, $wilayah)){
                    $data->whereIn("mutasiSupplier.wilayah", $wilayah);
                }
            }
        }

        if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $data->whereIn("mutasiSupplier.id_supplier", $supplier);
                }
            }
        }
        return $data->get();
    }



}
