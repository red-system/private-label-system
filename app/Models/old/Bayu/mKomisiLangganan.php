<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Main;

class mKomisiLangganan extends Model
{
    protected $table = 'tb_komisi_langganan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'minimal_pembelian',
        'id_satuan',
        'komisi_persen',
        'komisi_nominal',
        'created_at',
        'updated_at',
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all($id){
        $count = DB::table($this->table)
            ->select($this->table.".*", 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_golongan.golongan', 'tb_satuan.satuan')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->where($this->table.'.id_langganan', $id);
        return $count->count();
    }

    public function count_filter($query,$view,$id){
        $count = DB::table($this->table)
            ->select($this->table.".*", 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_golongan.golongan', 'tb_satuan.satuan')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->where($this->table.'.id_langganan', $id);
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }

    public function list($start,$length,$query,$view,$id){
        $data = DB::table($this->table)
            ->select($this->table.".*", 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_golongan.golongan', 'tb_satuan.satuan')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->where($this->table.'.id_langganan', $id);

        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_by_langganan($id){
        $count = DB::table($this->table)
            ->select($this->table.".*", 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_golongan.golongan', 'tb_satuan.satuan')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
            ->where($this->table.'.id_langganan', $id);
        return $count->count('tb_barang.kode_barang');
    }

    public function count_filter_by_langganan($query,$view,$id){
        $count = DB::table($this->table)
            ->select($this->table.".*", 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_golongan.golongan', 'tb_satuan.satuan')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
            ->where($this->table.'.id_langganan', $id);
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count('tb_barang.kode_barang');
    }

    public function list_by_langganan($start,$length,$query,$view,$id){
        $data = DB::table($this->table)
            ->select($this->table.".*", 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_golongan.golongan', 'tb_satuan.satuan')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'tb_barang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
            ->where($this->table.'.id_langganan', $id)
            ->orderBy($this->table.'.id', 'asc');

        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
