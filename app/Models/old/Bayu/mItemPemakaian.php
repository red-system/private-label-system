<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mItemPemakaian extends Model
{
    protected $table = 'tb_item_pemakaian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_pemakaian',
        'id_stok',
        'jml_barang',
        'satuan',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    
    public function count_all(){
        $count =  DB::table($this->table);
        return $count->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table);

        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*");
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countIdPemakaian($id){
        $data = DB::table($this->table)
                    ->select($this->table.'.*')
                    ->where($this->table.'.id_pemakaian', $id);

        return $data->count();
    }
}
