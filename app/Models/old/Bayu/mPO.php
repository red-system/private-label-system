<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPO extends Model
{
    protected $table = 'tb_po';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_supplier',
        'id_lokasi',
        'id_user',
        'status_pembelian',
        'status_pembelian_date',
        'no_po',
        'no_po_label',
        'tgl_po',
        'tgl_kirim_po',
        'keterangan_po',
        'total_po',
        'discount_persen_po',
        'discount_nominal_po',
        'pajak_persen_po',
        'pajak_nominal_po',
        'ongkos_kirim_po',
        'grand_total_po',
        'dp_po',
        'metode_pembayaran_dp_po',
        'sisa_pembayaran_po',
        'created_at',
        'updated_at'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function lokasi()
    {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function count_all_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table($this->table)
                    ->select($this->table.'.no_po_label as no_faktur', $this->table.'.id_supplier', 'tb_supplier.supplier', $this->table.'.tgl_po as tanggal', 'tb_lokasi.lokasi',
                        DB::raw('tb_po.`grand_total_po` AS nilai'),
                        DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                        DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
                    )
                    ->leftJoin('tb_supplier', $this->table.'.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_pembelian', $this->table.'.id','=','tb_pembelian.id_po')
                    ->leftJoin('tb_hutang_supplier',$this->table.'.id' ,'=','tb_hutang_supplier.id_po')
                    ->leftJoin('tb_hutang_giro','tb_pembelian.id','=','tb_hutang_giro.id_pembelian')
                    ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                    ->orderBy($this->table.'.no_po_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween($this->table.'.tgl_po', $date_where);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $count->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $count->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }

        return $count->get();
    }
    public function count_filter_laporan($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table($this->table)
                    ->select($this->table.'.no_po_label as no_faktur', $this->table.'.id_supplier', 'tb_supplier.supplier', $this->table.'.tgl_po as tanggal', 'tb_lokasi.lokasi',
                        DB::raw('tb_po.`grand_total_po` AS nilai'),
                        DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                        DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
                    )
                    ->leftJoin('tb_supplier', $this->table.'.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_pembelian', $this->table.'.id','=','tb_pembelian.id_po')
                    ->leftJoin('tb_hutang_supplier',$this->table.'.id' ,'=','tb_hutang_supplier.id_po')
                    ->leftJoin('tb_hutang_giro','tb_pembelian.id','=','tb_hutang_giro.id_pembelian')
                    ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                    ->orderBy($this->table.'.no_po_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween($this->table.'.tgl_po', $date_where);
        }
        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $count->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $count->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $count->get();
    }

    public function list_laporan($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table($this->table)
                    ->select($this->table.'.no_po_label as no_faktur', $this->table.'.id_supplier', 'tb_supplier.supplier', $this->table.'.tgl_po as tanggal', 'tb_lokasi.lokasi',
                        DB::raw('tb_po.`grand_total_po` AS nilai'),
                        DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                        DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
                    )
                    ->leftJoin('tb_supplier', $this->table.'.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_pembelian', $this->table.'.id','=','tb_pembelian.id_po')
                    ->leftJoin('tb_hutang_supplier',$this->table.'.id' ,'=','tb_hutang_supplier.id_po')
                    ->leftJoin('tb_hutang_giro','tb_pembelian.id','=','tb_hutang_giro.id_pembelian')
                    ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                    ->orderBy($this->table.'.no_po_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween($this->table.'.tgl_po', $date_where);
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $data->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $data->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table($this->table)
                    ->select($this->table.'.no_po_label as no_faktur', $this->table.'.id_supplier', 'tb_supplier.supplier', $this->table.'.tgl_po as tanggal', 'tb_lokasi.lokasi',
                        DB::raw('tb_po.`grand_total_po` AS nilai'),
                        DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                        DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
                    )
                    ->leftJoin('tb_supplier', $this->table.'.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_pembelian', $this->table.'.id','=','tb_pembelian.id_po')
                    ->leftJoin('tb_hutang_supplier',$this->table.'.id' ,'=','tb_hutang_supplier.id_po')
                    ->leftJoin('tb_hutang_giro','tb_pembelian.id','=','tb_hutang_giro.id_pembelian')
                    ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                    ->orderBy($this->table.'.no_po_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween($this->table.'.tgl_po', $date_where);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $data->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $data->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function count_all_detail_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table($this->table)
                    ->select($this->table.'.id','tb_po.no_po_label as no_faktur','tb_po.id_supplier', 'tb_supplier.supplier', 'tb_po.tgl_po as tanggal', 'tb_lokasi.lokasi', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
                    ->leftJoin('tb_pembelian', 'tb_pembelian.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po','=','tb_po.id')
                    ->leftJoin('tb_hutang_giro','tb_hutang_giro.id_pembelian','=','tb_pembelian.id')
                    ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
                    ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=','tb_pembelian.id')
                    ->leftJoin('tb_po_barang','tb_pembelian_barang.id_po_barang', '=','tb_po_barang.id')
                    ->leftJoin('tb_barang', 'tb_po_barang.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
                    ->groupBy($this->table.'.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween('tb_po.tgl_po', $date_where);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $count->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $count->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }

        return $count->get();
    }
    public function count_filter_detail_laporan($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table($this->table)
                    ->select('tb_po.no_po_label as no_faktur','tb_po.id_supplier', 'tb_supplier.supplier', 'tb_po.tgl_po as tanggal', 'tb_lokasi.lokasi', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
                    ->leftJoin('tb_pembelian', 'tb_pembelian.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po','=','tb_po.id')
                    ->leftJoin('tb_hutang_giro','tb_hutang_giro.id_pembelian','=','tb_pembelian.id')
                    ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
                    ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=','tb_pembelian.id')
                    ->leftJoin('tb_po_barang','tb_pembelian_barang.id_po_barang', '=','tb_po_barang.id')
                    ->leftJoin('tb_barang', 'tb_po_barang.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
                    ->groupBy($this->table.'.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween('tb_po.tgl_po', $date_where);
        }
        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $count->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $count->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $count->get();
    }

    public function list_detail_laporan($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table($this->table)
                    ->select('tb_po.no_po_label as no_faktur','tb_po.id_supplier', 'tb_supplier.supplier', 'tb_po.tgl_po as tanggal', 'tb_lokasi.lokasi', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
                    ->leftJoin('tb_pembelian', 'tb_pembelian.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po','=','tb_po.id')
                    ->leftJoin('tb_hutang_giro','tb_hutang_giro.id_pembelian','=','tb_pembelian.id')
                    ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
                    ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=','tb_pembelian.id')
                    ->leftJoin('tb_po_barang','tb_pembelian_barang.id_po_barang', '=','tb_po_barang.id')
                    ->leftJoin('tb_barang', 'tb_po_barang.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
                    ->groupBy($this->table.'.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('tb_po.tgl_po', $date_where);
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $data->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $data->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_detail_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table($this->table)
                    ->select($this->table.'.id','tb_po.no_po_label as no_faktur','tb_po.id_supplier', 'tb_supplier.supplier', 'tb_po.tgl_po as tanggal', 'tb_lokasi.lokasi', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
                    ->leftJoin('tb_pembelian', 'tb_pembelian.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po','=','tb_po.id')
                    ->leftJoin('tb_hutang_giro','tb_hutang_giro.id_pembelian','=','tb_pembelian.id')
                    ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
                    ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=','tb_pembelian.id')
                    ->leftJoin('tb_po_barang','tb_pembelian_barang.id_po_barang', '=','tb_po_barang.id')
                    ->leftJoin('tb_barang', 'tb_po_barang.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
                    ->groupBy($this->table.'.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('tb_po.tgl_po', $date_where);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $data->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $data->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_detail_barang_laporan($tanggal_start, $tanggal_end, $select_lokasi, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table($this->table)
                    ->select($this->table.'.id as id_pembelian','tb_po.no_po_label as no_faktur','tb_po.id_supplier', 'tb_supplier.supplier', 'tb_po.tgl_po as tanggal', 'tb_lokasi.lokasi',  'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_po_barang.qty', 'tb_satuan.satuan', 'tb_po_barang.harga', 'tb_po_barang.discount as discount_barang','tb_po_barang.sub_total', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
                    ->leftJoin('tb_pembelian', 'tb_po.id', '=','tb_pembelian.id_po' )
                    ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=','tb_supplier.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_po.id','=','tb_hutang_supplier.id_po')
                    ->leftJoin('tb_hutang_giro','tb_pembelian.id','=','tb_hutang_giro.id_pembelian')
                    ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
                    ->leftJoin('tb_po_barang','tb_po.id', '=','tb_po_barang.id_po')
                    ->leftJoin('tb_barang', 'tb_po_barang.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok', 'tb_po_barang.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
                    ->groupBy($this->table.'.id', 'tb_barang.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('tb_po.tgl_po', $date_where);
        }

        if($select_lokasi != null){
            if (!empty($select_lokasi)){
                if (!in_array(0, $select_lokasi)){
                    $data->whereIn($this->table.".id_lokasi", $select_lokasi);
                }
            }
        }

        if($select_supplier != null){
            if (!empty($select_supplier)){
                if (!in_array(0, $select_supplier)){
                    $data->whereIn($this->table.".id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }
}
