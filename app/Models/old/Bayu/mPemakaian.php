<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Main;

class mPemakaian extends Model
{
    protected $table = 'tb_pemakaian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_pemakaian',
        'tgl_pemakaian',
        'id_lokasi',
        'keterangan',
        'created_at',
        'updated_at',
    ];

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all(){
        $count = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id');
        return $count->count();
    }

    public function count_filter($query,$view){
        $count = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }

    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id');

        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function getIdByNoAndLocation($no, $location){
        $data = DB::table($this->table)
                    ->select($this->table.'.id')
                    ->where([
                        [$this->table.'.no_pemakaian',$no],
                        [$this->table.'.id_lokasi',$location]
                    ])
                    ->orderBy('id','desc');
        return $data->first();
    }
}
