<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mItemAssembly extends Model
{
    protected $table = 'tb_assembly_item';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_assembly',
        'id_barang',
        'jumlah',
        'id_satuan',
        'satuan',
        'created_at',
        'updated_at',
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
