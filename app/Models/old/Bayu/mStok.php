<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mStok extends Model
{
    protected $table = 'tb_stok';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_lokasi',
        'jml_barang',
        'id_satuan',
        'hpp',
        'id_supplier',
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    public function count_all_by_product($id){
        return DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->where('id_barang', '=', $id)
            ->count();
    }
    public function count_filter_by_product($id, $query,$view){

        $count = DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->where('id_barang', '=', $id);
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list_by_product($id, $start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*","tb_barang.nama_barang", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_satuan.satuan", "tb_supplier.supplier")
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->where('id_barang', '=', $id);
        $data -> where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_by_location($id){
        // if (empty($id) || $id == 0) {
        //     return DB::table($this->table)
        //     ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
        //     ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
        //     ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
        //     ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
        //     ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
        //     ->join('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
        //     ->count();
        // } else {
        return DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->join('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.id_lokasi', $id)
            ->count();
        // }
    }
    public function count_filter_by_location($id, $query,$produkList){
        // if (empty($id) || $id == 0) {
        //     $count = DB::table($this->table)
        //     ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
        //     ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
        //     ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
        //     ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
        //     ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
        //     ->join('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id');
        // } else {
        $count = DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->join('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.id_lokasi', $id);
        // }

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        return $count->count();
    }
    public function list_by_location($id, $start,$length,$query,$produkList){
        // if (empty($id) || $id == 0) {
        // $data = DB::table($this->table)
        //     ->select("tb_barang.kode_barang", "tb_barang.nama_barang", "tb_golongan.golongan")
        //     ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
        //     ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
        //     ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
        //     ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
        //     ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
        //     ->join('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id');  
        // } else {
        $data = DB::table($this->table)
            ->select("tb_barang.kode_barang", "tb_barang.nama_barang", "tb_golongan.golongan")
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_satuan',$this->table.'.id_satuan','=','tb_satuan.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->join('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.id_lokasi', $id);
        // }
        $data -> where(function($qry) use ($produkList,$query){

            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all(){
        $count =  DB::table($this->table, 'tb_lokasi.lokasi')
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id');
        $cek = session()->get('id_location');
        if($cek != null){
            $count->where(["tb_lokasi.id"=>$cek]);
        }
        return $count->count();    
            
    }

    public function count_filter($query,$produkList){
        $count = DB::table($this->table, 'tb_lokasi.lokasi')
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id');

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        $cek = session()->get('id_location');
        if($cek != null){
            $count->where(["tb_lokasi.id"=>$cek]);
        }

        return $count->count();
    }

    public function list_all($start,$length,$query,$produkList){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang", "tb_golongan.golongan")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0'); 

        $data -> where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        $cek = session()->get('id_location');
        if($cek != null){
            $data->where(["tb_lokasi.id"=>$cek]);
        }
        return $data->get(); 
    }

    public function count_all_expired($tanggal_expired, $golongan, $lokasi, $supplier){
        $count =  DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang", "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0');


        if ($tanggal_expired != null){
            $count->where($this->table.'.date_expired', '<=', $tanggal_expired);
        }
        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan)){
                    $count->whereIn("tb_barang.id_golongan", $golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi )){
                    $count->whereIn("tb_stok.id_lokasi",$lokasi);
                }
            }
        }
        if($supplier != null){
            $count->where("tb_stok.id_supplier", $supplier);
        }

        return $count->count();
    }

    public function count_filter_expired($query,$produkList,$tanggal_expired, $golongan, $lokasi, $supplier){
        $count = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0');

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        if ($tanggal_expired != null){
            $count->where('date_expired', '<=', $tanggal_expired);
        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan)){
                    $count->whereIn("tb_barang.id_golongan", $golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi )){
                    $count->whereIn("tb_stok.id_lokasi",$lokasi);
                }
            }
        }
        if($supplier != null){
            $count->where("tb_stok.id_supplier", $supplier);
        }

        return $count->count();
    }

    public function list_expired($start,$length,$query,$produkList,$tanggal_expired, $golongan, $lokasi, $supplier){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.harga_jual_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0')
            ->orderBy('date_expired', 'asc');

        $data -> where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if ($tanggal_expired != null){
            $data->where('date_expired', '<=', $tanggal_expired);
        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan )){
                    $data->whereIn("tb_barang.id_golongan",$golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi )){
                    $data->whereIn("tb_stok.id_lokasi",$lokasi);
                }
            }
        }
        if($supplier != null){
            $data->where("tb_stok.id_supplier", $supplier);
        }
        return $data->get();
    }

    public function export_expired($tanggal_expired, $golongan, $lokasi, $supplier){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.harga_jual_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0')
            ->where('date_expired', '<=', $tanggal_expired)
            ->orderBy('date_expired', 'asc');

        if($golongan != null){
            if (!empty($golongan)) {
                if (!in_array(0, $golongan)){
                    $data->whereIn("tb_barang.id_golongan",$golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi )){
                    $data->whereIn("tb_stok.id_lokasi",$lokasi);
                }
            }
        }
        if($supplier != null){
            $data->where("tb_stok.id_supplier", $supplier);
        }
        return $data->get();
    }

    public function count_all_akan_expired($golongan, $lokasi, $supplier, $interval){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime('+1 day', strtotime(date('d-m-Y'))))),
            Main::format_date_db(date('d-m-Y', strtotime($interval.' month', strtotime(date('d-m-Y')))))
        ];

//        dd($where_date);
        $count =  DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang", "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0')
            ->whereBetween($this->table.'.date_expired', $where_date);


//        if ($tanggal_expired != null){
//            $count->where($this->table.'.date_expired', '<=', $tanggal_expired);
//        }
        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan)){
                    $count->whereIn("tb_barang.id_golongan", $golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $count->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }
        if($supplier != null){
            $count->where("tb_stok.id_supplier", $supplier);
        }

        return $count->count();
    }

    public function count_filter_akan_expired($query,$produkList, $golongan, $lokasi, $supplier, $interval){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime('+1 day', strtotime(date('d-m-Y'))))),
            Main::format_date_db(date('d-m-Y', strtotime($interval.' month', strtotime(date('d-m-Y')))))
        ];

        $count = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0')
            ->whereBetween($this->table.'.date_expired', $where_date);

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

//        if ($tanggal_expired != null){
//            $count->where('date_expired', '<=', $tanggal_expired);
//        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan)){
                    $count->whereIn("tb_barang.id_golongan", $golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $count->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }
        if($supplier != null){
            $count->where("tb_stok.id_supplier", $supplier);
        }

        return $count->count();
    }

    public function list_akan_expired($start,$length,$query,$produkList, $golongan, $lokasi, $supplier, $interval){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime('+1 day', strtotime(date('d-m-Y'))))),
            Main::format_date_db(date('d-m-Y', strtotime($interval.' month', strtotime(date('d-m-Y')))))
        ];

        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.harga_jual_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0')
            ->whereBetween($this->table.'.date_expired', $where_date)
            ->orderBy('date_expired', 'asc');

        $data -> where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

//        if ($tanggal_expired != null){
//            $data->where('date_expired', '<=', $tanggal_expired);
//        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan )){
                    $data->whereIn("tb_barang.id_golongan",$golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $count->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }
        if($supplier != null){
            $data->where("tb_stok.id_supplier", $supplier);
        }
        return $data->get();
    }

    public function export_akan_expired($golongan, $lokasi, $supplier, $interval){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime('+1 day', strtotime(date('d-m-Y'))))),
            Main::format_date_db(date('d-m-Y', strtotime($interval.' month', strtotime(date('d-m-Y')))))
        ];

        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.harga_jual_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','!=','0')
            ->whereBetween($this->table.'.date_expired', $where_date)
            ->orderBy('date_expired', 'asc');

        if($golongan != null){
            if (!empty($golongan)) {
                if (!in_array(0, $golongan)){
                    $data->whereIn("tb_barang.id_golongan",$golongan);
                }
            }
        }
        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $count->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }
        if($supplier != null){
            $data->where("tb_stok.id_supplier", $supplier);
        }
        return $data->get();
    }

    public function count_all_zero(){
        $count =  DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang", "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','=','0');


        return $count->count();
    }

    public function count_filter_zero($query,$produkList){
        $count = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','=','0');

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        return $count->count();
    }

    public function list_zero($start,$length,$query,$produkList){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.harga_jual_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','==','0');

        $data -> where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }


        return $data->get();
    }

    public function export_zero(){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.id as id_barang", "tb_barang.harga_beli_terakhir_barang", "tb_barang.harga_jual_barang", "tb_barang.kode_barang", "tb_barang.nama_barang",  "tb_barang.id_golongan", "tb_golongan.golongan", "tb_lokasi.lokasi")
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->where($this->table.'.jml_barang','==','0');


        return $data->get();
    }

    public function idTujuan($idBarang, $idLokasi){
        $data = DB::table($this->table)
                    ->select($this->table.'.id')
                    ->where([
                        [$this->table.'.id_barang',$idBarang],
                        [$this->table.'.id_lokasi',$idLokasi]
                        ]);
        return $data->first();
    }

    public function penguranganJml($id, $qty){
//        dd($qty);
        $data = DB::table($this->table)
                    ->select(DB::raw('(tb_stok.jml_barang - '.$qty.') as pengurangan'))
                    ->where($this->table.'.id', $id);
        return $data->first();
    }

    public function penjumlahanJml($idStok, $qty){
        $data = DB::table($this->table)
                    ->select(DB::raw('(tb_stok.jml_barang + '.$qty.') as penjumlahan'))
                    ->where($this->table.'.id',$idStok);
        return $data->first();
    }

    public function countStokByBarangAndLocation($idBarang, $idLokasi){
        $data = DB::table($this->table)
                    ->select($this->table.'.id')
                    ->where([
                        [$this->table.'.id_barang', $idBarang],
                        [$this->table.'.id_lokasi', $idLokasi]
                    ]);
        return $data->count();
    }

    public function getNewStok($idBarang, $idLokasi, $idSupplier){
        $data = DB::table($this->table)
                    ->select($this->table.'.*')
                    ->where([
                        [$this->table.'.id_barang',$idBarang],
                        [$this->table.'.id_lokasi',$idLokasi],
                        [$this->table.'.id_supplier',$idSupplier]
                    ])
                    ->orderBy('id','desc');
        return $data->first();
    }

    public function getDataById($idStok){
        $data = DB::table($this->table)
                    ->select($this->table.'.*')
                    ->where($this->table.'.id',$idStok);
        return $data->first();
    }

    public function getHppById($idStok){
        $data = DB::table($this->table)
                    ->select($this->table.'.hpp')
                    ->where($this->table.'.id',$idStok);
        return $data->first();
    }

    public function count_all_stokOpname(){
        $count =  DB::table('totalStokBarang2'); 
        return $count->count();    
            
    }

    public function count_filter_stokOpname($query,$produkList){
        $count = DB::table('totalStokBarang2');

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });


        return $count->count();
    }

    public function list_stokOpname($start,$length,$query,$produkList){
        $data = DB::table('totalStokBarang2'); 

        $data -> where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get(); 
    }

    public function filterLokasiStokOpname($idBarang){
        $data = DB::table($this->table)
                    ->select('tb_barang.id as id_barang', $this->table.'.id as id_stok', $this->table.'.jml_barang', 'tb_lokasi.id as id_lokasi', 'tb_lokasi.lokasi')
                    ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
                    ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                    ->where($this->table.'.id_barang',$idBarang)
                    ->groupBy($this->table.'.id_lokasi');
        return $data->get();
    }
}
