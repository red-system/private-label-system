<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Main;
use function foo\func;

class mItemPemindahanStok extends Model
{
    protected $table = 'tb_item_pemindahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_pemindahan',
        'id_stok',
        'jml_barang',
        'id_satuan',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    
    public function count_all(){
        $count =  DB::table($this->table);
        return $count->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table);

        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*");
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function getDataItemPemindahan($id){
        $data = DB::table($this->table)
                    ->select($this->table.'.*', 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_stok.id_barang', 'tb_stok.jml_barang as jml_stok', 'tb_stok.id_supplier', 'tb_satuan.satuan')
                    ->leftJoin('tb_satuan', $this->table.'.id_satuan', '=', 'tb_satuan.id')
                    ->leftJoin('tb_pemindahan', $this->table.'.id_pemindahan', '=', 'tb_pemindahan.id')
                    ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
                    ->leftJoin('tb_barang', 'tb_stok.id_barang', '=', 'tb_barang.id')
                    ->where($this->table.'.id_pemindahan', $id);
        return $data->get();
    }
}
