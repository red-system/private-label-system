<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mKaryawan extends Model
{
    protected $table = 'tb_karyawan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_karyawan',
        'nama_karyawan',
        'alamat_karyawan',
        'telp_karyawan',
        'posisi_karyawan',
        'email_karyawan',
        'foto_karyawan'
    ];
    public function user() {
        return $this->hasMany(mUser::class, 'id');
    }
    public function count_all(){
        return DB::table($this->table)->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table);
        foreach ($view as $value){
            $count->orWhere($value['search_field'],'like','%'.$query.'%');
        }
        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table);
        foreach ($view as $value){
            $data->orWhere($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
