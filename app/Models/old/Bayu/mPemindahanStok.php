<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPemindahanStok extends Model
{
    protected $table = 'tb_pemindahan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_pemindahan',
        'tgl_pemindahan',
        'asal_barang',
        'tujuan_barang',
        'keterangan',
        'status',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    
    public function count_all(){
        $count =  DB::table($this->table)
                    ->select($this->table.".id",$this->table.".no_pemindahan",$this->table.".tgl_pemindahan",$this->table.".asal_barang",'l1.lokasi as asal',$this->table.".tujuan_barang",'l2.lokasi as tujuan',$this->table.".keterangan",$this->table.".status")
                    ->leftJoin('tb_lokasi as l1', $this->table.'.asal_barang', '=', 'l1.id')
                    ->leftJoin('tb_lokasi as l2', $this->table.'.tujuan_barang', '=', 'l2.id');
        return $count->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table)
                    ->select($this->table.".id",$this->table.".no_pemindahan",$this->table.".tgl_pemindahan",$this->table.".asal_barang",'l1.lokasi as asal',$this->table.".tujuan_barang",'l2.lokasi as tujuan',$this->table.".keterangan",$this->table.".status")
                    ->leftJoin('tb_lokasi as l1', $this->table.'.asal_barang', '=', 'l1.id')
                    ->leftJoin('tb_lokasi as l2', $this->table.'.tujuan_barang', '=', 'l2.id');

        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
                    ->select($this->table.".id",$this->table.".no_pemindahan",$this->table.".tgl_pemindahan",$this->table.".asal_barang",'l1.lokasi as asal',$this->table.".tujuan_barang",'l2.lokasi as tujuan',$this->table.".keterangan",$this->table.".status")
                    ->leftJoin('tb_lokasi as l1', $this->table.'.asal_barang', '=', 'l1.id')
                    ->leftJoin('tb_lokasi as l2', $this->table.'.tujuan_barang', '=', 'l2.id');
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function noTerakhir(){
        $data = DB::table($this->table)
                    ->select($this->table.".id")
                    ->orderBy($this->table.".id", 'desc');
        return $data->first();
    }

    public function list_by_pemindahan_stok($id){
        $data = DB::table($this->table)
            ->select($this->table.".*")
            ->where($this->table.".id", '=', $id);
        
        
        return $data->first();
    }
}
