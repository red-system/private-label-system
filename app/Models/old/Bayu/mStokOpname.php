<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Main;

class mStokOpname extends Model
{
    protected $table = 'tb_stok_opname';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_barang',
        'nama_barang',
        'id_lokasi',
        'tgl_opname',
        'qty_data',
        'satuan_qty_data',
        'qty_fisik',
        'satuan_qty_fisik',
        'selisih_qty',
        'selisih_satuan',
        'keterangan',
        'created_at',
        'updated_at',
    ];

    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all(){
        $count = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id');
        return $count->count();
    }

    public function count_filter($query,$view){
        $count = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }

    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id');

        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function getNewId($kodeBarang, $idLokasi, $tglOpname){
        $data = DB::table($this->table)
                    ->select($this->table.'.id')
                    ->where([
                        [$this->table.'.kode_barang', $kodeBarang],
                        [$this->table.'.id_lokasi', $idLokasi],
                        [$this->table.'.tgl_opname', $tglOpname],
                    ]);
        return $data->first();
    }
}
