<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mItemPenjualan extends Model
{
    protected $table = 'tb_item_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_penjualan',
        'id_barang',
        'id_stok',
        'nama_barang_penjualan',
        'jml_barang_penjualan',
        'golongan_penjualan',
        'satuan_penjualan',
        'harga_penjualan',
        'disc_persen_penjualan',
        'disc_nominal',
        'subtotal',
        'created_at',
        'updated_at',
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    function stok() {
        return $this->belongsTo(mStok::class, 'id_stok');
    }

    function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all($tanggal_out, $golongan, $lokasi, $supplier){
        $count =  DB::table($this->table)
            ->select($this->table.".*", "tb_barang.kode_barang", "tb_item_penjualan.nama_barang_penjualan as nama_barang",  "tb_barang.id_golongan", "tb_item_penjualan.golongan_penjualan as golongan", "tb_lokasi.lokasi", "tb_supplier.supplier", DB::raw('SUM(jml_barang_penjualan) AS total_penjualan_barang'))
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi','tb_stok.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->groupBy($this->table.'.id_barang');

        if ($tanggal_out != null){
            $count->whereYear($this->table.'.created_at', '=', date('Y', strtotime($tanggal_out)));
            $count->whereMonth($this->table.'.created_at', '=',date('m', strtotime($tanggal_out)));
        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan)){
                    $count->whereIn("tb_barang.id_golongan", $golongan);
                }
            }
        }

        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $count->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }

        if($supplier != null){
            $count->where("tb_stok.id_supplier", $supplier);
        }

        return $count->get();
    }

    public function count_filter($query,$produkList,$tanggal_out, $golongan, $lokasi, $supplier){
        $count = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.kode_barang", "tb_item_penjualan.nama_barang_penjualan as nama_barang",  "tb_barang.id_golongan", "tb_item_penjualan.golongan_penjualan as golongan", "tb_lokasi.lokasi", "tb_supplier.supplier", DB::raw('SUM(jml_barang_penjualan) AS total_penjualan_barang'))
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi','tb_stok.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->groupBy($this->table.'.id_barang');

        $count->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orHaving($value['search_field'],'like','%'.$query.'%');
            }
        });

        if ($tanggal_out != null){
            $count->whereYear($this->table.'.created_at', '=', date('Y', strtotime($tanggal_out)));
            $count->whereMonth($this->table.'.created_at', '=',date('m', strtotime($tanggal_out)));
        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan)){
                    $count->whereIn("tb_barang.id_golongan", $golongan);
                }
            }
        }

        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $count->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }

        if($supplier != null){
            $count->where("tb_stok.id_supplier", $supplier);
        }

        return $count->get();
    }

    public function take_all(){
        $data = DB::table($this->table)->select($this->table.'.*');

        return $data->get();
    }

    public function list($start,$length,$query,$produkList,$tanggal_out, $golongan, $lokasi, $supplier){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.kode_barang", "tb_item_penjualan.nama_barang_penjualan as nama_barang",  "tb_barang.id_golongan", "tb_item_penjualan.golongan_penjualan as golongan", "tb_stok.id_lokasi", "tb_lokasi.lokasi", "tb_supplier.supplier", DB::raw('SUM(jml_barang_penjualan) AS total_penjualan_barang'))
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi','tb_stok.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->groupBy($this->table.'.id_barang');

        $data -> where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value){
                $qry->orHaving($value['search_field'],'like','%'.$query.'%');
            }
        });

        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if ($tanggal_out != null){
            $data->whereYear($this->table.'.created_at', '=', date('Y', strtotime($tanggal_out)));
            $data->whereMonth($this->table.'.created_at', '=',date('m', strtotime($tanggal_out)));
        }

        if($golongan != null){
            if (!empty($golongan)){
                if (!in_array(0, $golongan )){
                    $data->whereIn("tb_barang.id_golongan",$golongan);
                }
            }
        }

        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $data->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }

        if($supplier != null){
            $data->where("tb_stok.id_supplier", $supplier);
        }
        return $data->get();
    }

    public function export($tanggal_out, $golongan, $lokasi, $supplier){
        $data = DB::table($this->table)
            ->select($this->table.".*", "tb_barang.kode_barang", "tb_item_penjualan.nama_barang_penjualan as nama_barang",  "tb_barang.id_golongan", "tb_item_penjualan.golongan_penjualan as golongan", "tb_stok.id_lokasi", "tb_lokasi.lokasi", "tb_supplier.supplier", DB::raw('SUM(jml_barang_penjualan) AS total_penjualan_barang'))
            ->leftJoin('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_lokasi','tb_stok.id_lokasi','=','tb_lokasi.id')
            ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->leftJoin('tb_golongan','tb_barang.id_golongan','=','tb_golongan.id')
            ->groupBy($this->table.'.id_barang');

        if ($tanggal_out != null){
            $data->whereYear($this->table.'.created_at', '=', date('Y', strtotime($tanggal_out)));
            $data->whereMonth($this->table.'.created_at', '=',date('m', strtotime($tanggal_out)));
        }

        if($golongan != null){
            if (!empty($golongan)) {
                if (!in_array(0, $golongan)){
                    $data->whereIn("tb_barang.id_golongan",$golongan);
                }
            }
        }

        if($lokasi != null){
            if (!empty($lokasi)){
                if (!in_array(0, $lokasi)){
                    $data->whereIn("tb_stok.id_lokasi", $lokasi);
                }
            }
        }

        if($supplier != null){
            $data->where("tb_stok.id_supplier", $supplier);
        }

        return $data->get();
    }
}
