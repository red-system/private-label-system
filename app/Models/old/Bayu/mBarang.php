<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBarang extends Model
{
    protected $table = 'tb_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_barang',
        'nama_barang',
        'id_golongan',
        'hpp',
        'harga_beli_terakhir_barang',
        'harga_jual_barang',
        'created_at',
        'updated_at',
    ];

    function golongan() {
        return $this->belongsTo(mGolongan::class, 'id_golongan');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    public function count_all(){
        $count = DB::table($this->table)
            ->select($this->table.".*",'perhitungan.total_stok','tb_golongan.golongan')
            ->join('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang');
        return $count->count();
    }
    public function count_filter($query,$view){
        $count = DB::table($this->table)
            ->select($this->table.".*",'perhitungan.total_stok', 'tb_golongan.golongan')
            ->join('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*",DB::raw('if((perhitungan.total_stok is null),0,perhitungan.total_stok) as total_stok'),'tb_golongan.golongan')
            ->join('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang');

        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_zero(){
        $count = DB::table($this->table)
            ->select($this->table.".*",'perhitungan.total_stok','tb_golongan.golongan', 'tb_stok.id_satuan', 'tb_stok.satuan')
            ->leftJoin('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang')
            ->leftJoin('tb_stok', $this->table.'.id', '=', 'tb_stok.id_barang')
            ->where('harga_jual_barang','=', '0');
        return $count->count();
    }

    public function count_filter_zero($query,$view){
        $count = DB::table($this->table)
            ->select($this->table.".*",'perhitungan.total_stok', 'tb_golongan.golongan', 'tb_stok.id_satuan', 'tb_stok.satuan')
            ->leftJoin('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang')
            ->leftJoin('tb_stok', $this->table.'.id', '=', 'tb_stok.id_barang')
            ->where('harga_jual_barang','=', '0');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list_zero($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*",DB::raw('if((perhitungan.total_stok is null),0,perhitungan.total_stok) as total_stok'),'tb_golongan.golongan', 'tb_stok.id_satuan', 'tb_stok.satuan')
            ->leftJoin('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang')
            ->leftJoin('tb_stok', $this->table.'.id', '=', 'tb_stok.id_barang')
            ->where('harga_jual_barang','=', '0');

        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function export_zero(){
        $data = DB::table($this->table)
            ->select($this->table.".*",DB::raw('if((perhitungan.total_stok is null),0,perhitungan.total_stok) as total_stok'),'tb_golongan.golongan', 'tb_stok.id_satuan', 'tb_stok.satuan')
            ->leftJoin('tb_golongan',$this->table.'.id_golongan','=','tb_golongan.id')
            ->leftJoin('perhitungan',$this->table.'.id','=','perhitungan.id_barang')
            ->leftJoin('tb_stok', $this->table.'.id', '=', 'tb_stok.id_barang')
            ->where('harga_jual_barang','=', '0');

        return $data->get();
    }


    public function stokBarang($idBarang){
        $data = DB::table('tb_stok')
            -> where('id_barang', '=', $idBarang);

        return $data->count();
    }

    public function listBarangLokasi($start,$length,$query,$produkList){
        $data = DB::table('tb_stok')
                ->select($this->table.".*",'tb_golongan.golongan')
                ->join($this->table, 'tb_stok.id_barang', '=', $this->table.'.id')
                ->join('tb_lokasi', 'tb_stok.id_lokasi', '=', 'tb_lokasi.id')
                ->join('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
                ->join('tb_supplier', 'tb_stok.id_supplier', '=', 'tb_supplier.id')
                ->join('tb_golongan', $this->table.'.id_golongan','=','tb_golongan.id');
        $data->where(function($qry) use ($produkList,$query){
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        
        return $data->get();
    }

    public function getDataById($idBarang){
        $data = DB::table($this->table)
                    ->select($this->table.'.*')
                    ->where($this->table.'.id',$idBarang);
        return $data->first();
    }

    public function barangDanSatuan(){
        $data = DB::table($this->table)
                    ->select($this->table.'.id as id_barang', $this->table.'.kode_barang', $this->table.'.nama_barang', 'tb_stok.id_satuan', 'tb_stok.satuan')
                    ->leftJoin('tb_stok', $this->table.'.id', '=', 'tb_stok.id_barang')
                    ->whereNotNull('tb_stok.id_satuan')
                    ->whereNotNull('tb_stok.satuan')
                    ->distinct();

        return $data->get();
    }

    public function count_all_assembly(){
        $count = DB::table('totalStokBarang2');
        return $count->count();
    }
    public function count_filter_assembly($query,$view){
        $count = DB::table('totalStokBarang2');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list_all_assembly($start,$length,$query,$view){
        $data = DB::table('totalStokBarang2');
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function count_all_penjualan(){
        $count = DB::table('totalPenjualanPerBarang');
        return $count->count();
    }
    public function count_filter_penjualan($query,$view){
        $count = DB::table('totalPenjualanPerBarang');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list_all_penjualan_terbesar($start,$length,$query,$view)
    {
        $data = DB::table('totalPenjualanPerBarang')->orderBy('total_jml_barang_penjualan', 'desc')->orderBy('total_subtotal', 'desc');
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function list_all_penjualan_terkecil($start,$length,$query,$view){
        $data = DB::table('totalPenjualanPerBarang')->orderBy('total_jml_barang_penjualan', 'asc')->orderBy('total_subtotal', 'asc');
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
