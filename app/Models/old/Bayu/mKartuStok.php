<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Main;
use function foo\func;

class mKartuStok extends Model
{
    protected $table = 'tb_kartu_stok';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_lokasi',
        'id_stok',
        'lokasi',
        'tgl_transit',
        'keterangan',
        'stok_awal',
        'hpp_awal',
        'stok_masuk',
        'hpp_masuk',
        'stok_keluar',
        'hpp_keluar',
        'stok_akhir',
        'sst_sprl',
        'hpp_stok',
        'id_produksi',
        'id_pemindahan',
        'id_pemakaian',
        'id_stokOpname',
        'id_pembelian',
        'id_po',
        'created_at',
        'updated_at',
    ];

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }
    function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }
    function stok() {
        return $this->belongsTo(mStok::class, 'id_stok');
    }
    function produksi() {
        return $this->belongsTo(mProduksi::class, 'id_produksi');
    }
    function pemindahan() {
        return $this->belongsTo(mPemindahanStok::class, 'id_pemindahan');
    }
    function pemakaian() {
        return $this->belongsTo(mPemakaian::class, 'id_pemakaian');
    }
    function stokOpname() {
        return $this->belongsTo(mStokOpname::class, 'id_stokOpname');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all(){
        $count =  DB::table($this->table)
                        ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
                        ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                        ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
                        ->leftJoin('tb_produksi', $this->table.'.id_produksi', '=', 'tb_produksi.id')
                        ->leftJoin('tb_pemindahan', $this->table.'.id_pemindahan', '=', 'tb_pemindahan.id')
                        ->leftJoin('tb_pemakaian', $this->table.'.id_pemakaian', '=', 'tb_pemakaian.id')
                        ->leftJoin('tb_stok_opname', $this->table.'.id_stokOpname', '=', 'tb_stok_opname.id');
        return $count->count();
    }

    public function count_filter($query,$view){

        $count = DB::table($this->table)
                        ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
                        ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                        ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
                        ->leftJoin('tb_produksi', $this->table.'.id_produksi', '=', 'tb_produksi.id')
                        ->leftJoin('tb_pemindahan', $this->table.'.id_pemindahan', '=', 'tb_pemindahan.id')
                        ->leftJoin('tb_pemakaian', $this->table.'.id_pemakaian', '=', 'tb_pemakaian.id')
                        ->leftJoin('tb_stok_opname', $this->table.'.id_stokOpname', '=', 'tb_stok_opname.id');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }

    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
                        ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
                        ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
                        ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
                        ->leftJoin('tb_produksi', $this->table.'.id_produksi', '=', 'tb_produksi.id')
                        ->leftJoin('tb_pemindahan', $this->table.'.id_pemindahan', '=', 'tb_pemindahan.id')
                        ->leftJoin('tb_pemakaian', $this->table.'.id_pemakaian', '=', 'tb_pemakaian.id')
                        ->leftJoin('tb_stok_opname', $this->table.'.id_stokOpname', '=', 'tb_stok_opname.id')
            ->select($this->table.".*");
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function eomonth_stok($start,$length,$query,$view){
        $from = date('2019-12-01');
        $to = date('2020-01-30');
        $data = DB::table($this->table)
            ->select( $this->table.'.id_barang', $this->table.'.id_lokasi', $this->table.".stok_akhir", $this->table.'.tgl_transit')
            ->leftJoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_stok', $this->table.'.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_produksi', $this->table.'.id_produksi', '=', 'tb_produksi.id')
            ->leftJoin('tb_pemindahan', $this->table.'.id_pemindahan', '=', 'tb_pemindahan.id')
            ->leftJoin('tb_pemakaian', $this->table.'.id_pemakaian', '=', 'tb_pemakaian.id')
            ->leftJoin('tb_stok_opname', $this->table.'.id_stokOpname', '=', 'tb_stok_opname.id')
            ->whereBetween('tgl_transit', [$from, $to])
            ->orderBy('tgl_transit');
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
