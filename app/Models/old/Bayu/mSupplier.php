<?php

namespace app\Models\Bayu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mSupplier extends Model
{
    protected $table = 'tb_supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_wilayah',
        'kode_supplier',
        'supplier',
        'supplier_alamat',
        'supplier_kontak',
    ];

    public function wilayah()
    {
        return $this->belongsTo(mWilayah::class, 'id_wilayah');
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    public function count_all(){
        return DB::table($this->table)
            ->leftJoin('tb_wilayah', 'tb_wilayah.id', '=',$this->table.'.id_wilayah')
            ->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table)
            ->leftJoin('tb_wilayah', 'tb_wilayah.id', '=',$this->table.'.id_wilayah');
        foreach ($view as $value){
            $count->orWhere($value['search_field'],'like','%'.$query.'%');
        }
        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*", 'tb_wilayah.wilayah')
            ->leftJoin('tb_wilayah', 'tb_wilayah.id', '=',$this->table.'.id_wilayah');
        foreach ($view as $value){
            $data->orWhere($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
