<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPenjualan extends Model
{
    protected $table = 'tb_penjualan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_langganan',
        'id_user',
        'id_salesman',
        'no_penjualan',
        'urutan',
        'tgl_penjualan',
        'lokasi_penjualan',
        'tgl_kirim_penjualan',
        'pelanggan_penjualan',
        'alamat_pengiriman',
        'penerima',
        'keterangan_penjualan',
        'total_penjualan',
        'disc_persen_penjualan',
        'disc_nominal_penjualan',
        'pajak_persen_penjualan',
        'pajak_nominal_penjualan',
        'ongkos_kirim_penjualan',
        'grand_total_penjualan',
        'dp_penjualan',
        'metode_pembayaran_dp_penjualan',
        'lokasi_penjualan',
        'date'
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all(){
        return DB::table($this->table)
            ->leftjoin('tb_salesman',$this->table.'.id_salesman','=','tb_salesman.id')
            ->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table)
            ->leftjoin('tb_salesman',$this->table.'.id_salesman','=','tb_salesman.id');
        foreach ($view as $value){
            $count->orWhere($value['search_field'],'like','%'.$query.'%');
        }
        return $count->count();
    }

    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*",'tb_salesman.salesman')
            ->leftjoin('tb_salesman',$this->table.'.id_salesman','=','tb_salesman.id');
        foreach ($view as $value){
            $data->orWhere($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countNoPenjualan($id_user){
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->select($this->table)
            ->where($this->table.'.date', $date)
            ->where($this->table.'.id_user', $id_user);
        if($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->max('urutan');
        }

        return $no;
    }

    public function count_all_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', DB::raw('SUM(qty) as qty_jual'), DB::raw('SUM(dpp) as nilai_jual'), DB::raw('SUM(total) as nilai_ppn'))
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $count->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }

        return $count->get();
    }

    public function count_filter_analisa($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', DB::raw('SUM(qty) as qty_jual'), DB::raw('SUM(dpp) as nilai_jual'), DB::raw('SUM(total) as nilai_ppn'))
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $count->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }

        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        return $count->get();
    }

    public function list_analisa($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', DB::raw('SUM(qty) as qty_jual'), DB::raw('SUM(dpp) as nilai_jual'), DB::raw('SUM(total) as nilai_ppn'))
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $data->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function export_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', DB::raw('SUM(qty) as qty_jual'), DB::raw('SUM(dpp) as nilai_jual'), DB::raw('SUM(total) as nilai_ppn'))
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $data->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }
        return $data->get();
    }

    public function count_all_analisa_stock($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', 'tb_stok.id as kode_stok', 'tb_lokasi.lokasi as lokasi' ,DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(tb_stok.`jml_barang`) AS stock'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_lokasi','analisa_penjualan_termasuk_ppn.id_lokasi','=','tb_lokasi.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_stok');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $count->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }

        return $count->get();
    }

    public function count_filter_analisa_stock($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', 'tb_stok.id as kode_stok', 'tb_lokasi.lokasi as lokasi' ,DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(tb_stok.`jml_barang`) AS stock'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_lokasi','analisa_penjualan_termasuk_ppn.id_lokasi','=','tb_lokasi.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_stok');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $count->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }
        return $count->get();
    }

    public function list_analisa_stock($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', 'tb_stok.id as kode_stok', 'tb_lokasi.lokasi as lokasi' ,DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(tb_stok.`jml_barang`) AS stock'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_lokasi','analisa_penjualan_termasuk_ppn.id_lokasi','=','tb_lokasi.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_stok');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $data->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }
        return $data->get();
    }

    public function export_analisa_stock($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang', 'tb_stok.id as kode_stok', 'tb_lokasi.lokasi as lokasi' ,DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(tb_stok.`jml_barang`) AS stock'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_lokasi','analisa_penjualan_termasuk_ppn.id_lokasi','=','tb_lokasi.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_stok');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_barang)){
            $data->where('analisa_penjualan_termasuk_ppn.id_barang', $select_barang);
        }
        return $data->get();
    }

    public function count_all_analisa_per_langganan($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_langganan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_langganan.langganan','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_langganan','analisa_penjualan_termasuk_ppn.id_langganan','=','tb_langganan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_langganan','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_langganan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_langganan', $select_langganan);
        }

        return $count->get();
    }

    public function count_filter_analisa_per_langganan($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_langganan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_langganan.langganan','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_langganan','analisa_penjualan_termasuk_ppn.id_langganan','=','tb_langganan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_langganan','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_langganan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_langganan', $select_langganan);
        }
        return $count->get();
    }

    public function list_analisa_per_langganan($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_langganan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_langganan.langganan','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_langganan','analisa_penjualan_termasuk_ppn.id_langganan','=','tb_langganan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_langganan','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_langganan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_langganan', $select_langganan);
        }
        return $data->get();
    }

    public function export_analisa_per_langganan($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_langganan){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_langganan.langganan','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_langganan','analisa_penjualan_termasuk_ppn.id_langganan','=','tb_langganan.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_langganan','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }

        if (!empty($select_langganan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_langganan', $select_langganan);
        }
        return $data->get();
    }

    public function count_all_analisa_per_salesman($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_salesman){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_salesman.salesman','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_salesman','analisa_penjualan_termasuk_ppn.id_salesman','=','tb_salesman.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_salesman','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_salesman)){
            $count->where('analisa_penjualan_termasuk_ppn.id_salesman', $select_salesman);
        }

        return $count->get();
    }

    public function count_filter_analisa_per_salesman($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_salesman){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_salesman.salesman','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_salesman','analisa_penjualan_termasuk_ppn.id_salesman','=','tb_salesman.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_salesman','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_salesman)){
            $count->where('analisa_penjualan_termasuk_ppn.id_salesman', $select_salesman);
        }
        return $count->get();
    }

    public function list_analisa_per_salesman($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_salesman){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_salesman.salesman','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_salesman','analisa_penjualan_termasuk_ppn.id_salesman','=','tb_salesman.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_salesman','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_salesman)){
            $data->where('analisa_penjualan_termasuk_ppn.id_salesman', $select_salesman);
        }
        return $data->get();
    }

    public function export_analisa_per_salesman($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_salesman){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_salesman.salesman','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_salesman','analisa_penjualan_termasuk_ppn.id_salesman','=','tb_salesman.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_salesman','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->where(DB::raw('analisa_penjualan_termasuk_ppn.tgl_penjualan >= '.$tanggal_start.' AND analisa_penjualan_termasuk_ppn.tgl_penjualan <= '.$tanggal_end));
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_salesman)){
            $data->where('analisa_penjualan_termasuk_ppn.id_salesman', $select_salesman);
        }
        return $data->get();
    }

    public function count_all_analisa_per_supplier($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_supplier.supplier','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('tb_stok.id_supplier','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }

        return $count->get();
    }

    public function count_filter_analisa_per_supplier($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_supplier.supplier','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('tb_stok.id_supplier','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $count->where('tb_stok.id_supplier', $select_supplier);
        }
        return $count->get();
    }

    public function list_analisa_per_supplier($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_supplier.supplier','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('tb_stok.id_supplier','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }
        return $data->get();
    }

    public function export_analisa_per_supplier($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_supplier.supplier','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_stok','analisa_penjualan_termasuk_ppn.id_stok','=','tb_stok.id')
                    ->leftJoin('tb_supplier','tb_stok.id_supplier','=','tb_supplier.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('tb_stok.id_supplier','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if(!empty($select_supplier)){
            $data->where('tb_stok.id_supplier', $select_supplier);
        }
        return $data->get();
    }

    public function count_all_analisa_per_wilayah($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_wilayah){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_wilayah.wilayah','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_wilayah','analisa_penjualan_termasuk_ppn.id_wilayah','=','tb_wilayah.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_wilayah','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_wilayah)){
            $count->where('analisa_penjualan_termasuk_ppn.id_wilayah', $select_wilayah);
        }

        return $count->get();
    }

    public function count_filter_analisa_per_wilayah($query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_wilayah){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_wilayah.wilayah','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_wilayah','analisa_penjualan_termasuk_ppn.id_wilayah','=','tb_wilayah.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_wilayah','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $count->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

        if (!empty($select_lokasi)){
            $count->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $count->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_wilayah)){
            $count->where('analisa_penjualan_termasuk_ppn.id_wilayah', $select_wilayah);
        }
        return $count->get();
    }

    public function list_analisa_per_wilayah($start,$length,$query,$view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_wilayah){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_wilayah.wilayah','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_wilayah','analisa_penjualan_termasuk_ppn.id_wilayah','=','tb_wilayah.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_wilayah','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_wilayah)){
            $data->where('analisa_penjualan_termasuk_ppn.id_wilayah', $select_wilayah);
        }
        return $data->get();
    }

    public function export_analisa_per_wilayah($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_wilayah){
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('analisa_penjualan_termasuk_ppn')
                    ->select('tb_wilayah.wilayah','tb_golongan.golongan' ,'tb_barang.nama_barang as barang', 'tb_barang.kode_barang',DB::raw('SUM(analisa_penjualan_termasuk_ppn.qty) AS qty_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.dpp) AS nilai_jual'), DB::raw('SUM(analisa_penjualan_termasuk_ppn.total) AS nilai_ppn'))
                    ->leftJoin('tb_wilayah','analisa_penjualan_termasuk_ppn.id_wilayah','=','tb_wilayah.id')
                    ->leftJoin('tb_barang','analisa_penjualan_termasuk_ppn.id_barang','=','tb_barang.id')
                    ->leftJoin('tb_golongan','analisa_penjualan_termasuk_ppn.id_golongan','=','tb_golongan.id')
                    ->groupBy('analisa_penjualan_termasuk_ppn.id_wilayah','analisa_penjualan_termasuk_ppn.id_golongan', 'analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)){
            $data->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if (!empty($select_lokasi)){
            $data->where('analisa_penjualan_termasuk_ppn.id_lokasi', $select_lokasi);
        }

        if (!empty($select_golongan)){
            $data->where('analisa_penjualan_termasuk_ppn.id_golongan', $select_golongan);
        }

        if (!empty($select_wilayah)){
            $data->where('analisa_penjualan_termasuk_ppn.id_wilayah', $select_wilayah);
        }
        return $data->get();
    }
}
