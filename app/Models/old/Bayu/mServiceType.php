<?php

namespace app\Models\Bayu;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mServiceType extends Model
{
    protected $table = 'tb_service_type';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_service_type',
        'keterangan',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    public function count_all($type=false){
        $count =  DB::table($this->table);
        if($type){
            $count->where('id','>','7');
        }
        return $count->count();
    }
    public function count_filter($query,$view,$type=false){

        $count = DB::table($this->table);

        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        if($type){
            $count->where('id','>','7');
        }
        return $count->count();
    }
    public function list($start,$length,$query,$view,$type=false){
        $data = DB::table($this->table)
            ->select($this->table.".*");
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($type){
            $data->where('id','>','7');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
