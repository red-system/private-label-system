<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use app\Models\Bayu\mPO;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPembelianBarang extends Model
{
    protected $table = 'tb_pembelian_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_po',
        'id_po_barang',
        'id_pembelian',
        'id_barang',
        'id_stok',
        'hpp_barang'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function po_barang()
    {
        return $this->belongsTo(mPoBarang::class, 'id_po_barang');
    }

    public function pembelian()
    {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function stok()
    {
        return $this->belongsTo(mStok::class, 'id_stok');
    }

    public function count_all_kartu_pembelian($tanggal_start, $tanggal_end, $supplier){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_start))),
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_end)))
        ];

         $count = DB::table($this->table)
                    ->select('tb_pembelian.id_po', 'tb_pembelian.no_pembelian_label', 'tb_pembelian.tanggal_pembelian', 'tb_pembelian.tanggal_jatuh_tempo', DB::raw('IF(tb_hutang_supplier.status_hutang_supplier = "lunas", tb_hutang_supplier.tgl_faktur_pembelian, NULL) as tanggal_bayar'), 'tb_po.total_po', DB::raw('SUM(tb_pembelian_return_barang.qty_return) as qty_return'), DB::raw('tb_po.grand_total_po as harga'), DB::raw('SUM((tb_pembelian_return_barang.qty_return * tb_po_barang.harga)) as retur'), 'tb_po.pajak_nominal_po', 'tb_hutang_supplier.sisa_hutang_supplier', 'tb_pembelian.keterangan_pembelian', DB::raw('(tb_po.dp_po + (tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier)) as bayar_dn_kn'))
                    ->leftJoin('tb_pembelian', $this->table.'.id_pembelian', '=', 'tb_pembelian.id')
                    ->leftJoin('tb_po', $this->table.'.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_po_barang', $this->table.'.id_po_barang', '=', 'tb_po_barang.id')
                    ->leftJoin('tb_pembelian_return', 'tb_pembelian.id', '=','tb_pembelian_return.id_pembelian')
                    ->leftJoin('tb_pembelian_return_barang', 'tb_pembelian_return_barang.id_pembelian_barang', '=', 'tb_pembelian_barang.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
                    ->groupBy('tb_pembelian.id_po');

         if (!empty($tanggal_start) && !empty($tanggal_end)){
             $count->whereBetween('tb_pembelian.tanggal_pembelian', $where_date);
         }

         if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $count->whereIn("tb_po.id_supplier", $supplier);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_kartu_pembelian($query,$view, $tanggal_start, $tanggal_end, $supplier){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_start))),
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_end)))
        ];

        $count = DB::table($this->table)
                    ->select('tb_pembelian.id_po', 'tb_pembelian.no_pembelian_label', 'tb_pembelian.tanggal_pembelian', 'tb_pembelian.tanggal_jatuh_tempo', DB::raw('IF(tb_hutang_supplier.status_hutang_supplier = "lunas", tb_hutang_supplier.tgl_faktur_pembelian, NULL) as tanggal_bayar'), 'tb_po.total_po', DB::raw('SUM(tb_pembelian_return_barang.qty_return) as qty_return'), DB::raw('tb_po.grand_total_po as harga'), DB::raw('SUM((tb_pembelian_return_barang.qty_return * tb_po_barang.harga)) as retur'), 'tb_po.pajak_nominal_po', 'tb_hutang_supplier.sisa_hutang_supplier', 'tb_pembelian.keterangan_pembelian', DB::raw('(tb_po.dp_po + (tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier)) as bayar_dn_kn'))
                    ->leftJoin('tb_pembelian', $this->table.'.id_pembelian', '=', 'tb_pembelian.id')
                    ->leftJoin('tb_po', $this->table.'.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_po_barang', $this->table.'.id_po_barang', '=', 'tb_po_barang.id')
                    ->leftJoin('tb_pembelian_return', 'tb_pembelian.id', '=','tb_pembelian_return.id_pembelian')
                    ->leftJoin('tb_pembelian_return_barang', 'tb_pembelian_return_barang.id_pembelian_barang', '=', 'tb_pembelian_barang.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
                    ->groupBy('tb_pembelian.id_po');
        foreach ($view as $value){
            $count->orHaving($value['search_field'],'like','%'.$query.'%');
        }

         if (!empty($tanggal_start) && !empty($tanggal_end)){
             $count->whereBetween('tb_pembelian.tanggal_pembelian', $where_date);
         }

         if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $count->whereIn("tb_po.id_supplier", $supplier);
                }
            }
        }

        return $count->get();
    }

    public function list_kartu_pembelian($start,$length,$query,$view, $tanggal_start, $tanggal_end, $supplier){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_start))),
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_end)))
        ];
        $data = DB::table($this->table)
                    ->select('tb_pembelian.id_po', 'tb_pembelian.no_pembelian_label', 'tb_pembelian.tanggal_pembelian', 'tb_pembelian.tanggal_jatuh_tempo', DB::raw('IF(tb_hutang_supplier.status_hutang_supplier = "lunas", tb_hutang_supplier.tgl_faktur_pembelian, NULL) as tanggal_bayar'), 'tb_po.total_po', DB::raw('SUM(tb_pembelian_return_barang.qty_return) as qty_return'), DB::raw('tb_po.grand_total_po as harga'), DB::raw('SUM((tb_pembelian_return_barang.qty_return * tb_po_barang.harga)) as retur'), 'tb_po.pajak_nominal_po', 'tb_hutang_supplier.sisa_hutang_supplier', 'tb_pembelian.keterangan_pembelian', DB::raw('(tb_po.dp_po + (tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier)) as bayar_dn_kn'))
                    ->leftJoin('tb_pembelian', $this->table.'.id_pembelian', '=', 'tb_pembelian.id')
                    ->leftJoin('tb_po', $this->table.'.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_po_barang', $this->table.'.id_po_barang', '=', 'tb_po_barang.id')
                    ->leftJoin('tb_pembelian_return', 'tb_pembelian.id', '=','tb_pembelian_return.id_pembelian')
                    ->leftJoin('tb_pembelian_return_barang', 'tb_pembelian_return_barang.id_pembelian_barang', '=', 'tb_pembelian_barang.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
                    ->groupBy('tb_pembelian.id_po');

        foreach ($view as $value){
            $data->orHaving($value['search_field'],'like','%'.$query.'%');
        }
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }

         if (!empty($tanggal_start) && !empty($tanggal_end)){
             $data->whereBetween('tb_pembelian.tanggal_pembelian', $where_date);
         }

         if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $data->whereIn("tb_po.id_supplier", $supplier);
                }
            }
        }

        return $data->get();
    }

    public function export_kartu_pembelian($tanggal_start, $tanggal_end, $supplier){
        $where_date = [
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_start))),
            Main::format_date_db(date('d-m-Y', strtotime($tanggal_end)))
        ];

        $data = DB::table($this->table)
                    ->select('tb_pembelian.id_po', 'tb_pembelian.no_pembelian_label', 'tb_pembelian.tanggal_pembelian', 'tb_pembelian.tanggal_jatuh_tempo', DB::raw('IF(tb_hutang_supplier.status_hutang_supplier = "lunas", tb_hutang_supplier.tgl_faktur_pembelian, NULL) as tanggal_bayar'), 'tb_po.total_po', DB::raw('SUM(tb_pembelian_return_barang.qty_return) as qty_return'), DB::raw('tb_po.grand_total_po as harga'), DB::raw('SUM((tb_pembelian_return_barang.qty_return * tb_po_barang.harga)) as retur'), 'tb_po.pajak_nominal_po', 'tb_hutang_supplier.sisa_hutang_supplier', 'tb_pembelian.keterangan_pembelian', DB::raw('(tb_po.dp_po + (tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier)) as bayar_dn_kn'))
                    ->leftJoin('tb_pembelian', $this->table.'.id_pembelian', '=', 'tb_pembelian.id')
                    ->leftJoin('tb_po', $this->table.'.id_po', '=', 'tb_po.id')
                    ->leftJoin('tb_po_barang', $this->table.'.id_po_barang', '=', 'tb_po_barang.id')
                    ->leftJoin('tb_pembelian_return', 'tb_pembelian.id', '=','tb_pembelian_return.id_pembelian')
                    ->leftJoin('tb_pembelian_return_barang', 'tb_pembelian_return_barang.id_pembelian_barang', '=', 'tb_pembelian_barang.id')
                    ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
                    ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
                    ->groupBy('tb_pembelian.id_po');

         if (!empty($tanggal_start) && !empty($tanggal_end)){
             $data->whereBetween('tb_pembelian.tanggal_pembelian', $where_date);
         }

         if($supplier != null){
            if (!empty($supplier)){
                if (!in_array(0, $supplier)){
                    $data->whereIn("tb_po.id_supplier", $supplier);
                }
            }
        }

        return $data->get();
    }
}
