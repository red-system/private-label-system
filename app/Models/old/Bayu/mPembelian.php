<?php

namespace app\Models\Bayu;

use app\Helpers\Main;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use app\Models\Bayu\mPenjualan;

class mPembelian extends Model
{
    protected $table = 'tb_pembelian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_po',
        'id_user',
        'no_pembelian',
        'no_pembelian_label',
        'no_surat_jalan',
        'tanggal_pembelian',
        'tanggal_surat_jalan',
        'keterangan_pembelian',
        'pajak_no_faktur',
        'tanggal_jatuh_tempo',
        'metode_pembayaran',
        'hutang_giro_bank',
        'hutang_giro_bank_cair',
        'status_return_pembelian',
        'status_return_pembelian_date',
        'status_return_pembelian_semua',
        'status_return_pembelian_semua_date',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function pembelian_barang()
    {
        return $this->hasMany(mPembelianBarang::class, 'id_pembelian');
    }

    public function count_all_laporan($tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $count = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', $this->table . '.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi',
                DB::raw('tb_po.`grand_total_po` AS nilai'),
                DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
            )
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_po.id', '=', 'tb_hutang_supplier.id_po')
            ->leftJoin('tb_hutang_giro', $this->table . '.id', '=', 'tb_hutang_giro.id_pembelian')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->orderBy($this->table . '.no_pembelian_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $count->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_laporan($query, $view, $tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $count = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', $this->table . '.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi',
                DB::raw('tb_po.`grand_total_po` AS nilai'),
                DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
            )
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_po.id', '=', 'tb_hutang_supplier.id_po')
            ->leftJoin('tb_hutang_giro', $this->table . '.id', '=', 'tb_hutang_giro.id_pembelian')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->orderBy($this->table . '.no_pembelian_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }
        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $count->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $count->get();
    }

    public function list_laporan($start, $length, $query, $view, $tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $data = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', $this->table . '.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi',
                DB::raw('tb_po.`grand_total_po` AS nilai'),
                DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
            )
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_po.id', '=', 'tb_hutang_supplier.id_po')
            ->leftJoin('tb_hutang_giro', $this->table . '.id', '=', 'tb_hutang_giro.id_pembelian')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->orderBy($this->table . '.no_pembelian_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $data->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_laporan($tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $data = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', $this->table . '.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi',
                DB::raw('tb_po.`grand_total_po` AS nilai'),
                DB::raw('tb_po.dp_po + IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.total_hutang_supplier - tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_hutang_giro.jumlah_hutang_giro, IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", tb_po.sisa_pembayaran_po, 0))) AS bayar_dn_kn'),
                DB::raw(' IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.`sisa_hutang_supplier`, IF(tb_pembelian.metode_pembayaran = "hutang_giro" AND tb_hutang_giro.status_hutang_giro = "cair", tb_po.sisa_pembayaran_po - tb_hutang_giro.jumlah_hutang_giro,  IF(tb_pembelian.metode_pembayaran = "transfer" OR tb_pembelian.`metode_pembayaran` = "cash", 0, tb_po.sisa_pembayaran_po))) AS sisa')
            )
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_po.id', '=', 'tb_hutang_supplier.id_po')
            ->leftJoin('tb_hutang_giro', $this->table . '.id', '=', 'tb_hutang_giro.id_pembelian')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->orderBy($this->table . '.no_pembelian_label', 'desc');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $data->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }


    public function count_all_detail_laporan($tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $count = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', 'tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi', 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_po_barang.qty', 'tb_satuan.satuan', 'tb_po_barang.harga', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_hutang_giro', 'tb_hutang_giro.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_barang', 'tb_po_barang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy($this->table . '.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $count->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_detail_laporan($query, $view, $tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $count = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', 'tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi', 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_po_barang.qty', 'tb_satuan.satuan', 'tb_po_barang.harga', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_hutang_giro', 'tb_hutang_giro.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_barang', 'tb_po_barang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy($this->table . '.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }
        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $count->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $count->get();
    }

    public function list_detail_laporan($start, $length, $query, $view, $tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $data = DB::table($this->table)
            ->select($this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', 'tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi', 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_po_barang.qty', 'tb_satuan.satuan', 'tb_po_barang.harga', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_hutang_giro', 'tb_hutang_giro.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_barang', 'tb_po_barang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy($this->table . '.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $data->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_detail_laporan($tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $data = DB::table($this->table)
            ->select($this->table . '.id', $this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', 'tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi', 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_po_barang.qty', 'tb_satuan.satuan', 'tb_po_barang.harga', 'tb_po_barang.sub_total', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_hutang_giro', 'tb_hutang_giro.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_barang', 'tb_po_barang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy($this->table . '.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $data->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function export_detail_barang_laporan($tanggal_start, $tanggal_end, $tanggal_start_jatuh_tempo, $tanggal_end_jatuh_tempo, $select_lokasi, $select_supplier)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $date_jatuh_tempo = [
            $tanggal_start_jatuh_tempo,
            $tanggal_end_jatuh_tempo
        ];

        $data = DB::table($this->table)
            ->select($this->table . '.id as id_pembelian', $this->table . '.no_pembelian_label as no_faktur', 'tb_po.id_supplier', 'tb_supplier.supplier', $this->table . '.tanggal_pembelian as tanggal', 'tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_lokasi.lokasi', 'tb_barang.kode_barang', 'tb_barang.nama_barang', 'tb_po_barang.qty', 'tb_satuan.satuan', 'tb_po_barang.harga', 'tb_po_barang.discount as discount_barang', 'tb_po_barang.sub_total', 'tb_po.pajak_nominal_po as ppn', 'tb_po.discount_persen_po', 'tb_po.discount_nominal_po', 'tb_po.ongkos_kirim_po', 'tb_po.grand_total_po as grand_total')
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_supplier', 'tb_hutang_supplier.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_hutang_giro', 'tb_hutang_giro.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_pembelian_barang', 'tb_pembelian_barang.id_pembelian', '=', 'tb_pembelian.id')
            ->leftJoin('tb_po_barang', 'tb_pembelian_barang.id_po_barang', '=', 'tb_po_barang.id')
            ->leftJoin('tb_barang', 'tb_po_barang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'tb_pembelian_barang.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy($this->table . '.id', 'tb_barang.id');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween($this->table . '.tanggal_pembelian', $date_where);
        }

        if (!empty($tanggal_start_jatuh_tempo) && !empty($tanggal_end_jatuh_tempo)) {
            $data->whereBetween('tb_pembelian.tanggal_jatuh_tempo', $date_jatuh_tempo);
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("tb_po.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("tb_po.id_supplier", $select_supplier);
                }
            }
        }
        return $data->get();
    }

    public function count_all_jatuh_tempo($tanggal_filter, $lokasi, $supplier)
    {
        $count = DB::table($this->table)
            ->select('tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_pembelian.metode_pembayaran', 'tb_pembelian.no_pembelian_label as no_faktur', 'tb_supplier.supplier', DB::raw('IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro", tb_hutang_giro.jumlah_hutang_giro, NULL)) as jumlah'))
            ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_giro', 'tb_pembelian.id', '=', 'tb_hutang_giro.id_pembelian')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'cash')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'transfer')
            ->where('tb_hutang_supplier.status_hutang_supplier', 'belum_lunas')
            ->orWhere('tb_hutang_giro.status_hutang_giro', "belum_cair")
            ->orderBy('tb_pembelian.no_pembelian_label');

        if ($lokasi != null) {
            if (!empty($lokasi)) {
                if (!in_array(0, $lokasi)) {
                    $count->whereIn($this->table . ".id_lokasi", $lokasi);
                }
            }
        }

        if ($supplier != null) {
            if (!empty($supplier)) {
                if (!in_array(0, $supplier)) {
                    $count->whereIn($this->table . ".id_supplier", $supplier);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_jatuh_tempo($query, $view, $tanggal_filter, $lokasi, $supplier)
    {

        $count = DB::table($this->table)
            ->select('tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_pembelian.metode_pembayaran', 'tb_pembelian.no_pembelian_label as no_faktur', 'tb_supplier.supplier', DB::raw('IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro", tb_hutang_giro.jumlah_hutang_giro, NULL)) as jumlah'))
            ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_giro', 'tb_pembelian.id', '=', 'tb_hutang_giro.id_pembelian')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'cash')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'transfer')
            ->where('tb_hutang_supplier.status_hutang_supplier', 'belum_lunas')
            ->orWhere('tb_hutang_giro.status_hutang_giro', "belum_cair")
            ->orderBy('tb_pembelian.no_pembelian_label');
        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        if (!empty($lokasi)) {
            $count->where('tb_po.id_lokasi', $lokasi);
        }

        if (!empty($supplier)) {
            $count->where($this->table . '.id_supplier', $supplier);
        }
        return $count->get();
    }

    public function list_jatuh_tempo($start, $length, $query, $view, $tanggal_filter, $lokasi, $supplier)
    {
        $data = DB::table($this->table)
            ->select('tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_pembelian.metode_pembayaran', 'tb_pembelian.no_pembelian_label as no_faktur', 'tb_supplier.supplier', DB::raw('IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro", tb_hutang_giro.jumlah_hutang_giro, NULL)) as jumlah'))
            ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_giro', 'tb_pembelian.id', '=', 'tb_hutang_giro.id_pembelian')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'cash')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'transfer')
            ->where('tb_hutang_supplier.status_hutang_supplier', 'belum_lunas')
            ->orWhere('tb_hutang_giro.status_hutang_giro', "belum_cair")
            ->orderBy('tb_pembelian.no_pembelian_label');
        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        if (!empty($lokasi)) {
            $data->where('tb_po.id_lokasi', $lokasi);
        }

        if (!empty($supplier)) {
            $data->where('tb_po.id_supplier', $supplier);
        }
        return $data->get();
    }

    public function export_jatuh_tempo($tanggal_filter, $lokasi, $supplier)
    {
        $data = DB::table($this->table)
            ->select('tb_pembelian.tanggal_jatuh_tempo as jatuh_tempo', 'tb_pembelian.metode_pembayaran', 'tb_pembelian.no_pembelian_label as no_faktur', 'tb_supplier.supplier', DB::raw('IF(tb_pembelian.metode_pembayaran = "hutang_supplier", tb_hutang_supplier.sisa_hutang_supplier, IF(tb_pembelian.metode_pembayaran = "hutang_giro", tb_hutang_giro.jumlah_hutang_giro, NULL)) as jumlah'))
            ->leftJoin('tb_hutang_supplier', 'tb_pembelian.id', '=', 'tb_hutang_supplier.id_pembelian')
            ->leftJoin('tb_po', 'tb_pembelian.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_hutang_giro', 'tb_pembelian.id', '=', 'tb_hutang_giro.id_pembelian')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'cash')
            ->where('tb_pembelian.metode_pembayaran', '!=', 'transfer')
            ->where('tb_hutang_supplier.status_hutang_supplier', 'belum_lunas')
            ->orWhere('tb_hutang_giro.status_hutang_giro', "belum_cair")
            ->orderBy('tb_pembelian.no_pembelian_label');

        if (!empty($lokasi)) {
            $data->where('tb_po.id_lokasi', $lokasi);
        }

        if (!empty($supplier)) {
            $data->where($this->table . '.id_supplier', $supplier);
        }
        return $data->get();
    }

    public function count_all_rekap($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.subtotal) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.ppn) AS ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_rekap($query, $view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.subtotal) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.ppn) AS ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }
        return $count->get();
    }

    public function list_rekap($start, $length, $query, $view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.subtotal) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.ppn) AS ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }
        return $data->get();
    }

    public function export_rekap($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.subtotal) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.ppn) AS ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }
        return $data->get();
    }

    public function count_all_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', 'pembelian_termasuk_ppn_perbarang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.total) AS nilai_ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_analisa($query, $view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', 'pembelian_termasuk_ppn_perbarang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.total) AS nilai_ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }
        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }
        return $count->get();
    }

    public function list_analisa($start, $length, $query, $view, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', 'pembelian_termasuk_ppn_perbarang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', 'tb_satuan.satuan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.total) AS nilai_ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }
        return $data->get();
    }

    public function export_analisa($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_golongan.golongan', 'pembelian_termasuk_ppn_perbarang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', 'tb_satuan.satuan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.qty) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.total) AS nilai_ppn'))
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }
        return $data->get();
    }

    public function count_all_analisa_tahun($tahun_filter, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_golongan.golongan', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`total`) AS nilai_ppn'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tahun_filter)) {
            $count->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        return $count->get();
    }

    public function count_filter_analisa_tahun($query, $view, $tahun_filter, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_golongan.golongan', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`total`) AS nilai_ppn'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tahun_filter)) {
            $count->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        foreach ($view as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }
        return $count->get();
    }

    public function list_analisa_tahun($start, $length, $query, $view, $tahun_filter, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_golongan.golongan', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`total`) AS nilai_ppn'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tahun_filter)) {
            $data->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        foreach ($view as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }
        return $data->get();
    }

    public function export_analisa_tahun($tahun_filter, $select_lokasi, $select_golongan, $select_supplier, $select_barang)
    {

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_golongan.golongan', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', 'tb_lokasi.lokasi as lokasi', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.dpp) AS nilai'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`total`) AS nilai_ppn'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_golongan', 'pembelian_termasuk_ppn_perbarang.id_golongan', '=', 'tb_golongan.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_golongan', 'pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tahun_filter)) {
            $data->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }


        if ($select_supplier != null) {
            if (!empty($select_supplier)) {
                if (!in_array(0, $select_supplier)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_supplier", $select_supplier);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }
        return $data->get();
    }

    public function count_all_analisa_vs_penjualan($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        $tb_pembelian = $count->get();

        $count_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count_penjualan->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        $tb_penjualan = $count_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['satuan_beli'] = $pembelian->satuan;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            $arr_data['satuan_jual'] = '';
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->kode, array_column($merge, 'kode'))) {
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['satuan_beli'] = '';
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;
                $arr_data['satuan_jual'] = $penjualan->satuan;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->kode == $m['kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                    array_set($merge[$key], 'satuan_jual', $penjualan->satuan);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function count_filter_analisa_vs_penjualan($query, $view_beli, $view_jual, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        $tb_pembelian = $count->get();

        $count_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $count_penjualan->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        $tb_penjualan = $count_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['satuan_beli'] = $pembelian->satuan;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            $arr_data['satuan_jual'] = '';
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->kode, array_column($merge, 'kode'))) {
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['satuan_beli'] = '';
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;
                $arr_data['satuan_jual'] = $penjualan->satuan;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->kode == $m['kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                    array_set($merge[$key], 'satuan_jual', $penjualan->satuan);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function list_analisa_vs_penjualan($start, $length, $query, $view_beli, $view_jual, $tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        $tb_pembelian = $data->get();

        $data_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data_penjualan->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        $tb_penjualan = $data_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['satuan_beli'] = $pembelian->satuan;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            $arr_data['satuan_jual'] = '';
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->kode, array_column($merge, 'kode'))) {
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['satuan_beli'] = '';
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;
                $arr_data['satuan_jual'] = $penjualan->satuan;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->kode == $m['kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                    array_set($merge[$key], 'satuan_jual', $penjualan->satuan);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function export_analisa_vs_penjualan($tanggal_start, $tanggal_end, $select_lokasi, $select_golongan, $select_barang)
    {
        $date_where = [
            $tanggal_start,
            $tanggal_end
        ];

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_satuan', 'pembelian_termasuk_ppn_perbarang.id_satuan', '=', 'tb_satuan.id')
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data->whereBetween('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        $tb_pembelian = $data->get();

        $data_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select('tb_barang.kode_barang as kode', 'tb_barang.nama_barang as barang', 'tb_satuan.satuan', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan', 'tb_stok.id_satuan', '=', 'tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang');

        if (!empty($tanggal_start) && !empty($tanggal_end)) {
            $data_penjualan->whereBetween('analisa_penjualan_termasuk_ppn.tgl_penjualan', $date_where);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        $tb_penjualan = $data_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['satuan_beli'] = $pembelian->satuan;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            $arr_data['satuan_jual'] = '';
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->kode, array_column($merge, 'kode'))) {
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['satuan_beli'] = '';
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;
                $arr_data['satuan_jual'] = $penjualan->satuan;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->kode == $m['kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                    array_set($merge[$key], 'satuan_jual', $penjualan->satuan);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function count_all_analisa_vs_penjualan_tahunan($tahun_filter, $select_lokasi, $select_golongan, $select_barang)
    {

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan','pembelian_termasuk_ppn_perbarang.id_satuan','=','tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang', DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b")'));

        if (!empty($tahun_filter)) {
            $count->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        $tb_pembelian = $count->get();

        $count_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select(DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b") AS bulan'),'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang', DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b")'));

        if (!empty($tahun_filter)) {
            $count_penjualan->whereYear('analisa_penjualan_termasuk_ppn.tgl_penjualan', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        $tb_penjualan = $count_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['bulan_kode'] = $pembelian->bulan.$pembelian->kode;
            $arr_data['bulan'] = $pembelian->bulan;
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->bulan.$penjualan->kode, array_column($merge, 'bulan_kode'))) {
                $arr_data['bulan'] = $penjualan->bulan;
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->bulan.$penjualan->kode == $m['bulan_kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function count_filter_analisa_vs_penjualan_tahunan($query, $view_beli, $view_jual, $tahun_filter, $select_lokasi, $select_golongan, $select_barang)
    {

        $count = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan','pembelian_termasuk_ppn_perbarang.id_satuan','=','tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang', DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b")'));

        if (!empty($tahun_filter)) {
            $count->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        foreach ($view_beli as $value) {
            $count->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        $tb_pembelian = $count->get();

        $count_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select(DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b") AS bulan'),'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang', DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b")'));

        if (!empty($tahun_filter)) {
            $count_penjualan->whereYear('analisa_penjualan_termasuk_ppn.tgl_penjualan', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $count_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        foreach ($view_jual as $value) {
            $count_penjualan->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }

        $tb_penjualan = $count_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['bulan_kode'] = $pembelian->bulan.$pembelian->kode;
            $arr_data['bulan'] = $pembelian->bulan;
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->bulan.$penjualan->kode, array_column($merge, 'bulan_kode'))) {
                $arr_data['bulan'] = $penjualan->bulan;
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->bulan.$penjualan->kode == $m['bulan_kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function list_analisa_vs_penjualan_tahunan($start, $length, $query, $view_beli, $view_jual, $tahun_filter, $select_lokasi, $select_golongan, $select_barang)
    {

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan','pembelian_termasuk_ppn_perbarang.id_satuan','=','tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang', DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b")'));

        if (!empty($tahun_filter)) {
            $data->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        foreach ($view_beli as $value) {
            $data->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        $tb_pembelian = $data->get();

        $data_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select(DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b") AS bulan'),'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang', DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b")'));

        if (!empty($tahun_filter)) {
            $data_penjualan->whereYear('analisa_penjualan_termasuk_ppn.tgl_penjualan', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        foreach ($view_jual as $value) {
            $data_penjualan->orHaving($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data_penjualan
                ->offset($start)
                ->limit($length);
        }

        $tb_penjualan = $data_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['bulan_kode'] = $pembelian->bulan.$pembelian->kode;
            $arr_data['bulan'] = $pembelian->bulan;
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->bulan.$penjualan->kode, array_column($merge, 'bulan_kode'))) {
                $arr_data['bulan'] = $penjualan->bulan;
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->bulan.$penjualan->kode == $m['bulan_kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function export_analisa_vs_penjualan_tahunan($tahun_filter, $select_lokasi, $select_golongan, $select_barang)
    {

        $data = DB::table('pembelian_termasuk_ppn_perbarang')
            ->select(DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b") AS bulan'), 'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`qty`) AS qty_beli'), DB::raw('SUM(pembelian_termasuk_ppn_perbarang.`dpp`) AS nilai_beli'))
            ->leftJoin('tb_barang', 'pembelian_termasuk_ppn_perbarang.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_lokasi', 'pembelian_termasuk_ppn_perbarang.id_lokasi', '=', 'tb_lokasi.id')
            ->leftJoin('tb_satuan','pembelian_termasuk_ppn_perbarang.id_satuan','=','tb_satuan.id')
            ->groupBy('pembelian_termasuk_ppn_perbarang.id_barang', DB::raw('DATE_FORMAT(pembelian_termasuk_ppn_perbarang.`tgl_pembelian`,"%b")'));

        if (!empty($tahun_filter)) {
            $data->whereYear('pembelian_termasuk_ppn_perbarang.tgl_pembelian', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data->whereIn("pembelian_termasuk_ppn_perbarang.id_barang", $select_barang);
                }
            }
        }

        $tb_pembelian = $data->get();

        $data_penjualan = DB::table('analisa_penjualan_termasuk_ppn')
            ->select(DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b") AS bulan'),'tb_barang.kode_barang as kode', 'tb_satuan.satuan', 'tb_barang.nama_barang as barang', DB::raw('SUM( analisa_penjualan_termasuk_ppn.`qty`) AS qty_jual'), DB::raw('SUM( analisa_penjualan_termasuk_ppn.`dpp`) AS nilai_jual'))
            ->leftJoin('tb_barang', 'analisa_penjualan_termasuk_ppn.id_barang', '=', 'tb_barang.id')
            ->leftJoin('tb_stok', 'analisa_penjualan_termasuk_ppn.id_stok', '=', 'tb_stok.id')
            ->leftJoin('tb_satuan','tb_stok.id_satuan','=','tb_satuan.id')
            ->groupBy('analisa_penjualan_termasuk_ppn.id_barang', DB::raw('DATE_FORMAT(analisa_penjualan_termasuk_ppn.`tgl_penjualan`,"%b")'));

        if (!empty($tahun_filter)) {
            $data_penjualan->whereYear('analisa_penjualan_termasuk_ppn.tgl_penjualan', $tahun_filter);
        }

        if ($select_golongan != null) {
            if (!empty($select_golongan)) {
                if (!in_array(0, $select_golongan)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_golongan", $select_golongan);
                }
            }
        }

        if ($select_lokasi != null) {
            if (!empty($select_lokasi)) {
                if (!in_array(0, $select_lokasi)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_lokasi", $select_lokasi);
                }
            }
        }

        if ($select_barang != null) {
            if (!empty($select_barang)) {
                if (!in_array(0, $select_barang)) {
                    $data_penjualan->whereIn("analisa_penjualan_termasuk_ppn.id_barang", $select_barang);
                }
            }
        }

        $tb_penjualan = $data_penjualan->get();

        $merge = array();
        foreach ($tb_pembelian as $pembelian) {
            $arr_data['bulan_kode'] = $pembelian->bulan.$pembelian->kode;
            $arr_data['bulan'] = $pembelian->bulan;
            $arr_data['kode'] = $pembelian->kode;
            $arr_data['barang'] = $pembelian->barang;
            $arr_data['qty_beli'] = $pembelian->qty_beli;
            $arr_data['nilai_beli'] = $pembelian->nilai_beli;
            $arr_data['qty_jual'] = 0;
            $arr_data['nilai_jual'] = 0;
            array_push($merge, $arr_data);
        }

        foreach ($tb_penjualan as $key => $penjualan) {
            if (!in_array($penjualan->bulan.$penjualan->kode, array_column($merge, 'bulan_kode'))) {
                $arr_data['bulan'] = $penjualan->bulan;
                $arr_data['kode'] = $penjualan->kode;
                $arr_data['barang'] = $penjualan->barang;
                $arr_data['qty_beli'] = 0;
                $arr_data['nilai_beli'] = 0;
                $arr_data['qty_jual'] = $penjualan->qty_jual;
                $arr_data['nilai_jual'] = $penjualan->nilai_jual;;
                array_push($merge, $arr_data);
            }
        }

        foreach ($merge as $key => $m) {
            foreach ($tb_penjualan as $penjualan) {
                if ($penjualan->bulan.$penjualan->kode == $m['bulan_kode']) {
                    array_set($merge[$key], 'qty_jual', $penjualan->qty_jual);
                    array_set($merge[$key], 'nilai_jual', $penjualan->nilai_jual);
                }
            }
        }

        return json_decode(json_encode($merge), FALSE);
    }

    public function isInArray($needle, $haystack)
    {
        foreach ($needle as $stack) {
            if (in_array($stack, $haystack)) {
                return true;
            }
        }
        return false;
    }
}
