<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mSatuan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPoBarang extends Model
{
    protected $table = 'tb_po_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_po',
        'id_barang',
        'id_satuan',
        'id_stok',
        'qty',
        'harga',
        'discount',
        'discount_nominal',
        'sub_total'
    ];

    public function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
