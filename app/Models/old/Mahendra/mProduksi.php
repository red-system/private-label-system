<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mProduksi extends Model
{
    protected $table = 'tb_produksi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_produksi',
        'no_produksi_label',
        'tgl_produksi',
        'tanggal_expired',
        'keterangan'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function count_all()
    {
        $count = DB::table($this->table);
        return $count->count();

    }

    public function count_filter($query, $produkList)
    {
        $count = DB::table($this->table);

        $count->where(function ($qry) use ($produkList, $query) {
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list_all($start, $length, $query, $produkList)
    {
        $data = DB::table($this->table)
            ->orderBy($this->table.'.'.$this->primaryKey, 'DESC');

        $data->where(function ($qry) use ($produkList, $query) {
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }


        return $data->get();
    }

}
