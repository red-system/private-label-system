<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Widi\mBarang;
use app\Models\Widi\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mStok extends Model
{
    protected $table = 'tb_stok';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_lokasi',
        'id_satuan',
        'jml_barang',
        'hpp',
        'id_supplier',
    ];

    function lokasi() {
        return $this->belongsTo( mLokasi::class, 'id_lokasi');
    }

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
    public function count_all_by_product($id){
        return DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->where('id_barang', '=', $id)
            ->count();
    }
    public function count_filter_by_product($id, $query,$view){

        $count = DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->where('id_barang', '=', $id);
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list_by_product($id, $start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*","tb_barang.nama_barang", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_supplier.supplier")
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->where('id_barang', '=', $id);
        $data -> where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
    public function count_all(){
        return DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id')
            ->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table)
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id');
        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*","tb_barang.nama_barang", "tb_lokasi.lokasi", "tb_wilayah.wilayah",
                "tb_supplier.supplier")
            ->join('tb_barang',$this->table.'.id_barang','=','tb_barang.id')
            ->join('tb_lokasi',$this->table.'.id_lokasi','=','tb_lokasi.id')
            ->join('tb_supplier',$this->table.'.id_supplier','=','tb_supplier.id')
            ->join('tb_wilayah','tb_lokasi'.'.id_wilayah','=','tb_wilayah.id');
        $data -> where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }
}
