<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPembelianReturnBarang extends Model
{
    protected $table = 'tb_pembelian_return_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_po',
        'id_pembelian',
        'id_pembelian_barang',
        'id_pembelian_return',
        'id_barang',
        'qty_return',
        'qty_harga_return',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function pembelian()
    {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->count();
    }

    public function countNoReturn($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->where($this->table . '.tanggal_return', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->orderBy('no_pembelian_return', 'DESC')->value('no_pembelian_return');
        }

        return $no;
    }
}
