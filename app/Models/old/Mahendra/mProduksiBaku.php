<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mBarang;
use app\Models\Widi\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mProduksiBaku extends Model
{
    protected $table = 'tb_produksi_baku';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produksi',
        'id_barang',
        'id_stok',
        'id_lokasi',
        'nama_barang',
        'harga_barang',
        'qty',
        'nilai_bahan_baku',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function stok()
    {
        return $this->hasMany(mStok::class, 'id_barang', 'id_barang');
    }

    public function lokasi() {
        return $this->belongsTo(mLokasi::class, 'id_lokasi', 'id');
    }

    public function count_all()
    {
        $count = DB::table($this->table);
        return $count->count();

    }

    public function count_filter($query, $produkList)
    {
        $count = DB::table($this->table);

        $count->where(function ($qry) use ($produkList, $query) {
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list_all($start, $length, $query, $produkList)
    {
        $data = DB::table($this->table);

        $data->where(function ($qry) use ($produkList, $query) {
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        return $data->get();
    }

}
