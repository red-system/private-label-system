<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPembelian extends Model
{
    protected $table = 'tb_pembelian';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_po',
        'id_user',
        'no_pembelian',
        'no_pembelian_label',
        'no_surat_jalan',
        'tanggal_pembelian',
        'tanggal_surat_jalan',
        'keterangan_pembelian',
        'pajak_no_faktur',
        'tanggal_jatuh_tempo',
        'metode_pembayaran',
        'hutang_giro_bank',
        'hutang_giro_bank_cair',
        'status_return_pembelian',
        'status_return_pembelian_date',
        'status_return_pembelian_semua',
        'status_return_pembelian_semua_date',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function pembelian_barang() {
        return $this->hasMany(mPembelianBarang::class, 'id_pembelian');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi','=','tb_lokasi.id');
        foreach ($view as $value) {
            $count->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select([$this->table.'.*', $this->table.'.id AS id_pembelian', 'tb_po.*', 'tb_supplier.supplier', 'tb_lokasi.lokasi'])
            ->leftJoin('tb_po', $this->table . '.id_po', '=', 'tb_po.id')
            ->leftJoin('tb_supplier', 'tb_po.id_supplier','=','tb_supplier.id')
            ->leftJoin('tb_lokasi', 'tb_po.id_lokasi','=','tb_lokasi.id')
            ->orderBy($this->table.'.id', 'DESC');
        foreach ($view as $value) {
            $data->orWhere($value['search_field'], 'like', '%' . $query . '%');
        }
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function countNoPembelian($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->where($this->table . '.tanggal_pembelian', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->orderBy('no_pembelian', 'DESC')->value('no_pembelian');
        }

        return $no;
    }
}
