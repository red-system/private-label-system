<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPembelianReturn extends Model
{
    protected $table = 'tb_pembelian_return';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_user',
        'id_po',
        'id_pembelian',
        'id_supplier',
        'id_lokasi',
        'no_pembelian_return',
        'no_pembelian_return_label',
        'tanggal_return',
        'keterangan_return'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function pembelian()
    {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function lokasi()
    {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->count();
    }

    public function countNoReturn($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->where($this->table . '.tanggal_return', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->orderBy('no_pembelian_return', 'DESC')->value('no_pembelian_return');
        }

        return $no;
    }
}
