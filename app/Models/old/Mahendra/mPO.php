<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPO extends Model
{
    protected $table = 'tb_po';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_supplier',
        'id_lokasi',
        'id_user',
        'status_pembelian',
        'status_pembelian_date',
        'no_po',
        'no_po_label',
        'tgl_po',
        'tgl_kirim_po',
        'keterangan_po',
        'total_po',
        'discount_persen_po',
        'discount_nominal_po',
        'pajak_persen_po',
        'pajak_nominal_po',
        'ongkos_kirim_po',
        'grand_total_po',
        'dp_po',
        'metode_pembayaran_dp_po',
        'sisa_pembayaran_po',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po_barang()
    {
        return $this->hasMany(mPoBarang::class, 'id_po');
    }

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function lokasi()
    {
        return $this->belongsTo(mLokasi::class, 'id_lokasi');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->where($this->table . '.status_pembelian', 'no')
            ->leftJoin('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->where('status_pembelian', 'no')
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->where($this->table . '.status_pembelian', 'no');
        foreach ($view as $value) {
            $count = $count->where($value['search_field'], 'like', '%' . $query . '%');
        }
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select([$this->table . '.*', 'tb_supplier.supplier', 'tb_lokasi.lokasi'])
            ->where($this->table . '.status_pembelian', 'no')
            ->leftJoin('tb_supplier', $this->table . '.id_supplier', '=', 'tb_supplier.id')
            ->leftJoin('tb_lokasi', $this->table . '.id_lokasi', '=', 'tb_lokasi.id')
            ->where($this->table . '.status_pembelian', 'no');

        foreach ($view as $value) {
            $data = $data->where($value['search_field'], 'like', '%' . $query . '%');
        }

        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->orderBy('tb_po.id','DESC')->get();
    }

    public function countNoPo($id_user)
    {
        $tanggal = now();
        $date = Main::format_date_db($tanggal);
        $data = DB::table($this->table)
            ->where($this->table . '.tgl_po', $date)
            ->where($this->table . '.id_user', $id_user);
        if ($data->count() == 0) {
            $no = 0;
        } else {
            $no = $data->orderBy('no_po', 'DESC')->value('no_po');
        }

        return $no;
    }
}
