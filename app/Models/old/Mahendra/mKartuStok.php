<?php

namespace app\Models\Mahendra;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mKartuStok extends Model
{
    protected $table = 'tb_kartu_stok';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_barang',
        'id_lokasi',
        'id_stok',
        'lokasi',
        'tgl_transit',
        'keterangan',
        'stok_awal',
        'hpp_awal',
        'stok_masuk',
        'hpp_masuk',
        'stok_keluar',
        'hpp_keluar',
        'stok_akhir',
        'sst_sprl',
        'hpp_stok',
        'id_produksi',
        'id_pemindahan',
        'id_pemakaian',
        'id_stokOpname',
        'id_pembelian',
        'id_po',
        'id_pembelian_return',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function count_all(){
        $count =  DB::table($this->table)
            ->leftjoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
            ->leftjoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id');
        return $count->count();
    }
    public function count_filter($query,$view){

        $count = DB::table($this->table)
            ->leftjoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
            ->leftjoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id');

        $count->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });

        return $count->count();
    }
    public function list($start,$length,$query,$view){
        $data = DB::table($this->table)
            ->select($this->table.".*",'tb_lokasi.lokasi', 'tb_barang.nama_barang')
            ->leftjoin('tb_lokasi', $this->table.'.id_lokasi', '=', 'tb_lokasi.id')
            ->leftjoin('tb_barang', $this->table.'.id_barang', '=', 'tb_barang.id');
        $data->where(function($qry) use ($view,$query){
            foreach ($view as $value){
                $qry->orWhere($value['search_field'],'like','%'.$query.'%');
            }
        });
        if($length != null){
            $data
                ->offset($start)
                ->limit($length);
        }
        return $data->get();
    }

    public function akhir($id_lokasi, $id_barang, $date_start, $date_end){
        $data = DB::table($this->table)
            ->select($this->table.".id_barang",$this->table.".stok_akhir",$this->table.".hpp_stok",$this->table.".id_lokasi")
            ->where('tgl_transit' , '<=' , $date_end)
            ->where('tgl_transit', '>=' , $date_start)
            ->where('id_lokasi', $id_lokasi)
            ->where('id_barang', $id_barang)
            ->orderBy('tgl_transit', 'DESC')
            ->limit(1);
        return $data->first();
    }

    public function akhir_null($id_lokasi, $id_barang, $date_end){
        $data = DB::table($this->table)
            ->select($this->table.".id_barang",$this->table.".stok_akhir",$this->table.".hpp_stok",$this->table.".id_lokasi")
            ->where('tgl_transit' , '<=' , $date_end)
            ->where('id_lokasi', $id_lokasi)
            ->where('id_barang', $id_barang)
            ->orderBy('tgl_transit', 'DESC')
            ->limit(1);
        return $data->first();
    }

    public function awal($id_lokasi, $id_barang, $date_start, $date_end){
        $data = DB::table($this->table)
            ->select($this->table.".id_barang",$this->table.".stok_awal",$this->table.".hpp_awal",$this->table.".id_lokasi")
            ->where('tgl_transit' , '<=' , $date_end)
            ->where('tgl_transit', '>=' , $date_start)
            ->where('id_lokasi', $id_lokasi)
            ->where('id_barang', $id_barang)
            ->orderBy('tgl_transit', 'ASC')
            ->limit(1);
        return $data->first();
    }

    public function total($id_lokasi, $id_barang, $date_start, $date_end){
        $data = DB::table($this->table)
            ->select($this->table.".id_barang", $this->table.".id_lokasi",
                DB::raw('SUM(stok_masuk) AS total_stok_masuk, SUM(stok_keluar) AS total_stok_keluar'))
            ->where('tgl_transit' , '<=' , $date_end)
            ->where('tgl_transit', '>=' , $date_start)
            ->where('id_lokasi', $id_lokasi)
            ->where('id_barang', $id_barang);
        return $data->first();
    }

}
