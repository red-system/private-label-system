<?php

namespace app\Models\Mahendra;

use app\Helpers\Main;
use app\Models\Bayu\mBarang;
use app\Models\Bayu\mLokasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mPembelianBarang extends Model
{
    protected $table = 'tb_pembelian_barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_po',
        'id_po_barang',
        'id_pembelian',
        'id_barang',
        'id_stok',
        'hpp_barang'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function po()
    {
        return $this->belongsTo(mPO::class, 'id_po');
    }

    public function po_barang()
    {
        return $this->belongsTo(mPoBarang::class, 'id_po_barang');
    }

    public function pembelian()
    {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function stok()
    {
        return $this->belongsTo(mStok::class, 'id_stok');
    }
}
