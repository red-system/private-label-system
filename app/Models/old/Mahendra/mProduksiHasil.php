<?php

namespace app\Models\Mahendra;

use app\Models\Widi\mLokasi;
use app\Models\Widi\mSatuan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mProduksiHasil extends Model
{
    protected $table = 'tb_produksi_hasil';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_produksi',
        'id_barang',
        'id_lokasi',
        'id_satuan',
        'nama_barang',
        'qty',
        'hpp',
    ];

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    public function stok()
    {
        return $this->hasMany(mStok::class, 'id_barang', 'id_barang');
    }

    public function satuan()
    {
        return $this->belongsTo(mSatuan::class, 'id_satuan', 'id');
    }

    public function lokasi()
    {
        return $this->belongsTo(mLokasi::class, 'id_lokasi', 'id');
    }

    public function count_all()
    {
        $count = DB::table($this->table);
        return $count->count();

    }

    public function count_filter($query, $produkList)
    {
        $count = DB::table($this->table);

        $count->where(function ($qry) use ($produkList, $query) {
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });

        return $count->count();
    }

    public function list_all($start, $length, $query, $produkList)
    {
        $data = DB::table($this->table);

        $data->where(function ($qry) use ($produkList, $query) {
            foreach ($produkList as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            $data
                ->offset($start)
                ->limit($length);
        }

        return $data->get();
    }

}
