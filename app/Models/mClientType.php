<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mClientType extends Model
{
    use SoftDeletes;
    protected $table = 'tb_client_type';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'client_type_id';
    protected $fillable = [
        'client_type_name',
        'client_type_data',
    ];
}
