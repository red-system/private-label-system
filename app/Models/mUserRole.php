<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mUserRole extends Model
{
    use SoftDeletes;
    protected $table = 'tb_user_role';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'user_role_id';
    protected $fillable = [
        'user_role_name',
        'role_keterangan',
        'role_akses'
    ];
}
