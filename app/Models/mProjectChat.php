<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mProjectChat extends Model
{
    use SoftDeletes;
    protected $table = 'tb_chat_project';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'project_chat_id';
    protected $fillable = [
        'user_id',
        'project_id',
        'agency_id',
        'client_id',
        'agent_id',
        'dari',
        'chat',
        'nama_file',
        'file',
        'tipe_file',
    ];

    public function count_all($from, $id, $project_status_id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_project', $this->table . '.project_id', '=', 'tb_project.project_id')
            ->leftJoin('tb_channel', 'tb_project.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', 'tb_project.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', 'tb_project.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', 'tb_project.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where('tb_project.deleted_at', '=', null)
            ->groupBy('tb_project.project_id');

        if ($from == 'agency_id') {
            $count = $count->where('tb_project.agency_id', '=', $id);
        } elseif ($from == 'client_id') {
            $count = $count->where('tb_project.client_id', '=', $id);
        }

        if ($project_status_id != 'all') {
            $count = $count->where('tb_project.project_status_id', '=', $project_status_id);
        }

        return $count->get();
    }

    public function count_filter($query, $view, $from, $id, $project_status_id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_project', $this->table . '.project_id', '=', 'tb_project.project_id')
            ->leftJoin('tb_channel', 'tb_project.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', 'tb_project.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', 'tb_project.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', 'tb_project.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where('tb_project.deleted_at', '=', null)
            ->groupBy('tb_project.project_id');

        if ($from == 'agency_id') {
            $count = $count->where('tb_project.agency_id', '=', $id);
        } elseif ($from == 'client_id') {
            $count = $count->where('tb_project.client_id', '=', $id);
        }

        if ($project_status_id != 'all') {
            $count = $count->where('tb_project.project_status_id', '=', $project_status_id);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->get();
    }

    public function list($start, $length, $query, $view, $from, $id, $project_status_id)
    {
        $data = DB::table($this->table)
            ->select('tb_project.*', 'tb_channel.channel_name', 'tb_client.client_name', 'tb_project_status.project_status_name',
                'tb_project_status.project_status_text_color', 'tb_project_status.project_status_bc_color', 'tb_project_type.project_type_name')
            ->leftJoin('tb_project', $this->table . '.project_id', '=', 'tb_project.project_id')
            ->leftJoin('tb_channel', 'tb_project.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', 'tb_project.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', 'tb_project.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', 'tb_project.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where('tb_project.deleted_at', '=', null)
            ->groupBy('tb_project.project_id');

        if ($from == 'agency_id') {
            $data = $data->where('tb_project.agency_id', '=', $id);
        } elseif ($from == 'client_id') {
            $data = $data->where('tb_project.client_id', '=', $id);
        }

        if ($project_status_id != 'all') {
            $data = $data->where('tb_project.project_status_id', '=', $project_status_id);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
