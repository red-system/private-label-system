<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mChat extends Model
{
    protected $table = 'tb_chat';
    protected $primaryKey = 'chat_id';
    protected $fillable = [
        'agency_id',
        'client_id',
        'from',
    ];

    function agency()
    {
        return $this->belongsTo(mAgency::class, 'agency_id');
    }

    function client()
    {
        return $this->belongsTo(mClient::class, 'client_id');
    }

    function chat_data()
    {
        return $this->hasMany(mChatData::class, 'chat_id');
    }
}
