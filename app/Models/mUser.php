<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mUser extends Model
{
    use SoftDeletes;
    protected $table = 'tb_user';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'from',
        'user_role_id',
        'user_position_id',
        'user_email',
        'user_name',
        'user_password',
        'user_dob',
        'user_data',
        'user_desc',
        'user_pict',
        'count',
    ];
    protected $hidden = [
        'user_password'
    ];

    function user_role()
    {
        return $this->belongsTo(mUserRole::class, 'user_role_id');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_user_role', 'tb_user.user_role_id', '=', 'tb_user_role.user_role_id')
            ->where('tb_user.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_user_role', 'tb_user.user_role_id', '=', 'tb_user_role.user_role_id')
            ->where('tb_user.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select('tb_user.*', 'tb_user_role.user_role_name')
            ->leftJoin('tb_user_role', 'tb_user.user_role_id', '=', 'tb_user_role.user_role_id')
            ->where('tb_user.deleted_at', '=', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
