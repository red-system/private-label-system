<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mTasks extends Model
{
    protected $table = 'tb_tasks';
    protected $primaryKey = 'tasks_id';
    protected $fillable = [
        'project_id',
        'tasks_name',
        'tasks_status',
        'tasks_desc',
        'start_date',
        'end_date',
        'assign_to',
    ];
}
