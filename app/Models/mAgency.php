<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mAgency extends Model
{
    use SoftDeletes;
    protected $table = 'tb_agency';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'agency_id';
    protected $fillable = [
        'agency_name',
        'agency_type_id',
        'agency_email',
        'agency_data',
        'agency_desc',
        'user_status'
    ];

    function agency_type()
    {
        return $this->belongsTo(mAgencyType::class, 'agency_type_id');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->where('tb_agency.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->where('tb_agency.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select('tb_agency.*')
            ->where('tb_agency.deleted_at', '=', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
