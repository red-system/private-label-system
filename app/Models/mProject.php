<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mProject extends Model
{
    use SoftDeletes;
    protected $table = 'tb_project';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'project_id';
    protected $fillable = [
        'company_id',
        'agency_id',
        'client_id',
        'services_id',
        'channel_id',
        'project_ammount',
        'project_timeline',
        'project_cost',
        'project_progress',
        'project_order_number',
        'project_order_title',
        'project_order_date',
        'user_id',
        'project_status_id',
        'project_type_id',
        'akses'
    ];

    function company()
    {
        return $this->belongsTo(mCompany::class, 'company_id');
    }

    function agency()
    {
        return $this->belongsTo(mAgency::class, 'agency_id');
    }

    function client()
    {
        return $this->belongsTo(mClient::class, 'client_id');
    }

    function services()
    {
        return $this->belongsTo(mService::class, 'services_id');
    }

    function channel()
    {
        return $this->belongsTo(mChannel::class, 'channel_id');
    }

    function user()
    {
        return $this->belongsTo(mUser::class, 'user_id');
    }

    function project_status()
    {
        return $this->belongsTo(mProjectStatus::class, 'project_status_id');
    }

    function project_type()
    {
        return $this->belongsTo(mProjectType::class, 'project_type_id');
    }


    public function count_all($from, $id, $project_status_id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', $this->table . '.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', $this->table . '.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where('tb_project.deleted_at', '=', null);

        if ($from == 'agency_id') {
            $count = $count->where('tb_project.agency_id', '=', $id);
        } elseif ($from == 'client_id') {
            $count = $count->where('tb_project.client_id', '=', $id);
        }

        if ($project_status_id != 'all') {
            $count = $count->where('tb_project.project_status_id', '=', $project_status_id);
        }

        return $count->count();
    }

    public function count_filter($query, $view, $from, $id, $project_status_id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', $this->table . '.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', $this->table . '.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where('tb_project.deleted_at', '=', null);

        if ($from == 'agency_id') {
            $count = $count->where('tb_project.agency_id', '=', $id);
        } elseif ($from == 'client_id') {
            $count = $count->where('tb_project.client_id', '=', $id);
        }

        if ($project_status_id != 'all') {
            $count = $count->where('tb_project.project_status_id', '=', $project_status_id);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view, $from, $id, $project_status_id)
    {
        $data = DB::table($this->table)
            ->select('tb_project.*', 'tb_channel.channel_name', 'tb_client.client_name', 'tb_project_status.project_status_name',
                'tb_project_status.project_status_text_color', 'tb_project_status.project_status_bc_color', 'tb_project_type.project_type_name')
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', $this->table . '.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', $this->table . '.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where('tb_project.deleted_at', '=', null);

        if ($from == 'agency_id') {
            $data = $data->where('tb_project.agency_id', '=', $id);
        } elseif ($from == 'client_id') {
            $data = $data->where('tb_project.client_id', '=', $id);
        }

        if ($project_status_id != 'all') {
            $data = $data->where('tb_project.project_status_id', '=', $project_status_id);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }

    public function count_single_all($from, $id, $project_status_id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', $this->table . '.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', $this->table . '.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where($this->table.'client_id', '=', $id)
            ->where('tb_project.deleted_at', '=', null);


        return $count->count();
    }

    public function count_single_filter($query, $view, $from, $id, $project_status_id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', $this->table . '.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', $this->table . '.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where($this->table.'client_id', '=', $id)
            ->where('tb_project.deleted_at', '=', null);

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_single($start, $length, $query, $view, $id)
    {
        $data = DB::table($this->table)
            ->select('tb_project.*', 'tb_channel.channel_name', 'tb_client.client_name', 'tb_project_status.project_status_name',
                'tb_project_status.project_status_text_color', 'tb_project_status.project_status_bc_color', 'tb_project_type.project_type_name')
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->leftJoin('tb_project_status', $this->table . '.project_status_id', '=', 'tb_project_status.project_status_id')
            ->leftJoin('tb_project_type', $this->table . '.project_type_id', '=', 'tb_project_type.project_type_id')
            ->where($this->table.'client_id', '=', $id)
            ->where('tb_project.deleted_at', '=', null);

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
