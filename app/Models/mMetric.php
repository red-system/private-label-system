<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mMetric extends Model
{
    use SoftDeletes;
    protected $table = 'tb_metric';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'metric_id';
    protected $fillable = [
        'project_id',
        'impr',
        'clicks',
        'spend',
        'all_rev',
        'paid_rev',
        'roars',
        'date',
    ];

    public function bulan_ini($id)
    {
        $year = DB::table($this->table)->select('date')->max('date');
        $date = explode('-', $year);
        $data = DB::table($this->table)
            ->select($this->table . '.*')
            ->where($this->table . '.project_id', '=', $id)
            ->whereYear($this->table . '.date', '=', $date[0])
            ->whereMonth($this->table . '.date', '=', $date[1])
            ->orderBy('date', 'desc');
        return $data->first();
    }

    public function bulan_lalu($id, $bulan, $tahun)
    {
        $data = DB::table($this->table)
            ->select($this->table . '.*')
            ->where($this->table . '.project_id', '=', $id);

        if ($bulan == 12) {
            $data = $data->whereYear($this->table . '.date', '<', $tahun)
                ->orderBy($this->table . '.date', 'desc');
        } else {
            $data = $data->whereYear($this->table . '.date', '=', $tahun)
                ->whereMonth($this->table . '.date', '<', $bulan)
                ->orderBy($this->table . '.date', 'desc');

        }

        return $data->first();
    }
}
