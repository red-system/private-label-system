<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mSubProject extends Model
{
    protected $table = 'tb_sub_project';
    protected $primaryKey = 'sub_project_id';
    protected $fillable = [
        'project_id',
        'services_title',
        'qty',
        'status',
        'start_date',
        'end_date',
        'assign_to',
        'position',
        'last_update',
    ];
}
