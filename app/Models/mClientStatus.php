<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mClientStatus extends Model
{
    use SoftDeletes;
    protected $table = 'tb_client_status';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'client_status_id';
    protected $fillable = [
        'agency_id',
        'client_status_name',
        'client_status_desc',
    ];
}
