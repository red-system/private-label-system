<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mChatData extends Model
{
    protected $table = 'tb_chat_data';
    protected $primaryKey = 'data_chat_id';
    protected $fillable = [
        'chat_id',
        'from',
        'to',
        'data_chat',
        'status'
    ];

    function chat()
    {
        return $this->belongsTo(mChat::class, 'chat_id');
    }
}
