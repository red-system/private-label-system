<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mFile extends Model
{
    protected $table = 'tb_file';
    protected $primaryKey = 'file_id';
    protected $fillable = [
        'project_id',
        'file_name',
        'file_link',
    ];
}
