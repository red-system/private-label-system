<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mGrowth extends Model
{
    use SoftDeletes;
    protected $table = 'tb_growth';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'growth_id';
    protected $fillable = [
        'from',
        'from_id',
        'bulan',
        'fullfillment',
        'profit',
        'revenue',
    ];
}
