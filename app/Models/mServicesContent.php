<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mServicesContent extends Model
{
    use SoftDeletes;
    protected $table = 'tb_services_content';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'content_id';
    protected $fillable = [
        'row',
        'block',
        'bc',
        'judul',
        'judul_bc',
        'isi',
        'isi_bc',
        'gambar',
        'button',
        'button_color',
        'button_text',
        'button_text_bc',
        'gambar_detail',
        'text_title',
        'text_detail',
        'faq_id',

    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->where($this->table . '.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->where($this->table . '.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select($this->table . '.*')
            ->where($this->table . '.deleted_at', '=', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
