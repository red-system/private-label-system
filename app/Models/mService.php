<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mService extends Model
{
    use SoftDeletes;
    protected $table = 'tb_services';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'services_id';
    protected $fillable = [
        'services_name',
        'services_fee',
        'services_cost',
        'services_data',
        'services_rules',
        'channel_id',
    ];

    function channel()
    {
        return $this->belongsTo(mChannel::class, 'channel_id');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->where('tb_services.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->where('tb_services.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select('tb_services.*', 'tb_channel.channel_name')
            ->leftJoin('tb_channel', $this->table . '.channel_id', '=', 'tb_channel.channel_id')
            ->where('tb_services.deleted_at', '=', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
