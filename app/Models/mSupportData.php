<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mSupportData extends Model
{
    use SoftDeletes;
    protected $table = 'tb_support_data';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'support_data_id';
    protected $fillable = [
        'support_id',
        'support_data',
        'support_desc',
    ];

    public function count_all($id)
    {
        return DB::table($this->table)
            ->where('tb_support_data.support_id', '=', $id)
            ->where('tb_support_data.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view, $id)
    {
        $count = DB::table($this->table)
            ->where('tb_support_data.support_id', '=', $id)
            ->where('tb_support_data.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view, $id)
    {
        $data = DB::table($this->table)
            ->select('tb_support_data.*')
            ->where('tb_support_data.support_id', '=', $id)
            ->where('tb_support_data.deleted_at', '=', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
