<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mClient extends Model
{
    use SoftDeletes;
    protected $table = 'tb_client';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'client_id';
    protected $fillable = [
        'agency_id',
        'client_name',
        'client_contact',
        'client_email',
        'client_phone',
        'client_type_id',
        'client_status_id',
        'client_desc',
        'client_data',
        'commisions',
        'fixed_cost',
        'user_status'
    ];

    function client_type()
    {
        return $this->belongsTo(mClientType::class, 'client_type_id');
    }

    function client_status()
    {
        return $this->belongsTo(mClientStatus::class, 'client_status_id');
    }

    public function count_all()
    {
        return DB::table($this->table)
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select('tb_client.*', 'tb_client_type.client_type_name', 'tb_client_status.client_status_name')
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }

    public function count_all_agency($id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', '=', null);

        if ($id != 'kosong') {
            $count = $count->where('tb_client.agency_id', '=', $id);
        }

        return $count->count();
    }

    public function count_filter_agency($query, $view, $id)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', '=', null);

        if ($id != 'kosong') {
            $count = $count->where('tb_client.agency_id', '=', $id);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_agency($start, $length, $query, $view, $id)
    {
        $data = DB::table($this->table)
            ->select('tb_client.*', 'tb_client_type.client_type_name', 'tb_client_status.client_status_name')
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', null);

        if ($id != 'kosong') {
            $data = $data->where('tb_client.agency_id', '=', $id);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }

    public function count_all_report($id, $client, $client_type, $client_status)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', '=', null);

        if ($id != 'kosong') {
            $count = $count->where('tb_client.agency_id', '=', $id);
        }

        if (!empty($client)) {
            if (!in_array(0, $client)) {
                $count->whereIn($this->table . ".client_id", $client);
            }
        }

        if (!empty($client_type)) {
            if (!in_array(0, $client_type)) {
                $count->whereIn($this->table . ".client_type_id", $client_type);
            }
        }

        if (!empty($client_status)) {
            if (!in_array(0, $client_status)) {
                $count->whereIn($this->table . ".client_status_id", $client_status);
            }
        }

        return $count->count();
    }

    public function count_filter_report($query, $view, $id, $client, $client_type, $client_status)
    {

        $count = DB::table($this->table)
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', '=', null);

        if ($id != 'kosong') {
            $count = $count->where('tb_client.agency_id', '=', $id);
        }
        if (!empty($client)) {
            if (!in_array(0, $client)) {
                $count->whereIn($this->table . ".client_id", $client);
            }
        }

        if (!empty($client_type)) {
            if (!in_array(0, $client_type)) {
                $count->whereIn($this->table . ".client_type_id", $client_type);
            }
        }

        if (!empty($client_status)) {
            if (!in_array(0, $client_status)) {
                $count->whereIn($this->table . ".client_status_id", $client_status);
            }
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list_report($start, $length, $query, $view, $id, $client, $client_type, $client_status)
    {
        $data = DB::table($this->table)
            ->select('tb_client.*', 'tb_client_type.client_type_name', 'tb_client_status.client_status_name')
            ->leftJoin('tb_client_type', $this->table . '.client_type_id', '=', 'tb_client_type.client_type_id')
            ->leftJoin('tb_client_status', $this->table . '.client_status_id', '=', 'tb_client_status.client_status_id')
            ->where('tb_client.deleted_at', null);

        if ($id != 'kosong') {
            $data = $data->where('tb_client.agency_id', '=', $id);
        }
        if (!empty($client)) {
            if (!in_array(0, $client)) {
                $data->whereIn($this->table . ".client_id", $client);
            }
        }

        if (!empty($client_type)) {
            if (!in_array(0, $client_type)) {
                $data->whereIn($this->table . ".client_type_id", $client_type);
            }
        }

        if (!empty($client_status)) {
            if (!in_array(0, $client_status)) {
                $data->whereIn($this->table . ".client_status_id", $client_status);
            }
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
