<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mCompany extends Model
{
    use SoftDeletes;
    protected $table = 'tb_company';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'company_id';
    protected $fillable = [
        'company_name',
        'company_email',
        'company_address',
        'company_type_id',
        'company_overview',
        'company_phone',
        'client_id',
        'company_data',
    ];

    function client()
    {
        return $this->belongsTo(mClient::class, 'client_id');
    }

    public function count_all($from, $id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_company_type', $this->table . '.company_type_id', '=', 'tb_company_type.company_type_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->where('tb_company.deleted_at', '=', null);
        if ($from == 'agency_id') {
            $count = $count->where('tb_agency.agency_id', '=', $id);
        }

        return $count->count();
    }

    public function count_filter($query, $view, $from, $id)
    {
        $count = DB::table($this->table)
            ->leftJoin('tb_company_type', $this->table . '.company_type_id', '=', 'tb_company_type.company_type_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->where('tb_company.deleted_at', '=', null);

        if ($from == 'agency_id') {
            $count = $count->where('tb_agency.agency_id', '=', $id);
        }

        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view, $from, $id)
    {
        $data = DB::table($this->table)
            ->select('tb_company.*', 'tb_company_type.company_type_name', 'tb_client.client_name')
            ->leftJoin('tb_company_type', $this->table . '.company_type_id', '=', 'tb_company_type.company_type_id')
            ->leftJoin('tb_client', $this->table . '.client_id', '=', 'tb_client.client_id')
            ->where('tb_company.deleted_at', '=', null);

        if ($from == 'agency_id') {
            $data = $data->where('tb_agency.agency_id', '=', $id);
        }

        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
