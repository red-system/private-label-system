<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mProjectStatus extends Model
{
    use SoftDeletes;
    protected $table = 'tb_project_status';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'project_status_id';
    protected $fillable = [
        'project_status_name',
        'project_status_text_color',
        'project_status_bc_color',
        'project_status_data',
        'project_status_desc',
    ];

    public function count_all()
    {
        return DB::table($this->table)
            ->where('tb_project_status.deleted_at', '=', null)
            ->count();
    }

    public function count_filter($query, $view)
    {
        $count = DB::table($this->table)
            ->where('tb_project_status.deleted_at', '=', null);
        $count->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        return $count->count();
    }

    public function list($start, $length, $query, $view)
    {
        $data = DB::table($this->table)
            ->select('tb_project_status.*')
            ->where('tb_project_status.deleted_at', '=', null);
        $data->where(function ($qry) use ($view, $query) {
            foreach ($view as $value) {
                $qry->orWhere($value['search_field'], 'like', '%' . $query . '%');
            }
        });
        if ($length != null) {
            if($length != -1){
                $data
                    ->offset($start)
                    ->limit($length);
            }

        }
        return $data->get();
    }
}
