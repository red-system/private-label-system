/*
 * Let's begin with validation functions
 */
 jQuery.extend(jQuery.fn, {
	/*
	 * check if field value lenth more than 3 symbols ( for name and comment ) 
	 */
	validate: function () {
		if (jQuery(this).val() != undefined && jQuery(this).val().length < 3) {jQuery(this).addClass('error');return false} else {jQuery(this).removeClass('error');return true}
	},
	/*
	 * check if email is correct
	 * add to your CSS the styles of .error field, for example border-color:red;
	 */
	validateEmail: function () {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
		    emailToValidate = jQuery(this).val();
		if (!emailReg.test( emailToValidate ) || emailToValidate == "") {
			jQuery(this).addClass('error');return false
		} else {
			jQuery(this).removeClass('error');return true
		}
	},
});
 
jQuery(function($){
 
	/*
	 * On comment form submit
	 */
	$( '#commentform' ).submit(function(){
 
		// define some vars
		var button = $('#submit'), // submit button
		    respond = $('#respond'), // comment form container
		    commentlist = $('.comment-list'), // comment list container
		    cancelreplylink = $('#cancel-comment-reply-link');
 
		// if user is logged in, do not validate author and email fields
		if( $( '#author' ).length )
			$( '#author' ).validate();
 
		if( $( '#email' ).length )
			$( '#email' ).validateEmail();
 
		// validate comment in any case
		$( '#comment' ).validate();
 
		// if comment form isn't in process, submit it
		if ( !button.hasClass( 'loadingform' ) && !$( '#author' ).hasClass( 'error' ) && !$( '#email' ).hasClass( 'error' ) && !$( '#comment' ).hasClass( 'error' ) ){
                    
                    var comment_id = button.attr('_edit');

                    // For some browsers, `attr` is undefined; for others,
                    // `attr` is false.  Check for both.
                    if (typeof comment_id !== typeof undefined && comment_id !== false)
                    {
			jQuery.ajax({
                                type : 'POST',
                                url : vo_ajax_comment_params.ajaxurl, // admin-ajax.php URL
                                data: jQuery(this).serialize() + '&action=editajaxcomments&cid=' + comment_id, // send form data + action parameter
                                beforeSend: function(xhr){
                                        // what to do just after the form has been submitted
                                        button.addClass('loadingform').val('Loading...');
                                },
                                error: function (request, status, error) {
                                        if( status == 500 ){
                                                alert( 'Error while editing comment' );
                                        } else if( status == 'timeout' ){
                                                alert('Error: Server doesn\'t respond.');
                                        } else {
                                                // process WordPress errors
                                                var wpErrorHtml = request.responseText.split("<p>"),
                                                        wpErrorStr = wpErrorHtml[1].split("</p>");

                                                alert( wpErrorStr[0] );
                                        }
                                },
                                success: function ( response ) {

                                        jQuery(respond).parent().find( ".comment-content p" ).html(jQuery('#comment').val());
                                        // clear textarea field
                                        jQuery('#comment').val('');
                                        jQuery(respond).find( "#cancel-comment-reply-link" ).click();
                                },
                                complete: function(){
                                        // what to do after a comment has been added
                                        button.removeClass( 'loadingform' ).val( 'Post Comment' );
                                }
                        });
                    }
                    else {
                       $.ajax({
				type : 'POST',
				url : vo_ajax_comment_params.ajaxurl, // admin-ajax.php URL
				data: $(this).serialize() + '&action=ajaxcomments', // send form data + action parameter
				beforeSend: function(xhr){
					// what to do just after the form has been submitted
					button.addClass('loadingform').val('Loading...');
				},
				error: function (request, status, error) {
					if( status == 500 ){
						alert( 'Error while adding comment' );
					} else if( status == 'timeout' ){
						alert('Error: Server doesn\'t respond.');
					} else {
						// process WordPress errors
						var wpErrorHtml = request.responseText.split("<p>"),
							wpErrorStr = wpErrorHtml[1].split("</p>");
 
						alert( wpErrorStr[0] );
					}
				},
				success: function ( addedCommentHTML ) {
 
					// if this post already has comments
					if( commentlist.length > 0 ){
 
						// if in reply to another comment
						if( respond.parent().hasClass( 'comment' ) ){
 
							// if the other replies exist
							if( respond.parent().children( '.children' ).length ){	
								respond.parent().children( '.children' ).append( addedCommentHTML );
							} else {
								// if no replies, add <ol class="children">
								addedCommentHTML = '<ul class="children">' + addedCommentHTML + '</ul>';
								respond.parent().append( addedCommentHTML );
							}
							// close respond form
							cancelreplylink.trigger("click");
						} else {
							// simple comment
							commentlist.append( addedCommentHTML );
						}
					}else{
						// if no comments yet
						addedCommentHTML = '<ul class="comment-list">' + addedCommentHTML + '</ul>';
						respond.before( $(addedCommentHTML) );
					}
					// clear textarea field
					$('#comment').val('');
				},
				complete: function(){
					// what to do after a comment has been added
					button.removeClass( 'loadingform' ).val( 'Post Comment' );
				}
			});
                    }
		}
		return false;
	});     
});

jQuery(document).on('click', '.comment-reply-link', function (e) {
    var comment_id=jQuery(this).attr("data-commentid"),
        post_id=jQuery(this).attr("data-postid");

   addComment.moveForm( "div-comment-" + comment_id, comment_id, "respond", post_id);
   jQuery(this).parents().eq(5).find( ".comment-respond form .comment-form-comment #comment" ).val("");
   jQuery(this).parents().eq(5).find( ".comment-respond #reply-title #cancel-comment-reply-link" ).html('Cancel Reply');
   jQuery(this).parents().eq(5).find( ".comment-respond form .form-submit #submit" ).val("Save");

   return false;
});
    
jQuery(document).on('click', '.comment-edit-link', function (e) {
    var comment_id=jQuery(this).attr("data-id"),
        post_id=jQuery(this).attr("data-pid"),
        contentElem=jQuery(this).parents().eq(4).find( ".comment-content p" ),
        old_content=contentElem.html();

   contentElem.parent().hide();
   editComment.moveForm( "div-comment-" + comment_id, comment_id, "respond", post_id);
   jQuery(this).parents().eq(5).find( ".comment-respond form .comment-form-comment #comment" ).val(old_content);
   jQuery(this).parents().eq(5).find( ".comment-respond #reply-title #cancel-comment-reply-link" ).html('Cancel Edit');
   jQuery(this).parents().eq(5).find( ".comment-respond form .form-submit #submit" ).attr("_edit",comment_id);
   jQuery(this).parents().eq(5).find( ".comment-respond form .form-submit #submit" ).val("Save");

   return false;
});

jQuery(document).on('click', '.comment-delete-link', function (e) {
    var comment_id=jQuery(this).attr("data-id");
    
    if (confirm('Are you sure you want to delete this comment?')) {
        
        jQuery.ajax({
                type : 'POST',
                url : vo_ajax_comment_params.ajaxurl, // admin-ajax.php URL
                data: jQuery(this).serialize() + '&action=deleteajaxcomments&cid=' + comment_id, // send form data + action parameter
                beforeSend: function(xhr){

                },
                error: function (request, status, error) {
                        if( status == 500 ){
                                alert( 'Error while deleting comment' );
                        } else if( status == 'timeout' ){
                                alert('Error: Server doesn\'t respond.');
                        } else {
                                // process WordPress errors
                                var wpErrorHtml = request.responseText.split("<p>"),
                                        wpErrorStr = wpErrorHtml[1].split("</p>");

                                alert( wpErrorStr[0] );
                        }
                },
                success: function ( response ) {
                        jQuery('#comment-' + comment_id).remove();
                },
                complete: function(){

                }
        });
    }

   return false;
});

var addComment = {
	/**
	 * @summary Retrieves the elements corresponding to the given IDs.
	 *
	 * @since 2.7.0
	 *
	 * @param {string} commId The comment ID.
	 * @param {string} parentId The parent ID.
	 * @param {string} respondId The respond ID.
	 * @param {string} postId The post ID.
	 * @returns {boolean} Always returns false.
	 */
	moveForm: function( commId, parentId, respondId, postId ) {
		var div, element, style, cssHidden,
			t           = this,
			comm        = t.I( commId ),
			respond     = t.I( respondId ),
			cancel      = t.I( 'cancel-comment-reply-link' ),
			parent      = t.I( 'comment_parent' ),
			post        = t.I( 'comment_post_ID' ),
			commentForm = respond.getElementsByTagName( 'form' )[0];
                
                jQuery(cancel).html('Cancel Reply');
                jQuery(commentForm).find( "#comment" ).val('');
                jQuery(commentForm).find( "#submit" ).val('Post Comment');
                jQuery(commentForm).find( "#submit" ).removeAttr("_edit");


		if ( ! comm || ! respond || ! cancel || ! parent || ! commentForm ) {
			return;
		}

		t.respondId = respondId;
		postId = postId || false;

		if ( ! t.I( 'wp-temp-form-div' ) ) {
			div = document.createElement( 'div' );
			div.id = 'wp-temp-form-div';
			div.style.display = 'none';
			respond.parentNode.insertBefore( div, respond );
		}

		comm.parentNode.insertBefore( respond, comm.nextSibling );
		if ( post && postId ) {
			post.value = postId;
		}
		parent.value = parentId;
		cancel.style.display = '';

		/**
		 * @summary Puts back the comment, hides the cancel button and removes the onclick event.
		 *
		 * @returns {boolean} Always returns false.
		 */
		cancel.onclick = function() {
			var t       = addComment,
				temp    = t.I( 'wp-temp-form-div' ),
				respond = t.I( t.respondId );

			if ( ! temp || ! respond ) {
				return;
			}

			t.I( 'comment_parent' ).value = '0';
			temp.parentNode.insertBefore( respond, temp );
			temp.parentNode.removeChild( temp );
			this.style.display = 'none';
			this.onclick = null;
			return false;
		};

		/*
		 * Sets initial focus to the first form focusable element.
		 * Uses try/catch just to avoid errors in IE 7- which return visibility
		 * 'inherit' when the visibility value is inherited from an ancestor.
		 */
		try {
			for ( var i = 0; i < commentForm.elements.length; i++ ) {
				element = commentForm.elements[i];
				cssHidden = false;

				// Modern browsers.
				if ( 'getComputedStyle' in window ) {
					style = window.getComputedStyle( element );
				// IE 8.
				} else if ( document.documentElement.currentStyle ) {
					style = element.currentStyle;
				}

				/*
				 * For display none, do the same thing jQuery does. For visibility,
				 * check the element computed style since browsers are already doing
				 * the job for us. In fact, the visibility computed style is the actual
				 * computed value and already takes into account the element ancestors.
				 */
				if ( ( element.offsetWidth <= 0 && element.offsetHeight <= 0 ) || style.visibility === 'hidden' ) {
					cssHidden = true;
				}

				// Skip form elements that are hidden or disabled.
				if ( 'hidden' === element.type || element.disabled || cssHidden ) {
					continue;
				}

				element.focus();
				// Stop after the first focusable element.
				break;
			}

		} catch( er ) {}
                
		return false;
	},

	/**
	 * @summary Returns the object corresponding to the given ID.
	 *
	 * @since 2.7.0
	 *
	 * @param {string} id The ID.
	 * @returns {Element} The element belonging to the ID.
	 */
	I: function( id ) {
		return document.getElementById( id );
	}
};

//this is a copy from wp-includes/js/comment-reply.
var editComment = {
	/**
	 * @summary Retrieves the elements corresponding to the given IDs.
	 *
	 * @since 2.7.0
	 *
	 * @param {string} commId The comment ID.
	 * @param {string} parentId The parent ID.
	 * @param {string} respondId The respond ID.
	 * @param {string} postId The post ID.
	 * @returns {boolean} Always returns false.
	 */
	moveForm: function( commId, parentId, respondId, postId ) {
		var div, element, style, cssHidden,
			t           = this,
			comm        = t.I( commId ),
			respond     = t.I( respondId ),
			cancel      = t.I( 'cancel-comment-reply-link' ),
			parent      = t.I( 'comment_parent' ),
			post        = t.I( 'comment_post_ID' ),
			commentForm = respond.getElementsByTagName( 'form' )[0];

		if ( ! comm || ! respond || ! cancel || ! parent || ! commentForm ) {
			return;
		}

		t.respondId = respondId;
		postId = postId || false;

		if ( ! t.I( 'wp-temp-form-div' ) ) {
			div = document.createElement( 'div' );
			div.id = 'wp-temp-form-div';
			div.style.display = 'none';
			respond.parentNode.insertBefore( div, respond );
		}

		comm.parentNode.insertBefore( respond, comm.nextSibling );
		if ( post && postId ) {
			post.value = postId;
		}
		parent.value = parentId;
		cancel.style.display = '';

		/**
		 * @summary Puts back the comment, hides the cancel button and removes the onclick event.
		 *
		 * @returns {boolean} Always returns false.
		 */
		cancel.onclick = function() {
			var t       = editComment,
				temp    = t.I( 'wp-temp-form-div' ),
				respond = t.I( t.respondId );

			if ( ! temp || ! respond ) {
				return;
			}
                        
                        jQuery(respond).find( "#comment" ).val('');
                        jQuery(respond).find( "#submit" ).val('Post Comment');
                        jQuery(respond).find( "#submit" ).removeAttr("_edit");
                        jQuery(respond).parent().find( ".comment-content" ).show();

			t.I( 'comment_parent' ).value = '0';
			temp.parentNode.insertBefore( respond, temp );
			temp.parentNode.removeChild( temp );
			this.style.display = 'none';
			this.onclick = null;
			return false;
		};

		/*
		 * Sets initial focus to the first form focusable element.
		 * Uses try/catch just to avoid errors in IE 7- which return visibility
		 * 'inherit' when the visibility value is inherited from an ancestor.
		 */
		try {
			for ( var i = 0; i < commentForm.elements.length; i++ ) {
				element = commentForm.elements[i];
				cssHidden = false;

				// Modern browsers.
				if ( 'getComputedStyle' in window ) {
					style = window.getComputedStyle( element );
				// IE 8.
				} else if ( document.documentElement.currentStyle ) {
					style = element.currentStyle;
				}

				/*
				 * For display none, do the same thing jQuery does. For visibility,
				 * check the element computed style since browsers are already doing
				 * the job for us. In fact, the visibility computed style is the actual
				 * computed value and already takes into account the element ancestors.
				 */
				if ( ( element.offsetWidth <= 0 && element.offsetHeight <= 0 ) || style.visibility === 'hidden' ) {
					cssHidden = true;
				}

				// Skip form elements that are hidden or disabled.
				if ( 'hidden' === element.type || element.disabled || cssHidden ) {
					continue;
				}

				element.focus();
				// Stop after the first focusable element.
				break;
			}

		} catch( er ) {}

		return false;
	},

	/**
	 * @summary Returns the object corresponding to the given ID.
	 *
	 * @since 2.7.0
	 *
	 * @param {string} id The ID.
	 * @returns {Element} The element belonging to the ID.
	 */
	I: function( id ) {
		return document.getElementById( id );
	}
};