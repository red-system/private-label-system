//sticky menu
var bindStickyMenu = function () {
    jQuery(window).bind('scroll', function () {

        var navHeight = 50; // custom nav height

        if (jQuery(window).scrollTop() > navHeight) {
                jQuery('#masthead').addClass('sticked-menu');

        } else {
                jQuery('#masthead').removeClass('sticked-menu');

        }

    });
};

/*
 * Replace all SVG images with inline SVG
 */
var svgImgToSvg = function () {
    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
};
jQuery(document).ready(function ($) {

    $("#slick-slider").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $("#slick-slider-2").slick({
        // autoplay: true,
        dots: false,
        arrows: false,
        //variableWidth: true,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $("#slick-slider-3").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $("#slick-slider-4").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });
});
var getAjaxGigs = function (skip,replaceUrl) {
    gigsLoading = true;
    newurl = location.protocol + '//' + location.host + location.pathname;
    jQuery("#content-loading").show();
    
    if (!skip)
    {
        jQuery("#gigs-ajax").html('');
        skip = 0;
    }
    
    if (replaceUrl === undefined)
    {
        replaceUrl=true;
    }

    var categories = jQuery.map(jQuery('#category-filter li a.active,#category-filter button.active'), function (el) {
        return jQuery(el).parent().data('catid');
    });
    
    jQuery("#filters-selected #filter-selected-wrapper").html("");
    var filters = jQuery.map(jQuery('#filter_terms_modal div div.is-selected'), function (el) {
        jQuery("#filters-selected #filter-selected-wrapper").append("<div class='btn' data-filterid='" + jQuery(el).data('filterid') + "' data-term='" + jQuery(el).data('filterid') + "' data-filter='" + jQuery(el).data('filter') + "'>" + jQuery(el).data('term_name') + ": " + jQuery(el).data('name') + "<span>&nbsp;x&nbsp;</span></div>");
        return jQuery(el).data('filterid');
    });
    
    
    //change the url witht the selected filters and category
    if (history.pushState && replaceUrl) {
        if(categories.length > 0 || filters.length > 0) {
            newurl+="?";
            
            if(categories.length > 0){
                newurl= newurl + "cat=" + categories.join(","); 
            }
            if(filters.length > 0){
                if(categories.length > 0){
                    newurl+="&";
                }
                newurl= newurl + "fil=" + filters.join(",");
            }
            window.history.pushState({path:newurl},'',newurl);
        }
        else {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var cat_param = url.searchParams.get("cat");
            var fil_param = url.searchParams.get("fil");
            
            if(cat_param !== null){
                categories=cat_param.split(',');
 
                for (i = 0; i < categories.length; i++) {
                   jQuery('#category-filter li[data-catid="' + categories[i] + '"]').children('a').eq(0).addClass('active');
                }
            }
            
            if(fil_param !== null){
                filters=fil_param.split(',');
                
                for (i = 0; i < filters.length; i++) {
                    var elem=jQuery('#filter_terms_modal > div > div.btn[data-filterid="' + filters[i] + '"]');
                    jQuery(elem).addClass('is-selected');
                    jQuery("#filters-selected #filter-selected-wrapper").append("<div class='btn' data-filterid='" + jQuery(elem).data('filterid') + "' data-term='" + jQuery(elem).data('filterid') + "' data-filter='" + jQuery(elem).data('filter') + "'>" + jQuery(elem).data('term_name') + ": " + jQuery(elem).data('name') + "<span>&nbsp;x&nbsp;</span></div>");
                }
            }
        }
    }
    
    //hide tope filters and modal based on selected categories
    //hide modal filters based on filter already selected
    if(categories.length > 0 || filters.length > 0){
        if(categories.length > 0) {
            jQuery('#filter-menu-scrool-container > button').hide();
            jQuery('#filter-menu-scrool-container > button').each(function(){
                var elem_categories = jQuery(this).data('category');
                var elem_categories_array=[];
                if(typeof elem_categories !== 'number' && elem_categories.indexOf('|') !== -1){
                    elem_categories_array=elem_categories.split('|');
                }
                else {
                    elem_categories_array.push(elem_categories);
                }

                for (i = 0; i < elem_categories_array.length; i++) {
                    if(categories.indexOf(parseInt(elem_categories_array[i])) !== -1 || categories.indexOf(elem_categories_array[i] + "") !== -1)
                        jQuery(this).show();
                }
            });
        }
        else {
            jQuery('#filter-menu-scrool-container > button').show();
        }
        
        jQuery('#filter_terms_modal  > div > .vo-filter-item').hide();
        jQuery('#filter_terms_modal  > div > .vo-filter-item').each(function(){
            var showThis=false;
            var elem_categories = jQuery(this).data('category');
            var elem_categories_array=[];
            if(typeof elem_categories !== 'number' && elem_categories.indexOf('|') !== -1){
                elem_categories_array=elem_categories.split('|');
            }
            else {
                elem_categories_array.push(elem_categories);
            }
            
            var elem_filters = jQuery(this).data('subfilter');
            var elem_filters_array=[];
            if(typeof elem_filters !== 'number' && elem_filters.indexOf('|') !== -1){
                elem_filters_array=elem_filters.split('|');
            }
            else {
                elem_filters_array.push(elem_filters);
            }
            
            for (i = 0; i < elem_categories_array.length; i++) {
                if(categories.indexOf(parseInt(elem_categories_array[i])) !== -1 || categories.indexOf(elem_categories_array[i] + "") !== -1)
                   showThis=true;
            }
            
            for (i = 0; i < elem_filters_array.length; i++) {
                if(filters.indexOf(parseInt(elem_filters_array[i])) !== -1 || filters.indexOf(elem_filters_array[i] + "") !== -1)
                   showThis=true;
            }
            
            if(showThis){
                jQuery(this).show();
            }
            
        });
    }
    else {
        jQuery('#filter-menu-scrool-container > button').show();
        jQuery('#filter_terms_modal  > div > .vo-filter-item').show();
    }
   
    var data = {
        action: 'ajax_get_gigs',
        security: theme_object.ajax_nonce,
        meta: 1,
        categories: categories.join("|"),
        filters: filters.join("|"),
        skip: skip,
        count: 6
    };
    
    jQuery.getJSON(theme_object.ajax_url, data, function (items) {
        var $load_more=jQuery("#load-more-gigs");
        if($load_more.lenght > 0) {
            load_more_clone = $load_more.clone(true);
        }
        var show_load_more=false;
        var remaining=0;

        jQuery("#content-loading").hide();
        $load_more.remove();

        if (items.length > 0)
        {
            show_load_more=true;
        }       
        
        jQuery.each(items, function() {
                var item=this;
                remaining=item.remaining;
                var stars_max=5;
                var gig= '<div id="gig" class="col-flex-6 col-flex-md-4  grid-sizer quote-box ' + item.class_color + '-border">' +
                        '<a href="' + item.permalink + '" class="qb-wrap">' +
                            '<h3>' + item.title + '</h3>' +
                            '<strong>' + item.price + '</strong>' +
                            '<span class="quote-star">';
                            for (var star = 0; star < stars_max; star++) {
                                if(star <= item.stars) {
                                       gig+= '<img src="' + theme_object.template_directory  +'/images/star-full.svg" alt="star" />';
                                }
                                else {
                                        gig+= '<img src="' + theme_object.template_directory  +'/images/star-empty.svg" alt="star" />';
                                }
                            } 
                    gig+= ' </span>' +
                            '<span class="quote-review">' + item.reviews + ' Reviews</span>' +
                            '<div class="excerpt">' + item.description + '</div>' +
                            '<i><img src="' + theme_object.template_directory  +'/images/arrow-right.svg" alt="" /></i>' +
                        '</a>' +
                        '</div>';

                var $gig=jQuery(gig);
              
                // append elements to container
                $container.append( $gig );
                
                $container = jQuery('.grid-items');
                $container.isotope({
                    percentPosition: true,
                      itemSelector: '.quote-box',
                      masonry: {
                          columnWidth: '.grid-sizer'
                      }
                });
                // add and lay out newly appended elements
                $container.isotope( 'appended', $gig );                    
        });
        
        if(skip == 0 && !$container.find("#gig").length)
        {
            var $contact_form=jQuery(contact_form);
            // append elements to container
            $container.append( $contact_form ); 
            $container = jQuery('.grid-items');
            $container.isotope({
                percentPosition: true,
                  itemSelector: '.quote-box',
                  masonry: {
                      columnWidth: '.grid-sizer'
                  }
            });
            // add and lay out newly appended elements
            $container.isotope( 'appended', $contact_form );         
        }
        
               
        if(remaining > 0){
            jQuery(".load-more-row").html(load_more_clone);
            if(show_load_more){
                jQuery("#load-more-gigs").show();
            }
            else {
                jQuery("#load-more-gigs").hide();
            }
        }

        gigsLoading = false;
    });
};

var loadMoreGigs = function () {
    var skip = jQuery("#gigs-ajax #gig").length;
    getAjaxGigs(skip);
    
};

jQuery(document).on("click", "#category-filter li", function (e) {
    e.preventDefault();

    if (gigsLoading)
        return;

    jQuery(this).find('a:first').toggleClass('active');
    
    if (history.pushState) {
        var reseturl = location.protocol + '//' + location.host + location.pathname;
        window.history.pushState({path:reseturl},'',reseturl);
    }
     
    
    getAjaxGigs();

});

var subscribeForGig = function (e, id, title, price, description) {
    e.preventDefault();

    var modalBox = "<div id='subscribe-gig-modal' class='inner-modal'>";
    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<h1>Please fill the form to complete the order</h1> ";
    modalBox += "<h3>We will contact you within 24 hours to discuss additional details.</h3> ";
    modalBox += "   <div> ";
    modalBox += "   <div class='box'> ";
    modalBox += "      <div class='wrapper'> ";
    modalBox += "         <span class='label-left'>PRICE</span>";
    modalBox += "         <span class='label-right'>" + price + "</span>";
    modalBox += "         <div class='gig-detail'> ";
    modalBox += "             <h3>You have chosen:</h3> ";
    modalBox += "             <h1>" + title + "</h1> ";
    modalBox += "             <p>" + description + "</p> ";
    modalBox += "         </div> ";
    modalBox += "      </div> ";
    modalBox += "   </div> ";
    modalBox += "   <div class='box'> ";
    modalBox += "      <div class='wrapper'> ";
    modalBox += "         <span class='label-left'>YOUR DETAILS</span>";
    modalBox += "         <div id='subscription' class='gig-detail validate'> ";
    modalBox += "             <input type='hidden' id='id' value='" + id + "'  /> ";
    modalBox += "             <input type='hidden' id='gigname' value='" + title + "'  /> ";
    modalBox += "             <input type='text' id='name' class='validate-required' placeholder='Your Name *' /> ";
    modalBox += "             <input type='email' id='email' class='validate-required validate-email' placeholder='Email Address *' /> ";
    modalBox += "             <input type='text' id='company' placeholder='Company' /> ";
    modalBox += "             <input type='text' id='phone' placeholder='Phone Number' /> ";
    modalBox += "         </div> ";
    modalBox += "         <button  class='btn-default' disabled onclick='submitGigSubscription()'> Subscribe <i class='fa fa-spinner fa-spin' style=' display:none; '></i> </button> ";
    modalBox += "      </div> ";
    modalBox += "   </div> ";
    modalBox += "   </div> ";
    modalBox += "   <div id='modal-error'></div> ";
    modalBox += "</div> ";

    showModal(modalBox);
};

var submitGigSubscription = function () {
    jQuery("#modal-container button i").show();
    jQuery.ajax({
        url: theme_object.ajax_url,
        type: 'post',
        data: {
            'security': theme_object.ajax_nonce,
            'action': 'ajax_subscribe',
            'type': 'gig-subscription',
            'id': jQuery("#subscription #id").val(),
            'gigname': jQuery("#subscription #gigname").val(),
            'name': jQuery("#subscription #name").val(),
            'email': jQuery("#subscription #email").val(),
            'company': jQuery("#subscription #company").val(),
            'phone': jQuery("#subscription #phone").val(),
        },
        success: function (data) {
            if (data == "1") {
                thankYouModal(jQuery("#subscription #name").val());
            }
            else {
                jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
                jQuery("#modal-container button i").hide();
            }
        },
        error: function (errorThrown) {
            jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
            jQuery("#modal-container button i").hide();
        }
    });
};

var mainMenuModal = function () {

    var modalBox = "<div id='main-menu-modal' class='inner-modal'>";
    modalBox += "<a href='/'><img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' alt='Go To Homepage' title='Go To Homepage' ></a>";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<div class='text-wrapper'> ";
    modalBox += "<div class='navbar-collapse'> ";
    modalBox += jQuery(".navbar-collapse").html();
    modalBox += "</div> ";
    modalBox += "</div> ";
    modalBox += "</div> ";

    jQuery('#overlay').addClass('black');
    showModal(modalBox);
};

var contactUsModal = function () {

    var modalBox = "<div id='contact-us-modal' class='inner-modal'>";

    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg'  >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";

    modalBox += "<div class='text-wrapper'> ";
    modalBox += "   <h1>Contact us</h1> ";
    modalBox += "   <div class='validate'> ";
    modalBox += "   <input type='text' id='name' class='validate-required' placeholder='What&#39;s Your Name or Company Name? *' /> ";
    modalBox += "   <input type='email' id='email' class='validate-required validate-email' placeholder='What&#39;s Your eMail? *' /> ";
    modalBox += "   <input type='text' id='website' placeholder='What&#39;s Your Website?' /> ";
    modalBox += "   <textarea rows='4' cols='50' id='message' placeholder='Type Your Message Here' ></textarea>";
    modalBox += "</div> ";
    modalBox += "<button  class='btn-default' disabled onclick='submitContactUs()'> Send <i class='fa fa-spinner fa-spin' style=' display:none; '></i> </button> ";
    modalBox += "   <div id='modal-error'></div> ";
    modalBox += "</div> ";

    showModal(modalBox);
};

var submitContactUs = function () {
    jQuery("#modal-container button i").show();
    jQuery.ajax({
        url: theme_object.ajax_url,
        type: 'post',
        data: {
            'security': theme_object.ajax_nonce,
            'action': 'ajax_subscribe',
            'type': 'contact-us',
            'name': jQuery("#contact-us-modal #name").val(),
            'email': jQuery("#contact-us-modal #email").val(),
            'website': jQuery("#contact-us-modal #website").val(),
            'message': jQuery("#contact-us-modal #message").val(),
        },
        success: function (data) {
            if (data == "1") {
                thankYouModal(jQuery("#contact-us-modal #name").val());
            }
            else {
                jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
                jQuery("#modal-container button i").hide();
            }
        },
        error: function (errorThrown) {
            jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
            jQuery("#modal-container button i").hide();
        }
    });
};

var getTipsModal = function () {

    var modalBox = "<div id='get-tips-modal' class='inner-modal'>";

    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg'  >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";

    modalBox += "<div class='text-wrapper'> ";
    modalBox += "   <h1>E-commerce &digital marketing insights</h1> ";
    modalBox += "   <h3>Get Cutting Edge Best Practices In Your Inbox</h3> ";
    modalBox += "   <div class='validate'> ";
    modalBox += "   <input type='text' id='name' class='validate-required' placeholder='What&#39;s Your Name or Company Name? *' /> ";
    modalBox += "   <input type='email' id='email' class='validate-required validate-email' placeholder='What&#39;s Your eMail? *' /> ";
    modalBox += "   <input type='text' id='website' placeholder='What&#39;s Your Website?' /> ";
    modalBox += "   <select id='industry' > ";
    modalBox += "   <option value='' disabled selected>Choose Your Industry For Targeted Tips</option> ";
    modalBox += "   <option > Fashion </option> ";
    modalBox += "   <option > Food </option> ";
    modalBox += "   <option > Design </option> ";
    modalBox += "   <option > Learning </option> ";
    modalBox += "   <option > Sport </option> ";
    modalBox += "   <option > Retail </option> ";
    modalBox += "   <option > B2B </option> ";
    modalBox += "   </select>";
    modalBox += "   <select id='where' > ";
    modalBox += "   <option value='' disabled selected>Tell Us Where You Sell For Specific Tips</option> ";
    modalBox += "   <option > Australia </option> ";
    modalBox += "   <option > United Kingdom </option> ";
    modalBox += "   <option > USA </option> ";
    modalBox += "   <option > Worldwide </option> ";
    modalBox += "   </select>";

    modalBox += "</div> ";
    modalBox += "<button  class='btn-default' disabled onclick='submitTipsModal()'> Get Tips <i class='fa fa-spinner fa-spin' style=' display:none; '></i> </button> ";
    modalBox += "   <div id='modal-error'></div> ";
    modalBox += "</div> ";

    jQuery('#overlay').addClass('black');
    showModal(modalBox);
};

var submitTipsModal = function () {
    jQuery("#modal-container button i").show();
    jQuery.ajax({
        url: theme_object.ajax_url,
        type: 'post',
        data: {
            'security': theme_object.ajax_nonce,
            'action': 'ajax_subscribe',
            'type': 'get-tips',
            'name': jQuery("#get-tips-modal #name").val(),
            'email': jQuery("#get-tips-modal #email").val(),
            'website': jQuery("#get-tips-modal #website").val(),
            'industry': jQuery("#get-tips-modal #industry").val(),
            'where': jQuery("#get-tips-modal #where").val(),
        },
        success: function (data) {
            if (data == "1") {
                thankYouModal(jQuery("#get-tips-modal #name").val());
            }
            else {
                jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
                jQuery("#modal-container button i").hide();
            }
        },
        error: function (errorThrown) {
            jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
            jQuery("#modal-container button i").hide();
        }
    });
};

var quoteModal = function () {
    window.quote = {
        bussinessModel: "",
        products: "",
        CMS: {},
        extraPages: [],
        optimizations: [],
        analytics: [],
        total: 0,
    };
    var modalBox = getQuotePageHtml(1);
    showModal(modalBox);
};

var quouteSkipIt = function (page_nr) {
    switch (page_nr) {
        case 4:
            window.quote.extraPages = [];
            showModal(getQuotePageHtml(5));
            break;
        case 5:
            window.quote.CMS = {};
            showModal(getQuotePageHtml(6));
            break;
        case 6:
            window.quote.optimizations = [];
            showModal(getQuotePageHtml(7));
            break;
        case 7:
            window.quote.analytics = [];
            showModal(getQuotePageHtml(8));
            break;
        default:
            break;
    }
};

var setQuouteBussinessModel = function (event, bussinessModel) {
    // jQuery(event.target).closest(".box").find(".option-details").toggle();
    window.quote.bussinessModel = bussinessModel;
    var modalBox = getQuotePageHtml(2);
    showModal(modalBox);
};

var setQuouteBussinessProducts = function (products) {
    window.quote.products = products;
    var modalBox = getQuotePageHtml(3);
    showModal(modalBox);
};

var quoteDynamic = function (event) {

    var option = jQuery(event.target.closest('.option'));

    var id = option.data('id');
    var name = option.data('name');
    var price = option.data('price');
    var optionType = option.data('option-type');

    if (option.hasClass('selected')) {

        window.quote[optionType] = window.quote[optionType].filter(function (obj) {
            return  obj.id !== id;
        });

        if (window.quote[optionType].length === 0) {
            jQuery(event.target.closest('.journey-wrapper')).find(".buttons-sidebar .continue").attr('disabled', true);
        }
    }
    else
    {
        window.quote[optionType].push({
            id: id * 1,
            name: name,
            price: price * 1,
        });

        jQuery(event.target.closest('.journey-wrapper')).find(".buttons-sidebar .continue").attr('disabled', false);
    }

    var summarySidebar = getSummarySidebar();
    jQuery("#quote-modal .summary-sidebar").replaceWith(summarySidebar);

    option.toggleClass('selected');d
};

var setQuouteCMS = function (event) {

    var option = jQuery(event.target.closest('.box'));

    var id = option.data('id');
    var name = option.data('name');
    var price = option.data('price');


    window.quote.CMS = {
        id: id,
        name: name,
        price: price
    };
    var modalBox = getQuotePageHtml(6);
    showModal(modalBox);
};

var getQuotePageHtml = function (page_nr) {
    var page = "";

    page += "<div id='quote-modal' class='inner-modal'>";
    page += jQuery("#quote-pages #custom-journey-" + page_nr).prop('outerHTML');
    page += '</div>';

    switch (page_nr) {

        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            summarySidebar = getSummarySidebar();
            page = page.replace('<div class="summary-sidebar"></div>', summarySidebar);
            break;
        case 8:
            productsSummary = getProductsSummary();
            page = page.replace('<div class="products-summary"></div>', productsSummary);
            page = page.replace('<span class="right" id="quote-total"></span>', '<span class="right" id="quote-total">' + "€ " + window.quote.total.formatMoney(2, '.', ',') + '</span>');

            break;
        default:
            break
    }

    //reset selection in case the user enter before
    var jQueryPage = jQuery(page);

    jQueryPage.find(".list-options .option").each(function () {
        var option = jQuery(this);
        var id = option.data('id');
        var optionType = option.data('option-type');

        var indexes = jQuery.map(window.quote[optionType], function (obj, index) {
            if (obj.id === id) {
                return index;
            }
        });
        if (indexes.length === 1 && window.quote[optionType][indexes[0]].selected != false) {
            option.addClass('selected');
            jQueryPage.find(".buttons-sidebar .continue").attr('disabled', false);
        }
    });


    return jQueryPage[0].outerHTML;
};

var getSummarySidebar = function () {

    var quoteTotal = 0;

    switch (window.quote.bussinessModel) {
        case "PHYSICAL PRODUCT":
            if (window.quote.products === "MULTIPLE PRODUCT") {
                quoteTotal += 17200;
            } else {
                //"SINGLE PRODUCT"
                quoteTotal += 13200;
            }
            break;
        case "DIGITAL PRODUCT":
            if (window.quote.products === "MULTIPLE PRODUCT") {
                quoteTotal += 16695;
            } else {
                //"SINGLE PRODUCT"
                quoteTotal += 9060;
            }
            break;
        case "PHYSICAL & DIGITAL":
            quoteTotal += 17200;
            break;
        case "ONLINE COURSE":
            quoteTotal += 16557;
            break;

        default:
            quoteTotal += 0;
            break;
    }


    var summarySidebar = '';
    summarySidebar += '<div class="summary-sidebar">';
    summarySidebar += '     <div class="summary-box">';
    summarySidebar += '         <h1>Summary</h1>';
    summarySidebar += '         <p class="price-in-summary">€' + quoteTotal.formatMoney(2, '.', ',') + '</p>';
    summarySidebar += '         <div style="clear:both;"></div>';
    summarySidebar += '         <table>';

    summarySidebar += '         <tr class="item">';
    summarySidebar += '             <td class="name">Basic e-Commerce Package</td>';
    summarySidebar += '             <td class="price">€' + quoteTotal.formatMoney(2, '.', ',') + '</td>';
    summarySidebar += '         </tr>';


    window.quote.extraPages.forEach(function (item, index) {
        summarySidebar += '         <tr class="item">';
        summarySidebar += '             <td class="name">' + item.name + '</td>';
        summarySidebar += '             <td class="price">€' + item.price.formatMoney(2, '.', ',') + '</td>';
        summarySidebar += '         </tr>';

        quoteTotal += item.price;
    });

    window.quote.optimizations.forEach(function (item, index) {
        summarySidebar += '         <tr class="item">';
        summarySidebar += '             <td class="name">' + item.name + '</td>';
        summarySidebar += '             <td class="price">€' + item.price.formatMoney(2, '.', ',') + '</td>';
        summarySidebar += '         </tr>';

        quoteTotal += item.price;
    });

    window.quote.analytics.forEach(function (item, index) {
        summarySidebar += '         <tr class="item">';
        summarySidebar += '             <td class="name">' + item.name + '</td>';
        summarySidebar += '             <td class="price">€' + item.price.formatMoney(2, '.', ',') + '</td>';
        summarySidebar += '         </tr>';

        quoteTotal += item.price;
    });

    if (window.quote.CMS.name !== undefined) {
        summarySidebar += '         <tr class="item">';
        summarySidebar += '             <td class="name">' + window.quote.CMS.name + '</td>';
        summarySidebar += '             <td class="price">€' + window.quote.CMS.price.formatMoney(2, '.', ',') + '</td>';
        summarySidebar += '         </tr>';

        quoteTotal += window.quote.CMS.price;
    }



    summarySidebar += '         <tr class="total">';
    summarySidebar += '             <td class="name">TOTAL</td>';
    summarySidebar += '             <td class="price">€' + quoteTotal.formatMoney(2, '.', ',') + '</td>';
    summarySidebar += '         </tr>';

    summarySidebar += '         </table>';
    summarySidebar += '     </div> ';
    summarySidebar += '     <div class="hide-show-details"><span>Show</span> Details <i class="fa fa-caret-down" aria-hidden="true"></i></div>';

    summarySidebar += '</div>';

    window.quote.total = quoteTotal;

    return  summarySidebar;


};

var changeProducts = function (event) {

    var option = jQuery(event.target);

    var id = option.data('id');
    var optionType = option.data('option-type');

    var indexes = jQuery.map(window.quote[optionType], function (obj, index) {
        if (obj.id === id) {
            return index;
        }
    });
    if (indexes.length !== 1) {
        return;
    }

    if (option.hasClass('selected')) {
        window.quote[optionType][indexes[0]].selected = false;
        window.quote.total -= window.quote[optionType][indexes[0]].price;
    }
    else
    {
        window.quote[optionType][indexes[0]].selected = true;
        window.quote.total += window.quote[optionType][indexes[0]].price;
    }

    var productsSummary = getProductsSummary();
    jQuery("#quote-modal .products-summary").replaceWith(productsSummary);
    jQuery("#quote-modal #quote-total").text("€ " + window.quote.total.formatMoney(2, '.', ','));

    option.toggleClass('selected');
};

var toggleCMS = function (event) {

    var option = jQuery(event.target);

    if (option.hasClass('selected')) {
        window.quote.CMS.selected = false;
        window.quote.total -= window.quote.CMS.price;
    }
    else
    {
        window.quote.CMS.selected = true;
        window.quote.total += window.quote.CMS.price;
    }

    var productsSummary = getProductsSummary();
    jQuery("#quote-modal .products-summary").replaceWith(productsSummary);
    jQuery("#quote-modal #quote-total").text("€ " + window.quote.total.formatMoney(2, '.', ','));

    option.toggleClass('selected');
};

var getProductsSummary = function () {

    var count = 0;

    var productsSummary = "<div class='products-summary'>";

    if (window.quote.extraPages.length > 0) {
        productsSummary += '<div class="service-block dynamic">';
        productsSummary += '    <h4>EXTRA PAGES</h4>';
        productsSummary += '    <ul>';

        window.quote.extraPages.forEach(function (item, index) {
            productsSummary += '     <li class="' + (item.selected === undefined || item.selected === true ? "selected" : "") + '" onclick="changeProducts(event);" data-id="' + item.id + '"   data-option-type="extraPages" >' + item.name + '</li>';

        });
        productsSummary += '    </ul>';
        productsSummary += '</div>';

        count++;
    }

    if (window.quote.analytics.length > 0) {
        productsSummary += '<div class="service-block dynamic">';
        productsSummary += '    <h4>ANALYTICS</h4>';
        productsSummary += '    <ul>';

        window.quote.analytics.forEach(function (item, index) {
            productsSummary += '     <li class="' + (item.selected === undefined || item.selected === true ? "selected" : "") + '" onclick="changeProducts(event);" data-id="' + item.id + '" data-option-type="analytics" >' + item.name + '</li>';

        });
        productsSummary += '    </ul>';
        productsSummary += '</div>';

        count++;
    }

    if (window.quote.optimizations.length > 0) {
        if (count % 2 === 0) {
            productsSummary += '<div class="clearfix"></div>';
        }

        productsSummary += '<div class="service-block dynamic">';
        productsSummary += '    <h4>OPTIMIZATION</h4>';
        productsSummary += '    <ul>';

        window.quote.optimizations.forEach(function (item, index) {
            productsSummary += '     <li class="' + (item.selected === undefined || item.selected === true ? "selected" : "") + '" onclick="changeProducts(event);" data-id="' + item.id + '"  data-option-type="optimizations" >' + item.name + '</li>';

        });
        productsSummary += '    </ul>';
        productsSummary += '</div>';

        count++;
    }

    if (window.quote.CMS.name !== undefined) {
        if (count % 2 === 0) {
            productsSummary += '<div class="clearfix"></div>';
        }

        productsSummary += '<div class="service-block dynamic">';
        productsSummary += '    <h4>DEVELOPMENT</h4>';
        productsSummary += '    <ul>';

        productsSummary += '     <li class="' + (window.quote.CMS.selected === undefined || window.quote.CMS.selected === true ? "selected" : "") + '" onclick="toggleCMS(event);"  >' + window.quote.CMS.name + '</li>';

        productsSummary += '    </ul>';
        productsSummary += '</div>';

        count++;
    }

    productsSummary += '</div>';

    return  productsSummary;
};

var submitQuoteModal = function () {
    jQuery("#modal-container button i").show();

    var quote = getFormatedQuote();
    jQuery.ajax({
        url: theme_object.ajax_url,
        type: 'post',
        data: {
            'security': theme_object.ajax_nonce,
            'action': 'ajax_subscribe',
            'type': 'quote-modal',
            'name': jQuery("#quote-modal #name").val(),
            'email': jQuery("#quote-modal #email").val(),
            'company': jQuery("#quote-modal #company").val(),
            'phone': jQuery("#quote-modal #phone").val(),
            'time': jQuery("#quote-modal #time").val(),
            'description': jQuery("#quote-modal #description").val(),
            'productssummary': quote,
            // 'productssummary': JSON.stringify(window.quote, null, '\t'),
        },
        success: function (data) {
            if (data == "1") {
                thankYouModal(jQuery("#quote-modal #name").val());
            }
            else {
                jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
                jQuery("#modal-container button i").hide();
            }
        },
        error: function (errorThrown) {
            jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
            jQuery("#modal-container button i").hide();
        }
    });
};

var getFormatedQuote = function () {
    var quote = "";

    quote += "Bussiness Model: " + window.quote.bussinessModel + " / " + window.quote.products + "<br>";

    if (Object.keys(window.quote.CMS).length > 0) {
        quote += "CMS: " + window.quote.CMS.name + " ( &euro;" + window.quote.CMS.price + ") <br>";
    }

    if (window.quote.extraPages.length > 0) {
        quote += "Extra Pages:<br>";
        for (var i = 0; i < window.quote.extraPages.length; i++) {
            if (window.quote.extraPages[i].selected !== false) {
                quote += "- " + window.quote.extraPages[i].name + " ( &euro;" + window.quote.extraPages[i].price + ") <br>";
            }
        }
    }

    if (window.quote.optimizations.length > 0) {
        quote += "Optimizations:<br>";
        for (var i = 0; i < window.quote.optimizations.length; i++) {
            if (window.quote.optimizations[i].selected !== false) {
                quote += "- " + window.quote.optimizations[i].name + " ( &euro;" + window.quote.optimizations[i].price + ") <br>";
            }
        }
    }

    if (window.quote.analytics.length > 0) {
        quote += "Analytics:<br>";
        for (var i = 0; i < window.quote.analytics.length; i++) {
            if (window.quote.analytics[i].selected !== false) {
                quote += "- " + window.quote.analytics[i].name + " ( &euro;" + window.quote.analytics[i].price + ") <br>";
            }
        }
    }

    quote += "<b>Total:  &euro;" + window.quote.total + "</b>";

    return quote;
};

var thankYouModal = function (name) {

    var modalBox = "<div id='thank-you-modal' class='inner-modal'>";
    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<h1>Congratulations " + name + "! Your order is complete</h1> ";
    modalBox += "<h3>We will contact you within 24 hours to discuss additional details.</h3> ";
    modalBox += "<div class='thank-you-buttons'>";
    modalBox += "<a href='/'><button class='btn-default go-to-homepage'>Homepage</button></a>";
    modalBox += "<a href='/gigs/'><button class='btn-default see-gigs'>See Gigs</button></a>";
    modalBox += "</div>";
    modalBox += "<div class='bottom'> ";
    modalBox += "   <h3>The Journey Begins: You Are On the Road to Success!</h3> ";
    modalBox += "   <h1>The Viral Octopus Team:)</h1> ";
    modalBox += "</div> ";
    modalBox += "</div> ";

    showModal(modalBox);
};

var thankYouModalRole = function (name) {

    var modalBox = "<div id='thank-you-modal' class='inner-modal'>";
    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<h1>CONGRATULATIONS " + name + "!  WE GOT YOUR APPLICATION</h1> ";
    modalBox += "<h3>We will contact you as soon as we have cleared the backlog to discuss additional details.</h3> ";
    modalBox += "<div class='thank-you-buttons'>";
    modalBox += "<a href='/'><button class='btn-default go-to-homepage'>Back to VO</button></a>";
    modalBox += "<a href='/be-a-gigger#section-break-third'><button class='btn-default facebook-subscribe'>SUBSCRIBE THE FACEBOOK GROUP</button></a>";
    modalBox += "</div>";
    modalBox += "<div class='bottom'> ";
    modalBox += "   <h3>The Journey Begins: You Are On the Road to Success!</h3> ";
    modalBox += "   <h1>The Viral Octopus Team:)</h1> ";
    modalBox += "</div> ";
    modalBox += "</div> ";

    showModal(modalBox);
};

var textModal = function (text) {

    var modalBox = "<div id='text-modal' class='inner-modal'>";
    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";

    modalBox += "<div class='text-wrapper'> ";
    modalBox += text;
    modalBox += "</div> ";
    modalBox += "</div> ";

    showModal(modalBox);
};

var categoriesMenuModal = function () {

    var modalBox = "<div id='categories-menu-modal' class='inner-modal'>";
    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<div class='text-wrapper'> ";
    modalBox += jQuery("#category-div .left").html();
    modalBox += "</div> ";
    modalBox += "<div class='show-results' onclick='closeModal()'>Show Results</div>";
    modalBox += "</div> ";

    showModal(modalBox);
};

jQuery(document).on("keyup change", ".validate [class^=validate-]", function (e) {
    var valid = true;
    var wrapper = e.target.closest(".validate");

    jQuery(wrapper).find("[class^=validate-]").each(function (index, element) {
        jQuery(element).removeClass("has-error");

        if (jQuery(element).hasClass("validate-required") && jQuery(element).val() === "") {
            jQuery(element).addClass("has-error");
            valid = false;
            return false;
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var email = jQuery(element).val();
        if (jQuery(element).hasClass("validate-email") && !re.test(email)) {
            jQuery(element).addClass("has-error");
            valid = false;
            return false;
        }
    });

    if (valid) {
        jQuery(wrapper).next('button').removeAttr('disabled');
    }
    else
    {
        jQuery(wrapper).next('button').attr('disabled', true);
    }

});

var moveSlider = function (e, step) {

    e.preventDefault();
    if (step === -1) {

        if (jQuery("#slider .images-wrapper .images-list img.slider-img.active").prev().length > 0) {
            jQuery("#slider .images-wrapper .images-list img.slider-img.active").removeClass('active').prev().addClass('active');
        }
        else {
            jQuery("#slider .images-wrapper .images-list img.slider-img.active").removeClass('active');
            jQuery("#slider .images-wrapper .images-list img.slider-img:last").addClass('active');
        }

        if (jQuery("#slider .thumbnails-wrapper img.slider-img.active").prev().length > 0) {
            jQuery("#slider .thumbnails-wrapper img.slider-img.active").removeClass('active').prev().addClass('active');
        }
        else {
            jQuery("#slider .thumbnails-wrapper img.slider-img.active").removeClass('active');
            jQuery("#slider .thumbnails-wrapper img.slider-img:last").addClass('active');
        }
    }
    else if (step === 1) {

        if (jQuery("#slider .images-wrapper .images-list img.slider-img.active").next().length > 0) {
            jQuery("#slider .images-wrapper .images-list img.slider-img.active").removeClass('active').next().addClass('active');
        }
        else {
            jQuery("#slider .images-wrapper .images-list img.slider-img.active").removeClass('active');
            jQuery("#slider .images-wrapper .images-list img.slider-img:first").addClass('active');
        }

        if (jQuery("#slider .thumbnails-wrapper img.slider-img.active").next().length > 0) {
            jQuery("#slider .thumbnails-wrapper img.slider-img.active").removeClass('active').next().addClass('active');
        }
        else {
            jQuery("#slider .thumbnails-wrapper img.slider-img.active").removeClass('active');
            jQuery("#slider .thumbnails-wrapper img.slider-img:first").addClass('active');
        }
    }

};
//modal
// jQuery(document).on("click", "#overlay, #modal-container, #modal-container-fixed, #modal-container-fixed span", function (e) {
    // if (e.target.nodeName === 'DIV' && jQuery(e.target).closest(".inner-modal").length == 0) {
        // closeModal();
    // }
// });
// jQuery(document).on("click", "#overlay, #modal-container, #text-modal", function (e) {
    // if (e.target.nodeName === 'DIV' && jQuery(e.target).closest(".inner-modal").length == 0) {
        // closeModal();
    // }
// });
jQuery(document).on("click", ".close-modal", function (e) {
    e.preventDefault();
    closeModal();
});
var closeModal = function () {
    jQuery("#modal-container").hide();
    jQuery("#overlay").hide();
    jQuery('#overlay').removeClass('black');
};
var showModal = function (content) {
    jQuery("html, body").animate({scrollTop: 0}, "fast");
    jQuery("#modal-container").html(content).show();
    jQuery("#overlay").show();
};
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

/*typeahead*/
var items_ajax = new Bloodhound({
    datumTokenizer: function (datum) {
        return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        wildcard: '%QUERY',
        url: theme_object.ajax_url + '?name=%QUERY&action=ajax_get_gigs&security=' + theme_object.ajax_nonce + '&meta=1',
        transform: function (response) {
            // Map the remote source JSON array to a JavaScript object array
            var r = jQuery.map(response, function (item) {
                return {
                    value: item.title,
                    id: item.id,
                    price: item.price,
                    permalink: item.permalink,
                    category: item.category,
                };
            });
            return r;
        }
    }
});

/*end typeahead*/

jQuery(document).ready(function ($) {
    //counterUp
    if(jQuery('.counter').length ){
        jQuery('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    }

    //svgImgToSvg();
    bindStickyMenu();

	
    // Instantiate the Typeahead UI
    jQuery('.search-gig, .search-gig-two').typeahead(null, {
        display: 'value',
        displayKey: 'id',
        limit: 10,
        source: items_ajax,
        templates: {
            empty: [
                '<div class="sugestion empty">',
                'No GIGs match your search!',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return '<a href ="' + data.permalink + '"  class="sugestion-link cat-' + data.category + '"><div class="sugestion">' + data.value + '<span>' + data.price + '<span></div></a>';
            }
        }
    });

    $('.open-category-menu.gigs-cat').click(function () {
        // $('#category-div .container .left').show();

        categoriesMenuModal();
    });
	$('.open-category-menu.roles-cat').click(function () {
        // $('#category-div .container .left').show();

        rolesCategoriesMenuModal();
    });
    $('.close-category-menu').click(function () {
        // $('#category-div .container .left').hide();
        closeModal();
    });

    $('.open-main-menu').click(function () {
        mainMenuModal();
    });

    $('#gig-detail-third .box').click(function () {
        if ($(this).css('float') === "none") {
            $('p', this).slideToggle();
        }
    });
	
	$('#role-detail-third .box').click(function () {
        if ($(this).css('float') === "none") {
            $('p', this).slideToggle();
        }
    });

});


jQuery(document).on("click", jQuery(document), function () {
    jQuery('.progress-step.done').click(function () {
        index = jQuery(this).index();
        showModal(getQuotePageHtml(index));
    });

    jQuery('.summary-sidebar .hide-show-details').click(function (e) {
        e.preventDefault();
        jQuery('.summary-box table').toggle();
        jQuery(this).toggleClass("expanded");
        if (jQuery(this).hasClass("expanded")) {
            jQuery(this).html("Hide details <i class='fa fa-caret-up' aria-hidden='true'></i>");
        } else {
            jQuery(this).html("Show details <i class='fa fa-caret-down' aria-hidden='true'></i>");
        }
        return false;
    });

    jQuery('#custom-journey-8 .show-details').click(function (e) {
        e.preventDefault();
        jQuery('#custom-journey-8 .summary-results').toggle();
        jQuery(this).toggleClass("expanded");
        if (jQuery(this).hasClass("expanded")) {
            jQuery(this).html("Hide details <i class='fa fa-caret-up' aria-hidden='true'></i>");
        } else {
            jQuery(this).html("Show details <i class='fa fa-caret-down' aria-hidden='true'></i>");
        }
        return false;
    });
});

/* ROLES functions */
var getAjaxRoles = function (skip) {
    rolesLoading = true;

    jQuery("#content-loading").show();
    
    if (!skip)
    {
        jQuery("#roles-ajax").html('');
        skip = 0;
    }

    var categories = jQuery.map(jQuery('#category-role-filter li a.active'), function (el) {
        return jQuery(el).parent().data('catid');
    });
    var data = {
        action: 'ajax_get_roles',
        security: theme_object.ajax_nonce,
        meta: 1,
        categories: categories.join("|"),
        skip: skip,
        count: 6
    };
    jQuery.getJSON(theme_object.ajax_url, data, function (items) {
        var $load_more=jQuery("#load-more-roles");

        if($load_more.lenght > 0) {
            load_more_clone = $load_more.clone(true);
        }
        var show_load_more=false;
        var remaining=0;

        jQuery("#content-loading").hide();
        $load_more.remove();

        if (items.length > 0)
        {
            show_load_more=true;
        }       
        
        jQuery.each(items, function() {
                var item=this;   
                remaining=item.remaining;
                
                var role= '<div id="role" class="col-flex-6 col-flex-md-4  grid-sizer quote-box ' + item.color + '-border">' +
                        '<a href="' + item.permalink + '" class="qb-wrap">' +
                            '<h3>' + item.title + '</h3>' +
                            '<h5 class="subcategory">' + item.category_name + '&nbsp;</h5>' +
                            '<div class="excerpt">' + item.description + '</div>' +
                            '<i><img src="' + theme_object.template_directory  +'/images/arrow-right.svg" alt="" /></i>' +
                        '</a>' +
                        '</div>';

                var $role=jQuery(role);
              
                // append elements to container
                $containerRole.append( $role);
                
                $containerRole = jQuery('.grid-items');
                $containerRole.isotope({
                    percentPosition: true,
                      itemSelector: '.quote-box',
                      masonry: {
                          columnWidth: '.grid-sizer'
                      }
                });
                // add and lay out newly appended elements
                $containerRole.isotope( 'appended', $role );                    
        });   
        if(remaining > 0){
            jQuery(".load-more-row").html(load_more_clone);
            if(show_load_more){
                jQuery("#load-more-roles").show();
            }
            else {
                jQuery("#load-more-roles").hide();
            }
        }
        rolesLoading = false;
    });
};

var loadMoreRoles = function () {
    var skip = jQuery("#roles-ajax #role").length;
    getAjaxRoles(skip);
};

jQuery(document).on("click", "#category-role-filter li", function (e) {
    e.preventDefault();

    if (rolesLoading)
        return;

    jQuery(this).find('a:first').toggleClass('active');

    getAjaxRoles();

});

var rolesCategoriesMenuModal = function () {

    var modalBox = "<div id='categories-menu-modal' class='inner-modal'>";
    modalBox += "<img class='svg logo' src='" + theme_object.template_directory + "/img/logo.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<img class='svg close-modal' src='" + theme_object.template_directory + "/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";
    modalBox += "<div class='text-wrapper'> ";
    modalBox += jQuery("#role-category-div .left").html();
    modalBox += "</div> ";
    modalBox += "<div class='show-results' onclick='closeModal()'>Show Results</div>";
    modalBox += "</div> ";

    showModal(modalBox);
};

// var submitApplication = function (form_data) {
var submitApplication = function () {
	
	var importFiles = jQuery('#portfolio')[0].files[0];	
	var form_data = new FormData();

	form_data.append('security',theme_object.ajax_nonce);
	form_data.append('action','ajax_subscribe');
	form_data.append('type','apply-for-job');
	form_data.append('name',jQuery("#_form_3_ input[name=fullname]").val());
	form_data.append('email',jQuery("#_form_3_ input[name=email]").val());
	form_data.append('website',jQuery("#_form_3_ input[name=field\\[24\\]]").val());
	form_data.append('tongue',jQuery("#_form_3_ input[name=field\\[25\\]]").val());
	form_data.append('linkedin',jQuery("#_form_3_ input[name=field\\[26\\]]").val());
	form_data.append('file', importFiles);
	
		
    jQuery.ajax({
        url: theme_object.ajax_url,
        type: 'post',
		contentType: false,
		cache: false,
		processData: false,
		data : form_data,
		
        success: function (data) {
			// console.log('before check: '+data);
            if (data==1) {
				// console.log(data);
                thankYouModalRole(jQuery("#_form_3_ input[name=fullname]").val());
				
				jQuery('#_form_3_ input').val('');
				jQuery('#upload_portfolio').html('Upload');
				
				jQuery(document).live("pagebeforechange", function(e,ob) {
					if(ob.toPage && (typeof ob.toPage==="string") && ob.toPage.indexOf('index.html') >= 0) {
						console.log("blocking the back");
						e.preventDefault();
					}
				});
            }
            else {
                jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
                jQuery("#modal-container button i").hide();
            }
        },
        error: function (errorThrown) {
            jQuery("#modal-error").html("Something goes wrong. Please try again or use other contact method!");
            jQuery("#modal-container button i").hide();
        }
    });
};
/* END of ROLES functions */


/*BEGIN of Common Nicola functions*/
function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}
/*END of Common Nicola functions*/


/*BEGIN of BOT functions*/
jQuery(document).ready(function ($) {
    if(window.location.host.indexOf('staging') === -1)
    {
    var get_params = getSearchParameters();

	 if( $('#motionbot').length )    
	 {
		document.getElementById("motionbot").src = "https://api.motion.ai/webchat/91621?color=3588eb&sendBtn=SEND&inputBox=Type%20something...&token=122d2f8ea8959e0702a0584350dadd8f&ref="+ get_params.email;
	 }
    }
});	
/*END of BOT functions*/


/*BEGIN of Gig Call functions*/
//https://hooks.zapier.com/hooks/catch/3353596/g481dr/?hash=1598eb981b1f403b492df8826d93bb47&feedback=yes
jQuery(document).ready(function ($) {
	var get_params = getSearchParameters();

	$("#call_response_success").hide();
	$("#call_response_failed").hide();

	if (get_params.hash!== undefined){

		$.ajax({
			url: "https://hooks.zapier.com/hooks/catch/3353596/g481dr/",
			type: "POST",
			data: {
				"hash":get_params.hash,
				"feedback":get_params.feedback
			},
			success: function() {
				$("#call_response_success").show();
				console.log("ok");
			},
			error: function() {
				$("#call_response_failed").show();
				console.log("ok");
			}
		}); 
	}
});	
/*END of BOT functions*/

jQuery(document).ready(function ($) {
    var refreshIntervalId=0;
    
    jQuery('body').on('click', '#continue-quiz', function (e) {
        $(".vo_global_notification_container").remove();
    }); 
    
    function checkForUncompleteUserTestFlow(){
        var $content = $("body");
        
        $.ajax({
            type: "GET",
            data: {user_id: theme_object.user_id,lang_code: theme_object.lang_code },
            dataType: "html",
            url: theme_object.template_directory + "/ajax/quizTestflowHandler.php",
            success: function (data) {
                $data = $(data);
                $(".vo_global_notification_container").remove();
                if ($data.length) {
                   $content.append($data);
                   //clearInterval(refreshIntervalId);
                } 
                else{
                    clearInterval(refreshIntervalId);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
    }
    
    checkForUncompleteUserTestFlow();
    
    refreshIntervalId = setInterval(checkForUncompleteUserTestFlow, 20000);
});
