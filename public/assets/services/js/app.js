jQuery(document).ready(function ($) {

    $("#slick-slider").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $("#slick-slider-2").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $("#slick-slider-3").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $("#slick-slider-4").slick({
        // autoplay: true,
        dots: false,
        arrows: false,

        slidesToShow: 1.1,
        slidesToScroll: 1
    });

    $('.sidebar-menu a').click(function(){

        if ($(this).hasClass('active')) {
            $('.sidebar-menu a').removeClass('active');
            $grid.isotope({ filter: '' });
        } else {
            $('.sidebar-menu a').removeClass('active');
            $(this).addClass('active');
            $grid.isotope({ filter: '.'+$(this).attr('rel') });
        }

    });

    $grid = jQuery('.grid-items').isotope({
        percentPosition: true,
        itemSelector: '.quote-box',
        masonry: {
            columnWidth: '.grid-sizer'
        }

    });

    $('.append-button').on( 'click', function() {
        // create new item elements
        var $items = getItemElement().add( getItemElement() ).add( getItemElement() );
        // append elements to container
        $grid.append( $items )
        // add and lay out newly appended elements
            .isotope( 'appended', $items );
    });


    // make <div class="grid-item grid-item--width# grid-item--height#" />
    function getItemElement() {
        var $item = $('<div class="col-flex-6 col-flex-md-4 grid-sizer quote-box">\n' +
            '                        <a href="javascript:void(0);" class="qb-wrap">\n' +
            '                            <h3>Hotjar analysis</h3>\n' +
            '                            <strong>$ 302 / Mo</strong>\n' +
            '                            <span class="quote-star"><img src="assets/images/star-full.svg" alt="star" /><img src="assets/images/star-full.svg" alt="star" /><img src="assets/images/star-full.svg" alt="star" /><img src="assets/images/star-empty.svg" alt="star" /></span>\n' +
            '                            <span class="quote-review">9 Reviews</span>\n' +
            '                            <div class="excerpt">\n' +
            '                                Hotjar is a solution that allows marketers and designers to see how visitors are using their site pages lorem ipsum dolor sit a….\n' +
            '                            </div>\n' +
            '                            <i><img src="assets/images/arrow-right.svg" alt="" /></i>\n' +
            '                        </a>\n' +
            '                    </div>');
        // add width and height class
        var wRand = Math.random();
        var hRand = Math.random();
        var widthClass = wRand > 0.85 ? 'blue-border' : wRand > 0.7 ? 'yellow-border' : 'red-border';

        $item.addClass( widthClass );
        return $item;
    }




});

