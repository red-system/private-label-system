$(document).ready(function () {

    $('[name="metode_pembayaran_hutang"]').change(function () {
        var metode_pembayaran_hutang = $(this).val();

        if (metode_pembayaran_hutang == 'hutang_giro') {
            $('.part-hutang-giro').removeClass('hidden');
        } else {
            $('.part-hutang-giro').addClass('hidden');
        }
    });

    $('.qty-return').keyup(function () {
        var total_qty_return = 0;
        $('.qty-return').each(function (index, self) {
            var qty_return = strtonumber($(this).val());

            if (qty_return) {
                total_qty_return = parseFloat(total_qty_return) + parseFloat(qty_return);
            }

        });

        $('.total-qty-return').html(format_number(total_qty_return));
    });

});