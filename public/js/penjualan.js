$(document).ready(function () {

    add_row_produk();
    produk_stok();
    change_biaya_tambahan();
    change_potongan_akhir();
    change_pajak();
    change_diskon_persen();

    btn_search_produk();
    delete_row_produk();
    proses_lokasi_lama();

    change_lokasi();
    change_qty();
    change_potongan();
    change_ppn_nominal();
    proses_sub_total_lama()

});

var base_url = $('#base-value').data('base-url');
var routeSalesOrderStok = $('#base-value').data('route-sales-order-stok');
var routeSalesOrderLokasi = $('#base-value').data('route-sales-order-lokasi');
var routeSalesOrderHarga = $('#base-value').data('route-sales-order-harga');
var _token = $('#base-value').data('csrf-token');
var method_proses = $('#base-value').data('method-proses');


function add_row_produk() {
    $('.btn-add-row-produk').click(function () {
        var row_produk = $('.row-produk tbody').html();
        $('.table-produk tbody').append(row_produk);

        var index = $('.table-produk tbody tr').length;

        $('.table-produk tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-produk tbody tr:nth-child(' + index + ') .touchspin-penjualan').TouchSpin(touchspin_number);
        $('.table-produk tbody tr:nth-child(' + index + ') .select2-penjualan').select2();

        btn_search_produk();
        delete_row_produk();
    });
}

function delete_row_produk() {
    $('.btn-delete-row-produk').click(function () {
        $(this).parents('tr').remove();
        proses_total();
        proses_grand_total();

        if (method_proses == 'edit') {
            var id_penjualan_produk = $(this).parents('tr').data('id-penjualan-produk');
            var id_penjualan_produk_delete = $('[name="id_penjualan_produk_delete"]').val();
            var id_penjualan_produk_val = id_penjualan_produk + ',' + id_penjualan_produk_delete;

            $('[name="id_penjualan_produk_delete"]').val(id_penjualan_produk_val);
        }
    });
}

function produk_stok() {

    $('.btn-produk-stok').click(function () {
        var id_barang = $(this).parents('tr').data('id');
        $.ajax({
            beforeSend: loading_start(),
            url: routeSalesOrderStok,
            type: 'POST',
            data: {
                id_barang: id_barang,
                _token: _token
            },
            success: function (view) {
                $('#modal-produk-stok .modal-body').html(view);
                $('#modal-produk-stok').modal('show');
                loading_finish();
            }
        });
    });
}

function btn_search_produk() {
    $('.btn-search-produk').click(function () {
        $('#modal-produk').modal('show');
        var index = $(this).parents('tr').data('index');
        $('#base-value').data('index', index);
        btn_select_produk();
    });
}

function btn_select_produk() {
    $('.datatable-general').on('click', '.btn-select-produk', function () {

        var index = $('#base-value').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');
        var id_barang = $(this).parents('tr').data('id');
        var kode_barang = $(this).parents('tr').data('kode-barang');
        var nama_barang = $(this).parents('tr').data('nama-barang');

        $('.table-produk tbody tr:nth-child(' + index + ') td.produk-nama').html(kode_barang + ' ' + nama_barang);
        $('.table-produk tbody tr:nth-child(' + index + ') .id_barang').val(id_barang);

        enabled_select_lokasi(index);


        proses_lokasi(index, id_barang);
        proses_harga(index);
        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();

        change_lokasi();
        change_qty();
        //change_no_seri_produk();
        change_potongan();
        change_ppn_nominal();


        //self.parents('td').siblings('td.produk-nama').html(kode_produk+' '+nama_produk);
        //self.parents('td').siblings('td.data').children('[name="id_produk[]"]').val(id_produk);

        $('#modal-produk').modal('hide');

    });
}

function enabled_select_lokasi(index) {
    $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').removeAttr('disabled');
    $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').val("").trigger('change');
}

function change_lokasi() {
    $('.id_lokasi').change(function () {
        var index = $(this).parents('tr').data('index');
    });
}

function change_qty() {
    $('.qty').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');

        proses_harga(index);
        proses_ppn(index);
        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_potongan() {
    $('.potongan').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');

        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_ppn_nominal() {
    $('.ppn_nominal').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');

        proses_harga_net(index);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_biaya_tambahan() {
    $('[name="biaya_tambahan"]').on('change keyup', function () {
        proses_grand_total();
    });
}

function change_pajak() {
    $('[name="pajak"]').on('change keyup', function () {
        proses_grand_total();
    });
}

function change_diskon_persen() {
    $('[name="diskon_persen"]').on('change keyup', function () {
        proses_grand_total();
    });
}


function change_potongan_akhir() {
    $('[name="potongan_akhir"]').on('change keyup', function () {
        proses_grand_total();
    });
}

function proses_lokasi(index, id_barang) {
    var data_send = {
        id_barang: id_barang,
        _token: _token
    };

    $.ajax({
        url: routeSalesOrderLokasi,
        type: 'post',
        data: data_send,
        success: function (data) {
            $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').html('');
            $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi')

                .append($("<option></option>")
                    .attr("value", "")
                    .text('Pilih Lokasi'));
            $.each(data, function (key, value) {
                console.log(value);
                $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi')
                    .append($("<option></option>")
                        .attr("value", value.id_lokasi)
                        .attr('data-satuan', value.satuan)
                        .attr('data-id', value.id)
                        .attr('data-stok', value.jml_barang)
                        .text(value.lokasi.lokasi));

            });
        }
    });
}

$(".table-produk").on("change", ".id_lokasi", function () {
    var index = $('#base-value').data('index');
    var id_stok = $(this).find(':selected').data('id');
    var stok = $(this).find(':selected').data('stok');
    var satuan = $(this).find(':selected').data('satuan');

    $('.table-produk tbody tr:nth-child(' + index + ') .td-satuan').html(satuan);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-stok').html(stok);
    $('[name="satuan[]"]').val(satuan);
    $('[name="id_stok[]"]').val(id_stok);
});

function proses_lokasi_lama() {

    $(".table-produk").on("change", ".id_lokasi_lama", function () {
        var index = $(this).parents('tr').data('index');
        // var lokasi = $('#base-value').data('index');
        // var lokasi = $(this).attr('id-penjualan-produk');
        var data = $(this).parents('tr').data('id-penjualan-produk');
        // console.log(data)
        var id_stok = $(this).find(':selected').data('id');
        var stok = $(this).find(':selected').data('stok');
        var satuan = $(this).find(':selected').data('satuan');

        $('.table-produk tbody tr:nth-child(' + index + ') .td-satuan').html(satuan);
        $('.table-produk tbody tr:nth-child(' + index + ') .td-stok').html(stok);
        $('[name="satuan[' + data + ']"]').val(satuan);
        $('[name="id_stok[' + data + ']"]').val(id_stok);
    });
}

function proses_harga(index) {
    var id_barang = $('.table-produk tbody tr:nth-child(' + index + ') .id_barang').val();
    var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty').val();
    var data_send = {
        _token: _token,
        id_barang: id_barang,
        qty: qty
    };
    console.log(data_send)
    $.ajax({
        url: routeSalesOrderHarga,
        type: "post",
        data: data_send,
        success: function (data) {
            $('.table-produk tbody tr:nth-child(' + index + ') .harga').val(data.number_raw);
            $('.table-produk tbody tr:nth-child(' + index + ') .td-harga').html(data.number_format);

            proses_ppn(index);
            proses_harga_net(index);
            proses_sub_total(index);
            proses_total();
            proses_grand_total();
        }
    });
}


function proses_ppn(index) {
    $('.table-produk tbody tr:nth-child(' + index + ') .ppn_nominal').val();
    //$('.table-produk tbody tr:nth-child(' + index + ') .td-ppn').html(ppn_format);
}

function proses_harga_net(index) {
    var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').val();
    var potongan = $('.table-produk tbody tr:nth-child(' + index + ') .potongan').val();
    var ppn_nominal = $('.table-produk tbody tr:nth-child(' + index + ') .ppn_nominal').val();

    var diskon = parseFloat(harga) * parseFloat(ppn_nominal) / 100;
    var harga_net = parseFloat(harga) - parseFloat(potongan) - parseFloat(diskon);


    var harga_net_format = format_number(harga_net);
    console.log(harga_net)
    $('[name="harga_net[]"]').val(harga_net);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-harga-net').html(harga_net_format);

}

function proses_sub_total_lama() {
    $('.qty-retur').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');

        var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').html();

        var nominal = $('.table-produk tbody tr:nth-child(' + index + ') .nominal').html();
        var persen = $('.table-produk tbody tr:nth-child(' + index + ') .persen').html();
        var diskon = parseFloat(harga) * parseFloat(persen) / 100;
        var harga_net = parseFloat(harga) - parseFloat(nominal) - parseFloat(diskon);

        var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty-retur').val();
        var sub_total = parseFloat(harga_net) * parseFloat(qty);
        var sub_total_format = format_number(sub_total);

        $('.table-produk tbody tr:nth-child(' + index + ') .sub_total').val(sub_total);
        $('.table-produk tbody tr:nth-child(' + index + ') .td-sub-total').html(sub_total_format);
        $('[name="sub_total[' + data + ']"]').val(sub_total);

        proses_total();
    });
}

function proses_sub_total(index) {
    var data = $(this).parents('tr').data('id-penjualan-produk');
    var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').val();
    var potongan = $('.table-produk tbody tr:nth-child(' + index + ') .potongan').val();
    var ppn_nominal = $('.table-produk tbody tr:nth-child(' + index + ') .ppn_nominal').val();
    var diskon = parseFloat(harga) * parseFloat(ppn_nominal) / 100;
    var harga_net = parseFloat(harga) - parseFloat(potongan) - parseFloat(diskon);

    var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty').val();
    var sub_total = parseFloat(harga_net) * parseFloat(qty);
    var sub_total_format = format_number(sub_total);

    $('.table-produk tbody tr:nth-child(' + index + ') .sub_total').val(sub_total);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-sub-total').html(sub_total_format);
    $('[name="sub_total[' + data + ']"]').val(sub_total);
}

function proses_total() {
    var total = 0;
    $('.table-produk .sub_total').each(function () {
        var sub_total = $(this).val();
        total = parseFloat(total) + parseFloat(sub_total);
    });

    $('[name="total"]').val(total);
    $('.total').html(format_number(total));
}

function proses_grand_total() {
    var total = $('[name="total"]').val();
    var diskon_persen = $('[name="diskon_persen"]').val();
    var biaya_tambahan = $('[name="biaya_tambahan"]').val();
    var pajak = $('[name="pajak"]').val();
    var pajak_persen = parseFloat(total) * parseFloat(pajak) / 100;
    var potongan_akhir = $('[name="potongan_akhir"]').val();
    var diskon = parseFloat(total) * parseFloat(diskon_persen) / 100;
    var grand_total = parseFloat(total) + parseFloat(biaya_tambahan) + parseFloat(pajak_persen) - parseFloat(diskon) - parseFloat(potongan_akhir);

    //console.log(total+' '+biaya_tambahan+' '+potongan_akhir+' '+grand_total);

    if (!isNaN(total)) {
        $('[name="grand_total"]').val(grand_total);
        $('.grand-total').html(format_number(grand_total));
    }
}
