$(document).ready(function () {

    select_supplier();
    add_row_barang();
    produk_stok();
    change_discount_total_po();
    change_discount_nominal_total_po();
    change_pajak_persen_po();
    change_ongkos_kirim();
    change_dp_po();
    btn_search_produk();

    // Saat Edit
    change_proses_table_barang();
    delete_row_produk();

});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');
var routeSalesOrderStok = $('#base-value').data('route-sales-order-stok');
var method_proses = $('#base-value').data('method-proses');

function select_supplier() {
    $('.datatable-general').on('click', '.select-supplier', function () {
        var id = $(this).data('id');
        var kode_supplier = $(this).data('kode-supplier');
        var supplier = $(this).data('supplier');
        var supplier_alamat = $(this).data('supplier-alamat');
        var supplier_kontak = $(this).data('supplier-kontak');

        $('[name="id_supplier"]').val(id);
        $('[name="supplier"]').val(supplier);

        $('#modal-supplier').modal('hide');

    });
}

function add_row_barang() {
    $('.btn-add-row-barang').click(function () {
        var row_produk = $('.row-produk tbody').html();
        $('.table-barang tbody').append(row_produk);

        var index = $('.table-barang tbody tr').length;

        $('.table-barang tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-barang tbody tr:nth-child(' + index + ') .select2').select2();

        input_numeral();
        btn_search_produk();
        delete_row_produk();
        change_kalkulasi(index);
    });
}

function delete_row_produk() {
    $('.btn-delete-row-produk').click(function () {
        $(this).parents('tr').remove();
        proses_total_po();
        proses_discount_nominal_total_po();
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();

        if (method_proses == 'edit') {
            var id_penjualan_produk = $(this).parents('tr').data('id-penjualan-produk');
            var id_penjualan_produk_delete = $('[name="id_penjualan_produk_delete"]').val();
            var id_penjualan_produk_val = id_penjualan_produk + ',' + id_penjualan_produk_delete;

            $('[name="id_penjualan_produk_delete"]').val(id_penjualan_produk_val);
        }
    });
}

function change_kalkulasi(index) {
    $('.table-barang tbody tr:nth-child(' + index + ') .harga').keyup(function () {
        proses_discount_nominal(index);
        proses_sub_total(index);
        proses_total_po();
        // proses_discount_nominal_total_po();
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
    $('.table-barang tbody tr:nth-child(' + index + ') .discount').keyup(function () {
        proses_discount_nominal(index);
        proses_sub_total(index);
        proses_total_po();
        // proses_discount_nominal_total_po();
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
    $('.table-barang tbody tr:nth-child(' + index + ') .qty').keyup(function () {
        proses_sub_total(index);
        proses_total_po();
        // proses_discount_nominal_total_po();
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_discount_total_po() {
    $('.discount-persen-po').keyup(function () {
        // proses_discount_nominal_total_po();
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_discount_nominal_total_po() {
    $('.discount-nominal-po').keyup(function () {
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_pajak_persen_po() {
    $('.pajak-persen-po').keyup(function () {
        proses_pajak_persen_nominal_po();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_ongkos_kirim() {
    $('.ongkos-kirim-po').keyup(function () {
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_dp_po() {
    $('.dp-po').keyup(function () {
        proses_sisa_pembayaran();
    });
}

function proses_discount_nominal(index) {
    var harga = $('.table-barang tbody tr:nth-child(' + index + ') .harga').val();
    harga = strtonumber(harga);
    var discount = $('.table-barang tbody tr:nth-child(' + index + ') .discount').val();
    discount = strtonumber(discount);
    var qty = $('.table-barang tbody tr:nth-child(' + index + ') .qty').val();

    var discount_nominal = (parseFloat(harga) * (parseFloat(discount)) / 100) * qty;
    var discount_nominal_last = parseFloat(harga) - discount_nominal;

    $('.table-barang tbody tr:nth-child(' + index + ') .td-discount-nominal').html(format_number(discount_nominal));
    $('.table-barang tbody tr:nth-child(' + index + ') .data .discount-nominal').val(discount_nominal);
}

function proses_sub_total(index) {
    var harga = $('.table-barang tbody tr:nth-child(' + index + ') .harga').val();
    harga = strtonumber(harga);
    //console.log('harga', harga);
    var discount_nominal = $('.table-barang tbody tr:nth-child(' + index + ') .data .discount-nominal').val();
    var qty = $('.table-barang tbody tr:nth-child(' + index + ') .qty').val();
    var sub_total = (parseFloat(harga) * parseFloat(qty)) - parseFloat(discount_nominal);

    $('.table-barang tbody tr:nth-child(' + index + ') .td-sub-total').html(format_number(sub_total));
    $('.table-barang tbody tr:nth-child(' + index + ') .data .sub-total').val(sub_total);
}

function proses_total_po() {
    var total_po = 0;
    $('.table-barang .sub-total').each(function () {
        var sub_total = $(this).val();
        total_po = parseFloat(total_po) + parseFloat(sub_total);
    });

    $('[name="total_po"]').val(total_po);
    $('.total-po').html(format_number(total_po));
}

function proses_discount_nominal_total_po() {
    var total_po = $('[name="total_po"]').val();
    var discount_total_po = strtonumber($('.discount-persen-po').val());
    var diskon_nominal = parseFloat(total_po) * (parseFloat(discount_total_po) / 100);
    var diskon_nominal_hasil = parseFloat(total_po) - diskon_nominal;

    //console.log('proses_discount_nominal_total_po', total_po, discount_total_po, diskon_nominal, diskon_nominal_hasil);

    $('[name="discount_nominal_po"]').val(format_number(diskon_nominal));
    $('.discount-nominal-po').html(format_number(diskon_nominal));
}

function proses_pajak_persen_nominal_po() {
    var total_po = strtonumber($('[name="total_po"]').val());
    var discount_persen_po = strtonumber($('[name="discount_persen_po"]').val());
    var discount_persen_nominal_po = parseFloat(total_po) * (discount_persen_po / 100);
    var discount_nominal_po = strtonumber($('[name="discount_nominal_po"]').val());
    var pajak_persen_po = strtonumber($('[name="pajak_persen_po"]').val());

    var total_temp = parseFloat(total_po) - parseFloat(discount_persen_nominal_po) - parseFloat(discount_nominal_po);

    var pajak_nominal_po = parseFloat(total_temp) * (parseFloat(pajak_persen_po) / 100);

    //console.log('proses_pajak_persen_nominal_po', total_po, discount_persen_po, discount_persen_nominal_po, discount_nominal_po, pajak_persen_po, total_temp, pajak_nominal_po);


    $('.pajak-nominal-po').html(format_number(pajak_nominal_po));
    $('[name="pajak_nominal_po"]').val(pajak_nominal_po);

}

function proses_grand_total() {
    var total_po = $('[name="total_po"]').val();
    var discount_po = strtonumber($('[name="discount_persen_po"]').val());
    var discount_po_nominal = parseFloat(total_po) * (parseFloat(discount_po) / 100);
    var discount_nominal_po = strtonumber($('[name="discount_nominal_po"]').val());
    var pajak_nominal_po = strtonumber($('[name="pajak_nominal_po"]').val());
    var ongkos_kirim_po = strtonumber($('[name="ongkos_kirim_po"]').val());
    var grand_total_po = parseFloat(total_po) - parseFloat(discount_po_nominal) - parseFloat(discount_nominal_po) + parseFloat(pajak_nominal_po) + parseFloat(strtonumber(ongkos_kirim_po));

    console.log('grand_total', total_po, discount_po, discount_po_nominal, discount_nominal_po, pajak_nominal_po, ongkos_kirim_po, grand_total_po);

    $('.grand-total-po').html(format_number(grand_total_po));
    $('[name="grand_total_po"]').val(grand_total_po);
}

function proses_sisa_pembayaran() {
    var grand_total_po = $('[name="grand_total_po"]').val();
    var dp_po = $('.dp-po').val();
    var sisa_pembayaran_po = parseFloat(grand_total_po) - parseFloat(strtonumber(dp_po));

    $('[name="sisa_pembayaran_po"]').val(sisa_pembayaran_po);
    $('.sisa-pembayaran-po').html(format_number(sisa_pembayaran_po));
}


function produk_stok() {
    $('.btn-produk-stok').click(function () {
        var id_barang = $(this).parents('tr').data('id');
        $.ajax({
            beforeSend: function () {
                loading_start();
            },
            url: routeSalesOrderStok,
            type: 'POST',
            data: {
                id_barang: id_barang,
                _token: _token
            },
            success: function (view) {
                $('#modal-produk-stok .modal-body').html(view);
                $('#modal-produk-stok').modal('show');
                loading_finish();
            }
        });
    });
}

function btn_search_produk() {
    $('.btn-search-produk').click(function () {
        $('#modal-produk').modal('show');
        var index = $(this).parents('tr').data('index');
        $('#base-value').data('index', index);
        btn_select_produk();
    });
}

function btn_select_produk() {
    $('.datatable-general').on('click', '.btn-select-produk', function () {

        var index = $('#base-value').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');
        var id_barang = $(this).parents('tr').data('id');
        var kode_barang = $(this).parents('tr').data('kode-barang');
        var nama_barang = $(this).parents('tr').data('nama-barang');

        $('.table-barang tbody tr:nth-child(' + index + ') td.produk-nama').html('(' + kode_barang + ') ' + nama_barang);
        $('.table-barang tbody tr:nth-child(' + index + ') .id-barang').val(id_barang);
        $('.table-barang tbody tr:nth-child(' + index + ') .qty').val(1);
        $('.table-barang tbody tr:nth-child(' + index + ') .harga').val(0);
        $('.table-barang tbody tr:nth-child(' + index + ') .discount').val(0);
        $('.table-barang tbody tr:nth-child(' + index + ') .m-select2').val("").trigger('change');

        $('#modal-produk').modal('hide');

    });
}

function change_proses_table_barang() {
    $('.table-barang tr').each(function (key, tr) {
        // var index = tr.data('index');
        var index = $(this).data('index');
        if (index) {
            change_kalkulasi(index);
        }
    });
}


//
//
//
// function enabled_select_lokasi(index) {
//     $('.table-barang tbody tr:nth-child(' + index + ') .id_lokasi').removeAttr('disabled');
//     $('.table-barang tbody tr:nth-child(' + index + ') .id_lokasi').val("").trigger('change');
// }
//
// function change_lokasi() {
//     $('.id_lokasi').change(function () {
//         var index = $(this).parents('tr').data('index');
//     });
// }
//
// function change_qty() {
//     $('.qty').on('change keyup', function () {
//         var index = $(this).parents('tr').data('index');
//         var data = $(this).parents('tr').data('id-penjualan-produk');
//
//         proses_harga(index);
//         proses_ppn(index);
//         proses_harga_net(index, data);
//         proses_sub_total(index);
//         proses_total();
//         proses_grand_total();
//     });
// }
//
// function change_potongan() {
//     $('.potongan').on('change keyup', function () {
//
//         var index = $(this).parents('tr').data('index');
//         var data = $(this).parents('tr').data('id-penjualan-produk');
//
//         proses_harga_net(index, data);
//         proses_sub_total(index);
//         proses_total();
//         proses_grand_total();
//     });
// }
//
// function change_ppn_nominal() {
//     $('.ppn_nominal').on('change keyup', function () {
//
//         var index = $(this).parents('tr').data('index');
//         var data = $(this).parents('tr').data('id-penjualan-produk');
//
//         proses_harga_net(index, data);
//         proses_sub_total(index);
//         proses_total();
//         proses_grand_total();
//     });
// }
//
// function change_biaya_tambahan() {
//     $('[name="biaya_tambahan"]').on('change keyup', function () {
//         proses_grand_total();
//     });
// }
//
// function change_pajak() {
//     $('[name="pajak"]').on('change keyup', function () {
//         proses_grand_total();
//     });
// }
//
// function change_diskon_persen() {
//     $('[name="diskon_persen"]').on('change keyup', function () {
//         proses_grand_total();
//     });
// }
//
//
// function change_potongan_akhir() {
//     $('[name="potongan_akhir"]').on('change keyup', function () {
//         proses_grand_total();
//     });
// }
//
// function proses_lokasi(index, id_barang) {
//     var data_send = {
//         id_barang: id_barang,
//         _token: _token
//     };
//
//     $.ajax({
//         url: routeSalesOrderLokasi,
//         type: 'post',
//         data: data_send,
//         success: function (data) {
//             $('.table-barang tbody tr:nth-child(' + index + ') .id_lokasi').html('');
//             $('.table-barang tbody tr:nth-child(' + index + ') .id_lokasi')
//
//                 .append($("<option></option>")
//                     .attr("value", "")
//                     .text('Pilih Lokasi'));
//             $.each(data, function (key, value) {
//                 $('.table-barang tbody tr:nth-child(' + index + ') .id_lokasi')
//                     .append($("<option></option>")
//                         .attr("value", value.id_lokasi)
//                         .attr('data-satuan', value.satuan)
//                         .attr('data-id', value.id)
//                         .attr('data-stok', value.jml_barang)
//                         .text(value.lokasi.lokasi));
//
//             });
//         }
//     });
// }
//
// $(".table-barang").on("change", ".id_lokasi", function () {
//     var index = $('#base-value').data('index');
//     var id_stok = $(this).find(':selected').data('id');
//     var stok = $(this).find(':selected').data('stok');
//     var satuan = $(this).find(':selected').data('satuan');
//
//     $('.table-barang tbody tr:nth-child(' + index + ') .td-satuan').html(satuan);
//     $('.table-barang tbody tr:nth-child(' + index + ') .td-stok').html(stok);
//     $('[name="satuan[]"]').val(satuan);
//     $('[name="id_stok[]"]').val(id_stok);
// });
//
// function proses_lokasi_lama() {
//
//     $(".table-barang").on("change", ".id_lokasi_lama", function () {
//         var index = $(this).parents('tr').data('index');
//         var data = $(this).parents('tr').data('id-penjualan-produk');
//         var id_stok = $(this).find(':selected').data('id');
//         var stok = $(this).find(':selected').data('stok');
//         var satuan = $(this).find(':selected').data('satuan');
//
//         $('.table-barang tbody tr:nth-child(' + index + ') .td-satuan').html(satuan);
//         $('.table-barang tbody tr:nth-child(' + index + ') .td-stok').html(stok);
//         $('[name="satuan[' + data + ']"]').val(satuan);
//         $('[name="id_stok[' + data + ']"]').val(id_stok);
//     });
// }
//
// function proses_harga(index) {
//     var data = $(this).parents('tr').data('id-penjualan-produk');
//     var id_barang = $('.table-barang tbody tr:nth-child(' + index + ') .id_barang').val();
//     var qty = $('.table-barang tbody tr:nth-child(' + index + ') .qty').val();
//     var data_send = {
//         _token: _token,
//         id_barang: id_barang,
//         qty: qty
//     };
//     $.ajax({
//         url: routeSalesOrderHarga,
//         type: "post",
//         data: data_send,
//         success: function (data) {
//             $('.table-barang tbody tr:nth-child(' + index + ') .harga').val(data.number_raw);
//             $('.table-barang tbody tr:nth-child(' + index + ') .td-harga').html(data.number_format);
//
//             proses_ppn(index);
//             proses_harga_net(index, data);
//             proses_sub_total(index);
//             proses_total();
//             proses_grand_total();
//         }
//     });
// }
//
//
// function proses_ppn(index) {
//     $('.table-barang tbody tr:nth-child(' + index + ') .ppn_nominal').val();
//     //$('.table-barang tbody tr:nth-child(' + index + ') .td-ppn').html(ppn_format);
// }
//
// function proses_harga_net(index, data) {
//     var harga = $('.table-barang tbody tr:nth-child(' + index + ') .harga').val();
//     var potongan = $('.table-barang tbody tr:nth-child(' + index + ') .potongan').val();
//     var ppn_nominal = $('.table-barang tbody tr:nth-child(' + index + ') .ppn_nominal').val();
//
//     var diskon = parseFloat(harga) * parseFloat(ppn_nominal) / 100;
//     var harga_net = parseFloat(harga) - parseFloat(potongan) - parseFloat(diskon);
//     // var data = $(this).parents('tr').data('id-penjualan-produk');
//
//
//     var harga_net_format = format_number(harga_net);
//     $('[name="harga_net[]"]').val(harga_net);
//     $('[name="harga_net[' + data + ']"]').val(harga_net);
//     $('.table-barang tbody tr:nth-child(' + index + ') .td-harga-net').html(harga_net_format);
//
// }
//
// function proses_sub_total(index) {
//     var data = $(this).parents('tr').data('id-penjualan-produk');
//     var harga = $('.table-barang tbody tr:nth-child(' + index + ') .harga').val();
//     var potongan = $('.table-barang tbody tr:nth-child(' + index + ') .potongan').val();
//     var ppn_nominal = $('.table-barang tbody tr:nth-child(' + index + ') .ppn_nominal').val();
//     var diskon = parseFloat(harga) * parseFloat(ppn_nominal) / 100;
//     var harga_net = parseFloat(harga) - parseFloat(potongan) - parseFloat(diskon);
//
//     var qty = $('.table-barang tbody tr:nth-child(' + index + ') .qty').val();
//     var sub_total = parseFloat(harga_net) * parseFloat(qty);
//     var sub_total_format = format_number(sub_total);
//
//     $('.table-barang tbody tr:nth-child(' + index + ') .sub_total').val(sub_total);
//     $('.table-barang tbody tr:nth-child(' + index + ') .td-sub-total').html(sub_total_format);
//     $('[name="sub_total[' + data + ']"]').val(sub_total);
// }
//
// function proses_total() {
//     var total = 0;
//     $('.table-barang .sub_total').each(function () {
//         var sub_total = $(this).val();
//         total = parseFloat(total) + parseFloat(sub_total);
//     });
//
//     $('[name="total"]').val(total);
//     $('.total').html(format_number(total));
// }
//
// function proses_grand_total() {
//     var total = $('[name="total"]').val();
//     var diskon_persen = $('[name="diskon_persen"]').val();
//     var biaya_tambahan = $('[name="biaya_tambahan"]').val();
//     var pajak = $('[name="pajak"]').val();
//     var pajak_persen = parseFloat(total) * parseFloat(pajak) / 100;
//     var potongan_akhir = $('[name="potongan_akhir"]').val();
//     var diskon = parseFloat(total) * parseFloat(diskon_persen) / 100;
//     var grand_total = parseFloat(total) + parseFloat(biaya_tambahan) + parseFloat(pajak_persen) - parseFloat(diskon) - parseFloat(potongan_akhir);
//
//     //console.log(total+' '+biaya_tambahan+' '+potongan_akhir+' '+grand_total);
//
//     if (!isNaN(total)) {
//         $('[name="grand_total"]').val(grand_total);
//         $('.grand-total').html(format_number(grand_total));
//     }
// }
