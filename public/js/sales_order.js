$(document).ready(function () {

    select_distributor();
    select_salesman()
    add_row_produk();
    produk_stok();
    change_ongkos_kirim();
    change_potongan_akhir();
    change_pajak();
    change_diskon_persen_akhir();

    btn_search_produk();
    delete_row_produk();
    proses_lokasi_lama();

    change_lokasi();
    change_qty();
    change_diskon_persen();
    change_diskon_nominal();


});

var base_url = $('#base-value').data('base-url');
var routeSalesOrderStok = $('#base-value').data('route-sales-order-stok');
var routeSalesOrderLokasi = $('#base-value').data('route-sales-order-lokasi');
var routeSalesOrderHarga = $('#base-value').data('route-sales-order-harga');
var routeSalesOrderNoSo = $('#base-value').data('route-sales-order-no-so');
var _token = $('#base-value').data('csrf-token');
var method_proses = $('#base-value').data('method-proses');

function select_distributor() {
    $('.datatable-general').on('click', '.select-distributor', function () {
        loading_start();
        var id = $(this).data('id');
        var kode_langganan = $(this).data('kode-langganan');
        var langganan = $(this).data('nama-langganan');
        var langganan_kontak = $(this).data('telepon-langganan');
        var langganan__alamat = $(this).data('alamat-langganan');

        $('[name="id_langganan"]').val(id);
        $('[name="kode_langganan"]').val(kode_langganan);
        $('[name="nama_langganan"]').val(langganan);
        $('.nama-langganan').html(langganan);
        $('.alamat-langganan').html(langganan__alamat);
        $('.telepon-langganan').html(langganan_kontak);

        $('#modal-langganan').modal('hide');
        loading_finish();

        proses_no_so();

    });
}

function select_salesman() {
    $('.datatable-general').on('click', '.select-salesman', function () {
        var id = $(this).data('id');
        var kode_salesman = $(this).data('kode-salesman');
        var salesman = $(this).data('nama-salesman');
        var salesman_kontak = $(this).data('telepon-salesman');
        var salesman__alamat = $(this).data('alamat-salesman');

        $('[name="id_salesman"]').val(id);
        $('[name="kode_salesman"]').val(kode_salesman);
        $('[name="nama_salesman"]').val(salesman);
        $('.alamat-salesman').html(salesman__alamat);
        $('.telepon-salesman').html(salesman_kontak);

        $('#modal-salesman').modal('hide');

    });
}

function proses_no_so() {
    var id_langganan = $('[name="id_langganan"]').val();
    var data_send = {
        id_langganan: id_langganan,
        _token: _token
    };

    $('[name="no_faktur"]').val('-');
    $('.no_faktur').html('-');
    $('[name="urutan"]').val('');

    $.ajax({
        beforeSend: loading_start(),
        url: routeSalesOrderNoSo,
        type: 'post',
        data: data_send,
        success: function (data) {

            var no_so = data.no_so;
            var urutan = data.urutan;

            $('[name="no_so"]').val(no_so);
            $('.no_so').html(no_so);
            $('[name="urutan"]').val(urutan);

            loading_finish();
        }
    })
}

function add_row_produk() {
    $('.btn-add-row-produk').click(function () {
        var row_produk = $('.row-produk tbody').html();
        $('.table-produk tbody').append(row_produk);
        $('.input-numeral').toArray().forEach(function (field) {
            new Cleave(field, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
        });
        var index = $('.table-produk tbody tr').length;

        $('.table-produk tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-produk tbody tr:nth-child(' + index + ') .touchspin-penjualan').TouchSpin(touchspin_number);
        $('.table-produk tbody tr:nth-child(' + index + ') .select2-penjualan').select2();

        btn_search_produk();
        delete_row_produk();
    });
}

function delete_row_produk() {
    $('.btn-delete-row-produk').click(function () {
        $(this).parents('tr').remove();
        proses_total();
        proses_grand_total();

        if (method_proses == 'edit') {
            var id_penjualan_produk = $(this).parents('tr').data('id-penjualan-produk');
            var id_penjualan_produk_delete = $('[name="id_penjualan_produk_delete"]').val();
            var id_penjualan_produk_val = id_penjualan_produk + ',' + id_penjualan_produk_delete;

            $('[name="id_penjualan_produk_delete"]').val(id_penjualan_produk_val);
        }
    });
}

function produk_stok() {

    $('.btn-produk-stok').click(function () {
        var id_barang = $(this).parents('tr').data('id');
        $.ajax({
            beforeSend: loading_start(),
            url: routeSalesOrderStok,
            type: 'POST',
            data: {
                id_barang: id_barang,
                _token: _token
            },
            success: function (view) {
                $('#modal-produk-stok .modal-body').html(view);
                $('#modal-produk-stok').modal('show');
                loading_finish();
            }
        });
    });
}

function btn_search_produk() {
    $('.btn-search-produk').click(function () {
        $('#modal-produk').modal('show');
        var index = $(this).parents('tr').data('index');
        $('#base-value').data('index', index);
        btn_select_produk();
    });
}

function btn_select_produk() {
    $('.datatable-general').on('click', '.btn-select-produk', function () {

        var index = $('#base-value').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');
        var id_barang = $(this).parents('tr').data('id');
        var kode_barang = $(this).parents('tr').data('kode-barang');
        var nama_barang = $(this).parents('tr').data('nama-barang');

        $('.table-produk tbody tr:nth-child(' + index + ') td.produk-nama').html(kode_barang + ' ' + nama_barang);
        $('.table-produk tbody tr:nth-child(' + index + ') .id_barang').val(id_barang);
        $('.table-produk tbody tr:nth-child(' + index + ') .diskon_nominal').val(0);
        $('.table-produk tbody tr:nth-child(' + index + ') .diskon_persen').val(0);
        $('.table-produk tbody tr:nth-child(' + index + ') .qty').val(1);
        $('.table-produk tbody tr:nth-child(' + index + ') .m-select2').val("").trigger('change');
        loading_start();
        enabled_select_lokasi(index);
        proses_lokasi(index, id_barang);
        proses_harga(index, id_barang);

        proses_diskon_nominal(index);
        proses_harga_net(index, data);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();

        change_lokasi();
        change_qty();
        //change_no_seri_produk();
        change_diskon_persen();
        change_diskon_nominal();
        loading_finish();


        //self.parents('td').siblings('td.produk-nama').html(kode_produk+' '+nama_produk);
        //self.parents('td').siblings('td.data').children('[name="id_produk[]"]').val(id_produk);

        $('#modal-produk').modal('hide');

    });
}

function enabled_select_lokasi(index) {
    $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').removeAttr('disabled');
    $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').val("").trigger('change');
}

function change_lokasi() {
    $('.id_lokasi').change(function () {
        var index = $(this).parents('tr').data('index');
    });
}

function change_qty() {
    $('.qty').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');

        proses_harga(index);
        proses_diskon_nominal(index);
        proses_harga_net(index, data);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_diskon_persen() {
    $('.diskon_persen').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');

        proses_harga_net(index, data);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_diskon_nominal() {
    $('.diskon_nominal').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');

        proses_harga_net(index, data);
        proses_sub_total(index);
        proses_total();
        proses_grand_total();
    });
}

function change_ongkos_kirim() {
    $('[name="ongkos_kirim"]').on('change keyup', function () {
        proses_grand_total();
    });
}

function change_pajak() {
    $('[name="pajak"]').on('change keyup', function () {
        proses_grand_total();
    });
}

function change_diskon_persen_akhir() {
    $('[name="diskon_persen_akhir"]').on('change keyup', function () {
        proses_grand_total();
    });
}


function change_potongan_akhir() {
    $('[name="potongan_akhir"]').on('change keyup', function () {
        proses_grand_total();
    });
}

function proses_lokasi(index, id_barang) {
    var data_send = {
        id_barang: id_barang,
        _token: _token
    };
    $.ajax({

        url: routeSalesOrderLokasi,
        type: 'post',
        data: data_send,
        success: function (data) {
            $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi').html('');
            $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi')

                .append($("<option></option>")
                    .attr("value", "")
                    .text('Pilih Lokasi'));
            loading_start();
            $.each(data, function (key, value) {
                $('.table-produk tbody tr:nth-child(' + index + ') .id_lokasi')
                    .append($("<option></option>")
                        .attr("value", value.id_lokasi)
                        .attr('data-satuan', value.satuan)
                        .attr('data-id', value.id)
                        .attr('data-stok', value.jml_barang)
                        .attr('data-id-satuan', value.id_satuan)
                        .text(value.lokasi.lokasi));
            });
            loading_finish();

        }
    });
}

$(".table-produk").on("change", ".id_lokasi", function () {
    var index = $('#base-value').data('index');
    var id_stok = $(this).find(':selected').data('id');
    var stok = $(this).find(':selected').data('stok');
    var satuan = $(this).find(':selected').data('satuan');
    var id_satuan = $(this).find(':selected').data('id-satuan');

    $('.table-produk tbody tr:nth-child(' + index + ') .td-satuan').html(satuan);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-stok').html(stok);
    $('.table-produk tbody tr:nth-child(' + index + ') .id_stok').val(id_stok);
    $('.table-produk tbody tr:nth-child(' + index + ') .satuan').val(satuan);
    $('.table-produk tbody tr:nth-child(' + index + ') .id_satuan').val(id_satuan);
});

function proses_lokasi_lama() {

    $(".table-produk").on("change", ".id_lokasi_lama", function () {
        var index = $(this).parents('tr').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');
        var id_stok = $(this).find(':selected').data('id');
        var stok = $(this).find(':selected').data('stok');
        var satuan = $(this).find(':selected').data('satuan');
        var id_satuan = $(this).find(':selected').data('id-satuan');


        $('.table-produk tbody tr:nth-child(' + index + ') .td-satuan').html(satuan);
        $('.table-produk tbody tr:nth-child(' + index + ') .td-stok').html(stok);
        $('[name="satuan[' + data + ']"]').val(satuan);
        $('[name="id_satuan[' + data + ']"]').val(id_satuan);
        $('[name="id_stok[' + data + ']"]').val(id_stok);
    });
}

function proses_harga(index, id_barang) {
    var data = $(this).parents('tr').data('id-penjualan-produk');
    id_barang = $('.table-produk tbody tr:nth-child(' + index + ') .id_barang').val();
    console.log('id_barang'+id_barang)
    var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty').val();
    var data_send = {
        _token: _token,
        id_barang: id_barang,
        qty: qty
    };
    $.ajax({
        beforeSend: loading_start(),
        url: routeSalesOrderHarga,
        type: "post",
        data: data_send,
        success: function (data) {
            console.log(data)
            // $('[name="harga[' + data + ']"]').val(data.number_raw);
            $('.table-produk tbody tr:nth-child(' + index + ') .harga').val(data.number_raw);
            $('.table-produk tbody tr:nth-child(' + index + ') .td-harga').html(data.number_format);

            proses_diskon_nominal(index);
            proses_harga_net(index);
            proses_sub_total(index);
            proses_total();
            proses_grand_total();
            loading_finish();
        }
    });
}


function proses_diskon_nominal(index) {
    $('.table-produk tbody tr:nth-child(' + index + ') .diskon_nominal').val();
    //$('.table-produk tbody tr:nth-child(' + index + ') .td-diskon_nominal').html(diskon_nominal_format);
}

function proses_harga_net(index, data) {
    var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').val();
    console.log('harga 1 : '+harga)

    if(harga == 'undefined' || harga == null){
        harga = '';
    }
    console.log('harga 2 : '+harga)
    harga = strtonumber(harga);
    var diskon_nominal = strtonumber($('.table-produk tbody tr:nth-child(' + index + ') .diskon_nominal').val());
    var diskon_persen = strtonumber($('.table-produk tbody tr:nth-child(' + index + ') .diskon_persen').val());

    var diskon = parseFloat(harga) * parseFloat(diskon_persen) / 100;
    var harga_net = parseFloat(harga) - parseFloat(diskon_nominal) - parseFloat(diskon);
    // var data = $(this).parents('tr').data('id-penjualan-produk');


    var harga_net_format = format_number(harga_net);
    $('.table-produk tbody tr:nth-child(' + index + ') .harga_net').val(harga_net);
    // $('[name="harga_net[]"]').val(harga_net);
    $('[name="harga_net[' + data + ']"]').val(harga_net);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-harga-net').html(harga_net_format);

}

function proses_sub_total(index) {
    var data = $(this).parents('tr').data('id-penjualan-produk');
    var harga = $('.table-produk tbody tr:nth-child(' + index + ') .harga').val();
    if(harga == 'undefined' || harga == null){
        harga = '';
    }
    console.log('harga : '+harga)
    harga = strtonumber(harga);
    var diskon_nominal = strtonumber($('.table-produk tbody tr:nth-child(' + index + ') .diskon_nominal').val());
    var diskon_persen = strtonumber($('.table-produk tbody tr:nth-child(' + index + ') .diskon_persen').val());
    var diskon = parseFloat(harga) * parseFloat(diskon_persen) / 100;
    var harga_net = parseFloat(harga) - parseFloat(diskon_nominal) - parseFloat(diskon);

    var qty = strtonumber($('.table-produk tbody tr:nth-child(' + index + ') .qty').val());
    var sub_total = parseFloat(harga_net) * parseFloat(qty);
    var sub_total_format = format_number(sub_total);

    $('.table-produk tbody tr:nth-child(' + index + ') .sub_total').val(sub_total);
    $('.table-produk tbody tr:nth-child(' + index + ') .td-sub-total').html(sub_total_format);
    $('[name="sub_total[' + data + ']"]').val(sub_total);
}

function proses_total() {
    var total = 0;
    $('.table-produk .sub_total').each(function () {
        var sub_total = $(this).val();
        total = parseFloat(total) + parseFloat(sub_total);
    });

    $('[name="total"]').val(total);
    $('.total').html(format_number(total));
}

function proses_grand_total() {
    var total = strtonumber($('[name="total"]').val());
    var diskon_persen = strtonumber($('[name="diskon_persen_akhir"]').val());
    var ongkos_kirim = strtonumber($('[name="ongkos_kirim"]').val());
    var pajak = strtonumber($('[name="pajak"]').val());
    var potongan_akhir = strtonumber($('[name="potongan_akhir"]').val());

    var diskon = parseFloat(total) * parseFloat(diskon_persen) / 100;

    var total_setelah_diskon = parseFloat(total) - parseFloat(diskon) - parseFloat(potongan_akhir);

    var pajak_setelah_diskon = parseFloat(total_setelah_diskon) * parseFloat(pajak) / 100;

    var hasil = parseFloat(total_setelah_diskon) + parseFloat(pajak_setelah_diskon) + parseFloat(ongkos_kirim);

    console.log(total)

    console.log(hasil)
    // var grand_total = parseFloat(total) + parseFloat(biaya_tambahan) + parseFloat(pajak_persen) - parseFloat(diskon) - parseFloat(potongan_akhir);

    //console.log(total+' '+biaya_tambahan+' '+potongan_akhir+' '+grand_total);

    if (!isNaN(hasil)) {
        $('[name="grand_total"]').val(hasil);
        $('.grand-total').html(format_number(hasil));
    }
}
