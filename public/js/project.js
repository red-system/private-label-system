$(document).ready(function () {

    add_row_tasks();
    btn_simpan();
    delete_row_completed_tasks();
    delete_row_next_tasks();
    add_row_file();
    delete_row_file_ada();
    btn_simpan_file();
});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');


function add_row_tasks() {
    $('#modal-tasks').on('click', '.btn-add-row-tasks', function () {
        // $('.btn-add-row-tasks').on('click', function () {
        var row_tasks = $('.row-tasks tbody').html();
        $('.table-tasks tbody').append(row_tasks);
        var index = $('.table-tasks tbody tr').length;
        $('.table-tasks tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-tasks tbody tr:nth-child(' + index + ') .select2-tasks').select2();
        $('.table-tasks tbody tr:nth-child(' + index + ') .datepicker').datepicker({
            rtl: mUtil.isRTL(),
            orientation: "auto",
            format: 'dd-mm-yyyy',
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true
        });
        delete_row_tasks();
    });
}

function btn_simpan() {
    // $("#tasks").on('submit', function(){
    $('#modal-tasks').on('click', '.btn-success', function () {
        $('#modal-tasks .btn-success').attr("disabled", true);
        $('#modal-tasks .btn-success').text('Saving...');
        $('#tasks').submit();

        //     btn.disabled = true;
    //     btn.innerText = 'Saving...'
    });
}

function delete_row_tasks() {
    $('.btn-delete-row-tasks').click(function () {
        $(this).parents('tr').remove();
    });
}

function delete_row_next_tasks() {
    $('#modal-tasks').on('click', '.btn-delete-row-next-tasks', function () {
        console.log('1')
        $(this).parents('tr').remove();
    });
}

function delete_row_completed_tasks() {
    $('#modal-tasks').on('click', '.btn-delete-row-completed-tasks', function () {
        $(this).parents('tr').remove();
    });
}

function add_row_file() {
    $('#modal-file').on('click', '.btn-add-row-file', function () {
        // $('.btn-add-row-file').on('click', function () {
        var row_file = $('.row-file tbody').html();
        $('.table-file tbody').append(row_file);
        var index = $('.table-file tbody tr').length;
        $('.table-file tbody tr:nth-child(' + index + ')').data('index', index);

        delete_row_file();
    });
}

function delete_row_file() {
    $('.btn-delete-row-file').click(function () {
        $(this).parents('tr').remove();
    });
}

function delete_row_file_ada() {
    $('#modal-file').on('click', '.btn-delete-row-file-ada', function () {
        $(this).parents('tr').remove();
    });
}

function btn_simpan_file() {
    $('#modal-file').on('click', '.btn-success', function () {
        $('#modal-file .btn-success').attr("disabled", true);
        $('#modal-file .btn-success').text('Saving...');
        $('#file').submit();
    });
}