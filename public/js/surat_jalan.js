$(document).ready(function () {
    btn_tambah();
    btn_reset();
    timbang_reset();
    timbang_mentah();
    modal_timbang_lama();

});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');

function modal_timbang_lama() {
    $('.btn-timbang-lama').on('click', function () {
        $('.btn-simpan').hide();
        $('.btn-kembali').hide();
        var data = $(this).data('no');
        var qty = $('[name="qty[' + data + ']"]').val();
        var count = $('#kali-' + data).html();
        var kirim = $('#kirim-' + data).html();
        $('#modal-timbang .qty').html(qty);
        $('#modal-timbang .hitung-qty').html(kirim);
        $('#modal-timbang .kali-qty').html(count);
        $('#modal-timbang .timbang-selesai').attr('data-no', data);


        btn_selesai();
        $('#modal-timbang').modal('show');
        $('#modal-timbang [name="timbang"]').focus();

    })
}

function timbang_mentah() {
    $('#modal-timbang [name="timbang"]').on('change', function () {
        var berat = $('[name="timbang"]').val();
        var panjang = berat.toString().length;
        var res = 0;
        if (panjang <= 3) {
            res = berat;
        } else {
            res = berat.substring(7, 12);
        }
        $('input[name="timbang"]').val(res);
        $('#modal-timbang .btn-timbang-tambah').focus();


    });
}

function btn_tambah() {
    var count = 0;
    var qty = 0;

    $('.btn-timbang-tambah').on('click', function () {
        var pesan = $('#modal-timbang .qty').html();
        var berat = $('[name="timbang"]').val();

        if (berat === "") {
            berat = 0;
        }
        qty = $('.hitung-qty').html();
        count = $('.kali-qty').html();
        qty = (parseFloat(qty) + parseFloat(berat));
        count = parseFloat(count) + 1;
        if (qty > pesan) {
            swal({
                title: "Perhatian ...",
                text: "Berat Kirim Melebihi Permintaan, Tolong Timbang Ulang Dengan Berat Sesuai",
                type: "warning"
            });
            $('input[name="timbang"]').val("");
            $('[name="timbang"]').focus();
        } else {
            $('#modal-timbang .hitung-qty').html(qty);
            $('#modal-timbang .kali-qty').html(count);
            $('input[name="timbang"]').val("");
            $('[name="timbang"]').focus();
        }
    })

}

function btn_reset() {
    $('.btn-timbang-reset').on('click', function () {
        var reset = 0;
        $('input[name="timbang"]').val("");
        $('[name="timbang"]').focus();
    })
}

function btn_selesai() {
    $('.btn-timbang-selesai').on('click', function () {
        var data = $(this).attr('data-no');
        var pesan = $('#modal-timbang .qty').html();
        var qty = $('#modal-timbang .hitung-qty').html();
        var count = $('#modal-timbang .kali-qty').html();

        if (qty < pesan) {
            swal({
                title: "Perhatian ...",
                text: "Berat Kirim Kurang Dari Permintaan, Tolong Timbang Ulang Dengan Berat Sesuai",
                type: "warning"
            });
            $('input[name="timbang"]').val("");
            $('[name="timbang"]').focus();
        } else {
            $('#kali-' + data).html(count);
            $('#kirim-' + data).html(qty);
            $('[name="qty_kirim[' + data + ']"]').val(qty);
            $('[name="qty_kali[' + data + ']"]').val(count);

            $('#modal-timbang').modal('hide');
            $('.btn-simpan').show();
            $('.btn-kembali').show();
        }
    })
}

function timbang_reset() {
    $('.btn-timbang-reset-all').on('click', function () {
        var index = $(this).data('no');
        $('#kali-' + index).html(0);
        $('#kirim-' + index).html(0);
        $('[name="qty_kirim[' + index + ']"]').val(0);
        $('[name="qty_kali[' + index + ']"]').val(0);
    })
}