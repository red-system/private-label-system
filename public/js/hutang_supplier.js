$(document).ready(function () {

    add_row_pembayaran();
    total();
    btn_simpan();

});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');

function btn_simpan() {
    $('#modal-bayar').on('click', '.btn-simpan', function () {
        $('input[type="submit"]').attr('disabled', 'disabled');
    });
}

function add_row_pembayaran() {
    $('#modal-bayar').on('click', '.btn-add-row-pembayaran', function () {
        // $('.btn-add-row-pembayaran').on('click', function () {
        var row_pembayaran = $('.row-pembayaran tbody').html();
        $('.table-pembayaran tbody').append(row_pembayaran);
        var index = $('.table-pembayaran tbody tr').length;
        console.log(index)
        $('.table-pembayaran tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .touchspin-pembayaran').TouchSpin(touchspin_number);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .metode-pembayaran').select2();

        metode_bayar();
        change_jumlah();
        input_numeral();
        delete_row_pembayaran();
    });
}

function delete_row_pembayaran() {
    $('.btn-delete-row-pembayaran').click(function () {
        $(this).parents('tr').remove();

        proses_terbayar();
        proses_sisa_pembayaran();
    });
}

function metode_bayar() {
    $("#modal-bayar").on("change", ".table-pembayaran .metode-pembayaran", function () {
        var index = $(this).parents('tr').data('index');
        var tanggal = $('[name="hari_ini[]"]').val();

        console.log(index)
        var metode = $(this).find(':selected').val();

        if (metode == "Bank") {
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .no-bg').val("").attr('readonly', true);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .nama-bank').val("").attr('readonly', false);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker('destroy').val("")
                .attr('class', 'form-control m-input tanggal-pembayaran');
        } else if (metode == "Hutang Giro") {
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .no-bg').val("").attr('readonly', false);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .nama-bank').val("").attr('readonly', false);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .tanggal-pembayaran').val(tanggal).attr('class', 'form-control m-input datepicker-pembayaran');
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker({
                rtl: mUtil.isRTL(),
                todayHighlight: !0,
                orientation: "bottom left",
                format: 'dd-mm-yyyy',
                disable: false,
            });
        } else if (metode == "Tunai") {
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .no-bg').val("").attr('readonly', true);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .nama-bank').val("").attr('readonly', true);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker('destroy').val("")
                .attr('class', 'form-control m-input tanggal-pembayaran');
        } else {
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .no-bg').val("").attr('readonly', true);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .nama-bank').val("").attr('readonly', true);
            $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker('destroy').val("")
                .attr('class', 'form-control m-input tanggal-pembayaran');
        }

    });
}

function total() {
    var total = $('#modal-bayar [name="total_hutang_supplier"]').val();
    console.log(total)
    total = parseFloat(total);

    $('#modal-bayar .total-hutang').html(format_number(total));
}

function change_jumlah() {
    $('[name="jumlah[]"]').on('change keyup', function () {
        proses_jumlah($(this));
        proses_sisa_pembayaran();
    });
}

function proses_jumlah(self) {
    var total = $('[name="total"]').val();
    var jumlah_all = 0;
    total = total;
    var jumlah = total;
    $('[name="jumlah[]"]').each(function () {
        jumlah_all = parseInt(jumlah_all) + parseInt($(this).val());
    });

    //console.log(jumlah_all+' '+grand_total);

    if (jumlah_all <= total) {
        jumlah = total - jumlah_all;
    }

    self.parents('td').siblings('td.td-jumlah').children('div').children('[name="jumlah[]"]').val(jumlah);

    proses_terbayar();
}

function proses_terbayar() {
    var terbayar = 0;
    $('[name="jumlah[]"]').each(function () {
        var jumlah = $(this).val();
        jumlah = strtonumber(jumlah);
        terbayar = parseInt(terbayar) + parseInt(jumlah);
    });

    $('.terbayar').html(format_number(terbayar));
    $('[name="terbayar"]').val(terbayar);

    proses_sisa_pembayaran();
}

function proses_sisa_pembayaran() {
    var total = $('[name="total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    total = parseInt(total);
    terbayar = parseInt(terbayar);

    var sisa = total - terbayar;

    if (terbayar > total) {
        sisa = 0;
    }


    $('.sisa-pembayaran').html(format_number(sisa));
    $('[name="sisa_hutang_supplier"]').val(sisa);
}