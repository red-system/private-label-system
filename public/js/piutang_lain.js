$(document).ready(function () {

    modal_create();
    modal_edit();
    change_table_name();
    change_nominal_piutang_lain_lain();

    add_row_kredit();

    modal_pembayaran();
    add_row_pembayaran();

});

var routePiutangLainEditKredit = $('#base-value').data('route-piutang-lain-edit-kredit');
var _token = $('#base-value').data('csrf-token');

function modal_create() {
    $('#modal-create').on('shown.bs.modal', function () {
        var table_name = $('#modal-create [name="table_name"]:checked').val();
        proses_table_id(table_name);
    });
}

function modal_edit() {
    $('#modal-edit').on('shown.bs.modal', function () {
        var table_name = $('#modal-edit [name="table_name"]:checked').val();
        proses_table_id(table_name);
        proses_edit_kredit();
    });
}

function change_table_name() {
    $('[name="table_name"]').change(function () {
        var table_name = $(this).val();
        proses_table_id(table_name);
    });
}

function proses_table_id(table_name) {
    if (table_name == 'tb_distributor') {
        $('.table-id-karyawan').hide();
        $('.table-id-distributor').show();
    } else {
        $('.table-id-distributor').hide();
        $('.table-id-karyawan').show();
    }
}

function proses_edit_kredit() {
    var id_piutang_lain = $('#modal-edit [name="id"]').val();

    $.ajax({
        beforeSend: function () {
            loading_start();
        },
        url: routePiutangLainEditKredit,
        type: "get",
        data: {
            id_piutang_lain: id_piutang_lain,
        },
        success: function (trBody) {
            $('#modal-edit .table-kredit tbody').html(trBody);
            $('#modal-edit [name="master_id_kredit[]"]').select2();
            $('#modal-edit .touchspin-kredit').TouchSpin(touchspin_number_decimal);

            delete_row_kredit();
            change_kredit();
            loading_finish();
        }
    });
}


//////////////////////////// KREDIT //////////////////////////

function change_nominal_piutang_lain_lain() {
    $('[name="pl_amount"]').on('keyup change', function () {
        proses_status_balance();
    });
}


function add_row_kredit() {
    $('.btn-add-row-kredit').click(function () {
        var row_kredit = $('.row-kredit tbody').html();
        $('.modal.show .table-kredit tbody').append(row_kredit);

        var index = $('.modal.show .table-kredit tbody tr').length;

        console.log(index);

        $('.table-kredit tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-kredit tbody tr:nth-child(' + index + ') .touchspin-kredit').TouchSpin(touchspin_number_decimal);
        $('.table-kredit tbody tr:nth-child(' + index + ') .select2-kredit').select2();

        delete_row_kredit();
        change_kredit();
    });
}

function delete_row_kredit() {
    $('.btn-delete-row-kredit').click(function () {
        $(this).parents('tr').remove();
    });
}

function change_kredit() {

    $('.touchspin-kredit').on('keyup change', function () {
        proses_total_kredit();
        proses_status_balance();

    });
}

function proses_total_kredit() {
    var total_kredit = parseFloat(0);
    $('.touchspin-kredit').each(function () {
        var value = $(this).val();
        value = parseFloat(value);
        total_kredit += value;
    });

    $('.modal.show th.kredit-total').html(format_number(total_kredit));
}

function proses_status_balance() {

    var total_kredit = parseFloat(0);
    var total_debet = $('.modal.show [name="pl_amount"]').val();
    total_debet = parseFloat(total_debet);
    var status_balance = '<span class="m-badge m-badge--success m-badge--wide">Balance</span>';
    var status_not_balance = '<span class="m-badge m-badge--danger m-badge--wide">Not Balance</span>';

    $('.modal.show .touchspin-kredit').each(function () {
        var value = $(this).val();
        value = parseFloat(value);
        total_kredit += value;
    });

    console.log(total_kredit, total_debet);


    if (total_debet == total_kredit) {
        $('.modal.show .status-balance').html(status_balance);
    } else {
        $('.modal.show .status-balance').html(status_not_balance);
    }
}

/////////////////////////// PEMBAYARAN /////////////////////////


function modal_pembayaran() {
    $('.datatable-general').on('click', '.btn-pembayaran', function () {
        var route = $(this).data('route');
        var row = $(this).parents('div').siblings('textarea').val();
        var json = JSON.parse(row);

        $('#modal-pembayaran').modal('show');
        $('#modal-pembayaran form').attr('action', route);

        $.each(json, function (key, val) {
            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#modal-pembayaran .img-preview').attr('src', base_url + '/upload/' + val);
            } else if (type == 'radio') {
                $('#modal-pembayaran [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
            } else {
                $('#modal-pembayaran [name="' + key + '"]').val(val);
                $('#modal-pembayaran [name="' + key + '"]').trigger('change');
                $('#modal-pembayaran .' + key).html(val);
            }
        });
    });
}

function add_row_pembayaran() {
    $('.btn-add-row-pembayaran').click(function () {
        var row_pembayaran = $('.row-pembayaran tbody').html();
        $('.table-pembayaran tbody').append(row_pembayaran);

        var index = $('.table-pembayaran tbody tr').length;

        $('.table-pembayaran tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .touchspin-pembayaran').TouchSpin(touchspin_number);
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .datepicker-pembayaran').datepicker({
            rtl: mUtil.isRTL(),
            todayHighlight: !0,
            orientation: "bottom left",
            format: 'dd-mm-yyyy'
        });
        $('.table-pembayaran tbody tr:nth-child(' + index + ') .select2-pembayaran').select2();

        change_pembayaran();
        change_jumlah();
        delete_row_pembayaran();
    });
}

function delete_row_pembayaran() {
    $('.btn-delete-row-pembayaran').click(function () {
        $(this).parents('tr').remove();

        proses_terbayar();
        proses_sisa_pembayaran();
        proses_kembalian();
    });
}

function change_pembayaran() {
    $('[name="master_id[]"]').change(function () {
        proses_jumlah($(this));
        proses_terbayar();
        proses_sisa_pembayaran();
    });
}

function change_jumlah() {
    $('[name="jumlah[]"]').on('change keyup', function () {
        proses_jumlah($(this));
        proses_sisa_pembayaran();
    });
}

function proses_jumlah(self) {
    var total = $('[name="total"]').val();
    var jumlah_all = 0;
    var jumlah = total;
    $('[name="jumlah[]"]').each(function () {
        jumlah_all = parseInt(jumlah_all) + parseInt($(this).val());
    });

    //console.log(jumlah_all+' '+grand_total);

    if (jumlah_all <= total) {
        jumlah = total - jumlah_all;
    }

    self.parents('td').siblings('td.td-jumlah').children('div').children('[name="jumlah[]"]').val(jumlah);

    proses_terbayar();
}

function proses_terbayar() {
    var terbayar = 0;
    $('[name="jumlah[]"]').each(function () {
        var jumlah = $(this).val();
        terbayar = parseInt(terbayar) + parseInt(jumlah);
    });

    $('.terbayar').html(format_number(terbayar));
    $('[name="terbayar"]').val(terbayar);

    proses_sisa_pembayaran();
    proses_kembalian();
}

function proses_sisa_pembayaran() {
    var total = $('[name="total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    total = parseInt(total);
    terbayar = parseInt(terbayar);

    var sisa = total - terbayar;

    if (terbayar > total) {
        sisa = 0;
    }


    $('.sisa-pembayaran').html(format_number(sisa));
    $('[name="sisa_pemabayaran"]').val(sisa);
}

function proses_kembalian() {
    var total = $('[name="total"]').val();
    var terbayar = $('[name="terbayar"]').val();

    total = parseInt(total);
    terbayar = parseInt(terbayar);

    var kembalian = terbayar - total;

    if (terbayar <= total) {
        kembalian = 0;
    }


    $('.kembalian').html(format_number(kembalian));
    $('[name="kembalian"]').val(kembalian);
}