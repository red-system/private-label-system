WebFont.load({
    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
    active: function () {
        sessionStorage.fonts = true;
    }
});

var decimalStep = $('#base-value').data('decimal-step');

var piutangLain_number = {
    buttondown_class: "btn btn-success",
    buttonup_class: "btn btn-warning",
    verticalbuttons: !0,
    verticalupclass: "la la-plus",
    verticaldownclass: "la la-minus",
    min: 0,
    max: 10000000000000000000000,
};

var touchspin_number = {
    buttondown_class: "btn btn-success",
    buttonup_class: "btn btn-warning",
    verticalbuttons: !0,
    verticalupclass: "la la-plus",
    verticaldownclass: "la la-minus",
    min: 0,
    max: 10000000000000000000000,
};

var touchspin_number_decimal = {
    buttondown_class: "btn btn-success",
    buttonup_class: "btn btn-warning",
    verticalbuttons: !0,
    verticalupclass: "la la-plus",
    verticaldownclass: "la la-minus",
    step: decimalStep,
    decimals: 2,
    min: 0,
    max: 10000000000000000000000,
};

$(document).ready(function () {

    var csrf_token = $('#base-value').data('csrf-token');
    var base_url = $('#base-value').data('base-url');
    var list_url = $('#list_url').data('list-url');
    // console.log('app.js : window '+window.location)

    user_role();
    user_role_menu_action();
    input_numeral();
    input_decimal();
    roles_login();



    /**
     *  Begin General Operations
     *
     *  data attribute :
     *  - confirm (true|false)
     *  - action (url controller)
     - method (post|put|dll)
     - redirect (url halaman redirect)
     - alert-show (setelah action, apakah muncul sweet alert)
     - alert-field-message (setelah action, apakah muncul sweet alert yang di texdt nya ada, field error)
     */
    $('.form-send').submit(function (e) {
        e.preventDefault();

        var self = $(this);
        var confirm = $(this).data('confirm');
        if (confirm) {
            swal({
                title: "Perhatian ...",
                text: "Yakin Simpan data ini ?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    loading_start();
                    form_send(self);
                }
            });
        } else {
            form_send(self);
        }

        return false;
    });
    var actionButton = {
        "edit": {"class": "btn-edit akses-edit", "icon": "fa fa-pencil-alt", "label": "Edit"},
        "edit_view": {"class": "btn-edit-view akses-edit", "icon": "fa fa-pencil-alt", "label": "Edit"},
        "edit_direct": {"class": "btn-edit-direct akses-edit", "icon": "fa fa-pencil-alt", "label": "Edit"},
        "delete": {"class": "btn-hapus akses-delete", "icon": "la la-remove", "label": "Delete"},
        "detail": {"class": "btn-detail akses-detail", "icon": "fa fa-eye", "label": "Detail"},
        "detail_view": {"class": "btn-detail-view akses-detail", "icon": "fa fa-eye", "label": "Detail"},
        "penjualan": {"class": "btn-penjualan akses-penjualan", "icon": "fa fa-shopping-cart", "label": "Penjualan"},
        "retur_penjualan": {"class": "btn-penjualan akses-penjualan", "icon": "fa fa-undo", "label": "Retur Penjualan"},
        "surat_jalan": {"class": "btn-surat-jalan akses-surat-jalan", "icon": "fa fa-envelope", "label": "Surat Jalan"},
        "download": {"class": "btn-download akses-download", "icon": "fa fa-download", "label": "Download"},
        "additional": {"class": "btn-additional akses-additional", "icon": "fa fa-plus", "label": "Additional"},
        "serviceDetailIndex": {"class": "btn-direct ", "icon": "fa fa-eye", "label": "Detail"},
        "accept": {"class": "btn-accept akses-acccept", "icon": "fa fa-check", "label": "Accept"},
        "barcode": {"class": "btn-accept akses-barcode", "icon": "fa fa-print", "label": "Barcode"},
        "print": {"class": "btn-accept akses-akses-print", "icon": "fa fa-print", "label": "Print"},
        "pdf": {"class": "btn-accept akses-pdf", "icon": "fa fa-print", "label": "Pdf"},
        "excel": {"class": "btn-accept akses-pdf", "icon": "fa fa-print", "label": "Excel"},
        "tasks": {"class": "btn-tasks akses-tasks", "icon": "fa fa-tasks", "label": "Tasks"},
        "metric": {"class": "btn-metric akses-metric", "icon": "fa fa-chart-bar", "label": "Metric"},
        "file": {"class": "btn-project-file akses-file", "icon": "fa fa-file", "label": "File"},
        "view": {"class": "btn-project-view akses-view", "icon": "fa fa-eyes", "label": "View"},
        "commisison_cost": {
            "class": "btn-commision-cost akses-commision-cost",
            "icon": "fa fa-money-bill",
            "label": "Commision & Cost"
        },

    };
    var action = $("#action_list").length > 0 ? JSON.parse($("#action_list").html()) : [];
    var columns = $("#table_coloumn").length > 0 ? JSON.parse($("#table_coloumn").html()) : [];
    var defAction = {
        targets: -1,
        responsivePriority: 1,
        title: 'Actions',
        orderable: false,
        render: function (data, type, full, meta) {
            console.log(data)
            var template = '<textarea class="row-data hidden">' + JSON.stringify(data) + '</textarea>';
            // console.log(data)
            var actionList = '';
            var edit_button = '';
            var delete_button = '';
            $.each(action, function (index, value) {
                // console.log('Value = ', value)
                // console.log('Value = ', actionButton[value])

                // if(actionButton[value] == undefined)
                if (actionButton[value] != undefined && value != 'edit' && value != 'edit_view' && value != 'delete') {
                    var label = ((data.label != undefined && data.label[value] != undefined) ? data.label[value] : actionButton[value]["label"]);
                    var icon = ((data.icon != undefined && data.icon[value] != undefined) ? data.icon[value] : actionButton[value]["icon"]);
                    var class_tag = ((data.class_tag != undefined && data.class_tag[value] != undefined) ? data.class_tag[value] : actionButton[value]["class"]);
                    var target_blank = ((data.target_blank != undefined && data.target_blank[value] != undefined) ? 'target="_blank"' : "");
                    var temp = '<a style="display:' + ((data.visibility != undefined && data.visibility[value] != undefined) ? data.visibility[value] : 'block') + '" ' +
                        'class="dropdown-item ' + class_tag + '" ' +
                        'href="' + (data.route[value] != undefined ? data.route[value] : '') + '" ' +
                        'data-route="' + (data.route[value] != undefined ? data.route[value] : '') + '" ' +
                        'data-direct="' + ((data.direct != undefined && data.direct[value] != undefined) ? data.direct[value] : '') + '" ' +
                        'data-ignore="' + ((data.ignore != undefined && data.ignore[value] != undefined) ? data.ignore[value] : false) + '" ' +
                        'data-check="' + ((data.check != undefined && data.check[value] != undefined) ? data.check[value] : '') + '"' +
                        'data-message="' + ((data.message != undefined && data.message[value] != undefined) ? data.message[value] : '') + '"' +
                        '' + target_blank + '>' +
                        '<i class="' + icon + '"></i> ' + label +
                        '</a>';
                    temp += (action.length - 1 > index) ? '<div class="dropdown-divider"></div>' : '';
                    actionList += temp;
                } else if (actionButton[value] != undefined && value == 'edit' || value == 'edit_view') {
                    var label = ((data.label != undefined && data.label[value] != undefined) ? data.label[value] : actionButton[value]["label"]);
                    var icon = ((data.icon != undefined && data.icon[value] != undefined) ? data.icon[value] : actionButton[value]["icon"]);
                    var class_tag = ((data.class_tag != undefined && data.class_tag[value] != undefined) ? data.class_tag[value] : actionButton[value]["class"]);
                    var target_blank = ((data.target_blank != undefined && data.target_blank[value] != undefined) ? 'target="_blank"' : "");

                    edit_button =
                        '<a href="' + (data.route[value] != undefined ? data.route[value] : '') + '"'
                        + 'class="' + class_tag + ' btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"'
                        + 'data-route="' + (data.route[value] != undefined ? data.route[value] : '') + '" ' +
                        'data-direct="' + ((data.direct != undefined && data.direct[value] != undefined) ? data.direct[value] : '') + '" ' +
                        'data-ignore="' + ((data.ignore != undefined && data.ignore[value] != undefined) ? data.ignore[value] : false) + '" ' +
                        'data-check="' + ((data.check != undefined && data.check[value] != undefined) ? data.check[value] : '') + '"' +
                        'data-message="' + ((data.message != undefined && data.message[value] != undefined) ? data.message[value] : '') + '"' +
                        target_blank + '>'
                        + '<span class="svg-icon svg-icon-md">'
                        + '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'
                        + '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'
                        + '<rect x="0" y="0" width="24" height="24"/>'
                        + '<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>'
                        + '<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>'
                        + '</g>'
                        + '</svg>'
                        + '</span>'
                        + '</a>';
                } else if (actionButton[value] != undefined && value == 'delete') {
                    var label = ((data.label != undefined && data.label[value] != undefined) ? data.label[value] : actionButton[value]["label"]);
                    var icon = ((data.icon != undefined && data.icon[value] != undefined) ? data.icon[value] : actionButton[value]["icon"]);
                    var class_tag = ((data.class_tag != undefined && data.class_tag[value] != undefined) ? data.class_tag[value] : actionButton[value]["class"]);
                    var target_blank = ((data.target_blank != undefined && data.target_blank[value] != undefined) ? 'target="_blank"' : "");

                    delete_button =
                        '<a href="' + (data.route[value] != undefined ? data.route[value] : '') + '"'
                        + 'class="' + class_tag + ' btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"' +
                        'data-route="' + (data.route[value] != undefined ? data.route[value] : '') + '" ' +
                        'data-direct="' + ((data.direct != undefined && data.direct[value] != undefined) ? data.direct[value] : '') + '" ' +
                        'data-ignore="' + ((data.ignore != undefined && data.ignore[value] != undefined) ? data.ignore[value] : false) + '" ' +
                        'data-check="' + ((data.check != undefined && data.check[value] != undefined) ? data.check[value] : '') + '"' +
                        'data-message="' + ((data.message != undefined && data.message[value] != undefined) ? data.message[value] : '') + '"' +
                        target_blank + '>'
                        + '<span class="svg-icon svg-icon-md">'
                        + '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'
                        + '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'
                        + '<rect x="0" y="0" width="24" height="24"/>'
                        + '<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'
                        + '<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'
                        + '</g>'
                        + '</svg>'
                        + '</span>'
                        + '</a>';
                }
            });
            var dropdown = '<div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">' +
                '<a href="#" class="btn btn-success btn-sm font-weight-bolder font-size-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                // 'type="button" id="dropdownMenuButton" data-toggle="dropdown"' +
                // ' ria-haspopup="true" aria-expanded="false"> ' +
                '<i class="fa fa-cog"></i> MENU </a>' +
                '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" ' +
                'x-placement="bottom-start" style="position: absolute; ' +
                'will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);">' +
                actionList +
                '</div>' +
                '</div>';
            var new_drop = '';
            if (actionList != '') {
                new_drop = '<div class="dropdown dropdown-inline">' +
                    '<a href="javascript:;" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" data-toggle="dropdown">' +
                    '<span class="svg-icon svg-icon-md">' +
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="svg-icon">' +
                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">' +
                    '<rect x="0" y="0" width="24" height="24"/>' +
                    '<path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"/>' +
                    '<path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"/>' +
                    '</g>' +
                    '</svg>' +
                    '</span>' +
                    '</a>' +
                    '<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">' +
                    actionList +
                    '</div>' +
                    '</div>';
            }
            return template + '<div> <span style="overflow: visible; position: relative; width: 130px;">' + new_drop + edit_button + delete_button + '</div> </span>';
            // return template + dropdown;


            // <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
            //         <a href="#" class="btn btn-warning btn-sm font-weight-bolder font-size-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Create</a>
            //         <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
            //         <!--begin::Navigation-->
            //
            //         <!--end::Navigation-->
            //         </div>
            //         </div>
            //         </div>

        },

    };
    var coloumnDef = null;
    if (action.length > 0) {
        columns.push({data: null, mData: null});
        coloumnDef = [defAction]
    }

    if ($('.datatable').hasClass("over")) {
        var table = $('.datatable').length < 1 ? '' : $('.datatable').DataTable({
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            // dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            ajax: list_url,
            columns: columns,
            columnDefs: coloumnDef,
        });
    } else {
        var table = $('.datatable').length < 1 ? '' : $('.datatable').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ordering: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 10,
            // dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            ajax: list_url,
            columns: columns,
            columnDefs: coloumnDef,
        });
    }


    $('.datatable-general, .datatable-no-pagination, .datatable-jurnal-umum').on('click', '.btn-hapus', function (e) {
        e.preventDefault();
        var self = $(this);
        var remove_by_index = $(this).data('remove-by-index');
        var remove_index = $(this).data('remove-index');
        var reload = $(this).data('reload');
        // console.log('reload ' + reload)
        var message = $(this).data('message');
        console.log(message)
        if (typeof message === "undefined") {
            message = "Yakin hapus data ini ?";
        }
        swal({
            title: "Are you sure you delete this data ?",
            html: message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, I'm Sure",
            cancelButtonText: "No",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: self.data('route'),
                    type: "delete",
                    data: {
                        _token: csrf_token
                    },
                    success: function () {
                        if (table === "") {
                            location.reload();
                        } else {
                            // console.log('2')
                            table.ajax.reload();
                        }
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'There is something wrong!';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
        return false;
    });

    $('.datatable-general, .datatable-no-pagination').on('click', '.btn-edit', function (e) {
        e.preventDefault();
        var self = $(this);
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        // console.log(row)

        if (typeof route !== 'undefined') {
            $('#modal-edit').parents('form').attr('action', route);
        }

        if (typeof redirect !== 'undefined') {
            $('#modal-edit').parents('form').data('redirect', redirect);
        }
        if (direct !== '') {
            window.location.href = direct;
        }
        $.each(json, function (key, val) {
            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#modal-edit .img-preview').attr('src', base_url + '/upload/' + val);
            } else if (type == 'radio') {
                $('#modal-edit [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
            } else if (type == 'checkbox') {
                $('#modal-edit [name="' + key + '"]').prop('checked', (val == 1 ? true : false)).val('true');
            } else {
                // if (val != null) {
                //     if ($('#modal-edit .input-numeral')) {
                //         val = String(val);
                //         if (val.includes(',00')) {
                //             val = val.replace(',00', '')
                //         }
                //         if (val.includes(',')) {
                //             val = val.replace(',', 'kkkkkk')
                //         }
                //         if (val.includes('Rp. ')) {
                //             val = val.replace('Rp. ', '');
                //             val = val.replace(',', 'zzzzzzz');
                //             val = val.replace('.', ',');
                //             val = val.replace('zzzzzzz', '.');
                //         }
                //         val = val.replace('kkkkkk', ',')
                //     }
                // }
                $('#modal-edit [name="' + key + '"]').val(val);
                $('#modal-edit [name="' + key + '"]').trigger('change');

                $('#modal-edit .' + key).html(val);
            }
        });

        $('#modal-edit').modal('show');

        return false;
    });

    $('.datatable-general, .datatable-no-pagination').on('click', '.btn-commision-cost', function (e) {
        e.preventDefault();
        var self = $(this);
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);

        if (typeof route !== 'undefined') {
            $('#modal-commision-cost').parents('form').attr('action', route);
        }

        if (typeof redirect !== 'undefined') {
            $('#modal-commision-cost').parents('form').data('redirect', redirect);
        }
        if (direct !== '') {
            window.location.href = direct;
        }
        $.each(json, function (key, val) {

            $('#modal-commision-cost [name="' + key + '"]').val(val);
            $('#modal-commision-cost [name="' + key + '"]').trigger('change');

            $('#modal-commision-cost .' + key).html(val);
        });

        $('#modal-commision-cost').modal('show');

        return false;
    });

    $('.datatable-general, .datatable-no-pagination').on('click', '.btn-view-edit', function (e) {
        e.preventDefault();
        var self = $(this);
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        console.log(row)
        console.log(json)


        if (typeof route !== 'undefined') {
            $('#modal-view-edit').parents('form').attr('action', route);
        }

        if (typeof redirect !== 'undefined') {
            $('#modal-view-edit').parents('form').data('redirect', redirect);
        }
        if (direct !== '') {
            window.location.href = direct;
        }
        $.each(json, function (key, val) {
            if(jQuery.isArray(val)){
                $.each(val, function (kunci, data_baru) {
                    var tipe = $('[name="' + key + '['+kunci+']"]').attr('type');
                    if (tipe == 'file') {
                        $('#modal-view-edit .img-preview').attr('src', base_url + '/upload/' + data_baru);
                    } else if (tipe == 'radio') {
                        $('#modal-view-edit [name="' + key + '['+kunci+']"][value=' + data_baru + ']').attr('checked', 'checked');
                    } else if (tipe == 'checkbox') {
                        $('#modal-view-edit [name="' + key + '['+kunci+']"]').prop('checked', (data_baru == 1 ? true : false));
                    } else {
                        $('#modal-view-edit [name="' + key + '['+kunci+']"]').val(data_baru);
                        $('#modal-view-edit [name="' + key + '['+kunci+']"]').trigger('change');

                    }
                });
            }else {
                var type = $('[name="' + key + '"]').attr('type');
                if (type == 'file') {
                    $('#modal-view-edit .img-preview').attr('src', base_url + '/upload/' + val);
                } else if (type == 'radio') {
                    $('#modal-view-edit [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
                } else if (type == 'checkbox') {
                    $('#modal-view-edit [name="' + key + '"]').prop('checked', (val == 1 ? true : false));
                } else {
                        $('#modal-view-edit [name="' + key + '"]').val(val);
                        $('#modal-view-edit [name="' + key + '"]').trigger('change');



                    $('#modal-view-edit .' + key).html(val);
                }
            }
        });

        $('#modal-view-edit').modal('show');

        return false;
    });

    $('.datatable-general, .datatable-no-pagination').on('click', '.btn-metric', function (e) {
        e.preventDefault();
        var self = $(this);
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);

        if (typeof route !== 'undefined') {
            $('#modal-metric').parents('form').attr('action', route);
        }

        if (typeof redirect !== 'undefined') {
            $('#modal-metric').parents('form').data('redirect', redirect);
        }
        if (direct !== '') {
            window.location.href = direct;
        }

        $('#modal-metric').modal('show');

        return false;
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-detail', function (e) {
        e.preventDefault();
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore")
        if (ignore) {
            if (direct != "") {
                window.location.href = direct;
            }
        }


        $.each(json, function (key, val) {

            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#modal-detail .img-preview').attr('src', base_url + '/upload/' + val);
            } else if (type == 'radio') {
                $('#modal-detail [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
            } else {
                $('#modal-detail [name="' + key + '"]').val(val);
                $('#modal-detail [name="' + key + '"]').html(val);
                $('#modal-detail [name="' + key + '"]').trigger('change');
                $('#modal-detail .' + key).html(val);
            }
        });

        $('#modal-detail').modal('show');
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-edit-akunting', function (e) {
        e.preventDefault();
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore");
        var href = $(this).attr('href');

        if (ignore) {
            if (direct != "") {
                window.location.href = direct;
            }
        }
        $.ajax({
            beforeSend: loading_start(),
            url: href,
            type: 'GET',
            success: function (response) {
                $('#modal-edit .modal-body').html(response);
                loading_finish();
                $('#modal-edit').modal('show');
                loading_finish();
            }
        });
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-detail-view', function (e) {
        e.preventDefault();
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore");
        var href = $(this).attr('href');
        if (ignore) {
            if (direct != "") {
                window.location.href = direct;
            }
        }
        $.ajax({
            beforeSend: loading_start(),
            url: href,
            type: 'GET',
            success: function (response) {
                $('#modal-detail-view .modal-body').html(response);
                loading_finish();
                $('#modal-detail-view').modal('show');
                loading_finish();
            }
        });
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-edit-view', function (e) {
        e.preventDefault();
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore");
        var href = $(this).attr('href');
        if (ignore) {
            if (direct != "") {
                window.location.href = direct;
            }
        }
        $.ajax({
            beforeSend: loading_start(),
            url: href,
            type: 'GET',
            success: function (response) {
                $('#modal-edit-view .modal-body').html(response);
                loading_finish();
                $('#modal-edit-view').modal('show');
                loading_finish();
            }
        });
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-tasks', function (e) {
        e.preventDefault();
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore");
        var href = $(this).attr('href');
        if (ignore) {
            if (direct != "") {
                window.location.href = direct;
            }
        }
        $.ajax({
            beforeSend: function () {
                loading_start();
            },
            url: href,
            type: 'GET',
            success: function (response) {
                $('#modal-tasks .modal-body').html(response);
                $('#modal-tasks .select2-tasks').select2();
                $('#modal-tasks .datepicker-tasks').datepicker({
                    rtl: mUtil.isRTL(),
                    orientation: "auto",
                    format: 'dd-mm-yyyy',
                    todayBtn: "linked",
                    clearBtn: true,
                    todayHighlight: true
                });
                input_numeral();
                $('#modal-tasks').modal('show');
                $('.btn-simpan').hide();
                $('.btn-batal').hide();
                loading_finish();
                $('.btn-simpan').show();
                $('.btn-batal').show();

            }
        });
    });

    $('.datatable-general, .datatable-no-pagination, .datatable-no-order').on('click', '.btn-project-file', function (e) {
        e.preventDefault();
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore");
        var href = $(this).attr('href');
        if (ignore) {
            if (direct != "") {
                window.location.href = direct;
            }
        }
        $.ajax({
            beforeSend: function () {
                loading_start();
            },
            url: href,
            type: 'GET',
            success: function (response) {
                $('#modal-file .modal-body').html(response);
                $('#modal-file').modal('show');
                loading_finish();
            }
        });
    });

    $('.modal').on('hidden.bs.modal', function () {
        if ($("input").hasClass('m_datepicker')) {
            var now = new Date();
            var date = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);
            var today = date + '-' + month + '-' + now.getFullYear();
            $('.m_datepicker').val(today);
        } else {
            $(':input', this).val('');
        }
        $('.form-group').removeClass('has-danger');
        $('.form-control-feedback').remove();
        //$('.img-preview').attr('src','');
    });

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".imgInp").change(function () {
        readURL(this);
    });

    /**
     * End General Operations
     */

    /**
     * Begin Stok Produk
     */

    $('.stok-produk-create[name="id_lokasi"]').change(function () {
        var id_lokasi = $(this).val();

    });

    /**
     * End Stok Produk
     */


    /**
     * Begin Transfer Stok Produk
     */

    $('.datatable-general').on('click', '.btn-transfer-stok-produk', function () {
        var row = $(this).siblings('textarea').val();
        var json = JSON.parse(row);
        var id_lokasi = $(this).data('id-agencies');
        $.each(json, function (key, value) {
            $('.' + key).html(value);
            $('[name="' + key + '"]').val(value);
        });
        $('[name="id_lokasi_tujuan"] option[value="' + id_lokasi + '"]').remove();
        $('#modal-transfer-stok-produk').modal('show');
    });

    /**
     * End Transfer Stok Produk
     */

    /**
     * Begin Transfer Stok Bahan
     */

    $('.datatable-general').on('click', '.btn-transfer-stok-bahan', function () {
        var row = $(this).siblings('textarea').val();
        var json = JSON.parse(row);
        var id_lokasi = $(this).data('id-agencies');
        $.each(json, function (key, value) {
            $('.' + key).html(value);
            $('[name="' + key + '"]').val(value);
        });
        $('[name="id_lokasi_tujuan"] option[value="' + id_lokasi + '"]').remove();
        $('#modal-transfer-stok-bahan').modal('show');
    });

    /**
     * End Transfer Stok Bahan
     */

    //  Begin Tambah Stok Opname

    $('.datatable-general').on('click', '.btn-tambah-stok-opname', function (e) {
        e.preventDefault();
        var self = $(this);
        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var json = JSON.parse(row);

        if (typeof route !== 'undefined') {
            $('#modal-tambah-stok-opname').parents('form').attr('action', route);
        }

        if (typeof redirect !== 'undefined') {
            $('#modal-tambah-stok-opname').parents('form').data('redirect', redirect);
        }
        if (direct !== '') {
            window.location.href = direct;
        }
        $.each(json, function (key, val) {
            var type = $('[name="' + key + '"]').attr('type');
            if (type == 'file') {
                $('#modal-tambah-stok-opname .img-preview').attr('src', base_url + '/upload/' + val);
            } else if (type == 'radio') {
                $('#modal-tambah-stok-opname [name="' + key + '"][value=' + val + ']').attr('checked', 'checked');
            } else if (type == 'checkbox') {
                $('#modal-tambah-stok-opname [name="' + key + '"]').prop('checked', (val == 1 ? true : false));
            } else {
                $('#modal-tambah-stok-opname [name="' + key + '"]').val(val);
                $('#modal-tambah-stok-opname [name="' + key + '"]').trigger('change');
                $('#modal-tambah-stok-opname .' + key).html(val);
                $('#modal-tambah-stok-opname .' + key).val(val);
            }
        });

        $('#modal-tambah-stok-opname').modal('show');

        return false;
    })

    // End Tambah Stok Opname

    // ======== START EDIT ASSEMBLY ========

    $('.datatable-general').on('click', '.btn-edit-assembly', function (e) {
        e.preventDefault();

        var row = $(this).parents('div').siblings('textarea').val();
        var route = $(this).data('route');
        var redirect = $(this).data('redirect');
        var direct = $(this).data('direct');
        var check = $(this).data('check');
        var json = JSON.parse(row);
        var ignore = $(this).data("ignore");
        var href = $(this).attr('href');

        if (check != '' || check != 0) {
            swal({
                title: "There is something wrong!",
                html: "<strong>Masih Terdapat Stok Pada Barang Assembly Ini, Jika Anda Ingin Melanjutkan Mengedit Barang Assembly Ini, Maka Akan Dilakukan Penghapusan Stok.</strong> <br>Apakah anda yakin akan melanjutkan?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Tidak",
            }).then(function (e) {
                if (e.value) {
                    window.location.href = direct;
                } else {

                }
            });
        } else {
            if (ignore) {
                if (direct != "") {
                    window.location.href = direct;
                }
            }
        }
    })

    // ======== END EDIT ASSEMBLY ========

    /**
     * Begin Rentang Harga
     */

    rentang_hapus();

    $('.btn-rentang-tambah').click(function () {

        var row = $('#tr-rentang-tambah tbody').html();
        var check = $('.table-produk-harga tbody tr').length;

        if (check > 0) {
            $('.table-produk-harga tbody tr:last').after(row);
        } else {
            $('.table-produk-harga tbody').append(row);
        }


        rentang_hapus();
    });

    function rentang_hapus() {
        $('.btn-rentang-hapus').click(function () {
            $(this).parents('tr').remove();
        });
    }

    /**
     * End Rentang Harga
     */

    /**
     * Begin Dashboard
     */

    $('.applyBtn').click(function () {
        if ($('form').hasClass('form-dashboard-filter')) {
            $('.form-dashboard-filter').submit();
        }
    });

    /**
     * End Dashboard
     */
    function form_send(self) {

        var action = self.attr('action');
        var method = self.attr('method');

        var redirect = self.data('redirect');
        var alert_show = self.data('alert-show');
        var alert_field_message = self.data('alert-field-message');
        var message = '';
        var message_field = '';

        var form = self;
        var formData = new FormData(form[0]);
        loading_start();

        $.ajax({
            url: action,
            type: method,
            data: formData,
            async: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "bearer c890cc0bd6562ed5b3a2a1105d6ddb9af9fdadab0c55216255bb0465d23c604aab845955b7b43787392c108f5a8cc82292435c99e7e93720145d89193cac06ba")
                loading_start();
            },
            error: function (request, error) {
                loading_finish();
                $('.form-control-feedback').remove();
                $('.form-group').removeClass('has-danger');
                $.each(request.responseJSON.errors, function (key, val) {
                    var type = $('[name="' + key + '"]').attr('type');
                    $('[name="' + key + '"]').parents('.form-group').addClass('has-danger');
                    message_field += val[0] + '<br />';
                    /**
                     * check apakah tipe inputan merupakan file atau tidak
                     */
                    if (type == 'file') {
                        $('[name="' + key + '"]').parents('.input-group').after('<div class="form-control-feedback">' + val[0] + '</div>');
                    } else {
                        /**
                         * check apakah inputan merupakan piutangLain atau tidak
                         */
                        if ($('[name="' + key + '"]').parent('div').hasClass('bootstrap-touchspin')) {
                            $('[name="' + key + '"]').parent('div').after('<div class="form-control-feedback">' + val[0] + '</div>');
                        } else {
                            $('[name="' + key + '"]').after('<div class="form-control-feedback">' + val[0] + '</div>');
                        }
                    }
                });

                if (alert_show == true) {

                    if (alert_field_message == true) {
                        if (message_field) {
                            message = message_field;
                        } else {
                            message = request.responseJSON.message;
                        }
                    } else {
                        message = request.responseJSON.message;
                    }

                    swal({
                        title: "There is something wrong!",
                        html: message,
                        type: "warning"
                    });
                }
            },
            success: function (data) {
                loading_finish();
                if (typeof redirect == 'undefined') {
                    $(".datatable").length > 0 ? table.ajax.reload() : window.location.reload();
                    form_clear();
                } else {
                    window.location.href = redirect;
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function form_clear() {
        $(".modal").modal("hide");
        // console.log(date)
        if ($("input[type=text]").hasClass('m_datepicker')) {
            var now = new Date();
            var date = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);
            var today = date + '-' + month + '-' + now.getFullYear();
            // console.log(today)
            $('.m_datepicker').val(today);
        } else {
            $("input[type=text]").val('');
        }
        $("input[type=file]").val('');
        $("input[type=email]").val('');
        $("input[type=password]").val('');
        $("input[type=checkbox]").prop("checked", false);
        $("textarea").val('');
        if ($('label').hasClass('custom-file-label selected')) {
            $('.custom-file-label').html(null);
        }
        if ($('select').hasClass('m-select2')) {
            $('.m-select2').val(null).trigger('change');
        }
        $('select option:first-child').attr("selected", "selected");
    }
});

/**
 * Proses ini untuk meremove menu pada sidebar jika user role pada menu tersebut adalah FALSE
 * jika TRUE, maka tetap ditampilkan menunya
 */
function user_role() {
    var check_user_role = $('#user-role-value').length;

    if (check_user_role > 0) {

        var user_role = $('#user-role-value').val();
        var obj = JSON.parse(user_role);
        $.each(obj, function (key, val) {
            // console.log(key+' akses = '+val.akses_menu)
            if (val.akses_menu) {
                $('.akses-' + key).removeAttr('hidden');
            }

            if (!val.akses_menu) {
                $('.akses-' + key).remove();
            }

            $.each(val, function (key_2, val_2) {
                // console.log('bagian 2 - '+key_2+' akses = '+val_2.akses_menu)
                if (val_2.akses_menu) {
                    $('.akses-' + key_2).removeAttr('hidden');
                    //console.log('delete');
                }

                if (!val_2.akses_menu) {
                    $('.akses-' + key_2).remove();
                    //console.log('delete');
                }



                if (typeof val_2 === 'object') {
                    $.each(val_2, function (key_3, val_3) {
                        // console.log('bagian 3 - '+key_3+' akses = '+val_3)
                        //console.log(key_3, val_3);
                    });
                }

                /**/

            });
        });
        $('#profile_menu').removeAttr('hidden');
    }
}

function roles_login() {

    var id_user_role = localStorage.getItem("id_user_role");
    var role_name = localStorage.getItem("role_name");
    $('[name="id_user_role"]').val(id_user_role);
    $('[name="id_user_role_list"]').val(id_user_role);
    $('.role-name').html(role_name);

    $('.roles-login-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function (json) {
                $('#modal-roles-login').modal('hide');
                $('#modal-roles-list').modal('show');
            },
            error: function (request, error) {

                swal({
                    title: "There is something wrong!",
                    html: "Saat memasukkan username atau password",
                    type: "warning"
                });

                $.each(request.responseJSON.errors, function (key, val) {
                    var type = $('[name="' + key + '"]').attr('type');
                    $('[name="' + key + '"]').parents('.form-group').addClass('has-danger');
                    message_field += val[0] + '<br />';
                    /**
                     * check apakah tipe inputan merupakan file atau tidak
                     */
                    if (type == 'file') {
                        $('[name="' + key + '"]').parents('.input-group').after('<div class="form-control-feedback">' + val[0] + '</div>');
                    } else {
                        /**
                         * check apakah inputan merupakan piutangLain atau tidak
                         */
                        if ($('[name="' + key + '"]').parent('div').hasClass('bootstrap-touchspin')) {
                            $('[name="' + key + '"]').parent('div').after('<div class="form-control-feedback">' + val[0] + '</div>');
                        } else {
                            $('[name="' + key + '"]').after('<div class="form-control-feedback">' + val[0] + '</div>');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('.btn-user-role-change').click(function () {
        var id_user_role = $('[name="id_user_role_list"]').val();
        var role_name = $('[name="id_user_role_list"] option:selected').html();


        $('[name="id_user_role"]').val(id_user_role);
        $('.role-name').html(role_name);
        localStorage.setItem('id_user_role', id_user_role);
        localStorage.setItem('role_name', role_name);

        $('#modal-roles-list').modal('hide')
    })
}


/**
 * Proses ini untuk meremove menu action yang ada di halaman active, jika menu action tersebut adalah FALSE
 */
function user_role_menu_action() {
    var check_user_role_menu_action = $('#user-role-menu-action').length;

    if (check_user_role_menu_action > 0) {
        var user_role_menu_action = $('#user-role-menu-action').val();
        var obj = JSON.parse(user_role_menu_action);

        $.each(obj, function (key, val) {
            if (key === 'action') {
                $.each(val, function (key_2, val_2) {
                    //console.log(key_2, val_2);
                    if (!val_2) {
                        $('.akses-' + key_2).remove();
                    }
                });
            } else {
                if (!val) {
                    $('.akses-' + key).remove();
                }
            }
        });
    }
}

function input_numeral() {
    $('.input-numeral').toArray().forEach(function (field) {
        new Cleave(field, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        })
    });
}

function input_decimal() {
    $('.input-decimal').toArray().forEach(function (field) {
        new Cleave(field, {
            numeral: true,
            numeralDecimalScale: 4
        })
    });
}

function strtonumber(str) {
    var arr = str.split(',');
    var res = '';
    $.each(arr, function (i, val) {
        res += val
    });
    return res
}

function loading_start() {
    $('.form-send [type="submit"]').prop('disabled', true);
    $('.wrapper-loading').hide().removeClass('hidden').fadeIn();
}

function loading_finish() {
    $('.form-send [type="submit"]').prop('disabled', false);
    $('.wrapper-loading').fadeOut().addClass('hidden');
}

function format_number(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

