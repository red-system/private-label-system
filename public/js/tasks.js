$(document).ready(function () {

    // add_row_pembayaran();
    // total();
    kirim_new();
    kirim_next();
    kirim_complete();
    btn_show();
    btn_hide();

    btn_file();
    file_selected();

});
var base_url = $('#base-value').data('base-url');

var csrf_token = $('#base-value').data('csrf-token');

function btn_file() {
    $(".btn-file-custom").on("click", function () {
        $("#file-banyak").click();
    });


}

function file_selected() {
    $("#file-banyak").on("change", function () {
        var input = document.getElementById('file-banyak').files;
        console.log(input.length);
        console.log(input);

        if (input){
            var tipe = input[0]['name'].split('.').pop();
            $('#text-pilih').html(input[0]['name']);
            $('#tipe-file').val(tipe);
        }
        else {
            $('#text-pilih').html('');
        }

    });


}

function kirim_new() {

    $("#tabel-new").on("change", ".status-baru", function () {
        var index = $(this).parents('tr').data('index');
        // var route = $('.tabel-new tbody tr:nth-child(' + index + ')').data('route');
        var route = $('#route-'+index).val();
        var st_baru = $('#status-baru-'+index).val();
        console.log(st_baru);
        $.ajax({
            // url: self.data('route'),
            // url: $(this).parents('tr').data('route'),
            url: route,
            type: "POST",
            data: {
                _token: csrf_token,
                0: st_baru
            },
            success: function () {
                // if (table === "") {
                    location.reload();
                // } else {
                //     // console.log('2')
                //     table.ajax.reload();
                // }
            },
            error: function (request) {
                var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                swal({
                    title: title,
                    html: request.responseJSON.message,
                    type: "warning"
                });
            }
        })
    });
}

function kirim_next() {

    $("#tabel-next").on("change", ".status-baru", function () {
        var index = $(this).parents('tr').data('index');
        // var route = $('.tabel-new tbody tr:nth-child(' + index + ')').data('route');
        var route = $('#route-next-'+index).val();
        var st_baru = $('#status-next-'+index).val();
        console.log(st_baru);
        $.ajax({
            // url: self.data('route'),
            // url: $(this).parents('tr').data('route'),
            url: route,
            type: "POST",
            data: {
                _token: csrf_token,
                0: st_baru
            },
            success: function () {
                // if (table === "") {
                location.reload();
                // } else {
                //     // console.log('2')
                //     table.ajax.reload();
                // }
            },
            error: function (request) {
                var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                swal({
                    title: title,
                    html: request.responseJSON.message,
                    type: "warning"
                });
            }
        })
    });
}

function kirim_complete() {

    $("#tabel-complete").on("change", ".status-baru", function () {
        var index = $(this).parents('tr').data('index');
        // var route = $('.tabel-new tbody tr:nth-child(' + index + ')').data('route');
        var route = $('#route-complete'+index).val();
        var st_baru = $('#status-complete-'+index).val();
        console.log(st_baru);
        $.ajax({
            // url: self.data('route'),
            // url: $(this).parents('tr').data('route'),
            url: route,
            type: "POST",
            data: {
                _token: csrf_token,
                0: st_baru
            },
            success: function () {
                // if (table === "") {
                location.reload();
                // } else {
                //     // console.log('2')
                //     table.ajax.reload();
                // }
            },
            error: function (request) {
                var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                swal({
                    title: title,
                    html: request.responseJSON.message,
                    type: "warning"
                });
            }
        })
    });
}

function btn_show() {

    $("#show_all").on("click", function () {
        $('#chat_page_all').removeClass('hidden');
        // $('#show_all').addClass('hidden');
        $('#chat_page').addClass('hidden');
        // $('#show_less').removeClass('hidden');
    });
}

function btn_hide() {

    $("#show_less").on("click", function () {
        $('#chat_page').removeClass('hidden');
        // $('#show_all').addClass('hidden');
        $('#chat_page_all').addClass('hidden');
        // $('#show_less').removeClass('hidden');
    });
}