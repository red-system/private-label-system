$('[name="qty"], [name="harga_beli"], [name="tanggal_beli"], [name="umur_ekonomis"]').on('change keyup', function () {
    var qty = $('[name="qty"]').val();
    var harga_beli = $('[name="harga_beli"]').val();
    var umur_ekonomis = $('[name="umur_ekonomis"]').val();
    var profisi = $('[name="provisi"]').val();

    if (qty !== '' || harga_beli !== '' || umur_ekonomis !== '') {
        var harga_beli_2 = parseFloat(harga_beli) - parseFloat(profisi);
        $('[name="harga_beli_2"]').val(harga_beli_2);
        hitung_total_beli(qty, harga_beli);
        hitung_beban_perbulan(harga_beli, umur_ekonomis);
        hitung_akumulasi_perbulan(harga_beli);
        hitung_tgl_pensiun();

    }
});

$('[name="harga_beli"]').on('change keyup', function () {
    var harga_beli = $(this).val();
    var penyusutan_status = $('[name="id_kategori_asset"] option:selected').data('penyusutan-status');

    if (penyusutan_status === 'no') {
        $('[name="nilai_buku"]').val(harga_beli);
    }
});

$('[name="provisi"]').on('change keyup', function () {
    var harga_beli = $('[name="harga_beli_2"]').val();
    var qty = $('[name="qty"]').val();
    var asset_profisi = $('[name="provisi"]').val();
    var umur_ekonomis = $('[name="umur_ekonomis"]').val();

    if (asset_profisi !== '') {
        var new_harga_beli = parseFloat(harga_beli) + parseFloat(asset_profisi);
        $('[name="harga_beli"]').val(new_harga_beli);
        hitung_total_beli(qty, new_harga_beli);
        hitung_beban_perbulan(new_harga_beli, umur_ekonomis);
        hitung_akumulasi_perbulan(new_harga_beli);

    }
});

$('[name="id_kategori_asset"]').change(function () {
    var penyusutan_status = $('[name="id_kategori_asset"] option:selected').data('penyusutan-status');
    var fields = [
        'provisi',
        'nilai_residu',
        'umur_ekonomis',
        'akumulasi_beban',
        'beban_perbulan'
    ];

    if (penyusutan_status === 'no') {
        fields.forEach(function (item) {
            $('[name="' + item + '"]').prop('disabled', true);
            $('[name="' + item + '"]').parents('.form-group').children('label').removeClass('required');
        });
    } else {
        fields.forEach(function (item) {
            $('[name="' + item + '"]').val(0).prop('disabled', false);
            $('[name="' + item + '"]').parents('.form-group').children('label').addClass('required');
        });
    }
});

$('.btn-hitung-akumulasi').click(function () {

    var harga_beli = $('[name="edit_asset_harga_beli"]').val();

    hitung_akumulasi_perbulan_jurnal(harga_beli);
});

$('[name="tanggal_beli"]').on('change keyup', function () {
    var tanggal_beli = $('[name="tanggal_beli"]').val();
    tanggal_beli = reformat_date(tanggal_beli);

    var tahun_baru = tanggal_beli.getFullYear();
    var bulan_baru = tanggal_beli.getMonth() + 1;
    var hari_baru = tanggal_beli.getDate();
    if (bulan_baru <= 9) {
        bulan_baru = '0' + bulan_baru;
    }
    if (hari_baru <= 9) {
        hari_baru = '0' + hari_baru;
    }
    var tanggal_baru = tahun_baru + '-' + bulan_baru + '-' + hari_baru;

    $('[name="tanggal_beli_2"]').val(tanggal_baru);

    var qty = $('[name="qty"]').val();
    var harga_beli = $('[name="harga_beli"]').val();
    var umur_ekonomis = $('[name="umur_ekonomis"]').val();

    if (qty !== '' || harga_beli !== '' || umur_ekonomis !== '') {
        hitung_total_beli(qty, harga_beli);
        hitung_beban_perbulan(harga_beli, umur_ekonomis);
        hitung_akumulasi_perbulan(harga_beli);

    }
    set_terhitung_tgl();

});

$('[name="tanggal_cek"]').on('change', function () {
    var penyusutan_status = $('[name="id_kategori_asset"] option:selected').data('penyusutan-status');
    if (penyusutan_status === 'yes') {

        var tanggal_beli = $('[name="tanggal_beli"]').val();
        tanggal_beli = reformat_date(tanggal_beli);
        var tahun_baru = tanggal_beli.getFullYear();
        var bulan_baru = tanggal_beli.getMonth() + 1;
        var hari_baru = tanggal_beli.getDate();
        if (bulan_baru <= 9) {
            bulan_baru = '0' + bulan_baru;
        }
        if (hari_baru <= 9) {
            hari_baru = '0' + hari_baru;
        }
        var tanggal_baru = tahun_baru + '-' + bulan_baru + '-' + hari_baru;

        $('[id="tanggal_beli_2"]').val(tanggal_baru);

        var qty = $('[name="qty"]').val();
        var harga_beli = $('[name="harga_beli"]').val();
        var umur_ekonomis = $('[name="umur_ekonomis"]').val();

        if (qty !== '' || harga_beli !== '' || umur_ekonomis !== '') {
            hitung_total_beli(qty, harga_beli);
            hitung_beban_perbulan(harga_beli, umur_ekonomis);
            hitung_akumulasi_perbulan(harga_beli);
        }
    }

});


function hitung_total_beli(qty, harga_beli) {
    var total_beli = parseFloat(qty) * parseFloat(harga_beli);
    $('[name="total_beli"]').val(total_beli.toFixed(2));
}

function set_terhitung_tgl() {
    // Reformat date to yyyy-mm-dd
    var tanggal_beli = $('[name="tanggal_beli"]').val();
    tanggal_beli = reformat_date(tanggal_beli);
    //tanggal_beli = tanggal_beli.toString("yyyy-mm-dd");
    //console.log("0" + tanggal_beli).slice(-2);
    //console.log(tanggal_beli.getMonth());


    var tahun = tanggal_beli.getFullYear();
    var hari = tanggal_beli.getDate();
    //console.log(tanggal_beli.getMonth()+' '+tanggal_beli.getFullYear()+' '+tanggal_beli.getDate());
    if (hari > 15) {
        var bulan = tanggal_beli.getMonth() + 2;
    } else {
        var bulan = tanggal_beli.getMonth() + 1;
    }
    var lastDay = new Date(tahun, bulan, 0);
    var tahun2 = lastDay.getFullYear();
    var bulan2 = lastDay.getMonth() + 1;
    var hari2 = lastDay.getDate();
    if (bulan2 <= 9) {
        bulan2 = '0' + bulan2;
    }
    if (hari2 <= 9) {
        hari2 = '0' + hari2;
    }
    //var lastDay_2 = tahun2 + '-' + bulan2 + '-' + hari2;
    var lastDay_2 = hari2 + '-' + bulan2 + '-' + tahun2;
    $('[name="terhitung_tanggal"]').val(lastDay_2);
}

function hitung_tgl_pensiun() {
    var tanggal_beli = $('[name="tanggal_beli"]').val();
    tanggal_beli = reformat_date(tanggal_beli);

    var tahun = tanggal_beli.getFullYear();
    var bulan = tanggal_beli.getMonth() + 1;
    var hari = tanggal_beli.getDate();
    var umur_ekonomis = $('[name="umur_ekonomis"]').val();
    var thn = parseInt(tahun) + parseInt(umur_ekonomis);
    if (bulan <= 9) {
        bulan = '0' + bulan;
    }
    if (hari <= 9) {
        hari = '0' + hari;
    }
    var kbs = thn / 4;
    var tanggal_baru = hari + '-' + bulan + '-' + thn;


    $('[name="tanggal_pensiun"]').val(tanggal_baru);

}

function hitung_beban_perbulan(harga_beli, umur_ekonomis) {
    // var beban_perbulan = parseFloat(harga_beli)/(parseFloat(umur_ekonomis) * parseFloat(12));
    var beban_perbulan = harga_beli / (umur_ekonomis * 12);
    $('[name="beban_perbulan"]').val(beban_perbulan.toFixed(2));
}

function hitung_akumulasi_perbulan(harga_beli) {
    var tanggal_beli = $('[name="tanggal_beli"]').val();
    tanggal_beli = reformat_date(tanggal_beli);
    console.log(tanggal_beli);

    var tanggal_sekarang = $('[name="tanggal_sekarang"]').val();
    tanggal_sekarang = reformat_date(tanggal_sekarang);
    console.log(tanggal_sekarang);

    var beban_perbulan = $('[name="beban_perbulan"]').val();

    var tahun_1 = tanggal_beli.getFullYear();
    var tahun_2 = tanggal_sekarang.getFullYear();
    var bulan_1 = tanggal_beli.getMonth();
    var bulan_2 = tanggal_sekarang.getMonth();
    if ($('[name="tanggal_cek"]').is(':checked')) {
        var jml_bulan = (tahun_2 - tahun_1) * 12 + (bulan_2 - bulan_1);
    } else {
        var jml_bulan = (tahun_2 - tahun_1) * 12 + (bulan_2 - bulan_1) + 1;
    }

    console.log(jml_bulan);

    if (jml_bulan > 0) {
        var akumulasi_beban = (parseFloat(jml_bulan)) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = akumulasi_beban;
        $('[name="akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    } else {
        var akumulasi_beban = parseFloat(jml_bulan) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = akumulasi_beban;
        $('[name="akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    }


    hitung_nilai_buku(akumulasi_beban_2, harga_beli);

}

function hitung_nilai_buku(akumulasi_beban, harga_beli) {
    var nilai_buku = parseFloat(harga_beli) - parseFloat(akumulasi_beban);
    $('[name="nilai_buku"]').val(nilai_buku.toFixed(2));
}

function hitung_akumulasi_perbulan_jurnal(harga_beli) {
    var tanggal_beli = $('[name="tanggal_beli_2"]').val();
    tanggal_beli = reformat_date(tanggal_beli);

    var tanggal_sekarang = $('[name="tanggal_sekarang"]').val();
    tanggal_sekarang = reformat_date(tanggal_sekarang);

    var beban_perbulan = $('[id="edit_beban_perbulan"]').val();

    var tahun_1 = tanggal_beli.getFullYear();
    var tahun_2 = tanggal_sekarang.getFullYear();
    var bulan_1 = tanggal_beli.getMonth();
    var bulan_2 = tanggal_sekarang.getMonth();
    var jml_bulan = (tahun_2 - tahun_1) * 12 + (bulan_2 - bulan_1) + 1;

    if (jml_bulan > 0) {
        var akumulasi_beban = (parseFloat(jml_bulan)) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = parseFloat(akumulasi_beban);
        $('[name="akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    } else {
        var akumulasi_beban = parseFloat(jml_bulan) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = parseFloat(akumulasi_beban);
        $('[name="akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    }


    hitung_nilai_buku_jurnal(akumulasi_beban_2, harga_beli);

}

function hitung_nilai_buku_jurnal(akumulasi_beban, harga_beli) {
    var nilai_buku = parseFloat(harga_beli) - parseFloat(akumulasi_beban);
    $('[name="nilai_buku"]').val(nilai_buku.toFixed(2));
}

function reformat_date(tanggal_beli) {

    var day = tanggal_beli.substring(0, 2);
    var month = tanggal_beli.substring(3, 5);
    var year = tanggal_beli.substring(6, 10);
    tanggal_beli = year + '-' + month + '-' + day;
    tanggal_beli = new Date(tanggal_beli);

    return tanggal_beli;
}

/*
$('.btn-posting-jurnal').click(function() {
    var href = $(this).data('href');
    $.ajax({
        url: href,
        success: function(data) {
            $.each(data.field, function(field, value) {
                if($('#modal-posting-asset-to-jurnal [name="'+field+'"]').is('select')) {
                    //$('#modal-edit [name="'+field+'"]').val(value);
                    $('#modal-posting-asset-to-jurnal [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                }
                else if ($('#modal-posting-asset-to-jurnal [name="'+field+'"]').is(':checkbox')) {
                    if (value == 'Aktif') {
                        $('#modal-posting-asset-to-jurnal [name="'+field+'"]').attr('checked', true);
                    }
                }
                else if (field == 'brg_product_img') {
                    if ($('#modal-posting-asset-to-jurnal [id="imgScrEdit"]').is('img')) {
                        if (value != null ) {
                            var l = window.location;
                            var base_url = l.protocol + "//" + l.host + "/";
                            // console.log(base_url);
                            $('#modal-posting-asset-to-jurnal [id="imgScrEdit"]').attr('src', base_url + value);
                        }
                        else {
                            var l = window.location;
                            var base_url = l.protocol + "//" + l.host + "/";
                            $('#modal-posting-asset-to-jurnal [id="imgScrEdit"]').attr('src', base_url + "img/icon/no_image.png");
                        }
                    }
                }
                else if(field == 'akumulasi_beban'){
                    $('#modal-posting-asset-to-jurnal [name="'+field+'"]').val(0);
                }
                else if(field == 'nilai_buku'){
                    $('#modal-posting-asset-to-jurnal [name="'+field+'"]').val(0);
                }
                else {
                    $('#modal-posting-asset-to-jurnal [name="'+field+'"]').val(value);
                }
            });
            $('#modal-posting-asset-to-jurnal form').attr('action', data.action);
            $('#modal-posting-asset-to-jurnal').modal('show');
        }
    });
});*/
