$(document).ready(function () {
    var pageMethod = $('#base-value').data('page-method');

    barang_baku_add();
    barang_produksi_add();
    select_barang_baku();
    select_lokasi_baku();
    change_qty();

    select_barang_produksi();

    hapus_barang();
    produksi_detail();
    produksi_publish();

});


var _token = $('#base-value').data('csrf-token');
var route_detail_produksi_delete = $('#base-value').data('route-detail-produksi-delete');
var route_produksi_barang_lokasi = $('#base-value').data('route-produksi-barang-lokasi');
var base_url = $('#base-value').data('base-url');

function hapus_barang() {
    $('.btn-hapus-barang').click(function (e) {
        e.preventDefault();
        var confirm = $(this).data('confirm');
        var id_detail_produksi = $(this).parents('tr').data('id-detail-produksi');
        var self = $(this);

        if (confirm) {
            swal({
                title: "Perhatian ...",
                text: "Yakin hapus data ini ?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    $.ajax({
                        url: route_detail_produksi_delete,
                        type: "POST",
                        data: {
                            _token: _token,
                            id_detail_produksi: id_detail_produksi
                        },
                        beforeSend: function () {
                            loading_start();
                        },
                        success: function () {
                            self.parents('tr').remove();
                            loading_finish();
                        }
                    });
                }
            });
        } else {
            $(this).parents('tr').remove();
        }

    });
}


////////////////////// Barang Baku /////////////////

function barang_baku_add() {
    $('.btn-barang-baku-add').click(function () {
        var row_barang_baku = $('.row-barang-baku table tbody').html();
        $('.table-barang-baku tbody').append(row_barang_baku);

        var index = $('.table-barang-baku tbody tr').length;

        $('.table-barang-baku tbody tr:nth-child(' + index + ') .touchspin').TouchSpin(touchspin_number_decimal);
        $('.table-barang-baku tbody tr:nth-child(' + index + ') select').select2();

        $('footer').hide();

        hapus_barang();
        select_barang_baku();
        select_lokasi_baku();
        change_qty();
    });
}

function select_barang_baku() {
    $('[name="id_barang_baku[]"]').change(function () {

        var id_barang = $(this).val();
        var kode_barang = $('[name="id_barang_baku[]"] option[value="' + id_barang + '"]').data('kode-barang');
        var nama_barang = $('[name="id_barang_baku[]"] option[value="' + id_barang + '"]').data('nama-barang');
        // var harga_jual_barang = $('[name="id_barang_baku[]"] option[value="' + id_barang + '"]').data('harga-jual-barang');
        var harga_beli_terakhir_barang = $('[name="id_barang_baku[]"] option[value="' + id_barang + '"]').data('harga-beli-terakhir-barang');
        var number_format = format_number(harga_beli_terakhir_barang);
        var qty = $(this).parents('td').siblings('td.qty').children('div').children('[name="qty_baku[]"]').val();
        var nilai_bahan_baku = parseFloat(qty) * parseInt(harga_beli_terakhir_barang);
        var self = $(this);
        nilai_bahan_baku = nilai_bahan_baku.toFixed(2);

        $(this).parents('td').siblings('.nama-barang').html("(" + kode_barang + ") - " + nama_barang);
        $(this).parents('td').siblings('.harga-barang').html(number_format);
        $(this).parents('td').siblings('td.value-hidden').children('[name="harga_barang_baku[]"]').val(harga_beli_terakhir_barang);
        $(this).parents('td').siblings('td.value-hidden').children('[name="nama_barang_baku[]"]').val(nama_barang);
        $(this).parents('td').siblings('td.value-hidden').children('[name="nilai_bahan_baku[]"]').val(nilai_bahan_baku);
        $(this).parents('td').siblings('td.nilai-bahan').html(format_number(nilai_bahan_baku));

        $.ajax({
            url: route_produksi_barang_lokasi,
            type: 'POST',
            data: {
                _token: _token,
                id_barang: id_barang
            },
            success: function (response) {
                var data = [{
                    id: '',
                    text: 'Pilih Lokasi Stok',
                    jml_barang: 0
                }];
                $.each(response, function (index, item) {
                    data.push({
                        id: item.id,
                        text: item.lokasi + ' (' + item.jml_barang + ' ' + item.satuan + ')',
                        jml_barang: item.jml_barang
                    });
                });

                self.parents('td').siblings('td.lokasi').children('select').html('').select2({'data': data});
            }
        });

    });
}

function select_lokasi_baku() {
    $('[name="id_stok_baku[]"]').change(function () {
        var jml_barang = $(this).select2('data')[0].jml_barang;

        // $(this).parents('td').siblings('td.qty').children('div').children('.touchspin').val(jml_barang - 1);
        $(this).parents('td').siblings('td.qty').children('div').children('.touchspin').trigger("touchspin.updatesettings", {max: jml_barang});


    });
}

function change_qty() {
    $('[name="qty_baku[]"]').change(function () {
        var qty = $(this).val();
        var harga = $(this).parents('td').siblings('td.value-hidden').children('[name="harga_barang_baku[]"]').val();
        var nilai_bahan_baku = parseFloat(qty) * parseInt(harga);
        nilai_bahan_baku = nilai_bahan_baku.toFixed(2);

        $(this).parents('td').siblings('td.nilai-bahan').html(format_number(nilai_bahan_baku));
        $(this).parents('td').siblings('td.value-hidden').children('[name="nilai_bahan_baku[]"]').val(nilai_bahan_baku);
    });
}


////////////////////// Barang Produksi ////////////////////////


function barang_produksi_add() {
    $('.btn-barang-produksi-add').click(function () {
        var row_barang_produksi = $('.row-barang-produksi table tbody').html();
        $('.table-barang-produksi tbody').append(row_barang_produksi);

        var index = $('.table-barang-produksi tbody tr').length;

        $('.table-barang-produksi tbody tr:nth-child(' + index + ') .touchspin').TouchSpin(touchspin_number_decimal);
        $('.table-barang-produksi tbody tr:nth-child(' + index + ') select').select2();

        $('footer').hide();

        hapus_barang();
        select_barang_produksi();
    });
}

function select_barang_produksi() {
    $('[name="id_barang_produksi[]"]').change(function () {

        var id_barang = $(this).val();
        var kode_barang = $('[name="id_barang_produksi[]"] option[value="' + id_barang + '"]').data('kode-barang');
        var nama_barang = $('[name="id_barang_produksi[]"] option[value="' + id_barang + '"]').data('nama-barang');
        var self = $(this);

        $(this).parents('td').siblings('.nama-barang').html("(" + kode_barang + ") - " + nama_barang);
        $(this).parents('td').siblings('td.value-hidden').children('[name="nama_barang_produksi[]"]').val(nama_barang);

        // $.ajax({
        //     url: route_produksi_barang_lokasi,
        //     type: 'POST',
        //     data: {
        //         _token: _token,
        //         id_barang: id_barang
        //     },
        //     success: function (response) {
        //         var data = [{
        //             id: '',
        //             text: 'Pilih Lokasi Stok',
        //             jml_barang: 0
        //         }];
        //         $.each(response, function (index, item) {
        //             data.push({
        //                 id: item.id,
        //                 text: item.lokasi + ' (' + item.jml_barang + ' ' + item.satuan + ')',
        //                 jml_barang: item.jml_barang
        //             });
        //         });
        //
        //         self.parents('td').siblings('td.lokasi').children('select').html('').select2({'data': data});
        //     }
        // });

    });
}


/////////////////// LIST ///////////////////


function produksi_detail() {
    $('.btn-produksi-detail').click(function () {
        loading_start();
        var route = $(this).data('route');
        $.ajax({
            url: route,
            success: function (table_view) {
                $('#modal-detail .modal-body').html(table_view);
                $('#modal-detail').modal('show');
                loading_finish();
            }
        })
    });
}

function produksi_publish() {
    $('.btn-publish').click(function () {
        var self = $(this);
        swal({
            title: "Perhatian ...",
            html: "Yakin <strong>Publish</strong> produksi ini ?<br /> Setelah di-Publish, akan <strong class='m--font-danger'>Mengurangi</strong> stok bahan.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: self.data('route'),
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (request, error) {
                        swal({
                            title: "Perhatian",
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
    });
}
