$(document).ready(function () {

    proses_sub_total();
    proses_total();


});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');


function proses_sub_total() {
    $('.qty-retur').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');
        var data = $(this).parents('tr').data('id-penjualan-produk');

        var harga = strtonumber($('.table-produk tbody tr:nth-child(' + index + ') .harga').html());

        var nominal = $('.table-produk tbody tr:nth-child(' + index + ') .nominal').html();
        var persen = $('.table-produk tbody tr:nth-child(' + index + ') .persen').html();
        var diskon = parseFloat(harga) * parseFloat(persen) / 100;
        var harga_net = parseFloat(harga) - parseFloat(nominal) - parseFloat(diskon);

        var qty = $('.table-produk tbody tr:nth-child(' + index + ') .qty-retur').val();
        console.log(harga);
        var sub_total = parseFloat(harga_net) * parseFloat(qty);
        var sub_total_format = format_number(sub_total);

        $('.table-produk tbody tr:nth-child(' + index + ') .sub_total').val(sub_total);
        $('.table-produk tbody tr:nth-child(' + index + ') .td-sub-total').html(sub_total_format);
        $('[name="subtotal_retur[' + data + ']"]').val(sub_total);

        proses_total();
    });
}

function proses_total() {
    var total = 0;
    $('.table-produk .td-sub-total').each(function () {
        var sub_total = strtonumber($(this).html());
        console.log(sub_total)
        total = parseFloat(total) + parseFloat(sub_total);
    });
    $('[name="total_retur"]').val(total);
    $('.total').html(total);

    proses_total_semua();
}

function proses_total_semua() {
    var total = $('[name="total_retur"]').val();
    var total_sebelum = strtonumber($('.total_sebelumnya').html());
    var total_sekarang = parseFloat(total_sebelum) + parseFloat(total);

    $('.total_keseluruhan').html(total_sekarang);
}
