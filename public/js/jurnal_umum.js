$(document).ready(function () {

    add_row_transaksi();

    ////// Saat edit ////////

    change_jenis_transaksi();
    change_proses_balance();
    delete_row_transaksi();

    footer_hide();
});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');
var labelBalance = $('#base-value').data('label-balance');
var labelBalanceNot = $('#base-value').data('label-balance-not');


function add_row_transaksi() {
    $('.btn-add-row-transaksi').click(function () {
        var row_transaksi = $('.row-transaksi tbody').html();
        $('.table-transaksi tbody').append(row_transaksi);
        $('.input-numeral').toArray().forEach(function (field) {
            new Cleave(field, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            })
        });

        var index = $('.table-transaksi tbody tr').length;

        $('.table-transaksi tbody tr:nth-child(' + index + ')').data('index', index);
        $('.table-transaksi tbody tr:nth-child(' + index + ') .touchspin-number-decimal-js').TouchSpin(touchspin_number_decimal);
        $('.table-transaksi tbody tr:nth-child(' + index + ') .select2-transaksi').select2();

        change_proses_balance();
        delete_row_transaksi();
        change_jenis_transaksi(index);
        footer_hide();
    });
}

function change_jenis_transaksi(index) {
    $('.jenis_transaksi').change(function () {
        var jenis_transaksi = $(this).val();
        if (jenis_transaksi == 'debet') {
            $('.table-transaksi tbody tr:nth-child(' + index + ') td.trs_kredit input').prop('readonly', true);
            $('.table-transaksi tbody tr:nth-child(' + index + ') td.trs_debet input').prop('readonly', false);
            $('.table-transaksi tbody tr:nth-child(' + index + ') td.trs_kredit input').val(0);
        } else {
            $('.table-transaksi tbody tr:nth-child(' + index + ') td.trs_kredit input').prop('readonly', false);
            $('.table-transaksi tbody tr:nth-child(' + index + ') td.trs_debet input').val(0);
            $('.table-transaksi tbody tr:nth-child(' + index + ') td.trs_debet input').prop('readonly', true);
        }
    });
}

function delete_row_transaksi() {
    $('.btn-delete-row-transaksi').click(function () {
        $(this).parents('tr').remove();
    });
}

function change_proses_balance() {
    $('.trs-debet, .trs-kredit').on('change keyup', '', function () {
        var total_debet = 0;
        var total_kredit = 0;
        var status_label = '';

        $('.trs-debet').each(function () {
            var trs_debet = $(this).val();
            total_debet += parseFloat(trs_debet);
        });

        $('.trs-kredit').each(function () {
            var trs_kredit = $(this).val();
            total_kredit += parseFloat(trs_kredit);
        });

        console.log(total_debet, total_kredit);

        if (total_debet == total_kredit) {
            status_label = labelBalance;
        } else {
            status_label = labelBalanceNot;
        }

        $('.th-status-label').html(status_label);

    });
}

function footer_hide() {
    $('footer').hide();
}