$(document).ready(function () {

    change_pict();
    old_pict();
});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');
var method_proses = $('#base-value').data('method-proses');

function change_pict() {
    var old = $('#old').val();
    $('.foto-pict').on('change keyup', function () {
        if($('.hapus-saja').val() === 0 || $('.hapus-saja').val() === '0'){
            $('.image-input-wrapper').css("background-image", "url({{ asset('upload/'."+old+") }})");
        }
        else {
            $('#kt_user_edit_avatar').css('background-image', 'none');
        }
    });
}
function old_pict() {
    var old = $('#old').val();
    $('.hapus-foto').click( function () {
        console.log('foto lama')
            $('.image-input-wrapper').css("background-image", "url({{ asset('upload/'.empty.png) }})");
    });
}