$(document).ready(function () {
    ////// Saat edit ////////

    change_proses_balance();

});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');

function change_proses_balance() {
    $('.jml-anggaran').on('change keyup', '', function () {
        var total_anggaran = 0;

        $('.jml-anggaran').each(function () {
            var jml_anggaran = strtonumber($(this).val());
            total_anggaran += parseFloat(jml_anggaran);
        });

        $('.th-total-anggaran-label').html(format_number(total_anggaran));

    });
}
