<form action="" method="post"
      class="m-form form-send"
>
    {{ csrf_field() }}
    <div class="modal" id="modal-metric" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document"  style="width:50%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Impr </label>
                            <input type="text" class="form-control m-input input-numeral" name="impr">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Clicks </label>
                            <input type="text" class="form-control m-input input-numeral" name="clicks">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Spend </label>
                            <input type="text" class="form-control m-input input-numeral" name="spend">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">All Revenue </label>
                            <input type="text" class="form-control m-input input-numeral" name="all_rev">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Paid Revenue </label>
                            <input type="text" class="form-control m-input input-numeral" name="paid_rev">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">ROAS </label>
                            <input type="text" class="form-control m-input input-numeral" name="roas">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>