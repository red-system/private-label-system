@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/project.js') }}" type="text/javascript"></script>
    {{--    <script src="{{ asset('assets/demo/demo5_new/js/scripts.bundle.js')}}"></script>--}}
    <script src="{{ asset('assets/demo/demo5_new/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>
@endsection

@section('body')
    @include('project/projectList/projectEdit')
    {{--    @include('project/projectList/projectDetail')--}}
    @include('project/projectList/projectTasks')
    @include('project/projectList/trTasks')
    @include('project/projectList/modalMetric')
    @include('project/projectList/projectListFile')
    @include('project/projectList/trFile')


    <!-- END: Left Aside -->
    <div class="container">
        <input type="hidden" id="list_url" data-list-url="{{route('projectListSingle',$params)}}">
        <div id="table_coloumn" style="display: none">{{$view}}</div>
        <div id="action_list" style="display: none">{{$actionButton}}</div>
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-custom  gutter-b card-shadowless bg-white">
            <br>
            <!--begin::Header-->
            <div class="card-body py-0">
                <div class="table-responsive">
                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">Order ID</th>
                            <th>Channel</th>
                            <th>Website Name</th>
                            <th>Order Title</th>
                            <th>Order Date</th>
                            <th>Status</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection
