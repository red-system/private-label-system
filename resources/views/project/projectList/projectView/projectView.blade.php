@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/project.js') }}" type="text/javascript"></script>
    {{--    <script src="{{ asset('assets/demo/demo5_new/js/scripts.bundle.js')}}"></script>--}}
    <script src="{{ asset('assets/demo/demo5_new/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>
@endsection

@section('body')
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow fade show gutter-b" role="alert">
                <div class="alert-icon">
										<span class="svg-icon svg-icon-primary svg-icon-xl">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
											<svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"></rect>
													<path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z"
                                                          fill="#000000" opacity="0.3"></path>
													<path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z"
                                                          fill="#000000" fill-rule="nonzero"></path>
												</g>
											</svg>
                                            <!--end::Svg Icon-->
										</span>
                </div>
                <div class="alert-text">Metronic extends
                    <code>Bootstrap Buttons</code>with a variety of options to provide unique looking buttons that
                    matches Metronic's design standards.
                    <br>For more info on the original Bootstrap utilities please visit the official
                    <a class="font-weight-bold" href="https://getbootstrap.com/docs/4.3/components/buttons/"
                       target="_blank">Bootstrap Documentation</a>.
                </div>
            </div>
            <!--end::Notice-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Base Examples</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Bootstrap includes several predefined button styles, each serving its own semantic
                                    purpose, with a few extras thrown in for more control.</p>
                                <div class="example-preview">
                                    <p>
                                        <button type="button" class="btn btn-primary mr-2">Primary</button>
                                        <button type="button" class="btn btn-secondary mr-2">Secondary</button>
                                        <button type="button" class="btn btn-success mr-2">Success</button>
                                        <button type="button" class="btn btn-danger mr-2">Danger</button>
                                        <button type="button" class="btn btn-warning mr-2">Warning</button>
                                    </p>
                                    <p>
                                        <button type="button" class="btn btn-info mr-2">Info</button>
                                        <button type="button" class="btn btn-light mr-2">Light</button>
                                        <button type="button" class="btn btn-dark mr-2">Dark</button>
                                        <button type="button" class="btn btn-link">Link</button>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>button</span> <span
                                                                                class="token attr-name">type</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>button<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-primary<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Secondary<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-info<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Info<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Light<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-dark<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-link<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Link<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Button Tags</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>The
                                    <code>.btn</code>classes are designed to be used with the
                                    <code>&lt;button&gt;</code>element. However, you can also use these classes on
                                    <code>&lt;a&gt;</code>or
                                    <code>&lt;input&gt;</code>elements (though some browsers may render slightly
                                    different).</p>
                                <div class="example-preview">
                                    <a class="btn btn-secondary mr-2" href="#" role="button">Link</a>
                                    <button class="btn btn-secondary mr-2" type="submit">Button</button>
                                    <input class="btn btn-secondary mr-2" type="button" value="Input">
                                    <input class="btn btn-secondary mr-2" type="submit" value="Submit">
                                    <input class="btn btn-secondary" type="reset" value="Reset">
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-secondary<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">role</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>button<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Link<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary<span
                class="token punctuation">"</span></span> <span class="token attr-name">type</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>submit<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary<span
                class="token punctuation">"</span></span> <span class="token attr-name">type</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">value</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Input<span
                class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary<span
                class="token punctuation">"</span></span> <span class="token attr-name">type</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>submit<span
                class="token punctuation">"</span></span> <span class="token attr-name">value</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Submit<span
                class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary<span
                class="token punctuation">"</span></span> <span class="token attr-name">type</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>reset<span
                class="token punctuation">"</span></span> <span class="token attr-name">value</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Reset<span
                class="token punctuation">"</span></span><span
            class="token punctuation">/&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Outline Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Replace the default modifier classes with
                                    <code>.btn-outline-*</code>to remove all background images and colors on any button.
                                </p>
                                <div class="example-preview">
                                    <p>
                                        <button type="button" class="btn btn-outline-primary mr-2">Primary</button>
                                        <button type="button" class="btn btn-outline-secondary mr-2">Secondary</button>
                                        <button type="button" class="btn btn-outline-success mr-2">Success</button>
                                        <button type="button" class="btn btn-outline-danger mr-2">Danger</button>
                                    </p>
                                    <p>
                                        <button type="button" class="btn btn-outline-warning mr-2">Warning</button>
                                        <button type="button" class="btn btn-outline-info mr-2">Info</button>
                                        <button type="button" class="btn btn-outline-dark">Dark</button>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>button</span> <span
                                                                                class="token attr-name">type</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>button<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-outline-primary<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-secondary<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Secondary<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-danger<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-warning<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-info<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Info<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-dark<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Sizes</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Fancy larger or smaller buttons? Add
                                    <code>.btn-lg</code>or
                                    <code>.btn-sm</code>for additional sizes.</p>
                                <div class="example-preview">
                                    <button type="button" class="btn btn-primary btn-sm mr-2">Small button</button>
                                    <button type="button" class="btn btn-primary mr-2">Default button</button>
                                    <button type="button" class="btn btn-primary btn-lg">Large button</button>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>button</span> <span
                                                                                class="token attr-name">type</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>button<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-primary btn-sm<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Small button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Default button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-lg<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Large button<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <p>Add block type buttons that span the full width of a parent by adding
                                    <code>.btn-block</code>.</p>
                                <div class="example-preview">
                                    <button type="button" class="btn btn-primary btn-lg btn-block">Block level button
                                    </button>
                                    <button type="button" class="btn btn-success btn-lg btn-block">Block level button
                                    </button>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>button</span> <span
                                                                                class="token attr-name">type</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>button<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-primary btn-lg btn-block<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Block level button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">type</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>button<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success btn-lg btn-block<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Block level button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">States</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <h5 class="text-dark">Active State</h5>
                                <p>Buttons will appear pressed (with a darker background, darker border, and inset
                                    shadow) when active with
                                    <code>.active</code>.</p>
                                <div class="example-preview">
                                    <p>
                                        <a href="#" class="btn btn-primary mr-2" role="button">Default state</a>
                                        <a href="#" class="btn btn-success mr-2" role="button">Default state</a>
                                        <a href="#" class="btn btn-danger mr-2" role="button">Default state</a>
                                        <a href="#" class="btn btn-warning mr-2" role="button">Default state</a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-primary active mr-2" role="button">Active state</a>
                                        <a href="#" class="btn btn-success active mr-2" role="button">Active state</a>
                                        <a href="#" class="btn btn-danger active mr-2" role="button">Active state</a>
                                        <a href="#" class="btn btn-warning active mr-2" role="button">Active state</a>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-primary active<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Active state<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success active<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Active state<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger active<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Active state<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning active<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Active state<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <h5 class="text-dark">Disabled State</h5>
                                <p>Make buttons look inactive by adding the
                                    <code>disabled</code>boolean attribute to any button element.</p>
                                <div class="example-preview">
                                    <p>
                                        <button class="btn btn-primary mr-2">Default state</button>
                                        <button class="btn btn-success mr-2">Default state</button>
                                        <button class="btn btn-danger mr-2">Default state</button>
                                        <button class="btn btn-warning mr-2">Default state</button>
                                    </p>
                                    <p>
                                        <button class="btn btn-primary mr-2" disabled="disabled">Disabled state</button>
                                        <button class="btn btn-success mr-2" disabled="disabled">Disabled state</button>
                                        <button class="btn btn-danger mr-2" disabled="disabled">Disabled state</button>
                                        <button class="btn btn-warning mr-2" disabled="disabled">Disabled state</button>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>button</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-primary<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">disabled</span><span
                                                                                class="token punctuation">&gt;</span></span>Disabled state<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success<span
                class="token punctuation">"</span></span> <span class="token attr-name">disabled</span><span
            class="token punctuation">&gt;</span></span>Disabled state<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger<span
                class="token punctuation">"</span></span> <span class="token attr-name">disabled</span><span
            class="token punctuation">&gt;</span></span>Disabled state<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning<span
                class="token punctuation">"</span></span> <span class="token attr-name">disabled</span><span
            class="token punctuation">&gt;</span></span>Disabled state<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>button</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Checkbox And Radio Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-10">
                                <p>Bootstrap’s button styles can be applied to other elements, such as labels, to
                                    provide checkbox or radio style button toggling.</p>
                                <div class="example-preview">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-primary active">
                                            <input type="checkbox" checked="checked">Checked</label>
                                    </div>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>div</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn-group-toggle<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">data-toggle</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>buttons<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>label</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-success active<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
                    class="token attr-name">type</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>checkbox<span
                        class="token punctuation">"</span></span> <span class="token attr-name">checked</span><span
                    class="token attr-value"><span class="token punctuation">=</span><span
                        class="token punctuation">"</span>checked<span class="token punctuation">"</span></span><span
                    class="token punctuation">/&gt;</span></span> Checked
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>label</span><span
                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <div class="example-preview">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-success active">
                                            <input type="radio" name="options" id="option1"
                                                   checked="checked">Active</label>
                                        <label class="btn btn-success">
                                            <input type="radio" name="options" id="option2">Radio</label>
                                        <label class="btn btn-success">
                                            <input type="radio" name="options" id="option3">Radio</label>
                                    </div>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>div</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn-group btn-group-toggle<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">data-toggle</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>buttons<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>label</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-success active<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
                    class="token attr-name">type</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>radio<span
                        class="token punctuation">"</span></span> <span class="token attr-name">name</span><span
                    class="token attr-value"><span class="token punctuation">=</span><span
                        class="token punctuation">"</span>options<span class="token punctuation">"</span></span> <span
                    class="token attr-name">id</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>option1<span
                        class="token punctuation">"</span></span> <span class="token attr-name">checked</span><span
                    class="token attr-value"><span class="token punctuation">=</span><span
                        class="token punctuation">"</span>checked<span class="token punctuation">"</span></span><span
                    class="token punctuation">/&gt;</span></span> Active
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>label</span><span
                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>label</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
                    class="token attr-name">type</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>radio<span
                        class="token punctuation">"</span></span> <span class="token attr-name">name</span><span
                    class="token attr-value"><span class="token punctuation">=</span><span
                        class="token punctuation">"</span>options<span class="token punctuation">"</span></span> <span
                    class="token attr-name">id</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>option2<span
                        class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span> Radio
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>label</span><span
                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>label</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span
                    class="token attr-name">type</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>radio<span
                        class="token punctuation">"</span></span> <span class="token attr-name">name</span><span
                    class="token attr-value"><span class="token punctuation">=</span><span
                        class="token punctuation">"</span>options<span class="token punctuation">"</span></span> <span
                    class="token attr-name">id</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>option3<span
                        class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span> Radio
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>label</span><span
                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Button Font Styles</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Button font styles can be set using special helper classes
                                    <code>font-weight-{lighter|light|normal|bold|bolder|boldest}</code>.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-secondary font-weight-lighter mr-2">Lighter</a>
                                    <a href="#" class="btn btn-secondary font-weight-light mr-2">Light</a>
                                    <a href="#" class="btn btn-secondary font-weight-normal mr-2">Normal</a>
                                    <a href="#" class="btn btn-secondary font-weight-bold mr-2">Bold</a>
                                    <a href="#" class="btn btn-secondary font-weight-bolder">Bolder</a>
                                    <a href="#" class="btn btn-secondary font-weight-boldest">Boldest</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-secondary font-weight-lighter mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Lighter<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary font-weight-light mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Light<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary font-weight-normal mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Normal<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Bold<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary font-weight-bolder<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Bolder<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary font-weight-boldest<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Boldest<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <p>Uppercase and lowercase texts can be set using
                                    <code>.text-uppercase</code>and
                                    <code>.text-lowercase</code>classes respectively.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-secondary text-uppercase mr-2">Uppercase</a>
                                    <a href="#" class="btn btn-secondary text-lowercase">Lowercase</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-secondary text-uppercase mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Uppercase<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-secondary text-lowercase<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Lowercase<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Button Styles</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Use
                                    <code>.btn-square</code>classes to have square style buttons.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-success font-weight-bold btn-pill mr-2">Success</a>
                                    <a href="#" class="btn btn-primary font-weight-bold btn-pill mr-2">Primary</a>
                                    <a href="#" class="btn btn-danger font-weight-bold btn-pill mr-2">Danger</a>
                                    <a href="#" class="btn btn-warning font-weight-bold btn-pill mr-2">Warning</a>
                                    <a href="#" class="btn btn-dark font-weight-bold btn-pill">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-success font-weight-bold btn-pill<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary font-weight-bold btn-pill<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger font-weight-bold btn-pill<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning font-weight-bold btn-pill<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-dark font-weight-bold btn-pill<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>.btn-pill</code>classes to have pill style buttons.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-success font-weight-bold btn-square mr-2">Success</a>
                                    <a href="#" class="btn btn-primary font-weight-bold btn-square mr-2">Primary</a>
                                    <a href="#" class="btn btn-danger font-weight-bold btn-square mr-2">Danger</a>
                                    <a href="#" class="btn btn-warning font-weight-bold btn-square mr-2">Warning</a>
                                    <a href="#" class="btn btn-dark font-weight-bold btn-square">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-success font-weight-bold btn-square<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary font-weight-bold btn-square<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger font-weight-bold btn-square<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning font-weight-bold btn-square<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-dark font-weight-bold btn-square<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Custom Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Metronic extends Bootstrap with custom button styles.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-default font-weight-bold mr-2">Default Button</a>
                                    <a href="#" class="btn btn-light font-weight-bold mr-2">Light Button</a>
                                    <a href="#" class="btn btn-clean font-weight-bold">Clean Button</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-default font-weight-bold mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Default Button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Light Button<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-clean font-weight-bold<span
                class="token punctuation">"</span></span><span
            class="token punctuation">&gt;</span></span>Clean Button<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Customed Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Metronic Bootstrap standard unitlity classes to change button size and spacing.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-default font-weight-bold font-size-h5 px-8 py-3 mr-2">Button</a>
                                    <a href="#" class="btn btn-danger font-weight-bold font-size-h6 px-10 py-4 mr-2">Button</a>
                                    <a href="#"
                                       class="btn btn-primary font-weight-bold font-size-h3 px-12 py-5">Button</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-default font-weight-bold font-size-h5 px-8 py-3 mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger font-weight-bold font-size-h6 px-10 py-4 mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary font-weight-bold font-size-h3 px-12 py-5<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Button<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Mixed Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Mix buttons with Metronic elements</p>
                                <div class="example-preview">
                                    <div class="btn btn-icon w-auto btn-clean d-inline-flex align-items-center btn-lg px-2 mr-5">
                                        <span class="text-muted font-weight-bold font-size-base mr-1">Hi,</span>
                                        <span class="text-dark-50 font-weight-bolder font-size-base mr-3">Sean</span>
                                        <span class="symbol symbol-35 symbol-light-warning">
																<span class="symbol-label font-size-h5 font-weight-bold">S</span>
															</span>
                                    </div>
                                    <div class="btn btn-icon btn-light w-auto btn-clean d-inline-flex align-items-center btn-lg px-2 mr-5">
                                        <span class="text-muted font-weight-bold font-size-base mr-1">Hi,</span>
                                        <span class="text-dark-50 font-weight-bolder font-size-base mr-3">Sean</span>
                                        <span class="symbol symbol-35 symbol-light-primary">
																<span class="symbol-label font-size-h5 font-weight-bold">S</span>
															</span>
                                    </div>
                                    <div class="btn btn-light-success d-inline-flex align-items-center btn-lg mr-5">
                                        <div class="d-flex flex-column text-right pr-3">
                                            <span class="text-dark-75 font-weight-bold font-size-sm">Sean</span>
                                            <span class="font-weight-bolder font-size-sm">UX Designer</span>
                                        </div>
                                        <span class="symbol symbol-40">
																<img alt="Pic" src="assets/media/users/300_25.jpg">
															</span>
                                    </div>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>div</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-icon w-auto btn-clean d-inline-flex align-items-center btn-lg px-2 mr-5<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>text-muted font-weight-bold font-size-base mr-1<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Hi,<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>text-dark-50 font-weight-bolder font-size-base mr-3<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Sean<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>symbol symbol-35 symbol-light-warning<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                    class="token attr-name">class</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>symbol-label font-size-h5 font-weight-bold<span
                        class="token punctuation">"</span></span><span
                    class="token punctuation">&gt;</span></span>S<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span
                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span>

<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-light w-auto btn-clean d-inline-flex align-items-center btn-lg px-2 mr-5<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>text-muted font-weight-bold font-size-base mr-1<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Hi,<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>text-dark-50 font-weight-bolder font-size-base mr-3<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Sean<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>symbol symbol-35 symbol-light-primary<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                    class="token attr-name">class</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>symbol-label font-size-h5 font-weight-bold<span
                        class="token punctuation">"</span></span><span
                    class="token punctuation">&gt;</span></span>S<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span
                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span>

<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span
            class="token attr-name">class</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light-success d-inline-flex align-items-center btn-lg mr-5<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>d-flex flex-column text-right pr-3<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                    class="token attr-name">class</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>text-dark-75 font-weight-bold font-size-sm<span
                        class="token punctuation">"</span></span><span
                    class="token punctuation">&gt;</span></span>Sean<span class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                    class="token attr-name">class</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>font-weight-bolder font-size-sm<span
                        class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>UX Designer<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
 <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
             class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>symbol symbol-40<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>img</span> <span
                    class="token attr-name">alt</span><span class="token attr-value"><span
                        class="token punctuation">=</span><span class="token punctuation">"</span>Pic<span
                        class="token punctuation">"</span></span> <span class="token attr-name">src</span><span
                    class="token attr-value"><span class="token punctuation">=</span><span
                        class="token punctuation">"</span>media/images/users/300_25.jpg<span
                        class="token punctuation">"</span></span><span class="token punctuation">/&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span
                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
                <div class="col-xl-6">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Light Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-light-{color}</code>class to have a custom button style with a lighter
                                    background color and dark font color.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-light-success font-weight-bold mr-2">Success</a>
                                    <a href="#" class="btn btn-light-primary font-weight-bold mr-2">Primary</a>
                                    <a href="#" class="btn btn-light-danger font-weight-bold mr-2">Danger</a>
                                    <a href="#" class="btn btn-light-warning font-weight-bold mr-2">Warning</a>
                                    <a href="#" class="btn btn-light-dark font-weight-bold mr-2">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-light-success font-weight-bold mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light-primary font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light-danger font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light-warning font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light-dark font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Light Hover Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-light-{color}</code>class to have a custom button style with a lighter
                                    background color and dark font color.</p>
                                <div class="example-preview">
                                    <a href="#"
                                       class="btn btn-text-success btn-hover-light-success font-weight-bold mr-2">Success</a>
                                    <a href="#"
                                       class="btn btn-text-primary btn-hover-light-primary font-weight-bold mr-2">Primary</a>
                                    <a href="#"
                                       class="btn btn-text-danger btn-hover-light-danger font-weight-bold mr-2">Danger</a>
                                    <a href="#"
                                       class="btn btn-text-warning btn-hover-light-warning font-weight-bold mr-2">Warning</a>
                                    <a href="#" class="btn btn-text-dark btn-hover-light-dark font-weight-bold mr-2">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-text-success btn-hover-light-success font-weight-bold mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-text-primary btn-hover-light-primary font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-text-danger btn-hover-light-danger font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-text-warning btn-hover-light-warning font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-text-dark btn-hover-light-dark font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Transparent Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-transparent-{color}</code>class to have a custom button style with a
                                    transparent background color.</p>
                                <div class="example-preview">
                                    <div class="d-flex align-items-center p-4 bg-dark">
                                        <a href="#"
                                           class="btn btn-transparent-success font-weight-bold mr-2">Success</a>
                                        <a href="#"
                                           class="btn btn-transparent-primary font-weight-bold mr-2">Primary</a>
                                        <a href="#" class="btn btn-transparent-danger font-weight-bold mr-2">Danger</a>
                                        <a href="#"
                                           class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
                                        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
                                    </div>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>div</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>d-flex align-items-center p-4 bg-dark<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-transparent-success font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Success<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-transparent-primary font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Primary<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-transparent-danger font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Danger<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-transparent-warning font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Warning<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-transparent-white font-weight-bold<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>White<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Transparent Hover Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-hover-transparent-{color}</code>class to have a custom button style with a
                                    transparent hover background color.</p>
                                <div class="example-preview">
                                    <div class="d-flex align-items-center p-4 bg-dark">
                                        <a href="#" class="btn btn-hover-transparent-success font-weight-bold mr-2">Success</a>
                                        <a href="#" class="btn btn-hover-transparent-primary font-weight-bold mr-2">Primary</a>
                                        <a href="#" class="btn btn-hover-transparent-danger font-weight-bold mr-2">Danger</a>
                                        <a href="#" class="btn btn-hover-transparent-warning font-weight-bold mr-2">Warning</a>
                                        <a href="#" class="btn btn-hover-transparent-white font-weight-bold">White</a>
                                    </div>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>div</span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>d-flex align-items-center p-4 bg-dark<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-hover-transparent-success font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Success<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-hover-transparent-primary font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Primary<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-hover-transparent-danger font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Danger<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-hover-transparent-warning font-weight-bold mr-2<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>Warning<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
                class="token attr-name">href</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                    class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
                class="token attr-value"><span class="token punctuation">=</span><span
                    class="token punctuation">"</span>btn btn-hover-transparent-white font-weight-bold<span
                    class="token punctuation">"</span></span><span
                class="token punctuation">&gt;</span></span>White<span class="token tag"><span class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Link Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-link-{color}</code>class to have a custom link button.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-link-success font-weight-bold mr-2">Success</a>
                                    <a href="#" class="btn btn-link-primary font-weight-bold mr-2">Primary</a>
                                    <a href="#" class="btn btn-link-danger font-weight-bold mr-2">Danger</a>
                                    <a href="#" class="btn btn-link-warning font-weight-bold mr-2">Warning</a>
                                    <a href="#" class="btn btn-link-dark font-weight-bold mr-2">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-link-success font-weight-bold<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-link-primary font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-link-danger font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-link-warning font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-link-dark font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Button Text Colors</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-text-{color}</code>class to set custom color for button texts.</p>
                                <div class="example-preview">
                                    <a href="#"
                                       class="btn btn-light btn-text-success btn-hover-text-success font-weight-bold mr-2">Success</a>
                                    <a href="#"
                                       class="btn btn-light btn-text-primary btn-hover-text-primary font-weight-bold mr-2">Primary</a>
                                    <a href="#"
                                       class="btn btn-light btn-text-danger btn-hover-text-danger font-weight-bold mr-2">Danger</a>
                                    <a href="#"
                                       class="btn btn-light btn-text-warning btn-hover-text-warning font-weight-bold mr-2">Warning</a>
                                    <a href="#"
                                       class="btn btn-light btn-text-dark btn-hover-text-dark font-weight-bold mr-2">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-light btn-text-success btn-hover-text-success font-weight-bold<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-text-primary btn-hover-text-primary font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-text-danger btn-hover-text-danger font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-text-warning btn-hover-text-warning font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-text-dark btn-hover-text-dark font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Hover Button Option</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-hover-{color}</code>class to have a custom hover color for your buttons.
                                </p>
                                <div class="example-preview">
                                    <a href="#"
                                       class="btn btn-light btn-hover-success font-weight-bold mr-2">Success</a>
                                    <a href="#"
                                       class="btn btn-light btn-hover-primary font-weight-bold mr-2">Primary</a>
                                    <a href="#" class="btn btn-light btn-hover-danger font-weight-bold mr-2">Danger</a>
                                    <a href="#"
                                       class="btn btn-light btn-hover-warning font-weight-bold mr-2">Warning</a>
                                    <a href="#" class="btn btn-light btn-hover-dark font-weight-bold mr-2">Dark</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-light btn-hover-success font-weight-bold mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-hover-primary font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-hover-danger font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-hover-warning font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light btn-hover-dark font-weight-bold<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Dark<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Buttons With Shadow</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Use
                                    <code>btn-shadow</code>class to set
                                    <code>box-shadow</code>to your buttons.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-light btn-shadow font-weight-bold mr-2">Light</a>
                                    <a href="#" class="btn btn-success btn-shadow font-weight-bold mr-2">Success</a>
                                    <a href="#" class="btn btn-primary btn-shadow font-weight-bold mr-2">Primary</a>
                                    <a href="#" class="btn btn-danger btn-shadow font-weight-bold mr-2">Danger</a>
                                    <a href="#" class="btn btn-warning btn-shadow font-weight-bold mr-2">Warning</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-light btn-shadow font-weight-bold mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Light<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success btn-shadow font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-shadow font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger btn-shadow font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning btn-shadow font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-shadow-hover</code>class to set
                                    <code>box-shadow</code>effect on hover to your buttons.</p>
                                <div class="example-preview">
                                    <a href="#" class="btn btn-light btn-shadow-hover font-weight-bold mr-2">Light</a>
                                    <a href="#"
                                       class="btn btn-success btn-shadow-hover font-weight-bold mr-2">Success</a>
                                    <a href="#"
                                       class="btn btn-primary btn-shadow-hover font-weight-bold mr-2">Primary</a>
                                    <a href="#" class="btn btn-danger btn-shadow-hover font-weight-bold mr-2">Danger</a>
                                    <a href="#"
                                       class="btn btn-warning btn-shadow-hover font-weight-bold mr-2">Warning</a>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-light btn-shadow-hover font-weight-bold mr-2<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>Light<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success btn-shadow-hover font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Success<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-shadow-hover font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Primary<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger btn-shadow-hover font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Danger<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-warning btn-shadow-hover font-weight-bold mr-2<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Warning<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>a</span><span
                                                                                class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Buttons With Icons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Use buttons with a wide range of Metronic's awesome
                                    <a href="" target="_blank">Icon Collection</a>.</p>
                                <div class="example-preview">
                                    <p>
                                        <a href="#" class="btn btn-success btn-sm mr-3">
                                            <i class="flaticon2-pie-chart"></i>Success</a>
                                        <a href="#" class="btn btn-primary mr-3">
                                            <i class="flaticon2-box-1"></i>Primary</a>
                                        <a href="#" class="btn btn-danger btn-lg">
                                            <i class="flaticon2-sms"></i>Danger</a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-outline-success btn-sm mr-3">
                                            <i class="flaticon-multisymbol-2"></i>Success</a>
                                        <a href="#" class="btn btn-outline-primary mr-3">
                                            <i class="flaticon-file"></i>Primary</a>
                                        <a href="#" class="btn btn-outline-danger btn-lg">
                                            <i class="flaticon-questions-circular-button"></i>Danger</a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-light-success btn-sm mr-3">
                                            <i class="flaticon2-chat-1"></i>Success</a>
                                        <a href="#" class="btn btn-light-primary mr-3">
                                            <i class="flaticon2-cube"></i>Primary</a>
                                        <a href="#" class="btn btn-light-danger btn-lg">
                                            <i class="flaticon2-graph-1"></i>Danger</a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-success btn-sm mr-3">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Call.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"></rect>
																		<path d="M12,22 C6.4771525,22 2,17.5228475 2,12 C2,6.4771525 6.4771525,2 12,2 C17.5228475,2 22,6.4771525 22,12 C22,17.5228475 17.5228475,22 12,22 Z M11.613922,13.2130341 C11.1688026,13.6581534 10.4887934,13.7685037 9.92575695,13.4869855 C9.36272054,13.2054673 8.68271128,13.3158176 8.23759191,13.760937 L6.72658218,15.2719467 C6.67169475,15.3268342 6.63034033,15.393747 6.60579393,15.4673862 C6.51847004,15.7293579 6.66005003,16.0125179 6.92202169,16.0998418 L8.27584113,16.5511149 C9.57592638,16.9844767 11.009274,16.6461092 11.9783003,15.6770829 L15.9775173,11.6778659 C16.867756,10.7876271 17.0884566,9.42760861 16.5254202,8.3015358 L15.8928491,7.03639343 C15.8688153,6.98832598 15.8371895,6.9444475 15.7991889,6.90644684 C15.6039267,6.71118469 15.2873442,6.71118469 15.0920821,6.90644684 L13.4995401,8.49898884 C13.0544207,8.94410821 12.9440704,9.62411747 13.2255886,10.1871539 C13.5071068,10.7501903 13.3967565,11.4301996 12.9516371,11.8753189 L11.613922,13.2130341 Z"
                                                                              fill="#000000"></path>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>Success</a>
                                        <a href="#" class="btn btn-primary mr-3">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"></rect>
																		<path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z"
                                                                              fill="#000000" opacity="0.3"></path>
																		<path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z"
                                                                              fill="#000000"></path>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>Primary</a>
                                        <a href="#" class="btn btn-danger btn-lg">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"></rect>
																		<circle fill="#000000" cx="9" cy="15"
                                                                                r="6"></circle>
																		<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                                                              fill="#000000" opacity="0.3"></path>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>Danger</a>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-success<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>flaticon2-pie-chart<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span> Success
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-outline-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span
                    class="token punctuation">"</span>flaticon-multisymbol-2<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span> Success
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-light-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>flaticon2-chat-1<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span> Success
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>svg-icon<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>svg</span><span
                                                                                class="token punctuation">&gt;</span></span>...<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>svg</span><span
                                                                                class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span> Success
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-icon</code>class to have icon only square buttons.</p>
                                <div class="example-preview">
                                    <p>
                                        <a href="#" class="btn btn-icon btn-success btn-sm mr-2">
                                            <i class="flaticon2-pie-chart"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-primary mr-2">
                                            <i class="flaticon2-box-1"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-danger btn-lg mr-4">
                                            <i class="flaticon2-sms"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-outline-success btn-sm mr-2">
                                            <i class="flaticon-multisymbol-2"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-outline-primary mr-2">
                                            <i class="flaticon-file"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-outline-warning btn-lg mr-4">
                                            <i class="flaticon-browser"></i>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-icon btn-light-success btn-sm mr-2">
                                            <i class="flaticon2-chat-1"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-primary mr-2">
                                            <i class="flaticon2-cube"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-warning btn-lg mr-4">
                                            <i class="flaticon2-rocket-1"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-success btn-sm mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Call.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
																			<path d="M12,22 C6.4771525,22 2,17.5228475 2,12 C2,6.4771525 6.4771525,2 12,2 C17.5228475,2 22,6.4771525 22,12 C22,17.5228475 17.5228475,22 12,22 Z M11.613922,13.2130341 C11.1688026,13.6581534 10.4887934,13.7685037 9.92575695,13.4869855 C9.36272054,13.2054673 8.68271128,13.3158176 8.23759191,13.760937 L6.72658218,15.2719467 C6.67169475,15.3268342 6.63034033,15.393747 6.60579393,15.4673862 C6.51847004,15.7293579 6.66005003,16.0125179 6.92202169,16.0998418 L8.27584113,16.5511149 C9.57592638,16.9844767 11.009274,16.6461092 11.9783003,15.6770829 L15.9775173,11.6778659 C16.867756,10.7876271 17.0884566,9.42760861 16.5254202,8.3015358 L15.8928491,7.03639343 C15.8688153,6.98832598 15.8371895,6.9444475 15.7991889,6.90644684 C15.6039267,6.71118469 15.2873442,6.71118469 15.0920821,6.90644684 L13.4995401,8.49898884 C13.0544207,8.94410821 12.9440704,9.62411747 13.2255886,10.1871539 C13.5071068,10.7501903 13.3967565,11.4301996 12.9516371,11.8753189 L11.613922,13.2130341 Z"
                                                                                  fill="#000000"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-danger mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
																			<circle fill="#000000" cx="9" cy="15"
                                                                                    r="6"></circle>
																			<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                                                                  fill="#000000" opacity="0.3"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-warning btn-lg">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
																			<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
                                                                                  fill="#000000" opacity="0.3"></path>
																			<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
                                                                                  fill="#000000"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-icon btn-success btn-circl btn-sme mr-2">
                                            <i class="flaticon2-pie-chart"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-primary btn-circle mr-2">
                                            <i class="flaticon2-box-1"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-danger btn-circle btn-lg mr-4">
                                            <i class="flaticon2-sms"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-outline-success btn-circle btn-sm mr-2">
                                            <i class="flaticon-multisymbol-2"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-outline-primary btn-circle mr-2">
                                            <i class="flaticon-file"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-outline-warning btn-circle btn-lg mr-4">
                                            <i class="flaticon-browser"></i>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-icon btn-light-success btn-circle btn-sm mr-2">
                                            <i class="flaticon2-chat-1"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-primary btn-circle mr-2">
                                            <i class="flaticon2-cube"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-warning btn-circle btn-lg mr-4">
                                            <i class="flaticon2-rocket-1"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-success btn-circle btn-sm mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Call.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
																			<path d="M12,22 C6.4771525,22 2,17.5228475 2,12 C2,6.4771525 6.4771525,2 12,2 C17.5228475,2 22,6.4771525 22,12 C22,17.5228475 17.5228475,22 12,22 Z M11.613922,13.2130341 C11.1688026,13.6581534 10.4887934,13.7685037 9.92575695,13.4869855 C9.36272054,13.2054673 8.68271128,13.3158176 8.23759191,13.760937 L6.72658218,15.2719467 C6.67169475,15.3268342 6.63034033,15.393747 6.60579393,15.4673862 C6.51847004,15.7293579 6.66005003,16.0125179 6.92202169,16.0998418 L8.27584113,16.5511149 C9.57592638,16.9844767 11.009274,16.6461092 11.9783003,15.6770829 L15.9775173,11.6778659 C16.867756,10.7876271 17.0884566,9.42760861 16.5254202,8.3015358 L15.8928491,7.03639343 C15.8688153,6.98832598 15.8371895,6.9444475 15.7991889,6.90644684 C15.6039267,6.71118469 15.2873442,6.71118469 15.0920821,6.90644684 L13.4995401,8.49898884 C13.0544207,8.94410821 12.9440704,9.62411747 13.2255886,10.1871539 C13.5071068,10.7501903 13.3967565,11.4301996 12.9516371,11.8753189 L11.613922,13.2130341 Z"
                                                                                  fill="#000000"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-danger btn-circle mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
																			<circle fill="#000000" cx="9" cy="15"
                                                                                    r="6"></circle>
																			<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                                                                  fill="#000000" opacity="0.3"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-warning btn-circle btn-lg">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24"
                                                                                  height="24"></rect>
																			<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
                                                                                  fill="#000000" opacity="0.3"></path>
																			<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
                                                                                  fill="#000000"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                        </a>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-icon btn-success<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>flaticon2-pie-chart<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-outline-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span
                    class="token punctuation">"</span>flaticon-multisymbol-2<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-light-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>flaticon2-chat-1<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-success<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>svg-icon<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>svg</span><span
                                                                                class="token punctuation">&gt;</span></span>...<span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>svg</span><span
                                                                                class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>span</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Social Buttons</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Use buttons with social icons listed in our
                                    <a href="" target="_blank">Social Icons Collection</a>.</p>
                                <div class="example-preview">
                                    <p>
                                        <a href="#" class="btn btn-icon btn-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-twitter mr-2">
                                            <i class="socicon-twitter"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-instagram mr-2">
                                            <i class="socicon-instagram"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-youtube mr-2">
                                            <i class="socicon-youtube"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-linkedin mr-2">
                                            <i class="socicon-linkedin"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-skype">
                                            <i class="socicon-skype"></i>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-icon btn-circle btn-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-twitter mr-2">
                                            <i class="socicon-twitter"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-instagram mr-2">
                                            <i class="socicon-instagram"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-youtube mr-2">
                                            <i class="socicon-youtube"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-linkedin mr-2">
                                            <i class="socicon-linkedin"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-skype mr-2">
                                            <i class="socicon-skype"></i>
                                        </a>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-icon btn-facebook<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-circle btn-facebook<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example mb-15">
                                <p>Use
                                    <code>btn-light-{social}</code>class to have light social buttons.</p>
                                <div class="example-preview">
                                    <p>
                                        <a href="#" class="btn btn-icon btn-light-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-twitter mr-2">
                                            <i class="socicon-twitter"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-instagram mr-2">
                                            <i class="socicon-instagram"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-youtube mr-2">
                                            <i class="socicon-youtube"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-linkedin mr-2">
                                            <i class="socicon-linkedin"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-light-skype">
                                            <i class="socicon-skype"></i>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-twitter mr-2">
                                            <i class="socicon-twitter"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-instagram mr-2">
                                            <i class="socicon-instagram"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-youtube mr-2">
                                            <i class="socicon-youtube"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-linkedin mr-2">
                                            <i class="socicon-linkedin"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-skype">
                                            <i class="socicon-skype"></i>
                                        </a>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-icon btn-facebook<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-circle btn-facebook<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                            <!--begin::Example-->
                            <div class="example">
                                <p>Use
                                    <code>btn-xs</code>,
                                    <code>btn-sm</code>,
                                    <code>btn-md</code>and
                                    <code>btn-lg</code>size classes to set required button sizes.</p>
                                <div class="example-preview">
                                    <p>
                                        <a href="#" class="btn btn-icon btn-xs btn-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-sm btn-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-lg btn-facebook">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" class="btn btn-icon btn-circle btn-xs btn-light-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-sm btn-light-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-light-facebook mr-2">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-circle btn-lg btn-light-facebook">
                                            <i class="socicon-facebook"></i>
                                        </a>
                                    </p>
                                </div>
                                <div class="example-code">
                                    <span class="example-copy" data-toggle="tooltip" title=""
                                          data-original-title="Copy code"></span>
                                    <div class="example-highlight">
															<pre class=" language-html"><code
                                                                        class=" language-html"><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;</span>a</span> <span
                                                                                class="token attr-name">href</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>#<span
                                                                                    class="token punctuation">"</span></span> <span
                                                                                class="token attr-name">class</span><span
                                                                                class="token attr-value"><span
                                                                                    class="token punctuation">=</span><span
                                                                                    class="token punctuation">"</span>btn btn-icon btn-xs btn-facebook<span
                                                                                    class="token punctuation">"</span></span><span
                                                                                class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-sm btn-facebook<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-facebook<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span
            class="token attr-name">href</span><span class="token attr-value"><span
                class="token punctuation">=</span><span class="token punctuation">"</span>#<span
                class="token punctuation">"</span></span> <span class="token attr-name">class</span><span
            class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-icon btn-lg btn-facebook<span
                class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span
                class="token attr-name">class</span><span class="token attr-value"><span
                    class="token punctuation">=</span><span class="token punctuation">"</span>socicon-facebook<span
                    class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span
                                                                            class="token tag"><span
                                                                                class="token tag"><span
                                                                                    class="token punctuation">&lt;/</span>i</span><span
                                                                                class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span
            class="token punctuation">&gt;</span></span></code></pre>
                                    </div>
                                </div>
                            </div>
                            <!--end::Example-->
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
@endsection
