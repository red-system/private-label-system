<div class="modal fade" id="modal-file" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form id="file" action="{{ route('projectListInsertFile') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('projectListPage') }}"
          data-alert-show="true"
          data-alert-field-message="true">

        {{ csrf_field() }}
        <div class="modal-dialog modal-xxlg" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <h3 class="modal-title m--font-light" id="exampleModalLabel"><strong>Project File</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height: 500px; overflow-y: auto;">

                </div>
                <div class=" modal-footer">
                    <button class="btn btn-success btn-simpan"><i class="la la-check"></i>Save</button>
                    <button type="button" class="btn btn-secondary btn-batal" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>

