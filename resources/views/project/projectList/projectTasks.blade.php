<div class="modal fade" id="modal-tasks" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form id="tasks" action="{{ route('projectListInsertTasks') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('projectListPage') }}"
          data-alert-show="true"
          data-alert-field-message="true">

        {{ csrf_field() }}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title m--font-light" id="exampleModalLabel"><strong>Project Tasks</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body" style="height: 500px; overflow-y: auto;">

                </div>
                <div class=" modal-footer">
                    <button class="btn btn-success btn-simpan"><i class="la la-check"></i>Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>

