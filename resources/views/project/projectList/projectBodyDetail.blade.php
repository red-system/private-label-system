@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/tasks.js') }}" type="text/javascript"></script>
@endsection
@section('body')
    {{csrf_field()}}
    <div class="container">
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>

        </div>

        <div class="d-flex flex-column-fluid">
            <div class="container card card-custom">

                <div class="m-content">
                    <div class="card-body pt-0 pb-3">
                        <div class="m-portlet__body row">
                            <div class="col-lg-6">
                                <div>
                                    <i class="icon-2x text-dark-50 flaticon2-menu-4"></i>
                                    <span class="col-3 col-form-label" style="font-size: 20px; font-weight: bold">Client Information</span>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Order ID</label>
                                    <div class="col-9 col-form-label no_so">{{$project->project_order_number}}</div>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Order Name</label>
                                    <div class="col-9 col-form-label no_so">{{$project->project_order_title}}</div>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Channel</label>
                                    <div class="col-9 col-form-label no_so">{{$project->channel->channel_name}}</div>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Services</label>
                                    <div class="col-9 col-form-label no_so">{{$project->services->services_name}}</div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    <i class="icon-2x text-dark-50 flaticon2-list-3"></i>
                                    <span class="col-3 col-form-label" style="font-size: 20px; font-weight: bold">Client Schedule</span>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">Order Date</label>
                                    <div class="col-8 col-form-label no_so">{{Main::format_date($project->project_order_date)}}</div>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">Current Status</label>
                                    <div class="col-8 col-form-label no_so">{{$project->project_status->project_status_name}}</div>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">Start Date</label>
                                    <div class="col-8 col-form-label no_so"></div>
                                </div>
                                <div class="m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">End Date</label>
                                    <div class="col-8 col-form-label no_so"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--                    <hr>--}}
                    {{--                    <br>--}}
                    {{--                    <div class="card-body pt-0 pb-3">--}}
                    {{--                        <div class="m-portlet__body row">--}}
                    {{--                            <div class="col-lg-12">--}}
                    {{--                                <div>--}}
                    {{--                                    <i class="icon-2x text-dark-50 flaticon2-menu-4"></i>--}}
                    {{--                                    <span class="col-3 col-form-label" style="font-size: 20px; font-weight: bold">Project Information</span>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Order ID</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so">{{$project->project_order_number}}</div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Order Name</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so">{{$project->project_order_title}}</div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Channel</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so">{{$project->channel->channel_name}}</div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Services</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so">{{$project->services->services_name}}</div>--}}
                    {{--                                </div>--}}
                    {{--                                --}}{{--                                <div class="m-form__group row">--}}
                    {{--                                --}}{{--                                    <label for="example-text-input" class="col-3 col-form-label">Website Name</label>--}}
                    {{--                                --}}{{--                                    <div class="col-9 col-form-label no_so">{{$project->client->client_name}}</div>--}}
                    {{--                                --}}{{--                                </div>--}}
                    {{--                                --}}{{--                                <div class="m-form__group row">--}}
                    {{--                                --}}{{--                                    <label for="example-text-input" class="col-3 col-form-label">Order Title</label>--}}
                    {{--                                --}}{{--                                    <div class="col-9 col-form-label no_so">{{$project->project_order_title}}</div>--}}
                    {{--                                --}}{{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <hr>--}}
                    {{--                    <br>--}}
                    {{--                    <div class="card-body pt-0 pb-3">--}}
                    {{--                        <div class="m-portlet__body row">--}}
                    {{--                            <div class="col-lg-6">--}}
                    {{--                                <div>--}}
                    {{--                                    <i class="icon-2x text-dark-50 flaticon2-list-3"></i>--}}
                    {{--                                    <span class="col-3 col-form-label" style="font-size: 20px; font-weight: bold">Project Schedule</span>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Order Date</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so">{{Main::format_date($project->project_order_date)}}</div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Current Status</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so">{{$project->project_status->project_status_name}}</div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Start Date</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">End Date</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="col-lg-6">--}}
                    {{--                                <div>--}}
                    {{--                                    <i class="icon-2x text-dark-50 flaticon-calendar-with-a-clock-time-tools"></i>--}}
                    {{--                                    <span class="col-3 col-form-label" style="font-size: 20px; font-weight: bold">Order Tracker</span>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Service Code</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Transaction ID</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Payment Status</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Quantity</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <hr>--}}
                    {{--                    <br>--}}
                    {{--                    <div class="card-body pt-0 pb-3">--}}
                    {{--                        <div class="m-portlet__body row">--}}
                    {{--                            <div class="col-lg-12">--}}
                    {{--                                <div>--}}
                    {{--                                    <i class="icon-2x text-dark-50 flaticon2-information"></i>--}}
                    {{--                                    <span class="col-3 col-form-label" style="font-size: 20px; font-weight: bold">Work Information</span>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Working URL</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Profile</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="m-form__group row">--}}
                    {{--                                    <label for="example-text-input" class="col-3 col-form-label">Assigned To</label>--}}
                    {{--                                    <div class="col-9 col-form-label no_so"></div>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <hr>
                    <br>
                    <br>
                    <div class="card-body pt-0 pb-3">
                        <div class="card card-custom card-shadowless gutter-b card-stretch card-shadowless p-0">
                            <!--begin::Nav Tabs-->
                            <ul class="dashboard-tabs nav nav-pills nav-danger row row-paddingless m-0 p-0"
                                role="tablist">
                                <!--begin::Item-->
                                <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                                    <a onclick="myFunction()"
                                       {{--                                       href="{{route('projectResult', ['id' => $project->project_id])}}"--}}
                                       class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center">
														<span class="nav-icon py-2 w-auto">
															<span class="svg-icon svg-icon-3x">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Home/Library.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                                                              fill="#000000" opacity="0.3"/>
                                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                                                              fill="#000000"/>
                                                                        <rect fill="#000000" opacity="0.3" x="7" y="10"
                                                                              width="5" height="2" rx="1"/>
                                                                        <rect fill="#000000" opacity="0.3" x="7" y="14"
                                                                              width="9" height="2" rx="1"/>
                                                                    </g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                                        <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Result Page</span>
                                    </a>
                                </li>
                                <!--end::Item-->
                                <!--begin::Item-->
                                <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3">
                                    <a onclick="myFunction()"
                                       class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center"
                                       href="#tab_forms_widget_5">
														<span class="nav-icon py-2 w-auto">
															<span class="svg-icon svg-icon-3x">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Shield-check.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                        <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                                              fill="#000000"/>
                                                                    </g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                                        <span class="nav-text font-size-lg py-2 font-weight-bolder text-center">Settings</span>
                                    </a>
                                </li>
                                <!--end::Item-->
                                <!--begin::Item-->
                                <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-0 mb-3 mb-lg-0">
                                    <a onclick="myFunction()"
                                       class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center"
                                       href="#tab_forms_widget_5">
														<span class="nav-icon py-2 w-auto">
															<span class="svg-icon svg-icon-3x">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                        <path d="M16,17 L16,21 C16,21.5522847 15.5522847,22 15,22 L9,22 C8.44771525,22 8,21.5522847 8,21 L8,17 L5,17 C3.8954305,17 3,16.1045695 3,15 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,15 C21,16.1045695 20.1045695,17 19,17 L16,17 Z M17.5,11 C18.3284271,11 19,10.3284271 19,9.5 C19,8.67157288 18.3284271,8 17.5,8 C16.6715729,8 16,8.67157288 16,9.5 C16,10.3284271 16.6715729,11 17.5,11 Z M10,14 L10,20 L14,20 L14,14 L10,14 Z"
                                                                              fill="#000000"/>
                                                                        <rect fill="#000000" opacity="0.3" x="8" y="2"
                                                                              width="8" height="2" rx="1"/>
                                                                    </g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                                        <span class="nav-text font-size-lg py-2 font-weight-bolder text-center">Reports</span>
                                    </a>
                                </li>
                                <!--end::Item-->
                            </ul>
                            <!--end::Nav Tabs-->
                            <!--begin::Nav Content-->
                            <div class="tab-content m-0 p-0">
                                <div class="tab-pane active" id="forms_widget_tab_1" role="tabpanel"></div>
                                <div class="tab-pane" id="forms_widget_tab_2" role="tabpanel"></div>
                                <div class="tab-pane" id="forms_widget_tab_3" role="tabpanel"></div>
                                <div class="tab-pane" id="forms_widget_tab_4" role="tabpanel"></div>
                                <div class="tab-pane" id="forms_widget_tab_6" role="tabpanel"></div>
                            </div>
                            <!--end::Nav Content-->
                        </div>
                    </div>
                    <hr>
                    <br>
                    <div class="card-body pt-0 pb-3">
                        <form
                                id="sub-project"
                                action="{{ route('projectInsertSub') }}"
                                method="post"
                                class="m-form form-send"
                                autocomplete="off"
                                data-alert-show="true"
                                data-alert-field-message="true">
                            {{ csrf_field() }}
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Sub Projects</span>
                            </h3>
                            <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-checkable datatable-no-pagination"
                                       width="100%">
                                    <thead>
                                    <tr style="background-color: #FF5454; color: #FFFFFF">
                                        <th>Service Title</th>
                                        <th>Qty</th>
                                        <th>Status</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Assign To</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($sub_project) == 0)
                                        <tr>
                                            <td colspan="6" style="text-align: center">There is no data</td>
                                        </tr>
                                    @else
                                        @if($from[0] == 'agent_id')
                                            @foreach($sub_project as $sp)
                                                <tr>
                                                    <td>{{$sp->services_title}}</td>
                                                    <td>{{$sp->qty}}</td>
                                                    <td><input type="hidden" name="sub_project_id[]"
                                                               value="{{$sp->sub_project_id}}">
                                                        <select class="form-control m-select2"
                                                                name="status[]"
                                                                style="width: 120px">
                                                            <option value="In Progress" {{$sp->status == 'In Progress' ? 'selected':''}}>
                                                                In Progress
                                                            </option>
                                                            <option value="Completed" {{$sp->status == 'Completed' ? 'selected':''}}>
                                                                Completed
                                                            </option>
                                                        </select></td>
                                                    <td>{{Main::format_date($sp->start_date)}}</td>
                                                    <td>{{Main::format_date($sp->end_date)}}</td>
                                                    <td>
                                                        <select class="form-control m-select2"
                                                                name="assign_to[]"
                                                                style="width: 120px">
                                                            <option value="" {{$sp->assign_to == '' || $sp->assign_to == 0 ? 'selected':''}}>
                                                                Select Agent
                                                            </option>
                                                            @foreach($agent as $ag)
                                                                <option value="{{$ag->agent_id}}" {{$sp->assign_to == $ag->agent_id ? 'selected':''}}>
                                                                    {{$ag->agent_name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach($sub_project as $sp)
                                                <tr>
                                                    <td>{{$sp->services_title}}</td>
                                                    <td>{{$sp->qty}}</td>
                                                    <td>{{$sp->status}}</td>
                                                    <td>{{Main::format_date($sp->start_date)}}</td>
                                                    <td>{{Main::format_date($sp->end_date)}}</td>
                                                    <td>{{$sp->assign_to_label}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #FF5454; color: #FFFFFF">
                                        <th colspan="6">Campaign Monitoring</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($campaign_monitoring) == 0)
                                        <tr>
                                            <td colspan="6" style="text-align: center">There is no data</td>
                                        </tr>
                                    @else
                                        @if($from[0] == 'agent_id')
                                            @foreach($campaign_monitoring as $cm)
                                                <tr>
                                                    <td>{{$cm->services_title}}</td>
                                                    <td>{{$cm->qty}}</td>
                                                    <td><input type="hidden" name="sub_project_id_campaign[]"
                                                               value="{{$cm->sub_project_id}}">
                                                        <select class="form-control m-select2"
                                                                name="status_campaign[]"
                                                                style="width: 120px">
                                                            <option value="In Progress" {{$cm->status == 'In Progress' ? 'selected':''}}>
                                                                In Progress
                                                            </option>
                                                            <option value="Completed" {{$cm->status == 'Completed' ? 'selected':''}}>
                                                                Completed
                                                            </option>
                                                        </select></td>
                                                    <td>{{Main::format_date($cm->start_date)}}</td>
                                                    <td>{{Main::format_date($cm->end_date)}}</td>
                                                    <td>
                                                        <select class="form-control m-select2"
                                                                name="assign_to_campaign[]"
                                                                style="width: 120px">
                                                            <option value="" {{$cm->assign_to == '' ? 'selected':''}}>
                                                                Select Agent
                                                            </option>
                                                            @foreach($agent as $ag)
                                                                <option value="{{$ag->agent_id}}" {{$cm->assign_to == $ag->agent_id ? 'selected':''}}>
                                                                    {{$ag->agent_name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach($campaign_monitoring as $cm)
                                                <tr>
                                                    <td>{{$cm->services_title}}</td>
                                                    <td>{{$cm->qty}}</td>
                                                    <td>{{$cm->status}}</td>
                                                    <td>{{Main::format_date($cm->start_date)}}</td>
                                                    <td>{{Main::format_date($cm->end_date)}}</td>
                                                    <td>{{$cm->assign_to_label}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            @if($from[0] == 'agent_id')
                                <div style="align-items: center; justify-content: center; display: flex;">
                                    <button type="submit" class="btn btn-light-success font-weight-bold mr-2">Update Sub
                                        Project
                                    </button>
                                </div>
                            @endif
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="d-flex flex-column-fluid ">
            <!--begin::Card-->
            <div class="card card-custom card-stretch col-lg-12">
                @if($from[0] != 'admin_id')

                    <div class="card-header row row-marginless align-items-center flex-wrap py-5 h-auto">

                        <form action="{{route('projectInsertChat')}}" method="post"
                              class="form-send"
                              data-redirect=""
                              data-alert-show="true" data-alert-field-message="true" style="width: 100%">
                            {{ csrf_field() }}
                            <input type="hidden" name="project_id"
                                   value="{{\app\Helpers\Main::encrypt($project->project_id)}}">
                            <textarea class="form-control" rows="2" placeholder="Type a message" name="chat"></textarea>
                            <input type="file" name="file" id="file-banyak" hidden>
                            <input type="text" name="tipe_file" id="tipe-file" hidden>
                            <div class="d-flex align-items-center justify-content-between mt-5">
                                <div class="mr-3">
                                    <a class="btn btn-clean btn-icon btn-md mr-1 btn-file-custom">
                                        <i class="flaticon-attachment icon-lg"></i>
                                    </a>
                                    <span id="text-pilih"></span>
                                </div>
                                <div>
                                    <button type="submit"
                                            class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">
                                        Send
                                    </button>
                                </div>
                            </div>
                            <!--end::Compose-->
                        </form>

                    </div>
            @endif

            <!--begin::Body-->
                <div class="card-body table-responsive px-0" id="chat_page">
                    <!--begin::Items-->
                    @foreach($chat['chat'] as $value)
                        <div class="list list-hover min-w-500px" data-inbox="list">
                            <!--begin::Item-->
                            <div class="d-flex align-items-start card-spacer-x py-3 rounded bg-light-secondary"
                                 data-inbox="message">
                                <!--begin::Info-->
                                <div class="flex-grow-1 mt-2 mr-2" data-toggle="view">
                                    <div class="d-flex">
                                        <div class="symbol symbol-100 mr-3 ">
                                            <div class="symbol-label"
                                                 style="background-image:url({{'http://dev-storage.redsystem.id/private-label-system/'.$value->foto}}); height: 50px; width: 50px"></div>

                                        </div>
                                        <div>
                                            <span class="font-weight-bolder font-size-lg mr-2">{{$value->dari}}</span>

                                            <div class="text-muted mt-1" style="font-size: 10px">{{$value->waktu}}</div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="mt-2">
                                        <span style="white-space: pre-line">{{$value->chat}}</span>
                                    </div>
                                    @if($value->file != null || $value->file != '')
                                        <br>
                                        <div class="mt-2">
                                            <p class="font-weight-bolder font-size-lg">Uploaded Files :</p>
                                            <a style="color:#0a6aa1"
                                               href="{{'http://dev-storage.redsystem.id/private-label-system/'.$value->file}}"
                                               target="_blank">
                                                {{$value->nama_file}}
                                            </a>
                                        </div>
                                    @endif
                                </div>
                                <!--end::Info-->
                                <!--begin::Datetime-->
                            {{--                                <div class="mt-2 mr-3 font-weight-bolder w-150px text-right"--}}
                            {{--                                     data-toggle="view">{{$value->waktu}}</div>--}}
                            <!--end::Datetime-->
                            </div>
                        </div>
                        <br>
                    @endforeach
                    @if($count == 'beda')
                        <div style="text-align: center">
                            <button id="show_all" class="btn btn-light-success">Show All</button>
                        </div>
                    @endif
                </div>

                <div class="card-body table-responsive px-0 hidden" id="chat_page_all">
                    <!--begin::Items-->
                    @foreach($chat['chat_all'] as $value)
                        <div class="list list-hover min-w-500px" data-inbox="list">
                            <!--begin::Item-->
                            <div class="d-flex align-items-start card-spacer-x py-3 rounded bg-light-secondary"
                                 data-inbox="message">
                                <!--begin::Info-->
                                <div class="flex-grow-1 mt-2 mr-2" data-toggle="view">
                                    <div class="d-flex">
                                        <div class="symbol symbol-100 mr-3 ">
                                            <div class="symbol-label"
                                                 style="background-image:url({{'http://dev-storage.redsystem.id/private-label-system/'.$value->foto}}); height: 50px; width: 50px"></div>

                                        </div>
                                        <div>
                                            <span class="font-weight-bolder font-size-lg mr-2">{{$value->dari}}</span>

                                            <div class="text-muted mt-1" style="font-size: 10px">{{$value->waktu}}</div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="mt-2">
                                        <span style="white-space: pre-line">{{$value->chat}}</span>
                                    </div>
                                    @if($value->file != null || $value->file != '')
                                        <br>
                                        <div class="mt-2">
                                            <p class="font-weight-bolder font-size-lg">Uploaded Files :</p>
                                            <a style="color:#0a6aa1"
                                               href="{{'http://dev-storage.redsystem.id/private-label-system/'.$value->file}}"
                                               target="_blank">
                                                {{$value->nama_file}}
                                            </a>
                                        </div>
                                    @endif
                                </div>

                                <!--end::Info-->
                                <!--begin::Datetime-->
                                <div class="mt-2 mr-3 font-weight-bolder w-150px text-right"
                                     data-toggle="view">{{$value->waktu}}</div>
                                <!--end::Datetime-->
                            </div>
                            <!--end::Item-->
                        </div>
                        <br>
                    @endforeach
                    <div style="text-align: center">
                        <button id="show_less" class="btn btn-light-success">Show Less</button>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
    </div>
    <script>
        function myFunction() {
            alert("Sorry, this feature is still not available");
        }
    </script>
@endsection