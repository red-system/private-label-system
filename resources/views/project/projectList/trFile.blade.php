<table class="row-file hidden">
    <tbody>
    <tr data-index="">
        <td class="td-file-name">
            <input class="form-control m-input nama-bank" type="text" name="file_name[]" style="width: 250px">
        </td>
        <td class="td-file-desc">
            <textarea class="form-control m-input" name="file_link[]" style="width: 300px"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-file btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Delete
            </button>
        </td>
    </tr>
    </tbody>
</table>