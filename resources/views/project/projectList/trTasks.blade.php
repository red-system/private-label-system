<table class="row-tasks hidden">
    <tbody>
    <tr data-index="">
        <td class="td-task-name">
            <input class="form-control m-input nama-bank" type="text" name="tasks_name[]" style="width: 250px">
        </td>
{{--        <td class="td-master-id">--}}
{{--            <select class="form-control select2-tasks" name="tasks_status[]" style="width: 250px">--}}
{{--                <option value="">Select Status</option>--}}
{{--                <option value="Next Tasks">Next Tasks</option>--}}
{{--                <option value="Completed">Completed</option>--}}
{{--            </select>--}}
{{--        </td>--}}
        <td>
            <div class="input-group date" style="width: 150px">
                <input type="text" class="form-control m-input datepicker" name="start_date[]"
                       readonly="" value="{{ \app\Helpers\Main::format_date(now()) }}">
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </td>
        <td>
            <div class="input-group date" style="width: 150px">
                <input type="text" class="form-control m-input datepicker" name="end_date[]"
                       readonly="" value="{{ \app\Helpers\Main::format_date(now()) }}">
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </td>
        <td class="td-tasks-desc">
            <textarea class="form-control m-input" name="assign_to[]" style="width: 300px"></textarea>
        </td>
        <td class="td-tasks-desc">
            <textarea class="form-control m-input" name="tasks_desc[]" style="width: 300px"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-tasks btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Delete
            </button>
        </td>
    </tr>
    </tbody>
</table>