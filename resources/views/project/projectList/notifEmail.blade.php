<link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>

<div class="form-group m-form__group row">
    <label class="form-control-label col-2">You have received a new message for the Order</label>
    <label>:</label>
{{--    <label class="form-control-label col-8">{{route('projectListDetail', \app\Helpers\Main::encrypt($project->project_id))}}</label>--}}
{{--</div>--}}
{{--<div style="border-bottom:3px dashed #000000;">--}}
{{--</div>--}}
{{--<br>--}}
{{--<div class="form-group m-form__group row">--}}
    <label class="form-control-label col-8">{{$project->project_order_title}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">From</label>
    <label>:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$dari}}</label>
</div>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Message</label>
    <label>:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$comment}}</label>
</div>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Regards</label>
    <label>:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">Project Manager</label>
</div>
@if($file)
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Uploade Files</label>
    <label>:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit"><a href="{{'http://dev-storage.redsystem.id/private-label-system/'.$file}}">{{$nama_file}}</a></label>
</div>
@endif
<br>
<div class="form-group m-form__group row">
    <a href="{{route('projectListDetail', \app\Helpers\Main::encrypt($project->project_id))}}">Click here to view & reply</a>
</div>
<br>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Best Regards</label>
</div>
<div class="form-group m-form__group row">
    <label><b>Private Label ID</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2"> Jl. Ratna No.68, Tonja, Kec. Denpasar Utara, Kota Denpasar, Bali 80236</label>
</div>