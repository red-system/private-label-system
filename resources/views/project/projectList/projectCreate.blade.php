<form
        action="{{ route('projectListInsert') }}"
        method="post"
        class="m-form form-send"
        autocomplete="off"
        data-alert-show="true"
        data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:50%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Order ID </label>
                            <input type="text" class="form-control m-input" name="project_order_number" autofocus>
                        </div>
                        @if($from == 'admin_id' || $from == 'agent_id')
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">Agency </label>
                                <select name="agency_id" class="form-control m-select2" style="width: 100%">
                                    <option value="">Select Agency</option>
                                    @foreach($agency as $r)
                                        <option value="{{ $r->agency_id }}">{{ $r->agency_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company </label>
                            <select name="company_id" class="form-control m-select2" style="width: 100%">
                                <option value="">Select Company</option>
                                @foreach($company as $r)
                                    <option value="{{ $r->company_id }}">{{ $r->company_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Type </label>
                            <select name="project_type_id" class="form-control m-select2" style="width: 100%">
                                <option value="">Select Project Type</option>
                                @foreach($project_type as $r)
                                    <option value="{{ $r->project_type_id }}">{{ $r->project_type_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Services Type </label>
                            <select name="services_id" class="form-control m-select2" style="width: 100%">
                                <option value="">Select Services</option>
                                @foreach($services as $r)
                                    <option value="{{ $r->services_id }}">{{ $r->services_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Ammount </label>
                            <input type="text" class="form-control m-input input-numeral" name="project_ammount">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Cost </label>
                            <input type="text" class="form-control m-input input-numeral" name="project_cost">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Timeline </label>
                            <input type="number" class="form-control m-input" name="project_timeline">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Progress </label>
                            <input type="text" class="form-control m-input" name="project_progress">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Order Date </label>
                            <div class="input-group date">
                                <input type="text" name="project_order_date" class="form-control m-input m_datepicker"
                                       readonly="" value="{{ date('d-m-Y') }}">
                                <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status </label>
                            <select name="project_status_id" class="form-control m-select2" style="width: 100%">
                                <option value="">Select</option>
                                @foreach($project_status as $r)
                                    <option value="{{ $r->project_status_id }}">{{ $r->project_status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>