<div class="row">
    <input type="hidden" name="project_id" value="{{$project->project_id}}">
    <div class="col-9">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Order Title</label>
            <div class="col-7 col-form-label">
                <span class="no_bukti">{{$project->project_order_title}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Order Date</label>
            <div class="col-7 col-form-label">
                <span class="no_bukti">{{ Main::format_date($project->project_order_date) }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Status</label>
            <div class="col-7 col-form-label">
                <span class="project_status_name">{{$project->project_status->project_status_name}}</span>
            </div>
        </div>
    </div>
</div>

<hr/>
<button type="button" class="btn-add-row-file btn btn-accent btn-sm m-btn--pill">
    <i class="la la-plus"></i> Add Tasks
</button>
<br/><br/>
<table class="table-file table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>File Name</th>
        <th>Link</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<br/>
<br/>
<label for="example-text-input" class="col-form-label"><strong>Project File</strong></label>
<table class="table-file-ada table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>File Name</th>
        <th>Link</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>
    @foreach($file as $n)
        <tr data-index="">
            <td class="td-task-name">
                <input class="form-control m-input" type="text" name="file_name_ada[]" style="width: 250px"
                       value="{{$n->file_name}}">
            </td>
            <td class="td-file-desc">
                <textarea class="form-control m-input" name="file_link_ada[]" style="width: 300px">{{$n->file_link}}</textarea>
            </td>
            <td>
                <button type="button" class="btn-delete-row-file-ada btn m-btn--pill btn-danger btn-sm"
                        data-confirm="false">
                    <i class="la la-remove"></i> Delete
                </button>
            </td>
        </tr>
    @endforeach
    @if($count_file == 0)
        <tr>
            <td colspan="3" style="text-align: center">There is no data</td>
        </tr>
    @endif
    </tbody>
</table>
