@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('body')
    <div class=" container ">
        <br>
    <div class=" container-fluid ">
        <!--begin::Row-->
        <div class="row">
            <div class="col-lg-6">
                <!--begin::Charts Widget 1-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header h-auto border-0">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                            <h3 class="card-label">
                                <span class="d-block text-dark font-weight-bolder">Recent Stats</span>
                                <span class="d-block text-muted mt-2 font-size-sm">More than 400+ new members</span>
                            </h3>
                        </div>
                        <!--end::Title-->

                        <!--begin::Toolbar-->
                        <div class="card-toolbar">
                            <div class="dropdown dropdown-inline">
                                <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ki ki-bold-more-hor"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                                    <!--begin::Naviigation-->
                                    <ul class="navi">
                                        <li class="navi-header font-weight-bold py-5">
                                            <span class="font-size-lg">Add New:</span>
                                            <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip"
                                               data-placement="right" title=""
                                               data-original-title="Click to learn more..."></i>
                                        </li>
                                        <li class="navi-separator mb-3 opacity-70"></li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i class="flaticon2-shopping-cart-1"></i></span>
                                                <span class="navi-text">Order</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i
                                                            class="navi-icon flaticon2-calendar-8"></i></span>
                                                <span class="navi-text">Members</span>
                                                <span class="navi-label">
                <span class="label label-light-danger label-rounded font-weight-bold">3</span>
            </span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i
                                                            class="navi-icon flaticon2-telegram-logo"></i></span>
                                                <span class="navi-text">Project</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i
                                                            class="navi-icon flaticon2-new-email"></i></span>
                                                <span class="navi-text">Record</span>
                                                <span class="navi-label">
                <span class="label label-light-success label-rounded font-weight-bold">5</span>
            </span>
                                            </a>
                                        </li>
                                        <li class="navi-separator mt-3 opacity-70"></li>
                                        <li class="navi-footer pt-5 pb-4">
                                            <a class="btn btn-light-primary font-weight-bolder btn-sm" href="#">More
                                                options</a>
                                            <a class="btn btn-clean font-weight-bold btn-sm d-none" href="#"
                                               data-toggle="tooltip" data-placement="right" title=""
                                               data-original-title="Click to learn more...">Learn more</a>
                                        </li>
                                    </ul>
                                    <!--end::Naviigation-->
                                </div>
                            </div>
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Header-->

                    <!--begin::Body-->
                    <div class="card-body" style="position: relative;">
                        <!--begin::Chart-->
                        <div id="kt_charts_widget_1_chart" style="min-height: 365px;">
                            <div id="apexchartstrp7f90o"
                                 class="apexcharts-canvas apexchartstrp7f90o apexcharts-theme-light"
                                 style="width: 358px; height: 350px;">
                                <svg id="SvgjsSvg1303" width="358" height="350" xmlns="http://www.w3.org/2000/svg"
                                     version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                     xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                     style="background: transparent;">
                                    <g id="SvgjsG1305" class="apexcharts-inner apexcharts-graphical"
                                       transform="translate(45.625, 30)">
                                        <defs id="SvgjsDefs1304">
                                            <linearGradient id="SvgjsLinearGradient1309" x1="0" y1="0" x2="0" y2="1">
                                                <stop id="SvgjsStop1310" stop-opacity="0.4"
                                                      stop-color="rgba(216,227,240,0.4)" offset="0"></stop>
                                                <stop id="SvgjsStop1311" stop-opacity="0.5"
                                                      stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                                <stop id="SvgjsStop1312" stop-opacity="0.5"
                                                      stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                            </linearGradient>
                                            <clipPath id="gridRectMasktrp7f90o">
                                                <rect id="SvgjsRect1314" width="308.375" height="279.494" x="-3" y="-1"
                                                      rx="0" ry="0" opacity="1" stroke-width="0" stroke="none"
                                                      stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <clipPath id="gridRectMarkerMasktrp7f90o">
                                                <rect id="SvgjsRect1315" width="306.375" height="281.494" x="-2" y="-2"
                                                      rx="0" ry="0" opacity="1" stroke-width="0" stroke="none"
                                                      stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                        </defs>
                                        <rect id="SvgjsRect1313" width="7.559375" height="277.494" x="0" y="0" rx="0"
                                              ry="0" opacity="1" stroke-width="0" stroke-dasharray="3"
                                              fill="url(#SvgjsLinearGradient1309)" class="apexcharts-xcrosshairs"
                                              y2="277.494" filter="none" fill-opacity="0.9"></rect>
                                        <g id="SvgjsG1334" class="apexcharts-xaxis" transform="translate(0, 0)">
                                            <g id="SvgjsG1335" class="apexcharts-xaxis-texts-g"
                                               transform="translate(0, -4)">
                                                <text id="SvgjsText1337" font-family="Poppins" x="25.197916666666668"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1338">Feb</tspan>
                                                    <title>Feb</title></text>
                                                <text id="SvgjsText1340" font-family="Poppins" x="75.59375" y="306.494"
                                                      text-anchor="middle" dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1341">Mar</tspan>
                                                    <title>Mar</title></text>
                                                <text id="SvgjsText1343" font-family="Poppins" x="125.98958333333333"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1344">Apr</tspan>
                                                    <title>Apr</title></text>
                                                <text id="SvgjsText1346" font-family="Poppins" x="176.38541666666669"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1347">May</tspan>
                                                    <title>May</title></text>
                                                <text id="SvgjsText1349" font-family="Poppins" x="226.78125000000003"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1350">Jun</tspan>
                                                    <title>Jun</title></text>
                                                <text id="SvgjsText1352" font-family="Poppins" x="277.1770833333333"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1353">Jul</tspan>
                                                    <title>Jul</title></text>
                                            </g>
                                        </g>
                                        <g id="SvgjsG1366" class="apexcharts-grid">
                                            <g id="SvgjsG1367" class="apexcharts-gridlines-horizontal">
                                                <line id="SvgjsLine1369" x1="0" y1="0" x2="302.375" y2="0"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1370" x1="0" y1="69.3735" x2="302.375" y2="69.3735"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1371" x1="0" y1="138.747" x2="302.375" y2="138.747"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1372" x1="0" y1="208.12050000000002" x2="302.375"
                                                      y2="208.12050000000002" stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1373" x1="0" y1="277.494" x2="302.375" y2="277.494"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                            </g>
                                            <g id="SvgjsG1368" class="apexcharts-gridlines-vertical"></g>
                                            <line id="SvgjsLine1375" x1="0" y1="277.494" x2="302.375" y2="277.494"
                                                  stroke="transparent" stroke-dasharray="0"></line>
                                            <line id="SvgjsLine1374" x1="0" y1="1" x2="0" y2="277.494"
                                                  stroke="transparent" stroke-dasharray="0"></line>
                                        </g>
                                        <g id="SvgjsG1317" class="apexcharts-bar-series apexcharts-plot-series">
                                            <g id="SvgjsG1318" class="apexcharts-series" rel="1" seriesName="NetxProfit"
                                               data:realIndex="0">
                                                <path id="SvgjsPath1320"
                                                      d="M 17.63854166666667 277.494L 17.63854166666667 176.63604375000003Q 20.41822916666667 174.85635625000003 23.197916666666668 176.63604375000003L 23.197916666666668 176.63604375000003L 23.197916666666668 277.494L 23.197916666666668 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 17.63854166666667 277.494L 17.63854166666667 176.63604375000003Q 20.41822916666667 174.85635625000003 23.197916666666668 176.63604375000003L 23.197916666666668 176.63604375000003L 23.197916666666668 277.494L 23.197916666666668 277.494z"
                                                      pathFrom="M 17.63854166666667 277.494L 17.63854166666667 277.494L 23.197916666666668 277.494L 23.197916666666668 277.494L 23.197916666666668 277.494L 17.63854166666667 277.494"
                                                      cy="175.74620000000002" cx="67.03437500000001" j="0" val="44"
                                                      barHeight="101.74780000000001" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1321"
                                                      d="M 68.03437500000001 277.494L 68.03437500000001 151.19909375000003Q 70.8140625 149.41940625000004 73.59375000000001 151.19909375000003L 73.59375000000001 151.19909375000003L 73.59375000000001 277.494L 73.59375000000001 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 68.03437500000001 277.494L 68.03437500000001 151.19909375000003Q 70.8140625 149.41940625000004 73.59375000000001 151.19909375000003L 73.59375000000001 151.19909375000003L 73.59375000000001 277.494L 73.59375000000001 277.494z"
                                                      pathFrom="M 68.03437500000001 277.494L 68.03437500000001 277.494L 73.59375000000001 277.494L 73.59375000000001 277.494L 73.59375000000001 277.494L 68.03437500000001 277.494"
                                                      cy="150.30925000000002" cx="117.43020833333335" j="1" val="55"
                                                      barHeight="127.18475000000001" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1322"
                                                      d="M 118.43020833333335 277.494L 118.43020833333335 146.57419375000003Q 121.20989583333335 144.79450625000004 123.98958333333336 146.57419375000003L 123.98958333333336 146.57419375000003L 123.98958333333336 277.494L 123.98958333333336 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 118.43020833333335 277.494L 118.43020833333335 146.57419375000003Q 121.20989583333335 144.79450625000004 123.98958333333336 146.57419375000003L 123.98958333333336 146.57419375000003L 123.98958333333336 277.494L 123.98958333333336 277.494z"
                                                      pathFrom="M 118.43020833333335 277.494L 118.43020833333335 277.494L 123.98958333333336 277.494L 123.98958333333336 277.494L 123.98958333333336 277.494L 118.43020833333335 277.494"
                                                      cy="145.68435000000002" cx="167.8260416666667" j="2" val="57"
                                                      barHeight="131.80965" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1323"
                                                      d="M 168.8260416666667 277.494L 168.8260416666667 148.88664375000002Q 171.6057291666667 147.10695625000002 174.38541666666669 148.88664375000002L 174.38541666666669 148.88664375000002L 174.38541666666669 277.494L 174.38541666666669 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 168.8260416666667 277.494L 168.8260416666667 148.88664375000002Q 171.6057291666667 147.10695625000002 174.38541666666669 148.88664375000002L 174.38541666666669 148.88664375000002L 174.38541666666669 277.494L 174.38541666666669 277.494z"
                                                      pathFrom="M 168.8260416666667 277.494L 168.8260416666667 277.494L 174.38541666666669 277.494L 174.38541666666669 277.494L 174.38541666666669 277.494L 168.8260416666667 277.494"
                                                      cy="147.9968" cx="218.22187500000004" j="3" val="56"
                                                      barHeight="129.49720000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1324"
                                                      d="M 219.22187500000004 277.494L 219.22187500000004 137.32439375Q 222.00156250000003 135.54470625000002 224.78125000000003 137.32439375L 224.78125000000003 137.32439375L 224.78125000000003 277.494L 224.78125000000003 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 219.22187500000004 277.494L 219.22187500000004 137.32439375Q 222.00156250000003 135.54470625000002 224.78125000000003 137.32439375L 224.78125000000003 137.32439375L 224.78125000000003 277.494L 224.78125000000003 277.494z"
                                                      pathFrom="M 219.22187500000004 277.494L 219.22187500000004 277.494L 224.78125000000003 277.494L 224.78125000000003 277.494L 224.78125000000003 277.494L 219.22187500000004 277.494"
                                                      cy="136.43455" cx="268.6177083333334" j="4" val="61"
                                                      barHeight="141.05945000000003" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1325"
                                                      d="M 269.6177083333334 277.494L 269.6177083333334 144.26174375000002Q 272.3973958333334 142.48205625000003 275.17708333333337 144.26174375000002L 275.17708333333337 144.26174375000002L 275.17708333333337 277.494L 275.17708333333337 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 269.6177083333334 277.494L 269.6177083333334 144.26174375000002Q 272.3973958333334 142.48205625000003 275.17708333333337 144.26174375000002L 275.17708333333337 144.26174375000002L 275.17708333333337 277.494L 275.17708333333337 277.494z"
                                                      pathFrom="M 269.6177083333334 277.494L 269.6177083333334 277.494L 275.17708333333337 277.494L 275.17708333333337 277.494L 275.17708333333337 277.494L 269.6177083333334 277.494"
                                                      cy="143.3719" cx="319.0135416666667" j="5" val="58"
                                                      barHeight="134.12210000000002" barWidth="7.559375"></path>
                                            </g>
                                            <g id="SvgjsG1326" class="apexcharts-series" rel="2" seriesName="Revenue"
                                               data:realIndex="1">
                                                <path id="SvgjsPath1328"
                                                      d="M 25.197916666666668 277.494L 25.197916666666668 102.63764375000001Q 27.97760416666667 100.85795625000002 30.757291666666667 102.63764375000001L 30.757291666666667 102.63764375000001L 30.757291666666667 277.494L 30.757291666666667 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 25.197916666666668 277.494L 25.197916666666668 102.63764375000001Q 27.97760416666667 100.85795625000002 30.757291666666667 102.63764375000001L 30.757291666666667 102.63764375000001L 30.757291666666667 277.494L 30.757291666666667 277.494z"
                                                      pathFrom="M 25.197916666666668 277.494L 25.197916666666668 277.494L 30.757291666666667 277.494L 30.757291666666667 277.494L 30.757291666666667 277.494L 25.197916666666668 277.494"
                                                      cy="101.74780000000001" cx="74.59375000000001" j="0" val="76"
                                                      barHeight="175.74620000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1329"
                                                      d="M 75.59375000000001 277.494L 75.59375000000001 81.82559375000001Q 78.37343750000001 80.04590625000002 81.15312500000002 81.82559375000001L 81.15312500000002 81.82559375000001L 81.15312500000002 277.494L 81.15312500000002 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 75.59375000000001 277.494L 75.59375000000001 81.82559375000001Q 78.37343750000001 80.04590625000002 81.15312500000002 81.82559375000001L 81.15312500000002 81.82559375000001L 81.15312500000002 277.494L 81.15312500000002 277.494z"
                                                      pathFrom="M 75.59375000000001 277.494L 75.59375000000001 277.494L 81.15312500000002 277.494L 81.15312500000002 277.494L 81.15312500000002 277.494L 75.59375000000001 277.494"
                                                      cy="80.93575000000001" cx="124.98958333333336" j="1" val="85"
                                                      barHeight="196.55825000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1330"
                                                      d="M 125.98958333333336 277.494L 125.98958333333336 44.82639375000001Q 128.76927083333337 43.04670625000001 131.54895833333336 44.82639375000001L 131.54895833333336 44.82639375000001L 131.54895833333336 277.494L 131.54895833333336 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 125.98958333333336 277.494L 125.98958333333336 44.82639375000001Q 128.76927083333337 43.04670625000001 131.54895833333336 44.82639375000001L 131.54895833333336 44.82639375000001L 131.54895833333336 277.494L 131.54895833333336 277.494z"
                                                      pathFrom="M 125.98958333333336 277.494L 125.98958333333336 277.494L 131.54895833333336 277.494L 131.54895833333336 277.494L 131.54895833333336 277.494L 125.98958333333336 277.494"
                                                      cy="43.93655000000001" cx="175.38541666666669" j="2" val="101"
                                                      barHeight="233.55745000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1331"
                                                      d="M 176.38541666666669 277.494L 176.38541666666669 51.76374374999999Q 179.16510416666668 49.98405624999999 181.94479166666667 51.76374374999999L 181.94479166666667 51.76374374999999L 181.94479166666667 277.494L 181.94479166666667 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 176.38541666666669 277.494L 176.38541666666669 51.76374374999999Q 179.16510416666668 49.98405624999999 181.94479166666667 51.76374374999999L 181.94479166666667 51.76374374999999L 181.94479166666667 277.494L 181.94479166666667 277.494z"
                                                      pathFrom="M 176.38541666666669 277.494L 176.38541666666669 277.494L 181.94479166666667 277.494L 181.94479166666667 277.494L 181.94479166666667 277.494L 176.38541666666669 277.494"
                                                      cy="50.87389999999999" cx="225.78125000000003" j="3" val="98"
                                                      barHeight="226.62010000000004" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1332"
                                                      d="M 226.78125000000003 277.494L 226.78125000000003 77.20069375000001Q 229.56093750000002 75.42100625000002 232.34062500000002 77.20069375000001L 232.34062500000002 77.20069375000001L 232.34062500000002 277.494L 232.34062500000002 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 226.78125000000003 277.494L 226.78125000000003 77.20069375000001Q 229.56093750000002 75.42100625000002 232.34062500000002 77.20069375000001L 232.34062500000002 77.20069375000001L 232.34062500000002 277.494L 232.34062500000002 277.494z"
                                                      pathFrom="M 226.78125000000003 277.494L 226.78125000000003 277.494L 232.34062500000002 277.494L 232.34062500000002 277.494L 232.34062500000002 277.494L 226.78125000000003 277.494"
                                                      cy="76.31085000000002" cx="276.17708333333337" j="4" val="87"
                                                      barHeight="201.18315" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1333"
                                                      d="M 277.17708333333337 277.494L 277.17708333333337 35.57659374999999Q 279.9567708333334 33.796906249999985 282.73645833333336 35.57659374999999L 282.73645833333336 35.57659374999999L 282.73645833333336 277.494L 282.73645833333336 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 277.17708333333337 277.494L 277.17708333333337 35.57659374999999Q 279.9567708333334 33.796906249999985 282.73645833333336 35.57659374999999L 282.73645833333336 35.57659374999999L 282.73645833333336 277.494L 282.73645833333336 277.494z"
                                                      pathFrom="M 277.17708333333337 277.494L 277.17708333333337 277.494L 282.73645833333336 277.494L 282.73645833333336 277.494L 282.73645833333336 277.494L 277.17708333333337 277.494"
                                                      cy="34.68674999999999" cx="326.5729166666667" j="5" val="105"
                                                      barHeight="242.80725000000004" barWidth="7.559375"></path>
                                                <g id="SvgjsG1327" class="apexcharts-datalabels" data:realIndex="1"></g>
                                            </g>
                                            <g id="SvgjsG1319" class="apexcharts-datalabels" data:realIndex="0"></g>
                                        </g>
                                        <line id="SvgjsLine1376" x1="0" y1="0" x2="302.375" y2="0" stroke="#b6b6b6"
                                              stroke-dasharray="0" stroke-width="1"
                                              class="apexcharts-ycrosshairs"></line>
                                        <line id="SvgjsLine1377" x1="0" y1="0" x2="302.375" y2="0" stroke-dasharray="0"
                                              stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                                        <g id="SvgjsG1378" class="apexcharts-yaxis-annotations"></g>
                                        <g id="SvgjsG1379" class="apexcharts-xaxis-annotations"></g>
                                        <g id="SvgjsG1380" class="apexcharts-point-annotations"></g>
                                    </g>
                                    <g id="SvgjsG1354" class="apexcharts-yaxis" rel="0"
                                       transform="translate(15.625, 0)">
                                        <g id="SvgjsG1355" class="apexcharts-yaxis-texts-g">
                                            <text id="SvgjsText1356" font-family="Poppins" x="20" y="31.4"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1357">120</tspan>
                                            </text>
                                            <text id="SvgjsText1358" font-family="Poppins" x="20" y="100.77350000000001"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1359">90</tspan>
                                            </text>
                                            <text id="SvgjsText1360" font-family="Poppins" x="20" y="170.14700000000002"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1361">60</tspan>
                                            </text>
                                            <text id="SvgjsText1362" font-family="Poppins" x="20" y="239.52050000000003"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1363">30</tspan>
                                            </text>
                                            <text id="SvgjsText1364" font-family="Poppins" x="20" y="308.894"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1365">0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g id="SvgjsG1306" class="apexcharts-annotations"></g>
                                </svg>
                                <div class="apexcharts-legend"></div>
                                <div class="apexcharts-tooltip apexcharts-theme-light">
                                    <div class="apexcharts-tooltip-title"
                                         style="font-family: Poppins; font-size: 12px;"></div>
                                    <div class="apexcharts-tooltip-series-group"><span class="apexcharts-tooltip-marker"
                                                                                       style="background-color: rgb(27, 197, 189);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: Poppins; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                    <div class="apexcharts-tooltip-series-group"><span class="apexcharts-tooltip-marker"
                                                                                       style="background-color: rgb(229, 234, 238);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: Poppins; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                    <div class="apexcharts-yaxistooltip-text"></div>
                                </div>
                            </div>
                        </div>
                        <!--end::Chart-->
                        <div class="resize-triggers">
                            <div class="expand-trigger">
                                <div style="width: 417px; height: 466px;"></div>
                            </div>
                            <div class="contract-trigger"></div>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Charts Widget 1-->
            </div>
            <div class="col-lg-6">
                <!--begin::Charts Widget 1-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header h-auto border-0">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                            <h3 class="card-label">
                                <span class="d-block text-dark font-weight-bolder">Recent Stats</span>
                                <span class="d-block text-muted mt-2 font-size-sm">More than 400+ new members</span>
                            </h3>
                        </div>
                        <!--end::Title-->

                        <!--begin::Toolbar-->
                        <div class="card-toolbar">
                            <div class="dropdown dropdown-inline">
                                <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ki ki-bold-more-hor"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                                    <!--begin::Naviigation-->
                                    <ul class="navi">
                                        <li class="navi-header font-weight-bold py-5">
                                            <span class="font-size-lg">Add New:</span>
                                            <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip"
                                               data-placement="right" title=""
                                               data-original-title="Click to learn more..."></i>
                                        </li>
                                        <li class="navi-separator mb-3 opacity-70"></li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i class="flaticon2-shopping-cart-1"></i></span>
                                                <span class="navi-text">Order</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i
                                                            class="navi-icon flaticon2-calendar-8"></i></span>
                                                <span class="navi-text">Members</span>
                                                <span class="navi-label">
                <span class="label label-light-danger label-rounded font-weight-bold">3</span>
            </span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i
                                                            class="navi-icon flaticon2-telegram-logo"></i></span>
                                                <span class="navi-text">Project</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon"><i
                                                            class="navi-icon flaticon2-new-email"></i></span>
                                                <span class="navi-text">Record</span>
                                                <span class="navi-label">
                <span class="label label-light-success label-rounded font-weight-bold">5</span>
            </span>
                                            </a>
                                        </li>
                                        <li class="navi-separator mt-3 opacity-70"></li>
                                        <li class="navi-footer pt-5 pb-4">
                                            <a class="btn btn-light-primary font-weight-bolder btn-sm" href="#">More
                                                options</a>
                                            <a class="btn btn-clean font-weight-bold btn-sm d-none" href="#"
                                               data-toggle="tooltip" data-placement="right" title=""
                                               data-original-title="Click to learn more...">Learn more</a>
                                        </li>
                                    </ul>
                                    <!--end::Naviigation-->
                                </div>
                            </div>
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Header-->

                    <!--begin::Body-->
                    <div class="card-body" style="position: relative;">
                        <!--begin::Chart-->
                        <div id="kt_charts_widget_1_chart" style="min-height: 365px;">
                            <div id="apexchartstrp7f90o"
                                 class="apexcharts-canvas apexchartstrp7f90o apexcharts-theme-light"
                                 style="width: 358px; height: 350px;">
                                <svg id="SvgjsSvg1303" width="358" height="350" xmlns="http://www.w3.org/2000/svg"
                                     version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                     xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                     style="background: transparent;">
                                    <g id="SvgjsG1305" class="apexcharts-inner apexcharts-graphical"
                                       transform="translate(45.625, 30)">
                                        <defs id="SvgjsDefs1304">
                                            <linearGradient id="SvgjsLinearGradient1309" x1="0" y1="0" x2="0" y2="1">
                                                <stop id="SvgjsStop1310" stop-opacity="0.4"
                                                      stop-color="rgba(216,227,240,0.4)" offset="0"></stop>
                                                <stop id="SvgjsStop1311" stop-opacity="0.5"
                                                      stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                                <stop id="SvgjsStop1312" stop-opacity="0.5"
                                                      stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                            </linearGradient>
                                            <clipPath id="gridRectMasktrp7f90o">
                                                <rect id="SvgjsRect1314" width="308.375" height="279.494" x="-3" y="-1"
                                                      rx="0" ry="0" opacity="1" stroke-width="0" stroke="none"
                                                      stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <clipPath id="gridRectMarkerMasktrp7f90o">
                                                <rect id="SvgjsRect1315" width="306.375" height="281.494" x="-2" y="-2"
                                                      rx="0" ry="0" opacity="1" stroke-width="0" stroke="none"
                                                      stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                        </defs>
                                        <rect id="SvgjsRect1313" width="7.559375" height="277.494" x="0" y="0" rx="0"
                                              ry="0" opacity="1" stroke-width="0" stroke-dasharray="3"
                                              fill="url(#SvgjsLinearGradient1309)" class="apexcharts-xcrosshairs"
                                              y2="277.494" filter="none" fill-opacity="0.9"></rect>
                                        <g id="SvgjsG1334" class="apexcharts-xaxis" transform="translate(0, 0)">
                                            <g id="SvgjsG1335" class="apexcharts-xaxis-texts-g"
                                               transform="translate(0, -4)">
                                                <text id="SvgjsText1337" font-family="Poppins" x="25.197916666666668"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1338">Feb</tspan>
                                                    <title>Feb</title></text>
                                                <text id="SvgjsText1340" font-family="Poppins" x="75.59375" y="306.494"
                                                      text-anchor="middle" dominant-baseline="auto" font-size="12px"
                                                      font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1341">Mar</tspan>
                                                    <title>Mar</title></text>
                                                <text id="SvgjsText1343" font-family="Poppins" x="125.98958333333333"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1344">Apr</tspan>
                                                    <title>Apr</title></text>
                                                <text id="SvgjsText1346" font-family="Poppins" x="176.38541666666669"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1347">May</tspan>
                                                    <title>May</title></text>
                                                <text id="SvgjsText1349" font-family="Poppins" x="226.78125000000003"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1350">Jun</tspan>
                                                    <title>Jun</title></text>
                                                <text id="SvgjsText1352" font-family="Poppins" x="277.1770833333333"
                                                      y="306.494" text-anchor="middle" dominant-baseline="auto"
                                                      font-size="12px" font-weight="400" fill="#b5b5c3"
                                                      class="apexcharts-text apexcharts-xaxis-label "
                                                      style="font-family: Poppins;">
                                                    <tspan id="SvgjsTspan1353">Jul</tspan>
                                                    <title>Jul</title></text>
                                            </g>
                                        </g>
                                        <g id="SvgjsG1366" class="apexcharts-grid">
                                            <g id="SvgjsG1367" class="apexcharts-gridlines-horizontal">
                                                <line id="SvgjsLine1369" x1="0" y1="0" x2="302.375" y2="0"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1370" x1="0" y1="69.3735" x2="302.375" y2="69.3735"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1371" x1="0" y1="138.747" x2="302.375" y2="138.747"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1372" x1="0" y1="208.12050000000002" x2="302.375"
                                                      y2="208.12050000000002" stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                                <line id="SvgjsLine1373" x1="0" y1="277.494" x2="302.375" y2="277.494"
                                                      stroke="#ecf0f3" stroke-dasharray="4"
                                                      class="apexcharts-gridline"></line>
                                            </g>
                                            <g id="SvgjsG1368" class="apexcharts-gridlines-vertical"></g>
                                            <line id="SvgjsLine1375" x1="0" y1="277.494" x2="302.375" y2="277.494"
                                                  stroke="transparent" stroke-dasharray="0"></line>
                                            <line id="SvgjsLine1374" x1="0" y1="1" x2="0" y2="277.494"
                                                  stroke="transparent" stroke-dasharray="0"></line>
                                        </g>
                                        <g id="SvgjsG1317" class="apexcharts-bar-series apexcharts-plot-series">
                                            <g id="SvgjsG1318" class="apexcharts-series" rel="1" seriesName="NetxProfit"
                                               data:realIndex="0">
                                                <path id="SvgjsPath1320"
                                                      d="M 17.63854166666667 277.494L 17.63854166666667 176.63604375000003Q 20.41822916666667 174.85635625000003 23.197916666666668 176.63604375000003L 23.197916666666668 176.63604375000003L 23.197916666666668 277.494L 23.197916666666668 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 17.63854166666667 277.494L 17.63854166666667 176.63604375000003Q 20.41822916666667 174.85635625000003 23.197916666666668 176.63604375000003L 23.197916666666668 176.63604375000003L 23.197916666666668 277.494L 23.197916666666668 277.494z"
                                                      pathFrom="M 17.63854166666667 277.494L 17.63854166666667 277.494L 23.197916666666668 277.494L 23.197916666666668 277.494L 23.197916666666668 277.494L 17.63854166666667 277.494"
                                                      cy="175.74620000000002" cx="67.03437500000001" j="0" val="44"
                                                      barHeight="101.74780000000001" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1321"
                                                      d="M 68.03437500000001 277.494L 68.03437500000001 151.19909375000003Q 70.8140625 149.41940625000004 73.59375000000001 151.19909375000003L 73.59375000000001 151.19909375000003L 73.59375000000001 277.494L 73.59375000000001 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 68.03437500000001 277.494L 68.03437500000001 151.19909375000003Q 70.8140625 149.41940625000004 73.59375000000001 151.19909375000003L 73.59375000000001 151.19909375000003L 73.59375000000001 277.494L 73.59375000000001 277.494z"
                                                      pathFrom="M 68.03437500000001 277.494L 68.03437500000001 277.494L 73.59375000000001 277.494L 73.59375000000001 277.494L 73.59375000000001 277.494L 68.03437500000001 277.494"
                                                      cy="150.30925000000002" cx="117.43020833333335" j="1" val="55"
                                                      barHeight="127.18475000000001" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1322"
                                                      d="M 118.43020833333335 277.494L 118.43020833333335 146.57419375000003Q 121.20989583333335 144.79450625000004 123.98958333333336 146.57419375000003L 123.98958333333336 146.57419375000003L 123.98958333333336 277.494L 123.98958333333336 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 118.43020833333335 277.494L 118.43020833333335 146.57419375000003Q 121.20989583333335 144.79450625000004 123.98958333333336 146.57419375000003L 123.98958333333336 146.57419375000003L 123.98958333333336 277.494L 123.98958333333336 277.494z"
                                                      pathFrom="M 118.43020833333335 277.494L 118.43020833333335 277.494L 123.98958333333336 277.494L 123.98958333333336 277.494L 123.98958333333336 277.494L 118.43020833333335 277.494"
                                                      cy="145.68435000000002" cx="167.8260416666667" j="2" val="57"
                                                      barHeight="131.80965" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1323"
                                                      d="M 168.8260416666667 277.494L 168.8260416666667 148.88664375000002Q 171.6057291666667 147.10695625000002 174.38541666666669 148.88664375000002L 174.38541666666669 148.88664375000002L 174.38541666666669 277.494L 174.38541666666669 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 168.8260416666667 277.494L 168.8260416666667 148.88664375000002Q 171.6057291666667 147.10695625000002 174.38541666666669 148.88664375000002L 174.38541666666669 148.88664375000002L 174.38541666666669 277.494L 174.38541666666669 277.494z"
                                                      pathFrom="M 168.8260416666667 277.494L 168.8260416666667 277.494L 174.38541666666669 277.494L 174.38541666666669 277.494L 174.38541666666669 277.494L 168.8260416666667 277.494"
                                                      cy="147.9968" cx="218.22187500000004" j="3" val="56"
                                                      barHeight="129.49720000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1324"
                                                      d="M 219.22187500000004 277.494L 219.22187500000004 137.32439375Q 222.00156250000003 135.54470625000002 224.78125000000003 137.32439375L 224.78125000000003 137.32439375L 224.78125000000003 277.494L 224.78125000000003 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 219.22187500000004 277.494L 219.22187500000004 137.32439375Q 222.00156250000003 135.54470625000002 224.78125000000003 137.32439375L 224.78125000000003 137.32439375L 224.78125000000003 277.494L 224.78125000000003 277.494z"
                                                      pathFrom="M 219.22187500000004 277.494L 219.22187500000004 277.494L 224.78125000000003 277.494L 224.78125000000003 277.494L 224.78125000000003 277.494L 219.22187500000004 277.494"
                                                      cy="136.43455" cx="268.6177083333334" j="4" val="61"
                                                      barHeight="141.05945000000003" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1325"
                                                      d="M 269.6177083333334 277.494L 269.6177083333334 144.26174375000002Q 272.3973958333334 142.48205625000003 275.17708333333337 144.26174375000002L 275.17708333333337 144.26174375000002L 275.17708333333337 277.494L 275.17708333333337 277.494z"
                                                      fill="rgba(27,197,189,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 269.6177083333334 277.494L 269.6177083333334 144.26174375000002Q 272.3973958333334 142.48205625000003 275.17708333333337 144.26174375000002L 275.17708333333337 144.26174375000002L 275.17708333333337 277.494L 275.17708333333337 277.494z"
                                                      pathFrom="M 269.6177083333334 277.494L 269.6177083333334 277.494L 275.17708333333337 277.494L 275.17708333333337 277.494L 275.17708333333337 277.494L 269.6177083333334 277.494"
                                                      cy="143.3719" cx="319.0135416666667" j="5" val="58"
                                                      barHeight="134.12210000000002" barWidth="7.559375"></path>
                                            </g>
                                            <g id="SvgjsG1326" class="apexcharts-series" rel="2" seriesName="Revenue"
                                               data:realIndex="1">
                                                <path id="SvgjsPath1328"
                                                      d="M 25.197916666666668 277.494L 25.197916666666668 102.63764375000001Q 27.97760416666667 100.85795625000002 30.757291666666667 102.63764375000001L 30.757291666666667 102.63764375000001L 30.757291666666667 277.494L 30.757291666666667 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 25.197916666666668 277.494L 25.197916666666668 102.63764375000001Q 27.97760416666667 100.85795625000002 30.757291666666667 102.63764375000001L 30.757291666666667 102.63764375000001L 30.757291666666667 277.494L 30.757291666666667 277.494z"
                                                      pathFrom="M 25.197916666666668 277.494L 25.197916666666668 277.494L 30.757291666666667 277.494L 30.757291666666667 277.494L 30.757291666666667 277.494L 25.197916666666668 277.494"
                                                      cy="101.74780000000001" cx="74.59375000000001" j="0" val="76"
                                                      barHeight="175.74620000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1329"
                                                      d="M 75.59375000000001 277.494L 75.59375000000001 81.82559375000001Q 78.37343750000001 80.04590625000002 81.15312500000002 81.82559375000001L 81.15312500000002 81.82559375000001L 81.15312500000002 277.494L 81.15312500000002 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 75.59375000000001 277.494L 75.59375000000001 81.82559375000001Q 78.37343750000001 80.04590625000002 81.15312500000002 81.82559375000001L 81.15312500000002 81.82559375000001L 81.15312500000002 277.494L 81.15312500000002 277.494z"
                                                      pathFrom="M 75.59375000000001 277.494L 75.59375000000001 277.494L 81.15312500000002 277.494L 81.15312500000002 277.494L 81.15312500000002 277.494L 75.59375000000001 277.494"
                                                      cy="80.93575000000001" cx="124.98958333333336" j="1" val="85"
                                                      barHeight="196.55825000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1330"
                                                      d="M 125.98958333333336 277.494L 125.98958333333336 44.82639375000001Q 128.76927083333337 43.04670625000001 131.54895833333336 44.82639375000001L 131.54895833333336 44.82639375000001L 131.54895833333336 277.494L 131.54895833333336 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 125.98958333333336 277.494L 125.98958333333336 44.82639375000001Q 128.76927083333337 43.04670625000001 131.54895833333336 44.82639375000001L 131.54895833333336 44.82639375000001L 131.54895833333336 277.494L 131.54895833333336 277.494z"
                                                      pathFrom="M 125.98958333333336 277.494L 125.98958333333336 277.494L 131.54895833333336 277.494L 131.54895833333336 277.494L 131.54895833333336 277.494L 125.98958333333336 277.494"
                                                      cy="43.93655000000001" cx="175.38541666666669" j="2" val="101"
                                                      barHeight="233.55745000000002" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1331"
                                                      d="M 176.38541666666669 277.494L 176.38541666666669 51.76374374999999Q 179.16510416666668 49.98405624999999 181.94479166666667 51.76374374999999L 181.94479166666667 51.76374374999999L 181.94479166666667 277.494L 181.94479166666667 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 176.38541666666669 277.494L 176.38541666666669 51.76374374999999Q 179.16510416666668 49.98405624999999 181.94479166666667 51.76374374999999L 181.94479166666667 51.76374374999999L 181.94479166666667 277.494L 181.94479166666667 277.494z"
                                                      pathFrom="M 176.38541666666669 277.494L 176.38541666666669 277.494L 181.94479166666667 277.494L 181.94479166666667 277.494L 181.94479166666667 277.494L 176.38541666666669 277.494"
                                                      cy="50.87389999999999" cx="225.78125000000003" j="3" val="98"
                                                      barHeight="226.62010000000004" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1332"
                                                      d="M 226.78125000000003 277.494L 226.78125000000003 77.20069375000001Q 229.56093750000002 75.42100625000002 232.34062500000002 77.20069375000001L 232.34062500000002 77.20069375000001L 232.34062500000002 277.494L 232.34062500000002 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 226.78125000000003 277.494L 226.78125000000003 77.20069375000001Q 229.56093750000002 75.42100625000002 232.34062500000002 77.20069375000001L 232.34062500000002 77.20069375000001L 232.34062500000002 277.494L 232.34062500000002 277.494z"
                                                      pathFrom="M 226.78125000000003 277.494L 226.78125000000003 277.494L 232.34062500000002 277.494L 232.34062500000002 277.494L 232.34062500000002 277.494L 226.78125000000003 277.494"
                                                      cy="76.31085000000002" cx="276.17708333333337" j="4" val="87"
                                                      barHeight="201.18315" barWidth="7.559375"></path>
                                                <path id="SvgjsPath1333"
                                                      d="M 277.17708333333337 277.494L 277.17708333333337 35.57659374999999Q 279.9567708333334 33.796906249999985 282.73645833333336 35.57659374999999L 282.73645833333336 35.57659374999999L 282.73645833333336 277.494L 282.73645833333336 277.494z"
                                                      fill="rgba(229,234,238,1)" fill-opacity="1" stroke="transparent"
                                                      stroke-opacity="1" stroke-linecap="square" stroke-width="2"
                                                      stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                      clip-path="url(#gridRectMasktrp7f90o)"
                                                      pathTo="M 277.17708333333337 277.494L 277.17708333333337 35.57659374999999Q 279.9567708333334 33.796906249999985 282.73645833333336 35.57659374999999L 282.73645833333336 35.57659374999999L 282.73645833333336 277.494L 282.73645833333336 277.494z"
                                                      pathFrom="M 277.17708333333337 277.494L 277.17708333333337 277.494L 282.73645833333336 277.494L 282.73645833333336 277.494L 282.73645833333336 277.494L 277.17708333333337 277.494"
                                                      cy="34.68674999999999" cx="326.5729166666667" j="5" val="105"
                                                      barHeight="242.80725000000004" barWidth="7.559375"></path>
                                                <g id="SvgjsG1327" class="apexcharts-datalabels" data:realIndex="1"></g>
                                            </g>
                                            <g id="SvgjsG1319" class="apexcharts-datalabels" data:realIndex="0"></g>
                                        </g>
                                        <line id="SvgjsLine1376" x1="0" y1="0" x2="302.375" y2="0" stroke="#b6b6b6"
                                              stroke-dasharray="0" stroke-width="1"
                                              class="apexcharts-ycrosshairs"></line>
                                        <line id="SvgjsLine1377" x1="0" y1="0" x2="302.375" y2="0" stroke-dasharray="0"
                                              stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                                        <g id="SvgjsG1378" class="apexcharts-yaxis-annotations"></g>
                                        <g id="SvgjsG1379" class="apexcharts-xaxis-annotations"></g>
                                        <g id="SvgjsG1380" class="apexcharts-point-annotations"></g>
                                    </g>
                                    <g id="SvgjsG1354" class="apexcharts-yaxis" rel="0"
                                       transform="translate(15.625, 0)">
                                        <g id="SvgjsG1355" class="apexcharts-yaxis-texts-g">
                                            <text id="SvgjsText1356" font-family="Poppins" x="20" y="31.4"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1357">120</tspan>
                                            </text>
                                            <text id="SvgjsText1358" font-family="Poppins" x="20" y="100.77350000000001"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1359">90</tspan>
                                            </text>
                                            <text id="SvgjsText1360" font-family="Poppins" x="20" y="170.14700000000002"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1361">60</tspan>
                                            </text>
                                            <text id="SvgjsText1362" font-family="Poppins" x="20" y="239.52050000000003"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1363">30</tspan>
                                            </text>
                                            <text id="SvgjsText1364" font-family="Poppins" x="20" y="308.894"
                                                  text-anchor="end" dominant-baseline="auto" font-size="12px"
                                                  font-weight="400" fill="#b5b5c3"
                                                  class="apexcharts-text apexcharts-yaxis-label "
                                                  style="font-family: Poppins;">
                                                <tspan id="SvgjsTspan1365">0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g id="SvgjsG1306" class="apexcharts-annotations"></g>
                                </svg>
                                <div class="apexcharts-legend"></div>
                                <div class="apexcharts-tooltip apexcharts-theme-light">
                                    <div class="apexcharts-tooltip-title"
                                         style="font-family: Poppins; font-size: 12px;"></div>
                                    <div class="apexcharts-tooltip-series-group"><span class="apexcharts-tooltip-marker"
                                                                                       style="background-color: rgb(27, 197, 189);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: Poppins; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                    <div class="apexcharts-tooltip-series-group"><span class="apexcharts-tooltip-marker"
                                                                                       style="background-color: rgb(229, 234, 238);"></span>
                                        <div class="apexcharts-tooltip-text"
                                             style="font-family: Poppins; font-size: 12px;">
                                            <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                            <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                    <div class="apexcharts-yaxistooltip-text"></div>
                                </div>
                            </div>
                        </div>
                        <!--end::Chart-->
                        <div class="resize-triggers">
                            <div class="expand-trigger">
                                <div style="width: 417px; height: 466px;"></div>
                            </div>
                            <div class="contract-trigger"></div>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Charts Widget 1-->
            </div>
        </div>
        <!--end::Row-->

        <!--begin::Row-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Advance Table Widget 3-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 py-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">New Arrivals</span>
                            <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span>
                        </h3>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-success font-weight-bolder font-size-sm"><span
                                        class="svg-icon svg-icon-md svg-icon-white"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Communication/Add-user.svg--><svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                            viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
              fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
        <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
              fill="#000000" fill-rule="nonzero"></path>
    </g>
</svg><!--end::Svg Icon--></span>Add New Member</a>
                        </div>
                    </div>
                    <!--end::Header-->

                    <!--begin::Body-->
                    <div class="card-body pt-0 pb-3">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                <thead>
                                <tr class="text-uppercase">
                                    <th><span class="text-dark-75">products</span>
                                    </th>
                                    <th>pruce</th>
                                    <th>deposit</th>
                                    <th>agent</th>
                                    <th>status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 1; $i<=5; $i++)
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50 flex-shrink-0 mr-4">
                                                <div class="symbol-label"
                                                     style="background-image: url('{{asset('assets/demo/demo5_new/media/stock-600x400/img-26.jpg')}}')"></div>
                                            </div>

                                            <div>
                                                <a href="#"
                                                   class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sant
                                                    Extreanet Solution</a>
                                                <span class="text-muted font-weight-bold d-block">HTML, JS, ReactJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $2,790
                            </span>
                                        <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                    </td>
                                    <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $520
                            </span>
                                        <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                    </td>
                                    <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                Bradly Beal
                            </span>
                                        <span class="text-muted font-weight-bold">
                                Insurance
                            </span>
                                    </td>
                                    <td>
                                        <span class="label label-lg label-light-primary label-inline">Approved</span>
                                    </td>
                                </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 3-->
            </div>
        </div>
        <!--end::Row-->
    </div>
    </div>
@endsection