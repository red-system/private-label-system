<div class="row">
    <input type="hidden" name="project_id" value="{{$project->project_id}}">
    <div class="col-9">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Order Title</label>
            <div class="col-7 col-form-label">
                <span class="no_bukti">{{$project->project_order_title}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Order Date</label>
            <div class="col-7 col-form-label">
                <span class="no_bukti">{{ Main::format_date($project->project_order_date) }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Status</label>
            <div class="col-7 col-form-label">
                <span class="project_status_name">{{$project->project_status->project_status_name}}</span>
            </div>
        </div>
    </div>
</div>

<hr/>
<button type="button" class="btn-add-row-tasks btn btn-light-success font-weight-bold mr-2">
    <i class="la la-plus"></i> Add Tasks
</button>
<br/><br/>
<table class="table-tasks table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Task Name</th>
{{--        <th>Status</th>--}}
        <th>Start Date</th>
        <th>End Date</th>
        <th>Assign To</th>
        <th>Task Description</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
{{--<br/>--}}
{{--<br/>--}}
{{--<label for="example-text-input" class="col-form-label"><strong>Next Tasks</strong></label>--}}
{{--<table class="table-next-tasks table table-striped table-bordered table-hover">--}}
{{--    <thead>--}}
{{--    <tr>--}}
{{--        <th>Task Name</th>--}}
{{--        <th>Status</th>--}}
{{--        <th>Start Date</th>--}}
{{--        <th>End Date</th>--}}
{{--        <th>Assign To</th>--}}
{{--        <th>Task Description</th>--}}
{{--        <th>Delete</th>--}}
{{--    </tr>--}}
{{--    </thead>--}}
{{--    <tbody>--}}
{{--    @foreach($next_tasks as $n)--}}
{{--        <tr data-index="">--}}
{{--            <td class="td-task-name">--}}
{{--                <input class="form-control m-input" type="text" name="next_tasks_name[]" style="width: 250px"--}}
{{--                       value="{{$n->tasks_name}}">--}}
{{--            </td>--}}
{{--            <td class="td-master-id">--}}
{{--                <select class="form-control select2-tasks" name="next_tasks_status[]" style="width: 250px">--}}
{{--                    <option value="">Select Status</option>--}}
{{--                    <option value="Next Tasks" {{$n->tasks_status == 'Next Tasks' ? 'selected':''}}>Next Tasks</option>--}}
{{--                    <option value="Completed" {{$n->tasks_status == 'Completed' ? 'selected':''}}>Completed</option>--}}
{{--                </select>--}}
{{--            </td>--}}
{{--            <td>--}}
{{--                <div class="input-group date" style="width: 150px">--}}
{{--                    <input type="text"--}}
{{--                           class="form-control m-input datepicker-tasks"--}}
{{--                           name="next_start_date[]"--}}
{{--                           readonly=""--}}
{{--                           value="{{ \app\Helpers\Main::format_date($n->start_date) }}">--}}
{{--                    <div class="input-group-append">--}}
{{--                    <span class="input-group-text">--}}
{{--                        <i class="la la-calendar"></i>--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </td>--}}
{{--            <td>--}}
{{--                <div class="input-group date" style="width: 150px">--}}
{{--                    <input type="text" class="form-control datepicker-tasks"--}}
{{--                           readonly="readonly"--}}
{{--                           value="{{ \app\Helpers\Main::format_date($n->end_date) }}"--}}
{{--                           name="next_end_date[]">--}}
{{--                    <div class="input-group-append">--}}
{{--                    <span class="input-group-text">--}}
{{--                        <i class="la la-calendar"></i>--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </td>--}}
{{--            <td class="td-tasks-desc">--}}
{{--                <textarea class="form-control m-input" name="next_assign_to[]" style="width: 300px">{{$n->assign_to}}</textarea>--}}
{{--            </td>--}}
{{--            <td class="td-tasks-desc">--}}
{{--                <textarea class="form-control m-input" name="next_tasks_desc[]" style="width: 300px">{{$n->tasks_desc}}</textarea>--}}
{{--            </td>--}}
{{--            <td>--}}
{{--                <button type="button" class="btn-delete-row-next-tasks btn m-btn--pill btn-danger btn-sm"--}}
{{--                        data-confirm="false">--}}
{{--                    <i class="la la-remove"></i> Delete--}}
{{--                </button>--}}
{{--            </td>--}}
{{--        </tr>--}}
{{--    @endforeach--}}
{{--    @if($count_next_tasks == 0)--}}
{{--        <tr>--}}
{{--            <td colspan="6" style="text-align: center">There is no data</td>--}}
{{--        </tr>--}}
{{--    @endif--}}
{{--    </tbody>--}}
{{--</table>--}}
{{--<br/>--}}
{{--<br/>--}}
{{--<label for="example-text-input" class="col-form-label"><strong>Completed Tasks</strong></label>--}}
{{--<table class="table-next-tasks table table-striped table-bordered table-hover">--}}
{{--    <thead>--}}
{{--    <tr>--}}
{{--        <th>Task Name</th>--}}
{{--        <th>Status</th>--}}
{{--        <th>Start Date</th>--}}
{{--        <th>End Date</th>--}}
{{--        <th>Assign To</th>--}}
{{--        <th>Task Description</th>--}}
{{--        <th>Delete</th>--}}
{{--    </tr>--}}
{{--    </thead>--}}
{{--    <tbody>--}}
{{--    @foreach($completed as $c)--}}
{{--        <tr data-index="">--}}
{{--            <td class="td-task-name">--}}
{{--                <input class="form-control m-input nama-bank" type="text" name="completed_tasks_name[]"--}}
{{--                       style="width: 250px" value="{{$c->tasks_name}}">--}}
{{--            </td>--}}
{{--            <td class="td-master-id">--}}
{{--                <select class="form-control select2-tasks" name="completed_tasks_status[]" style="width: 250px">--}}
{{--                    <option value="">Select Status</option>--}}
{{--                    <option value="Next Tasks" {{$c->tasks_status == 'Next Tasks' ? 'selected':''}}>Next Tasks</option>--}}
{{--                    <option value="Completed" {{$c->tasks_status == 'Completed' ? 'selected':''}}>Completed</option>--}}
{{--                </select>--}}
{{--            </td>--}}
{{--            <td>--}}
{{--                <div class="input-group date" style="width: 150px">--}}
{{--                    <input type="text"--}}
{{--                           class="form-control m-input datepicker-tasks"--}}
{{--                           name="completed_start_date[]"--}}
{{--                           readonly=""--}}
{{--                           value="{{ \app\Helpers\Main::format_date($c->start_date) }}">--}}
{{--                    <div class="input-group-append">--}}
{{--                    <span class="input-group-text">--}}
{{--                        <i class="la la-calendar"></i>--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </td>--}}
{{--            <td>--}}
{{--                <div class="input-group date" style="width: 150px">--}}
{{--                    <input type="text" class="form-control datepicker-tasks"--}}
{{--                           readonly="readonly"--}}
{{--                           value="{{ \app\Helpers\Main::format_date($c->end_date) }}"--}}
{{--                           name="completed_end_date[]">--}}
{{--                    <div class="input-group-append">--}}
{{--                    <span class="input-group-text">--}}
{{--                        <i class="la la-calendar"></i>--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </td>--}}
{{--            <td class="td-tasks-desc">--}}
{{--                <textarea class="form-control m-input" name="completed_assign_to[]" style="width: 300px">{{$c->assign_to}}</textarea>--}}
{{--            </td><td class="td-tasks-desc">--}}
{{--                <textarea class="form-control m-input" name="completed_tasks_desc[]" style="width: 300px">{{$c->tasks_desc}}</textarea>--}}
{{--            </td>--}}

{{--            <td>--}}
{{--                <button type="button" class="btn-delete-row-completed-tasks btn m-btn--pill btn-danger btn-sm"--}}
{{--                        data-confirm="false">--}}
{{--                    <i class="la la-remove"></i> Delete--}}
{{--                </button>--}}
{{--            </td>--}}
{{--        </tr>--}}
{{--    @endforeach--}}
{{--    @if($count_completed == 0)--}}
{{--        <tr>--}}
{{--            <td colspan="6" style="text-align: center">There is no data</td>--}}
{{--        </tr>--}}
{{--    @endif--}}
{{--    </tbody>--}}
{{--</table>--}}
