@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('body')
    <div class=" container ">
        <br>
        <div class=" container-fluid ">
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="d-flex">
                        <!--begin: Info-->
                        <div class="flex-grow-1">
                            <!--begin: Title-->
                            <div class="d-flex align-items-center justify-content-between flex-wrap">
                                <div class="mr-3">
                                    <!--begin::Name-->
                                    <a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
                                        Nexa - Next generation SAAS <i class="flaticon2-correct text-success icon-md ml-2"></i>
                                    </a>
                                    <!--end::Name-->

                                    <!--begin::Contacts-->
                                    <div class="d-flex flex-wrap my-2">
                                        <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Communication/Mail-notification.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
        <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
    </g>
</svg><!--end::Svg Icon--></span>                                jason@siastudio.com
                                        </a>
                                        <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/General/Lock.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <mask fill="white">
            <use xlink:href="#path-1"></use>
        </mask>
        <g></g>
        <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"></path>
    </g>
</svg><!--end::Svg Icon--></span>                                PR Manager
                                        </a>
                                        <a href="#" class="text-muted text-hover-primary font-weight-bold">
                                <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Map/Marker2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
    </g>
</svg><!--end::Svg Icon--></span>                                Melbourne
                                        </a>
                                    </div>
                                    <!--end::Contacts-->
                                </div>
                            </div>
                            <!--end: Title-->

                            <!--begin: Content-->
                            <div class="d-flex align-items-center flex-wrap justify-content-between">
                                <div class="flex-grow-1 font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">
                                    I distinguish three main text objectives could be merely to inform people.<br>
                                    A second could be persuade people. You want people to bay objective.
                                </div>

                                <div class="d-flex flex-wrap align-items-center py-2">
                                    <div class="d-flex align-items-center mr-10">
                                        <div class="mr-6">
                                            <div class="font-weight-bold mb-2">Start Date</div>
                                            <span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold">07 May, 2020</span>
                                        </div>
                                        <div class="">
                                            <div class="font-weight-bold mb-2">Due Date</div>
                                            <span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold">10 June, 2021</span>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 flex-shrink-0 w-150px w-xl-300px mt-4 mt-sm-0">
                                        <span class="font-weight-bold">Progress</span>
                                        <div class="progress progress-xs mt-2 mb-2">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 63%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="font-weight-bolder text-dark">78%</span>
                                    </div>
                                </div>
                            </div>
                            <!--end: Content-->
                        </div>
                        <!--end: Info-->
                    </div>

                    <div class="separator separator-solid my-7"></div>

                    <!--begin: Items-->
                    <div class="d-flex align-items-center flex-wrap">
                        <!--begin: Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="mr-4">
                    <i class="flaticon-piggy-bank icon-2x text-muted font-weight-bold"></i>
                </span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Earnings</span>
                                <span class="font-weight-bolder font-size-h5"><span class="text-dark-50 font-weight-bold">$</span>249,500</span>
                            </div>
                        </div>
                        <!--end: Item-->

                        <!--begin: Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="mr-4">
                    <i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
                </span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Expenses</span>
                                <span class="font-weight-bolder font-size-h5"><span class="text-dark-50 font-weight-bold">$</span>164,700</span>
                            </div>
                        </div>
                        <!--end: Item-->

                        <!--begin: Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="mr-4">
                    <i class="flaticon-pie-chart icon-2x text-muted font-weight-bold"></i>
                </span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Net</span>
                                <span class="font-weight-bolder font-size-h5"><span class="text-dark-50 font-weight-bold">$</span>782,300</span>
                            </div>
                        </div>
                        <!--end: Item-->

                        <!--begin: Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="mr-4">
                    <i class="flaticon-file-2 icon-2x text-muted font-weight-bold"></i>
                </span>
                            <div class="d-flex flex-column flex-lg-fill">
                                <span class="text-dark-75 font-weight-bolder font-size-sm">73 Tasks</span>
                                <a href="#" class="text-primary font-weight-bolder">View</a>
                            </div>
                        </div>
                        <!--end: Item-->

                        <!--begin: Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="mr-4">
                    <i class="flaticon-chat-1 icon-2x text-muted font-weight-bold"></i>
                </span>
                            <div class="d-flex flex-column">
                                <span class="text-dark-75 font-weight-bolder font-size-sm">648 Comments</span>
                                <a href="#" class="text-primary font-weight-bolder">View</a>
                            </div>
                        </div>
                        <!--end: Item-->
                    </div>
                    <!--begin: Items-->
                </div>
            </div>

            <div class="row">
                <div class="col-lg-7">
                    <!--begin::Advance Table Widget 3-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 py-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Text</span>
                                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span>
                            </h3>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div class="card-body pt-0 pb-3">
                            <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                    <thead>
                                    <tr class="text-uppercase">
                                        <th style="min-width: 250px" class="pl-7"><span class="text-dark-75">products</span></th>
                                        <th style="min-width: 100px">pruce</th>
                                        <th style="min-width: 100px">deposit</th>
                                        <th style="min-width: 150px">agent</th>
                                        <th style="min-width: 130px">status</th>
                                        <th style="min-width: 120px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 flex-shrink-0 mr-4">
                                                    <div class="symbol-label" style="background-image: url('/metronic/themes/metronic/theme/html/demo5/dist/assets/media/stock-600x400/img-26.jpg')"></div>
                                                </div>

                                                <div>
                                                    <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sant Extreanet Solution</a>
                                                    <span class="text-muted font-weight-bold d-block">HTML, JS, ReactJS</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $2,790
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $520
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                Bradly Beal
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Insurance
                            </span>
                                        </td>
                                        <td>
                                            <span class="label label-lg label-light-primary label-inline">Approved</span>
                                        </td>
                                        <td class="text-right pr-0">
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mr-3">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/General/Bookmark.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M8,4 L16,4 C17.1045695,4 18,4.8954305 18,6 L18,17.726765 C18,18.2790497 17.5522847,18.726765 17,18.726765 C16.7498083,18.726765 16.5087052,18.6329798 16.3242754,18.4639191 L12.6757246,15.1194142 C12.2934034,14.7689531 11.7065966,14.7689531 11.3242754,15.1194142 L7.67572463,18.4639191 C7.26860564,18.8371115 6.63603827,18.8096086 6.26284586,18.4024896 C6.09378519,18.2180598 6,17.9769566 6,17.726765 L6,6 C6,4.8954305 6.8954305,4 8,4 Z" fill="#000000"></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Navigation/Arrow-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-0">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 flex-shrink-0 mr-4">
                                                    <div class="symbol-label" style="background-image: url('/metronic/themes/metronic/theme/html/demo5/dist/assets/media/stock-600x400/img-3.jpg')"></div>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Telegram Development</a>
                                                    <span class="text-muted font-weight-bold d-block">C#, ASP.NET, MS SQL</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $4,600
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Pending
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $1,600
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Rejected
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                Chris Thompson
                            </span>
                                            <span class="text-muted font-weight-bold">
                                NBA Player
                            </span>
                                        </td>
                                        <td>
                                            <span class="label label-lg label-light-warning label-inline">In Progress</span>
                                        </td>
                                        <td class="text-right pr-0">
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mr-3">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/General/Bookmark.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M8,4 L16,4 C17.1045695,4 18,4.8954305 18,6 L18,17.726765 C18,18.2790497 17.5522847,18.726765 17,18.726765 C16.7498083,18.726765 16.5087052,18.6329798 16.3242754,18.4639191 L12.6757246,15.1194142 C12.2934034,14.7689531 11.7065966,14.7689531 11.3242754,15.1194142 L7.67572463,18.4639191 C7.26860564,18.8371115 6.63603827,18.8096086 6.26284586,18.4024896 C6.09378519,18.2180598 6,17.9769566 6,17.726765 L6,6 C6,4.8954305 6.8954305,4 8,4 Z" fill="#000000"></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Navigation/Arrow-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 flex-shrink-0 mr-4">
                                                    <div class="symbol-label" style="background-image: url('/metronic/themes/metronic/theme/html/demo5/dist/assets/media/stock-600x400/img-5.jpg')"></div>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Payroll Application</a>
                                                    <span class="text-muted font-weight-bold d-block">PHP, Laravel, VueJS</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $23,800
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $6,700
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                Zoey McGee
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Ruby Developer
                            </span>
                                        </td>
                                        <td>
                                            <span class="label label-lg label-light-success label-inline">Success</span>
                                        </td>
                                        <td class="text-right pr-0">
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mr-3">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/General/Bookmark.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M8,4 L16,4 C17.1045695,4 18,4.8954305 18,6 L18,17.726765 C18,18.2790497 17.5522847,18.726765 17,18.726765 C16.7498083,18.726765 16.5087052,18.6329798 16.3242754,18.4639191 L12.6757246,15.1194142 C12.2934034,14.7689531 11.7065966,14.7689531 11.3242754,15.1194142 L7.67572463,18.4639191 C7.26860564,18.8371115 6.63603827,18.8096086 6.26284586,18.4024896 C6.09378519,18.2180598 6,17.9769566 6,17.726765 L6,6 C6,4.8954305 6.8954305,4 8,4 Z" fill="#000000"></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Navigation/Arrow-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-0">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 flex-shrink-0 mr-4">
                                                    <div class="symbol-label" style="background-image: url('/metronic/themes/metronic/theme/html/demo5/dist/assets/media/stock-600x400/img-18.jpg')"></div>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">HR Management System</a>
                                                    <span class="text-muted font-weight-bold d-block">Python, PostgreSQL, ReactJS</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $57,000
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                $14,000
                            </span>
                                            <span class="text-muted font-weight-bold">
                                Paid
                            </span>
                                        </td>
                                        <td>
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                Brandon Ingram
                            </span>
                                            <span class="text-muted font-weight-bold">
                                NBA Player
                            </span>
                                        </td>
                                        <td>
                                            <span class="label label-lg label-light-danger label-inline">Rejected</span>
                                        </td>
                                        <td class="text-right pr-0">
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mr-3">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/General/Bookmark.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M8,4 L16,4 C17.1045695,4 18,4.8954305 18,6 L18,17.726765 C18,18.2790497 17.5522847,18.726765 17,18.726765 C16.7498083,18.726765 16.5087052,18.6329798 16.3242754,18.4639191 L12.6757246,15.1194142 C12.2934034,14.7689531 11.7065966,14.7689531 11.3242754,15.1194142 L7.67572463,18.4639191 C7.26860564,18.8371115 6.63603827,18.8096086 6.26284586,18.4024896 C6.09378519,18.2180598 6,17.9769566 6,17.726765 L6,6 C6,4.8954305 6.8954305,4 8,4 Z" fill="#000000"></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                            <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo5/dist/assets/media/svg/icons/Navigation/Arrow-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Advance Table Widget 3-->
                </div>
                <div class="col-lg-5">
                    <!--begin::Charts Widget 3-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header h-auto border-0">
                            <div class="card-title py-5">
                                <h3 class="card-label">
                                    <span class="d-block text-dark font-weight-bolder">Recent Orders</span>
                                    <span class="d-block text-muted mt-2 font-size-sm">More than 500+ new orders</span>
                                </h3>
                            </div>
                            <div class="card-toolbar">
                                <ul class="nav nav-pills nav-pills-sm nav-dark-75" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_charts_widget_2_chart_tab_1">
                                            <span class="nav-text font-size-sm">Month</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_charts_widget_2_chart_tab_2">
                                            <span class="nav-text font-size-sm">Week</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_charts_widget_2_chart_tab_3">
                                            <span class="nav-text font-size-sm">Day</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div class="card-body" style="position: relative;">
                            <div id="kt_charts_widget_3_chart" style="min-height: 365px;"><div id="apexchartss23h8xft" class="apexcharts-canvas apexchartss23h8xft apexcharts-theme-light apexcharts-zoomable" style="width: 414px; height: 350px;"><svg id="SvgjsSvg1453" width="414" height="350" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1455" class="apexcharts-inner apexcharts-graphical" transform="translate(46, 30)"><defs id="SvgjsDefs1454"><clipPath id="gridRectMasks23h8xft"><rect id="SvgjsRect1459" width="352.1796875" height="280.797375" x="-3.5" y="-1.5" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMasks23h8xft"><rect id="SvgjsRect1460" width="349.1796875" height="281.797375" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath></defs><g id="SvgjsG1468" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1469" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"><text id="SvgjsText1471" font-family="Poppins" x="0" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1472">Feb</tspan><title>Feb</title></text><text id="SvgjsText1474" font-family="Poppins" x="57.52994791666667" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1475">Mar</tspan><title>Mar</title></text><text id="SvgjsText1477" font-family="Poppins" x="115.05989583333333" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1478">Apr</tspan><title>Apr</title></text><text id="SvgjsText1480" font-family="Poppins" x="172.58984374999997" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1481">May</tspan><title>May</title></text><text id="SvgjsText1483" font-family="Poppins" x="230.11979166666666" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1484">Jun</tspan><title>Jun</title></text><text id="SvgjsText1486" font-family="Poppins" x="287.64973958333337" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1487">Jul</tspan><title>Jul</title></text><text id="SvgjsText1489" font-family="Poppins" x="345.17968750000006" y="306.797375" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1490">Aug</tspan><title>Aug</title></text></g></g><g id="SvgjsG1503" class="apexcharts-grid"><g id="SvgjsG1504" class="apexcharts-gridlines-horizontal"><line id="SvgjsLine1506" x1="0" y1="0" x2="345.1796875" y2="0" stroke="#ecf0f3" stroke-dasharray="4" class="apexcharts-gridline"></line><line id="SvgjsLine1507" x1="0" y1="69.44934375" x2="345.1796875" y2="69.44934375" stroke="#ecf0f3" stroke-dasharray="4" class="apexcharts-gridline"></line><line id="SvgjsLine1508" x1="0" y1="138.8986875" x2="345.1796875" y2="138.8986875" stroke="#ecf0f3" stroke-dasharray="4" class="apexcharts-gridline"></line><line id="SvgjsLine1509" x1="0" y1="208.34803125" x2="345.1796875" y2="208.34803125" stroke="#ecf0f3" stroke-dasharray="4" class="apexcharts-gridline"></line><line id="SvgjsLine1510" x1="0" y1="277.797375" x2="345.1796875" y2="277.797375" stroke="#ecf0f3" stroke-dasharray="4" class="apexcharts-gridline"></line></g><g id="SvgjsG1505" class="apexcharts-gridlines-vertical"></g><line id="SvgjsLine1512" x1="0" y1="277.797375" x2="345.1796875" y2="277.797375" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1511" x1="0" y1="1" x2="0" y2="277.797375" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1462" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1463" class="apexcharts-series" seriesName="NetxProfit" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1466" d="M 0 277.797375L 0 243.07270312500003C 20.135481770833334 243.07270312500003 37.394466145833334 208.34803125000002 57.52994791666667 208.34803125000002C 77.66542968750001 208.34803125000002 94.9244140625 208.34803125000002 115.05989583333334 208.34803125000002C 135.19537760416668 208.34803125000002 152.45436197916666 34.72467187500007 172.58984375 34.72467187500007C 192.72532552083334 34.72467187500007 209.98430989583335 34.72467187500007 230.11979166666669 34.72467187500007C 250.25527343750002 34.72467187500007 267.51425781250003 104.17401562500004 287.64973958333337 104.17401562500004C 307.7852213541667 104.17401562500004 325.04420572916666 104.17401562500004 345.1796875 104.17401562500004C 345.1796875 104.17401562500004 345.1796875 104.17401562500004 345.1796875 277.797375M 345.1796875 104.17401562500004z" fill="rgba(238,229,255,1)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMasks23h8xft)" pathTo="M 0 277.797375L 0 243.07270312500003C 20.135481770833334 243.07270312500003 37.394466145833334 208.34803125000002 57.52994791666667 208.34803125000002C 77.66542968750001 208.34803125000002 94.9244140625 208.34803125000002 115.05989583333334 208.34803125000002C 135.19537760416668 208.34803125000002 152.45436197916666 34.72467187500007 172.58984375 34.72467187500007C 192.72532552083334 34.72467187500007 209.98430989583335 34.72467187500007 230.11979166666669 34.72467187500007C 250.25527343750002 34.72467187500007 267.51425781250003 104.17401562500004 287.64973958333337 104.17401562500004C 307.7852213541667 104.17401562500004 325.04420572916666 104.17401562500004 345.1796875 104.17401562500004C 345.1796875 104.17401562500004 345.1796875 104.17401562500004 345.1796875 277.797375M 345.1796875 104.17401562500004z" pathFrom="M -1 347.24671875L -1 347.24671875L 57.52994791666667 347.24671875L 115.05989583333334 347.24671875L 172.58984375 347.24671875L 230.11979166666669 347.24671875L 287.64973958333337 347.24671875L 345.1796875 347.24671875"></path><path id="SvgjsPath1467" d="M 0 243.07270312500003C 20.135481770833334 243.07270312500003 37.394466145833334 208.34803125000002 57.52994791666667 208.34803125000002C 77.66542968750001 208.34803125000002 94.9244140625 208.34803125000002 115.05989583333334 208.34803125000002C 135.19537760416668 208.34803125000002 152.45436197916666 34.72467187500007 172.58984375 34.72467187500007C 192.72532552083334 34.72467187500007 209.98430989583335 34.72467187500007 230.11979166666669 34.72467187500007C 250.25527343750002 34.72467187500007 267.51425781250003 104.17401562500004 287.64973958333337 104.17401562500004C 307.7852213541667 104.17401562500004 325.04420572916666 104.17401562500004 345.1796875 104.17401562500004" fill="none" fill-opacity="1" stroke="#8950fc" stroke-opacity="1" stroke-linecap="butt" stroke-width="3" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMasks23h8xft)" pathTo="M 0 243.07270312500003C 20.135481770833334 243.07270312500003 37.394466145833334 208.34803125000002 57.52994791666667 208.34803125000002C 77.66542968750001 208.34803125000002 94.9244140625 208.34803125000002 115.05989583333334 208.34803125000002C 135.19537760416668 208.34803125000002 152.45436197916666 34.72467187500007 172.58984375 34.72467187500007C 192.72532552083334 34.72467187500007 209.98430989583335 34.72467187500007 230.11979166666669 34.72467187500007C 250.25527343750002 34.72467187500007 267.51425781250003 104.17401562500004 287.64973958333337 104.17401562500004C 307.7852213541667 104.17401562500004 325.04420572916666 104.17401562500004 345.1796875 104.17401562500004" pathFrom="M -1 347.24671875L -1 347.24671875L 57.52994791666667 347.24671875L 115.05989583333334 347.24671875L 172.58984375 347.24671875L 230.11979166666669 347.24671875L 287.64973958333337 347.24671875L 345.1796875 347.24671875"></path><g id="SvgjsG1464" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1520" r="0" cx="0" cy="0" class="apexcharts-marker w0a3m744s no-pointer-events" stroke="#8950fc" fill="#eee5ff" fill-opacity="1" stroke-width="3" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG1465" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine1514" x1="0" y1="0" x2="0" y2="277.797375" stroke="#8950fc" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="277.797375" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><line id="SvgjsLine1515" x1="0" y1="0" x2="345.1796875" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1516" x1="0" y1="0" x2="345.1796875" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1517" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1518" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1519" class="apexcharts-point-annotations"></g><rect id="SvgjsRect1521" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe" class="apexcharts-zoom-rect"></rect><rect id="SvgjsRect1522" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe" class="apexcharts-selection-rect"></rect></g><g id="SvgjsG1491" class="apexcharts-yaxis" rel="0" transform="translate(16, 0)"><g id="SvgjsG1492" class="apexcharts-yaxis-texts-g"><text id="SvgjsText1493" font-family="Poppins" x="20" y="31.4" text-anchor="end" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1494">100</tspan></text><text id="SvgjsText1495" font-family="Poppins" x="20" y="100.84934375" text-anchor="end" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1496">80</tspan></text><text id="SvgjsText1497" font-family="Poppins" x="20" y="170.2986875" text-anchor="end" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1498">60</tspan></text><text id="SvgjsText1499" font-family="Poppins" x="20" y="239.74803125" text-anchor="end" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1500">40</tspan></text><text id="SvgjsText1501" font-family="Poppins" x="20" y="309.19737499999997" text-anchor="end" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#b5b5c3" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Poppins;"><tspan id="SvgjsTspan1502">20</tspan></text></g></g><rect id="SvgjsRect1513" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG1456" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-title" style="font-family: Poppins; font-size: 12px;"></div><div class="apexcharts-tooltip-series-group"><span class="apexcharts-tooltip-marker" style="background-color: rgb(238, 229, 255);"></span><div class="apexcharts-tooltip-text" style="font-family: Poppins; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-light"><div class="apexcharts-xaxistooltip-text" style="font-family: Poppins; font-size: 12px;"></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
                            <div class="resize-triggers"><div class="expand-trigger"><div style="width: 474px; height: 418px;"></div></div><div class="contract-trigger"></div></div></div>
                        <!--end::Body-->
                    </div>
                    <!--end::Charts Widget 3-->
                </div>
            </div>


        </div>
    </div>
@endsection