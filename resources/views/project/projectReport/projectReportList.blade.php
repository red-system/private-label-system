@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/project.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <!-- END: Left Aside -->
    <div class="container">
        <input type="hidden" id="list_url" data-list-url="{{route('projectReportList')}}">
        <div id="table_coloumn" style="display: none">{{$view}}</div>
        <div id="action_list" style="display: none">{{$actionButton}}</div>
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-custom gutter-b card-shadowless bg-white">
            <div class="card-header border-0 py-5">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <i class="la la-gear"></i> Filter Data
                        </h3>
                    </div>
                </div>
                <br>
            </div>
            <form method="get" class="m-form m-form--fit m-form--label-align-right">
                <div class="card-body py-0">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">Status</label>
                        <div class="col-lg-3">
                            <select class="form-control m-select2" name="project_status_id">
                                <option value="all" {{ $project_status_id == 'all' ? 'selected':'' }}>All</option>
                                @foreach($status as $r)
                                    <option value="{{ $r->project_status_id }}" {{ $project_status_id == $r->project_status_id ? 'selected':'' }}>{{ $r->project_status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot text-center">
{{--                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">--}}
                        <button type="submit" class="btn btn btn-primary mr-2 akses-filter">
                            <i class="la la-search"></i> Filter Data
                        </button>
{{--                    </div>--}}
                </div>
            </form>
            <br>
        </div>

        <div class="card card-custom gutter-b card-shadowless bg-white">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title align-items-start flex-column">
                    {{--                            <span class="card-label font-weight-bolder text-dark">Agents Stats</span>--}}
                </h3>
            </div>
            <div class="card-body py-0">
                <div class="table-responsive">
                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th>Order Title</th>
                            <th>Status</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection
