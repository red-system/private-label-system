<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i
            class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left bg-white">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            @foreach($menu as $key=>$value)
                @if(!isset($value['sub']))
                    @php
                        $aksesName = 'akses-'.$key;
                        $menuStatus = '';
                        if($menuActive) {
                            if($key == $menuActive) {
                                $menuStatus = 'm-menu__item--active';
                            }
                        } elseif($routeName == $value['route'] && $routeName !== 'underconstructionPage') {
                            $menuStatus = 'm-menu__item--active';
                        }
                    @endphp
                    <li class="m-menu__item {{ $menuStatus }} {{ $aksesName }}"
                        data-akses-name="{{ $aksesName }}"
                        aria-haspopup="true" hidden>
                        @if($value['route'] == 'supportCenter')
                            <a href="{{ route($value['route'], $menu_support->support_id) }}" class="m-menu__link ">
                                <i class="m-menu__link-icon {{$value['icon']}}"></i>
                                <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">{{ Main::menuAction($key) }} </span>
                                </span>
                            </span>
                            </a>
                        @else
                            <a href="{{ route($value['route']) }}" class="m-menu__link ">
                                <i class="m-menu__link-icon {{$value['icon']}}"></i>
                                <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">{{ Main::menuAction($key) }} </span>
                                </span>
                            </span>
                            </a>
                        @endif
                    </li>
                @else
                    @php

                        $aksesName = 'akses-'.$key;

                        $menuArr = [];
                        foreach($value['sub'] as $r) {
                            $menuArr[] = $r['route'];
                        }

                        $labelArr = [];
                        foreach($value['sub'] as $label=>$r) {
                            $labelArr[] = $label;
                        }

                        $subMenuStatus = '';
                        if(in_array($routeName, $menuArr)) {
                            $subMenuStatus = 'm-menu__item--active m-menu__item--open akses-aktif';
                        } else {
                            if(in_array($menuActive, $labelArr)) {
                                $subMenuStatus = 'm-menu__item--active m-menu__item--open akses-aktif';
                            }
                        }

                    //ksort($value['sub']);

                    @endphp
                    <li class="m-menu__item  m-menu__item--submenu m-menu__item--open {{ $aksesName }}"
                        data-akses-name="{{ $aksesName }}"
                        aria-haspopup="true"
                        m-menu-submenu-toggle="hover" hidden>
                        <a href="javascript:;" class="m-menu__link m-menu__toggle ">
                            <i class="m-menu__link-icon {{ $value['icon'] }}"></i>
                            <span class="m-menu__link-text">{{ Main::menuAction($key) }}</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                @foreach($value['sub'] as $key2=>$value2)

                                    @php

                                        $aksesName = 'akses-'.$key2;

                                        $subMenuActive = '';
                                        if($routeName == $value2['route'] || $menuActive == $key2 ) {
                                            $subMenuActive = 'm-menu__item--active akses-aktif';
                                        }

                                    @endphp
                                    @if($key2 == 'project')
{{--                                        @php($project_parent = '')--}}
{{--                                        @if($routeName == 'projectListPage'||$routeName == 'projectPerPage')--}}
{{--                                            @php($project_parent = 'm-menu__item--active akses-aktif m-menu__item--open')--}}
{{--                                        @endif--}}
{{--                                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--open {{$project_parent}}  {{ $aksesName }}"--}}
{{--                                            data-akses-name="{{ $aksesName }}"--}}
{{--                                            aria-haspopup="true">--}}
{{--                                            <a href="javascript:;" class="m-menu__link m-menu__toggle ">--}}
{{--                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
{{--                                                    <span></span>--}}
{{--                                                </i>--}}
{{--                                                <span class="m-menu__link-text">{{ Main::menuAction($key2) }}</span>--}}
{{--                                                <i class="m-menu__ver-arrow la la-angle-right"></i>--}}

{{--                                            </a>--}}
{{--                                            <div class="m-menu__submenu ">--}}
{{--                                                <span class="m-menu__arrow"></span>--}}
{{--                                                <ul class="m-menu__subnav">--}}
                                                    @php($project_sub_parent = '')
                                                    @if($routeName == 'projectListPage')
                                                        @php($project_sub_parent = 'm-menu__item--active akses-aktif')
                                                    @endif
                                                    @foreach($project_status as $kunci => $ps)
                                                        @php($project_menu = '')
                                                        @if($menuActive == 'project-status-'.$ps->project_status_id)
                                                            @php($project_menu = 'm-menu__item--active akses-aktif')
                                                        @endif
                                                        <li class="m-menu__item {{$project_menu}}"
                                                            aria-haspopup="true">
                                                            <a href="{{ route('projectPerPage', $ps->project_status_id) }}"
                                                               class="m-menu__link ">
                                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                    <span></span>
                                                                </i>
                                                                <span class="m-menu__link-title">
                                                                    <span class="m-menu__link-wrap">
                                                                        <span class="m-menu__link-text">{{$ps->project_status_name}}</span>
                                                                        @if($from[0] == 'agency_id' && $ps->count != 0)
                                                                            <span class="m-menu__link-badge">
                                                                                <span class="m-badge m-badge--danger">{{$ps->count}}</span>
                                                                            </span>
                                                                        @endif
                                                                    </span>
                                                                </span>
                                                                {{--                                                                <span class="m-menu__link-text">{{$ps->project_status_name}}</span> @if($from[0] == 'agency_id')--}}
                                                                {{--                                                                    <span class="m-menu__link-badge"><span--}}
                                                                {{--                                                                                class="m-badge m-badge--danger"--}}
                                                                {{--                                                                                style="vertical-align:middle">{{$ps->count}}</span></span>--}}
                                                                {{--                                                                    <span class="menu-label">--}}
                                                                {{--                                                                                                                                            <span class="label label-rounded label-primary">{{$ps->count}}</span>--}}
                                                                {{--                                                                                                                                        </span>--}}
                                                                {{--                                                                    <span class="btn btn-icon btn-sm font-weight-bolder p-0 bg-light-primary">{{$ps->count}}</span>--}}
                                                                {{--                                                                @endif--}}
                                                            </a>

                                                        </li>
                                                    @endforeach
                                                    <li class="m-menu__item {{$project_sub_parent}}"
                                                        aria-haspopup="true">
                                                        <a href="{{ route($value2['route']) }}" class="m-menu__link ">
                                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                <span></span>
                                                            </i>
                                                            <span class="m-menu__link-title">
                                                                    <span class="m-menu__link-wrap">
                                                                        <span class="m-menu__link-text">All Project</span>
                                                                        @if($from[0] == 'agency_id' && $count != 0)
                                                                            <span class="m-menu__link-badge">
                                                                                <span class="m-badge m-badge--danger">{{$count}}</span>
                                                                            </span>
                                                                        @endif
                                                                    </span>
                                                                </span>
{{--                                                            <span class="m-menu__link-text">All Project @if($from[0] == 'agency_id')--}}
{{--                                                                    <span class="btn btn-icon btn-sm font-weight-bolder p-0 bg-light-primary">{{$count}}</span>@endif</span>--}}

                                                        </a>
                                                    </li>
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
                                    @else
                                        <li class="m-menu__item {{ $subMenuActive }} {{ $aksesName }}"
                                            data-akses-name="{{ $aksesName }}"
                                            aria-haspopup="true">
                                            <a href="{{ route($value2['route']) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">{{ Main::menuAction($key2) }}</span>

                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif
            @endforeach
            {{--                <li class="m-menu__item"--}}
            {{--                    aria-haspopup="true">--}}
            {{--                </li>--}}

        </ul>

        <ul class="m-menu__nav" hidden id="profile_menu">
            <li class="m-menu__item">
                <hr>

                <div class="d-flex align-items-center mt-5">
                    <div class="symbol symbol-100 mr-5">
                    </div>
                    <div class="symbol symbol-100 mr-3 d-flex flex-column">
                        <div class="symbol-label"
                             style="background-image:url({{'http://dev-storage.redsystem.id/private-label-system/'.$user_foto}}); height: 80px; width: 80px"></div>
                    </div>
                    <div class="d-flex flex-column">
                        <a href="{{route('profilPage')}}"
                           class="font-weight-bold text-dark-75 text-hover-primary"
                           style="font-size: 13px">{{$user->user_name}}</a>
                        <div class="text-muted mt-1" style="font-size: 10px">{{$user->user_role->user_role_name}}</div>
                        <div class="navi mt-2">
                            <a href="{{route('profilPage')}}" class="navi-item">
								<span class="navi-link p-0 pb-2">
									<span class="navi-icon mr-1">
										<span class="svg-icon svg-icon-lg svg-icon-primary">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
											<svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px"
                                                 viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
                                                          fill="#000000"/>
													<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"/>
												</g>
											</svg>
                                            <!--end::Svg Icon-->
										</span>
									</span>
									<span class="navi-text text-muted text-hover-primary"
                                          style="font-size: 9px">{{$user->user_email}}</span>
								</span>
                            </a>
                            <a href="{{route('logoutDo')}}"
                               class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5" style="font-size: 9px">Sign
                                Out</a>
                        </div>
                    </div>
                </div>
                <!--end::Separator-->
                <!--begin::Nav-->
{{--                <div class="navi navi-spacer-x-1 p-0">--}}
{{--                    <!--begin::Item-->--}}
{{--                    <a href="{{route('profilPage')}}" class="navi-item">--}}
{{--                        <div class="navi-link">--}}
{{--                            <div class="symbol symbol-40 bg-light mr-3">--}}
{{--                                <div class="symbol-label">--}}
{{--									<span class="svg-icon svg-icon-md svg-icon-success">--}}
{{--										<!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg-->--}}
{{--										<svg xmlns="http://www.w3.org/2000/svg"--}}
{{--                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"--}}
{{--                                             viewBox="0 0 24 24" version="1.1">--}}
{{--											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--												<rect x="0" y="0" width="24" height="24"/>--}}
{{--												<path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"--}}
{{--                                                      fill="#000000"/>--}}
{{--												<circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5"/>--}}
{{--											</g>--}}
{{--										</svg>--}}
{{--                                        <!--end::Svg Icon-->--}}
{{--									</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="navi-text">--}}
{{--                                <div class="font-weight-bold">My Profile</div>--}}
{{--                                <div class="text-muted" style="font-size: 8px">Account and Password settings</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
            </li>
        </ul>

    </div>
</div>
