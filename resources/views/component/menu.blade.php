<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
    <!-- BEGIN: Aside Menu -->
    <div id="kt_aside_menu" class="aside-menu min-h-lg-800px" data-menu-vertical="1" data-menu-scroll="1"
         data-menu-dropdown-timeout="500">
        <ul class="menu-nav">
            @foreach($menu as $key=>$value)
                @if(!isset($value['sub']))
                    @php
                        $aksesName = 'akses-'.$key;
                        $menuStatus = '';
                        if($menuActive) {
                            if($key == $menuActive) {
                                $menuStatus = 'm-menu__item--active';
                            }
                        } elseif($routeName == $value['route'] && $routeName !== 'underconstructionPage') {
                            $menuStatus = 'menu-item-active';
                        }
                    @endphp
                    <li class="menu-item {{ $menuStatus }} {{ $aksesName }}"
                        data-akses-name="{{ $aksesName }}"
                        aria-haspopup="true">
                        @if($value['route'] == 'supportCenter')
                            <a href="{{ route($value['route'], $menu_support->support_id) }}"
                               class="menu-link">
                            <span class="svg-icon menu-icon">
                                <i class="{{$value['icon']}}">
                                <span></span>
                            </i>
                            </span>
                                <span class="menu-text">{{ Main::menuAction($key) }}</span>
                            </a>
                        @else
                            <a href="{{ route($value['route']) }}"
                               class="menu-link">
                            <span class="svg-icon menu-icon">
                                <i class="{{$value['icon']}}">
                                <span></span>
                            </i>
                            </span>
                                <span class="menu-text">{{ Main::menuAction($key) }}</span>
                            </a>
                        @endif
                    </li>
                @else
                    @php
                        $aksesName = 'akses-'.$key;

                        $menuArr = [];
                        foreach($value['sub'] as $r) {
                            $menuArr[] = $r['route'];
                        }
                        $labelArr = [];
                        foreach($value['sub'] as $label=>$r) {
                            $labelArr[] = $label;
                        }
                        $subMenuStatus = '';
                        if(in_array($routeName, $menuArr)) {
                            $subMenuStatus = 'menu-item-active akses-aktif';
                        } else {
                            if(in_array($menuActive, $labelArr)) {
                                $subMenuStatus = 'menu-item-active akses-aktif';
                            }
                        }

                    //ksort($value['sub']);
                    @endphp

                    <li class="menu-item menu-item-submenu  menu-item-open {{ $subMenuStatus }} {{ $aksesName }}"
                        aria-haspopup="true" data-akses-name="{{ $aksesName }}" data-menu-toggle="hover">
                        <a href="javascript:;" class="menu-link menu-toggle">
                                <span class="svg-icon menu-icon">
                            <i class="m-menu__link-icon {{ $value['icon'] }} fa-fw"><span></span></i>
                                </span>
                            <span class="menu-text">{{ Main::menuAction($key) }}</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="menu-submenu">
                            <i class="menu-arrow"></i>
                            <ul class="menu-subnav">
                                <li class="menu-item menu-item-parent" aria-haspopup="true">
                                    <span class="menu-link">
                                        <span class="menu-text">{{ Main::menuAction($key) }}</span>
                                    </span>
                                </li>
                                @foreach($value['sub'] as $key2=>$value2)
                                    @php
                                        $aksesName = 'akses-'.$key2;
                                        $subMenuActive = '';
                                        if($routeName == $value2['route'] || $menuActive == $key2 ) {
                                            $subMenuActive = 'menu-item-active akses-aktif';
                                        }
                                    @endphp
                                    <li class="menu-item {{ $subMenuActive }} {{ $aksesName }}"
                                        data-akses-name="{{ $aksesName }}"
                                        aria-haspopup="true">
                                        <a href="{{ route($value2['route']) }}" class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">{{ Main::menuAction($key2) }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>