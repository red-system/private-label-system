<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
@php
        $count = count($breadcrumb);
        $hitung = 0;
    @endphp
    @foreach($breadcrumb as $row)
        @php($hitung += 1)
        <li class="breadcrumb-item">
            <a href="{{ $row['route'] }}" class="text-muted">{{ Main::menuAction($row['label']) }}</a>
        </li>
{{--        <li class="m-nav__item">--}}
{{--            <a href="{{ $row['route'] }}" class="m-nav__link">--}}
{{--                <span class="m-nav__link-text">{{ Main::menuAction($row['label']) }}</span>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        @if($count > $hitung)--}}
{{--            <li class="m-nav__separator">--}}
{{--                <i class="la la-angle-right"></i>--}}
{{--            </li>--}}
{{--        @endif--}}
    @endforeach
</ul>

{{--<div class="d-flex align-items-baseline mr-5">--}}
{{--    <!--begin::Page Title-->--}}
{{--    <h2 class="subheader-title text-dark font-weight-bold my-2 mr-3">Buttons</h2>--}}
{{--    <!--end::Page Title-->--}}
{{--    <!--begin::Breadcrumb-->--}}
{{--    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">--}}
{{--        <li class="breadcrumb-item">--}}
{{--            <a href="" class="text-muted">Bootstrap</a>--}}
{{--        </li>--}}
{{--        <li class="breadcrumb-item">--}}
{{--            <a href="" class="text-muted">Buttons</a>--}}
{{--        </li>--}}
{{--    </ul>--}}
{{--    <!--end::Breadcrumb-->--}}
{{--</div>--}}