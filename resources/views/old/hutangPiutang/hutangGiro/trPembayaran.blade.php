<table class="row-pembayaran m--hide">
    <tbody>
    <tr data-index="">
        <input type="hidden" name="hari_ini[]" value="{{date('d-m-Y')}}">
        <td class="td-master-id">
            <select class="form-control metode-pembayaran m-select2" name="metode_pembayaran[]" style="width: 250px">
                <option value="">Pilih Metode</option>
                <option value="Tunai">Tunai</option>
                <option value="Bank">Bank</option>
            </select>
        </td>
        <td class="td-jumlah">
            <input type="text" name="jumlah[]" class="form-control input-numeral" value="0"
                   style="width: 120px">
        </td>
        <td class="td-nama-bank">
            <input class="form-control m-input nama-bank" type="text" name="nama_bank[]" style="width: 120px" readonly>
        </td>
        <td class="td-keterangan">
            <textarea class="form-control m-input" name="keterangan[]" style="width: 120px"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-pembayaran btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
    </tbody>
</table>