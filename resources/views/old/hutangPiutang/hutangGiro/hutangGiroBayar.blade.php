<div class="modal fade" id="modal-bayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form action="{{ route('hutangGiroInsert') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('hutangGiroPage') }}"
          data-alert-show="true"
          data-alert-field-message="true">

        {{ csrf_field() }}
        <div class="modal-dialog modal-xxlg" role="document">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <h3 class="modal-title m--font-light" id="exampleModalLabel">PENCAIRAN</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="la la-check"></i> Bayar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </form>
</div>

