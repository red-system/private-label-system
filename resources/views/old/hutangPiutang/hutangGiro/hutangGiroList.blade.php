@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/status.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('old.hutangPiutang.hutangGiro.hutangGiroBayar')
    @include('old.hutangPiutang.hutangGiro.trPembayaran')
    @include('old.hutangPiutang.hutangGiro.hutangDetail')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('hutangGiroList')}}">
        <div id="table_coloumn" style="display: none">{{$view}}</div>
        <div id="action_list" style="display: none">{{$actionButton}}</div>
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Hutang</th>
                            <th>Tanggal Pencairan</th>
                            <th>Jumlah Hutang</th>
                            <th>Langganan</th>
                            <th>No Faktur</th>
                            <th>Tanggal Faktur</th>
                            <th>Bank</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
