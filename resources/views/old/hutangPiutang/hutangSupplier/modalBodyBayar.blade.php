<div class="row">
    <input type="hidden" name="no_hutang_supplier" value="{{$hs->no_hutang_supplier}}">
    <input type="hidden" name="id_pembelian" value="{{$hs->id_pembelian}}">
    <input type="hidden" name="id_supplier" value="{{$hs->id_supplier}}">
    <input type="hidden" name="no_bukti_pembayaran" value="{{$no_bukti_pembayaran}}">
    <input type="hidden" name="total" value="{{$hs->sisa_hutang_supplier}}">
    <input type="hidden" name="id" value="{{$hs->id}}">
    <input type="hidden" name="terbayar" value="0">
    <input type="hidden" name="sisa_hutang_supplier" value="{{$hs->sisa_hutang_supplier}}">
    <div class="col-6">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">No Bukti</label>
            <div class="col-9 col-form-label">
                <span class="no_bukti">{{$no_bukti_pembayaran}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
            <div class="col-4">
                <input type="text" class="form-control m-input datepicker-modal" name="tanggal_transaksi"
                       readonly=""
                       value="{{ date('d-m-Y') }}">
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">No Hutang</label>
            <div class="col-9 col-form-label">
                <span class="no_hutang_pelanggan">{{$hs->no_hutang_supplier}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
            <div class="col-9 col-form-label">
                <span class="fullname_distributor">{{$hs->suppliers}}</span>
            </div>
        </div>
    </div>
</div>

<hr/>
<button type="button" class="btn-add-row-pembayaran btn btn-accent btn-sm m-btn--pill">
    <i class="la la-plus"></i> Tambah Pembayaran
</button>
<br/><br/>
<table class="table-pembayaran table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Metode Pembayaran</th>
        <th>Jumlah Pembayaran</th>
        <th>No. Check / Giro / BG</th>
        <th>Tanggal Pencairan</th>
        <th>Nama Bank</th>
        <th>Keterangan</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<br/>
<hr/>
<div class="form-no-padding">
    <div class="form-group m-form__group row">
        <label for="example-text-input" class="col-3 col-form-label">
            <h3>Total</h3>
        </label>
        <div class="col-9">
            <h3 class="total">{{$hs->sisa_hutang_supplier}}</h3>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label for="example-text-input" class="col-3 col-form-label">
            <h3>Terbayar</h3>
        </label>
        <div class="col-9">
            <h3 class="terbayar">0</h3>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label for="example-text-input" class="col-3 col-form-label">
            <h3>Sisa Pembayaran</h3>
        </label>
        <div class="col-9">
            <h3 class="sisa-pembayaran">{{$hs->sisa_hutang_supplier}}</h3>
        </div>
    </div>
</div>
