<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Hutang</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8">{{$hutang->no_hutang_supplier}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Faktur</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$hutang->no_faktur_pembelian}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Supplier</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$hutang->suppliers}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Faktur</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($hutang->tgl_faktur_pembelian)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Jatuh Tempo</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit"><b>{{\Carbon\Carbon::parse($hutang->jatuh_tempo)->format('d-m-Y')}}</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Keterangan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$hutang->keterangan_hutang_supplier}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Total</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$hutang->total_hutang_supplier}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Sisa</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$hutang->sisa_hutang_supplier}}</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Status</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$hutang->status_hutang_supplier}}</b></label>
</div>

<table class="table-pembayaran table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>No</th>
        <th>No Bukti Pembayaran</th>
        <th>Tanggal Pembayaran</th>
        <th>Metode Pembayaran</th>
        <th>Jumlah Pembayaran</th>
        <th>No Check / Giro / BG</th>
        <th>Tanggal Pencairan</th>
        <th>Nama Bank</th>
        <th>Keterangan</th>

    </tr>
    </thead>
    <tbody>


    @php
        $no = 0;
    @endphp
    @foreach($history as $r)
        @php($no += 1)
        <tr data-index="">
            <td>
                {{$no}}
            </td>
            <td>
                {{$r->no_bukti_pembayaran}}
            </td>
            <td>
                {{\Carbon\Carbon::parse($r->tgl_pembayaran)->format('d-m-Y')}}
            </td>
            <td>
                {{$r->metode_pembayaran}}
            </td>
            <td>
                {{$r->jumlah}}
            </td>
            <td>
                {{$r->no_cek_giro}}
            </td>
            <td>
                {{\Carbon\Carbon::parse($r->tgl_pencairan_giro)->format('d-m-Y')}}
            </td>
            <td>
                {{$r->bank}}
            </td>
            <td>
                {{$r->keterangan}}
            </td>
        </tr>

    @endforeach
    </tbody>
    @if($no == 0)
        <tfoot>
        <tr>
            <td colspan="9" style="text-align: center">Belum Ada Data Pembayaran</td>
        </tr>
        </tfoot>
    @endif
</table>