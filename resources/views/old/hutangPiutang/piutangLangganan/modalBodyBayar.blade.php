<div class="row">
    <input type="hidden" name="no_piutang_langganan" value="{{$pl->no_piutang_langganan}}">
    <input type="hidden" name="id_langganan" value="{{$pl->id_langganan}}">
    <input type="hidden" name="no_bukti" value="{{$no_bukti}}">
    <input type="hidden" name="total" value="{{$pl->sisa_piutang}}">
    <input type="hidden" name="id" value="{{$pl->id}}">
    <input type="hidden" name="terbayar" value="0">
    <input type="hidden" name="sisa_piutang" value="{{$pl->sisa_piutang}}">
    <div class="col-6">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">No Bukti</label>
            <div class="col-9 col-form-label">
                <span class="no_bukti">{{$no_bukti}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
            <div class="col-4">
                <input type="text" class="form-control m-input datepicker-modal" name="tanggal_transaksi"
                       readonly=""
                       value="{{ date('d-m-Y') }}">
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">No Piutang</label>
            <div class="col-9 col-form-label">
                <span class="no_piutang_pelanggan">{{$pl->no_piutang_langganan}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Langganan</label>
            <div class="col-9 col-form-label">
                <span class="fullname_distributor">{{$langganan}}</span>
            </div>
        </div>
    </div>
</div>

<hr/>
<button type="button" class="btn-add-row-pembayaran btn btn-accent btn-sm m-btn--pill">
    <i class="la la-plus"></i> Tambah Pembayaran
</button>
<br/><br/>
<table class="table-pembayaran table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Metode Pembayaran</th>
        <th>Jumlah Pembayaran</th>
        <th>No. Check / Giro / BG</th>
        <th>Tanggal Pencairan</th>
        <th>Nama Bank</th>
        <th>Keterangan</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<br/>
<hr/>
<div class="form-no-padding">
    <div class="form-group m-form__group row">
        <label for="example-text-input" class="col-3 col-form-label">
            <h3>Total</h3>
        </label>
        <div class="col-9">
            <h3 class="total">{{Main::format_number($pl->sisa_piutang)}}</h3>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label for="example-text-input" class="col-3 col-form-label">
            <h3>Terbayar</h3>
        </label>
        <div class="col-9">
            <h3 class="terbayar">0</h3>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label for="example-text-input" class="col-3 col-form-label">
            <h3>Sisa Pembayaran</h3>
        </label>
        <div class="col-9">
            <h3 class="sisa-pembayaran">{{Main::format_number($pl->sisa_piutang)}}</h3>
        </div>
    </div>
</div>
