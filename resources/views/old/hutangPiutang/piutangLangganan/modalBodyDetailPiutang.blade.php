<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Piutang</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8">{{$piutang->no_piutang_langganan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Faktur</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$piutang->no_faktur_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Pelanggan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$piutang->langganan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Faktur</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($piutang->tgl_faktur_penjualan)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Jatuh Tempo</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit"><b>{{\Carbon\Carbon::parse($piutang->jatuh_tempo)->format('d-m-Y')}}</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Lokasi Penjualan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$piutang->lokasi_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Keterangan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$piutang->keterangan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Total</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$piutang->total_piutang}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Terbayar</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$piutang->terbayar}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Sisa</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$piutang->sisa_piutang}}</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Status</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$piutang->status}}</b></label>
</div>

<table class="table-pembayaran table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>No</th>
        <th>No Bukti Pembayaran</th>
        <th>Tanggal Pembayaran</th>
        <th>Metode Pembayaran</th>
        <th>Jumlah Pembayaran</th>
        <th>No Check / Giro / BG</th>
        <th>Tanggal Pencairan</th>
        <th>Nama Bank</th>
        <th>Keterangan</th>

    </tr>
    </thead>
    <tbody>


    @php
        $no = 0;
    @endphp
    @foreach($history as $r)
        @php($no += 1)
        <tr data-index="">
            <td>
                {{$no}}
            </td>
            <td>
                {{$r->no_bukti_pembayaran}}
            </td>
            <td>
                {{\Carbon\Carbon::parse($r->tgl_pembayaran)->format('d-m-Y')}}
            </td>
            <td>
                {{$r->metode_pembayaran}}
            </td>
            <td>
                {{$r->jumlah}}
            </td>
            <td>
                {{$r->no_cek_giro}}
            </td>
            @if($r->tgl_pencairan_giro == null)
                <td>
                </td>
            @else
                <td>
                    {{\Carbon\Carbon::parse($r->tgl_pencairan_giro)->format('d-m-Y')}}
                </td>
            @endif
            <td>
                {{$r->bank}}
            </td>
            <td>
                {{$r->keterangan}}
            </td>
        </tr>

    @endforeach
    </tbody>
    @if($no == 0)
        <tfoot>
        <tr>
            <td colspan="9" style="text-align: center">Belum Ada Data Pembayaran</td>
        </tr>
        </tfoot>
    @endif
</table>