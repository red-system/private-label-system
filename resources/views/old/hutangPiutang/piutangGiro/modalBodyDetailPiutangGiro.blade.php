<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Piutang</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8">{{$piutang->no_piutang_giro}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Pencairan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($piutang->tgl_pencairan_piutang_giro)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Pelanggan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$piutang->dari_piutang_giro}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Faktur</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$piutang->no_faktur_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Faktur</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit"><b>{{\Carbon\Carbon::parse($piutang->tgl_faktur_penjualan)->format('d-m-Y')}}</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Bank</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$piutang->nama_bank_piutang_giro}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Keterangan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$piutang->keterangan_piutang_giro}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Jumlah Piutang</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$piutang->jumlah_piutang_giro}}</b></label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Status</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$piutang->status_piutang_giro}}</b></label>
</div>

<table class="table-pembayaran table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>No</th>
        <th>No Bukti Pembayaran</th>
        <th>Tanggal Pembayaran</th>
        <th>Metode Pembayaran</th>
        <th>Jumlah Pembayaran</th>
        <th>Nama Bank</th>
        <th>Keterangan</th>

    </tr>
    </thead>
    <tbody>


    @php
        $no = 0;
    @endphp
    @foreach($history as $r)
        @php($no += 1)
        <tr data-index="">
            <td>
                {{$no}}
            </td>
            <td>
                {{$r->no_bukti_pembayaran}}
            </td>
            <td>
                {{\Carbon\Carbon::parse($r->tgl_pembayaran)->format('d-m-Y')}}
            </td>
            <td>
                {{$r->metode_pembayaran}}
            </td>
            <td>
                {{$r->jumlah}}
            </td>
            <td>
                {{$r->bank}}
            </td>
            <td>
                {{$r->keterangan}}
            </td>
        </tr>

    @endforeach
    </tbody>
    @if($no == 0)
        <tfoot>
        <tr>
            <td colspan="7" style="text-align: center">Belum Ada Data Pembayaran</td>
        </tr>
        </tfoot>
    @endif
</table>