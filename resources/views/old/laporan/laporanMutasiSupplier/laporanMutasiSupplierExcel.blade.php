<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Mutasi Supplier " . $tanggal_dari.'-'.$tanggal_sampai  . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN MUTASI SUPPLIER<br>
            Wilayah :
            @if(is_array($nama_wilayah))
                @foreach($nama_wilayah as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_wilayah}}
            @endif<br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif<br>
            Tanggal {{$tanggal_dari}} S/d {{$tanggal_sampai}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Supplier</th>
        <th>Nama Supplier</th>
        <th>Saldo Awal</th>
        <th>Pembelian</th>
        <th>Bayar</th>
        <th>Retur</th>
        <th>Sisa</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_supplier }}</td>
            <td>{{ $r->supplier }}</td>
            <td align="right">{{ $r->saldo_awal }}</td>
            <td align="right">{{ $r->pembelian }}</td>
            <td align="right">{{ $r->bayar }}</td>
            <td align="right">{{ $r->retur }}</td>
            <td align="right">{{ $r->sisa }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="2"></td>
        <td class="item" align="right">{{$total_saldo_awal}}</td>
        <td class="item" align="right">{{$total_pembelian}}</td>
        <td class="item" align="right">{{$total_bayar}}</td>
        <td class="item" align="right">{{$total_retur}}</td>
        <td class="item" align="right">{{$total_sisa}}</td>
    </tr>
    </tfoot>
</table>