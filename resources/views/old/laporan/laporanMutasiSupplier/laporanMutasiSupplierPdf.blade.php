<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Mutasi Supplier</font><br>
                        Wilayah :
                        @if(is_array($nama_wilayah))
                            @foreach($nama_wilayah as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_wilayah}}
                        @endif<br>
                        Supplier :
                        @if(is_array($nama_supplier))
                            @foreach($nama_supplier as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_supplier}}
                        @endif<br>
                        Tanggal {{$tanggal_dari}} S/d {{$tanggal_sampai}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>

<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode</th>
                <th class="item">Nama</th>
                <th class="item">Saldo Awal</th>
                <th class="item">Pembelian</th>
                <th class="item">Bayar</th>
                <th class="item">Retur</th>
                <th class="item">Sisa</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem"><p class="itemtext">{{ $r->kode_supplier }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->supplier }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->saldo_awal }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->pembelian }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->bayar }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->retur }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->sisa }}</p></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"></td>
                <td class="item" align="right">{{$total_saldo_awal}}</td>
                <td class="item" align="right">{{$total_pembelian}}</td>
                <td class="item" align="right">{{$total_bayar}}</td>
                <td class="item" align="right">{{$total_retur}}</td>
                <td class="item" align="right">{{$total_sisa}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>