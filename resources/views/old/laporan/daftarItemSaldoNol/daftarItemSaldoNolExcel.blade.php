<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Stok " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR STOK<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>

@foreach($lokasi as $letak)
    @php
        $tempat = \app\Models\Widi\mLokasi::where('id', $letak->id_lokasi)->first();
    @endphp
    <span style="padding: 10px; line-height: normal; margin-bottom: 30px"><b>{{$tempat->lokasi}}</b></span>
    <br style="line-height: 30px">
    <table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
        <thead>
        <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Stok Akhir</th>
        </tr>
        </thead>
        <tbody>
        @foreach($stok as $r)
            @if($r->id_lokasi == $letak->id_lokasi)
                <tr>
                    <td>{{ $r->kode_barang }}</td>
                    <td>{{$r->nama_barang }}</td>
                    <td>{{Main::format_number( $r->jml_barang )}}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@endforeach