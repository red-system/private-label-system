<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }

    tr.noBorder td {
        border: 0 !important;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Daftar Daftar Saldo Nol</font><br>
                        Tanggal : {{\app\Helpers\Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<div id="invoice-bot">
    <br>
    <div id="table">
        @foreach($lokasi as $letak)
            <table width="100%" border="0">
                <thead>
                <tr class="noBorder">
                    <td colspan="3" style="text-align: left" class="item">Lokasi : {{$letak->lokasi}}</td>
                </tr>
                <tr class="tabletitle">
                    <th class="item">Kode Barang</th>
                    <th class="item">Nama Barang</th>
                    <th class="item">Stok Akhir</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $count = 0
                @endphp
                @foreach($stok as $r)
                    @if($r->id_lokasi == $letak->id_lokasi)
                        @php($count += 1)
                        <tr>
                            <td class="item">{{ $r->kode_barang }}</td>
                            <td class="item">{{$r->nama_barang }}</td>
                            <td class="item">{{Main::format_number( $r->jml_barang )}}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
                @if($count == 0)
                    <tfoot>
                    <tr>
                        <td class="item" style="text-align: center" colspan="3">
                            Data Kosong
                        </td>
                    </tr>
                    </tfoot>
            </table>
            @endif
            <br>
            <br>
            <br>
        @endforeach
    </div>
</div>