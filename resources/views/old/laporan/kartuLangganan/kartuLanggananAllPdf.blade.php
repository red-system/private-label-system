<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
    tr.noBorder td {
        border: 0 !important;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Daftar Kartu Langganan</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
                        Langganan : Semua Langganan
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
            @foreach($langganan as $l)
                <table width="100%" border="0">
                    <thead>
                    <tr class="noBorder">
                        <td colspan="10" style="text-align: left" class="item">Langganan : {{$l->langganan}}</td>
                    </tr>
                    <tr class="tabletitle">
                        <th class="item">No Faktur</th>
                        <th class="item">Tanggal</th>
                        <th class="item">Jatuh Tempo</th>
                        <th class="item">Tanggal Bayar</th>
                        <th class="item">Nilai</th>
                        <th class="item">Retur</th>
                        <th class="item">Pajak</th>
                        <th class="item">Bayar/DN/KN</th>
                        <th class="item">Sisa</th>
                        <th class="item">Keterangan</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $total_nilai = null;
                        $total_retur = 0;
                        $total_pajak = 0;
                        $total_terbayar = 0;
                        $total_sisa = 0;
                    @endphp
                    @foreach($list as $r)
                        @if($l->id_langganan == $r->id_langganan)
                            <tr>
                                <td class="item">{{$r->no_penjualan}}</td>
                                <td class="item">{{$r->tgl_penjualan}}</td>
                                <td class="item">{{$r->jatuh_tempo}}</td>
                                <td class="item">{{$r->tgl_terbayar}}</td>
                                <td class="item" style="text-align: right"   >{{Main::format_money($r->grand_total_penjualan)}}</td>
                                <td class="item" style="text-align: right"   >{{Main::format_money($r->total_retur)}}</td>
                                <td class="item" style="text-align: right"   >{{Main::format_money($r->pajak_nominal_penjualan)}}</td>
                                <td class="item" style="text-align: right"   >{{Main::format_money($r->terbayar)}}</td>
                                <td class="item" style="text-align: right"   >{{Main::format_money($r->sisa_piutang)}}</td>
                                <td class="item">{{$r->keterangan_penjualan}}</td>
                            </tr>
                            @php
                                $total_nilai = $total_nilai + $r->grand_total_penjualan;
                                $total_retur = $total_retur + $r->total_retur;
                                $total_pajak = $total_pajak + $r->pajak_nominal_penjualan;
                                $total_terbayar = $total_terbayar + $r->terbayar;
                                $total_sisa = $total_sisa + $r->sisa_piutang;
                            @endphp
                        @endif
                    @endforeach
                    </tbody>
                    @if($total_nilai != null)
                        <tfoot>
                        <tr>
                            <td class="item" colspan="4" style="text-align: center">Total</td>
                            <td class="item" colspan="1" style="text-align: right">{{Main::format_money($total_nilai)}}</td>
                            <td class="item" colspan="1" style="text-align: right">{{Main::format_money($total_retur)}}</td>
                            <td class="item" colspan="1" style="text-align: right">{{Main::format_money($total_pajak)}}</td>
                            <td class="item" colspan="1" style="text-align: right">{{Main::format_money($total_terbayar)}}</td>
                            <td class="item" colspan="1" style="text-align: right">{{Main::format_money($total_sisa)}}</td>
                            <td class="item" colspan="1" style="text-align: right"></td>
                        </tr>
                        </tfoot>
                    @else
                        <tfoot>
                        <tr>
                            <td class="item" colspan="10" style="text-align: center">Data Kosong</td>
                        </tr>
                        </tfoot>
                    @endif
                </table>
                <br>
                <br>
                <br>
            @endforeach
    </div>
</div>