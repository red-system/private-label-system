<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Kartu Langganan " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN KARTU LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
            Langganan : {{$langganan->langganan}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th>No Faktur</th>
        <th>Tanggal</th>
        <th>Jatuh Tempo</th>
        <th>Tanggal Bayar</th>
        <th>Nilai</th>
        <th>Retur</th>
        <th>Pajak</th>
        <th>Bayar/DN/KN</th>
        <th>Sisa</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_nilai = null;
        $total_retur = 0;
        $total_pajak = 0;
        $total_terbayar = 0;
        $total_sisa = 0;

    @endphp
    @foreach($list as $r)
        <tr>
            <td width="10%">{{$r->no_penjualan}}</td>
            <td width="10%">{{$r->tgl_penjualan}}</td>
            <td width="10%">{{$r->jatuh_tempo}}</td>
            <td width="10%">{{$r->tgl_terbayar}}</td>
            <td width="10%" style="text-align: right"   >{{Main::format_money($r->grand_total_penjualan)}}</td>
            <td width="10%" style="text-align: right"   >{{Main::format_money($r->total_retur)}}</td>
            <td width="10%" style="text-align: right"   >{{Main::format_money($r->pajak_nominal_penjualan)}}</td>
            <td width="10%" style="text-align: right"   >{{Main::format_money($r->terbayar)}}</td>
            <td width="10%" style="text-align: right"   >{{Main::format_money($r->sisa_piutang)}}</td>
            <td width="10%">{{$r->keterangan_penjualan}}</td>
        </tr>
        @php
            $total_nilai = $total_nilai + $r->grand_total_penjualan;
            $total_retur = $total_retur + $r->total_retur;
            $total_pajak = $total_pajak + $r->pajak_nominal_penjualan;
            $total_terbayar = $total_terbayar + $r->terbayar;
            $total_sisa = $total_sisa + $r->sisa_piutang;
        @endphp
    @endforeach
    </tbody>
    @if($total_nilai != null)
        <tfoot>
        <tr>
            <td colspan="4">Total</td>
            <td colspan="1" style="text-align: right">{{Main::format_money($total_nilai)}}</td>
            <td colspan="1" style="text-align: right">{{Main::format_money($total_retur)}}</td>
            <td colspan="1" style="text-align: right">{{Main::format_money($total_pajak)}}</td>
            <td colspan="1" style="text-align: right">{{Main::format_money($total_terbayar)}}</td>
            <td colspan="1" style="text-align: right">{{Main::format_money($total_sisa)}}</td>
            <td colspan="1" style="text-align: right"></td>
        </tr>
        </tfoot>
    @else
        <tfoot>
        <tr>
            <td colspan="10">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>