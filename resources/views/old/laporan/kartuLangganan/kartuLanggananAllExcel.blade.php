<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Kartu Langganan  " . $date_start ." sampai ". $date_end.".xls");
?>
<style type="text/css">
    tr.noBorder td {
        border: 0 !important;
    }
</style>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN KARTU LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
            Langganan : Semua Langganan
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
@foreach($langganan as $l)
    <table>
        <tbody>
        <tr>
            <td colspan="10" style="text-align: left">Langganan : {{$l->langganan}}</td>
        </tr>
        </tbody>
    </table>
    <table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
        <thead>
        <tr>
            <th width="10%">No Faktur</th>
            <th width="10%">Tanggal</th>
            <th width="10%">Jatuh Tempo</th>
            <th width="10%">Tanggal Bayar</th>
            <th width="10%">Nilai</th>
            <th width="10%">Retur</th>
            <th width="10%">Pajak</th>
            <th width="10%">Bayar/DN/KN</th>
            <th width="10%">Sisa</th>
            <th width="10%">Keterangan</th>
        </tr>
        </thead>
        <tbody>
        @php
            $total_nilai = null;
            $total_retur = 0;
            $total_pajak = 0;
            $total_terbayar = 0;
            $total_sisa = 0;

        @endphp
        @foreach($list as $r)
            @if($l->id_langganan == $r->id_langganan)
                <tr>
                    <td width="10%">{{$r->no_penjualan}}</td>
                    <td width="10%">{{$r->tgl_penjualan}}</td>
                    <td width="10%">{{$r->jatuh_tempo}}</td>
                    <td width="10%">{{$r->tgl_terbayar}}</td>
                    <td width="10%" style="text-align: right"   >{{Main::format_money($r->grand_total_penjualan)}}</td>
                    <td width="10%" style="text-align: right"   >{{Main::format_money($r->total_retur)}}</td>
                    <td width="10%" style="text-align: right"   >{{Main::format_money($r->pajak_nominal_penjualan)}}</td>
                    <td width="10%" style="text-align: right"   >{{Main::format_money($r->terbayar)}}</td>
                    <td width="10%" style="text-align: right"   >{{Main::format_money($r->sisa_piutang)}}</td>
                    <td width="10%">{{$r->keterangan_penjualan}}</td>
                </tr>
                @php
                    $total_nilai = $total_nilai + $r->grand_total_penjualan;
                    $total_retur = $total_retur + $r->total_retur;
                    $total_pajak = $total_pajak + $r->pajak_nominal_penjualan;
                    $total_terbayar = $total_terbayar + $r->terbayar;
                    $total_sisa = $total_sisa + $r->sisa_piutang;
                @endphp
            @endif
        @endforeach
        </tbody>
        @if($total_nilai != null)
            <tfoot>
            <tr>
                <td colspan="4">Total</td>
                <td colspan="1" style="text-align: right">{{Main::format_money($total_nilai)}}</td>
                <td colspan="1" style="text-align: right">{{Main::format_money($total_retur)}}</td>
                <td colspan="1" style="text-align: right">{{Main::format_money($total_pajak)}}</td>
                <td colspan="1" style="text-align: right">{{Main::format_money($total_terbayar)}}</td>
                <td colspan="1" style="text-align: right">{{Main::format_money($total_sisa)}}</td>
                <td colspan="1" style="text-align: right"></td>
            </tr>
            </tfoot>
        @else
            <tfoot>
            <tr>
                <td colspan="10">Data Kosong</td>
            </tr>
            </tfoot>
        @endif
    </table>
    <br>
@endforeach