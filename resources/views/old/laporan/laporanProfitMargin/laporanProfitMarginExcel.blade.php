<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Profit Margin " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN PROFIT MARGIN<br>
            Tanggal : {{ Carbon\Carbon::parse(now())->format('d-m-Y') }}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="10%">Kode Barang</th>
        <th width="10%">Nama Barang</th>
        <th width="10%">HPP</th>
        <th width="10%">Harga Jual</th>
        <th width="10%">Margin</th>
    </tr>
    </thead>
</table>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <tbody>
    @foreach($barang as $r)
        <tr>
            <td width="10%">{{ $r->kode_barang }}</td>
            <td width="10%">{{$r->nama_barang }}</td>
            <td width="10%" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->hpp )}}</td>
            <td width="10%" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->harga_jual_barang )}}</td>
            <td width="10%" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->margin )}}</td>
        </tr>
    @endforeach
    </tbody>
    @if($count == 0)
        <tr>
            <td class="tableitem" style="text-align: center" colspan="4">
                <p class="itemtext">Data Kosong</p>
            </td>
        </tr>
    @endif
</table>