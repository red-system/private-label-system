<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Profit Margin</font><br>
                        Tanggal : {{ Carbon\Carbon::parse(now())->format('d-m-Y') }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">HPP</th>
                <th class="item">Harga Jual</th>
                <th class="item">Margin</th>
            </tr>
            </thead>
            <tbody>
            @foreach($barang as $r)
                <tr>
                    <td class="item">{{ $r->kode_barang }}</td>
                    <td class="item">{{$r->nama_barang }}</td>
                    <td class="item" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->hpp )}}</td>
                    <td class="item" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->harga_jual_barang )}}</td>
                    <td class="item" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->margin )}}</td>
                </tr>
            @endforeach
            </tbody>
            @if($count == 0)
                <tr>
                    <td class="tableitem" style="text-align: center" colspan="4">
                        <p class="itemtext">Data Kosong</p>
                    </td>
                </tr>
            @endif
        </table>
    </div>
</div>