@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('laporanProfitMarginList')}}">
        <div id="table_coloumn" style="display: none">{{$view}} </div>
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                            <h3 class="m-portlet__head-text">
                                Cetak Data
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot text-center">
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                        <a href="{{ route('laporanProfitMarginPdf') }}"
                           class="btn btn-danger akses-pdf" target="_blank">
                            <i class="la la-file-pdf-o"></i> Print PDF
                        </a>
                        <a href="{{ route('laporanProfitMarginExcel') }}"
                           class="btn btn-success akses-excel" target="_blank">
                            <i class="la la-file-excel-o"></i> Print Excel
                        </a>
                    </div>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="datatable table table-striped- table-bordered table-hover table-checkable
                            datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>HPP</th>
                            <th>Harga Jual</th>
                            <th>Margin</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
