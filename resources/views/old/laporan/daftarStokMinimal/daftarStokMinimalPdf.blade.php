
<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
    tr.noBorder td {
        border: 0 !important;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Daftar Stok Minimal</font><br>
                        Tanggal : {{\app\Helpers\Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        @if($id_lokasi != 'all')
            @php
                $jumlah_total = 0;
            @endphp
                <table>
                    <thead>
                    <tr class="tabletitle">
                        <th class="item">Kode Barang</th>
                        <th class="item">Nama Barang</th>
                        <th class="item">Saldo</th>
                        <th class="item">Min Stok</th>
                        <th class="item">Min Reorder</th>
                        <th class="item">Hargan Beli</th>
                        <th class="item">Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stok as $r)
                        @if($lokasi->id == $r->id_lokasi)
                            @php
                                $jumlah = $r->harga_beli_terakhir_barang*$r->minimal_stok;
                            @endphp
                            <tr>
                                <td class="item">{{ $r->kode_barang }}</td>
                                <td class="item">{{$r->nama_barang }}</td>
                                <td class="item">{{$r->jml_barang }}.{{$r->satuan}}</td>
                                <td class="item">{{$r->minimal_stok }}.{{$r->satuan}}</td>
                                <td class="item">{{$r->minimal_stok }}.{{$r->satuan}}</td>
                                <td class="item">{{$r->harga_beli_terakhir_barang }}</td>
                                <td class="item" style="text-align: right">{{Main::format_number( $jumlah )}}</td>
                            </tr>
                            @php
                                $jumlah_total = $jumlah_total + $jumlah;
                            @endphp
                        @endif
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="6" style="text-align: center">Total</td>
                        <td class="item" style="text-align: right">
                            {{Main::format_money($jumlah_total)}}
                        </td>
                    </tr>
                    </tfoot>
                </table>
                <br>
                <br>
        @else

            @foreach($lokasi as $letak)
                @php
                    $jumlah_total = 0;
                @endphp
                <table width="100%" border="0">
                    <thead>
                    <tr class="noBorder">
                        <td colspan="7" style="text-align: left" class="item">Lokasi : {{$letak->lokasi}}</td>
                    </tr>
                    <tr class="tabletitle">
                        <th class="item">Kode Barang</th>
                        <th class="item">Nama Barang</th>
                        <th class="item">Saldo</th>
                        <th class="item">Min Stok</th>
                        <th class="item">Min Reorder</th>
                        <th class="item">Hargan Beli</th>
                        <th class="item">Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $count = 0
                    @endphp
                    @foreach($stok as $r)
                        @if($letak->id == $r->id_lokasi)
                            @php
                                $count = $count + 1;
                                    $jumlah = $r->harga_beli_terakhir_barang * $r->minimal_stok;
                            @endphp
                            <tr>
                                <td class="item">{{ $r->kode_barang }}</td>
                                <td class="item">{{$r->nama_barang }}</td>
                                <td class="item">{{$r->jml_barang .' '.$r->satuan}}</td>
                                @if(!$r->minimal_stok)
                                    <td class="item">{{'0 '.$r->satuan}}</td>
                                    <td class="item">{{'0 '.$r->satuan}}</td>
                                @else
                                    <td class="item">{{$r->minimal_stok.' '.$r->satuan}}</td>
                                    <td class="item">{{$r->minimal_stok.' '.$r->satuan}}</td>
                                @endif
                                <td class="item">{{Main::format_money($r->harga_beli_terakhir_barang) }}</td>
                                <td class="item" style="text-align: right">{{Main::format_money( $jumlah )}}</td>
                            </tr>
                            @php
                                $jumlah_total = $jumlah_total + $jumlah;
                            @endphp
                        @endif
                    @endforeach
                    @if($count == 0)
                        <tr>
                            <td class="item" colspan="7">Data Kosong</td>
                        </tr>
                    @endif
                    </tbody>
                    @if($count > 0)
                        <tfoot>
                        <tr>
                            <td class="item" colspan="6">Total</td>
                            <td class="item" style="text-align: right">
                                {{Main::format_money($jumlah_total)}}
                            </td>
                        </tr>
                        </tfoot>
                </table>
                @endif
                <br>
                <br>
                <br>
            @endforeach
        @endif
    </div>
</div>