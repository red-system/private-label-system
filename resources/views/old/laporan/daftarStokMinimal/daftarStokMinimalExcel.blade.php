<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Harga Barang " . date('d-m-Y') . ".xls");
?>
<style type="text/css">
    tr.noBorder th {
        border: 0 !important;
    }
</style>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN STOK MINIMAL<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
@if($id_lokasi != 'all')
    @php
        $jumlah_total = 0;
    @endphp

    <table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
        <thead>
        <tr class="noBorder">
            <th colspan="7">Lokasi : {{$lokasi->lokasi}}</th>
        </tr>
        </thead>
    </table>
    <table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
        <thead>
        <tr>
            <th width="10%">Kode Barang</th>
            <th width="10%">Nama Barang</th>
            <th width="10%">Saldo</th>
            <th width="10%">Min Stok</th>
            <th width="10%">Min Reorder</th>
            <th width="10%">Hargan Beli</th>
            <th width="10%">Jumlah</th>
        </tr>
        </thead>
        <tbody>

        @foreach($stok as $r)
            @if($lokasi->id == $r->id_lokasi)
                @php
                    $jumlah = $r->harga_beli_terakhir_barang*$r->minimal_stok;
                @endphp
                <tr>
                    <td width="10%" style="text-align: left">{{ $r->kode_barang }}</td>
                    <td width="10%" style="text-align: left">{{$r->nama_barang }}</td>
                    <td width="10%" style="text-align: left">{{$r->jml_barang }}.{{$r->satuan}}</td>
                    <td width="10%" style="text-align: left">{{$r->minimal_stok }}.{{$r->satuan}}</td>
                    <td width="10%" style="text-align: left">{{$r->minimal_stok }}.{{$r->satuan}}</td>
                    <td width="10%" style="text-align: left">{{$r->harga_beli_terakhir_barang }}</td>
                    <td width="10%" style="text-align: right">{{Main::format_number( $jumlah )}}</td>
                </tr>
                @php
                    $jumlah_total = $jumlah_total + $jumlah;
                @endphp
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6"></td>
            <td style="text-align: right">
                {{Main::format_money($jumlah_total)}}
            </td>
        </tr>
        </tfoot>
    </table>
@else
    @foreach($lokasi as $letak)
        @php
            $jumlah_total = 0;
        @endphp

        <table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
            <thead>
            <tr>
                <th width="15%">Lokasi : {{$letak->lokasi}}</th>
                <th width="10%"></th>
                <th width="10%"></th>
                <th width="10%"></th>
                <th width="10%"></th>
                <th width="10%"></th>
                <th width="10%"></th>
            </tr>
            </thead>
        </table>
        <table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
            <thead>
            <tr>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Saldo</th>
                <th>Min Stok</th>
                <th>Min Reorder</th>
                <th>Hargan Beli</th>
                <th>Jumlah</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tbody>
            @php
                $count = 0
            @endphp
            @foreach($stok as $r)
                @if($letak->id == $r->id_lokasi)
                    @php
                        $count = $count + 1;
                            $jumlah = $r->harga_beli_terakhir_barang * $r->minimal_stok;
                    @endphp
                    <tr>
                        <td style="text-align: left">{{ $r->kode_barang }}</td>
                        <td style="text-align: left">{{$r->nama_barang }}</td>
                        <td style="text-align: left">{{$r->jml_barang .' '.$r->satuan}}</td>
                        @if(!$r->minimal_stok)
                            <td style="text-align: left">{{'0 '.$r->satuan}}</td>
                        @else
                            <td style="text-align: left">{{$r->minimal_stok.' '.$r->satuan}}</td>
                        @endif

                        @if(!$r->minimal_stok)
                            <td style="text-align: left">{{'0 '.$r->satuan}}</td>
                        @else
                            <td style="text-align: left">{{$r->minimal_stok.' '.$r->satuan}}</td>
                        @endif
                        <td style="text-align: left">{{Main::format_money($r->harga_beli_terakhir_barang) }}</td>
                        <td style="text-align: right">{{Main::format_money( $jumlah )}}</td>
                    </tr>
                    @php
                        $jumlah_total = $jumlah_total + $jumlah;
                    @endphp
                @endif
            @endforeach
            @if($count == 0)
                <tr>
                    <td colspan="7">Data Kosong</td>
                </tr>
            @endif
            </tbody>
            @if($count > 0)
                <tfoot>
                <tr>
                    <td colspan="6"></td>
                    <td style="text-align: right">
                        {{Main::format_money($jumlah_total)}}
                    </td>
                </tr>
                </tfoot>
        </table>
        @endif
        <br>
        <br>
        <br>
    @endforeach
@endif