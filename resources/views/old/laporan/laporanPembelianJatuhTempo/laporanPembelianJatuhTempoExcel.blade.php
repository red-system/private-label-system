<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Pembelian Jatuh Tempo ". $tanggal_filter  .".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td></td>
        <td colspan="3">
            LAPORAN DAFTAR STOK OUT<br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif
            <br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif<br>
            Tanggal : {{$tanggal_filter}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Tgl. Jatuh Tempo</th>
        <th>Hari</th>
        <th>No. Faktur</th>
        <th>Supplier</th>
        <th>Jumlah</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->jatuh_tempo }}</td>
            <td align="center">{{ $r->hari }} Hari</td>
            <td>{{ $r->no_faktur }}</td>
            <td>{{ $r->supplier }}</td>
            <td align="right">{{ $r->jumlah }}</td>
        </tr>
    @endforeach
    </tbody>
</table>