<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Omzet Salesman Per Faktur " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN OMZET SALESMAN PER FAKTUR<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br>
<br>
<table width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="20%">Salesman</th>
        <th width="15%">No Faktur</th>
        <th width="15%">Tanggal</th>
        <th width="15%">Grand Total</th>
    </tr>
    </thead>
    <tbody>
    @php
        $count = 0;
    @endphp
    @foreach($list as $r)
        @php
            $count += 1;
        @endphp
        <tr>
            <td width="20%" style="text-align: left">{{ $r->salesman }}</td>
            <td width="15%">{{ $r->no_penjualan }}</td>
            <td width="15%">{{$r->tgl_penjualan }}</td>
            <td width="15%" style="text-align: right">{{$r->grand_total_penjualan }}</td>
        </tr>
    @endforeach
    </tbody>
    @if($count == 0)
        <tfoot>
        <tr>
            <td colspan="4"> Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>