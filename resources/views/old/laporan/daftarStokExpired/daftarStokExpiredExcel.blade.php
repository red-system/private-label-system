<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Stok Expired " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR STOK EXPIRED<br>
            Expire Tgl. : {{$tanggal_expired}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Expire</th>
        <th>Lokasi</th>
        <th>Saldo</th>
        <th>Tot. Harga Beli</th>
        <th>Tot. Harga Jual</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_barang }}</td>
            <td>{{ $r->nama_barang }}</td>
            <td>{{ $r->date_expired }}</td>
            <td>{{ $r->lokasi }}</td>
            <td align="right">{{ $r->jml_barang }}</td>
            <td align="right">{{ $r->harga_beli_terakhir_barang }}</td>
            <td align="right">{{ $r->harga_jual_barang }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="4"></td>
        <td class="item" align="right">{{$total_saldo}}</td>
        <td class="item" align="right">{{$total_beli_barang}}</td>
        <td class="item" align="right">{{$total_jual_barang}}</td>
    </tr>
    </tfoot>
</table>