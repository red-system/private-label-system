@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>

@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('laporanSaldoStokList', $params)}}">
        <div id="table_coloumn" style="display: none">{{$detail}} </div>
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <div class="form-group m-form__group row">
                                    <label for="supplier">Lokasi</label>
                                    <div class="input-daterange input-group">
                                        <select class="form-control m-input m-select2" name="id_lokasi" id="lokasi">
                                            <option value="all">Semua Lokasi</option>
                                            @foreach($lokasi as $r)
                                                <option value="{{ $r->id }}" {{ $id_lokasi == $r->id ? 'selected':'' }}>{{$r->lokasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <div class="form-group m-form__group row">
                                    <label for="supplier">Golongan</label>
                                    <div class="input-daterange input-group">
                                        <select class="form-control m-input m-select2" name="id_golongan" id="golongan">
                                            <option value="all">Semua Golongan</option>
                                            @foreach($golongan as $r)
                                                <option value="{{ $r->id }}" {{ $id_golongan == $r->id ? 'selected':'' }}>{{$r->golongan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('laporanSaldoStokPdf', $params) }}"
                               class="btn btn-danger akses-pdf" target="_blank">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('laporanSaldoStokExcel', $params) }}"
                               class="btn btn-success akses-excel" target="_blank">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="datatable table table-striped- table-bordered table-hover table-checkable
                            datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Golongan</th>
                            <th>Lokasi</th>
                            <th>Saldo</th>
                            <th>Harga Beli</th>
                            <th>Harga jual</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
