<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Saldo Stok " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN SALDO STOK<br>
            Tanggal : {{ Carbon\Carbon::parse(now())->format('d-m-Y') }}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>

<table width="100%" class="table-data" border="1">
    <thead>
    <tr>
        <th width="60">Kode Barang</th>
        <th width="90">Nama Barang</th>
        <th width="60">Golongan</th>
        <th width="70">Lokasi</th>
        <th width="70">Saldo</th>
        <th width="60">HPP</th>
        <th width="60">Hrg Beli</th>
        <th width="60">Hrg Jual</th>
        <th width="90">Total Hrg Beli</th>
        <th width="90">Total Hrg Jual</th>
    </tr>
    </thead>


@foreach($barang as $b)
    @php
        $total_stok = 0;
    @endphp
        <tbody>
        @foreach($barang as $r)
            @if($b->id == $r->id)
                <tr>
                    <td width="60" style="text-align: left; margin: auto">{{ $r->kode_barang }}</td>
                    <td width="90" style="text-align: left; margin: auto">{{$r->nama_barang }}</td>
                    <td width="60">{{$r->golongan }}</td>
                    <td width="70">
                        @php
                            $count = 0;
                        @endphp
                        @foreach($stok as $s)
                            @if($r->id == $s->id_barang)
                                @php
                                    $count = $count + 1;
                                @endphp
                                {{$s->lokasi}} <br>
                            @endif
                        @endforeach
                        @if($count == 0)
                            {{$lokasi}}
                        @endif
                    </td>
                    <td width="70">
                        <div style="text-align: right">
                            @foreach($stok as $s)
                                @if($r->id == $s->id_barang)
                                    {{Main::format_number_system( $s->jml_barang ).' '.$s->satuan}}<br>
                                    @php
                                        $total_stok = $total_stok + $s->jml_barang;
                                        $satuan = $s->satuan;
                                    @endphp
                                @endif
                            @endforeach
                            @if($count == 0)
                                0
                            @endif
                        </div>
                    </td>
                    @php
                        $total_beli = $total_stok*$r->harga_beli_terakhir_barang;
                        $total_jual = $total_stok*$r->harga_jual_barang;
                    @endphp
                    <td width="60">{{Main::format_money( $r->hpp )}}</td>
                    <td width="60">{{Main::format_money( $r->harga_beli_terakhir_barang )}}</td>
                    <td width="60">{{Main::format_money( $r->harga_jual_barang )}}</td>
                    <td width="90">
                        @foreach($stok as $st)
                            @if($r->id == $st->id_barang)
                                @php
                                    $harga_beli = $r->harga_beli_terakhir_barang * $st->jml_barang;
                                @endphp
                                <div style="text-align: right">
                                    {{Main::format_money( $harga_beli )}}<br>
                                </div>
                            @endif
                        @endforeach
                        @if($count == 0)
                            <div style="text-align: right">0</div>
                        @endif
                    </td>
                    <td style="text-align: right" width="90">
                        @foreach($stok as $st)
                            @if($r->id == $st->id_barang)
                                @php
                                    $harga_jual = $r->harga_jual_barang * $st->jml_barang;
                                @endphp
                                {{Main::format_money( $harga_jual )}}<br>
                            @endif
                        @endforeach
                        @if($count == 0)
                            0
                        @endif
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4" style="text-align: center; font-weight: bold">Total</td>
            <td colspan="1" style="text-align: right; font-weight: bold">

                {{Main::format_number_system( $total_stok ).' '.$satuan}}</td>
            <td colspan="3"></td>
            <td colspan="1" style="text-align: right; font-weight: bold">

                {{Main::format_money( $total_beli )}}</td>
            <td style="text-align: right; font-weight: bold">

                {{Main::format_money( $total_jual )}}</td>
        </tr>
        </tfoot>
@endforeach
</table>