<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Pembayaran Tepat Waktu</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">No Faktur</th>
                <th class="item">Jatuh Tempo</th>
                <th class="item">Tanggal Pelunasan</th>
                <th class="item">Langganan</th>
                <th class="item">Total</th>
                <th class="item">Terbayar</th>
            </tr>
            </thead>
            <tbody>
            @php
                $a = null;
            @endphp
            @foreach($list as $r)
                <tr>
                    <td class="item">{{ $r->no_faktur_penjualan }}</td>
                    <td class="item">{{$r->jatuh_tempo }}</td>
                    <td class="item">{{$r->updated_at }}</td>
                    <td class="item">{{$r->langganan }}</td>
                    <td class="item" style="text-align: right">{{$r->total_piutang }}</td>
                    <td class="item" style="text-align: right">{{$r->terbayar }}</td>
                </tr>
                @php
                    $a = $a + 1;
                @endphp
            @endforeach
            </tbody>
            @if($a == null)
                <tfoot>
                <tr>
                    <td colspan="6" class="item" style="text-align: center">Data Kosong</td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>