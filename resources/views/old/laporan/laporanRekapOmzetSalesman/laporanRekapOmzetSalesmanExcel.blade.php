<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Rekap Omzet Salesman " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN REKAP OMZET SALESMAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>

<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="15%" style="text-align: left">Salesman</th>
        <th width="10%" style="text-align: right">Qty</th>
        <th width="15%" style="text-align: right">Total</th>
        <th width="15%" style="text-align: right">Dibayar Tepat Waktu</th>
    </tr>
    </thead>
    <tbody>
    @php
        $count = 0;
    @endphp
    @foreach($list as $r)
        @php
            $count += 1;
        @endphp
        <tr>
            <td width="15%" style="text-align: left">{{ $r->salesman }}</td>
            <td width="10%" style="text-align: right">{{ $r->qty }}</td>
            <td width="15%" style="text-align: right">{{$r->total }}</td>
            <td width="15%" style="text-align: right">{{$r->dibayar_tepat_waktu }}</td>
        </tr>
    @endforeach
    </tbody>
    @if($count == 0)
        <tfoot>
        <tr>
            <td colspan="4"> Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>