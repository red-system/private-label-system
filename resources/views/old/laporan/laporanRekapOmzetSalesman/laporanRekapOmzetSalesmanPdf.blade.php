<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Rekap Omzet Salesman</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Salesman</th>
                <th class="item">Qty</th>
                <th class="item">Total</th>
                <th class="item">Dibayar Tepat Waktu</th>
            </tr>
            </thead>
            <tbody>
            @php
                $a = null;
            @endphp
            @foreach($list as $r)
                <tr>
                    <td class="item" style="text-align: left">{{ $r->salesman }}</td>
                    <td class="item" style="text-align: right">{{ $r->qty }}</td>
                    <td class="item" style="text-align: right">{{$r->total }}</td>
                    <td class="item" style="text-align: right">{{$r->dibayar_tepat_waktu }}</td>
                </tr>
                @php
                    $a = $a + 1;
                @endphp
            @endforeach
            </tbody>
            @if($a == null)
                <tfoot>
                <tr>
                    <td colspan="4" class="item" style="text-align: center">Data Kosong</td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>