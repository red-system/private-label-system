<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Daftar Stok Akan Expired</font><br>
                        Akan Expire Dalam {{$expired_dalam}} Bulan
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br>
    <br>
    <br>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">Expire</th>
                <th class="item">Lokasi</th>
                <th class="item">Saldo</th>
                <th class="item">Tot. Harga Beli</th>
                <th class="item">Tot. Harga Jual</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem"><p class="itemtext">{{ $r->kode_barang }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->nama_barang }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->date_expired }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->lokasi }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->jml_barang }}</p></td>
                    <td class="tableitem" align="right"><p
                                class="itemtext">{{ $r->harga_beli_terakhir_barang }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->harga_jual_barang }}</p></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4"></td>
                <td class="item" align="right">{{$total_saldo}}</td>
                <td class="item" align="right">{{$total_beli_barang}}</td>
                <td class="item" align="right">{{$total_jual_barang}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>