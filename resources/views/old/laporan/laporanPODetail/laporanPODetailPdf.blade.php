<style type="text/css">
    .table-data {
        /*border-left: 0.01em solid #ccc;*/
        /*border-right: 0;*/
        /*border-top: 0.01em solid #ccc;*/
        /*border-bottom: 0;*/
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        /*border-left: 0;*/
        /*border-right: 0.01em solid #ccc;*/
        /*border-top: 0;*/
        /*border-bottom: 0.01em solid #ccc;*/
        padding: 2px 4px;
        text-align: center !important;
        font-size: 12px;
    }
</style>
<table width="100%">
    <tbody>
    <tr>
        <td width="90">
            <img src="{{ base_path() }}/public/images/logo.png" width="90">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            Laporan Detail PO<br>
            Tanggal : {{$tanggal_start}} S/d {{$tanggal_end}} <br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif
            <br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif
        </td>
    </tr>
    </tbody>
</table>
<br/>
@foreach($list as $r)
    <table style="margin-top: 15px; margin-bottom: 10px" width="100%" class="table-data">
        {{--        <tb></tb>--}}
        <thead>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <th>No Faktur</th>
            <th>Tanggal</th>
            <th>Lokasi</th>
            <th>Supplier</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $r->no_faktur }}</td>
            <td>{{$r->tanggal }}</td>
            <td>{{$r->lokasi }}</td>
            <td>{{$r->supplier }}</td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tbody>

        <thead>
        <tr>
            <th><b>Kode Barang</b></th>
            <th><b>Nama Barang</b></th>
            <th><b>Qty</b></th>
            <th><b>Harga</b></th>
            <th><b>Jumlah</b></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($item as $i)
            @if($i->id_pembelian == $r->id)
                <tr>
                    <td>{{$i->kode_barang}}</td>
                    <td>{{$i->nama_barang}}</td>
                    <td>{{$i->qty}} {{$i->satuan}}</td>
                    <td>{{$i->harga}}</td>
                    <td>{{$i->sub_total}}</td>
                    <td></td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td><b>Diskon ({{$r->discount_persen_po}}%)</b></td>
            <td>{{$r->discount_nominal_po}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><b>PPN</b></td>
            <td>{{$r->ppn}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><b>Ongkos Kirim</b></td>
            <td>{{$r->ongkos_kirim_po}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><b>Grand Total</b></td>
            <td>{{$r->grand_total}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tfoot>
    </table>
@endforeach
