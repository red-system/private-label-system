<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan PO Detail ". $tanggal_start  ." S/d ".$tanggal_end. ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DETAIL PO<br>
            Tanggal : {{$tanggal_start}} S/d {{$tanggal_end}} <br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif
            <br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif
        </td>
    </tr>
    </tbody>
</table>
<br/>
@foreach($list as $r)
    <table style="margin-top: 15px; margin-bottom: 10px" width="100%" class="table-data">
        {{--        <tb></tb>--}}
        <thead>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <th>No Faktur</th>
            <th>Tanggal</th>
            <th>Lokasi</th>
            <th>Supplier</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td align="center">{{ $r->no_faktur }}</td>
            <td align="center">{{$r->tanggal }}</td>
            <td align="center">{{$r->lokasi }}</td>
            <td align="center">{{$r->supplier }}</td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tbody>

        <thead>
        <tr>
            <th><b>Kode Barang</b></th>
            <th><b>Nama Barang</b></th>
            <th><b>Qty</b></th>
            <th><b>Harga</b></th>
            <th><b>Jumlah</b></th>
        </tr>
        </thead>
        <tbody>
        @foreach($item as $i)
            @if($i->id_pembelian == $r->id)
                <tr>
                    <td align="center">{{$i->kode_barang}}</td>
                    <td align="center">{{$i->nama_barang}}</td>
                    <td align="center">{{$i->qty}} {{$i->satuan}}</td>
                    <td align="center">{{$i->harga}}</td>
                    <td align="center">{{$i->sub_total}}</td>
                    <td align="center"></td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td><b>Diskon ({{$r->discount_persen_po}}%)</b></td>
            <td align="center">{{$r->discount_nominal_po}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><b>PPN</b></td>
            <td align="center">{{$r->ppn}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><b>Ongkos Kirim</b></td>
            <td align="center">{{$r->ongkos_kirim_po}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><b>Grand Total</b></td>
            <td align="center">{{$r->grand_total}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tfoot>
    </table>
@endforeach