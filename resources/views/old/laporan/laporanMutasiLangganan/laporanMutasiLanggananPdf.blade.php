<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Mutasi Langganan</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}
                        <br>Lokasi : {{$lokasi}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Langganan</th>
                <th class="item">Nama Langganan</th>
                <th class="item">Saldo Awal</th>
                <th class="item">Penjualan</th>
                <th class="item">Total Bayar</th>
                <th class="item">Total Retur</th>
                <th class="item">Saldo Akhir Rp</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr>
                    <td class="item">{{ $r->kode_langganan }}</td>
                    <td class="item">{{$r->langganan }}</td>
                    <td class="item" style="text-align: right">{{$r->saldo_awal }}</td>
                    <td class="item" style="text-align: right">{{$r->penjualan}}</td>
                    <td class="item" style="text-align: right">{{$r->bayar}}</td>
                    <td class="item" style="text-align: right">{{$r->retur }}</td>
                    <td class="item" style="text-align: right">{{$r->sisa}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>