<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Mutasi Langganan " . $date_start . " sampai " . $date_end . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN MUTASI LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
            <br>Lokasi : {{$lokasi}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="10%">Kode Langganan</th>
        <th width="15%">Nama Langganan</th>
        <th width="10%">Saldo Awal</th>
        <th width="10%">Penjualan</th>
        <th width="9%">Total Bayar</th>
        <th width="9%">Total Retur</th>
        <th width="10%">Saldo Akhir Rp</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td width="10%">{{ $r->kode_langganan }}</td>
            <td width="15%">{{$r->langganan }}</td>
            <td width="10%" style="text-align: right">{{$r->saldo_awal }}</td>
            <td width="10%" style="text-align: right">{{$r->penjualan}}</td>
            <td width="9%" style="text-align: right">{{$r->bayar}}</td>
            <td width="9%" style="text-align: right">{{$r->retur }}</td>
            <td width="10%" style="text-align: right">{{$r->sisa}}</td>
        </tr>
    @endforeach
    </tbody>
</table>