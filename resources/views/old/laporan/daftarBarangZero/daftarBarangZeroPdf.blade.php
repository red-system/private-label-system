<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Daftar Barang Nol</font><br>
                        Tanggal : {{\app\Helpers\Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">Satuan</th>
                <th class="item">Harga Jual</th>
            </tr>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem" align="left"><p class="itemtext">{{ $r->kode_barang }}</p></td>
                    <td class="tableitem" align="left"><p class="itemtext">{{ $r->nama_barang }}</p></td>
                    <td class="tableitem" align="left"><p class="itemtext">{{ $r->satuan }}</p></td>
                    <td class="tableitem" align="left"><p class="itemtext">{{ $r->harga_jual_barang }}</p></td>
                </tr>
            @endforeach
            @if($count == 0)
                <tr>
                    <td class="tableitem" style="text-align: center" colspan="4">
                        <p class="itemtext">Data Kosong</p>
                    </td>
                </tr>
            @endif
        </table>
    </div>
</div>