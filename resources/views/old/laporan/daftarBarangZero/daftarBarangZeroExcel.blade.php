<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Barang Zero " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR BARANG ZERO<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Satuan</th>
        <th>Harga Jual</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_barang }}</td>
            <td align="left">{{ $r->nama_barang }}</td>
            <td align="left">{{ $r->satuan }}</td>
            <td align="left">{{ $r->harga_jual_barang }}</td>
        </tr>
    @endforeach
    </tbody>
    @if($count == 0)
        <tfoot>
        <tr>
            <td class="tableitem" style="text-align: center" colspan="4">
                <p class="itemtext">Data Kosong</p>
            </td>
        </tr>
        </tfoot>
    @endif
</table>