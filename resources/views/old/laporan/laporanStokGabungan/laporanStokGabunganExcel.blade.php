<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Harga Barang " . date('d-m-Y') . ".xls");
?>

<h3 align="center">LAPORAN DAFTAR HARGA BARANG</h3>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Harga Jual</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td>{{ $r->kode_barang }}</td>
            <td>{{ $r->nama_barang }}</td>
            <td>{{ Main::format_number($r->harga_jual_barang) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>