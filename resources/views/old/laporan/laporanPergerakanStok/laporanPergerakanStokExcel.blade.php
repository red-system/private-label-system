<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Pergerakan Stok " . $date_start . " sampai " . $date_end . ".xls");
?>

<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN PERGERAKAN STOK<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
            Lokasi : {{$lokasi}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Lokasi Pembelian</th>
        <th>Qty Pembelian</th>
        <th>Total Pembelian</th>
        <th>Lokasi Penjualan</th>
        <th>Qty Penjualan</th>
        <th>Total Penjualan</th>
        <th>Tgl Terakhir Pembelian</th>
        <th>Tgl Terakhir Penjualan</th>
    </tr>
    </thead>
    <tbody>
    @php($count = 0)
    @foreach($barang as $r)
        @php($count += 1)
        <tr>
            <td>{{ $r->kode_barang }}</td>
            <td>{{$r->nama_barang }}</td>
            <td>{{$r->po_lokasi }}</td>
            <td>{{$r->qty_beli}}</td>
            <td>{{$r->total_beli}}</td>
            <td>{{$r->so_lokasi }}</td>
            <td>{{$r->qty_jual}}</td>
            <td>{{$r->total_jual}}</td>
            <td>{{$r->tanggal_pembelian}}</td>
            <td>{{$r->tgl_penjualan}}</td>
        </tr>
    @endforeach
    </tbody>
    @if($count == 0)
        <tfoot>
        <tr>
            <td colspan="10" style="text-align: center">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>