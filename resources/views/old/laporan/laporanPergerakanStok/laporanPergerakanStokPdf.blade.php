<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Pergerakan Stok</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
                        Lokasi : {{$lokasi}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">Lokasi Pembelian</th>
                <th class="item">Qty Pembelian</th>
                <th class="item">Total Pembelian</th>
                <th class="item">Lokasi Penjualan</th>
                <th class="item">Qty Penjualan</th>
                <th class="item">Total Penjualan</th>
                <th class="item">Tgl Terakhir Pembelian</th>
                <th class="item">Tgl Terakhir Penjualan</th>
            </tr>
            </thead>
            <tbody>
            @php($count = 0)
            @foreach($barang as $r)
                @php($count += 1)
                <tr>
                    <td class="item">{{ $r->kode_barang }}</td>
                    <td class="item">{{$r->nama_barang }}</td>
                    <td class="item">{{$r->po_lokasi }}</td>
                    <td class="item">{{$r->qty_beli}}</td>
                    <td class="item">{{$r->total_beli}}</td>
                    <td class="item">{{$r->so_lokasi }}</td>
                    <td class="item">{{$r->qty_jual}}</td>
                    <td class="item">{{$r->total_jual}}</td>
                    <td class="item">{{$r->tanggal_pembelian}}</td>
                    <td class="item">{{$r->tgl_penjualan}}</td>
                </tr>
            @endforeach
            </tbody>
            @if($count == 0)
                <tfoot>
                <tr>
                    <td colspan="10" style="text-align: center" class="item">Data Kosong</td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>