<style type="text/css">
    .table-data {
        border-left: 0;
        border-right: 0;
        border-top: 0;
        border-bottom: 0;
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0;
        border-top: 0;
        border-bottom: 0;
        padding: 2px 4px;
        text-align: center;
        font-size: 12px;
        font-weight: normal;
        font-family: Roboto;
    }
</style>
<table width="100%" style="font-family: Roboto">
    <tbody>
    <tr>
        <td width="90">
            <img src="{{ base_path() }}/public/images/logo.png" width="90">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            Laporan Detail Sales Order<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br/>
            Langganan : {{$langganan}}<br/>
            Lokasi : {{$lokasi}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
@foreach($list as $r)
    <table style="margin-top: 15px; margin-bottom: 10px" width="100%" class="table-data">
        {{--        <tb></tb>--}}
        <tbody>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <th>No Faktur</th>
            <th>Tanggal</th>
            <th>Lokasi</th>
            <th>Langganan</th>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td>{{ $r->no_so }}</td>
            <td>{{Main::format_date($r->tgl_so) }}</td>
            <td>{{$r->lokasi }}</td>
            <td>{{$r->langganan }}</td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tbody>

        <tbody>
        <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th></th>
        </tr>
        </tbody>
        <tbody>
        @foreach($item as $i)
            @if($i->id_so == $r->id)
                <tr>
                    <td>{{$i->kode_barang}}</td>
                    <td>{{$i->nama_barang}}</td>
                    <td>{{Main::format_number($i->jml_barang_so)}}</td>
                    <td>Rp. {{Main::format_number($i->harga_net)}}</td>
                    <td>Rp. {{Main::format_number($i->subtotal)}}</td>
                    <td></td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td>Diskon ({{$r->disc_persen_so}}%)</td>
            @php($diskon_persen = $r->total_so / 100 * $r->disc_persen_so)
            <td>Rp. {{Main::format_number($diskon_persen)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Diskon Tambahan</td>
            <td>Rp. {{Main::format_number($r->disc_nominal_so)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>PPN</td>
            <td>Rp. {{Main::format_number($r->pajak_nominal_so)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Ongkos Kirim</td>
            <td>Rp. {{Main::format_number($r->ongkos_kirim_so)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Grand Total</td>
            <td>Rp. {{Main::format_number($r->grand_total_so)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>DP</td>
            <td>Rp. {{Main::format_number($r->dp_so)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        <tr>
            @php($sisa = $r->grand_total_so - $r->dp_so)
            <td colspan="3"></td>
            <td>Sisa</td>
            <td>Rp. {{Main::format_number($sisa)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tfoot>
    </table>
@endforeach
