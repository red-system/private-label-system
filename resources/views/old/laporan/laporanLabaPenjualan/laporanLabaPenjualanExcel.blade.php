<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Laba Penjualan " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN LABA PENJUALAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item">No Faktur</th>
        <th class="item">Tanggal Penjualan</th>
        <th class="item">Jatuh Tempo</th>
        <th class="item">Lokasi</th>
        <th class="item">Langganan</th>
        <th class="item">Salesman</th>
        <th class="item">Netto</th>
        <th class="item">Harga Pokok</th>
        <th class="item">Laba</th>
        <th class="item">Margin</th>
    </tr>
    </thead>
    <tbody>
    @php
        $a = null;
    @endphp
    @foreach($list as $r)
        <tr>
            <td class="item">{{ $r->no_penjualan }}</td>
            <td class="item">{{ $r->tgl_penjualan }}</td>
            <td class="item">{{$r->jatuh_tempo }}</td>
            <td class="item">{{$r->lokasi }}</td>
            <td class="item">{{$r->langganan }}</td>
            <td class="item">{{$r->salesman }}</td>
            <td class="item">{{$r->netto }}</td>
            <td class="item">{{$r->hpp_penjualan }}</td>
            <td class="item">{{$r->laba }}</td>
            <td class="item">{{$r->margin }}</td>
        </tr>
        @php
            $a = $a + 1;
        @endphp
    @endforeach
    </tbody>
    @if($a == null)
        <tfoot>
        <tr>
            <td colspan="10" class="item" style="text-align: center">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>