<link rel="stylesheet" type="text/css" href="{{ base_path()}}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Daftar Stok</font><br>
                        Tanggal : {{ Carbon\Carbon::parse(now())->format('d-m-Y') }}<br>
                        Lokasi : {{$lokasi}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        <table>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">Stok Akhir</th>
            </tr>
            @foreach($stok as $r)
                <tr class="service">
                    <td class="tableitem"><p class="itemtext">{{ $r->kode_barang }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->nama_barang }}</p></td>
                    <td class="tableitem" align="right"><p
                                class="itemtext">{{Main::format_number_system( $r->jml_barang ).'.'.$r->satuan}}</p></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>