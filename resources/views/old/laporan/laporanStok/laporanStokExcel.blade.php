<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Stok " . date('d-m-Y') . ".xls");
?>
<table width="100%" class="table-header">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="80">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR STOK<br>
            Tanggal : {{ Carbon\Carbon::parse(now())->format('d-m-Y') }}<br>
            Lokasi : {{$lokasi}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Stok Akhir</th>
    </tr>
    </thead>
    <tbody>
    @foreach($stok as $r)
        <tr>
            <td style="text-align: left">{{ $r->kode_barang }}</td>
            <td>{{$r->nama_barang }}</td>
            <td style="text-align: right">{{Main::format_number_system( $r->jml_barang ).'.'.$r->satuan}}</td>
        </tr>
    @endforeach
    </tbody>
</table>