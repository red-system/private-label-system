<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Pembelian VS Penjualan " . $tanggal_start . " S/d " . $tanggal_end . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PEMBELIAN VS PENJUALAN<br>
            Golongan :
            @if(is_array($nama_golongan))
                @foreach($nama_golongan as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_golongan}}
            @endif
            <br>
            Barang :
            @if(is_array($nama_barang))
                @foreach($nama_barang as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_barang}}
            @endif
            <br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif<br>
            Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Qty Beli</th>
        <th>Nilai Beli</th>
        <th>Qty Jual</th>
        <th>Nilai Jual</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td>{{ $r->kode }}</td>
            <td>{{ $r->barang }}</td>
            <td align="right">{{ $r->qty_beli }}</td>
            <td align="right">{{ $r->nilai_beli }}</td>
            <td align="right">{{ $r->qty_jual }}</td>
            <td align="right">{{ $r->nilai_jual }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="2"></td>
        <td align="right">{{$total_qty_beli}}</td>
        <td align="right">{{$total_nilai_beli}}</td>
        <td align="right">{{$total_qty_jual}}</td>
        <td align="right">{{$total_nilai_jual}}</td>
    </tr>
    </tfoot>
</table>