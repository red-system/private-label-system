<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Analisa Pembelian VS Penjualan</font><br>
                        Golongan :
                        @if(is_array($nama_golongan))
                            @foreach($nama_golongan as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_golongan}}
                        @endif
                        <br>
                        Barang :
                        @if(is_array($nama_barang))
                            @foreach($nama_barang as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_barang}}
                        @endif
                        <br>
                        Lokasi :
                        @if(is_array($nama_lokasi))
                            @foreach($nama_lokasi as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_lokasi}}
                        @endif<br>
                        Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/><br>
<br/><br>
<br/><br>
<div id="invoice-bot">
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode</th>
                <th class="item">Barang</th>
                <th class="item">Qty Beli</th>
                <th class="item">Nilai Beli</th>
                <th class="item">Qty Jual</th>
                <th class="item">Nilai Jual</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem"><p class="itemtext">{{ $r->kode }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->barang }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->qty_beli }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_beli }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->qty_jual }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_jual }}</p></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"></td>
                <td style="text-align: right" class="item">{{$total_qty_beli}}</td>
                <td style="text-align: right" class="item">{{$total_nilai_beli}}</td>
                <td style="text-align: right" class="item">{{$total_qty_jual}}</td>
                <td style="text-align: right" class="item">{{$total_nilai_jual}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>