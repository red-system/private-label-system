<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Tagihan Penjualan " . $date . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN TAGIHAN PENJUALAN<br>
            Per : {{ $date }}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item">Jatuh Tempo</th>
        <th class="item">Telat</th>
        <th class="item">No Faktur</th>
        <th class="item">Langganan</th>
        <th class="item">Salesman</th>
        <th class="item">Jumlah</th>
    </tr>
    </thead>
    <tbody>
    @php
        $a = null;
    @endphp
    @foreach($list as $r)
        <tr>
            <td class="item">{{ $r->jatuh_tempo }}</td>
            <td class="item" style="text-align: right;">{{$r->telat }}</td>
            <td class="item">{{$r->no_faktur_penjualan }}</td>
            <td class="item">{{$r->langganan }}</td>
            <td class="item">{{$r->salesman }}</td>
            <td class="item" style="text-align: right;">{{$r->sisa_piutang }}</td>
        </tr>
        @php
            $a = $a + 1;
        @endphp
    @endforeach
    </tbody>
    @if($a == null)
        <tfoot>
        <tr>
            <td colspan="6" class="item" style="text-align: center">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>