<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Sales Order</font><br>
                        Tanggal : {{\app\Helpers\Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">No Faktur</th>
                <th class="item">Tanggal</th>
                <th class="item">Lokasi</th>
                <th class="item">Langganan</th>
                <th class="item">Nilai</th>
                <th class="item">DP</th>
            </tr>
            </thead>
            <tbody>
            @php
                $a = null;
            @endphp
            @foreach($list as $r)
                <tr>
                    <td class="item">{{ $r->no_so }}</td>
                    <td class="item">{{$r->tgl_so }}</td>
                    <td class="item">{{$r->lokasi }}</td>
                    <td class="item">{{$r->pelanggan_sok }}</td>
                    <td class="item" style="text-align: right">{{$r->total_so }}</td>
                    <td class="item" style="text-align: right">{{$r->dp_so }}</td>
                </tr>
                @php
                    $a = $a + 1;
                @endphp
            @endforeach
            </tbody>
            @if($a == null)
                <tfoot>
                <tr>
                    <td colspan="6" class="item" style="text-align: center">Data Kosong</td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>