<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Sales Order " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN SALES ORDER<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th width="15%">No Faktur</th>
        <th width="10%">Tanggal</th>
        <th width="10%">Lokasi</th>
        <th width="10%">Langganan</th>
        <th width="10%">Nilai</th>
        <th width="10%">DP</th>
    </tr>
    </thead>
    <tbody>
    @php
        $a = null;
    @endphp
    @foreach($list as $r)
        <tr>
            <td width="15%">{{ $r->no_so }}</td>
            <td width="10%">{{$r->tgl_so }}</td>
            <td width="10%">{{$r->lokasi }}</td>
            <td width="10%">{{$r->pelanggan_sok }}</td>
            <td width="10%" style="text-align: right">{{$r->total_so }}</td>
            <td width="10%" style="text-align: right">{{$r->dp_so }}</td>
        </tr>
        @php
            $a = $a + 1;
        @endphp
    @endforeach
    </tbody>
    @if($a == null)
        <tfoot>
        <tr>
            <td colspan="6">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>