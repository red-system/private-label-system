<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }

    tr.noBorder th {
        border: 0 !important;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Pemindahan Stok</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
                        Lokasi :
                        @foreach($tempat as $kunci => $l)
                            @if($count == $l['lokasi'])
                                {{$l['lokasi']}}.
                            @else
                                {{$l['lokasi']}},
                            @endif
                        @endforeach
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        @foreach($lokasi as $lok)
            <table>
                <thead>
                <tr class="noBorder">
                    <th colspan="3" class="item" style="text-align: left">{{$lok->lokasi}}</th>
                </tr>
                <tr class="tabletitle">
                    <th class="item">Nama Barang</th>
                    <th class="item">Satuan</th>
                    <th class="item">Total Pemindahan</th>
                </tr>
                </thead>
                <tbody>
                @php($hitung = 0)
                @foreach($list as $l)
                    @if($lok->id == $l->id_lokasi)
                        @php($hitung += 1)
                        <tr>
                            <td class="item">{{$l->nama_barang}}</td>
                            <td class="item">{{$l->satuan}}</td>
                            <td class="item">{{$l->total_pemindahan}}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
                @if($hitung==0)
                    <tfoot>
                    <tr>
                        <td class="item" style="text-align: center" colspan="3">Data Kosong</td>
                    </tr>
                    </tfoot>
                @endif
            </table>
            <br>
            <br>
            <br>
        @endforeach
    </div>
</div>
