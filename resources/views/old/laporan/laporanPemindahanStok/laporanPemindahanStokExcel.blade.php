<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Pemindahan Stok " . $date_start . " sampai " . $date_end . ".xls");
?>
<style type="text/css">
    tr.noBorder th {
        border: 0 !important;
    }
</style>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN PEMINDAHAN STOK<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br>
            Lokasi :
            @foreach($tempat as $kunci => $l)
                @if($count == $l['lokasi'])
                    {{$l['lokasi']}}.
                @else
                    {{$l['lokasi']}},
                @endif
            @endforeach
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
@foreach($lokasi as $lok)
    <table>
        <thead>
        <tr class="noBorder">
            <th colspan="3" class="item" style="text-align: left">{{$lok->lokasi}}</th>
        </tr>
        </thead>
    </table>
        <table border="1">
        <tr class="tabletitle">
            <th class="item">Nama Barang</th>
            <th class="item">Satuan</th>
            <th class="item">Total Pemindahan</th>
        </tr>
        </thead>
        <tbody>
        @php($hitung = 0)
        @foreach($list as $l)
            @if($lok->id == $l->id_lokasi)
                @php($hitung += 1)
                <tr>
                    <td class="item">{{$l->nama_barang}}</td>
                    <td class="item">{{$l->satuan}}</td>
                    <td class="item">{{$l->total_pemindahan}}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
        @if($hitung==0)
            <tfoot>
            <tr>
                <td class="item" style="text-align: center" colspan="3">Data Kosong</td>
            </tr>
            </tfoot>
        @endif
    </table>
    <br>
    <br>
@endforeach