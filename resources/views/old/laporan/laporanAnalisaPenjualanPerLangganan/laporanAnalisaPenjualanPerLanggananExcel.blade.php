<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Penjualan Per Langganan " . $nama_langganan . ' ' . $tanggal_start . " S/d " . $tanggal_end . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PENJUALAN PER LANGGANAN<br>
            Golongan : {{$nama_golongan}}<br>
            Lokasi : {{$nama_lokasi}}<br>
            Langganan : {{$nama_langganan}}<br>
            Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Golongan</th>
        <th>Barang</th>
        <th>Kode</th>
        <th>Qty Jual</th>
        <th>Nilai Jual</th>
        <th>Nilai + PPN</th>
    </tr>
    </thead>
    <tbody>
    @php($golongan_saved = "")
    @php($langganan_saved = "")
    @foreach($list as $r)
        @if($langganan_saved != $r->langganan)
            <tr>
                <td align="center" colspan="2"><p style="font-weight: bold">{{ $r->langganan }}</p></td>
                <td colspan="4"></td>
            </tr>
        @endif
        <tr>
            @if($golongan_saved != $r->golongan)
                <td align="left">{{ $r->golongan }}</td>
            @else
                <td colspan="1"></td>
            @endif
            <td align="right">{{ $r->barang }}</td>
            <td align="right">{{ $r->kode_barang }}</td>
            <td align="right">{{ $r->qty_jual }}</td>
            <td align="right">{{ $r->nilai_jual }}</td>
            <td align="right">{{ $r->nilai_ppn }}</td>
            @php($golongan_saved = $r->golongan)
            @php($langganan_saved = $r->langganan)
        </tr>
    @endforeach
    </tbody>
</table>