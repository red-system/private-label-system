@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('laporanAnalisaPenjualanPerLanggananList').$uri}}">
        <div id="table_coloumn" style="display: none">{{$view}}</div>
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <div class="form-group m-form__group row">
                                    <label>Tanggal</label>
                                    <div class="input-daterange input-group row" id="m_datepicker_5">
                                        <div class="col-lg-5">
                                            <input type="text" class="form-control m-input" name="date_start"
                                                   value="{{ $date_start }}" id="tanggal_dari">
                                        </div>
                                        <span style="line-height: 3;">S/d</span>
                                        <div class="col-lg-5">
                                            <input type="text" class="form-control m-input" name="date_end"
                                                   value="{{ $date_end }}" id="tanggal_sampai">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label for="golongan">Golongan</label>
                                    <div class="input-daterange input-group">
                                        <select class="form-control m-input" name="id_golongan" id="golongan">
                                            <option value="">--Pilih Golongan--</option>
                                            @foreach($golongan as $item)
                                                @if($item->id == $select_golongan)
                                                    <option value="{{$item->id}}" selected>{{$item->golongan}}</option>
                                                @else
                                                    <option value="{{$item->id}}">{{$item->golongan}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <div class="form-group m-form__group row">
                                    <label for="lokasi">Lokasi</label>
                                    <div class="input-daterange input-group">
                                        <select class="form-control m-input" name="id_lokasi" id="lokasi">
                                            <option value="">--Pilih Lokasi--</option>
                                            @foreach($lokasi as $item)
                                                @if($item->id == $select_lokasi)
                                                    <option value="{{$item->id}}" selected>{{$item->lokasi}}</option>
                                                @else
                                                    <option value="{{$item->id}}">{{$item->lokasi}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group m-form__group row">
                                    <label for="langganan">Langganan</label>
                                    <div class="input-daterange input-group">
                                        <select class="form-control m-input" name="id_langganan" id="langganan">
                                            <option value="">--Pilih Langganan--</option>
                                            @foreach($langganan as $item)
                                                @if($item->id == $select_langganan)
                                                    <option value="{{$item->id}}" selected>{{$item->langganan}}</option>
                                                @else
                                                    <option value="{{$item->id}}">{{$item->langganan}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot text-center">
                            <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                <button type="submit" class="btn btn-accent akses-filter">
                                    <i class="la la-search"></i> Filter Data
                                </button>
                                <a href="{{ route('laporanAnalisaPenjualanPerLanggananPdf').$uri }}"
                                   class="btn btn-danger akses-pdf" target="_blank">
                                    <i class="la la-file-pdf-o"></i> Print PDF
                                </a>
                                <a href="{{ route('laporanAnalisaPenjualanPerLanggananExcel').$uri }}"
                                   class="btn btn-success akses-excel" target="_blank">
                                    <i class="la la-file-excel-o"></i> Print Excel
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Langganan</th>
                            <th>Golongan</th>
                            <th>Barang</th>
                            <th>Kode Barang</th>
                            <th>Qty</th>
                            <th>Nilai</th>
                            <th>Nilai + PPN</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection