<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Penjualan Barang Kurang Laris " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR PENJUALAN BARANG KURANG LARIS<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Golongan</th>
        <th>Terjual</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_barang }}</td>
            <td align="left">{{ $r->nama_barang }}</td>
            <td align="left">{{ $r->golongan }}</td>
            <td align="left">{{ Main::format_number($r->total_jml_barang_penjualan) }}</td>
            <td align="left">{{ Main::format_money($r->total_subtotal) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>