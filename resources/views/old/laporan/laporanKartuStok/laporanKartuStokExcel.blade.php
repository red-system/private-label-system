<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Mutasi Stok " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN MUTASI STOK<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
@foreach($id_barang as $id)
    <table style="margin-bottom: 10px" width="100%" border="1" class="table-data">
        <thead>
        <tr class="tabletitle">
            <th class="item">Kode Barang</th>
            <th class="item">Nama Barang</th>
            <th class="item">Lokasi</th>
            <th class="item">Saldo Awal</th>
            <th class="item">Total Masuk</th>
            <th class="item">Total Keluar</th>
            <th class="item">Saldo Akhir</th>
            <th class="item">Saldo Gabungan</th>
        </tr>
        </thead>
        <tbody>
        @php
            $total_awal = 0;
            $total_masuk = 0;
            $total_keluar = 0;
            $total_saldo = 0;
        @endphp
        @foreach($laporan as $r)
            @if($r->id_barang == $id->id_barang)
                @php
                    $total_awal = $total_awal + $r->saldo_awal;
                    $total_saldo = $total_saldo + $r->saldo_akhir;
                    $total_masuk = $total_masuk + $r->total_masuk;
                    $total_keluar = $total_keluar + $r->total_keluar;
                @endphp
                <tr>
                    <td width="10%">{{ $r->kode_barang }}</td>
                    <td width="10%" style="text-align: left">{{$r->nama_barang }}</td>
                    <td width="10%">{{$r->lokasi }}</td>
                    <td width="10%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->saldo_awal).' '.$r->satuan }}</td>
                    <td width="10%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->total_masuk).' '.$r->satuan }}</td>
                    <td width="10%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->total_keluar).' '.$r->satuan }}</td>
                    <td width="10%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->saldo_akhir).' '.$r->satuan }}</td>
                    <td width="10%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->saldo_akhir).' '.$r->satuan }}</td>
                </tr>
                @php
                    $satuan = $r->satuan;
                @endphp
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3">Total</td>
            <td style="text-align: right">
                {{\app\Helpers\Main::format_decimal($total_awal).' '.$satuan }}
            </td>
            <td style="text-align: right">
                {{\app\Helpers\Main::format_decimal($total_masuk).' '.$satuan }}
            </td>
            <td style="text-align: right">
                {{\app\Helpers\Main::format_decimal($total_keluar).' '.$satuan }}
            </td>
            <td style="text-align: right">
                {{\app\Helpers\Main::format_decimal($total_saldo).' '.$satuan }}
            </td>
            <td></td>
        </tr>
        </tfoot>
    </table>
    <br>
    <br>
@endforeach