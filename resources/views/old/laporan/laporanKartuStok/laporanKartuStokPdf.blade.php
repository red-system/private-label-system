<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Mutasi Stok</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        @foreach($id_barang as $id)
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">Lokasi</th>
                <th class="item">Saldo Awal</th>
                <th class="item">Total Masuk</th>
                <th class="item">Total Keluar</th>
                <th class="item">Saldo Akhir</th>
                <th class="item">Saldo Gabungan</th>
            </tr>
            </thead>
            <tbody>
            @php
                $total_awal = 0;
                $total_masuk = 0;
                $total_keluar = 0;
                $total_saldo = 0;
            @endphp
            @foreach($laporan as $r)
                @if($r->id_barang == $id->id_barang)
                    @php
                        $total_awal = $total_awal + $r->saldo_awal;
                        $total_saldo = $total_saldo + $r->saldo_akhir;
                        $total_masuk = $total_masuk + $r->total_masuk;
                        $total_keluar = $total_keluar + $r->total_keluar;
                    @endphp
                <tr class="service">
                    <td class="item">{{ $r->kode_barang }}</td>
                    <td class="item" style="text-align: left">{{$r->nama_barang }}</td>
                    <td class="item">{{$r->lokasi }}</td>
                    <td class="item" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->saldo_awal).' '.$r->satuan }}</td>
                    <td class="item" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->total_masuk).' '.$r->satuan }}</td>
                    <td class="item" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->total_keluar).' '.$r->satuan }}</td>
                    <td class="item" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->saldo_akhir).' '.$r->satuan }}</td>
                    <td class="item" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->saldo_akhir).' '.$r->satuan }}</td>
                </tr>
                    @php
                        $satuan = $r->satuan;
                    @endphp
                @endif
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3" class="item" style="text-align: center">Total</td>
                <td style="text-align: right" class="item">
                    {{\app\Helpers\Main::format_decimal($total_awal).' '.$satuan }}
                </td>
                <td style="text-align: right" class="item">
                    {{\app\Helpers\Main::format_decimal($total_masuk).' '.$satuan }}
                </td>
                <td style="text-align: right" class="item">
                    {{\app\Helpers\Main::format_decimal($total_keluar).' '.$satuan }}
                </td>
                <td style="text-align: right" class="item">
                    {{\app\Helpers\Main::format_decimal($total_saldo).' '.$satuan }}
                </td>
                <td></td>
            </tr>
            </tfoot>
        </table>
            <br>
            <br>
        @endforeach
    </div>
</div>