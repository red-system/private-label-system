<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Saldo Piutang Langganan " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            SALDO PIUTANG LANGGANAN<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="10%">Kode Langganan</th>
        <th width="15%">Nama Langganan</th>
        <th width="10%">Wilayah</th>
        <th width="10%">Netto</th>
        <th width="10%">Netto Rp</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td width="10%">{{ $r->kode_langganan }}</td>
            <td width="15%">{{$r->langganan }}</td>
            <td width="10%">{{$r->wilayah }}</td>
            <td width="10%" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->netto )}}</td>
            <td width="10%" style="text-align: right; padding-right: 20px">{{Main::format_number_system( $r->netto_rp )}}</td>
        </tr>
    @endforeach
    </tbody>
</table>