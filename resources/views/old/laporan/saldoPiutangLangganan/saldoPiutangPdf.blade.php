<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Saldo Piutang Langganan</font><br>
                        Per : {{\app\Helpers\Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Langganan</th>
                <th class="item">Nama Langganan</th>
                <th class="item">Wilayah</th>
                <th class="item">Netto</th>
                <th class="item">Netto Rp</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr>
                    <td class="item">{{ $r->kode_langganan }}</td>
                    <td class="item">{{$r->langganan }}</td>
                    <td class="item">{{$r->wilayah }}</td>
                    <td class="item" style="text-align: right; padding-right: 20px">{{Main::format_money( $r->netto )}}</td>
                    <td class="item" style="text-align: right; padding-right: 20px">{{Main::format_number_system( $r->netto_rp )}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>