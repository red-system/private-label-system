<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Penjualan Dan Stock " . $nama_golongan . ' ' . $tanggal_start . " S/d " . $tanggal_end . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PENJUALAN DAN STOCK<br>
            Golongan : {{$nama_golongan}}<br>
            Lokasi : {{$nama_lokasi}}<br>
            Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Golongan</th>
        <th>Barang</th>
        <th>Kode</th>
        <th>Qty Jual</th>
        <th>Nilai Jual</th>
        <th>Stock</th>
    </tr>
    </thead>
    <tbody>
    @php($golongan_saved = "")
    @foreach($list as $r)
        <tr>
            @if($golongan_saved != $r->golongan)
                <td align="left">{{ $r->golongan }}</td>
            @else
                <td colspan="1"></td>
            @endif
            <td align="right">{{ $r->barang }}</td>
            <td align="right">{{ $r->kode_barang }}</td>
            <td align="right">{{ $r->qty_jual }}</td>
            <td align="right">{{ $r->nilai_jual }}</td>
            <td align="right">{{ $r->stock }}</td>
            @php($golongan_saved = $r->golongan)
        </tr>
    @endforeach
    </tbody>
</table>