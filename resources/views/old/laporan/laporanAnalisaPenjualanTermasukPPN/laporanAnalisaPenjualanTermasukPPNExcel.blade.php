<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Penjualan Termasuk PPN " . $date_start . " sampai " . $date_end . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            ANALISA PENJUALAN TERMASUK PPN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="10%">Kode Barang</th>
        <th width="10%">Nama Barang</th>
        <th width="10%">Qty</th>
        <th width="10%">DPP</th>
        <th width="10%">PPN</th>
        <th width="10%">Nilai + PPN</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td width="10%">{{ $r->kode_barang }}</td>
            <td width="10%" style="text-align: left">{{$r->nama_barang }}</td>
            <td width="10%" style="text-align: right">{{$r->total_qty }}</td>
            <td width="10%" style="text-align: right">{{$r->total_dpp }}</td>
            <td width="10%" style="text-align: right">{{$r->total_ppn }}</td>
            <td width="10%" style="text-align: right">{{$r->total_nilai }}</td>
        </tr>
    @endforeach
    </tbody>
</table>