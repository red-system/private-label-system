<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Komisi Langganan</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Langganan</th>
                <th class="item">No Faktur</th>
                <th class="item">Tanggal</th>
                <th class="item">Barang</th>
                <th class="item">Min Beli</th>
                <th class="item">Qty Beli</th>
                <th class="item">Jenis Komisi</th>
                <th class="item">Komisi</th>
                <th class="item">Komisi Nominal</th>
                <th class="item">Status</th>
                <th class="item">Tanggal Bayar</th>
            </tr>
            </thead>
            <tbody>
            @php
                $a = null;
            @endphp
            @foreach($list as $r)
                <tr>
                    <td class="item">{{ $r->langganan }}</td>
                    <td class="item">{{ $r->no_penjualan }}</td>
                    <td class="item">{{$r->tgl_penjualan }}</td>
                    <td class="item">{{$r->nama_barang }}</td>
                    <td class="item" style="text-align: right">{{$r->min_beli }}</td>
                    <td class="item" style="text-align: right">{{$r->qty_beli }}</td>
                    <td class="item">{{$r->jenis_komisi }}</td>
                    <td class="item" style="text-align: right">{{$r->komisi }}</td>
                    <td class="item" style="text-align: right">{{$r->komisi_nominal }}</td>
                    <td class="item">{{$r->status }}</td>
                    <td class="item">{{$r->updated_at }}</td>
                </tr>
                @php
                    $a = $a + 1;
                @endphp
            @endforeach
            </tbody>
            @if($a == null)
                <tfoot>
                <tr>
                    <td colspan="11" class="item" style="text-align: center">Data Kosong</td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>