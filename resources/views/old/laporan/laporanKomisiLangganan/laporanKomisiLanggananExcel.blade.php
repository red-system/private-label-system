<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Komisi Langganan " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN KOMISI LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item">Langganan</th>
        <th class="item">No Faktur</th>
        <th class="item">Tanggal</th>
        <th class="item">Barang</th>
        <th class="item">Min Beli</th>
        <th class="item">Qty Beli</th>
        <th class="item">Jenis Komisi</th>
        <th class="item">Komisi</th>
        <th class="item">Komisi Nominal</th>
        <th class="item">Status</th>
        <th class="item">Tanggal Bayar</th>
    </tr>
    </thead>
    <tbody>
    @php
        $a = null;
    @endphp
    @foreach($list as $r)
        <tr>
            <td class="item">{{ $r->langganan }}</td>
            <td class="item">{{ $r->no_penjualan }}</td>
            <td class="item">{{$r->tgl_penjualan }}</td>
            <td class="item">{{$r->nama_barang }}</td>
            <td class="item" style="text-align: right">{{$r->min_beli }}</td>
            <td class="item" style="text-align: right">{{$r->qty_beli }}</td>
            <td class="item">{{$r->jenis_komisi }}</td>
            <td class="item" style="text-align: right">{{$r->komisi }}</td>
            <td class="item" style="text-align: right">{{$r->komisi_nominal }}</td>
            <td class="item">{{$r->status }}</td>
            <td class="item">{{$r->updated_at }}</td>
        </tr>
        @php
            $a = $a + 1;
        @endphp
    @endforeach
    </tbody>
    @if($a == null)
        <tfoot>
        <tr>
            <td colspan="11" class="item" style="text-align: center">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>