<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Harga Barang " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR HARGA BARANG<br>
            Tanggal : {{ Carbon\Carbon::parse(now())->format('d-m-Y') }}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Harga Jual</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_barang }}</td>
            <td align="left">{{ $r->nama_barang }}</td>
            <td align="left">{{ Main::format_money($r->harga_jual_barang) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>