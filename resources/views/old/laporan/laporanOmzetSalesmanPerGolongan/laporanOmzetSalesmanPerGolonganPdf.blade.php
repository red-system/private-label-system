<style type="text/css">
    .table-data {
        border-left: 0;
        border-right: 0;
        border-top: 0;
        border-bottom: 0;
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0;
        border-top: 0;
        border-bottom: 0;
        padding: 2px 4px;
        text-align: center;
        font-size: 12px;
        font-weight: normal;
        font-family: Roboto;
    }
</style>

<table width="100%" style="font-family: Roboto; font-size: 12px">
    <tbody>
    <tr>
        <td width="80">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            Laporan Omzet Salesman Per Golongan<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<hr>
<table style="margin-top: 8px; margin-bottom: 10px" border="1" width="100%" class="table-data">
    <thead>
    <tr>
        <th width="15%">Golongan</th>
        <th width="10%">Kode Barang</th>
        <th width="15%" style="text-align: left">Nama Barang</th>
        <th width="10%">Qty</th>
        <th width="15%" style="text-align: right">Harga Jual</th>
        <th width="15%">Subtotal</th>
        <th width="20%" style="text-align: left">Salesman</th>
    </tr>
    </thead>
</table>
<hr>
@foreach($id_golongan as $id)
    <table style="margin-bottom: 10px" width="100%" border="1" class="table-data">
        <tbody>
        @php
            $total = 0;
            $barang = 0;
        @endphp
        @foreach($list as $r)
            @if($r->id_golongan == $id->id_golongan)
                @php
                    $total = $total + $r->subtotal;
                    $barang = $barang + $r->total_qty;
                @endphp
                <tr>
                    <td width="15%">{{ $r->golongan }}</td>
                    <td width="10%">{{ $r->kode_barang }}</td>
                    <td width="15%" style="text-align: left">{{$r->nama_barang }}</td>
                    <td width="10%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->total_qty).' '.$r->satuan }}</td>
                    <td width="15%" style="text-align: right">{{\app\Helpers\Main::format_money($r->harga_net) }}</td>
                    <td width="15%" style="text-align: right">{{\app\Helpers\Main::format_money($r->subtotal) }}</td>
                    <td width="20%" style="text-align: left">{{$r->salesman}}</td>
                </tr>

                @php
                    $satuan = $r->satuan;
                @endphp
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td style="text-align: right">
                <hr>{{\app\Helpers\Main::format_decimal($barang).' '.$satuan}}</td>
            <td></td>
            <td style="text-align: right">
                <hr>{{\app\Helpers\Main::format_money($total)}}</td>
            <td></td>
        </tr>
        </tfoot>
    </table>
    <div style="border-bottom:2px dashed;"></div>
    <br>
@endforeach