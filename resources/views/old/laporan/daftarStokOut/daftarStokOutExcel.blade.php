<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Stok Out " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR STOK OUT<br>
            {{$lokasi}} / {{date('m-Y',strtotime($tanggal_out))}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Golongan</th>
        <th>Supplier</th>
        <th>Qty Out</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_barang }}</td>
            <td>{{ $r->nama_barang }}</td>
            <td>{{ $r->golongan }}</td>
            <td>{{ $r->supplier }}</td>
            <td align="right">{{ $r->total_penjualan_barang }}</td>
        </tr>
    @endforeach
    </tbody>
</table>