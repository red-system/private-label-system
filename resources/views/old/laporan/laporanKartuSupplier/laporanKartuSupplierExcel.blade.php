<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Kartu Supplier " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN KARTU SUPPLIER<br>
            Tanggal : {{$tanggal_start}} S/d {{$tanggal_end}}<br>
            Supplier :
            @if(is_array($supplier))
                @foreach($supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$supplier}}
            @endif
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>No. Faktur</th>
        <th>Tanggal</th>
        <th>Tgl. Jatuh Tempo</th>
        <th>Tgl. Bayar</th>
        <th>Nilai</th>
        <th>Retur</th>
        <th>Pajak</th>
        <th>Bayar/DN/KN</th>
        <th>Sisa</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->no_pembelian_label }}</td>
            <td>{{ $r->tanggal_pembelian }}</td>
            <td>{{ $r->tanggal_jatuh_tempo }}</td>
            <td>{{ $r->tanggal_bayar }}</td>
            <td align="right">{{ $r->harga }}</td>
            <td align="right">{{ $r->retur }}</td>
            <td align="right">{{ $r->pajak_nominal_po }}</td>
            <td align="right">{{ $r->bayar_dn_kn }}</td>
            <td align="right">{{ $r->sisa_hutang_supplier }}</td>
            <td align="left">{{ $r->keterangan_pembelian }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="4"></td>
        <td class="item" align="right">{{$total_nilai}}</td>
        <td class="item" align="right">{{$total_retur}}</td>
        <td class="item" align="right">{{$total_pajak}}</td>
        <td class="item" align="right">{{$total_bayar_dn_kn}}</td>
        <td class="item" align="right">{{$total_sisa}}</td>
    </tr>
    </tfoot>
</table>