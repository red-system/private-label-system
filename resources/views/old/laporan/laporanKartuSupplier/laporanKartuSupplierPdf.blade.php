<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Kartu Supplier</font><br>
                        Supplier :
                        @if(is_array($supplier))
                            @foreach($supplier as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$supplier}}
                        @endif<br>
                        Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>

<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">No. Faktur</th>
                <th class="item">Tanggal</th>
                <th class="item">Tgl. Jatuh Tempo</th>
                <th class="item">Tgl. Bayar</th>
                <th class="item">Nilai</th>
                <th class="item">Retur</th>
                <th class="item">Pajak</th>
                <th class="item">Bayar/DN/KN</th>
                <th class="item">Sisa</th>
                <th class="item">Keterangan</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem"><p class="itemtext">{{ $r->no_pembelian_label }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->tanggal_pembelian }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->tanggal_jatuh_tempo }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->tanggal_bayar }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->harga }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->retur }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->pajak_nominal_po }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->bayar_dn_kn }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->sisa_hutang_supplier }}</p>
                    </td>
                    <td class="tableitem" align="left"><p class="itemtext">{{ $r->keterangan_pembelian }}</p>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4"></td>
                <td class="item" align="right">{{$total_nilai}}</td>
                <td class="item" align="right">{{$total_retur}}</td>
                <td class="item" align="right">{{$total_pajak}}</td>
                <td class="item" align="right">{{$total_bayar_dn_kn}}</td>
                <td class="item" align="right">{{$total_sisa}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>