<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Daftar Stok Zero " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DAFTAR STOK ZERO<br>
            Per : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Lokasi</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_barang }}</td>
            <td align="left">{{ $r->nama_barang }}</td>
            <td align="left">{{ $r->lokasi }}</td>
        </tr>
    @endforeach
    </tbody>
</table>