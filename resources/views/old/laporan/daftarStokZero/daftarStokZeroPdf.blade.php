<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Daftar Stok Nol</font><br>
                        Per : {{Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        <table>
            <tr class="tabletitle">
                <th class="item">Kode Barang</th>
                <th class="item">Nama Barang</th>
                <th class="item">Lokasi</th>
            </tr>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem"><p class="itemtext">{{ $r->kode_barang }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $r->nama_barang }}</p></td>
                    <td class="tableitem" align="left"><p class="itemtext">{{ $r->lokasi }}</p></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>