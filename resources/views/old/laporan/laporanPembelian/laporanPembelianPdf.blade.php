<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}<br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </td>
                    <td colspan="2" width="230">
                        <font size="2">Laporan Pembelian</font><br>
                        Supplier :
                        @if(is_array($nama_supplier))
                            @foreach($nama_supplier as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_supplier}}
                        @endif<br>
                        Lokasi :
                        @if(is_array($nama_lokasi))
                            @foreach($nama_lokasi as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_lokasi}}
                        @endif<br>
                        Tanggal : {{$tanggal_start}} S/d {{$tanggal_end}}<br>
                        Tanggal Jatuh Tempo : {{$tanggal_start_jatuh_tempo}}
                        S/d {{$tanggal_end_jatuh_tempo}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<br/>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">No Faktur</th>
                <th class="item">Tanggal</th>
                <th class="item">Tgl. Jatuh Tempo</th>
                <th class="item">Lokasi</th>
                <th class="item">Supplier</th>
                <th class="item">Nilai</th>
                <th class="item">Bayar/DN/KN</th>
                <th class="item">Sisa</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->no_faktur }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->tanggal }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->jatuh_tempo }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->lokasi }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->supplier }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->bayar_dn_kn }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->sisa }}</p></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5"></td>
                <td class="item" align="right">{{$total_nilai}}</td>
                <td class="item" align="right">{{$total_bayar_dn_kn}}</td>
                <td class="item" align="right">{{$total_sisa}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>