<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Nota Tangihan Langganan : " . $langganan->langganan . ".xls");
?>

<table width="100%" style="font-size: 14px">
    <tbody>
    <tr>
        <td width="70">
            <img src="{{ url('/logo') }}" width="70">
            <br>
            <br>
        </td>
        <td colspan="2">
            {{ $company->companyName.' - '.$lokasi->kode_lokasi }}<br>
            {{$lokasi->alamat}}<br>
            Telp : {{$lokasi->kontak}}<br>
            <br>
            <br>
        </td>
        <td>
            {{$lokasi->lokasi.', '.Main::format_date(now())}}<br>
            Kepada Yth :<br>
            {{$langganan->langganan}}<br>
            {{$langganan->langganan_alamat}}<br>
            {{$langganan->langganan_kontak}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th><font size="1">No Faktur Penjualan</font></th>
        <th><font size="1">Tanggal</font></th>
        <th><font size="1">Jatuh Tempo</font></th>
        <th><font size="1">Jumlah</font></th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 0;
    @endphp
    @foreach($list as $r)
        @php
            $no = $no +1;
        @endphp
        <tr>
            <td><font size="1">{{$r->no_faktur_penjualan }}</font></td>
            <td><font size="1">{{Main::format_date( $r->tgl_faktur_penjualan )}}</font></td>
            <td><font size="1">{{Main::format_date( $r->jatuh_tempo )}}</font></td>
            <td><font size="1">Rp. {{Main::format_number( $r->sisa_piutang )}}</font></td>
        </tr>
    @endforeach
    </tbody>
    @if($no == 0)
        <tfoot>
        <td colspan="4" style="text-align: center">
            Data Kosong
        </td>
        </tfoot>
    @endif
</table>