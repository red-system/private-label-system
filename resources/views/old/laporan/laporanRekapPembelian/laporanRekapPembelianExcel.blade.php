<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Rekap Pembelian ". $tanggal_start  ." S/d ".$tanggal_end.  ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN REKAP PEMBELIAN<br>
            Golongan : @if(is_array($nama_golongan))
                @foreach($nama_golongan as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_golongan}}
            @endif<br>
            Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th> Golongan </th>
        <th>Nilai</th>
        <th>Nilai %</th>
        <th>Nilai + PPN</th>
        <th>Nilai + PPN %</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->golongan }}</td>
            <td align="right">{{ $r->nilai }}</td>
            <td align="right">{{ $r->nilai_persen }}</td>
            <td align="right">{{ $r->nilai_ppn }}</td>
            <td align="right">{{ $r->nilai_ppn_persen }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="1"></td>
        <td class="item" align="right">{{$total_nilai_kirim}}</td>
        <td class="item" align="right">{{$total_nilai_persen_kirim}}</td>
        <td class="item" align="right">{{$total_nilai_ppn_kirim}}</td>
        <td class="item" align="right">{{$total_nilai_ppn_persen_kirim}}</td>
    </tr>
    </tfoot>
</table>