<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Rekap Pembelian</font><br>
                        Golongan :
                        @if(is_array($nama_golongan))
                            @foreach($nama_golongan as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_golongan}}
                        @endif<br>
                        Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Golongan</th>
                <th class="item">Nilai</th>
                <th class="item">Nilai %</th>
                <th class="item">Nilai + PPN</th>
                <th class="item">Nilai + PPN %</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                <tr class="service">
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->golongan }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_persen }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_ppn }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_ppn_persen }}</p></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="1"></td>
                <td class="item" align="right">{{$total_nilai_kirim}}</td>
                <td class="item" align="right">{{$total_nilai_persen_kirim}}</td>
                <td class="item" align="right">{{$total_nilai_ppn_kirim}}</td>
                <td class="item" align="right">{{$total_nilai_ppn_persen_kirim}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>