<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Detail Penjualan " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN DETAIL PENJUALAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br/>
            Langganan : {{$langganan}}<br/>
            Lokasi : {{$lokasi}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
@foreach($list as $r)
    <table style="margin-top: 15px; margin-bottom: 10px" width="100%" class="table-data">
        {{--        <tb></tb>--}}
        <tbody>
        <tr>
            <td colspan="6">
                <hr/>
            </td>
        </tr>
        <tr>
            <th width="10%">No Faktur :</th>
            <th width="7%">Tanggal :</th>
            <th width="7%">Jatuh Tempo :</th>
            <th width="8%">Lokasi :</th>
            <th width="8%">Langganan :</th>
            <th width="15%">Salesaman :</th>
        </tr>
        <tr>
            <td>{{ $r->no_penjualan }}</td>
            <td>{{Main::format_date($r->tgl_penjualan) }}</td>
            <td>{{Main::format_date($r->jatuh_tempo) }}</td>
            <td>{{$r->lokasi }}</td>
            <td>{{$r->langganan }}</td>
            <td>{{$r->salesman }}</td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Qty</th>
            <th>Harga Barang</th>
            <th>Jumlah</th>
            <th></th>
        </tr>
        </tbody>
        <tbody>
        @foreach($item as $i)
            @if($i->id_penjualan == $r->id)
                <tr>
                    <td>{{$i->kode_barang}}</td>
                    <td>{{$i->nama_barang}}</td>
                    <td>{{Main::format_number($i->jml_barang_penjualan).' '.$i->satuan_penjualan}}</td>
                    <td style="text-align: right">{{Main::format_money($i->harga_penjualan)}}</td>
                    <td style="text-align: right">{{Main::format_money($i->subtotal)}}</td>
                    <td></td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td>Diskon ({{$r->disc_persen_penjualan}}%)</td>
            @php($diskon_persen = $r->total_penjualan / 100 * $r->disc_persen_penjualan)
            <td style="text-align: right">{{Main::format_money($diskon_persen)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Diskon Tambahan</td>
            <td style="text-align: right">{{Main::format_money($r->disc_nominal_penjualan)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>PPN</td>
            <td style="text-align: right">{{Main::format_money($r->pajak_nominal_penjualan)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Ongkos Kirim</td>
            <td style="text-align: right">{{Main::format_money($r->ongkos_kirim_penjualan)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Grand Total</td>
            <td style="text-align: right">{{Main::format_money($r->grand_total_penjualan)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>DP</td>
            <td style="text-align: right">{{Main::format_money($r->dp_penjualan)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom:1px dashed;"></td>
        </tr>
        <tr>
            @php($sisa = $r->grand_total_penjualan - $r->dp_penjualan)
            <td colspan="3"></td>
            <td>Sisa</td>
            <td style="text-align: right">{{Main::format_money($sisa)}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">
                <hr>
            </td>
        </tr>
        </tfoot>
    </table>
    <br>
    <br>
@endforeach