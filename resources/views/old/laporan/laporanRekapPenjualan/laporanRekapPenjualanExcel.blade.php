<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Kartu Stok " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN REKAP PENJUALAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}<br/>
            Lokasi : {{$lokasi}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 15px; margin-bottom: 10px" width="100%" class="table-data" border="1">
    <thead>
    <tr>
        <th width="10%">Golongan</th>
        <th width="10%">Qty Barang</th>
        <th width="10%">Nilai</th>
        <th width="5%">%</th>
        <th width="10%">Laba</th>
        <th width="5%">%</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        @php
            $persentase_nilai = $r->nilai / $total_nilai * 100;
            $persentase_laba = $r->laba / $total_laba * 100;
            $satuan = $r->satuan;
        @endphp
        <tr>
            <td width="10%">{{ $r->golongan }}</td>
            <td width="10%">{{Main::format_number($r->jumlah_barang).' '.$r->satuan }}</td>
            <td width="10%" style="text-align: right">{{Main::format_money($r->nilai) }}</td>
            <td width="5%" style="text-align: right">{{Main::format_number_db($persentase_nilai) }}</td>
            <td width="10%" style="text-align: right">{{Main::format_money($r->laba) }}</td>
            <td width="5%" style="text-align: right">{{Main::format_number_db($persentase_laba) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    @php
        $persen_nilai = $total_nilai/$total_nilai*100;
        $persen_laba = $total_laba/$total_laba*100;
    @endphp
    <tr>
        <td>Total</td>
        <td>{{Main::format_decimal($total_barang)}}</td>
        <td style="text-align: right">{{Main::format_money($total_nilai)}}</td>
        <td style="text-align: right">{{$persen_nilai}}%</td>
        <td style="text-align: right">{{Main::format_money($total_laba)}}</td>
        <td style="text-align: right">{{$persen_laba}}%</td>
    </tr>
    </tfoot>
</table>