<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Rekap Penjualan</font><br>
                        Tanggal : {{ $date_start }} s/d {{$date_end}}<br/>
                        Lokasi : {{$lokasi}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Golongan</th>
                <th class="item">Qty Barang</th>
                <th class="item">Nilai</th>
                <th class="item">%</th>
                <th class="item">Laba</th>
                <th class="item">%</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $r)
                @php
                    $persentase_nilai = $r->nilai / $total_nilai * 100;
                    $persentase_laba = $r->laba / $total_laba * 100;
                    $satuan = $r->satuan;
                @endphp
                <tr>
                    <td class="item">{{ $r->golongan }}</td>
                    <td class="item">{{Main::format_number($r->jumlah_barang).' '.$r->satuan }}</td>
                    <td class="item" style="text-align: right">{{Main::format_money($r->nilai) }}</td>
                    <td class="item" style="text-align: right">{{Main::format_number_db($persentase_nilai) }}</td>
                    <td class="item" style="text-align: right">{{Main::format_money($r->laba) }}</td>
                    <td class="item" style="text-align: right">{{Main::format_number_db($persentase_laba) }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
            @php
                $persen_nilai = $total_nilai/$total_nilai*100;
                $persen_laba = $total_laba/$total_laba*100;
            @endphp
            <tr>
                <td class="item">Total</td>
                <td class="item">{{Main::format_decimal($total_barang)}}</td>
                <td class="item" style="text-align: right">{{Main::format_money($total_nilai)}}</td>
                <td class="item" style="text-align: right">{{$persen_nilai}}%</td>
                <td class="item" style="text-align: right">{{Main::format_money($total_laba)}}</td>
                <td class="item" style="text-align: right">{{$persen_laba}}%</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
