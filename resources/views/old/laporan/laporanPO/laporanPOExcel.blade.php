<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan PO ". $tanggal_start  ." S/d ".$tanggal_end.  ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN PO<br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif<br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif<br>
            Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>No Faktur</th>
        <th>Tanggal</th>
        <th>Lokasi</th>
        <th>Supplier</th>
        <th>Nilai</th>
        <th>Bayar/DN/KN</th>
        <th>Sisa</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->no_faktur }}</td>
            <td align="left">{{ $r->tanggal }}</td>
            <td align="right">{{ $r->lokasi }}</td>
            <td align="right">{{ $r->supplier }}</td>
            <td align="right">{{ $r->nilai }}</td>
            <td align="right">{{ $r->bayar_dn_kn }}</td>
            <td align="right">{{ $r->sisa }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="4"></td>
        <td class="item" align="right">{{$total_nilai}}</td>
        <td class="item" align="right">{{$total_bayar_dn_kn}}</td>
        <td class="item" align="right">{{$total_sisa}}</td>
    </tr>
    </tfoot>
</table>