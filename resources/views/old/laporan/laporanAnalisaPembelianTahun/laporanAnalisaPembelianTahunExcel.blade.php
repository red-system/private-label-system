<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Pembelian Tahunan " . $tahun_filter . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PEMBELIAN TAHUNAN<br>
            Golongan :
            @if(is_array($nama_golongan))
                @foreach($nama_golongan as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_golongan}}
            @endif
            <br>
            Barang :
            @if(is_array($nama_barang))
                @foreach($nama_barang as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_barang}}
            @endif
            <br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif
            <br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif<br>
            Tahun {{$tahun_filter}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Bulan Pembelian</th>
        <th>Golongan</th>
        <th>Barang</th>
        <th>Lokasi</th>
        <th>Qty</th>
        <th>Nilai</th>
        <th>Nilai + PPN</th>
    </tr>
    </thead>
    <tbody>
    @php($golongan_saved = "")
    @php($bulan_saved = "")
    @foreach($list as $r)
        <tr>
            @if($bulan_saved != $r->bulan)
                <td align="left">{{ $r->bulan }}</td>
            @else
                <td colspan="1"></td>
            @endif
            @if($golongan_saved != $r->golongan)
                <td align="left">{{ $r->golongan }}</td>
            @else
                <td colspan="1"></td>
            @endif
            <td align="right">{{ $r->barang }}</td>
            <td align="right">{{ $r->lokasi }}</td>
            <td align="right">{{ $r->qty }}</td>
            <td align="right">{{ $r->nilai }}</td>
            <td align="right">{{ $r->nilai_ppn }}</td>
            @php($golongan_saved = $r->golongan)
            @php($bulan_saved = $r->bulan)
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4"></td>
            <td align="right">{{$total_qty}}</td>
            <td align="right">{{$total_nilai}}</td>
            <td align="right">{{$total_nilai_ppn}}</td>
        </tr>
    </tfoot>
</table>