<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Analisa Penjualan Semua Langganan  " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            ANALISA PENJUALAN SEMUA LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<hr>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" class="table-data">
    <thead>
    <tr>
        <th width="15%">Nama Barang</th>
        <th width="15%">Kode Barang</th>
        <th width="15%">Kode Langganan</th>
        <th width="15%">Langganan</th>
        <th width="15%">Qty</th>
        <th width="15%">Nilai</th>
    </tr>
    </thead>
</table>
<hr>
@foreach($id_barang as $id)
    @php
        $barang = \app\Models\Widi\mBarang::where('id', $id->id_barang)->value('nama_barang');
    @endphp
    <table style="margin-bottom: 10px" width="100%" border="1" class="table-data">
        <tbody>
        @php
            $total = 0;
            $barang = 0;
        @endphp
        @foreach($list as $r)
            @if($r->id_barang == $id->id_barang)
                @php
                    $total = $total + $r->subtotal;
                    $barang = $barang + $r->jml_barang_penjualan;
                @endphp
                <tr>
                    <td width="15%">{{ $r->nama_barang }}</td>
                    <td width="15%">{{$r->kode_barang }}</td>
                    <td width="15%">{{$r->kode_langganan }}</td>
                    <td width="15%">{{$r->langganan }}</td>
                    <td width="15%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->jml_barang_penjualan).' '.$r->satuan }}</td>
                    <td width="15%" style="text-align: right">{{\app\Helpers\Main::format_money($r->subtotal)}}</td>
                </tr>
                @php
                    $satuan = $r->satuan;
                @endphp
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td style="text-align: right">
                <hr>{{\app\Helpers\Main::format_decimal($barang).' '.$satuan}}</td>
            <td style="text-align: right">
                <hr>{{\app\Helpers\Main::format_money($total)}}</td>
        </tr>
        </tfoot>
    </table>
    <div style="border-bottom:2px dashed;"></div>
    <br>
@endforeach