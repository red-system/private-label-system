<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Pembelian " . $tanggal_start . " S/d " . $tanggal_end . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PEMBELIAN<br>
            Golongan :
            @if(is_array($nama_golongan))
                @foreach($nama_golongan as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_golongan}}
            @endif
            <br>
            Barang :
            @if(is_array($nama_barang))
                @foreach($nama_barang as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_barang}}
            @endif
            <br>
            Lokasi :
            @if(is_array($nama_lokasi))
                @foreach($nama_lokasi as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_lokasi}}
            @endif
            <br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif<br>
            Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Golongan</th>
        <th>Barang</th>
        <th>Lokasi</th>
        <th>Qty</th>
        <th>Nilai</th>
        <th>Nilai + PPN</th>
    </tr>
    </thead>
    <tbody>
    @php($golongan_saved = "")
    @foreach($list as $r)
        <tr>
            @if($golongan_saved != $r->golongan)
                <td align="left">{{ $r->golongan }}</td>
            @else
                <td colspan="1"></td>
            @endif
            <td align="right">{{ $r->barang }}</td>
            <td align="right">{{ $r->lokasi }}</td>
            <td align="right">{{ $r->qty }}</td>
            <td align="right">{{ $r->nilai }}</td>
            <td align="right">{{ $r->nilai_ppn }}</td>
            @php($golongan_saved = $r->golongan)
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3"></td>
        <td align="right">{{$total_qty}}</td>
        <td align="right">{{$total_nilai}}</td>
        <td align="right">{{$total_nilai_ppn}}</td>
    </tr>
    </tfoot>
</table>