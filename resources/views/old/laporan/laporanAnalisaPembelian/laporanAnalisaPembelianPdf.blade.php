<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
        text-align: center;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Analisa Pembelian</font><br>
                        Golongan :
                        @if(is_array($nama_golongan))
                            @foreach($nama_golongan as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_golongan}}
                        @endif
                        <br>
                        Barang :
                        @if(is_array($nama_barang))
                            @foreach($nama_barang as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_barang}}
                        @endif
                        <br>
                        Lokasi :
                        @if(is_array($nama_lokasi))
                            @foreach($nama_lokasi as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_lokasi}}
                        @endif
                        <br>
                        Supplier :
                        @if(is_array($nama_supplier))
                            @foreach($nama_supplier as $key => $value)
                                @if($key == 0) {{$value}} @else , {{$value}} @endif
                            @endforeach
                        @else
                            {{$nama_supplier}}
                        @endif<br>
                        Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/><br>
<br/><br>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Golongan</th>
                <th class="item">Barang</th>
                <th class="item">Lokasi</th>
                <th class="item">Qty</th>
                <th class="item">Nilai</th>
                <th class="item">Nilai + PPN</th>
            </tr>
            </thead>
            <tbody>
            @php($golongan_saved = "")
            @foreach($list as $r)
                <tr class="service">
                    @if($golongan_saved != $r->golongan)
                        <td class="tableitem" align="right"><p class="itemtext">{{ $r->golongan }}</p></td>
                    @else
                        <td colspan="1"></td>
                    @endif
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->barang }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->lokasi }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->qty }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_ppn }}</p></td>
                </tr>
                @php( $golongan_saved = $r->golongan)
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3"></td>
                <td style="text-align: right" class="item">{{$total_qty}}</td>
                <td style="text-align: right" class="item">{{$total_nilai}}</td>
                <td style="text-align: right" class="item">{{$total_nilai_ppn}}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>