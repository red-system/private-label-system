<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Analisa Umur Penjualan</font><br>
                        Per Tanggal : {{ $date}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Langganan</th>
                <th class="item">Sebelumnya</th>
                <th class="item">1-7 Hari</th>
                <th class="item">8-14 Hari</th>
                <th class="item">15-21 Hari</th>
                <th class="item"> > 21 Hari</th>
                <th class="item">Jumlah</th>
            </tr>
            </thead>
            <tbody>
            @php
                $a = null;
            @endphp
            @foreach($list as $r)
                <tr>
                    <td class="item" style="text-align: left">{{ $r->langganan }}</td>
                    <td class="item" style="text-align: right">{{ $r->sebelumnya }}</td>
                    <td class="item" style="text-align: right">{{ $r->minggu_pertama }}</td>
                    <td class="item" style="text-align: right">{{ $r->minggu_kedua }}</td>
                    <td class="item" style="text-align: right">{{ $r->minggu_ketiga }}</td>
                    <td class="item" style="text-align: right">{{ $r->seterusnya }}</td>
                    <td class="item" style="text-align: right">{{$r->jumlah }}</td>
                </tr>
                @php
                    $a = $a + 1;
                @endphp
            @endforeach
            </tbody>
            @if($a == null)
                <tfoot>
                <tr>
                    <td colspan="7" class="item" style="text-align: center">Data Kosong</td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>