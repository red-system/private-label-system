<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Umur Penjualan " . $date . ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA UMUR PENJUALAN<br>
            Per Tanggal : {{ $date}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="15%">Langganan</th>
        <th width="15%">Sebelumnya</th>
        <th width="15%">1-7 Hari</th>
        <th width="15%">8-14 Hari</th>
        <th width="15%">15-21 Hari</th>
        <th width="15%"> > 21 Hari</th>
        <th width="15%">Jumlah</th>
    </tr>
    </thead>
    <tbody>
    @php
        $count = 0;
    @endphp
    @foreach($list as $r)
        @php
            $count += 1;
        @endphp
        <tr>
            <td width="15%" style="text-align: left">{{ $r->langganan }}</td>
            <td width="15%" style="text-align: right">{{ $r->sebelumnya }}</td>
            <td width="15%" style="text-align: right">{{ $r->minggu_pertama }}</td>
            <td width="15%" style="text-align: right">{{ $r->minggu_kedua }}</td>
            <td width="15%" style="text-align: right">{{ $r->minggu_ketiga }}</td>
            <td width="15%" style="text-align: right">{{ $r->seterusnya }}</td>
            <td width="15%" style="text-align: right">{{$r->jumlah }}</td>
        </tr>
    @endforeach
    </tbody>
    @if($count == 0)
        <tfoot>
        <tr>
            <td colspan="7"> Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>