<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Penjualan " . date('d-m-Y').".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN PENJUALAN<br>
            Tanggal : {{\app\Helpers\Main::format_date(now())}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <th width="10%">No Faktur</th>
        <th width="10%">Tanggal</th>
        <th width="10%">Jatuh Tempo</th>
        <th width="10%">Lokasi</th>
        <th width="10%">Langganan</th>
        <th width="15%">Salesaman</th>
        <th width="10%">Nilai</th>
        <th width="10%">Bayar / DN/ KN</th>
        <th width="10%">Sisa</th>
    </tr>
    </thead>
    <tbody>
    @php($a=null)
    @foreach($list as $r)
        @php($a = $a + 1)
        <tr>
            <td width="10%">{{ $r->no_penjualan }}</td>
            <td width="10%">{{Main::format_date($r->tgl_penjualan) }}</td>
            <td width="10%">{{Main::format_date($r->jatuh_tempo) }}</td>
            <td width="10%">{{$r->lokasi }}</td>
            <td width="10%">{{$r->langganan }}</td>
            <td width="15%">{{$r->salesman }}</td>
            <td width="10%" style="text-align: right">{{$r->total_penjualan }}</td>
            <td width="10%" style="text-align: right">{{$r->terbayar }}</td>
            <td width="10%" style="text-align: right">{{$r->sisa_piutang }}</td>
        </tr>
    @endforeach
    </tbody>
    @if($a == null)
        <tfoot>
        <tr>
            <td colspan="9">Data Kosong</td>
        </tr>
        </tfoot>
    @endif
</table>