<?php
header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Saldo Supplier " . $tanggal_filter. ".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN SALDO SUPPLIER<br>
            Wilayah :
            @if(is_array($nama_wilayah))
                @foreach($nama_wilayah as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_wilayah}}
            @endif<br>
            Supplier :
            @if(is_array($nama_supplier))
                @foreach($nama_supplier as $key => $value)
                    @if($key == 0) {{$value}} @else , {{$value}} @endif
                @endforeach
            @else
                {{$nama_supplier}}
            @endif<br>
            Tanggal {{$tanggal_filter}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1">
    <thead>
    <tr>
        <th>Kode Supplier</th>
        <th>Nama Supplier</th>
        <th>Wilayah</th>
        <th>Saldo</th>
        <th colspan="2">Saldo Rp.</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $r)
        <tr>
            <td align="left">{{ $r->kode_supplier }}</td>
            <td align="left">{{ $r->supplier }}</td>
            <td align="left">{{ $r->wilayah }}</td>
            <td align="right">{{ $r->saldo }}</td>
            <td align="right" colspan="2">{{ $r->saldo }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="4"></td>
        <td class="item" align="right" colspan="2">{{$total_saldo}}</td>
    </tr>
    </tfoot>
</table>