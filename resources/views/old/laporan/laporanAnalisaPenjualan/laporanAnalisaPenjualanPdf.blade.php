<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Analisa Penjualan</font><br>
                        Golongan : {{$nama_golongan}}<br>
                        Lokasi : {{$nama_lokasi}}<br>
                        Tanggal {{$tanggal_start}} S/d {{$tanggal_end}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Golongan</th>
                <th class="item">Barang</th>
                <th class="item">Kode</th>
                <th class="item">Qty Jual</th>
                <th class="item">Nilai Jual</th>
                <th class="item">Nilai + PPN</th>
            </tr>
            </thead>
            <tbody>
            @php($golongan_saved = "")
            @foreach($list as $r)
                <tr class="service">
                    @if($golongan_saved != $r->golongan)
                        <td class="tableitem" align="right"><p class="itemtext">{{ $r->golongan }}</p></td>
                    @else
                        <td colspan="1"></td>
                    @endif
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->barang }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->kode_barang }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->qty_jual }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_jual }}</p></td>
                    <td class="tableitem" align="right"><p class="itemtext">{{ $r->nilai_ppn }}</p></td>
                </tr>
                @php( $golongan_saved = $r->golongan)
            @endforeach
            </tbody>
        </table>
    </div>
</div>