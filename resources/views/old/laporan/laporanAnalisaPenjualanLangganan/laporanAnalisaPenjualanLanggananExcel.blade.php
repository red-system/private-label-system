<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Analisa Penjualan Langganan  " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PENJUALAN LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<hr>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" class="table-data">
    <thead>
    <tr>
        <th width="15%">Wilayah</th>
        <th width="15%">Langganan</th>
        <th width="15%">Penjualan</th>
        <th width="15%">Retur</th>
        <th width="15%">Net Penjualan</th>
        <th width="15%">Pembayaran</th>
    </tr>
    </thead>
</table>
<hr>
@foreach($wilayah as $id)
    <table style="margin-bottom: 10px" width="100%" border="1" class="table-data">
        <tbody>
        @php
            $total_penjualan = 0;
            $total_retur = 0;
            $total_net_penjualan = 0;
            $total_pembayaran = 0;
        @endphp
        @foreach($list as $r)
            @if($r->id_wilayah == $id->id_wilayah)
                @php
                    $total_penjualan += $r->grand_total_penjualan;
                    $total_retur += $r->retur;
                    $total_net_penjualan += $r->net_penjualan;
                    $total_pembayaran += $r->pembayaran;
                @endphp
                <tr>
                    <td width="15%">{{ $r->wilayah }}</td>
                    <td width="15%">{{ $r->langganan }}</td>
                    <td width="15%" style="text-align: right">
                        {{\app\Helpers\Main::format_money($r->grand_total_penjualan) }}</td>
                    @if($r->retur == 0)
                        <td width="15%"></td>
                    @else
                        <td width="15%" style="text-align: right">
                            {{\app\Helpers\Main::format_money($r->retur) }}</td>
                    @endif
                    <td width="15%" style="text-align: right">
                        {{\app\Helpers\Main::format_money($r->net_penjualan) }}</td>
                    @if($r->pembayaran == 0)
                        <td width="15%"></td>
                    @else
                        <td width="15%" style="text-align: right">
                            {{\app\Helpers\Main::format_money($r->pembayaran) }}</td>
                    @endif
                </tr>
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td style="text-align: right">
                <hr>
                {{\app\Helpers\Main::format_money($total_penjualan)}}</td>
            @if($total_retur == 0)
                <td></td>
            @else
                <td style="text-align: right">
                    <hr>
                    {{\app\Helpers\Main::format_money($total_retur)}}</td>
            @endif
            <td style="text-align: right">
                <hr>
                {{\app\Helpers\Main::format_money($total_net_penjualan)}}</td>
            @if($total_pembayaran == 0)
                <td></td>
            @else
                <td style="text-align: right">
                    <hr>
                    {{\app\Helpers\Main::format_money($total_pembayaran)}}</td>
            @endif
        </tr>
        </tfoot>
    </table>
    <div style="border-bottom:2px dashed;"></div>
    <br>
@endforeach