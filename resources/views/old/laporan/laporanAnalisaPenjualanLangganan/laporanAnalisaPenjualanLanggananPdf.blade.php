<style type="text/css">
    .table-data {
        border-left: 0;
        border-right: 0;
        border-top: 0;
        border-bottom: 0;
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0;
        border-top: 0;
        border-bottom: 0;
        padding: 2px 4px;
        text-align: center;
        font-size: 12px;
        font-weight: normal;
        font-family: Roboto;
    }
</style>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LAPORAN ANALISA PENJUALAN LANGGANAN<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<hr>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" class="table-data">
    <thead>
    <tr>
        <th width="15%">Wilayah</th>
        <th width="15%">Langganan</th>
        <th width="15%">Penjualan</th>
        <th width="15%">Retur</th>
        <th width="15%">Net Penjualan</th>
        <th width="15%">Pembayaran</th>
    </tr>
    </thead>
</table>
<hr>
@foreach($wilayah as $id)
    <table style="margin-bottom: 10px" width="100%" border="1" class="table-data">
        <tbody>
        @php
            $total_penjualan = 0;
            $total_retur = 0;
            $total_net_penjualan = 0;
            $total_pembayaran = 0;
        @endphp
        @foreach($list as $r)
            @if($r->id_wilayah == $id->id_wilayah)
                @php
                    $total_penjualan += $r->grand_total_penjualan;
                    $total_retur += $r->retur;
                    $total_net_penjualan += $r->net_penjualan;
                    $total_pembayaran += $r->pembayaran;
                @endphp
                <tr>
                    <td width="15%">{{ $r->wilayah }}</td>
                    <td width="15%">{{ $r->langganan }}</td>
                    <td width="15%" style="text-align: right">
                        {{\app\Helpers\Main::format_money($r->grand_total_penjualan) }}</td>
                    @if($r->retur == 0)
                        <td width="15%"></td>
                    @else
                        <td width="15%" style="text-align: right">
                            {{\app\Helpers\Main::format_money($r->retur) }}</td>
                    @endif
                    <td width="15%" style="text-align: right">
                        {{\app\Helpers\Main::format_money($r->net_penjualan) }}</td>
                    @if($r->pembayaran == 0)
                        <td width="15%"></td>
                    @else
                        <td width="15%" style="text-align: right">
                            {{\app\Helpers\Main::format_money($r->pembayaran) }}</td>
                    @endif
                </tr>
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td style="text-align: right">
                <hr>
                {{\app\Helpers\Main::format_money($total_penjualan)}}</td>
            @if($total_retur == 0)
                <td></td>
            @else
                <td style="text-align: right">
                    <hr>
                    {{\app\Helpers\Main::format_money($total_retur)}}</td>
            @endif
            <td style="text-align: right">
                <hr>
                {{\app\Helpers\Main::format_money($total_net_penjualan)}}</td>
            @if($total_pembayaran == 0)
                <td></td>
            @else
                <td style="text-align: right">
                    <hr>
                    {{\app\Helpers\Main::format_money($total_pembayaran)}}</td>
            @endif
        </tr>
        </tfoot>
    </table>
    <div style="border-bottom:2px dashed;"></div>
    <br>
@endforeach