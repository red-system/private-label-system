<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Omzet Salesman Per Barang " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            OMZET SALESMAN PER BARANG<br>
            Tanggal : {{ $date_start }} s/d {{$date_end}}
        </td>
    </tr>
    </tbody>
</table>
<br>
<br>
<hr>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" class="table-data">
    <thead>
    <tr>
        <th width="15%">Kode Barang</th>
        <th width="15%">Nama Barang</th>
        <th width="15%">Qty</th>
        <th width="15%">Harga Jual</th>
        <th width="15%">Subtotal</th>
        <th width="20%">Salesman</th>
    </tr>
    </thead>
</table>
<hr>
@foreach($id_barang as $id)
    <table style="margin-bottom: 10px" width="100%" border="1" class="table-data">
        <tbody>
        @php
            $total = 0;
            $barang = 0;
        @endphp
        @foreach($list as $r)
            @if($r->id_barang == $id->id_barang)
                @php
                    $total = $total + $r->subtotal;
                    $barang = $barang + $r->total_qty;
                @endphp
                <tr>
                    <td width="15%">{{ $r->nama_barang }}</td>
                    <td width="15%">{{$r->kode_barang }}</td>
                    <td width="15%" style="text-align: right">{{\app\Helpers\Main::format_decimal($r->total_qty).' '.$r->satuan }}</td>
                    <td width="15%">{{\app\Helpers\Main::format_money($r->harga_net) }}</td>
                    <td width="15%" style="text-align: right">{{\app\Helpers\Main::format_money($r->subtotal) }}</td>
                    <td width="20%" style="text-align: right">{{$r->salesman}}</td>
                </tr>

                @php
                    $satuan = $r->satuan;
                @endphp
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td style="text-align: right">
                <hr>{{\app\Helpers\Main::format_decimal($barang).' '.$satuan}}</td>
            <td></td>
            <td style="text-align: right">
                <hr>{{\app\Helpers\Main::format_money($total)}}</td>
            <td></td>
        </tr>
        </tfoot>
    </table>
    <div style="border-bottom:2px dashed;"></div>
    <br>
@endforeach