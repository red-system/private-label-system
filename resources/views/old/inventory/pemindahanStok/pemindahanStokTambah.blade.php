@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/appSelect.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    @include('old.inventory.pemindahanStok.pemindahanStok_add');
    @include('old.inventory.pemindahanStok.pemindahanStok_edit');


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">{{ $pageTitle }}</h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Data Pemindahan Stok
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit" id="form-pemindahan-stok"
                          action="{{ route('pemindahanStokInsert') }}" method="post">
                        @csrf
                        <div class="m-portlet__body padding-top-0">
                            <div class="row" id="row-pemindahan-stok">
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group">
                                        <label>No Pemindahan</label>
                                        <input type="text" class="form-control m-input" name="no_pemindahan"
                                               placeholder="No Pemindahan" value="">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>Tanggal</label>
                                        <div class="input-group date">
                                            <input type="text" name="tgl_pemindahan" class="form-control m-input"
                                                   id="m_datepicker_5" value="{{date('d-m-Y')}}">
                                            <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar"></i>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group row">
                                        <label>Dari</label>
                                        <select class="form-control m-select2 m_select2_1" name="asal_barang"
                                                id="select_dari">
                                            <option value="" selected>Pilih Lokasi</option>
                                            @foreach ($lokasi as $item)
                                                <option value="{{$item['id']}}"
                                                        data-no="{{$item['lokasi']}}">@php echo ucwords($item['lokasi']) @endphp</option>
                                            @endforeach
                                        </select>
                                        {{-- <input type="text" id="select_url"> --}}
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label>Ke</label>
                                        <select class="form-control m-select2 m_select2_1 required"
                                                name="tujuan_barang">
                                            <option value="" selected>Pilih Lokasi</option>
                                            @foreach ($lokasi as $item)
                                                <option value="{{$item['id']}}">@php echo ucwords($item['lokasi']) @endphp</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group">
                                        <label>Keterangan</label>
                                        <textarea class="form-control m-input" id="exampleTextarea" name="keterangan"
                                                  rows="7" placeholder="Keterangan"></textarea>
                                    </div>
                                </div>
                                <input type="text" id="total-item-pemindahan-stok" name="totalItem" class="hidden">
                                <input type="submit" id='submit-form-pemindahan' class="hidden">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">

                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" data-toggle="modal" data-target="#pemindahan_stok_add"
                                   class="btn btn-info m-btn m-btn--custom">
                                    <span>
                                        <i class="la la-plus-circle"></i>
                                        <span>Tambah Data</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table tablePindah table-striped- table-bordered table-hover table-checkable"
                           id="listBarangPindah">
                        <textarea type="text" id="json_satuan" class="row-data hidden">{{$json_satuan}}</textarea>
                        <thead>
                        <tr>
                            <th width="250">Kode Produk</th>
                            <th>Nama Produk</th>
                            <th width="100">Qty</th>
                            <th width="180">Satuan</th>
                            <th width="60">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-lg-6 offset-lg-6">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total
                                            Persediaan</label>
                                        <div class="col-8">
                                            <input type="text" id="input-total-persediaan" class="form-control m-input"
                                                   name="" placeholder="Total Persediaan" value="0" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="modal-footer modal--button-custom">
                        <label type="button" for="submit-form-pemindahan" class="btn btn-primary">Save changes</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
