@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/appSelect.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('old.inventory.pemindahanStok.pemindahanStok_add');
    @include('old.inventory.pemindahanStok.pemindahanStok_edit');


    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">{{ $pageTitle }}</h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Data Pemindahan Stok
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit" id="form-pemindahan-stok"
                          action="{{ route('pemindahanStokInsertAccept', ['id'=>$pemindahan->id]) }}" method="post">
                        @csrf
                        <div class="m-portlet__body padding-top-0">
                            <div class="row" id="row-pemindahan-stok">
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group">
                                        <label>No Pemindahan</label>
                                        <input type="text" class="form-control m-input" name="no_pemindahan"
                                               placeholder="No Pemindahan" value="{{$pemindahan->no_pemindahan}}"
                                               readonly>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>Tanggal</label>
                                        <div class="input-group date">
                                            <input type="text" name="tgl_pemindahan" class="form-control m-input"
                                                   readonly id="m_datepicker_5"
                                                   value="{{$pemindahan->tgl_pemindahan}}"/>
                                            {{-- <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar"></i>
                                                </span>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group row">
                                        <label>Dari</label>
                                        <input type="hidden" name="asal_barang" value="{{$pemindahan->asal_barang}}" class="form-control m-input" readonly>
                                        <input type="text" value="{{$pemindahan->nama_asal_barang}}" class="form-control m-input" readonly>
                                        {{-- <select class="form-control m-select2 m_select2_1" name="asal_barang" id="select_dari">
                                            <option value="">Pilih Lokasi</option>
                                            @foreach ($lokasi as $item)
                                                @if ($item['lokasi'] == $pemindahan->asal_barang)
                                                    <option value="{{$item['lokasi']}}" selected>@php echo ucwords($item['lokasi']) @endphp</option>    
                                                @else
                                                    <option value="{{$item['lokasi']}}">@php echo ucwords($item['lokasi']) @endphp</option>
                                                @endif
                                            @endforeach
                                        </select> --}}
                                        {{-- <input type="text" id="select_url"> --}}
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label>Ke</label>
                                        <input type="hidden" name="tujuan_barang" value="{{$pemindahan->tujuan_barang}}" class="form-control m-input" readonly>
                                        <input type="text" value="{{$pemindahan->nama_tujuan_barang}}" class="form-control m-input" readonly>
                                        {{-- <select class="form-control m-select2 m_select2_1 required" name="tujuan_barang">
                                            <option value="" selected>Pilih Lokasi</option>
                                            @foreach ($lokasi as $item)
                                                @if ($item['lokasi'] == $pemindahan->tujuan_barang)
                                                    <option value="{{$item['lokasi']}}" selected>@php echo ucwords($item['lokasi']) @endphp</option>    
                                                @else
                                                    <option value="{{$item['lokasi']}}">@php echo ucwords($item['lokasi']) @endphp</option>
                                                @endif
                                            @endforeach
                                        </select> --}}
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group">
                                        <label>Keterangan</label>
                                        <textarea readonly class="form-control m-input" id="exampleTextarea"
                                                  name="keterangan" rows="7"
                                                  placeholder="Keterangan">{{$pemindahan->keterangan}}</textarea>
                                    </div>
                                </div>
                                <input type="text" id="total-item-pemindahan-stok" name="totalItem" class="hidden">
                                @foreach ($itemPemindahan as $index => $value)
                                    @php
                                        $index += 1;
                                    @endphp
                                    <div id="item-pemindahan-{{$index}}">
                                        <input type="hidden" id="item-{{$index}}-kode" name="item_kode[]" value="{{$value->kode_barang}}">
                                        <input type="hidden" id="item-{{$index}}-id-stok" name="item_id_stok[]" value="{{$value->id_stok}}">
                                        <input type="hidden" id="item-{{$index}}-id-barang" name="item_id_barang[]" value="{{$value->id_barang}}">
                                        <input type="hidden" id="item-{{$index}}-id-supplier" name="item_id_supplier[]" value="{{$value->id_supplier}}">
                                        <input type="hidden" id="item-{{$index}}-nama" name="item_nama[]" value="{{$value->nama_barang}}">
                                        <input type="hidden" id="item-{{$index}}-qty" name="item_qty[]" value="{{$value->jml_barang}}">
                                        <input type="hidden" id="item-{{$index}}-id-satuan" name="item_satuan[]" value="{{$value->id_satuan}}">
                                    </div>
                                @endforeach
                                <input type="submit" id='accept-form-pemindahan' class="hidden">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">

                        </div>
                    </div>
                    {{-- <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" data-toggle="modal" data-target="#pemindahan_stok_add" class="btn btn-info m-btn m-btn--custom">
                                    <span>
                                        <i class="la la-plus-circle"></i>
                                        <span>Tambah Data</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div> --}}
                </div>
                <div class="m-portlet__body">
                    <table class="table tablePindah tableAccept table-striped- table-bordered table-hover table-checkable"
                           id="listBarangPindah">
                        <textarea type="text" id="json_satuan" class="row-data hidden">{{$json_satuan}}</textarea>
                        <thead>
                        <tr>
                            <th width="250">Kode Produk</th>
                            <th>Nama Produk</th>
                            <th width="100">Qty</th>
                            <th width="180">Satuan</th>
                            <th width="60">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($itemPemindahan as $index => $value)
                            @php
                                $index += 1;
                            @endphp
                            <tr id="row-{{$index}}">
                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="kode-row-{{$index}}"
                                               placeholder="Kode Produk" value="{{$value->kode_barang}}" readonly>
                                        <input type="hidden" id="id-row-stok-{{$index}}" value="{{$value->id_stok}}">
                                        <input type="hidden" id="id-row-barang-{{$index}}"
                                               value="{{$value->id_barang}}">
                                        {{-- <div class="input-group-append">
                                            <a class="btn btn-primary btn-view" data-toggle="modal"  data-no="{{$index}}" data-target="#pemindahan_stok_edit"><i class="fa flaticon-search"></i></a>
                                        </div> --}}
                                    </div>
                                </td>
                                <td id="nama-row-{{$index}}">{{$value->nama_barang}}</td>
                                <td><input type="number" readonly id="qty-row-{{$index}}"
                                           class="form-control m-input qty-item" data-no="{{$index}}" placeholder="Qty"
                                           value="{{$value->jml_barang}}" max="{{$value->jml_stok}}"></td>
                                <td id="satuan-row-"{{$index}}>{{$value->satuan}}
                                    {{-- <select class="form-control m-select2 m_select2_1 satuan-item" id="satuan-row-{{$index}}" data-no="{{$index}}">
                                        <option id="option-satuan-0" value="">Pilih Satuan</option>
                                        @foreach ($satuan as $indexSatuan=>$valueSatuan)
                                            @php
                                                // $indexSatuan += 1;    
                                            @endphp
                                            @if ($value->satuan == $valueSatuan->satuan)
                                                <option id="option-satuan-{{$index.$indexSatuan}}" value="{{$valueSatuan->satuan}}" selected>{{$valueSatuan->satuan}}</option>
                                            @else 
                                                <option id="option-satuan-{{$index.$indexSatuan}}" value="{{$valueSatuan->satuan}}">{{$valueSatuan->satuan}}</option>
                                            @endif
                                        @endforeach
                                    </select> --}}

                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="First group">
                                        <a class="btn btn-danger btn-delete" id="barang{{$index}}" data-no="{{$index}}"><i
                                                    class="la la-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-lg-6 offset-lg-6">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total
                                            Persediaan</label>
                                        <div class="col-8">
                                            <input type="text" id="input-total-persediaan" class="form-control m-input"
                                                   name="" placeholder="Total Persediaan" value="0" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="modal-footer modal--button-custom">
                        <label type="button" for="accept-form-pemindahan" class="btn btn-success">Terima</label>
                        <a href="{{route('pemindahanStokDecline',['id'=>$pemindahan->id])}}">
                            <button type="button" class="btn btn-danger">Tolak</button>
                        </a>
                        <a href="{{route('pemindahanStokPage')}}">
                            <button type="button" class="btn btn-warning">Batal</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
