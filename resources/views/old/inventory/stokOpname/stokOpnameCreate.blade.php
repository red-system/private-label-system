<form action="" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-tambah-stok-opname" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group padding-top-15">
                            <label>Kode Produk <b class="label--required">*</b></label>
                            <input type="text" class="form-control m-input" id="" name="kode_barang"
                                   placeholder="Kode Produk" readonly>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Nama Produk <b class="label--required">*</b></label>
                            <input type="text" class="form-control m-input" id="" name="nama_barang"
                                   placeholder="Nama Produk" readonly>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-sm-12 align--left">Lokasi <b class="label--required">*</b></label>
                            <div class="col-sm-12">
                                <input type="hidden" id="base_value" data-base-url="{{env('APP_URL')}}">
                                <input type="hidden" id="id_barang" name="id_barang">
                                <input type="hidden" id="id_stok" name="id_stok">
                                <select class="form-control m-select2" name="id_lokasi" id="filter-lokasi-on-barang">
                                    <option>Pilih Lokasi</option>
{{--                                    @foreach ($lokasi as $k)--}}
{{--                                        <option value="{{$k->id}}">{{$k->lokasi}}</option>--}}
{{--                                    @endforeach--}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal <b class="label--required">*</b></label>
                            <input type="text" class="form-control m-input" name="tgl_opname" placeholder="Tanggal"
                                   id="m_datepicker_5" value="{{date('d-m-Y')}}">
                        </div>
                        <div class="form-group input-group">
                            <label>Qty Data <b class="label--required">*</b></label>

                            <div class="input-group">
                                <input type="number" class="form-control m-input" name="qty_data" placeholder="Qty Data"
                                       id="qty-data" min="0" step="0.01">
                                <div class="input-group-append">
                                    <input type="text" class="form-control m-input satuan" name="satuan_qty_data"
                                           readonly>
                                    <input type="hidden" name="id_satuan">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Qty Fisik <b class="label--required">*</b></label>

                            <div class="input-group">
                                <input type="number" class="form-control m-input" name="qty_fisik"
                                       placeholder="Qty Fisik" id="qty-fisik" min="0" step="0.01">
                                <div class="input-group-append">
                                    <input type="text" class="form-control m-input satuan" name="satuan_qty_fisik"
                                           readonly>
                                    <input type="hidden" name="id_satuan">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Selisih <b class="label--required">*</b></label>

                            <div class="input-group">
                                <input type="number" class="form-control m-input" name="selisih_qty"
                                       placeholder="Qty Selisih" id="qty-selisih" min="0" step="0.01">
                                <div class="input-group-append">
                                    <input type="text" class="form-control m-input satuan" name="selisih_satuan"
                                           readonly>
                                    <input type="hidden" name="id_satuan">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Keterangan <b class="label--required"></b></label>
                            <textarea class="form-control m-input" name="keterangan"> </textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
