@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/produksi.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("produksiPage") }}"
                       class="btn-produk-add btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar Produksi</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('produksiUpdate', ['id'=>$produksi->id]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('produksiPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}
                <input type="hidden"
                       name="no_produksi"
                       value="{{ $produksi->no_produksi }}">
                <input type="hidden"
                       name="no_produksi_label"
                       value="{{ $produksi->no_produksi_label }}">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-placeholder"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Data Produksi
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Kode
                                        Produksi</label>
                                    <div class="col-9 font-weight-bold">
                                        {{ $produksi->no_produksi_label }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Tanggal
                                        Produksi</label>
                                    <div class="col-9">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker_2"
                                                   name="tgl_produksi" readonly
                                                   value="{{ Main::format_date($produksi->tgl_produksi) }}"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Tanggal
                                        Expired</label>
                                    <div class="col-9">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker_2"
                                                   name="tanggal_expired" readonly
                                                   value="{{ Main::format_date($produksi->tanggal_expired) }}"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Keterangan /
                                        Catatan</label>
                                    <div class="col-9">
                                        <textarea class="form-control m-input" name="keterangan"
                                                  rows="1">{{ $produksi->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-box"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Barang Baku
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button"
                                            class="btn-barang-baku-add btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>Tambah Baku</span>
                                    </span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <table class="table-barang-baku table m-table m-table--head-separator-primary">
                                    <thead>
                                    <tr>
                                        <th>Kode/Nama Barang</th>
                                        <th>Harga</th>
                                        <th>Lokasi Stok</th>
                                        <th>Qty (Bahan Baku)</th>
                                        <th>Nilai Bahan Baku (Harga * Qty)</th>
                                        <th width="100">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($produksi_baku as $row)
                                        @php
                                            $jml_barang_select = 0;
                                        @endphp
                                        <tr>
                                            <td class="value-hidden hidden">
                                                <input type="text" name="id_produksi_baku[]" value="{{ $row->id }}">
                                                <input type="text" name="nama_barang_baku[]"
                                                       value="{{ $row->nama_barang }}">
                                                <input type="text" name="harga_barang_baku[]"
                                                       value="{{ $row->harga_barang }}">
                                                <input type="text" name="nilai_bahan_baku[]"
                                                       value="{{ $row->nilai_bahan_baku }}">
                                            </td>
                                            <td>
                                                <select class="form-control id-barang m-select2" name="id_barang_baku[]"
                                                        required style="width: 100%">
                                                    <option value="">Pilih Barang</option>
                                                    @foreach($barang as $r)
                                                        <option {{ $r->id == $row->id_barang ? 'selected' : '' }}
                                                                value="{{ $r->id }}"
                                                                data-kode-barang="{{ $r->kode_barang }}"
                                                                data-nama-barang="{{ $r->nama_barang }}"
                                                                data-harga-jual-barang="{{ $r->harga_jual_barang }}"
                                                                data-harga-beli-terakhir-barang="{{ $r->harga_beli_terakhir_barang }}">
                                                            {{ $r->kode_barang.' - '.$r->nama_barang }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="harga-barang">
                                                {{ Main::format_number($row->harga_barang) }}
                                            </td>
                                            <td class="lokasi">
                                                <select class="form-control m-select2" name="id_stok_baku[]" required
                                                        style="width: 100%">
                                                    <option value="">Pilih Lokasi Stok</option>
                                                    @foreach($row->stok as $row_stok)
                                                        @php
                                                            if($row_stok->id == $row->id_stok) {
                                                                $jml_barang_select = $row_stok->jml_barang;
                                                            }
                                                        @endphp
                                                        <option
                                                                value="{{ $row_stok->id }}"
                                                                {{ $row_stok->id == $row->id_stok ? 'selected':'' }}>
                                                            {{ $row_stok->lokasi->lokasi.' ('.$row_stok->jml_barang.' '.$row_stok->satuan.')' }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="qty">
                                                <input type="text"
                                                       class="form-control touchspin-number-decimal touchspin"
                                                       name="qty_baku[]" required value="{{ $row->qty }}"
                                                       max="{{ $row->qty + $jml_barang_select }}" style="width: 40px">
                                            </td>
                                            <td class="nilai-bahan">
                                                {{ Main::format_number($row->nilai_bahan_baku) }}
                                            </td>
                                            <td>
                                                <button type="button"
                                                        class="btn-hapus-barang btn m-btn--pill btn-danger btn-sm"
                                                        data-confirm="false">
                                                    <i class="la la-remove"></i> Hapus
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-bag"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Daftar Barang Hasil Produksi
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button"
                                            class="btn-barang-produksi-add btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>Tambah Barang</span>
                                    </span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <table class="table-barang-produksi table m-table m-table--head-separator-primary">
                                    <thead>
                                    <tr>
                                        <th>Kode/Nama Barang</th>
                                        <th>Lokasi Stok</th>
                                        <th>Qty (Barang Jadi)</th>
                                        <th>Satuan</th>
                                        <th>HPP</th>
                                        <th width="40">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($produksi_hasil as $row)
                                        <tr>
                                            <td class="value-hidden hidden">
                                                <input type="text" name="id_produksi_hasil[]" value="{{ $row->id }}">
                                                <input type="text" name="nama_barang_produksi[]"
                                                       value="{{ $row->nama_barang }}">
                                            </td>
                                            <td>
                                                <select class="form-control id-barang m-select2"
                                                        name="id_barang_produksi[]" required style="width: 100%">
                                                    <option value="">Pilih Barang</option>
                                                    @foreach($barang as $r)
                                                        <option {{ $r->id == $row->id_barang ? 'selected':'' }}
                                                                value="{{ $r->id }}"
                                                                data-kode-barang="{{ $r->kode_barang }}"
                                                                data-nama-barang="{{ $r->nama_barang }}"
                                                                data-harga-jual-barang="{{ $r->harga_jual_barang }}"
                                                                data-harga-beli-terakhir-barang="{{ $r->harga_beli_terakhir_barang }}">
                                                            {{ $r->kode_barang.' - '.$r->nama_barang }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="lokasi">
                                                <select class="form-control id-lokasi m-select2"
                                                        name="id_lokasi_produksi[]" required style="width: 100%">
                                                    <option value="">Pilih Lokasi</option>
                                                    @foreach($lokasi as $r)
                                                        <option {{ $r->id == $row->id_lokasi ? 'selected':'' }}
                                                                value="{{ $r->id }}">
                                                            {{ $r->lokasi }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="qty">
                                                <input type="text"
                                                       class="form-control touchspin-number-decimal touchspin"
                                                       name="qty_produksi[]" required value="{{ $row->qty }}" min="1"
                                                       style="width: 40px">
                                            </td>
                                            <td class="satuan">
                                                <select class="form-control m-select2" name="id_satuan_produksi[]"
                                                        required style="width: 100%">
                                                    <option value="">Pilih Satuan</option>
                                                    @foreach($satuan as $r)
                                                        <option {{ $r->id == $row->id_satuan ? 'selected':'' }}
                                                                value="{{ $r->id }}">
                                                            {{ $r->satuan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="hpp">
                                                <input type="text"
                                                       class="form-control touchspin-number-decimal touchspin"
                                                       name="hpp_produksi[]" required value="{{ $row->hpp }}" min="1"
                                                       style="width: 60px">
                                            </td>
                                            <td>
                                                <button type="button"
                                                        class="btn-hapus-barang btn m-btn--pill btn-danger btn-sm"
                                                        data-confirm="false">
                                                    <i class="la la-remove"></i> Hapus
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Perbarui Produksi</span>
                        </span>
                    </button>

                    <a href="{{ route("produksiPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar Produksi</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="row-barang-baku hidden">
        <table>
            <tr>
                <td class="value-hidden hidden">
                    <input type="text" name="id_produksi_baku[]">
                    <input type="text" name="nama_barang_baku[]">
                    <input type="text" name="harga_barang_baku[]">
                    <input type="text" name="nilai_bahan_baku[]">
                </td>
                <td>
                    <select class="form-control id-barang" name="id_barang_baku[]" required style="width: 100%">
                        <option value="">Pilih Barang</option>
                        @foreach($barang as $r)
                            <option value="{{ $r->id }}"
                                    data-kode-barang="{{ $r->kode_barang }}"
                                    data-nama-barang="{{ $r->nama_barang }}"
                                    data-harga-jual-barang="{{ $r->harga_jual_barang }}"
                                    data-harga-beli-terakhir-barang="{{ $r->harga_beli_terakhir_barang }}">
                                {{ $r->kode_barang.' - '.$r->nama_barang }}
                            </option>
                        @endforeach
                    </select>
                </td>
                <td class="harga-barang">
                    -
                </td>
                <td class="lokasi">
                    <select class="form-control" name="id_stok_baku[]" required style="width: 100%">
                        <option value="">Pilih Lokasi Stok</option>
                    </select>
                </td>
                <td class="qty">
                    <input type="text" class="form-control touchspin" name="qty_baku[]" required value="1"
                           style="width: 40px">
                </td>
                <td class="nilai-bahan">
                    -
                </td>
                <td>
                    <button type="button" class="btn-hapus-barang btn m-btn--pill btn-danger btn-sm"
                            data-confirm="false">
                        <i class="la la-remove"></i> Hapus
                    </button>
                </td>
            </tr>
        </table>
    </div>

    <div class="row-barang-produksi hidden">
        <table>
            <tr>
                <td class="value-hidden hidden">
                    <input type="text" name="id_produksi_hasil[]">
                    <input type="text" name="nama_barang_produksi[]">
                </td>
                <td>
                    <select class="form-control id-barang" name="id_barang_produksi[]" required style="width: 100%">
                        <option value="">Pilih Barang</option>
                        @foreach($barang as $r)
                            <option value="{{ $r->id }}"
                                    data-kode-barang="{{ $r->kode_barang }}"
                                    data-nama-barang="{{ $r->nama_barang }}"
                                    data-harga-jual-barang="{{ $r->harga_jual_barang }}"
                                    data-harga-beli-terakhir-barang="{{ $r->harga_beli_terakhir_barang }}">
                                {{ $r->kode_barang.' - '.$r->nama_barang }}
                            </option>
                        @endforeach
                    </select>
                </td>
                <td class="lokasi">
                    <select class="form-control id-lokasi" name="id_lokasi_produksi[]" required style="width: 100%">
                        <option value="">Pilih Lokasi</option>
                        @foreach($lokasi as $r)
                            <option value="{{ $r->id }}">
                                {{ $r->lokasi }}
                            </option>
                        @endforeach
                    </select>
                </td>
                <td class="qty">
                    <input type="text" class="form-control touchspin" name="qty_produksi[]" required value="1" min="1"
                           style="width: 40px">
                </td>
                <td class="satuan">
                    <select class="form-control" name="id_satuan_produksi[]" required style="width: 100%">
                        <option value="">Pilih Satuan</option>
                        @foreach($satuan as $r)
                            <option value="{{ $r->id }}">
                                {{ $r->satuan }}
                            </option>
                        @endforeach
                    </select>
                </td>
                <td class="hpp">
                    <input type="text" class="form-control touchspin" name="hpp_produksi[]" required value="1" min="1"
                           style="width: 60px">
                </td>
                <td>
                    <button type="button" class="btn-hapus-barang btn m-btn--pill btn-danger btn-sm"
                            data-confirm="false">
                        <i class="la la-remove"></i> Hapus
                    </button>
                </td>
            </tr>
        </table>
    </div>

@endsection