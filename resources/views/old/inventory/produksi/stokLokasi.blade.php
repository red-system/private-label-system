<form role="form" action="{{ route('stokPerlokasiInsert') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cetak Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="form-control-label required col-sm-12 align--left">Pilih Lokasi Stok Untuk
                                Dicetak</label>
                            <div class="col-sm-12">
                                <select class="form-control m-select2" name="id_lokasi">
                                    <option value="0">Pilih Lokasi</option>
                                    @foreach ($lokasi as $l)
                                        <option value="{{$l->id}}">{{$l->lokasi}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" formtarget="_blank" class="btn btn-success" a>Cetak</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>