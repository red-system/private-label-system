<h4>Detail Produksi</h4>
<hr/>
<div class="m-form">
    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first row data-detail-section">
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Kode Produksi</label>
                    <div class="col-lg-6 col-form-label">
                        {{ $produksi->no_produksi_label }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Tanggal Produksi</label>
                    <div class="col-lg-6 col-form-label">
                        {{ $produksi->tgl_produksi }}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Catatan</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $produksi->keterangan }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<h4>Barang Baku</h4>
<hr/>

<table class="table-barang-baku table m-table m-table--head-separator-primary">
    <thead>
    <tr>
        <th>Kode/Nama Barang</th>
        <th>Harga</th>
        <th>Lokasi Stok</th>
        <th>Qty (Bahan Baku)</th>
        <th>Nilai Bahan Baku (Harga * Qty)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($produksi_baku as $row)
        @php
            $jml_barang_select = 0;
        @endphp
        <tr>
            <td>{{ $row->nama_barang }}</td>
            <td>{{ Main::format_number($row->harga_barang) }}</td>
            <td>{{ $row->lokasi->lokasi }}</td>
            <td>{{ $row->qty }}</td>
            <td>{{ Main::format_number($row->nilai_bahan_baku) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<br/>
<h4>Barang Hasil Produksi</h4>
<hr/>
<table class="table-barang-produksi table m-table m-table--head-separator-primary">
    <thead>
    <tr>
        <th>Kode/Nama Barang</th>
        <th>Lokasi Stok</th>
        <th>Qty (Barang Jadi)</th>
        <th>Satuan</th>
        <th>HPP</th>
    </tr>
    </thead>
    <tbody>
    @foreach($produksi_hasil as $row)
        <tr>
            <td>{{ $row->nama_barang }}</td>
            <td>{{ $row->lokasi->lokasi }}</td>
            <td>{{ $row->qty }}</td>
            <td>{{ $row->satuan->satuan }}</td>
            <td>{{ $row->hpp }} </td>
        </tr>
    @endforeach
    </tbody>
</table>