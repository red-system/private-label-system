<div>
    <br></br>
</div>
<div>
    <br></br>
</div>
<div>
    <br></br>
</div>
<div>
    <br></br>
</div>
<div>
    <br></br>
</div>
<center>
    <h3 class="box-title">Data Stok di {{$lokasi}}</h3>
</center>
<div class="box-body">
    <table rules="all" border="1" cellpadding="5">
        <tr>
            <th>
                <center>No</center>
            </th>
            <th>
                <center>Nama Barang</center>
            </th>
            <th>
                <center>Jumlah Barang</center>
            </th>
            <th>
                <center>Satuan</center>
            </th>
            <th>
                <center>Harga Pokok</center>
            </th>
            <th>
                <center>Supplier</center>
            </th>
        </tr>
        @php
            $no = 1;
        @endphp
        @foreach($stok as $d)
            <tr>
                <th>
                    <center>{{$no++}}</center>
                </th>
                <th>
                    <center>{{$d->nama_barang}}</center>
                </th>
                <th>
                    <center>{{$d->jml_barang}}</center>
                </th>
                <th>
                    <center>{{$d->satuan}}</center>
                </th>
                <th>
                    <center>{{$d->hpp}}</center>
                </th>
                <th>
                    <center>{{$d->supplier}}</center>
                </th>
            </tr>
        @endforeach
    </table>
</div>