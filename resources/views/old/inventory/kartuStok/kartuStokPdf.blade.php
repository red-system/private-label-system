<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Kartu Stok Produk</font><br>
                        Lokasi : {{ $nama_lokasi }}<br>
                        Tanggal : {{ Carbon\Carbon::parse($date_start)->format('d-m-Y') }}
                        s/d {{ Carbon\Carbon::parse($date_end)->format('d/m/Y') }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
            <table width="100%">
                <thead class="tabletitle">
                <tr>
                    <th class="item">Lokasi</th>
                    <th class="item">Nama Produk</th>
                    <th class="item">Tanggal Transit</th>
                    <th class="item">Keterangan</th>
                    <th class="item">Stok Awal</th>
                    <th class="item">HPP Awal</th>
                    <th class="item">Masuk</th>
                    <th class="item">HPP Masuk</th>
                    <th class="item">Keluar</th>
                    <th class="item">HPP Keluar</th>
                    <th class="item">Stok Akhir</th>
                    <th class="item">SST/SRPL</th>
                    <th class="item">HPP Stok</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $a)
                    <tr>
                        <td class="item">{{ $a->lokasi }}</td>
                        <td class="item">{{ '('.$a->kode_barang.') '.$a->nama_barang }}</td>
                        <td class="item">{{\Carbon\Carbon::parse($a['tgl_transit'])->format('d-m-Y')}}</td>
                        <td class="item">{{ $a->keterangan }}</td>
                        <td class="item">{{Main::format_number( $a->stok_awal )}}</td>
                        <td class="item">{{Main::format_number( $a->hpp_awal )}}</td>
                        @if($a->stok_masuk == null)
                            <td class="item"></td>
                            <td class="item"></td>
                        @else
                            <td class="item">{{Main::format_number( $a->stok_masuk )}}</td>
                            <td class="item">{{Main::format_number( $a->hpp_masuk )}}</td>
                        @endif
                        @if($a->stok_keluar == null)
                            <td class="item"></td>
                            <td class="item"></td>
                        @else
                            <td class="item">{{Main::format_number( $a->stok_keluar )}}</td>
                            <td class="item">{{Main::format_number( $a->hpp_keluar )}}</td>
                        @endif

                        <td class="item">{{ $a->stok_akhir }}</td>
                        <td class="item">{{ $a->sst_sprl }}</td>
                        <td class="item">{{Main::format_number( $a->hpp_stok )}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    </div>
</div>