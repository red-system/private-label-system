<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Kartu Stok " . $date_start ." sampai ". $date_end.".xls");
?>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            KARTU STOK PRODUK<br>
            Lokasi : {{ $nama_lokasi }}<br>
            Tanggal : {{ $date_start }}
            s/d {{ $date_end }}
        </td>
    </tr>
    </tbody>
</table>
<br/>
<br/>
<table width="100%" border="1" class="table-data">
    <caption>

    </caption>
    <thead>
    <tr>
        <th>Lokasi</th>
        <th>Nama Produk</th>
        <th>Tanggal Transit</th>
        <th>Keterangan</th>
        <th>Stok Awal</th>
        <th>HPP Awal</th>
        <th>Masuk</th>
        <th>HPP Masuk</th>
        <th>Keluar</th>
        <th>HPP Keluar</th>
        <th>Stok Akhir</th>
        <th>SST/SRPL</th>
        <th>HPP Stok</th>
        {{--        <th>Tanggal</th>--}}
        {{--        @if($view_barang == 'all')--}}
        {{--            <th>Produk</th>--}}
        {{--        @endif--}}
        {{--        <th>Kode Produksi</th>--}}
        {{--        <th>Masuk</th>--}}
        {{--        @if($view_lokasi == 'all')--}}
        {{--            <th>Lokasi</th>--}}
        {{--        @endif--}}
        {{--        <th>Keterangan</th>--}}
        {{--        <th>Tanggal</th>--}}
        {{--        @if($view_barang == 'all')--}}
        {{--            <th>Produk</th>--}}
        {{--        @endif--}}
        {{--        <th>Kode Produksi</th>--}}
        {{--        <th>Keluar</th>--}}
        {{--        @if($view_lokasi == 'all')--}}
        {{--            <th>Lokasi</th>--}}
        {{--        @endif--}}
        {{--        <th>Penerima/Keterangan</th>--}}
        {{--        <th>Stok Akhir</th>--}}
    </tr>
    </thead>
    <tbody>
    @foreach($list as $a)
        <tr>
            <td>{{ $a->lokasi }}</td>
            <td>{{ '('.$a->kode_barang.') '.$a->nama_barang }}</td>
            <td>{{\Carbon\Carbon::parse($a['tgl_transit'])->format('d-m-Y')}}</td>
            <td>{{ $a->keterangan }}</td>
            <td>{{Main::format_number( $a->stok_awal )}}</td>
            <td>{{Main::format_number( $a->hpp_awal )}}</td>
            @if($a->stok_masuk == null)
                <td></td>
                <td></td>
            @else
                <td>{{Main::format_number( $a->stok_masuk )}}</td>
                <td>{{Main::format_number( $a->hpp_masuk )}}</td>
            @endif
            @if($a->stok_keluar == null)
                <td></td>
                <td></td>
            @else
                <td>{{Main::format_number( $a->stok_keluar )}}</td>
                <td>{{Main::format_number( $a->hpp_keluar )}}</td>
            @endif

            <td>{{ $a->stok_akhir }}</td>
            <td>{{ $a->sst_sprl }}</td>
            <td>{{Main::format_number( $a->hpp_stok )}}</td>
        </tr>
    @endforeach

    </tbody>
</table>