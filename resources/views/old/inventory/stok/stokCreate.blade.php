<form action="{{ route('stokInsert',['id'=>$id]) }}" method="post" data-alert-show="true"
      data-alert-field-message="true" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="form-control-label col-3">Kode Produk</label>
                            <label class="form-control-label col-1">:</label>
                            <label class="form-control-label col-8" id="id_produk">{{$produk->kode_barang}}</label>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="form-control-label col-3">Nama Produk</label>
                            <label class="form-control-label col-1">:</label>
                            <label class="form-control-label col-8"
                                   id="kode_package_edit">{{$produk->nama_barang}}</label>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="form-control-label required col-sm-12 align--left">Lokasi</label>
                            <div class="col-sm-12">
                                <select class="form-control m-select2" name="id_lokasi" style="width: 100%;opacity: 1;">
                                    <option value="">Pilih Lokasi</option>
                                    @foreach ($lokasi as $l)
                                        <option value="{{$l->id}}">{{$l->lokasi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required col-sm-12 align--left">Jumlah </label>
                            <input type="text" class="form-control m-input bootstrap-touchspin input-numeral" id=""
                                   name="jml_barang" placeholder="Masukan Jumlah">
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="form-control-label required col-sm-12 align--left">Satuan </label>
                            <div class="col-sm-12">
                                <select class="form-control m-select2" name="id_satuan" style="width: 100%;opacity: 1;">
                                    <option value="">Pilih Satuan</option>
                                    @foreach ($satuan as $s)
                                        <option value="{{$s->id}}">{{$s->satuan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required col-sm-12 align--left">HPP </label>
                            <input type="text" class="form-control m-input input-numeral" id="" name="hpp"
                                   placeholder="Masukan Jumlah">
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="form-control-label required col-sm-12 align--left">Supplier </label>
                            <div class="col-sm-12">
                                <select class="form-control m-select2" name="id_supplier"
                                        style="width: 100%;opacity: 1;">
                                    <option value="">Pilih Supplier</option>
                                    @foreach ($supplier as $su)
                                        <option value="{{$su->id}}">{{$su->supplier}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="form-control-label required col-sm-12 align--left">Tanggal Kadaluarsa </label>
                            <div class="col-sm-12">
                                <div class="input-group date">
                                    <input type="text" name="date_expired" class="form-control m-input"
                                           id="m_datepicker_4_3"
                                           readonly="" value="">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
