<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Laporan Stok Per Lokasi</font><br>
                        Lokasi : {{$lokasi}}<br>
                        Tanggal : {{\app\Helpers\Main::format_date(now())}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        @if($lokasi == 'Semua Lokasi')
            @foreach($lokasi_all as $all)
                <span style="padding: 10px; line-height: normal; margin-bottom: 30px" class="item">{{$all->lokasi}}</span>
{{--                <br style="line-height: 30px">--}}
                <table width="100%">
                    <thead class="tabletitle">
                    <tr>
                        <th class="item">No</th>
                        <th class="item">Nama Barang</th>
                        <th class="item">Lokasi</th>
                        <th class="item">Wilayah</th>
                        <th class="item">Jumlah Barang</th>
                        <th class="item">Satuan</th>
                        <th class="item">Supplier</th>
                        <th class="item">Tanggal Kadaluarsa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($no = 1)
                    @foreach($list as $r)
                        @if($all->id == $r->id_lokasi)
                            <tr>
                                <td class="item">{{ $no }}</td>
                                <td class="item">{{$r->nama_barang }}</td>
                                <td class="item">{{$r->lokasi }}</td>
                                <td class="item">{{$r->wilayah}}</td>
                                <td class="item">{{$r->jml_barang}}</td>
                                <td class="item">{{$r->satuan }}</td>
                                <td class="item">{{$r->supplier}}</td>
                                <td class="item">{{$r->date_expired}}</td>
                            </tr>
                            @php($no = $no + 1)
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <br>
                <br>
            @endforeach
        @else
            <table width="100%">
                <thead class="tabletitle">
                <tr>
                    <th class="item">No</th>
                    <th class="item">Nama Barang</th>
                    <th class="item">Lokasi</th>
                    <th class="item">Wilayah</th>
                    <th class="item">Jumlah Barang</th>
                    <th class="item">Satuan</th>
                    <th class="item">Supplier</th>
                    <th class="item">Tanggal Kadaluarsa</th>
                </tr>
                </thead>
                <tbody>
                @php($no = 1)
                @foreach($list as $r)
                    <tr>
                        <td class="item">{{ $no }}</td>
                        <td class="item">{{$r->nama_barang }}</td>
                        <td class="item">{{$r->lokasi }}</td>
                        <td class="item">{{$r->wilayah}}</td>
                        <td class="item">{{$r->jml_barang}}</td>
                        <td class="item">{{$r->satuan }}</td>
                        <td class="item">{{$r->supplier}}</td>
                        <td class="item">{{$r->date_expired}}</td>
                    </tr>
                    @php($no = $no + 1)
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>