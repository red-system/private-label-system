<form action="{{ route('produkInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group padding-top-15">
                            <label class="required">Kode Produk</label>
                            <input type="text" class="form-control m-input" id="" name="kode_barang"
                                   placeholder="Kode Produk">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="required">Nama Produk</label>
                            <input type="text" class="form-control m-input" id="" name="nama_barang"
                                   placeholder="Nama Produk">
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-sm-12 align--left required">Golongan</label>
                            <div class="col-sm-12">
                                <select class="form-control m-select2" name="id_golongan"
                                        style="width: 100%;opacity: 1;">
                                    <option value="">Pilih Golongan</option>
                                    @foreach ($golongan as $k)
                                        <option value="{{$k->id}}">{{$k->golongan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="required">Harga Pokok</label>
                            <input type="text" class="form-control m-input input-numeral" name="hpp"
                                   placeholder="Harga Pokok">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="required">Harga Beli</label>
                            <input type="text" class="form-control m-input input-numeral"
                                   name="harga_beli_terakhir_barang" placeholder="Harga Beli">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="required">Harga Jual</label>
                            <input type="text" class="form-control m-input input-numeral" name="harga_jual_barang"
                                   placeholder="Harga Jual">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="required">Minimal Stok</label>
                            <input type="text" class="form-control m-input input-numeral" name="minimal_stok"
                                   placeholder="Minimal Stok">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
