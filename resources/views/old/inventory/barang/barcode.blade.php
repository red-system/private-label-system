<html lang="en" dir="ltr">
<script type="text/javascript" charset="utf-8" id="zm-extension"
        src="chrome-extension://fdcgdnkidjaadafnichfpabhfomcebme/scripts/webrtc-patch.js" async=""></script>
<head>
    <meta charset="utf-8">
    <style>
        p {
            font-family: Roboto;
            font-size: 12px;
            padding: 0 !important;
            margin: 0 !important;
        }

         .barcode {
             padding: 1.5mm;
             margin: 0;
             vertical-align: top;
         }

    </style>

</head>
<body>
<div id="div-barcode" class="container text-center" style="border: 0px solid; width: 114mm;">
    <div class="barcode" style="border: 0px solid;">
        <table>
            <tr>
                <td colspan="3" align="center">
                    <p>{{$barang->nama_barang}}</p>

                </td>
            </tr>
            <tr>
                <td class="center" colspan="3" align="center">
                    <div class="logo">
{{--                        <img src="{!! $path !!}">--}}
                        {!! $barcode !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                        <p>{{$barang->kode_barang}}</p>
                </td>
            </tr>

            <tr>
                <td style="text-align: left" colspan="2">
                    <p><b>{{Main::format_money($barang->harga_jual_barang)}}</b></p>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>