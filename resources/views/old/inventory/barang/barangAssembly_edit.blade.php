<div class="modal fade" id="pemindahan_stok_edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cari Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group padding-top-15">
                        {{-- <label>Cari</label>
                        <input type="text" class="form-control m-input" id="" name="" placeholder="Masukkan keyword"> --}}
                    </div>
                    <div class="form-group m-form__group padding-top-15 padding-bottom-15">
                        <input type="hidden" id="base_value" data-base-url="{{env('APP_URL')}}">
                        <input type="hidden" id="select_url" data-select-url="{{route('produkAssemblySelectBase')}}">
                        @csrf
                        <div id="table_coloumn" style="display: none">{{$produkList}}</div>
                        <div id="action_list" style="display: none">{{$actionButton}}</div>
                        <table class="tableBarang table table-striped- table-bordered table-hover table-checkable datatable-general table-edit">
                            <thead>
                            <tr>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Golongan</th>
                                <th>Satuan</th>
                                <th width="100">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>