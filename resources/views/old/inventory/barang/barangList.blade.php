@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    @include('old.inventory.barang.barangCreate')
    @include('old.inventory.barang.barangEdit')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('produkList')}}">
        <div id="table_coloumn" style="display: none">{{$view}}</div>
        <div id="action_list" style="display: none">{{$actionButton}}</div>
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                    </a>
                </div>
                <div>
                    <a href="{{ route('produkAssembly') }}"
                       class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                       style="margin-left:10px;">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Assembly Barang</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table
                            class="datatable table table-striped- table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Golongan</th>
                            <th>Harga Pokok</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th>Stok Minimal</th>
                            <th>Total Stok</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
