@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/appSelect.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    @include('old.inventory.barang.barangAssembly_add');
    @include('old.inventory.barang.barangAssembly_edit');

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">{{ $pageTitle }}</h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Data Assembly Produk
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit form-kirim"
                          data-alert-show="true"
                          data-alert-field-message="true"
                          data-redirect="{{ route('produkPage') }}"
                          id="form-pemindahan-stok"
                          action="{{ route('assemblyUpdate', ['id'=>$barang->id]) }}"
                          method="post">
                        @csrf
                        <div class="m-portlet__body padding-top-0">
                            <div class="row" id="row-pemindahan-stok">
                                <div class="col-lg-6">
                                    <div class="form-group m-form__group row">
                                        <label>Kode Produk Assembly</label>
                                        <input type="text" class="form-control m-input" name="kode_barang"
                                               placeholder="Kode Produk Assembly" value="{{$barang->kode_barang}}">
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label>Nama Produk</label>
                                        <input type="text" class="form-control m-input" name="nama_barang"
                                               placeholder="Nama Produk" value="{{$barang->nama_barang}}">
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label>Golongan</label>
                                        <select class="form-control m-select2 m_select2_1" name="id_golongan">
                                            <option value="">Pilih Golongan</option>
                                            @foreach ($golongan as $item)
                                                @if($item['id'] == $barang->id_golongan)
                                                    <option value="{{$item['id']}}" data-no="{{$item['golongan']}}"
                                                            selected>@php echo ucwords($item['golongan']) @endphp</option>
                                                @else
                                                    <option value="{{$item['id']}}"
                                                            data-no="{{$item['golongan']}}">@php echo ucwords($item['golongan']) @endphp</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group m-form__group">
                                        <label>Harga Beli</label>
                                        <input type="text" class="form-control m-input input-numeral conv-jumlah"
                                               name="harga_beli_terakhir_barang" data-hide="hide-beli-barang"
                                               placeholder="Harga Beli" value="" min="0"
                                               value="{{$barang->harga_beli_terakhir_barang}}">
                                        <input type="hidden" id="hide-beli-barang" name="harga_beli_terakhir_barang"
                                               placeholder="Harga Jual" value="{{$barang->harga_jual_barang}}" min="0">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>Harga Jual</label>
                                        <input type="text" class="form-control m-input input-numeral conv-jumlah"
                                               data-hide="hide-jual-barang" placeholder="Harga Jual"
                                               value="{{$barang->harga_jual_barang}}" min="0">
                                        <input type="hidden" id="hide-jual-barang" name="harga_jual_barang"
                                               placeholder="Harga Jual" value="{{$barang->harga_jual_barang}}" min="0">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>HPP</label>
                                        <input type="text" class="form-control m-input input-numeral conv-jumlah"
                                               data-hide="hide-hpp" placeholder="HPP" value="{{$barang->hpp}}" min="0">
                                        <input type="hidden" id="hide-hpp" name="hpp" placeholder="HPP"
                                               value="{{$barang->hpp}}" min="0">
                                    </div>
                                </div>
                                <input type="hidden" id="total-item-pemindahan-stok" name="total_item" class="hidden">
                                @foreach ($itemAssembly as $index => $value)
                                    @php
                                        $index += 1;
                                    @endphp
                                    <div id="item-pemindahan-{{$index}}">
                                        <input type="hidden" id="item-{{$index}}-kode" name="item_kode[]"
                                               value="{{$value->barang->kode_barang}}">
                                        <input type="hidden" id="item-{{$index}}-id-stok" name="item_id_stok[]"
                                               value="{{$value->id_stok}}">
                                        <input type="hidden" id="item-{{$index}}-id-barang" name="item_id_barang[]"
                                               value="{{$value->id_barang}}">
                                        <input type="hidden" id="item-{{$index}}-id-supplier" name="item_id_supplier[]"
                                               value="{{$value->id_supplier}}">
                                        <input type="hidden" id="item-{{$index}}-nama" name="item_nama[]"
                                               value="{{$value->barang->nama_barang}}">
                                        <input type="hidden" id="item-{{$index}}-qty" name="item_qty[]"
                                               value="{{$value->jumlah}}">
                                        <input type="hidden" id="item-{{$index}}-id-satuan" name="item_satuan[]"
                                               value="{{$value->id_satuan}}">
                                    </div>
                                @endforeach

                                <input type="submit" id='submit-form' class="hidden">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">

                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" data-toggle="modal" data-target="#pemindahan_stok_add"
                                   class="btn btn-info m-btn m-btn--custom">
                                    <span>
                                        <i class="la la-plus-circle"></i>
                                        <span>Tambah Data</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table tableAssembly table-striped- table-bordered table-hover table-checkable"
                           id="listBarangPindah">
                        {{--                        <textarea type="text" id="json_satuan" class="row-data hidden">{{$json_satuan}}</textarea>--}}
                        <thead>
                        <tr>
                            <th width="250">Kode Produk</th>
                            <th>Nama Produk</th>
                            <th width="100">Qty</th>
                            <th width="180">Satuan</th>
                            <th width="60">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($itemAssembly as $index => $value)
                            @php
                                $index += 1;
                            @endphp
                            <tr id="row-{{$index}}" data-no="{{$index}}">
                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="kode-row-{{$index}}"
                                               placeholder="Kode Produk" value="{{$value->barang->kode_barang}}"
                                               readonly>
                                        <div class="input-group-append">
                                            <a class="btn btn-primary btn-view" data-toggle="modal" data-no="{{$index}}"
                                               data-target="#pemindahan_stok_edit"><i
                                                        class="fa flaticon-search"></i></a>
                                        </div>
                                        <input type="hidden" id="id-row-stok-{{$index}}" value="{{$value->id_stok}}">
                                        <input type="hidden" id="id-row-barang-{{$index}}"
                                               value="{{$value->barang->id}}">
                                        <input type="hidden" id="harga-beli-barang-{{$index}}"
                                               value="{{$value->barang->harga_beli_terakhir_barang}}">
                                    </div>
                                </td>
                                <td id="nama-row-{{$index}}">{{$value->barang->nama_barang}}</td>
                                <td><input type="number" readonly id="qty-row-{{$index}}"
                                           class="form-control m-input qty-item" data-no="{{$index}}" placeholder="Qty"
                                           value="{{$value->jumlah}}"></td>
                                <td id="satuan-row-"{{$index}}>{{$value->satuan}}

                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="First group">
                                        <a class="btn btn-danger btn-delete" id="barang{{$index}}" data-no="{{$index}}"><i
                                                    class="la la-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="modal-footer modal--button-custom">
                        <label type="button" for="submit-form" class="btn btn-primary">Simpan Assembly</label>
                        <a href="{{route('produkPage')}}">
                            <button type="button" class="btn btn-warning">Batal</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
