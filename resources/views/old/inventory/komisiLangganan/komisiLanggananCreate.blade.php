<form action="{{ route('komisiLanggananInsertKomisi', ['id'=>$id]) }}" method="post" class="m-form form-send"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create-komisi-langganan" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group padding-top-15">
                            <label>Barang <b class="label--required">*</b></label>
                            <div class="col-sm-12">
                                <select class="form-control m-select2 id-barang-komisi" name="id_barang"
                                        style="width:100%;">
                                    <option value="">Pilih Barang</option>
                                    @foreach ($barang as $k)
                                        <option value="{{$k->id_barang}}" data-id-satuan="{{$k->id_satuan}}"
                                                data-satuan="{{$k->satuan}}">{{$k->kode_barang}} - {{$k->nama_barang}}
                                            - {{$k->satuan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Min. Beli <b class="label--required">*</b></label>
                            <input type="hidden" name="id_langganan" value="{{$id}}">
                            <input type="number" class="form-control m-input touchspin-number"
                                   name="minimal_beli_komisi_langganan" placeholder="Min. Beli" min="0">
                        </div>
                        <div class="form-group m-form__group">
                            <label>Satuan <b class="label--required">*</b></label>
                            <input type="hidden" name="id_satuan">
                            <input type="text" class="form-control m-input" name="satuan" placeholder="Satuan" readonly>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tipe Komisi <b class="label--required">*</b></label>
                            <div class="col-sm-12">
                                <label class="radio-inline"><input type="radio"
                                                                   style="margin-right:5px; margin-left:10px;"
                                                                   name="tipe_komisi_langganan" value="persentase"
                                                                   checked>Persentase (%)</label>
                                <label class="radio-inline"><input type="radio"
                                                                   style="margin-right:5px; margin-left:10px;"
                                                                   name="tipe_komisi_langganan" value="nominal">Nominal</label>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Komisi <b class="label--required">*</b></label>
                            <input type="number" class="form-control m-input touchspin-number" name="komisi_langganan"
                                   placeholder="Komisi Langganan" min="0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>
