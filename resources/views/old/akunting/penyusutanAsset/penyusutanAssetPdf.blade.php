<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ base_path() }}/public/images/logo.png" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>PENYUSUTAN ASSET</h3></td>
                    </tr>
                    <tr>
                        <td>Tahun : {{ $tahun }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <br/>
            <div id="table">

                <table class="table m-table table-bordered table-striped table-hover m-table--head-bg-success m-table--border-success">
                    <thead>
                    <tr class="tabletitle">
                        <th rowspan="2" align="center" class="item"> No</th>
                        <th rowspan="2" class="item"> Keterangan</th>
                        <th rowspan="2" width="5%" class="item">Jumlah</th>
                        <th rowspan="2" width="5%" class="item"> Tahun Perolehan</th>
                        <th rowspan="2" width="5%" class="item"> Nilai Perolehan</th>
                        <th rowspan="2" width="5%" class="item"> Nilai Penyusutan</th>
                        <th colspan="13" class="item"> Penyusutan {{ $tahun }}</th>
                        <th rowspan="2" width="5%" class="item"> Akumulasi Penyusutan</th>
                        <th rowspan="2" width="5%" class="item"> Nilai Buku</th>
                    </tr>
                    <tr class="tabletitle">
                        <th class="item">Thn Lalu</th>
                        <th class="item">Jan</th>
                        <th class="item">Feb</th>
                        <th class="item">Mar</th>
                        <th class="item">Apr</th>
                        <th class="item">Mei</th>
                        <th class="item">Jun</th>
                        <th class="item">Jul</th>
                        <th class="item">Ags</th>
                        <th class="item">Sep</th>
                        <th class="item">Okt</th>
                        <th class="item">Nov</th>
                        <th class="item">Des</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $ktgAsset)
                        <tr class="service">
                            <td class="tableitem">
                                <p class="itemtext">
                                    <strong>
                                        {{$no++}}
                                    </strong>
                                </p>
                            </td>
                            <td class="tableitem"
                                colspan="20">
                                <p class="itemtext">
                                    <strong>
                                        {{$ktgAsset->nama}}
                                    </strong>
                                </p>
                            </td>
                        </tr>
                        @foreach($ktgAsset->asset as $asset)
                            @php
                                $sum_penyusutan_perbulan = $asset
                                ->penyusutanAsset
                                ->where('tahun', '<', $tahun)
                                ->sum('penyusutan_perbulan')
                            @endphp
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext"></p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ $asset->nama }}
                                    </p>
                                </td>
                                <td class="text-center tableitem">
                                    <p class="itemtext" align="right">
                                        {{ Main::format_number($asset->qty) }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ date('M Y', strtotime($asset->tanggal_beli)) }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ Main::format_number($asset->harga_beli) }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ Main::format_number($asset->beban_perbulan) }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ Main::format_number($sum_penyusutan_perbulan) }}
                                    </p>
                                </td>
                                @for($i=1;$i<=12;$i++)
                                    @php
                                        $sum_penyusutan_perbulan = $asset
                                        ->penyusutanAsset
                                        ->where('bulan', '=', $i)
                                        ->where('tahun', '=', $tahun)
                                        ->sum('penyusutan_perbulan');
                                    @endphp
                                    <td class="tableitem">
                                        <p class="itemtext" align="right">
                                            {{ Main::format_number($sum_penyusutan_perbulan) }}
                                        </p>
                                    </td>
                                @endfor
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ Main::format_number($asset->akumulasi_beban) }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext" align="right">
                                        {{ Main::format_number($asset->nilai_buku) }}
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>