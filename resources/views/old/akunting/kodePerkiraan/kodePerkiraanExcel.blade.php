<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Kode Perkiraan " . date('d-m-Y') . ".xls");
?>
<table width="100%">
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="80">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            KODE PERKIRAAN<br>
            Tanggal : {{ date('d F Y H:is') }}
        </td>
    </tr>
</table>

<br>
<table width="100%" border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item">Kode Rekening</th>
        <th class="item">Nama Rekening</th>
        <th class="item">Posisi</th>
        <th class="item">Normal</th>
    </tr>
    </thead>
    <tbody>
    @foreach($perkiraan as $row)
        <tr>
            <td class="tableitem"><p class="itemtext"><strong>{{ $row['mst_kode_rekening'] }}</strong></p></td>
            <td class="tableitem"><p class="itemtext"><strong>{{ $row['mst_nama_rekening'] }}</strong></p></td>
            <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row['mst_posisi']) }}</p></td>
            <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row['mst_normal']) }}</p></td>
        </tr>
        @foreach($row['childs'] as $row1)
            <tr>
                <td class="tableitem"><p class="itemtext">{{ $row1['mst_kode_rekening'] }}</p></td>
                <td class="tableitem"><p class="itemtext">{!! $space1.$row1['mst_nama_rekening'] !!}</p></td>
                <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row1['mst_posisi']) }}</p></td>
                <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row1['mst_normal']) }} </td>
            </tr>
            @foreach($row1['childs'] as $row2)
                <tr>
                    <td class="tableitem"><p class="itemtext">{{ $row2['mst_kode_rekening'] }}</p></td>
                    <td class="tableitem"><p class="itemtext">{!! $space2.$row2['mst_nama_rekening'] !!}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row2['mst_posisi']) }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row2['mst_normal']) }}</p></td>
                </tr>
                @foreach($row2['childs'] as $row3)
                    <tr>
                        <td class="tableitem"><p class="itemtext">{{ $row3['mst_kode_rekening'] }}</p></td>
                        <td class="tableitem"><p class="itemtext">{!! $space3.$row3['mst_nama_rekening'] !!}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row3['mst_posisi']) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row3['mst_normal']) }}</p></td>
                    </tr>
                @endforeach
            @endforeach
        @endforeach
    @endforeach

    </tbody>
</table>