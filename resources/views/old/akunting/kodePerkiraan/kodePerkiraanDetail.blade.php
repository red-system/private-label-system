<form action="{{ route('perkiraanUpdate', ['kode' => $master_id]) }}"
      method="post"
      class="m-form form-send"
      enctype="multipart/form-data"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}

    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first">
            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label required">Parent Data</label>
                <div class="col-lg-9">
                    <select name="mst_master_id"
                            class="form-control"
                            required
                            data-live-search="true">
                        <option value="0">Parent Data</option>
                        @foreach($perkiraan as $r)
                            <option value="{{ $r['master_id'] }}" {{ $list['mst_master_id'] == $r['master_id'] ? 'selected':'' }}>
                                --- {{ $r['mst_kode_rekening'] . ' - ' . $r['mst_nama_rekening'] }}
                            </option>
                            @foreach($r['childs'] as $r1)
                                <option value="{{ $r1['master_id'] }}" {{ $list['mst_master_id'] == $r1['master_id'] ? 'selected':'' }}>
                                    ------ {{ $r1['mst_kode_rekening'] . ' - ' . $r1['mst_nama_rekening'] }}
                                </option>
                                @foreach($r1['childs'] as $r2)
                                    <option value="{{ $r2['master_id'] }}" {{ $list['mst_master_id'] == $r2['master_id'] ? 'selected':'' }}>
                                        --------- {{ $r2['mst_kode_rekening'] . ' - ' . $r2['mst_nama_rekening'] }}
                                    </option>
                                    @foreach($r2['childs'] as $r3)
                                        <option value="{{ $r3['master_id'] }}" {{ $list['mst_master_id'] == $r3['master_id'] ? 'selected':'' }}>
                                            ------------ {{ $r3['mst_kode_rekening'] . ' - ' . $r3['mst_nama_rekening'] }}
                                        </option>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Kode Rekening </label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="mst_kode_rekening"
                           value="{{$list['mst_kode_rekening']}}">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Nama Rekening </label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="mst_nama_rekening"
                           value="{{$list['mst_nama_rekening']}}">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Posisi </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_posisi">
                        <option value="neraca" {{ $list['mst_posisi'] == 'neraca' ? 'selected':'' }}>Neraca</option>
                        <option value="laba rugi" {{ $list['mst_posisi'] == 'laba rugi' ? 'selected':'' }}>Laba Rugi
                        </option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Normal Balance </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_normal">
                        <option value="debet" {{ $list['mst_normal'] == 'debet' ? 'selected':'' }}>Debet</option>
                        <option value="kredit" {{ $list['mst_normal'] == 'kredit' ? 'selected':'' }}>Kredit</option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Tipe Laporan </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_tipe_laporan">
                        <option value="laba bersih" {{ $list['mst_tipe_laporan'] == 'laba bersih' ? 'selected':'' }}>
                            Laba Bersih
                        </option>
                        <option value="laba kotor" {{ $list['mst_tipe_laporan'] == 'laba kotor' ? 'selected':'' }}>Laba
                            Kotor
                        </option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Tipe Nominal </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_tipe_nominal">
                        <option value="pendapatan" {{ $list['mst_tipe_nominal'] == 'pendapatan' ? 'selected':'' }}>
                            Pendapatan
                        </option>
                        <option value="beban" {{ $list['mst_tipe_nominal'] == 'beban' ? 'selected':'' }}>Beban</option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Tipe Neraca </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_neraca_tipe">
                        <option value="asset" {{ $list['mst_neraca_tipe'] == 'asset' ? 'selected':'' }}>Asset</option>
                        <option value="liabilitas" {{ $list['mst_neraca_tipe'] == 'liabilitas' ? 'selected':'' }}>
                            Liabilitas
                        </option>
                        <option value="ekuitas" {{ $list['mst_neraca_tipe'] == 'ekuitas' ? 'selected':'' }}>Ekuitas
                        </option>
                        <option value="null" {{ $list['mst_neraca_tipe'] == 'null' ? 'selected':'' }}>Kosongkan</option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Termasuk Arus Khas </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_kas_status">
                        <option value="yes" {{ $list['mst_kas_status'] == 'yes' ? 'selected':'' }}>Yes</option>
                        <option value="no" {{ $list['mst_kas_status'] == 'no' ? 'selected':'' }}>No</option>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 form-control-label required">Termasuk Pembayaran </label>
                <div class="col-lg-9">
                    <select class="form-control" name="mst_pembayaran">
                        <option value="yes" {{ $list['mst_kas_status'] == 'yes' ? 'selected':'' }}>Yes</option>
                        <option value="no" {{ $list['mst_kas_status'] == 'no' ? 'selected':'' }}>No</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Perbarui</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
    </div>
</form>