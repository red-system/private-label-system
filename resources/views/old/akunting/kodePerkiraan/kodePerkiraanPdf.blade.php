<link rel="stylesheet" type="text/css" href="{{ base_path()}}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Kode Perkiraan</font><br>
                        Tanggal : {{  date('d F Y H:i') }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/><br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">Kode Rekening</th>
                <th class="item">Nama Rekening</th>
                <th class="item">Posisi</th>
                <th class="item">Normal</th>
            </tr>
            </thead>
            <tbody>
            @foreach($perkiraan as $row)
                <tr>
                    <td class="tableitem"><p class="itemtext"><strong>{{ $row['mst_kode_rekening'] }}</strong></p></td>
                    <td class="tableitem"><p class="itemtext"><strong>{{ $row['mst_nama_rekening'] }}</strong></p></td>
                    <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row['mst_posisi']) }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row['mst_normal']) }}</p></td>
                </tr>
                @foreach($row['childs'] as $row1)
                    <tr>
                        <td class="tableitem"><p class="itemtext">{{ $row1['mst_kode_rekening'] }}</p></td>
                        <td class="tableitem"><p class="itemtext">{!! $space1.$row1['mst_nama_rekening'] !!}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row1['mst_posisi']) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row1['mst_normal']) }} </td>
                    </tr>
                    @foreach($row1['childs'] as $row2)
                        <tr>
                            <td class="tableitem"><p class="itemtext">{{ $row2['mst_kode_rekening'] }}</p></td>
                            <td class="tableitem"><p class="itemtext">{!! $space2.$row2['mst_nama_rekening'] !!}</p>
                            </td>
                            <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row2['mst_posisi']) }}</p>
                            </td>
                            <td class="tableitem"><p class="itemtext">{{ Main::menuAction($row2['mst_normal']) }}</p>
                            </td>
                        </tr>
                        @foreach($row2['childs'] as $row3)
                            <tr>
                                <td class="tableitem"><p class="itemtext">{{ $row3['mst_kode_rekening'] }}</p></td>
                                <td class="tableitem"><p class="itemtext">{!! $space3.$row3['mst_nama_rekening'] !!}</p>
                                </td>
                                <td class="tableitem"><p
                                            class="itemtext">{{ Main::menuAction($row3['mst_posisi']) }}</p></td>
                                <td class="tableitem"><p
                                            class="itemtext">{{ Main::menuAction($row3['mst_normal']) }}</p></td>
                            </tr>
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach

            </tbody>
        </table>
    </div>

</div>
