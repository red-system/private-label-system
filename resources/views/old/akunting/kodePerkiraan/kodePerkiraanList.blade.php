@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('old.akunting.kodePerkiraan.modalPerkiraanCreate')
    @include('old.akunting.kodePerkiraan.modalPerkiraanEdit')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('perkiraanPage')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <button data-toggle="modal"
                            href="#modal-create"
                            type="button"
                            class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data Perkiraan</span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-print"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Cetak Data
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot text-center">
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                        <a href="{{ route('perkiraanPdf') }}"
                           class="btn btn-danger akses-pdf" target="_blank">
                            <i class="la la-file-pdf-o"></i> Print PDF
                        </a>
                        <a href="{{ route('perkiraanExcel') }}"
                           class="btn btn-success akses-excel" target="_blank">
                            <i class="la la-file-excel-o"></i> Print Excel
                        </a>
                    </div>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-no-pagination">
                        <thead>
                        <tr class="">
                            <th width="150"> Kode Rekening</th>
                            <th> Nama Rekening</th>
                            <th> Posisi</th>
                            <th> Normal</th>
                            <th width="40"> Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($perkiraan as $row)
                            <tr>
                                <td class="font-weight-bold">{{ $row['mst_kode_rekening'] }}</td>
                                <td class="font-weight-bold">{{ $row['mst_nama_rekening'] }}</td>
                                <td> {{ Main::menuAction($row['mst_posisi']) }} </td>
                                <td> {{ Main::menuAction($row['mst_normal']) }} </td>
                                <td>
                                    <textarea class="row-data hidden">@json($row)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm akses-edit">
                                        <button class="btn btn-sm btn-success btn-edit-akunting"
                                                href="{{ route('perkiraanEdit', $row['master_id']) }}">
                                            <i class="la la-edit"></i> Edit
                                        </button>
                                        <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                data-route="{{ route('perkiraanDelete', $row['master_id']) }}"
                                                data-reload="true"
                                        >
                                            <i class="la la-remove"></i> Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @foreach($row['childs'] as $row1)
                                <tr>
                                    <td {{ count($row1['childs']) > 0 ? 'class=font-weight-bold':'' }}>
                                        {{ $row1['mst_kode_rekening'] }}
                                    </td>
                                    <td {{ count($row1['childs']) > 0 ? 'class=font-weight-bold':'' }}>
                                        {!! $space1.$row1['mst_nama_rekening'] !!}
                                    </td>
                                    <td> {{ Main::menuAction($row1['mst_posisi']) }} </td>
                                    <td> {{ Main::menuAction($row1['mst_normal']) }} </td>
                                    <td>
                                        <textarea class="row-data hidden">@json($row1)</textarea>
                                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                            <button class="btn btn-sm btn-success btn-edit-akunting akses-edit"
                                                    href="{{ route('perkiraanEdit', $row1['master_id']) }}" >
                                                <i class="la la-edit"></i> Edit
                                            </button>
                                            <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                    data-route="{{ route('perkiraanDelete', $row1['master_id']) }}"
                                                    data-reload="true"
                                            >
                                                <i class="la la-remove"></i> Delete
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @foreach($row1['childs'] as $row2)
                                    <tr>
                                        <td {{ count($row2['childs']) > 0 ? 'class=font-weight-bold':'' }}>
                                            {{ $row2['mst_kode_rekening'] }}
                                        </td>
                                        <td {{ count($row2['childs']) > 0 ? 'class=font-weight-bold':'' }}>
                                            {!! $space2.$row2['mst_nama_rekening'] !!}
                                        </td>
                                        <td> {{ Main::menuAction($row2['mst_posisi']) }} </td>
                                        <td> {{ Main::menuAction($row2['mst_normal']) }} </td>
                                        <td>
                                            <textarea class="row-data hidden">@json($row2)</textarea>
                                            <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                                <button class="btn btn-sm btn-success btn-edit-akunting akses-edit"
                                                        href="{{ route('perkiraanEdit', $row2['master_id']) }}">
                                                    <i class="la la-edit"></i> Edit
                                                </button>
                                                <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                        data-route="{{ route('perkiraanDelete', $row2['master_id']) }}"
                                                        data-reload="true"
                                                >
                                                    <i class="la la-remove"></i> Delete
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @foreach($row2['childs'] as $row3)
                                        <tr>
                                            <td> {{ $row3['mst_kode_rekening'] }} </td>
                                            <td> {!! $space3.$row3['mst_nama_rekening'] !!} </td>
                                            <td> {{ Main::menuAction($row3['mst_posisi']) }} </td>
                                            <td> {{ Main::menuAction($row3['mst_normal']) }} </td>
                                            <td>
                                                <textarea class="row-data hidden">@json($row3)</textarea>
                                                <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                                    <button class="btn btn-sm btn-success btn-edit-akunting akses-edit"
                                                            href="{{ route('perkiraanEdit', $row3['master_id']) }}">
                                                        <i class="la la-edit"></i> Edit
                                                    </button>
                                                    <button class="btn btn-sm btn-danger btn-hapus akses-delete"
                                                            data-route="{{ route('perkiraanDelete', $row3['master_id']) }}"
                                                            data-reload="true"
                                                    >
                                                        <i class="la la-remove"></i> Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
