@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('jurnalUmumPage')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('jurnalUmumCreate') }}"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <i class="la la-plus"></i>
                        Tambah Jurnal Umum
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ Main::format_date($date_start) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end"
                                           value="{{ Main::format_date($date_end) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('jurnalUmumPdf', $params) }}"
                               class="btn btn-danger akses-pdf" target="_blank">
                                <i class="la la-file-pdf-o"></i> Download PDF
                            </a>
                            <a href="{{ route('jurnalUmumExcel', $params) }}"
                               class="btn btn-success akses-excel" target="_blank">
                                <i class="la la-file-excel-o"></i> Download Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-bordered table-hover m-table m-table--head-bg-success table-header-fixed datatable-jurnal-umum"
                           style="overflow-x: auto; display: table">
                        <thead>
                        <tr class="">
                            <th width="10">No</th>
                            <th>Tanggal</th>
                            <th>No Bukti</th>
                            <th>Keterangan</th>
                            <th>No Akun</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Catatan</th>
                            <th width="40" style="width: 40px">Menu</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jurnal_umum as $jmu)
                            <tr data-row-index="{{ $jmu['jurnal_umum_id'] }}" style="background-color: #F4F5F8">
                                <td class="hidden m--hide">
                                    @json($jmu)
                                </td>
                                <td align="center">
                                    <strong>{{ $no++ }}.</strong>
                                </td>
                                <td><strong>{{ Main::format_date($jmu['jmu_tanggal']) }}</strong></td>
                                <td><strong>{{ $jmu['no_invoice'] }}</strong></td>
                                <td colspan="5"><strong>{{ $jmu['jmu_keterangan'] }}</strong></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm m-btn--air">
                                        <a class="btn btn-success btn-sm m-btn--pill akses-edit"
                                           href="{{ route('jurnalUmumEdit', ['id'=>Main::encrypt($jmu['jurnal_umum_id'])]) }}">
                                            <span class="la la-pencil"></span> Edit
                                        </a>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus akses-delete"
                                                data-route='{{ route('jurnalUmumDelete', ['id'=>Main::encrypt($jmu['jurnal_umum_id'])]) }}'
                                                data-remove-by-index="true"
                                                data-remove-index="{{ $jmu['jurnal_umum_id'] }}">
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @foreach($jmu['transaksi'] as $trs)
                                <tr data-row-index="{{ $jmu['jurnal_umum_id'] }}">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td <?php if ($trs['trs_debet'] == 0) echo 'align="right"';?>>{{ $trs['trs_nama_rekening'] }}</td>
                                    <td <?php if ($trs['trs_debet'] == 0) echo 'align="right"';?>>{{ $trs['trs_kode_rekening'] }}</td>
                                    <td align="right">   {{ Main::format_number_system($trs['trs_debet']) }} </td>
                                    <td align="right">   {{ Main::format_number_system($trs['trs_kredit']) }} </td>
                                    <td>{{ $trs['trs_catatan'] }} </td>
                                    <td></td>
                                </tr>
                            @endforeach
                            <tr data-row-index="{{ $jmu['jurnal_umum_id'] }}">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="right"
                                    class="font-weight-bold">{{ Main::format_number_system($jmu['debet_total']) }}</td>
                                <td align="right"
                                    class="font-weight-bold">{{ Main::format_number_system($jmu['kredit_total']) }}</td>
                                <td>
                                    @if($jmu['balance_status'] == true)
                                        <span class="m-badge m-badge--success m-badge--wide">
                                            Balance
                                        </span>
                                    @else
                                        <div class="pull-left">
                                            <span class="m-badge m-badge--danger m-badge--wide">
                                                Not Balance
                                            </span>
                                        </div>
                                        <div class="pull-right">
                                            <h4> {{ Main::format_number_system($jmu['balance_selisih']) }}</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endif
                                </td>
                                <td>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="">
                            <th width="10"></th>
                            <th colspan="4" class="text-center">
                                <strong> TOTAL </strong>
                            </th>
                            <th class="text-right">
                                <strong>{{ Main::format_number_system($total_debet) }} </strong>
                            </th>
                            <th class="text-right">
                                <strong>{{ Main::format_number_system($total_kredit) }} </strong>
                            </th>
                            @if($balance_status == 'true')
                                <th>
                                    <span class="m-badge m-badge--success m-badge--wide">
                                        Balance
                                    </span>
                                </th>
                            @elseif($balance_status != 'true')
                                <th>
                                    <div class="pull-left">
                                        <span class="m-badge m-badge--danger m-badge--wide">
                                            Not Balance
                                        </span>
                                    </div>
                                    <div class="pull-right">

                                        <h4> {{ Main::format_number_system($balance_selisih) }}</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </th>
                            @endif
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection