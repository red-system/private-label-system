<table class="row-transaksi m--hide">
    <tr>
        <td class="m--hide data">

        </td>
        <td>
            <select class="select2-transaksi" name="master_id[]"  style="width:100%;">
                <option value="">Pilih Kode Perkiraan</option>
                @foreach($kode_perkiraan_option_list as $r)
                    <optgroup label="{{ $r['mst_kode_rekening'].' - '.$r['mst_nama_rekening'] }}">
                        @foreach($r['childs'] as $r2)
                            <option value="{{ $r2['master_id'] }}">
                                {{ $r2['mst_kode_rekening'].' - '.$r2['mst_nama_rekening'] }}
                            </option>
                            @if(!empty($r2['childs']))
                                @foreach($r2['childs'] as $r3)
                                    <option value="{{ $r3['master_id'] }}">
                                        {{$r3['mst_kode_rekening'].' - '.$r3['mst_nama_rekening']}}
                                    </option>
                                    @if(!empty($r3['childs']))
                                        @foreach($r3['childs'] as $r4)
                                            <option value="{{ $r4['master_id'] }}">
                                                {{$r4['mst_kode_rekening'].' - '.$r4['mst_nama_rekening']}}
                                            </option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </td>
        <td>
            <select class="form-control jenis_transaksi" name="trs_jenis_transaksi[]">
                <option value="debet">Debet</option>
                <option value="kredit">Kredit</option>
            </select>
        </td>
        <td class="trs_debet">
            <input type="text" class="form-control m-input input-numeral trs-debet" name="trs_debet[]" value="0">
        </td>
        <td class="trs_kredit">
            <input type="text" class="form-control m-input input-numeral trs-kredit" name="trs_kredit[]" value="0" readonly>
        </td>
        <td>
            <select name="tipe_arus_kas[]" class="form-control">
                <option value="Operasi">Operasi</option>
                <option value="Pendanaan">Pendanaan</option>
                <option value="Investasi">Investasi</option>
            </select>
        </td>
        <td>
            <textarea class="form-control" name="trs_catatan[]"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-transaksi btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i>
            </button>
        </td>
    </tr>
</table>