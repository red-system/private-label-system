<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Jurnal Umum " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="80">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            JURNAL UMUM<br>
            Tanggal : {{ $date_start.' s/d '.$date_end }}
        </td>
    </tr>
</table>
    <br/>
    <table width="100%" border="1">
        <thead>
        <tr class="tabletitle">
            <th class="item" width="40">No</th>
            <th class="item">Tanggal</th>
            <th class="item">No Bukti</th>
            <th class="item">Keterangan</th>
            <th class="item">No Akun</th>
            <th class="item">Debet</th>
            <th class="item">Kredit</th>
            <th class="item">Catatan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($jurnal_umum as $jmu)
            <tr style="background-color: #F4F5F8">
                <td class="tableitem" align="center"><p class="itemtext"> {{ $no++ }}.</p></td>
                <td class="tableitem"><p class="itemtext">{{ Main::format_date($jmu['jmu_tanggal']) }} </p>
                </td>
                <td class="tableitem"><p class="itemtext">{{ $jmu['no_invoice'] }}</p></td>
                <td class="tableitem" colspan="5"><p class="itemtext"> {{ $jmu['jmu_keterangan'] }}</p></td>
            </tr>
            @foreach($jmu['transaksi'] as $trs)
                <tr>
                    <td class="tableitem"><p class="itemtext"></p></td>
                    <td class="tableitem"><p class="itemtext"></p></td>
                    <td class="tableitem"><p class="itemtext"></p></td>
                    <td <?php if ($trs['trs_debet'] == 0) echo 'align="right"';?> class="tableitem">
                        <p class="itemtext">{{ $trs['trs_nama_rekening'] }}</p></td>
                    <td <?php if ($trs['trs_debet'] == 0) echo 'align="right"';?> class="tableitem">
                        <p class="itemtext">{{ $trs['trs_kode_rekening']}}</p></td>
                    <td align="right" class="tableitem"><p
                                class="itemtext">{{ Main::format_number_system($trs['trs_debet']) }}</p></td>
                    <td align="right" class="tableitem"><p
                                class="itemtext">{{ Main::format_number_system($trs['trs_kredit']) }}</p></td>
                    <td class="tableitem"><p class="itemtext">{{ $trs['trs_catatan'] }}</p></td>
                </tr>
            @endforeach
            <tr data-row-index="{{ $jmu['jurnal_umum_id'] }}">
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td class="tableitem"><p class="itemtext"></p></td>
                <td align="right" class="tableitem"><p
                            class="itemtext">{{ Main::format_number_system($jmu['debet_total']) }}</p></td>
                <td align="right" class="tableitem"><p
                            class="itemtext">{{ Main::format_number_system($jmu['kredit_total']) }}</p></td>
                <td class="tableitem">
                    <p class="itemtext">
                    @if($jmu['balance_status'] == true)
                        <span class="m-badge m-badge--success m-badge--wide">
                            Balance
                        </span>
                    @else
                        <div class="pull-left">
                            <span class="m-badge m-badge--danger m-badge--wide">
                                Not Balance
                            </span>
                        </div>
                        <div class="pull-right">
                            <h4> {{ Main::format_number_system($jmu['balance_selisih']) }}</h4>
                        </div>
                        <div class="clearfix"></div>
                    @endif
                    </p>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr class="">
            <th width="10" class="tableitem"><p class="itemtext"></p></th>
            <th colspan="4" class="tableitem">
                <p class="text-center itemtext">
                    <strong> TOTAL </strong>
                </p>
            </th>
            <th class="text-right tableitem">
                <p class="itemtext">
                    <strong>{{number_format($total_debet, 2)}} </strong>
                </p>
            </th>
            <th class="text-right tableitem">
                <p class="itemtext">
                    <strong>{{number_format($total_kredit, 2)}} </strong>
                </p>
            </th>
            @if($balance_status == 'true')
                <th class="tableitem">
                    <p class="itemtext">
                        <span class="m-badge m-badge--success m-badge--wide">
                            Balance
                        </span>
                    </p>
                </th>
            @else
                <th class="tableitem">
                    <p class="itemtext">
                    <div class="pull-left">
                        <span class="m-badge m-badge--danger m-badge--wide">
                            Not Balance
                        </span>
                    </div>
                    <div class="pull-right">
                        <h4> {{ Main::format_number_system($balance_selisih) }}</h4>
                    </div>
                    <div class="clearfix"></div>
                    </p>
                </th>
            @endif
        </tr>
        </tfoot>
    </table>