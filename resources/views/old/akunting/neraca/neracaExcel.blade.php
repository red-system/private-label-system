<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Neraca " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="80">
        </td>
        <td colspan="2">

            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}

        </td>
        <td>
            NERACA<br>
            Tanggal : {{ $date_start.' s/d '.$date_end }}

        </td>

    </tr>
</table>
<br/>

<table border="1" width="100%">
    <thead>
    <tr class="success">
        <th width="25%" class="text-center">ASET</th>
        <th width="25%" class="text-center">LIABILITAS</th>
        <th width="25%" class="text-center">EKUITAS</th>
        <th width="25%" class="text-center">STATUS</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td align="center">{{ Main::format_number_system($total_asset) }}</td>
        <td align="center">{{ Main::format_number_system($total_liabilitas) }}</td>
        <td align="center">{{ Main::format_number_system($total_ekuitas) }}</td>
        @if($status=='BALANCE')
            <td align="center"><font color="green"><strong>{{$status}}
                        : {{ Main::format_number_system($selisih) }}</strong></font></td>
        @else
            <td align="center"><font color="red"><strong>{{$status}}
                        : {{ Main::format_number_system($selisih) }}</strong></font></td>
        @endif
    </tr>
    </tbody>
</table>


<table border="1" width="100%">
    <thead>
    <tr>
        <th colspan="4" class="text-center">Activa</th>
    </tr>
    <tr>
        <th class="text-center" colspan="2">Description</th>
        <th class="text-center" colspan="2">Jumlah</th>
    </tr>
    </thead>
    <tbody>
    @foreach($passiva_data as $passiva)
        <tr>
            <td class="font-weight-bold" colspan="2">
                {{ $passiva['mst_kode_rekening'] }} {{ $passiva['mst_nama_rekening'] }}
            </td>
            <td align="right" class="font-weight-bold" colspan="2">
                {{ Main::format_number_system($passiva['value']) }}
            </td>
        </tr>
        @if(!empty($passiva['childs']))
            @foreach($passiva['childs'] as $passChild1)
                <tr>
                    <td colspan="2">
                        {!! $space1 !!}
                        {{ $passChild1['mst_kode_rekening'] }} {{ $passChild1['mst_nama_rekening'] }}
                    </td>
                    <td align="right" colspan="2">{{ Main::format_number_system($passChild1['value']) }}</td>
                </tr>
            @endforeach
        @endif
    @endforeach

    </tbody>
    <tfoot>
    <tr class="m-table__row--success">
        <th class="font-weight-bold" colspan="2">Total Pasiva</th>
        <th class="text-right font-weight-bold" colspan="2">{{ Main::format_number_system($passiva_total) }}</th>
    </tr>
    </tfoot>
</table>
<div class="col-sm-12 col-md-6">
    <br/><br/>
    <table border="1" width="100%">
        <thead>
        <tr>
            <th colspan="4" class="text-center">Pasiva</th>
        </tr>
        <tr>
            <th class="text-center" colspan="2">Description</th>
            <th class="text-center" colspan="2">Jumlah</th>
        </tr>
        </thead>
        <tbody>
        @foreach($activa_data as $activa)
            <tr>
                <td class="font-weight-bold" colspan="2">
                    {{ $activa['kode_rekening'] }} {{ $activa['nama_rekening'] }}
                </td>
                <td align="right" class="font-weight-bold" colspan="2">
                    {{ Main::format_number_system($activa['value']) }}
                </td>
            </tr>
            @if(!empty($activa['childs']))
                @foreach($activa['childs'] as $actChild1)
                    <tr>
                        <td colspan="2">
                            {!! $space1 !!}
                            {{ $actChild1['mst_kode_rekening'] }} {{ $actChild1['mst_nama_rekening'] }}
                        </td>
                        <td align="right" colspan="2">{{ Main::format_number_system($actChild1['value']) }}</td>
                    </tr>
                @endforeach
            @endif
        @endforeach
        </tbody>
        <tfoot>
        <tr class="m-table__row--success">
            <th class="font-weight-bold" colspan="2">Total Aktiva</th>
            <th class="text-right font-weight-bold" align="right"
                colspan="2">{{ Main::format_number_system($activa_total) }}</th>
        </tr>
        <tr class="m-table__row--success">
            <th class="font-weight-bold" colspan="2">Selisih</th>
            <th class="text-right font-weight-bold" colspan="2" align="right">{{ Main::format_number_system($selisih) }}</th>
        </tr>
        </tfoot>
    </table>
</div>