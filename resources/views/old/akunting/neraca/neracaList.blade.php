@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form method="get">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-gear"></i>
                            </span>
                                <h3 class="m-portlet__head-text">
                                    Filter Data
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ Main::format_date($date_start) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end"
                                           value="{{ Main::format_date($date_end) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('neracaPdf', $params) }}"
                               class="btn btn-danger akses-pdf" target="_blank">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('neracaExcel', $params) }}"
                               class="btn btn-success akses-excel" target="_blank">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <h3 align="center">
                        NERACA
                    </h3>
                    <h6 align="center">
                        {{ Main::format_date_label($date_start) }} s/d {{ Main::format_date_label($date_end) }}
                    </h6>
                    <br /><br />
                    <table class="table table-bordered m-table m-table--head-bg-success" width="100%" >
                        <thead>
                        <tr class="success">
                            <th width="25%" class="text-center">ASET</th>
                            <th width="25%" class="text-center">LIABILITAS</th>
                            <th width="25%" class="text-center">EKUITAS</th>
                            <th width="25%" class="text-center">STATUS</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center">{{ Main::format_number_system($total_asset) }}</td>
                            <td align="center">{{ Main::format_number_system($total_liabilitas) }}</td>
                            <td align="center">{{ Main::format_number_system($total_ekuitas) }}</td>
                            @if($status=='BALANCE')
                                <td align="center"><font color="green"><strong>{{$status}}
                                            : {{ Main::format_number_system($selisih) }}</strong></font></td>
                            @else
                                <td align="center"><font color="red"><strong>{{$status}}
                                            : {{ Main::format_number_system($selisih) }}</strong></font></td>
                            @endif
                        </tr>
                        </tbody>
                    </table>


                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                            <table class="table table-bordered m-table m-table--head-bg-success datatable-neraca">
                                <thead>
                                <tr>
                                    <th colspan="2" class="text-center">Activa</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Jumlah</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($activa_data as $activa)
                                    <tr>
                                        <td class="font-weight-bold">
                                            {{ $activa['kode_rekening'] }} {{ $activa['nama_rekening'] }}
                                        </td>
                                        <td align="right" class="font-weight-bold">
                                            {{ Main::format_number_system($activa['value']) }}
                                        </td>
                                    </tr>
                                    @if(!empty($activa['childs']))
                                        @foreach($activa['childs'] as $actChild1)
                                            <tr>
                                                <td>
                                                    {!! $space1 !!}
                                                    {{ $actChild1['mst_kode_rekening'] }} {{ $actChild1['mst_nama_rekening'] }}
                                                </td>
                                                <td align="right">{{ Main::format_number_system($actChild1['value']) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="m-table__row--success">
                                    <th class="font-weight-bold">Total Aktiva</th>
                                    <th class="text-right font-weight-bold">{{ Main::format_number_system($activa_total)}}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <table class="table table-bordered m-table m-table--head-bg-success datatable-neraca">
                                <thead>
                                <tr>
                                    <th colspan="2" class="text-center">Pasiva</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Jumlah</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($passiva_data as $passiva)
                                    <tr>
                                        <td class="font-weight-bold">
                                            {{ $passiva['mst_kode_rekening'] }} {{ $passiva['mst_nama_rekening'] }}
                                        </td>
                                        <td align="right" class="font-weight-bold">
                                            {{ Main::format_number_system($passiva['value']) }}
                                        </td>
                                    </tr>
                                    @if(!empty($passiva['childs']))
                                        @foreach($passiva['childs'] as $passChild1)
                                            <tr>
                                                <td>
                                                    {!! $space1 !!}
                                                    {{ $passChild1['mst_kode_rekening'] }} {{ $passChild1['mst_nama_rekening'] }}
                                                </td>
                                                <td align="right">{{ Main::format_number_system($passChild1['value']) }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="m-table__row--success">
                                    <th class="font-weight-bold">Total Pasiva</th>
                                    <th class="text-right font-weight-bold">{{ Main::format_number_system($passiva_total) }}</th>
                                </tr>
                                </tfoot>
                            </table>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection