<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">NERACA</font><br>
                        Tanggal : {{ $date_start.' s/d '.$date_end }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <br/>
    <div id="table">
        <table class="table table-bordered m-table m-table--head-bg-success" width="100%">
            <thead>
            <tr class="tabletitle item">
                <th width="25%" class="text-center">ASET</th>
                <th width="25%" class="text-center">LIABILITAS</th>
                <th width="25%" class="text-center">EKUITAS</th>
                <th width="25%" class="text-center">STATUS</th>
            </tr>
            </thead>
            <tbody>
            <tr class="item">
                <td align="center">{{ Main::format_number_system($total_asset) }}</td>
                <td align="center">{{ Main::format_number_system($total_liabilitas) }}</td>
                <td align="center">{{ Main::format_number_system($total_ekuitas) }}</td>
                @if($status=='BALANCE')
                    <td align="center"><font color="green"><strong>{{$status}}
                                : {{ Main::format_number_system($selisih) }}</strong></font></td>
                @else
                    <td align="center"><font color="red"><strong>{{$status}}
                                : {{ Main::format_number_system($selisih) }}</strong></font></td>
                @endif
            </tr>
            </tbody>
        </table>


        <div class="row">
            <div class="col-sm-12 col-md-6">

                <table class="table table-bordered m-table m-table--head-bg-success">
                    <thead>
                    <tr class="tabletitle item">
                        <th colspan="2" class="text-center">Activa</th>
                    </tr>
                    <tr class="tabletitle item">
                        <th class="text-center">Description</th>
                        <th class="text-center">Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activa_data as $activa)
                        <tr>
                            <td>
                                <p class="itemtext">
                                    {{ $activa['kode_rekening'] }} {{ $activa['nama_rekening'] }}
                                </p>
                            </td>
                            <td align="right">
                                <p class="itemtext">
                                    {{ Main::format_number_system($activa['value']) }}
                                </p>
                            </td>
                        </tr>
                        @if(!empty($activa['childs']))
                            @foreach($activa['childs'] as $actChild1)
                                <tr>
                                    <td>
                                        <p class="itemtext">
                                            {!! $space1 !!}
                                            {{ $actChild1['mst_kode_rekening'] }} {{ $actChild1['mst_nama_rekening'] }}
                                        </p>
                                    </td>
                                    <td align="right">
                                        <p class="itemtext">
                                            {{ Main::format_number_system($actChild1['value']) }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr class="m-table__row--success item">
                        <th class="">Total Aktiva</th>
                        <th class="text-right" align="right">{{ Main::format_number_system($activa_total) }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-sm-12 col-md-6">
                <br/><br/>
                <table class="table table-bordered m-table m-table--head-bg-success">
                    <thead>
                    <tr class="tabletitle item">
                        <th colspan="2" class="text-center">Pasiva</th>
                    </tr>
                    <tr class="tabletitle item">
                        <th class="text-center">Description</th>
                        <th class="text-center">Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($passiva_data as $passiva)
                        <tr>
                            <td>
                                <p class="itemtext">
                                    {{ $passiva['mst_kode_rekening'] }} {{ $passiva['mst_nama_rekening'] }}
                                </p>
                            </td>
                            <td align="right">
                                <p class="itemtext">
                                    {{ Main::format_number_system($passiva['value']) }}
                                </p>
                            </td>
                        </tr>
                        @if(!empty($passiva['childs']))
                            @foreach($passiva['childs'] as $passChild1)
                                <tr>
                                    <td>
                                        <p class="itemtext">
                                            {!! $space1 !!}
                                            {{ $passChild1['mst_kode_rekening'] }} {{ $passChild1['mst_nama_rekening'] }}
                                        </p>
                                    </td>
                                    <td align="right">
                                        <p class="itemtext">
                                            {{ Main::format_number_system($passChild1['value']) }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr class="m-table__row--success item">
                        <th class="">Total Pasiva</th>
                        <th class="text-right" align="right">{{ Main::format_number_system($passiva_total) }}</th>
                    </tr>
                    <tr class="m-table__row--success item">
                        <th class="">Selisih</th>
                        <th class="text-right" align="right">{{ Main::format_number_system($selisih) }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>