<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Anggaran " . $anggaran_tahun . ".xls");
?>
<table>
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="70">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            Anggaran<br>
            Tahun : {{ $anggaran_tahun }}
        </td>
    </tr>
</table>

<br>
<table width="100%" border="1">
    <thead>
    <tr class="tabletitle">
        <th class="item">Item</th>
        <th class="item">Kode Perkiraan</th>
        <th class="item">Anggaran</th>
        <th class="item">Realisasi</th>
        <th class="item">Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($anggaran_detail as $row)
        <tr>
            <td class="tableitem">
                <p class="itemtext">{{ $row['master']['mst_nama_rekening'] }}</p>
            </td>
            <td class="tableitem">
                <p class="itemtext">{{ $row['master']['mst_kode_rekening'] }}</p>
            </td>
            <td class="tableitem" style="text-align: right">
                <p class="itemtext">{{Main::format_number_system($row['anggaran_nominal'])}}</p>
            </td>
            <td class="tableitem" style="text-align: right">
                <p class="itemtext">{{Main::format_number_system($row['realisasi_anggaran_total'])}}</p>
            </td>
            <td class="tableitem">
                <p class="itemtext">
                    {{$row['budget_status']}}
                </p>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="2" class="font-weight-bold text-center tableitem">
            <p class="itemtext">TOTAL</p>
        </th>
        <th class="font-weight-bold th-total-anggaran-label tableitem" style="text-align: right">
            <p class="itemtext">{{Main::format_number_system($anggaran_total)}}</p>
        </th>
        <th class="font-weight-bold th-total-anggaran-label tableitem" style="text-align: right">
            <p class="itemtext">{{Main::format_number_system($realisasi_anggaran_total)}}</p>
        </th>
        <th class="font-weight-bold th-total-anggaran-label tableitem">
            {{$budget_status}}
        </th>
    </tr>
    </tfoot>
</table>