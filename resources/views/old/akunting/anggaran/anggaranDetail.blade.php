@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/anggaran.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <form method="post"
          action="{{ route('anggaranUpdate', ['id'=>Main::encrypt($anggaran_id)]) }}"
          class="form-send"
          data-alert-show="true"
          data-alert-field-message="true"
          data-redirect="{{ route('anggaranPage') }}"
          style="width: 100%">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            {{ csrf_field() }}

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-1 col-form-label">Tahun :</label>
                            <div class="col-2">
                                <div class="input-group date">
                                    <input type="text" name="anggaran_tahun" class="form-control m-input"
                                           style="text-align: center"
                                           readonly="" value="{{ $anggaran_tahun }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <a href="{{ route('anggaranPdf', ['id'=> Main::encrypt($anggaran_id)]) }}"
                               class="btn btn-danger akses-pdf" target="_blank">
                                <i class="la la-file-pdf-o"></i> Download PDF
                            </a>
                            <a href="{{ route('anggaranExcel', ['id'=> Main::encrypt($anggaran_id)]) }}"
                               class="btn btn-success akses-excel" target="_blank">
                                <i class="la la-file-excel-o"></i> Download Excel
                            </a>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-no-pagination">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Item</th>
                                <th>Kode Perkiraan</th>
                                <th>Anggaran</th>
                                <th>Realisasi</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($anggaran_detail as $row)
                                <tr>
                                    <td>
                                        {{$no++}}
                                    </td>
                                    <td>
                                        {{ $row['master']['mst_nama_rekening'] }}
                                    </td>
                                    <td>
                                        {{ $row['master']['mst_kode_rekening'] }}
                                    </td>
                                    <td class="td-no-bg" style="text-align: right">
                                        {{Main::format_number_system($row['anggaran_nominal'])}}
                                    </td>
                                    <td style="text-align: right">
                                        {{Main::format_number_system($row['realisasi_anggaran_total'])}}
                                    </td>
                                    <td>
                                        {{$row['budget_status']}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" class="font-weight-bold text-center">
                                    TOTAL
                                </th>
                                <th class="font-weight-bold th-total-anggaran-label" style="text-align: right">
                                    {{Main::format_number_system($anggaran_total)}}
                                </th>
                                <th class="font-weight-bold th-total-anggaran-label" style="text-align: right">
                                    {{Main::format_number_system($realisasi_anggaran_total)}}
                                </th>
                                <th class="font-weight-bold th-total-anggaran-label item">
                                    {{$budget_status}}
                                </th>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="m-portlet m-portlet--mobile hidden">

                            <div class="m-portlet__body">

                                <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Tanggal</th>
                                        <th>Kode Produk</th>
                                        <th>Nama Produk</th>
                                        <th>No Seri Produk</th>
                                        <th>Qty In</th>
                                        <th>Qty Out</th>
                                        <th>Gudang</th>
                                        <th>Penerima/Keterangan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </form>
@endsection