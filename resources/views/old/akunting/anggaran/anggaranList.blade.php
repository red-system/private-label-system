@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/hutang_supplier.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <input type="hidden" id="list_url" data-list-url="{{route('anggaranPage')}}">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('anggaranCreate') }}"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <i class="la la-plus"></i>
                        Tambah Data
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-no-pagination">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tahun</th>
                            <th>Total Anggaran</th>
                            <th>Total Realisasi</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        @php($no=1)
                        <tbody>
                        @foreach($anggaran as $list)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$list['anggaran_tahun']}}</td>
                                <td>{{Main::format_money($list['anggaran_total'])}}</td>
                                <td>{{Main::format_money($list['realisasi_anggaran_total'])}}</td>
                                <td>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm m-btn--air">
                                        <a class="btn btn-success btn-sm m-btn--pill akses-edit"
                                           href="{{ route('anggaranEdit', ['id'=>Main::encrypt($list['anggaran_id'])]) }}">
                                            <span class="la la-pencil"></span> Edit
                                        </a>
                                        <a class="m-btn btn btn-primary"
                                           href="{{ route('anggaranDetail', ['id'=>Main::encrypt($list['anggaran_id'])]) }}">
                                            <span class="la la-eye"></span> Detail
                                        </a>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus akses-delete"
                                                data-route='{{ route('anggaranDelete', ['id'=>Main::encrypt($list['anggaran_id'])]) }}'
                                                data-remove-by-index="true"
                                                data-remove-index="{{ $list['anggaran_id'] }}">
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
