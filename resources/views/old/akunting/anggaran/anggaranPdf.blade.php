<link rel="stylesheet" type="text/css" href="{{ base_path()}}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
    }
</style>
<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">Anggaran</font><br>
                        Tahun : {{  $anggaran_tahun }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/><br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item">No</th>
                <th class="item">Item</th>
                <th class="item">Kode Perkiraan</th>
                <th class="item">Anggaran</th>
                <th class="item">Realisasi</th>
                <th class="item">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($anggaran_detail as $row)
                <tr>
                    <td class="item">
                        {{$no++}}
                    </td>
                    <td class="item">
                        {{ $row['master']['mst_nama_rekening'] }}
                    </td>
                    <td class="item">
                        {{ $row['master']['mst_kode_rekening'] }}
                    </td>
                    <td class="item" style="text-align: right">
                        {{Main::format_number_system($row['anggaran_nominal'])}}
                    </td>
                    <td class="item" style="text-align: right">
                        {{Main::format_number_system($row['realisasi_anggaran_total'])}}
                    </td>
                    <td class="item">
                        {{$row['budget_status']}}
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th colspan="3" class="font-weight-bold text-center item">
                    TOTAL
                </th>
                <th class="font-weight-bold th-total-anggaran-label item" style="text-align: right">
                    {{Main::format_number_system($anggaran_total)}}
                </th>
                <th class="font-weight-bold th-total-anggaran-label item" style="text-align: right">
                    {{Main::format_number_system($realisasi_anggaran_total)}}
                </th>
                <th class="font-weight-bold item" style="text-align: left">
                    {{$budget_status}}
                </th>
            </tr>
            </tfoot>
        </table>
    </div>

</div>
