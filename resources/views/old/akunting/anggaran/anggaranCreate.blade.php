@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/anggaran.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    <form method="post"
          action="{{ route('anggaranInsert') }}"
          class="form-send"
          data-alert-show="true"
          data-alert-field-message="true"
          data-redirect="{{ route('anggaranPage') }}"
          style="width: 100%">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            {{ csrf_field() }}

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-1 col-form-label">Tahun :</label>
                            <div class="col-2">
                                <div class="input-group date">
                                    <input type="text" name="anggaran_tahun" class="form-control m-input m_datepicker-year"
                                           style="text-align: center"
                                           readonly="" value="{{ date('Y') }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-header-fixed fixed_header" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Item</th>
                                <th>Kode Perkiraan</th>
                                <th>Anggaran</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($kode_perkiraan as $row)
                                <input type="hidden" name="master_id[]" value="{{ $row['master_id'] }}">
                                <input type="hidden" name="item[]" value="{{ $row['mst_nama_rekening'] }}">
                                <tr>
                                    <td>
                                        {{$no++}}
                                    </td>
                                    <td>
                                        {{ $row['mst_nama_rekening'] }}
                                    </td>
                                    <td>
                                        {{ $row['mst_kode_rekening'] }}
                                    </td>
                                    <td class="td-no-bg">
                                        <input class="form-control m-input no-bg input-numeral jml-anggaran" type="text"
                                               name="anggaran[]" style=" height: 10px; text-align: right" value="0">
                                    </td>
                                </tr>
                                @foreach($row['childs'] as $row1)
                                    <input type="hidden" name="master_id[]"
                                           value="{{ $row1['master_id'] }}">
                                    <input type="hidden" name="item[]" value="{{ $row1['mst_nama_rekening'] }}">
                                    <tr>
                                        <td>
                                            {{$no++}}
                                        </td>
                                        <td>
                                            {{ $row1['mst_nama_rekening'] }}
                                        </td>
                                        <td>
                                            {{ $row1['mst_kode_rekening'] }}
                                        </td>
                                        <td class="td-no-bg">
                                            <input class="form-control m-input no-bg input-numeral jml-anggaran" type="text"
                                                   name="anggaran[]" style=" height: 10px; text-align: right" value="0">
                                        </td>
                                    </tr>
                                    @foreach($row1['childs'] as $row2)
                                        <input type="hidden" name="master_id[]"
                                               value="{{ $row2['master_id'] }}">
                                        <input type="hidden" name="item[]" value="{{ $row2['mst_nama_rekening'] }}">
                                        <tr>
                                            <td>
                                                {{$no++}}
                                            </td>
                                            <td>
                                                {{ $row2['mst_nama_rekening'] }}
                                            </td>
                                            <td>
                                                {{ $row2['mst_kode_rekening'] }}
                                            </td>
                                            <td class="td-no-bg">
                                                <input class="form-control m-input no-bg input-numeral jml-anggaran" type="text"
                                                       name="anggaran[]" style=" height: 10px; text-align: right" value="0">
                                            </td>
                                        </tr>
                                        @foreach($row2['childs'] as $row3)
                                            <input type="hidden" name="master_id[]"
                                                   value="{{ $row3['master_id'] }}">
                                            <input type="hidden" name="item[]" value="{{ $row3['mst_nama_rekening'] }}">
                                            <tr>
                                                <td>
                                                    {{$no++}}
                                                </td>
                                                <td>
                                                    {{ $row3['mst_nama_rekening'] }}
                                                </td>
                                                <td>
                                                    {{ $row3['mst_kode_rekening'] }}
                                                </td>
                                                <td class="td-no-bg">
                                                    <input class="form-control m-input no-bg input-numeral jml-anggaran" type="text"
                                                           name="anggaran[]" style="height: 10px; text-align: right" value="0">
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="2" class="font-weight-bold text-center">
                                    TOTAL
                                </th>
                                <th  colspan="2" class="font-weight-bold th-total-anggaran-label" style="text-align: right;">
                                    0
                                </th>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="m-portlet m-portlet--mobile hidden">

                            <div class="m-portlet__body">

                                <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                                    <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th>Tanggal</th>
                                        <th>Kode Produk</th>
                                        <th>Nama Produk</th>
                                        <th>No Seri Produk</th>
                                        <th>Qty In</th>
                                        <th>Qty Out</th>
                                        <th>Gudang</th>
                                        <th>Penerima/Keterangan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="target-produksi-buttons">
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan </span>
                </span>
            </button>
            <a href="#" onclick="history.back(-1)"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>
@endsection