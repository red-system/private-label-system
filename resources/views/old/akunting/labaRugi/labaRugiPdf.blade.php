<link rel="stylesheet" type="text/css" href="{{ base_path()}}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">LABA RUGI</font><br>
                        Tanggal : {{ $date_start.' s/d '.$date_end }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <br/>
    <div id="table">

        <table>
            <thead>
            <tr class="tabletitle">
                <th width="10%">
                    <p class="itemtext">
                        Kode Rekening
                    </p>
                </th>
                <th width="60%">
                    <p class="itemtext">
                        Nama Rekening
                    </p>
                </th>
                <th width="20%" class="text-right tableitem">
                    <p class="itemtext">
                        Nominal
                    </p>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($laba_rugi_kotor['data'] as $kotor)
                @foreach($kotor['data'] as $kotor2)
                    <tr class="service">
                        <td>
                            <p class="itemtext">
                                {{ $kotor2['mst_kode_rekening'] }}
                            </p>
                        </td>
                        <td>
                            <p class="itemtext">
                                {!!$kotor2['mst_nama_rekening'] !!}
                            </p>
                        </td>
                        <td
                                align="right">
                            <p class="itemtext">
                                {{ Main::format_number_system($kotor2['value']) }}
                            </p>
                        </td>
                    </tr>
                    @if(!empty($kotor2['childs']))
                        @foreach($kotor2['childs'] as $kotorChild1)
                            <tr class="service">
                                <td>
                                    <p class="itemtext">
                                        {{ $kotorChild1['mst_kode_rekening'] }}
                                    </p>
                                </td>
                                <td>
                                    <p class="itemtext">
                                        {!! $space1.$kotorChild1['mst_nama_rekening'] !!}
                                    </p>
                                </td>
                                <td
                                        align="right">
                                    <p class="itemtext">
                                        {{ Main::format_number_system($kotorChild1['value']) }}
                                    </p>
                                </td>
                            </tr>
                            @if(!empty($kotorChild1['childs']))
                                @foreach($kotorChild1['childs'] as $kotorChild2)
                                    <tr class="service">
                                        <td>
                                            <p class="itemtext">
                                                {{ $kotorChild2['mst_kode_rekening'] }}
                                            </p>
                                        </td>
                                        <td>
                                            <p class="itemtext">
                                                {!! $space2.$kotorChild2['mst_nama_rekening'] !!}
                                            </p>
                                        </td>
                                        <td
                                                align="right">
                                            <p class="itemtext">
                                                {{ Main::format_number_system($kotorChild2['value']) }}
                                            </p>
                                        </td>
                                    </tr>
                                    @if(!empty($kotorChild2['childs']))
                                        @foreach($kotorChild2['childs'] as $kotorChild3)
                                            <tr class="service">
                                                <td>
                                                    <p class="itemtext">
                                                        {{ $kotorChild3['mst_kode_rekening'] }}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="itemtext">
                                                        {!! $space3.$kotorChild3['mst_nama_rekening'] !!}
                                                    </p>
                                                </td>
                                                <td
                                                        align="right">
                                                    <p class="itemtext">
                                                        {{ Main::format_number_system($kotorChild3['value']) }}
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endforeach
            <tr>
                <td colspan="2" align="center">
                    <h4 class="itemtext">{{$laba_rugi_kotor['label']}}</h4>
                </td>
                <td align="right"
                >
                    <h4>
                        {{ Main::format_number_system($laba_rugi_kotor['value'])}}
                    </h4>
                </td>
            </tr>
            @foreach($laba_rugi_bersih['data'] as $bersih)
                @foreach($bersih['data'] as $bersih2)
                    <tr class="service">
                        <td>
                            <p class="itemtext">
                                {{ $bersih2['mst_kode_rekening'] }}
                            </p>
                        </td>
                        <td>
                            <p class="itemtext">
                                {!!$bersih2['mst_nama_rekening'] !!}
                            </p>
                        </td>
                        <td
                                align="right">
                            <p class="itemtext">
                                {{ Main::format_number_system($bersih2['value']) }}
                            </p>
                        </td>
                    </tr>
                    @if(!empty($bersih2['childs']))
                        @foreach($bersih2['childs'] as $bersihChild1)
                            <tr class="service">
                                <td>
                                    <p class="itemtext">
                                        {{ $bersihChild1['mst_kode_rekening'] }}
                                    </p>
                                </td>
                                <td>
                                    <p class="itemtext">
                                        {!! $space1.$bersihChild1['mst_nama_rekening'] !!}
                                    </p>
                                </td>
                                <td
                                        align="right">
                                    <p class="itemtext">
                                        {{ Main::format_number_system($bersihChild1['value']) }}
                                    </p>
                                </td>
                            </tr>
                            @if(!empty($bersihChild1['childs']))
                                @foreach($bersihChild1['childs'] as $bersihChild2)
                                    <tr class="service">
                                        <td>
                                            <p class="itemtext">
                                                {{ $bersihChild2['mst_kode_rekening'] }}
                                            </p>
                                        </td>
                                        <td>
                                            <p class="itemtext">
                                                {!! $space2.$bersihChild2['mst_nama_rekening'] !!}
                                            </p>
                                        </td>
                                        <td
                                                align="right">
                                            <p class="itemtext">
                                                {{ Main::format_number_system($bersihChild2['value']) }}
                                            </p>
                                        </td>
                                    </tr>
                                    @if(!empty($bersihChild2['childs']))
                                        @foreach($bersihChild2['childs'] as $bersihChild3)
                                            <tr class="service">
                                                <td>
                                                    <p class="itemtext">
                                                        {{ $bersihChild3['mst_kode_rekening'] }}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="itemtext">
                                                        {!! $space3.$bersihChild3['mst_nama_rekening'] !!}
                                                    </p>
                                                </td>
                                                <td
                                                        align="right">
                                                    <p class="itemtext">
                                                        {{ Main::format_number_system($bersihChild3['value']) }}
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endforeach
            <tr class="item">
                <td colspan="2" align="center">
                    {{$laba_rugi_bersih['label']}}
                </td>
                <td align="right">
                    {{ Main::format_number_system($laba_rugi_bersih['value'])}}
                </td>
            </tr>
            </tbody>

        </table>
    </div>
</div>