@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form method="get">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-gear"></i>
                            </span>
                                <h3 class="m-portlet__head-text">
                                    Filter Data
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ Main::format_date($date_start) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end"
                                           value="{{ Main::format_date($date_end) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('labaRugiPdf', $params) }}"
                               class="btn btn-danger akses-pdf" target="_blank">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('labaRugiExcel', $params) }}"
                               class="btn btn-success akses-excel" target="_blank">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">

                    <table class="table table-striped table-bordered table-hover table-checkable datatable-no-pagination" width="100%">
                        <thead>
                        <tr class="success">
                            <th width="10%">Kode Rekening</th>
                            <th width="60%">Nama Rekening</th>
                            <th width="20%" class="text-right">Nominal</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($laba_rugi_kotor['data'] as $kotor)
                            @foreach($kotor['data'] as $kotor2)
                                <tr>
                                    <td class="font-weight-bold">{{$kotor2['mst_kode_rekening']}}</td>
                                    <td class="font-weight-bold">{!! $kotor2['mst_nama_rekening'] !!}</td>
                                    <td colspan="3" align="right" class="font-weight-bold">
                                        {{ Main::format_number_system($kotor2['value']) }}
                                    </td>
                                </tr>
                                @if(!empty($kotor2['childs']))
                                @foreach($kotor2['childs'] as $kotorChild1)
                                    <tr>
                                        <td>{{$kotorChild1['mst_kode_rekening']}}</td>
                                        <td>{!! $space1.$kotorChild1['mst_nama_rekening'] !!}</td>
                                        <td colspan="3" align="right">
                                            {{ Main::format_number_system($kotorChild1['value']) }}
                                        </td>
                                    </tr>
                                    @if(!empty($kotorChild1['childs']))
                                    @foreach($kotorChild1['childs'] as $kotorChild2)
                                        <tr>
                                            <td>{{$kotorChild2['mst_kode_rekening']}}</td>
                                            <td>{!! $space2.$kotorChild2['mst_nama_rekening'] !!}</td>
                                            <td colspan="3" align="right">
                                                {{ Main::format_number_system($kotorChild2['value']) }}
                                            </td>
                                        </tr>
                                        @if(!empty($kotorChild2['childs']))
                                        @foreach($kotorChild2['childs'] as $kotorChild3)
                                            <tr>
                                                <td>{{$kotorChild3['mst_kode_rekening']}}</td>
                                                <td>{!! $space3.$kotorChild3['mst_nama_rekening'] !!}</td>
                                                <td colspan="3" align="right">
                                                    {{ Main::format_number_system($kotorChild3['value']) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    @endforeach
                                    @endif
                                @endforeach
                                @endif
                            @endforeach
                        @endforeach
                        <tr class="bg-success m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold">
                                <h4>{{$laba_rugi_kotor['label']}}</h4>
                            </td>
                            <td align="right">
                                <h4>
                                    <strong>{{ Main::format_number_system($laba_rugi_kotor['value']) }}</strong>
                                </h4>
                            </td>
                        </tr>
                        @foreach($laba_rugi_bersih['data'] as $bersih)
                            @foreach($bersih['data'] as $bersih2)
                                <tr>
                                    <td class="font-weight-bold">{{$bersih2['mst_kode_rekening']}}</td>
                                    <td class="font-weight-bold">{!! $bersih2['mst_nama_rekening'] !!}</td>
                                    <td colspan="3" align="right" class="font-weight-bold">
                                        {{ Main::format_number_system($bersih2['value']) }}
                                    </td>
                                </tr>
                                @if(!empty($bersih2['childs']))
                                    @foreach($bersih2['childs'] as $bersihChild1)
                                        <tr>
                                            <td>{{$bersihChild1['mst_kode_rekening']}}</td>
                                            <td>{!! $space1.$bersihChild1['mst_nama_rekening'] !!}</td>
                                            <td colspan="3" align="right">
                                                {{ Main::format_number_system($bersihChild1['value']) }}
                                            </td>
                                        </tr>
                                        @if(!empty($bersihChild1['childs']))
                                            @foreach($bersihChild1['childs'] as $bersihChild2)
                                                <tr>
                                                    <td>{{$bersihChild2['mst_kode_rekening']}}</td>
                                                    <td>{!! $space2.$bersihChild2['mst_nama_rekening'] !!}</td>
                                                    <td colspan="3" align="right">
                                                        {{ Main::format_number_system($bersihChild2['value']) }}
                                                    </td>
                                                </tr>
                                                @if(!empty($bersihChild2['childs']))
                                                    @foreach($bersihChild2['childs'] as $bersihChild3)
                                                        <tr>
                                                            <td>{{$bersihChild3['mst_kode_rekening']}}</td>
                                                            <td>{!! $space3.$bersihChild3['mst_nama_rekening'] !!}</td>
                                                            <td colspan="3" align="right">
                                                                {{ Main::format_number_system($bersihChild3['value']) }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="bg-success m--font-light">
                            <td colspan="2" align="center" class="font-weight-bold">
                                <h4>{{$laba_rugi_bersih['label']}}</h4>
                            </td>
                            <td align="right">
                                <h4>
                                    <strong>{{ Main::format_number_system($laba_rugi_bersih['value']) }}</strong>
                                </h4>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection