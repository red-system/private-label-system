<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laba Rugi " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="80">
        </td>
        <td colspan="2">
            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}
        </td>
        <td>
            LABA RUGI<br>
            Tanggal : {{ $date_start.' s/d '.$date_end }}
        </td>
    </tr>
</table>
<br>


<table width="100%" border="1">
    <thead>
    <tr class="service">
        <th width="10%" class="tableitem">
            <p class="itemtext">
                Kode Rekening
            </p>
        </th>
        <th width="60%" class="tableitem">
            <p class="itemtext">
                Nama Rekening
            </p>
        </th>
        <th width="20%" class="text-right tableitem">
            <p class="itemtext">
                Nominal
            </p>
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($laba_rugi_kotor['data'] as $kotor)
        @foreach($kotor['data'] as $kotor2)
            <tr class="service">
                <td class="tableitem">
                    <p class="itemtext">
                        {{ $kotor2['mst_kode_rekening'] }}
                    </p>
                </td>
                <td class="tableitem">
                    <p class="itemtext">
                        {!!$kotor2['mst_nama_rekening'] !!}
                    </p>
                </td>
                <td class="tableitem"
                    align="right">
                    <p class="itemtext">
                        {{ Main::format_number_number($kotor2['value']) }}
                    </p>
                </td>
            </tr>
            @if(!empty($kotor2['childs']))
                @foreach($kotor2['childs'] as $kotorChild1)
                    <tr class="service">
                        <td class="tableitem">
                            <p class="itemtext">
                                {{ $kotorChild1['mst_kode_rekening'] }}
                            </p>
                        </td>
                        <td class="tableitem">
                            <p class="itemtext">
                                {!! $space1.$kotorChild1['mst_nama_rekening'] !!}
                            </p>
                        </td>
                        <td class="tableitem"
                            align="right">
                            <p class="itemtext">
                                {{ Main::format_number_number($kotorChild1['value']) }}
                            </p>
                        </td>
                    </tr>
                    @if(!empty($kotorChild1['childs']))
                        @foreach($kotorChild1['childs'] as $kotorChild2)
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $kotorChild2['mst_kode_rekening'] }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {!! $space2.$kotorChild2['mst_nama_rekening'] !!}
                                    </p>
                                </td>
                                <td class="tableitem"
                                    align="right">
                                    <p class="itemtext">
                                        {{ Main::format_number_number($kotorChild2['value']) }}
                                    </p>
                                </td>
                            </tr>
                            @if(!empty($kotorChild2['childs']))
                                @foreach($kotorChild2['childs'] as $kotorChild3)
                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">
                                                {{ $kotorChild3['mst_kode_rekening'] }}
                                            </p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">
                                                {!! $space3.$kotorChild3['mst_nama_rekening'] !!}
                                            </p>
                                        </td>
                                        <td class="tableitem"
                                            align="right">
                                            <p class="itemtext">
                                                {{ Main::format_number_number($kotorChild3['value']) }}
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif
        @endforeach
    @endforeach
    <tr class="tableitem">
        <td colspan="2" align="center" class="tableitem">
            <p class="itemtext">
            <h4>{{$laba_rugi_kotor['label']}}</h4>
            </p>
        </td>
        <td align="right"
            class="tableitem">
            <p class="itemtext">
            <h4>
                {{ Main::format_number_number($laba_rugi_kotor['value'])}}
            </h4>
            </p>
        </td>
    </tr>
    @foreach($laba_rugi_bersih['data'] as $bersih)
        @foreach($bersih['data'] as $bersih2)
            <tr class="service">
                <td class="tableitem">
                    <p class="itemtext">
                        {{ $bersih2['mst_kode_rekening'] }}
                    </p>
                </td>
                <td class="tableitem">
                    <p class="itemtext">
                        {!!$bersih2['mst_nama_rekening'] !!}
                    </p>
                </td>
                <td class="tableitem"
                    align="right">
                    <p class="itemtext">
                        {{ Main::format_number_number($bersih2['value']) }}
                    </p>
                </td>
            </tr>
            @if(!empty($bersih2['childs']))
                @foreach($bersih2['childs'] as $bersihChild1)
                    <tr class="service">
                        <td class="tableitem">
                            <p class="itemtext">
                                {{ $bersihChild1['mst_kode_rekening'] }}
                            </p>
                        </td>
                        <td class="tableitem">
                            <p class="itemtext">
                                {!! $space1.$bersihChild1['mst_nama_rekening'] !!}
                            </p>
                        </td>
                        <td class="tableitem"
                            align="right">
                            <p class="itemtext">
                                {{ Main::format_number_number($bersihChild1['value']) }}
                            </p>
                        </td>
                    </tr>
                    @if(!empty($bersihChild1['childs']))
                        @foreach($bersihChild1['childs'] as $bersihChild2)
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $bersihChild2['mst_kode_rekening'] }}
                                    </p>
                                </td>
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {!! $space2.$bersihChild2['mst_nama_rekening'] !!}
                                    </p>
                                </td>
                                <td class="tableitem"
                                    align="right">
                                    <p class="itemtext">
                                        {{ Main::format_number_number($bersihChild2['value']) }}
                                    </p>
                                </td>
                            </tr>
                            @if(!empty($bersihChild2['childs']))
                                @foreach($bersihChild2['childs'] as $bersihChild3)
                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">
                                                {{ $bersihChild3['mst_kode_rekening'] }}
                                            </p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">
                                                {!! $space3.$bersihChild3['mst_nama_rekening'] !!}
                                            </p>
                                        </td>
                                        <td class="tableitem"
                                            align="right">
                                            <p class="itemtext">
                                                {{ Main::format_number_number($bersihChild3['value']) }}
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif
        @endforeach
    @endforeach
    <tr class="tableitem">
        <td colspan="2" align="center" class="tableitem">
            <p class="itemtext">
            <h4>{{$laba_rugi_bersih['label']}}</h4>
            </p>
        </td>
        <td align="right"
            class="tableitem">
            <p class="itemtext">
            <h4>
                {{ Main::format_number_number($laba_rugi_bersih['value'])}}
            </h4>
            </p>
        </td>
    </tr>
    </tbody>
</table>