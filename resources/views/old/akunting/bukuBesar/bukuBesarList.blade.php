@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ Main::format_date($date_start) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end"
                                           value="{{ Main::format_date($date_end) }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row text-center">
                        <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Parent Data</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <select name="master_id[]"
                                        class="form-control selectpicker"
                                        multiple
                                        required
                                        data-live-search="true">
                                    @if(in_array('all', $master_id))
                                        <option value="all" selected >Semua Data</option>
                                    @else
                                        <option value="all">Semua Data</option>
                                    @endif
                                    @foreach($perkiraan as $r)
                                        @if(in_array($r['master_id'], $master_id))
                                            <option value="{{ $r['master_id'] }}" selected >
                                                --- {{ $r['mst_kode_rekening'] . ' - ' . $r['mst_nama_rekening'] }}
                                            </option>
                                        @else
                                            <option value="{{ $r['master_id'] }}">
                                                --- {{ $r['mst_kode_rekening'] . ' - ' . $r['mst_nama_rekening'] }}
                                            </option>
                                        @endif
                                        @foreach($r['childs'] as $r1)
                                        @if(in_array($r1['master_id'], $master_id))
                                            <option value="{{ $r1['master_id'] }}" selected>
                                                ------ {{ $r1['mst_kode_rekening'] . ' - ' . $r1['mst_nama_rekening'] }}
                                            </option>
                                        @else
                                            <option value="{{ $r1['master_id'] }}">
                                                ------ {{ $r1['mst_kode_rekening'] . ' - ' . $r1['mst_nama_rekening'] }}
                                            </option>
                                        @endif
                                            @foreach($r1['childs'] as $r2)
                                                @if(in_array($r2['master_id'], $master_id))
                                                <option value="{{ $r2['master_id'] }}" selected >
                                                    --------- {{ $r2['mst_kode_rekening'] . ' - ' . $r2['mst_nama_rekening'] }}
                                                </option>
                                                @else
                                                <option value="{{ $r2['master_id'] }}">
                                                    --------- {{ $r2['mst_kode_rekening'] . ' - ' . $r2['mst_nama_rekening'] }}
                                                </option>
                                                @endif
                                                @foreach($r2['childs'] as $r3)
                                                    @if(in_array($r2['master_id'], $master_id))
                                                        <option value="{{ $r3['master_id'] }}" selected>
                                                        ------------ {{ $r3['mst_kode_rekening'] . ' - ' . $r3['mst_nama_rekening'] }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $r3['master_id'] }}">
                                                            ------------ {{ $r3['mst_kode_rekening'] . ' - ' . $r3['mst_nama_rekening'] }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </select>
                        </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('bukuBesarPdf', $params) }}"
                               class="btn btn-danger akses-pdf" target="_blank">
                                <i class="la la-file-pdf-o"></i> Download PDF
                            </a>
                            <a href="{{ route('bukuBesarExcel', $params) }}"
                               class="btn btn-success akses-excel" target="_blank">
                                <i class="la la-file-excel-o"></i> Download Excel
                            </a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <h3 align="center">
                        BUKU BESAR
                    </h3>
                    <h6 align="center">
                        {{ Main::format_date_label($date_start) }} s/d {{ Main::format_date_label($date_end) }}
                    </h6>
                    <br /><br />
                    @foreach($buku_besar as $b)
                        @php($no = 1)
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td style="font-size: 12px;" width="50%"><strong>Perkiraan : {{$b['mst_nama_rekening']}}</strong></td>
                                <td style="font-size: 12px;" align="right" width="50%"><strong>Kode Rek : {{$b['mst_kode_rekening']}}</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    <table class="table table-bordered table-hover m-table m-table--head-bg-success table-header-fixed datatable-buku-besar"
                           >
                        <thead>
                        <tr class="">
                            <th width="10">No</th>
                            <th>Tanggal</th>
                            <th>No Bukti</th>
                            <th>Keterangan</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>{{$b['master_saldo_awal_label']}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$no++}}</td>
                            <td></td>
                            <td></td>
                            <td>Saldo Awal</td>
                            <td class="text-right">{{Main::format_number_system(0)}}</td>
                            <td class="text-right">{{Main::format_number_system(0)}}</td>
                            <td class="text-right">{{Main::format_number_system($b['master_saldo_awal_value'])}}</td>
                        </tr>
                        @foreach($b['transaksi'] as $trs)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$trs["jurnal_umum"]["jmu_tanggal"]}}</td>
                                    <td>{{$trs["jurnal_umum"]["no_invoice"]}}</td>
                                    <td>{{$trs["jurnal_umum"]["jmu_keterangan"]}}</td>
                                    <td class="text-right">{{Main::format_number_system($trs["trs_debet"])}}</td>
                                    <td class="text-right">{{Main::format_number_system($trs["trs_kredit"])}}</td>
                                    <td class="text-right">{{Main::format_number_system($trs["saldo_now"])}}</td>
                                </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4" class="text-center">
                                <strong> TOTAL </strong>
                            </th>
                            <th class="text-right">
                                <strong>{{ Main::format_number_system($b['total_debet']) }} </strong>
                            </th>
                            <th class="text-right">
                                <strong>{{ Main::format_number_system($b['total_kredit']) }} </strong>
                            </th>
                            <th class="text-right">
                                <strong>{{ Main::format_number_system($b['master_saldo_akhir_value']) }} </strong>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endsection