<link rel="stylesheet" type="text/css" href="{{ base_path()}}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
    tr.noBorder td {
        border: 0 !important;
    }
</style>

<head>
    <div style="margin-right: 40px; margin-left: 40px">
        <div class="logo">
            <img src="{{ base_path() }}/public/images/logo.png" width="80">
        </div>
        <div class="info">
            <br>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="12">{{ $company->companyName }}</font><br>
                        {{ $company->companyAddress }}<br>
                        {{ $company->companyTelp }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="title">
            <br/>
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <font size="2">BUKU BESAR</font><br>
                        Tanggal : {{ $date_start.' s/d '.$date_end }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</head>
<br/><br/>
<div id="invoice-bot">
    <br/><br/><br/>
    <div id="table">
        @foreach($buku_besar as $b)
            @php($no = 1)
            <table>
                <thead>
                <tr class="noBorder">
                    <td style="font-size: 12px;" colspan="5"><strong>Perkiraan : {{$b['mst_nama_rekening']}}</strong>
                    </td>
                    <td style="font-size: 12px;" align="right" colspan="2"><strong>Kode Rek
                            : {{$b['mst_kode_rekening']}}</strong></td>
                </tr>
                <tr class="tabletitle">
                    <th class="item" width="10">No</th>
                    <th class="item">Tanggal</th>
                    <th class="item">No Bukti</th>
                    <th class="item">Keterangan</th>
                    <th class="item">Debet</th>
                    <th class="item">Kredit</th>
                    <th class="item">{{$b['master_saldo_awal_label']}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="item">{{$no++}}</td>
                    <td></td>
                    <td></td>
                    <td class="item">Saldo Awal</td>
                    <td class="item" style="text-align: right">{{Main::format_number_system(0)}}</td>
                    <td class="item" style="text-align: right">{{Main::format_number_system(0)}}</td>
                    <td class="item"
                        style="text-align: right">{{Main::format_number_system($b['master_saldo_awal_value'])}}</td>
                </tr>
                @foreach($b['transaksi'] as $trs)
                    <tr>
                        <td class="item">{{$no++}}</td>
                        <td class="item">{{$trs["jurnal_umum"]["jmu_tanggal"]}}</td>
                        <td class="item">{{$trs["jurnal_umum"]["no_invoice"]}}</td>
                        <td class="item">{{$trs["jurnal_umum"]["jmu_keterangan"]}}</td>
                        <td class="item" style="text-align: right">{{Main::format_number_system($trs["trs_debet"])}}</td>
                        <td class="item" style="text-align: right">{{Main::format_number_system($trs["trs_kredit"])}}</td>
                        <td class="item" style="text-align: right">{{Main::format_number_system($trs["saldo_now"])}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="4" class="text-center item">
                        <strong> TOTAL </strong>
                    </th>
                    <th class=" item" style="text-align: right">
                        <strong>{{ Main::format_number_system($b['total_debet']) }} </strong>
                    </th>
                    <th class=" item" style="text-align: right">
                        <strong>{{ Main::format_number_system($b['total_kredit']) }} </strong>
                    </th>
                    <th class=" item" style="text-align: right">
                        <strong>{{ Main::format_number_system($b['master_saldo_akhir_value']) }} </strong>
                    </th>
                </tr>
                </tfoot>
            </table>
            <br>
            <br>
        @endforeach
    </div>
</div>
