<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Buku Besar " . date('d-m-Y') . ".xls");
?>

<table width="100%">
    <tr>
        <td>
            <img src="{{ url('/logo') }}" width="80">
        </td>
        <td colspan="2">

            {{ $company->companyName }}<br>
            {{ $company->companyAddress }}<br>
            {{ $company->companyTelp }}

        </td>
        <td>
            BUKU BESAR<br>
            Tanggal : {{ $date_start.' s/d '.$date_end }}

        </td>

    </tr>
</table>
<br>
    @foreach($buku_besar as $b)
        @php($no = 1)
        <table width="100%">
            <tbody>
            <tr>
                <td style="font-size: 12px;" width="50%"><strong>Perkiraan : {{$b['mst_nama_rekening']}}</strong></td>
                <td style="font-size: 12px;" align="right" width="50%"><strong>Kode Rek : {{$b['mst_kode_rekening']}}</strong></td>
            </tr>
            </tbody>
        </table>
        <table>
            <thead>
            <tr class="tabletitle">
                <th class="item" width="10">No</th>
                <th class="item">Tanggal</th>
                <th class="item">No Bukti</th>
                <th class="item">Keterangan</th>
                <th class="item">Debet</th>
                <th class="item">Kredit</th>
                <th class="item">{{$b['master_saldo_awal_label']}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="item" style="text-align: left;  padding-left: 25px">{{$no++}}</td>
                <td></td>
                <td></td>
                <td class="item">Saldo Awal</td>
                <td class="item" style="text-align: right">{{Main::format_number_system(0)}}</td>
                <td class="item" style="text-align: right">{{Main::format_number_system(0)}}</td>
                <td class="item" style="text-align: right">{{Main::format_number_system($b['master_saldo_awal_value'])}}</td>
            </tr>
            @foreach($b['transaksi'] as $trs)
                <tr>
                    <td class="item" style="text-align: left; padding-left: 25px">{{$no++}}</td>
                    <td class="item">{{$trs["jurnal_umum"]["jmu_tanggal"]}}</td>
                    <td class="item">{{$trs["jurnal_umum"]["no_invoice"]}}</td>
                    <td class="item">{{$trs["jurnal_umum"]["jmu_keterangan"]}}</td>
                    <td class="item" style="text-align: right">{{Main::format_number_system($trs["trs_debet"])}}</td>
                    <td class="item" style="text-align: right">{{Main::format_number_system($trs["trs_kredit"])}}</td>
                    <td class="item" style="text-align: right">{{Main::format_number_system($trs["saldo_now"])}}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th colspan="4" class="text-center tableitem">
                    <strong> TOTAL </strong>
                </th>
                <th class=" tableitem" style="text-align: right">
                    <strong>{{ Main::format_number_system($b['total_debet']) }} </strong>
                </th>
                <th class=" tableitem" style="text-align: right">
                    <strong>{{ Main::format_number_system($b['total_kredit']) }} </strong>
                </th>
                <th class=" tableitem" style="text-align: right">
                    <strong>{{ Main::format_number_system($b['master_saldo_akhir_value']) }} </strong>
                </th>
            </tr>
            </tfoot>
        </table>
        <br>
        <br>
@endforeach