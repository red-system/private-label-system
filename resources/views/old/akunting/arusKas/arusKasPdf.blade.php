<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
</style>

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ base_path() }}/public/images/logo.png" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div>
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2"><h3>Arus Kas</h3></td>
                    </tr>
                    <tr>
                        <td>{{ $date_start.' s/d '.$date_end }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br/><br/><br/>
        <div id="invoice-bot">
            <br/><br/><br/>
            <br/>
            <div id="table">

                <table>
                    <tbody>
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                I. ARUS KAS DARI KEGIATAN OPERASI
                            </p>
                        </td>
                    </tr>
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                - Arus Kas Masuk :
                            </p>
                        </td>
                    </tr>
                    <?php
                    $total_operasi = 0;
                    $total_pendanaan = 0;
                    $total_investasi = 0;
                    ?>
                    @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='debet')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_debet) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_operasi = $total_operasi + $trs->trs_debet;
                            ?>
                        @endif
                    @endforeach
                    @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Operasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='debet')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{$trs->jurnal_umum['jmu_keterangan']}}
                                    </p>
                                </td>
                                <td align="right" class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_debet) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_operasi = $total_operasi + $trs->trs_debet;
                            ?>
                        @endif
                    @endforeach


                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                - Arus Kas Keluar :
                            </p>
                        </td>
                    </tr>

                    @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='kredit')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{$trs->jurnal_umum['jmu_keterangan']}}
                                    </p>
                                </td>
                                <td align="right" class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_kredit) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_operasi = $total_operasi - $trs->trs_kredit;
                            ?>
                        @endif
                    @endforeach
                    @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Operasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='kredit')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{$trs->jurnal_umum['jmu_keterangan']}}
                                    </p>
                                </td>
                                <td align="right" class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_kredit) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_operasi = $total_operasi - $trs->trs_kredit;
                            ?>
                        @endif
                    @endforeach
                    <tr class="service">
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h5>TOTAL KEGIATAN OPERASI</h5>
                            </p>
                        </td>
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h5>{{ Main::format_number($total_operasi) }}</h5>
                            </p>
                        </td>
                    </tr>
                    <tr class="service">
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            </p>
                        </td>
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            </p>
                        </td>
                    </tr>
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                II. ARUS KAS DARI KEGIATAN PENDANAAN
                            </p>
                        </td>
                    </tr>
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                - Arus Kas Masuk :
                            </p>
                        </td>
                    </tr>

                    @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                        @if($trs->trs_jenis_transaksi=='debet')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_debet) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_pendanaan = $total_pendanaan + $trs->trs_debet;
                            ?>
                        @endif
                    @endforeach
                    @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                        @if($trs->trs_jenis_transaksi=='debet')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{$trs->jurnal_umum['jmu_keterangan']}}
                                    </p>
                                </td>
                                <td align="right" class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_debet) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_pendanaan = $total_pendanaan + $trs->trs_debet;
                            ?>
                        @endif
                    @endforeach
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                - Arus Kas Keluar :
                            </p>
                        </td>
                    </tr>
                    @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                        @if($trs->trs_jenis_transaksi=='kredit')
                            <tr class="service">
                                <td class="font-weight-bold tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class="tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_kredit) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_pendanaan = $total_pendanaan - $trs->trs_kredit;
                            ?>
                        @endif
                    @endforeach
                    @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                        @if($trs->trs_jenis_transaksi=='kredit')
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class="tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_kredit) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_pendanaan = $total_pendanaan - $trs->trs_kredit;
                            ?>
                        @endif
                    @endforeach
                    <tr class="service">
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h5>TOTAL KEGIATAN PENDANAAN</h5>
                            </p>
                        </td>
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h5>{{ Main::format_number($total_pendanaan) }}</h5>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                III. ARUS KAS DARI KEGIATAN INVESTASI
                            </p>
                        </td>
                    </tr>
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                - Arus Kas Masuk :
                            </p>
                        </td>
                    </tr>

                    @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='debet')
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class="tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_debet) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_investasi = $total_investasi + $trs->trs_debet;
                            ?>
                        @endif
                    @endforeach
                    @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Investasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='debet')
                            <tr class="service">
                                <td class="tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class="tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_debet) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_investasi = $total_investasi + $trs->trs_debet;
                            ?>
                        @endif
                    @endforeach
                    <tr class="service">
                        <td colspan="2" class="font-weight-bold tableitem">
                            <p class="itemtext">
                                - Arus Kas Keluar :
                            </p>
                        </td>
                    </tr>
                    @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='kredit')
                            <tr class="service">
                                <td class=" tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class=" tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_kredit) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_investasi = $total_investasi - $trs->trs_kredit;
                            ?>
                        @endif
                    @endforeach
                    @foreach($transaksi_kas_besar->where('trs_tipe_arus_kas','Investasi') as $trs)
                        @if($trs->trs_jenis_transaksi=='kredit')
                            <tr class="service">
                                <td class=" tableitem">
                                    <p class="itemtext">
                                        {{ $trs->jurnal_umum['jmu_keterangan'] }}
                                    </p>
                                </td>
                                <td align="right" class=" tableitem">
                                    <p class="itemtext">
                                        {{ Main::format_number($trs->trs_kredit) }}
                                    </p>
                                </td>
                            </tr>
                            <?php
                            $total_investasi = $total_investasi - $trs->trs_kredit;
                            ?>
                        @endif
                    @endforeach
                    <tr class="service">
                        <td class="tableitem">
                            <p class="itemtext">
                            <h5>TOTAL KEGIATAN INVESTASI</h5>
                            </p>
                        </td>
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h5>{{ Main::format_number($total_investasi) }}</h5>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="service">
                        <td class="tableitem">
                            <p class="itemtext">
                            <h4>TOTAL</h4>
                            </p>
                        </td>
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h4>
                                {{ Main::format_number($total_investasi+$total_operasi+$total_pendanaan) }}
                            </h4>
                            </p>
                        </td>
                    </tr>
                    <tr class="service">
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                            <h4>Saldo Awal</h4>
                            </p>
                        </td>
                        <td class="font-weight-bold"><h4>{{ Main::format_number($kas_awal) }}</h4></td>
                    </tr>
                    <tr class="service">
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                                <h4>Saldo Akhir</h4>
                            </p>
                        </td>
                        <td class="font-weight-bold tableitem">
                            <p class="itemtext">
                                <h4>
                                    {{ Main::format_number($kas_awal+($total_investasi+$total_operasi+$total_pendanaan))}}
                                </h4>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>


            </div>

        </div>
    </div>
</div>