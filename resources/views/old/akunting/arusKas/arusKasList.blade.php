@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <form method="get">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-gear"></i>
                            </span>
                                <h3 class="m-portlet__head-text">
                                    Filter Data
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Tanggal Progress</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="date_start"
                                           value="{{ Main::format_date($date_start) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="date_end"
                                           value="{{ Main::format_date($date_end) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot text-center">
                        <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                            <button type="submit" class="btn btn-accent akses-filter">
                                <i class="la la-search"></i> Filter Data
                            </button>
                            <a href="{{ route('arusKasPdf', $params) }}"
                               class="btn btn-danger akses-pdf">
                                <i class="la la-file-pdf-o"></i> Print PDF
                            </a>
                            <a href="{{ route('arusKasExcel', $params) }}"
                               class="btn btn-success akses-excel">
                                <i class="la la-file-excel-o"></i> Print Excel
                            </a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover tg">
                        <tbody>
                        <tr>
                            <td colspan="2" class="font-weight-bold">I. ARUS KAS DARI KEGIATAN OPERASI</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="font-weight-bold">- Arus Kas Masuk :</td>
                        </tr>


                        <tr>
                            <td colspan="2" class="font-weight-bold">- Arus Kas Keluar :</td>
                        </tr>

                        <tr>
                            <td class="font-weight-bold"><h5>TOTAL KEGIATAN OPERASI</h5></td>
                            <td class="font-weight-bold"><h5>0</h5></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="font-weight-bold">II. ARUS KAS DARI KEGIATAN PENDANAAN</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="font-weight-bold">- Arus Kas Masuk :</td>
                        </tr>

                        <tr>
                            <td class="font-weight-bold"><h5>TOTAL KEGIATAN PENDANAAN</h5></td>
                            <td class="font-weight-bold"><h5>0</h5></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td colspan="2" class="font-weight-bold">III. ARUS KAS DARI KEGIATAN INVESTASI</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="font-weight-bold">- Arus Kas Masuk :</td>
                        </tr>

                        <tr>
                            <td colspan="2" class="font-weight-bold">- Arus Kas Keluar :</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold"><h5>TOTAL KEGIATAN INVESTASI</h5></td>
                            <td class="font-weight-bold"><h5>0</h5></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold"><h4>TOTAL</h4></td>
                            <td class="font-weight-bold">
                                <h4>
                                    Kosong
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold"><h4>Saldo Awal</h4></td>
                            <td class="font-weight-bold"><h4>0</h4></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold"><h4>Saldo Akhir</h4></td>
                            <td class="font-weight-bold">
                                <h4>
                                    Kosong
                                </h4>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection