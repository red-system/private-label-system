<link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>

<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Nomor Order</label>
    <label>:</label>
    <label class="form-control-label col-8">{{$data['data_retur']['no_retur']}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Nama Pelanggan</label>
    <label>:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$data['langganan']}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Tanggal Retur</label>
    <label>:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($data['data_retur']['date'])->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-2">Keterangan</label>
    <label>:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$data['data_retur']['keterangan_retur']}}</label>
</div>
<br>
@php
    $no = 0;
@endphp
@foreach($data['item_retur'] as $r)
    @if($r['jml_retur'] > 0)
        @php($no += 1)
        <div class="form-group m-form__group row">
            <label class="form-control-label col-2" style="text-indent: 60px"><b>Kode Barang {{$no}}</b></label>
            <label>:</label>
            <label class="form-control-label col-8" id="harga_package_edit"><b>{{$r['kode_barang']}}</b></label>
        </div>
        <div class="form-group m-form__group row">
            <label class="form-control-label col-2" style="text-indent: 60px">Nama Barang</label>
            <label>:</label>
            <label class="form-control-label col-8" id="harga_package_edit">{{$r['nama_barang']}}</label>
        </div>
        <div class="form-group m-form__group row">
            <label class="form-control-label col-2" style="text-indent: 60px">Jumlah Retur</label>
            <label>:</label>
            <label class="form-control-label col-8"
                   id="harga_package_edit">{{$r['jml_retur'].'.'.$r['stok']->satuan}}</label>
        </div>
        <div class="form-group m-form__group row">
            <label class="form-control-label col-2" style="text-indent: 60px">Lokasi Retur</label>
            <label>:</label>
            <label class="form-control-label col-8"
                   id="harga_package_edit">{{$r['stok']->lokasi}}</label>
        </div>
        <div class="form-group m-form__group row">
            <label class="form-control-label col-2" style="text-indent: 60px">Sub Total Retur</label>
            <label>:</label>
            <label class="form-control-label col-8"
                   id="harga_package_edit">{{$r['subtotal']}}</label>
        </div>
        <br>
    @endif
@endforeach
<div class="form-group m-form__group row">
    <label class="form-control-label col-2"><b>Total Retur</b></label>
    <label>:</label>
    <label class="form-control-label col-8" id="harga_package_edit"><b>{{$data['data_retur']['total_retur']}}</b></label>
</div>