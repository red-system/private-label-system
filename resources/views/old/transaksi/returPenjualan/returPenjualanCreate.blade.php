@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/retur_penjualan.js') }}" type="text/javascript"></script>
    {{--    <script src="{{ asset('js/sales_order.js') }}" type="text/javascript"></script>--}}
@endsection

@section('body')

    <form action="{{ route('returPenjualanInsert',['id'=>$penjualan->id]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('penjualanPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        {{ csrf_field() }}

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Retur</label>
                                <div class="col-8 col-form-label">{{$no_retur}}</div>
                                <input type="hidden" name="no_retur" value="{{$no_retur}}">
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">No Penjualan</label>
                                <div class="col-8 col-form-label">{{$penjualan->no_penjualan}}</div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Tanggal Retur</label>
                                <div class="col-6">
                                    <div class="input-group date">
                                        <input type="text" name="tgl_retur" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
                                <div class="col-9">
                                    <textarea class="form-control m-input" name="keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Lokasi</th>
                                <th>Stok Saat Ini</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Disc (%)</th>
                                <th>Disc (Nominal)</th>
                                <th>Retur Sebelumnya</th>
                                <th>Qty Retur</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item_penjualan as $r)
                                <tr data-index="{{ $no++ }}"
                                    data-id-penjualan-produk="{{ $r->id }}">
                                    <td class="m--hide data">
                                        <input type="hidden" name="id_item_penjualan[{{ $r->id }}]" value="{{$r->id}}">
                                        <input type="hidden" name="id_barang[{{ $r->id }}]" value="{{$r->id_barang}}">
                                        <input type="hidden" name="id_stok[{{ $r->id }}]" value="{{$r->id_stok}}">
                                        <input type="hidden" name="jml_barang_penjualan[{{ $r->id }}]"
                                               value="{{$r->jml_barang_penjualan}}">
                                        <input type="hidden" name="satuan_penjualan[{{ $r->id }}]"
                                               value="{{$r->satuan_penjualan}}">
                                        <input type="hidden" name="harga_penjualan[{{ $r->id }}]"
                                               value="{{$r->harga_penjualan}}">
                                        <input type="hidden" name="diskon_persen[{{ $r->id }}]"
                                               value="{{$r->disc_persen_penjualan}}">
                                        <input type="hidden" name="diskon_nominal[{{ $r->id }}]"
                                               value="{{$r->disc_nominal}}">
                                        <input type="hidden" name="subtotal_retur[{{ $r->id }}]">
                                    </td>
                                    <td class="kode-barang">
                                        {{$r->barang->kode_barang}}
                                    </td>
                                    <td class="produk-nama">
                                        {{$r->barang->nama_barang}}
                                    </td>
                                    <td class="lokasi">
                                        {{ $r->lokasi }}
                                    </td>
                                    <td class="td-stok">
                                        {{Main::format_number($r->jml_barang)}}
                                    </td>
                                    <td class="td-qty">
                                        {{$r->jml_barang_penjualan}}
                                    </td>
                                    <td class="td-satuan">
                                        {{$r->satuan_penjualan}}
                                    </td>
                                    <td class="harga" width="10%">
                                        {{Main::format_number($r->harga_penjualan)}}
                                    </td>
                                    <td class="persen">
                                        {{Main::format_number($r->disc_persen_penjualan)}}%
                                    </td>
                                    <td class="nominal">
                                        {{$r->disc_nominal}}
                                    </td>
                                    <td>
                                        @foreach($retur as $i)
                                            @if($i->id_barang == $r->id_barang)
                                                {{$i->jumlah_barang_retur}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="td-qty-retur" width="15%">
                                        <input type="text" name="qty_retur[{{$r->id}}]"
                                               class="touchspin-number qty-retur" value="0" style="width: 50px">
                                    </td>
                                    <td class="td-sub-total" width="10%">
                                        0
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                                                data-confirm="false">
                                            <i class="la la-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile hidden">

                    <div class="m-portlet__body">

                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Tanggal</th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>No Seri Produk</th>
                                <th>Qty In</th>
                                <th>Qty Out</th>
                                <th>Gudang</th>
                                <th>Penerima/Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-8">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total Nilai Retur
                                    Sebelumnya</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="total_sebelumnya">{{$total_sebelumnya}}</span></strong>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total Nilai Retur Saat
                                    Ini</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="total">0</span></strong>
                                </div>
                                <input type="hidden" name="total_retur">
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total Keseluruhan Nilai
                                    Retur</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="total_keseluruhan">0</span></strong>
                                </div>
                            </div>
                        </div>
                        <div class="target-produksi-buttons">
                                        <span>
                                        <button type="submit"
                                                class="btn-simpan btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                            <span>
                                                <i class="la la-check"></i>Simpan Data</span></button>
                                            </span>
                            <a href="#" onclick="history.back(-1)"
                               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-angle-double-left"></i>
                                            <span>Kembali</span>
                                        </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection