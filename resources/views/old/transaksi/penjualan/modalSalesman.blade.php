<div class="modal" id="modal-salesman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-success">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Salesman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable datatable-general">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Salesman</th>
                        <th>Nama Salesman</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($no = 1)
                    @foreach($salesman as $r)
                        <tr>
                            <td width="10">{{ $no++ }}.</td>
                            <td>{{ $r->kode_salesman }}</td>
                            <td>{{ $r->salesman }}</td>
                            <td width="30">
                                <button type="button"
                                        class="select-salesman btn btn-success btn-sm m-btn--pill"
                                        data-id="{{ $r->id }}"
                                        data-kode-salesman="{{ $r->kode_salesman }}"
                                        data-nama-salesman="{{ $r->salesman }}"
                                        data-telepon-salesman="{{ $r->salesman_kontak }}"
                                        data-alamat-salesman="{{ $r->salesman_alamat }}">
                                    <i class="la la-check"></i> Pilih
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>