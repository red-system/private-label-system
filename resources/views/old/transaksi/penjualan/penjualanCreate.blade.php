@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
    {{--    <script src="{{ asset('js/sales_order.js') }}" type="text/javascript"></script>--}}
@endsection

@section('body')


    <form action="{{ route('penjualanInsert',['id'=>$so->id]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('salesOrderPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        {{--        @include('transaksi/penjualanProduk/modalPembayaranCreate')--}}

        {{ csrf_field() }}
        <input type="hidden" name="penerima" value="{{$so->penerima}}">
        <input type="hidden" name="alamat_pengiriman" value="{{$so->alamat_pengiriman}}">
        <input type="hidden" name="id_langganan" value="{{$pelanggan->id}}">
        <input type="hidden" name="total" value="{{$so->total_so}}">
        <input type="hidden" name="grand_total" value="{{$so->grand_total_so}}">
        <input type="hidden" name="no_penjualan" value="{{$no_faktur}}">
        <input type="hidden" name="urutan" value="{{$urutan}}">
        <input type="hidden" name="id_salesman" value="{{$so->id_salesman}}">
        <input type="hidden" name="pelanggan_penjualan" value="{{$so->pelanggan_so}}">
        <input type="hidden" name="pajak_nominal" value="{{$so->pajak_nominal_so}}">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
                                <div class="col-9 col-form-label">{{$no_faktur}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Faktur</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No SO</label>
                                <div class="col-9 col-form-label no_so" name="no_so">{{$so->no_so}}</div>
                                <input type="hidden" name="no_so" value="{{$so->no_so}}">
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi Order </label>
                                <div class="col-9 col-form-label no_so"
                                     name="lokasi_penjualan">{{$lokasi_penjualan}}</div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Langganan</label>
                                <div class="col-9 col-form-label no_so"><b>{{$so->pelanggan_so}}</b></div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Salesman </label>
                                <div class="col-9 col-form-label no_so"><b>{{$salesman}}</b></div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
                                <div class="col-9">
                                    <textarea class="form-control m-input" name="keterangan"></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Kontak </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" class="form-control m-input" name="kontak"
                                               value="{{$pelanggan->langganan_kontak}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jatuh Tempo</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_tempo" class="form-control m-input"
                                               id="m_datepicker_5"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi Penjualan </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <select class="form-control m-select2" name="lokasi_penjualan">
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($lokasi as $r)
                                                <option value="{{$r->lokasi}}">{{$r->lokasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input" name="alamat"
                                                  readonly>{{$pelanggan->langganan_alamat}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Lokasi</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Disc (%)</th>
                                <th>Disc (Nominal)</th>
                                <th>Harga Net</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item_so as $r)
                                <tr data-index="{{ $no++ }}"
                                    data-id-penjualan-produk="{{ $r->id }}">
                                    <td class="m--hide data">
                                        <input type="hidden" name="id_barang[{{ $r->id }}]" class="id_barang"
                                               value="{{ $r->id_barang }}">
                                        <input type="hidden" name="id_stok[{{$r->id}}]" class="td-stok-id"
                                               value="{{$r->id_stok}}">
                                        <input type="hidden" name="harga[{{ $r->id }}]" class="harga"
                                               value="{{ $r->harga_jual_barang }}">
                                        <input type="hidden" name="harga_net[{{ $r->id }}]" class="harga_net"
                                               value="{{ $r->harga_net }}">
                                        <input type="hidden" name="sub_total[{{ $r->id }}]" class="sub_total"
                                               value="{{ $r->subtotal }}">
                                        <input type="hidden" name="satuan[{{ $r->id }}]" class="td-satuan"
                                               value="{{$r->satuan_so}}">
                                        <input type="hidden" name="id_lokasi[{{ $r->id }}]" class="td-satuan"
                                               value="{{$r->id_lokasi}}">
                                        <input type="hidden" name="lokasi[{{ $r->id }}]" value="{{$r->lokasi}}">

                                    </td>
                                    <td>
                                        {{$r->kode_barang}}
                                    </td>
                                    <td class="produk-nama">
                                        {{$r->nama_barang}}
                                    </td>
                                    <td>
                                        {{$r->lokasi}}
                                    </td>
                                    <td class="td-harga">
                                        {{Main::format_number($r->harga_so)}}
                                    </td>
                                    <td class="td-qty">
                                        <input type="hidden" name="qty[{{ $r->id }}]" value="{{$r->jml_barang_so}}">
                                        {{Main::format_number($r->jml_barang_so)}}
                                    </td>
                                    <td class="td-satuan">
                                        {{$r->satuan_so}}
                                    </td>
                                    <td class="td-ppn">
                                        <input type="hidden" name="diskon_persen_satuan[{{ $r->id }}]"
                                               value="{{$r->disc_persen_so}}">
                                        {{$r->disc_persen_so}}%
                                    </td>
                                    <td class="td-potongan">
                                        <input type="hidden" name="diskon_nominal_satuan[{{ $r->id }}]"
                                               value="{{$r->disc_nominal_so}}">
                                        {{Main::format_number($r->disc_nominal_so)}}
                                    </td>
                                    <td class="td-harga-net">
                                        {{Main::format_number($r->harga_net)}}
                                    </td>
                                    <td class="td-sub-total">
                                        {{Main::format_number($r->subtotal)}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="total">{{Main::format_number($so->total_so)}}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc (%)</label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="diskon_persen" value="{{$so->disc_persen_so}}">
                                    <span>{{Main::format_number($so->disc_persen_so)}}%</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc Tambahan
                                    (Nominal)</label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="potongan_akhir" value="{{$so->disc_nominal_so}}">
                                    <span>{{Main::format_number($so->disc_nominal_so)}}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak</label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="pajak" value="{{$so->pajak_persen_so}}">
                                    <span>{{Main::format_number($so->pajak_persen_so)}}%</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Ongkos Kirim</label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="biaya_tambahan" value="{{$so->ongkos_kirim_so}}">
                                    <span>{{Main::format_number($so->ongkos_kirim_so)}}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                <div class="col-8 col-form-label">
                                    <strong>
                                        <span class="grand-total">
                                            {{Main::format_number($so->grand_total_so)}}
                                        </span>
                                    </strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">DP</label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="bayar" value="{{$so->dp_so}}">
                                    <span>{{Main::format_number($so->dp_so)}}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Metode Pembayaran </label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="metode_pembayaran"
                                           value="{{$so->metode_pembayaran_dp_so}}">
                                    <span>{{$so->metode_pembayaran_dp_so}}</span>
                                </div>
                            </div>
                            @php
                                $sisa_pembayaran = $so->grand_total_so - $so->dp_so;
                            @endphp
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Sisa Pembayaran </label>
                                <div class="col-8 col-form-label">
                                    <input type="hidden" name="sisa_pembayaran" value="{{$sisa_pembayaran}}">
                                    <span>{{Main::format_number($sisa_pembayaran)}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="target-produksi-buttons">
                            <span>
                            <button type="submit"
                                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-check"></i>
                                    Simpan Data
                                </span></button>
                            </span>
                            <a href="#" onclick="history.back(-1)"
                               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-angle-double-left"></i>
                                    <span>Kembali</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </form>


@endsection