<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Order</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8">{{$penjualan->no_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Pelanggan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$penjualan->pelanggan_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Salesman</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$penjualan->salesman}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Order</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($penjualan->tgl_penjualan)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Kirim</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($penjualan->tgl_kirim_penjualan)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Alamat Pengiriman</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$penjualan->alamat_pengiriman}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Penerima</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$penjualan->penerima}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Keterangan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$penjualan->keterangan_penjualan}}</label>
</div>
@php
    $no = 1;
@endphp
@foreach($item as $r)
    @php
        $barang = \app\Models\Widi\mBarang
                            ::where('id', $r->id_barang)->first();
        $stok = \app\Models\Widi\mStok
                            ::where('id', $r->id_stok)->with('lokasi')
                            ->first();

    @endphp

    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px"><b>Barang {{$no++}}</b></label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$barang->nama_barang}}</label>

    </div>

    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Lokasi</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$stok->lokasi->lokasi}}</label>
    </div>
    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Jumlah Order</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8"
               id="harga_package_edit">{{$r->jml_barang_penjualan." ".$r->satuan_penjualan}}</label>
    </div>
    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Harga Net</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->harga_penjualan}}</label>
    </div>
    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Sub Total</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->subtotal}}</label>
    </div>

@endforeach
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Grand Total</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$penjualan->grand_total_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Metode Pembayaran DP</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{$penjualan->metode_pembayaran_dp_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">DP</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$penjualan->dp_penjualan}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3"><b>Sisa Pembayaran</b></label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$sisa}}</label>
</div>

<table class="table-pembayaran table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>No</th>
        <th>No Bukti Retur</th>
        <th>Tanggal Retur</th>
        <th>Qty Retur</th>
        <th>Keterangan</th>

    </tr>
    </thead>
    <tbody>


    @php
        $no = 0;
    @endphp
    @foreach($history as $r)
        @php($no = $no + 1)
        <tr data-index="">
            <td>
                {{$no}}
            </td>
            <td>
                {{$r->no_retur}}
            </td>
            <td>
                {{\Carbon\Carbon::parse($r->tgl_retur)->format('d-m-Y')}}
            </td>
            <td>
                {{Main::format_number($r->jml_retur_penjualan)}}
            </td>
            <td>
                {{Main::format_money($r->subtotal_retur)}}
            </td>
        </tr>

    @endforeach
    @if($no == 0)
        <tr data-index="">
            <td colspan="5" style="text-align: center">
                Tidak Ada Data Retur
            </td>
        </tr>
    @endif
    </tbody>
</table>