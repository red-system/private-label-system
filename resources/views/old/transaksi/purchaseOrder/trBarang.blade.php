<table class="row-produk m--hide">
    <tr>
        <td class="m--hide data">
            <input type="hidden" name="id_barang[]" class="id-barang" value="0">
            <input type="hidden" name="discount_nominal[]" class="discount-nominal" value="0">
            <input type="hidden" name="sub_total[]" class="sub-total" value="0">
        </td>
        <td>
            <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td class="produk-nama">-</td>
        <td class="td-qty">
            <input type="text" name="qty[]" class="input-numeral form-control qty" value="1">
        </td>
        <td class="td-satuan">
            <select class="form-control select2" name="id_satuan[]" style="width: 150px">
                <option value="">Pilih Satuan</option>
                @foreach($satuan as $row)
                    <option value="{{ $row->id }}">{{ $row->satuan }}</option>
                @endforeach
            </select>
        </td>
        <td class="td-harga">
            <input type="text" name="harga[]" class="input-numeral form-control harga" value="0">
        </td>
        <td class="td-discount">
            <input type="text" name="discount[]" class="input-numeral form-control discount" value="0">
        </td>
        <td class="td-discount-nominal">
            -
        </td>
        <td class="td-sub-total">
            -
        </td>
        <td>
            <button type="button" class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i>
            </button>
        </td>
    </tr>
</table>