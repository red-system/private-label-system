<table class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Lokasi</th>
        <th>Stok</th>
    </tr>
    </thead>
    <tbody>
    @php($qty = 0)
    @foreach($produkStok as $r)
        @php(
            $qty += $r->jml_barang
        )
        <tr>
            <td>{{ $no++ }}.</td>
            <td>{{ $r->lokasi->lokasi }}</td>
            <td align="right">{{ Main::format_number($r->jml_barang) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="2" align="center">
            <strong>Total</strong>
        </td>
        <td align="right">
            <strong>{{ Main::format_number($qty) }}</strong>
        </td>
    </tr>
    </tfoot>
</table>