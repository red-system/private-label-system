@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/purchase_order.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('old.transaksi.salesOrder.modalBarang')
    @include('old.transaksi.salesOrder.modalStokBarang')
    @include('old.transaksi.purchaseOrder.modalSupplier')
    @include('old.transaksi.purchaseOrder.trBarang')

    <form action="{{ route('purchaseOrderUpdate', ['id' => Main::encrypt($po->id)]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('purchaseOrderPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">


        {{ csrf_field() }}
        <input type="hidden" name="no_po" value="{{$po->no_po}}">
        <input type="hidden" name="no_po_label" value="{{$po->no_po_label}}">
        <input type="hidden" name="id_supplier" value="{{ $po->id_supplier }}">
        <input type="hidden" name="total_po" value="{{ $po->total_po }}">
        <input type="hidden" name="pajak_nominal_po" value="{{ $po->pajak_nominal_po }}">
        <input type="hidden" name="grand_total_po" value="{{ $po->grand_total_po }}">
        <input type="hidden" name="sisa_pembayaran_po" value="{{ $po->sisa_pembayaran_po }}">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Order</label>
                                <div class="col-9 col-form-label">{{$po->no_po_label}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tgl_po" class="form-control m-input m_datepicker"
                                               readonly="" value="{{ Main::format_date($po->tgl_po) }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label required">Lokasi </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <select class="form-control m-select2" name="id_lokasi">
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($lokasi as $r)
                                                <option value="{{$r->id}}" {{ $r->id == $po->id_lokasi ? 'selected':'' }}>{{$r->lokasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Kirim</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tgl_kirim_po" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               readonly="" value="{{ Main::format_date($po->tgl_kirim_po) }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">


                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label required">Supplier</label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="supplier"
                                               placeholder="Nama Supplier..." value="{{ $po->supplier->supplier }}"
                                               disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-supplier">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input"
                                                  name="keterangan_po">{{ $po->keterangan_po }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-barang btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Barang
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-barang table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Barang</th>
                                <th>Nama Barang</th>
                                <th width="120">Qty</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Disc (%)</th>
                                <th>Disc (Nominal)</th>
                                <th>Sub Total</th>
                                <th width="30">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($po_barang as $key=>$row)
                                <tr data-index="{{ $key+1 }}">
                                    <td class="m--hide data">
                                        <input type="hidden" name="id_barang[]" class="id-barang"
                                               value="{{ $row->id_barang }}">
                                        <input type="hidden" name="discount_nominal[]" class="discount-nominal"
                                               value="{{ $row->discount_nominal }}">
                                        <input type="hidden" name="sub_total[]" class="sub-total"
                                               value="{{ $row->sub_total }}">
                                    </td>
                                    <td>
                                        <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                                                type="button">
                                            <i class="la la-search"></i> Cari
                                        </button>
                                    </td>
                                    <td class="produk-nama">{{ '('.$row->barang->kode_barang.') '.$row->barang->nama_barang }}</td>
                                    <td class="td-qty">
                                        <input type="text" name="qty[]" class="input-numeral form-control qty"
                                               value="{{ $row->qty }}">
                                    </td>
                                    <td class="td-satuan">
                                        <select class="form-control m-select2" name="id_satuan[]" style="width: 150px">
                                            <option value="">Pilih Satuan</option>
                                            @foreach($satuan as $row_satuan)
                                                <option value="{{ $row_satuan->id }}" {{ $row_satuan->id == $row->id_satuan ? 'selected':'' }}>{{ $row_satuan->satuan }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="td-harga">
                                        <input type="text" name="harga[]" class="input-numeral form-control harga"
                                               value="{{ $row->harga }}">
                                    </td>
                                    <td class="td-discount">
                                        <input type="text" name="discount[]" class="input-numeral form-control discount"
                                               value="{{ $row->discount }}">
                                    </td>
                                    <td class="td-discount-nominal">
                                        {{ Main::format_number($row->discount_nominal) }}
                                    </td>
                                    <td class="td-sub-total">
                                        {{ Main::format_number($row->sub_total) }}
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                                                data-confirm="false">
                                            <i class="la la-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span
                                                class="total-po">{{ Main::format_number($po->total_po) }}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Discount (%)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral discount-persen-po" type="text"
                                           name="discount_persen_po"
                                           value="{{ Main::format_number($po->discount_persen_po) }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Discount (Rp)
                                    (Nominal)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral discount-nominal-po" type="text"
                                           name="discount_nominal_po" value="{{ $po->discount_nominal_po }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak (%)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral pajak-persen-po" type="text"
                                           name="pajak_persen_po"
                                           value="{{ Main::format_number($po->pajak_persen_po) }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak (Rp)
                                    (Nominal)</label>
                                <div class="col-8">
                                    <strong><span
                                                class="pajak-nominal-po">{{ Main::format_number($po->pajak_nominal_po) }}</span></strong>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Ongkos Kirim</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral ongkos-kirim-po" type="text"
                                           name="ongkos_kirim_po" value="{{ $po->ongkos_kirim_po }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span
                                                class="grand-total-po">{{ Main::format_number($po->grand_total_po) }}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">DP - Down Payment</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral dp-po" type="text"
                                           name="dp_po" value="{{ $po->dp_po }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">DP - Metode
                                    Pembayaran </label>
                                <div class="col-8">
                                    <select class="form-control m-select2" name="metode_pembayaran_dp_po">
                                        <option value="">Pilih Metode</option>
                                        <option value="cash" {{ $po->metode_pembayaran_dp_po == 'cash' ? 'selected':'' }}>
                                            Cash
                                        </option>
                                        <option value="transfer" {{ $po->metode_pembayaran_dp_po == 'transfer' ? 'selected':'' }}>
                                            Transfer
                                        </option>
                                        <option value="bank" {{ $po->metode_pembayaran_dp_po == 'bank' ? 'selected':'' }}>
                                            Bank
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Sisa Pembayaran</label>
                                <div class="col-8 col-form-label">
                                    <strong><span
                                                class="sisa-pembayaran-po">{{ Main::format_number($po->sisa_pembayaran_po) }}</span></strong>
                                </div>
                            </div>
                        </div>

                        <div class="produksi-buttons">
                            <button type="submit"
                                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Perbarui Data</span>
                        </span>
                            </button>

                            <a href="#"
                               onclick="history.back(-1)"
                               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar</span>
                        </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection