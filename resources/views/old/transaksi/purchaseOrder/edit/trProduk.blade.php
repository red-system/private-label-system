<table class="row-produk m--hide">
    <tr>
        <td class="m--hide data">
            <input type="hidden" name="id_barang[]" class="id_barang">
            <input type="hidden" name="harga[]" class="harga">
            <input type="hidden" name="id_stok[]" class="td-stok">
            <input type="hidden" name="harga_net[]" class="td-harga-net">
            <input type="hidden" name="sub_total[]" class="sub_total">
            <input type="hidden" name="satuan[]" class="td-satuan">
        </td>
        <td>
            <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td class="produk-nama">-</td>
        <td class="td-stok-produk">
            <select class="form-control select2-penjualan id_lokasi" name="id_lokasi[]" disabled style="width: 140px">

            </select>
        </td>
        <td class="td-stok">
            -
        </td>
        <td class="td-harga">
            -
        </td>
        <td class="td-qty">
            <input type="text" name="qty[]" class="touchspin-penjualan qty" value="1" style="width: 50px">
        </td>
        <td class="td-satuan">
            -
        </td>
        <td class="td-ppn">
            <input type="text" name="ppn_nominal[]" class="touchspin-penjualan ppn_nominal" value="0"
                   style="width: 50px">
        </td>
        <td class="td-potongan">
            <input type="text" name="potongan[]" class="touchspin-penjualan potongan" value="0" style="width: 50px">
        </td>
        <td class="td-harga-net">
            -
        </td>
        <td class="td-sub-total">
            -
        </td>
        <td>
            <button type="button" class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i>
            </button>
        </td>
    </tr>
</table>