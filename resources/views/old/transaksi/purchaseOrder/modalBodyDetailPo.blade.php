<div class="row">
    <div class="col-lg-4">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">No Order</label>
            <div class="col-9 col-form-label">{{$po->no_po_label}}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
            <div class="col-9">
                <div class="input-group date">
                    {{ Main::format_date_label($po->tgl_po)}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label required">Lokasi </label>
            <div class="col-9">
                <div class="input-group date">
                    {{ $po->lokasi->lokasi }}
                </div>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Tanggal Kirim</label>
            <div class="col-9">
                <div class="input-group date">
                    {{ Main::format_date_label($po->tgl_kirim_po)}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label required">Supplier</label>
            <div class="col-9">
                <div class="input-group">
                    {{ $po->supplier->supplier }}
                </div>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
            <div class="col-9">
                <div class="input-group date">
                    {{ $po->keterangan_po }}
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>


<table class="table-barang table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Qty</th>
        <th>Satuan</th>
        <th>Harga</th>
        <th>Disc (%)</th>
        <th>Disc (Nominal)</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($po_barang as $key=>$row)
        <tr>
            <td>
                {{ $row->barang->kode_barang }}
            </td>
            <td>{{ $row->barang->nama_barang }}</td>
            <td>{{ $row->qty }}</td>
            <td>{{ $row->satuan->satuan }}</td>
            <td>{{ Main::format_number($row->harga) }}</td>
            <td>{{ Main::format_number($row->discount) }}</td>
            <td>{{ Main::format_number($row->discount_nominal) }}</td>
            <td>{{ Main::format_number($row->sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<br/>
<br/>


<div class="row">
    <div class="col-lg-6">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Total</label>
            <div class="col-6 col-form-label">
                <strong><span class="total-po">{{ Main::format_number($po->total_po) }}</span></strong>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Discount (%)</label>
            <div class="col-6">{{ Main::format_number($po->discount_persen_po) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Discount (Rp)
                (Nominal)</label>
            <div class="col-6">
                <strong><span
                            class="discount-nominal-po">{{ Main::format_number($po->discount_nominal_po) }}</span></strong>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Pajak (%)</label>
            <div class="col-6">{{ Main::format_number($po->pajak_persen_po) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Pajak (Rp) (Nominal)</label>
            <div class="col-6">
                <strong><span
                            class="pajak-nominal-po">{{ Main::format_number($po->pajak_nominal_po) }}</span></strong>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Ongkos Kirim</label>
            <div class="col-6">{{ Main::format_number($po->ongkos_kirim_po) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Grand Total</label>
            <div class="col-6 col-form-label">
                <strong><span class="grand-total-po">{{ Main::format_number($po->grand_total_po) }}</span></strong>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">DP - Down Payment</label>
            <div class="col-6">{{ Main::format_number($po->dp_po) }}</div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Metode Pembayaran </label>
            <div class="col-6">
                {{ ucwords($po->metode_pembayaran_dp_po) }}
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-6 col-form-label">Sisa Pembayaran</label>
            <div class="col-6 col-form-label">
                <strong><span
                            class="sisa-pembayaran-po">{{ Main::format_number($po->sisa_pembayaran_po) }}</span></strong>
            </div>
        </div>
    </div>
</div>