@extends('../general/index')

@section('css')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/pembelian_purchase_order.js') }}"></script>
@endsection

@section('body')

    <form action="{{ route('pembelianPurchaseOrderInsert',['id'=>Main::encrypt($po->id)]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('pembelianPurchaseOrderPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        <input type="hidden" name="no_pembelian" value="{{ $no_pembelian }}">
        <input type="hidden" name="no_pembelian_label" value="{{ $no_pembelian_label }}">
        <input type="hidden" name="sisa_pembayaran_po" value="{{ $po->sisa_pembayaran_po }}">

        {{ csrf_field() }}

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Pembelian</label>
                                <div class="col-9 col-form-label">{{$no_pembelian_label}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Pembelian</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_pembelian" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Surat Jalan</label>
                                <div class="col-9 col-form-label no_so">
                                    <input type="text" name="no_surat_jalan" class="form-control">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Surat Jalan</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_surat_jalan" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No PO</label>
                                <div class="col-9">
                                    {{ $po->no_po_label }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nama Supplier</label>
                                <div class="col-9">{{ $po->supplier->supplier }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi</label>
                                <div class="col-9">{{ $po->lokasi->lokasi }}</div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input" name="keterangan_pembelian"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pajak : No Faktur</label>
                                <div class="col-9">
                                    <input type="text" class="form-control m-input" name="pajak_no_faktur">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Jatuh Tempo</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_jatuh_tempo" class="form-control m-input"
                                               id="m_datepicker_5"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <h4 class="m-portlet__head-text">
                                Daftar Barang Purchase Order
                            </h4>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="15">No</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Discount (%)</th>
                                <th>Discount (Nominal)</th>
                                <th>Sub Total</th>
                                <th>HPP Barang</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($po->po_barang as $key=>$row)
                                <input type="hidden" name="id_po_barang[]" value="{{ $row->id }}">
                                <input type="hidden" name="id_barang[]" value="{{ $row->id_barang }}">
                                <input type="hidden" name="id_satuan[]" value="{{ $row->id_satuan }}">
                                <input type="hidden" name="qty[]" value="{{ $row->qty }}">
                                <input type="hidden" name="satuan[]" value="{{ $row->satuan->satuan }}">
                                <tr>
                                    <td>{{ $key+1 }}.</td>
                                    <td>{{ $row->barang->nama_barang }}</td>
                                    <td align="right">{{ Main::format_number($row->qty) }}</td>
                                    <td>{{ $row->satuan->satuan }}</td>
                                    <td align="right">{{ Main::format_number($row->harga) }}</td>
                                    <td align="right">{{ Main::format_number($row->discount) }}%</td>
                                    <td align="right">{{ Main::format_number($row->discount_nominal) }}</td>
                                    <td align="right">{{ Main::format_number($row->sub_total) }}</td>
                                    <td>
                                        <input type="text" name="hpp_barang[]"
                                               class="form-control m-input input-numeral" value="">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span
                                                class="total-po">{{ Main::format_number($po->total_po) }}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Discount (%)</label>
                                <div class="col-8">{{ Main::format_number($po->discount_persen_po) }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Discount (Rp)
                                    (Nominal)</label>
                                <div class="col-8">
                                    <strong><span
                                                class="discount-nominal-po">{{ Main::format_number($po->discount_nominal_po) }}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak (%)</label>
                                <div class="col-8">{{ Main::format_number($po->pajak_persen_po) }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak (Rp)
                                    (Nominal)</label>
                                <div class="col-8">
                                    <strong><span
                                                class="pajak-nominal-po">{{ Main::format_number($po->pajak_nominal_po) }}</span></strong>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Ongkos Kirim</label>
                                <div class="col-8">{{ Main::format_number($po->ongkos_kirim_po) }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span
                                                class="grand-total-po">{{ Main::format_number($po->grand_total_po) }}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">DP - Down Payment</label>
                                <div class="col-8">{{ Main::format_number($po->dp_po) }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">DP - Metode
                                    Pembayaran </label>
                                <div class="col-8">{{ ucwords($po->metode_pembayaran_dp_po) }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Sisa Pembayaran</label>
                                <div class="col-8 col-form-label">
                                    <strong>
                                        <span class="sisa-pembayaran-po">{{ Main::format_number($po->sisa_pembayaran_po) }}</span>
                                    </strong>
                                </div>
                            </div>
                            @if($po->sisa_pembayaran_po > 0)
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-4 col-form-label">Metode Pembayaran <br/>-
                                        Sisa Pembayaran</label>
                                    <div class="col-8 col-form-label">
                                        <select class="form-control m-select2" name="metode_pembayaran_hutang">
                                            <option value="">Pilih Metode</option>
                                            <option value="cash">Cash</option>
                                            <option value="hutang_supplier">Hutang Suplier</option>
                                            <option value="hutang_giro">Hutang Giro</option>
                                            <option value="transfer">Transfer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row part-hutang-giro hidden">
                                    <label for="example-text-input" class="col-4 col-form-label">Bank Hutang
                                        Giro</label>
                                    <div class="col-8 col-form-label">
                                        <select class="form-control m-select2" name="hutang_giro_bank"
                                                style="width: 100%">
                                            <option value="BNI">BNI</option>
                                            <option value="BCA">BCA</option>
                                            <option value="Mandiri">Mandiri</option>
                                            <option value="Permata Bank">Permata Bank</option>
                                            <option value="BRI">BRI</option>
                                            <option value="BPD">BPD</option>
                                            <option value="Bank Sinar">Bank Sinar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row part-hutang-giro hidden">
                                    <label for="example-text-input" class="col-4 col-form-label">Tanggal Pencairan
                                        Giro</label>
                                    <div class="col-8">
                                        <div class="input-group date">
                                            <input type="text" name="hutang_giro_bank_cair" class="form-control m-input"
                                                   id="m_datepicker_5"
                                                   readonly="" value="{{ date('d-m-Y') }}">
                                            <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="produksi-buttons">
                            <button type="submit"
                                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Beli Barang</span>
                        </span>
                            </button>

                            <a href="#"
                               onclick="history.back(-1)"
                               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar</span>
                        </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection