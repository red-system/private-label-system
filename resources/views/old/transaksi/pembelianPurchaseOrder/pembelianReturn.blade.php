@extends('../general/index')

@section('css')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/pembelian_purchase_order.js') }}"></script>
@endsection

@section('body')

    <form action="{{ route('returnPembelianPurchaseOrderInsert') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('pembelianPurchaseOrderPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        <input type="hidden" name="no_pembelian_return" value="{{ $no_pembelian_return }}">
        <input type="hidden" name="no_pembelian_return_label" value="{{ $no_pembelian_return_label }}">
        <input type="hidden" name="id_po" value="{{ $po->id }}">
        <input type="hidden" name="id_supplier" value="{{ $po->id_supplier}}">
        <input type="hidden" name="id_lokasi" value="{{ $po->id_lokasi }}">
        <input type="hidden" name="id_pembelian" value="{{ $pembelian->id }}">

        {{ csrf_field() }}

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">No Return Pembelian</label>
                                <div class="col-7 col-form-label">{{$no_pembelian_return_label}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">No Faktur Pembelian</label>
                                <div class="col-7 col-form-label">{{$pembelian->no_pembelian_label}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">Tanggal Faktur
                                    Pembelian</label>
                                <div class="col-7 col-form-label">{{ Main::format_date_label($pembelian->tanggal_pembelian)}}</div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">No PO</label>
                                <div class="col-7 col-form-label">
                                    {{ $po->no_po_label }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">Nama Supplier</label>
                                <div class="col-7 col-form-label">{{ $po->supplier->supplier }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">Lokasi</label>
                                <div class="col-7 col-form-label">{{ $po->lokasi->lokasi }}</div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label">Tanggal Jatuh Tempo</label>
                                <div class="col-7 col-form-label">{{ Main::format_date_label($pembelian->tanggal_jatuh_tempo) }}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-5 col-form-label required">Keterangan Return
                                    Pembelian</label>
                                <div class="col-7">
                                    <textarea class="form-control m-input" name="keterangan_return"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <h4 class="m-portlet__head-text">
                                Daftar Barang Purchase Order
                            </h4>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="15">No</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Discount (%)</th>
                                <th>Discount (Nominal)</th>
                                <th>Sub Total</th>
                                <th width="150">Qty Return</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pembelian->pembelian_barang as $key=>$row)
                                <input type="hidden" name="id_pembelian_barang[]" value="{{ $row->id }}">
                                <input type="hidden" name="id_po_barang[]" value="{{ $row->id_po_barang }}">
                                <input type="hidden" name="id_barang[]" value="{{ $row->id_barang }}">
                                <input type="hidden" name="id_stok[]" value="{{ $row->id_stok }}">
                                <input type="hidden" name="qty[]" value="{{ $row->po_barang->qty }}">
                                <input type="hidden" name="po_barang_harga[]" value="{{ $row->po_barang->harga }}">
                                <input type="hidden" name="hpp_barang[]" value="{{ $row->hpp_barang }}">
                                <tr>
                                    <td>{{ $key+1 }}.</td>
                                    <td>{{ $row->po_barang->barang->nama_barang }}</td>
                                    <td align="right">{{ Main::format_number($row->po_barang->qty) }}</td>
                                    <td>{{ $row->po_barang->satuan->satuan }}</td>
                                    <td align="right">{{ Main::format_number($row->po_barang->harga) }}</td>
                                    <td align="right">{{ Main::format_number($row->po_barang->discount) }}%</td>
                                    <td align="right">{{ Main::format_number($row->po_barang->discount_nominal) }}</td>
                                    <td align="right">{{ Main::format_number($row->po_barang->sub_total) }}</td>
                                    <td>
                                        <input type="text" name="qty_return[]"
                                               class="form-control m-input input-numeral qty-return" value=""
                                               max="{{ $row->po_barang->qty }}">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-right" colspan="8">Total Qty Return</th>
                                <th class="text-right"><strong class="total-qty-return">0</strong></th>
                            </tr>
                            </tfoot>
                        </table>


                        <div class="produksi-buttons">
                            <button type="submit"
                                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="fa fa-undo"></i>
                            <span>Return Barang</span>
                        </span>
                            </button>

                            <a href="#"
                               onclick="history.back(-1)"
                               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar</span>
                        </span>
                            </a>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </form>


@endsection