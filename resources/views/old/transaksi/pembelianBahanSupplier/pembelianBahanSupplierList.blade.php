@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <form action="{{ route('targetProduksiInsert') }}" method="post" class="form-send">
                        {{ csrf_field() }}
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th width="230">Tanggal Target</th>
                                <th width="180">Qty</th>
                                <th width="200">Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <div class="target-produksi-buttons">
                            <button type="submit"
                                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-check"></i>
                                    <span>Simpan Target Produksi</span>
                                </span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection