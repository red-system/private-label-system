<style type="text/css">
    .table-data {
        border-left: 0.01em solid #ccc;
        border-right: 0;
        border-top: 0.01em solid #ccc;
        border-bottom: 0;
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0.01em solid #ccc;
        border-top: 0;
        border-bottom: 0.01em solid #ccc;
        padding: 2px 4px;
        text-align: center;
        font-size: 12px;
        font-family: Roboto;
    }
    .table-foot {
        border-collapse: collapse;
        width: 100%;
    }

    .table-foot td,
    .table-foot th {
        padding: 2px 4px;
        text-align: center;
        font-size: 12px;
        font-family: Roboto;
    }
</style>
<table width="100%">
    <tbody>
    <tr>
        <td style="font-family: Roboto"><font size="2">PURI PANGAN SEJATI<br>
                {{$lokasi->alamat}} <br>
                Telp/Fax : {{$lokasi->kontak}} <br></font></td>
    </tr>
    </tbody>
</table>
<table width="100%">
    <tr>
        <td style="font-family: Roboto"><font size="2"> SURAT JALAN
                <br>No. Surat Jalan :<span>{{$surat_jalan->no_surat_jalan}}</span>
                <br>Tanggal Order : <span>{{Main::format_date($surat_jalan->tanggal_order)}}</span>
                <br>
            </font></td>
        <td style="font-family: Roboto"><font size="2">
                <span>KIRIM KE :</span> <span>{{$sales_order->pelanggan_so}}</span>
                <br><span></span><span>{{$langganan->langganan_alamat}}</span>
                <br><span></span><span>Kontak : {{$langganan->langganan_kontak}}</span>
                <br>
            </font></td>
    </tr>
</table>
<br/>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%" border="1" class="table-data">
    <thead>
    <tr>
        <td width="3%"><font size="1">No</font></td>
        <td width="10%"><font size="1">Nama Barang</font></td>
        <td width="10%"><font size="1">QTY</font></td>
        <td width="10%"><font size="1">Harga</font></td>
        <td width="10%"><font size="1">Diskon</font></td>
        <td width="10%"><font size="1">Sub Total</font></td>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 0;
        $total_berat = 0;
    @endphp
    @foreach($item_sj as $r)
        @php
            $no = $no +1;
        @endphp
        <tr>
            <td><font size="1">{{ $no }}</font></td>
            <td><font size="1">{{$r->nama_barang }}</font></td>
            <td style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_number_system( $r->qty_kirim_surat_jalan ).' '.$r->satuan_surat_jalan}}</font></td>
            <td style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $r->harga_jual_barang )}}</font></td>
            <td style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $r->diskon )}}</font></td>
            <td style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $r->subtotal_surat_jalan )}}</font></td>
        </tr>
        @php
            $total_berat = $total_berat + $r->qty_kirim_surat_jalan;
        @endphp
    @endforeach
    </tbody>
</table>
<table class="table-data">
    <tr>
        <td width="13%" colspan="2"><font size="1">Total Item : {{$no}}</font></td>
        <td width="40%" style="text-align: left; padding-left: 20px"><font size="1">Total Berat : {{$total_berat }} KG</font></td>
    </tr>
</table>
<table style="margin-top: 8px; margin-bottom: 10px" width="100%">
    <tbody>
    <tr>
        <td></td>
    </tr>
    </tbody>
</table>
<footer>
    <table style="margin-top: 8px; margin-bottom: 10px" width="100%" class="table-foot">
        <tbody>
        <tr><td width="70%"></td>
            <td width="17%" style="text-align: left; padding-left: 20px"><font size="1">Sub Total</font></td>
            <td width="3%" style="text-align: left;"><font size="1">:</font></td>
            <td width="20%" style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $sales_order->total_so )}}</font></td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td width="20%" style="text-align: left; padding-left: 20px"><font size="1">Diskon Persen</font></td>
            <td width="3%" style="text-align: left;"><font size="1">:</font></td>
            <td width="20%" style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $diskon_persen )}}</font></td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td width="20%" style="text-align: left; padding-left: 20px"><font size="1">Diskon Nominal</font></td>
            <td width="3%" style="text-align: left;"><font size="1">:</font></td>
            <td width="20%" style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $sales_order->disc_nominal_so )}}</font></td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td width="20%" style="text-align: left; padding-left: 20px"><font size="1">Pajak</font></td>
            <td width="3%" style="text-align: left;"><font size="1">:</font></td>
            <td width="20%" style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $sales_order->pajak_nominal_so )}}</font></td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td width="20%" style="text-align: left; padding-left: 20px"><font size="1">Ongkos Kirim</font></td>
            <td width="3%" style="text-align: left;"><font size="1">:</font></td>
            <td width="20%" style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $sales_order->ongkos_kirim_so )}}</font></td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td width="20%" style="text-align: left; padding-left: 20px"><font size="1">Grand Total</font></td>
            <td width="3%" style="text-align: left;"><font size="1">:</font></td>
            <td width="20%" style="text-align: right; padding-right: 30px"><font size="1">{{Main::format_money( $sales_order->grand_total_so )}}</font></td>
        </tr>
        </tbody>
    </table>

    <br>
    <table width="100%">
        <tbody>
        <tr>
            <td align="center" width="45%" style="font-family: Roboto"><font size="1">Barang telah diterima dan diperiksa<br>
                    dengan baik <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <hr width="45%">
                    <p>Nama Jelas & Cap Perusahaan</p> <br></font></td>

            <td align="center" width="40%" style="font-family: Roboto"><font size="1"><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <hr width="40%">
                    <p>Administrator</p> <br></font></td>
        </tr>
        </tbody>
    </table>
</footer>