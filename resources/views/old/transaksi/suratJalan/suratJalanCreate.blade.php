@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/input-mask.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/surat_jalan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    {{--    @include('transaksi/suratJalan/modalBarang')--}}
    @include('old.transaksi.suratJalan.modalTimbang')
    <form action="{{ route('suratJalanInsert',['id'=>$so->id]) }}"
          method="post"
          class="surat-jalan-send"
          data-redirect="{{ route('salesOrderPage') }}"
          data-pdf="{{ route('suratJalanPdf',['id'=>$so->id]) }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        {{ csrf_field() }}
        <input type="hidden" name="no_surat_jalan" value="{{$no_sj}}">
        <input type="hidden" name="staff" value="{{$staff}}">
        <input type="hidden" name="id_langganan" class="id_langganan">
        <input type="hidden" name="kode_distributor">
        <input type="hidden" name="total" value="0">
        <input type="hidden" name="grand_total" value="{{$so->grand_total_so}}">
        <input type="hidden" name="terbayar" value="0">
        <input type="hidden" name="sisa_pembayaran" value="0">
        <input type="hidden" name="kembalian" value="0">
        <input type="hidden" name="id_salesman" class="id_salesman">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Surat Jalan</label>
                                <div class="col-9 col-form-label no_so">{{$no_sj}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Order</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_order" class="form-control m-input"
                                               id="m_datepicker_5"
                                               readonly=""
                                               value="{{ Carbon\Carbon::parse($so->tgl_so)->format('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No SO</label>
                                <div class="col-9 col-form-label no_so" name="no_so">{{$so->no_so}}</div>
                                <input type="hidden" name="no_so" value="{{$so->no_so}}">
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Langganan</label>
                                <div class="col-9">
                                    <div class="col-9 col-form-label no_so" name="pelanggan">{{$so->pelanggan_so}}</div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input" name="alamat"
                                                  readonly>{{$pelanggan->langganan_alamat}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Kontak </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" class="form-control m-input" name="kontak"
                                               value="{{$pelanggan->langganan_kontak}}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Staff </label>
                                <div class="col-9">
                                    <div class="col-9 col-form-label no_so"><b>{{$staff}}</b></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Salesman</label>
                                <div class="col-9">
                                    <div class="col-9 col-form-label no_so"><b>{{$salesman}}</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable ">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Lokasi</th>
                                <th>Stok</th>
                                <th>Harga Satuan</th>
                                <th>Disc (%)</th>
                                <th>Disc (Nominal)</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Qty Kirim</th>
                                <th>Kali</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item_so as $r)
                                <tr data-index="{{ $no++ }}"
                                    data-id-penjualan-produk="{{ $r->id }}">
                                    <td class="m--hide data">
                                        <input type="hidden" name="id_barang[{{ $r->id }}]" class="id_barang"
                                               value="{{ $r->id_barang }}">
                                        <input type="hidden" name="harga[{{ $r->id }}]" class="harga"
                                               value="{{ $r->harga_jual_barang }}">
                                        <input type="hidden" name="id_stok[{{$r->id}}]" class="td-stok"
                                               value="{{$r->id_stok}}">
                                        <input type="hidden" name="harga_net[{{ $r->id }}]" class="harga_net"
                                               value="{{ $r->harga_net }}">
                                        <input type="hidden" name="sub_total[{{ $r->id }}]" class="sub_total"
                                               value="{{ $r->subtotal }}">
                                        <input type="hidden" name="satuan[{{ $r->id }}]" value="{{ $r->satuan_so }}">
                                        <input type="hidden" name="qty[{{ $r->id }}]" value="{{ $r->jml_barang_so }}">
                                        <input type="hidden" name="qty_kirim[{{ $r->id }}]" class="td-qty-kirim">
                                        <input type="hidden" name="qty_kali[{{ $r->id }}]" class="td-qty-kali">
                                        <input type="hidden" name="diskon[{{ $r->id }}]" value="{{$r->diskon}}">
                                    </td>
                                    <td>
                                        {{$r->barang->kode_barang}}
                                    </td>
                                    <td class="produk-nama">
                                        {{$r->nama_barang}}
                                    </td>
                                    <td>
                                        {{$r->lokasi}}
                                    </td>
                                    <td class="td-stok">
                                        {{Main::format_number($r->jml_barang)}}
                                    </td>
                                    <td class="td-harga" style="width: 50px">
                                        {{Main::format_number($r->harga_so)}}
                                    </td>
                                    <td class="td-ppn">
                                        {{$r->disc_persen_so}}%
                                    </td>
                                    <td class="td-potongan">
                                        {{$r->disc_nominal_so}}
                                    </td>
                                    <td class="td-qty">
                                        {{$r->jml_barang_so}}
                                    </td>
                                    <td class="td-satuan">
                                        {{$r->satuan_so}}
                                    </td>
                                    <td class="td-qty-kirim" id="kirim-{{$r->id}}">
                                        0
                                    </td>
                                    <td class="td-qty-kali" id="kali-{{$r->id}}">
                                        0
                                    </td>
                                    <td class="td-sub-total">
                                        {{Main::format_number($r->subtotal)}}
                                    </td>
                                    <td>
                                        <span>
                                        <button type="button"
                                                class="btn btn-accent m-btn m-btn--pill btn-success btn-sm btn-timbang-lama"
                                                data-no="{{$r->id}}">
                                            <i class="la la-balance-scale"></i>
                                        </button>
                                        <button type="button"
                                                class="btn-reset-timbang btn m-btn--pill btn-warning btn-sm btn-timbang-reset-all"
                                                data-no="{{$r->id}}"
                                                data-confirm="false">
                                            <i class="la la-refresh"></i>
                                        </button>
                                            </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile hidden">

                    <div class="m-portlet__body">

                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Tanggal</th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>No Seri Produk</th>
                                <th>Qty In</th>
                                <th>Qty Out</th>
                                <th>Gudang</th>
                                <th>Penerima/Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-8">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total</label>

                                <div class="col-8 col-form-label">
                                    <strong><span class="total">{{$so->total_so}}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc (%)</label>
                                <div class="col-8 col-form-label">
                                    <span class="total">{{$so->disc_persen_so}}%</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc Tambahan
                                    (Nominal)</label>
                                <div class="col-8 col-form-label">
                                    <span class="total">{{$so->disc_nominal_so}}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak</label>
                                <div class="col-8 col-form-label">
                                    <span class="total">{{$so->pajak_persen_so}}%</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Ongkos Kirim</label>
                                <div class="col-8 col-form-label">
                                    <span class="total">{{$so->ongkos_kirim_so}}</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="grand-total">{{$so->grand_total_so}}</span></strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="target-produksi-buttons">
                                        <span>
                                        <button type="submit"
                                                class="btn-simpan btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                            <span>
                                                <i class="la la-check"></i>Simpan Data</span></button>
                                            </span>
                                <a href="#" onclick="history.back(-1)"
                                   class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-angle-double-left"></i>
                                            <span>Kembali</span>
                                        </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </form>


@endsection