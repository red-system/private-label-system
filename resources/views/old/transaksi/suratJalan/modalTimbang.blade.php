<div class="modal" id="modal-timbang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Timbang</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body row">
                    <div class="col-lg-8">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Qty </label>
                            <label class="col-lg-4 col-form-label qty">-</label>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Qty Kirim </label>
                            <label class="col-lg-4 col-form-label hitung-qty"></label>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Kali </label>
                            <label class="col-lg-4 col-form-label kali-qty"></label>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="form-group">
                            <input type="text" class="form-control m-input" name="timbang"
                                   style="height:200px; font-size:100px" value="">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning btn-timbang-reset">Reset</button>
                            <span></span>
                            <button type="submit" class="btn btn-success btn-timbang-tambah">Tambah</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-timbang-selesai timbang-selesai">Selesai</button>
            </div>
        </div>
    </div>
</div>