<table class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>No Seri Bahan</th>
        <th>Lokasi</th>
        <th>Stok</th>
    </tr>
    </thead>
    <tbody>
    @php($qty = 0)
    @foreach($bahanStok as $r)
        @php(
            $qty += $r->qty
        )
        <tr>
            <td>{{ $no++ }}.</td>
            <td>{{ $r->no_seri_produk }}</td>
            <td>{{ $r->lokasi->lokasi }}</td>
            <td>{{ Main::format_number($r->qty) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3">Total</td>
        <td>{{ Main::format_number($qty) }}</td>
    </tr>
    </tfoot>
</table>