@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/po_bahan_supplier.js') }}"
            type="text/javascript"></script>

@endsection

@section('body')
    @include('old.transaksi.poBahanSupplier.trPembayaran')

    <form
            action="{{ route('poBahanSupplierPembelianInsert', ['id'=>Main::encrypt($id_po_bahan)]) }}"
            class="form-send"
            method="post"
            style="width: 100%"
            data-redirect="{{ route('poBahanSupplierPage') }}"
            data-alert-show="true"
            data-alert-field-message="true">

        {{ csrf_field() }}
        @include('old.transaksi.poBahanSupplier.modalPembayaran')

        <input type="hidden" name="urutan" value="{{ $urutan }}">
        <input type="hidden" name="no_pembelian" value="{{ $no_pembelian }}">
        <input type="hidden" name="total" value="{{ $po_bahan->total }}">
        <input type="hidden" name="grand_total" value="{{ $po_bahan->grand_total }}">
        <input type="hidden" name="terbayar" value="0">
        <input type="hidden" name="sisa_pembayaran" value="0">
        <input type="hidden" name="kembalian" value="0">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">No Pembelian</label>
                                <div class="col-10 col-form-label">
                                    {{ $no_pembelian }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">No Pre Order</label>
                                <div class="col-10 col-form-label">
                                    {{ $po_bahan->no_faktur }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Tanggal</label>
                                <div class="col-10">
                                    <div class="input-group date">
                                        <input type="text"
                                               name="tanggal"
                                               class="form-control m-input m_datepicker"
                                               readonly=""
                                               value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
                                <div class="col-9">
                                    <div class="input-group col-form-label">
                                        {{ '('.$po_bahan->supplier->kode_supplier.') '.$po_bahan->supplier->nama_supplier }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-9">
                                    <div class="input-group col-form-label telp_supplier">{{ $po_bahan->supplier->telp_supplier }}</div>
                                </div>
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <div class="col-9">
                                    <div class="input-group col-form-label alamat_supplier">{{ $po_bahan->supplier->alamat_supplier }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>
                                <th>Harga</th>
                                <th>PPN (%)</th>
                                <th>Harga Net</th>
                                <th>Qty Order</th>
                                <th>Gudang</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($po_bahan_detail as $r)
                                <tr>
                                    <td class="m--hide">
                                        <input type="hidden" name="id_po_bahan_detail[]" value="{{ $r->id }}">
                                        <input type="hidden" name="id_bahan[]" value="{{ $r->id_bahan }}">
                                        <input type="hidden" name="qty[]" value="{{ $r->qty }}">
                                    </td>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $r->bahan->kode_bahan }}</td>
                                    <td>{{ $r->bahan->nama_bahan }}</td>
                                    <td>{{ Main::format_number($r->harga) }}</td>
                                    <td>{{ Main::format_number($r->ppn_nominal) }}</td>
                                    <td>{{ Main::format_number($r->harga_net) }}</td>
                                    <td>{{ Main::format_number($r->qty) }}</td>
                                    <td>
                                        <select name="id_lokasi[]"
                                                class="form-control m-select2"
                                                style="width: 150px;">
                                            <option value="">Pilih Gudang</option>
                                            @foreach($gudang as $r_gudang)
                                                <option value="{{ $r_gudang->id }}">{{ $r_gudang->lokasi }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>{{ Main::format_number($r->sub_total) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-4 offset-lg-8">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label">{{ Main::format_number($po_bahan->total) }}</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8 col-form-label">{{ Main::format_number($po_bahan->biaya_tambahan) }}</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8 col-form-label">{{ Main::format_number($po_bahan->potongan) }}</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label">{{ Main::format_number($po_bahan->grand_total) }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="button"
                    class="btn-selanjutnya btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                    data-toggle="modal"
                    data-target="#modal-pembayaran"
                    data-backdrop="static"
                    data-keyboard="false">
                <span>
                    <i class="la la-arrow-right"></i>
                    <span>Proses Selanjutnya</span>
                </span>
            </button>
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan Pembelian</span>
                </span>
            </button>
            <a href="{{ route("poBahanSupplierPage") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>
@endsection