<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">

<div id="invoiceholder">

    <div id="headerimage"></div>
    <div id="invoice" class="effect2">
        <div id="invoice-top">
            <div class="logo">
                <img src="{{ base_path() }}/public/images/logo.png" width="80">
            </div>
            <div class="info">
                <br/>
                <h2>{{ $company->companyName }}</h2>
                <p>{{ $company->companyAddress }}</p>
                <p>{{ $company->companyTelp }}</p>
            </div><!--End Info-->
            <div class="title">
                <br/>
                <table width="100%" border="0">
                    <tr>
                        <td><h1>PURCHASE ORDER</h1></td>
                    </tr>
                </table>
            </div><!--End Title-->
        </div><!--End InvoiceTop-->

        <div style="clear: both"></div>
        <br/><br/><br/><br/><br/>

        <div id="invoice-mid">
            <div class="info">
                <table width="100%">
                    <tr>
                        <td>Order To</td>
                        <td>
                            {{ $po_bahan->supplier->nama_supplier }}.<br/>
                            {{ $po_bahan->supplier->alamat_supplier }}
                        </td>
                    </tr>
                    <tr>
                        <td>Order No</td>
                        <td>{{ $po_bahan->no_faktur }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Dokumen</td>
                        <td>{{ Main::format_date($po_bahan->tanggal) }}</td>
                    </tr>
                    <tr>
                        <td>Pembayaran</td>
                        <td>{{ ucwords($po_bahan->pembayaran) }}</td>
                    </tr>
                    <tr>
                        <td>Pengiriman</td>
                        <td>{{ ucwords($po_bahan->pengiriman) }}</td>
                    </tr>
                </table>
            </div>

            <div class="project">
                <h3>Pengiriman ke:</h3>
                {{ $company->companyName }}<br/>
                up: {{ $company->companyTuju }}<br/>
                {{ $company->companyAddress }}<br/>
                HP: {{ $company->companyPhone }}<br/>
            </div>

            <div style="clear: both;"></div>

        </div><!--End Invoice Mid-->

        <div id="invoice-bot">

            <div id="table">
                <table>
                    <tr class="tabletitle">
                        <td class="no"><h2>No</h2></td>
                        <td class="item"><h2>Keterangan</h2></td>
                        <td class="item"><h2>Satuan</h2></td>
                        <td class="item"><h2>Harga</h2></td>
                        <td class="item"><h2>PPN</h2></td>
                        <td class="item"><h2>PPN Nominal</h2></td>
                        <td class="item"><h2>Harga Net</h2></td>
                        <td class="Hours"><h2>Qty</h2></td>
                        <td class="Hours"><h2>Total</h2></td>
                    </tr>
                    @foreach($po_bahan_detail as $r)
                        <tr class="service">
                            <td class="tableitem"><p class="itemtext">{{ $no++ }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->bahan->nama_bahan }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ $r->bahan->satuan->satuan }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->harga) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ ($r->ppn_persen*100).'% ' }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->ppn_nominal) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->harga_net) }}</p></td>
                            <td class="tableitem"><p class="itemtext">{{ Main::format_number($r->qty) }}</p></td>
                            {{--<td class="tableitem"><p class="itemtext">{{ Main::format_money($r->ppn_nominal) }}</p></td>--}}
                            <td class="tableitem"><p class="itemtext">{{ Main::format_money($r->sub_total) }}</p></td>
                        </tr>
                    @endforeach
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Total</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($po_bahan->total) }}</h2></td>
                    </tr>
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Biaya Tambahan</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($po_bahan->biaya_tambahan) }}</h2></td>
                    </tr>
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Potongan</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($po_bahan->potongan) }}</h2></td>
                    </tr>
                    <tr class="tabletitle">
                        <td class="Rate" colspan="8" align="right"><h2>Grand Total</h2></td>
                        <td class="payment"><h2>{{ Main::format_money($po_bahan->grand_total) }}</h2></td>
                    </tr>

                </table>
                <p>
                    <span style="color: red">*</span> price are tax included
                </p>
            </div>
            <br/><Br/>


            <div id="legalcopy">
                <h4><strong>Dipesan oleh.</strong></h4>
            </div>

            <div id="ttd">
                <h3><strong>Checked by.</strong></h3>
            </div>

            <div style="clear: both"></div>
            <br/><br/><br/><br/>

        </div><!--End InvoiceBot-->
    </div><!--End Invoice-->
</div><!-- End Invoice Holder-->