<div class="modal" id="modal-pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xlg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <h3 class="modal-title m--font-light" id="exampleModalLabel">PEMBAYARAN</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">No Faktur</label>
                            <div class="col-10">
                                <input class="form-control m-input" type="text" name="no_faktur" readonly
                                       value="PL-001-002-003">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">Total</label>
                            <div class="col-10">
                                <input class="form-control m-input" type="text" name="total" readonly value="1.000.000">
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_3">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Pembayaran</th>
                        <th>Jumlah</th>
                        <th>Jatuh Tempo</th>
                        <th>No. Check / BG</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            <select class="form-control" name="pembayaran">
                                <option value="pembayaran_tunai">Pembayaran Tunai</option>
                            </select>
                        </td>
                        <td>
                            1.000.000
                        </td>
                        <td>
                            <div class="input-group date">
                                <input type="text" class="form-control m-input" readonly="" value="05/20/2017"
                                       id="m_datepicker_3">
                                <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <input class="form-control m-input" type="text" name="no_bg">
                        </td>
                        <td>
                            <textarea class="form-control m-input" name="no_faktur"></textarea>
                        </td>
                        <td>
                            <button type="button" class="btn-hapus-produk btn m-btn--pill btn-danger btn-sm"
                                    data-confirm="false">
                                <i class="la la-remove"></i> Hapus
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                <hr/>
                <div class="form-no-padding">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            <h3>Terbayar</h3>
                        </label>
                        <div class="col-10">
                            <h3>100.000</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            <h3>Sisa</h3>
                        </label>
                        <div class="col-10">
                            <h3>560.000</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            <h3>Kembalian</h3>
                        </label>
                        <div class="col-10">
                            <h3>0</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>