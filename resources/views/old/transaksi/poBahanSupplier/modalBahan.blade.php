<div class="modal" id="modal-bahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-accent">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Bahan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_4">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Bahan</th>
                        <th>Nama Bahan</th>
                        <th>Total Stok</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($no = 1)
                    @foreach($bahan as $r)
                        <tr data-id="{{ $r->id }}"
                            data-kode-bahan="{{ $r->kode_bahan }}"
                            data-nama-bahan="{{ $r->nama_bahan }}"
                            data-harga="{{ $r->harga }}">
                            <td width="10">{{ $no++ }}.</td>
                            <td>{{ $r->kode_bahan }}</td>
                            <td>{{ $r->nama_bahan }}</td>
                            <td align="right">{{ Main::format_number($r->stok_bahan) }}</td>
                            <td width="100">
                                <div class="btn-group m-btn-group m-btn-group--pill">
                                    <button type="button"
                                            class="btn-bahan-stok btn btn-info btn-sm">
                                        <i class="la la-check"></i> Stok
                                    </button>
                                    <button type="button"
                                            class="btn-bahan-select btn btn-success btn-sm">
                                        <i class="la la-check"></i> Pilih
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>