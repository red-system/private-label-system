<table class="row-pembayaran m--hide">
    <tbody>
    <tr>
        <td class="td-master-id">
            <select class="form-control select2-pembayaran master_id" name="master_id[]" style="width: 250px">
                <option value="">Pilih Kode Perkiraan</option>
                @foreach($master as $r)
                    <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                @endforeach
            </select>
        </td>
        <td class="td-jumlah">
            <input type="text" name="jumlah[]" class="form-control touchspin-pembayaran jumlah" value="0"
                   style="width: 80px">
        </td>
        <td class="td-jatuh-tempo">
            <div class="input-group date" style="width: 150px">
                <input type="text" class="form-control m-input datepicker-pembayaran jatuh_tempo" name="jatuh_tempo[]"
                       readonly=""
                       value="{{ date('d-m-Y') }}">
                <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                </div>
            </div>
        </td>
        <td class="td-no-bg">
            <input class="form-control m-input no_bg" type="text" name="no_bg[]" style="width: 120px">
        </td>
        <td class="td-keterangan">
            <textarea class="form-control m-input keterangan" name="keterangan[]" style="width: 120px"></textarea>
        </td>
        <td>
            <button type="button" class="btn-delete-row-pembayaran btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
    </tbody>
</table>