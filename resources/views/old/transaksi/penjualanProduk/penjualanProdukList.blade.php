@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('penjualanProdukCreate') }}"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Membuat Penjualan Produk</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Distributor</th>
                            <th>No Faktur</th>
                            <th>Tanggal</th>
                            <th>Jenis Transaksi</th>
                            <th>Pengiriman</th>
                            <th>Grand Total</th>
                            <th width="100">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total = 0)
                        @foreach($list as $r)
                            @php($total += $r->grand_total)
                            <tr data-id-penjualan="{{ $r->id }}">
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ '('.$r->distributor->kode_distributor.') '.$r->distributor->nama_distributor }}</td>
                                <td>{{ $r->no_faktur }}</td>
                                <td>{{ Main::format_date($r->tanggal) }}</td>
                                <td>{{ ucwords($r->jenis_transaksi) }}</td>
                                <td>{!! Main::distributor_pengiriman($r->pengiriman) !!}</td>
                                <td align="right">{{ Main::format_money($r->grand_total) }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-accent btn-sm  dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i> Menu
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item akses-cetak_faktur"
                                               href="{{ route('penjualanProdukFaktur', ['id'=>Main::encrypt($r->id)]) }}">
                                                <i class="la la-print"></i> Cetak Faktur
                                            </a>
                                            <a href="{{ route('penjualanProdukEdit', ['id'=>Main::encrypt($r->id)]) }}"
                                               class="dropdown-item akses-edit">
                                                <i class="la la-edit"></i> Edit
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item btn-hapus m--font-danger"
                                               data-route="{{ route('penjualanProdukDelete', ['id'=>Main::encrypt($r->id)]) }}"
                                               data-message="Yakin Hapus data ini ? <br />
                                                             Penghapusan data Penjualan Produk, akan menghapus juga data yang berkaitan.<br />
                                                             Seperti: Pembayaran Penjualan Produk, Piutang Pelanggan dan Jurnal Umum"
                                               href="#">
                                                <i class="la la-remove m--font-danger"></i> Hapus
                                            </a>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6" class="font-weight-bold text-center">
                                TOTAL
                            </th>
                            <th class="font-weight-bold text-right">
                                {{ Main::format_money($total) }}
                            </th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection