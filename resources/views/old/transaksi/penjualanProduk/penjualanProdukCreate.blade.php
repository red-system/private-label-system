@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('old.transaksi.penjualanProduk.modalDistributor')
    @include('old.transaksi.penjualanProduk.modalProduk')
    @include('old.transaksi.penjualanProduk.modalProdukStok')

    @include('old.transaksi.penjualanProduk.trProduk')
    @include('old.transaksi.penjualanProduk.trPembayaran')

    <form action="{{ route('penjualanProdukInsert') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('penjualanProdukPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        @include('old.transaksi.penjualanProduk.modalPembayaranCreate')

        {{ csrf_field() }}
        <input type="hidden" name="urutan" value="">
        <input type="hidden" name="id_distributor">
        <input type="hidden" name="kode_distributor">
        <input type="hidden" name="total" value="0">
        <input type="hidden" name="grand_total" value="0">
        <input type="hidden" name="terbayar" value="0">
        <input type="hidden" name="sisa_pembayaran" value="0">
        <input type="hidden" name="kembalian" value="0">
        <input type="hidden" name="no_faktur">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal" class="form-control m-input m_datepicker"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Distributor</label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="nama_distributor"
                                               placeholder="Nama Distributor..." disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-distributor">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <div class="col-9 col-form-label alamat-distributor">
                                    -
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Telepon</label>
                                <div class="col-9 col-form-label telepon-distributor">
                                    -
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
                                <div class="col-9 col-form-label no_faktur">-</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Transaksi</label>
                                <div class="col-4">
                                    <select class="form-control" name="jenis_transaksi">
                                        <option value="distributor" selected>Distributor</option>
                                        <option value="retail">Retail</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pengiriman</label>
                                <div class="col-3">
                                    <div class="m-radio-inline">
                                        <label class="m-radio">
                                            <input type="radio" name="pengiriman" value="yes" checked> Ya
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="pengiriman" value="no"> Tidak
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <label for="example-text-input"
                                       class="col-2 col-form-label wrapper-ekspedisi">Ekspedisi</label>
                                <div class="col-4 wrapper-ekspedisi">
                                    <select class="form-control m-select2 " name="id_ekspedisi">
                                        @foreach($ekspedisi as $r)
                                            <option value="{{ $r->id }}">{{ $r->nama_ekspedisi }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-produk btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Produk
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Gudang</th>
                                <th>No Seri Produk</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Potongan</th>
                                <th>PPN({{ $ppnPersen*100 }}%)</th>
                                <th>Harga Net</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label">
                                            <strong><span class="total">0</span></strong>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number" type="text"
                                                   name="biaya_tambahan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input touchspin-number" type="text"
                                                   name="potongan_akhir" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label">
                                            <strong><span class="grand-total">0</span></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div class="target-produksi-buttons">
            <button type="button"
                    class="btn-selanjutnya btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                    data-toggle="modal"
                    data-target="#modal-pembayaran"
                    data-backdrop="static"
                    data-keyboard="false">
                <span>
                    <i class="la la-arrow-right"></i>
                    <span>Proses Selanjutnya</span>
                </span>
            </button>
            <button type="submit"
                    class="btn-simpan btn btn-success btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-check"></i>
                    <span>Simpan Penjualan</span>
                </span>
            </button>
            <a href="{{ route("penjualanProdukPage") }}"
               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                <span>
                    <i class="la la-angle-double-left"></i>
                    <span>Kembali</span>
                </span>
            </a>
        </div>
    </form>


@endsection