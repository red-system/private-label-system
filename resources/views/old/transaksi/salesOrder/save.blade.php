<div tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Data</h5>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Nomor Order</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8">{{$so->no_so}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Nama Pelanggan</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="nama_package_edit">{{$so->pelanggan_so}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Tanggal Order</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit">{{\Carbon\Carbon::parse($so->tgl_so)->format('d-m-Y')}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Tanggal Kirim</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit">{{\Carbon\Carbon::parse($so->tgl_kirim_so)->format('d-m-Y')}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Alamat Pengiriman</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit">{{$so->alamat_pengiriman}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Penerima</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit">{{$so->penerima}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Keterangan</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit">{{$so->keterangan_so}}</label>
                </div>
                @php
                    $no = 1;
                @endphp
                @foreach($item as $r)
                    @php
                        $barang = \app\Models\Widi\mBarang
                                            ::where('id', $r->id_barang)->first();
                        $stok = \app\Models\Widi\mStok
                                            ::where('id', $r->id_stok)->with('lokasi')
                                            ->first();

                    @endphp
                    <div class="form-group m-form__group row">
                        <label class="form-control-label col-3">Barang {{$no++}}</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8" id="harga_package_edit">{{$barang->nama_barang}}</label>
                    </div>
                    <div>
                        <label class="form-control-label col-3">Lokasi</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8"
                               id="harga_package_edit">{{$stok->lokasi->lokasi}}</label>
                    </div>
                    <div>
                        <label class="form-control-label col-3">Jumlah Order</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8"
                               id="harga_package_edit">{{$r->jml_barang_so." ".$r->satuan_so}}</label>
                    </div>
                    <div>
                        <label class="form-control-label col-3">Harga Net</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8" id="harga_package_edit">{{$r->harga_net}}</label>
                    </div>
                    <div>
                        <label class="form-control-label col-3">Sub Total</label>
                        <label class="form-control-label col-1">:</label>
                        <label class="form-control-label col-8" id="harga_package_edit">{{$r->subtotal}}</label>
                    </div>

                @endforeach
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Grand Total</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit">{{$so->grand_total}}</label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Metode Pembayaran</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit">{{$so->metode_pembayaran_dp_so}}</label>
                </div>
            </div>
            <div>
                {{--                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>--}}
            </div>
        </div>
    </div>
</div>
