@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    {{--    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('js/sales_order.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('old.transaksi.salesOrder.modalBarang')
    @include('old.transaksi.salesOrder.modalStokBarang')

    @include('old.transaksi.salesOrder.trProduk')
    {{--    @include('transaksi/penjualanProduk/trPembayaran')--}}

    <form action="{{ route('salesOrderUpdate', ['id'=>$so->id]) }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('salesOrderPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">

        {{ csrf_field() }}
        <input type="hidden" name="no_so" value="{{$so->no_so}}">
        <input type="hidden" name="nama_langganan" value="{{$so->pelanggan_so}}">
        <input type="hidden" name="total" value="{{$so->total_so}}">
        <input type="hidden" name="grand_total" value="{{$so->grand_total_so}}">
        {{--        <input type="hidden" name="bayar" value="{{$so->dp_so}}">--}}

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Order</label>
                                <div class="col-9 col-form-label no_so">{{$so->no_so}}</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal" class="form-control m-input"
                                               id="m_datepicker_5"
                                               readonly value="{{\Carbon\Carbon::parse($so->tgl_so)->format('d-m-Y')}}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input"
                                                  name="keterangan">{{$so->keterangan_so}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi Order </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <select class="form-control m-select2" name="id_lokasi_penjualan">
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($lokasi as $r)
                                                <option value="{{$r->id}}" {{$so->id_lokasi == $r->id ? 'selected':''}}>{{$r->lokasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Langganan</label>
                                <div class="col-9 col-form-label alamat-langganan">
                                    <b>{{$so->pelanggan_so}}</b>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <div class="col-9 col-form-label alamat-langganan">
                                    {{$pelanggan->langganan_alamat}}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Telepon</label>
                                <div class="col-9 col-form-label telepon-langganan">
                                    {{$pelanggan->langganan_kontak}}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Telepon</label>
                                <div class="col-9 col-form-label telepon-langganan">
                                    <b>{{$salesman}}</b>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat Pengiriman </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input"
                                                  name="alamat_pengiriman">{{$so->alamat_pengiriman}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Penerima </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" class="form-control m-input" name="penerima"
                                               value="{{$so->penerima}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Kirim</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_kirim" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               value="{{\Carbon\Carbon::parse($so->tgl_kirim_so)->format('d-m-Y')}}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-produk btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Produk
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Lokasi</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Disc (%)</th>
                                <th>Disc (Nominal)</th>
                                <th>Harga Net</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item_so as $r)
                                <tr data-index="{{ $no++ }}"
                                    data-id-penjualan-produk="{{ $r->id }}">
                                    <td class="m--hide data">
                                        <input type="hidden" name="id_barang[{{ $r->id }}]" class="id_barang"
                                               value="{{ $r->id_barang }}">
                                        <input type="hidden" name="id_stok[{{$r->id}}]" class="id_stok"
                                               value="{{$r->id_stok}}">
                                        <input type="hidden" name="harga[{{ $r->id }}]" class="harga"
                                               value="{{ $r->harga_jual_barang }}">
                                        <input type="hidden" name="harga_net[{{ $r->id }}]" value="{{ $r->harga_net }}">
                                        <input type="hidden" name="sub_total[{{ $r->id }}]" class="sub_total"
                                               value="{{ $r->subtotal }}">
                                        <input type="hidden" name="satuan[{{ $r->id }}]" value="{{$r->satuan_so}}">
                                        <input type="hidden" name="id_satuan[{{ $r->id }}]" value="{{$r->id_satuan}}">
                                    </td>
                                    <td>
                                        <button class="btn-search-produk btn-sm btn btn-accent m-btn--pill"
                                                type="button">
                                            <i class="la la-search"></i> Cari
                                        </button>
                                    </td>
                                    <td class="produk-nama">
                                        {{ $r->kode_barang.' '.$r->nama_barang}}
                                    </td>
                                    <td>
                                        <select class="form-control m-select2 id_lokasi_lama"
                                                name="id_lokasi[{{ $r->id }}]" style="width: 140px">
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($r->gudang as $r_gudang)
                                                <option data-satuan="{{$r_gudang->satuan}}"
                                                        data-id-satuan="{{$r_gudang->id_satuan}}"
                                                        data-id="{{$r_gudang->id}}"
                                                        data-stok="{{$r_gudang->jml_barang}}"
                                                        value="{{ $r_gudang->id_lokasi }}" {{ $r_gudang->id_lokasi == $r->id_lokasi ? 'selected':'' }}>{{ $r_gudang->lokasi->lokasi }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="td-stok">
                                        {{Main::format_number($r->jml_barang)}}
                                    </td>
                                    <td class="td-harga">
                                        {{Main::format_number($r->harga_so)}}
                                    </td>
                                    <td class="td-qty">
                                        <input type="text" name="qty[{{$r->id}}]"
                                               class="form-control m-input input-numeral qty"
                                               value="{{$r->jml_barang_so}}" style="width: 50px">
                                    </td>
                                    <td class="td-satuan">
                                        {{$r->satuan_so}}
                                    </td>
                                    <td class="td-ppn">
                                        <input type="text" name="diskon_persen[{{$r->id}}]"
                                               class="form-control m-input input-numeral diskon_persen"
                                               value="{{$r->disc_persen_so}}" style="width: 50px">
                                    </td>
                                    <td class="td-potongan">
                                        <input type="text" name="diskon_nominal[{{$r->id}}]"
                                               class="form-control m-input input-numeral diskon_nominal"
                                               value="{{$r->disc_nominal_so}}" style="width: 100px">
                                    </td>
                                    <td class="td-harga-net">
                                        {{Main::format_number($r->harga_net)}}
                                    </td>
                                    <td class="td-sub-total">
                                        {{Main::format_number($r->subtotal)}}
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn-delete-row-produk btn m-btn--pill btn-danger btn-sm"
                                                data-confirm="false">
                                            <i class="la la-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="total">{{$so->total_so}}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc (%)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="diskon_persen_akhir" value="{{$so->disc_persen_so}}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc Tambahan
                                    (Nominal)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="potongan_akhir" value="{{$so->disc_nominal_so}}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="pajak" value="{{$so->pajak_persen_so}}">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Ongkos Kirim</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="ongkos_kirim" value="{{$so->ongkos_kirim_so}}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="grand-total">{{$so->grand_total_so}}</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Bayar</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="bayar" value="{{$so->dp_so}}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Metode Pembayaran </label>
                                <div class="col-8">
                                    <select class="form-control m-select2" name="metode_pembayaran">
                                        <option value="">Pilih Metode</option>
                                        <option value="cash" {{$so->metode_pembayaran_dp_so == 'cash' ? 'selected':''}} >
                                            Cash
                                        </option>
                                        <option value="piutang" {{$so->metode_pembayaran_dp_so == 'piutang' ? 'selected':''}}>
                                            Piutang
                                        </option>
                                        <option value="bank" {{$so->metode_pembayaran_dp_so == 'bank' ? 'selected':''}}>
                                            Bank
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="target-produksi-buttons">
                                            <span>
                                            <button type="submit"
                                                    class="btn-simpan btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                                <span>
                                                    <i class="la la-check"></i>Simpan Order</span></button>
                                                </span>
                            <a href="#" onclick="history.back(-1)"
                               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                            <span>
                                                <i class="la la-angle-double-left"></i>
                                                <span>Kembali</span>
                                            </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </form>


@endsection