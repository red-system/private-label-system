<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nomor Order</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8">{{$so->no_so}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Pelanggan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$so->pelanggan_so}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Nama Salesman</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$salesman}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Order</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($so->tgl_so)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Tanggal Kirim</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($so->tgl_kirim_so)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Alamat Pengiriman</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$so->alamat_pengiriman}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Penerima</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$so->penerima}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Keterangan</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$so->keterangan_so}}</label>
</div>
@php
    $no = 1;
@endphp
@foreach($item as $r)

    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px"><b>Barang {{$no++}}</b></label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->nama_barang}}</label>

    </div>

    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Lokasi</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->lokasi}}</label>
    </div>
    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Jumlah Order</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->jml_barang_so." ".$r->satuan_so}}</label>
    </div>
    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Harga Net</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->harga_net}}</label>
    </div>
    <div class="form-group m-form__group row">
        <label class="form-control-label col-3" style="text-indent: 60px">Sub Total</label>
        <label class="form-control-label col-1">:</label>
        <label class="form-control-label col-8" id="harga_package_edit">{{$r->subtotal}}</label>
    </div>

@endforeach
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Grand Total</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$so->grand_total_so}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Metode Pembayaran</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$so->metode_pembayaran_dp_so}}</label>
</div>