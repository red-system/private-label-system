<div class="modal" id="modal-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" style="overflow-y: auto">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-accent">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="width: 100%">
                <table class="table table-striped- table-bordered table-hover table-checkable datatable-general">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Total Stok</th>
                        <th width="50">Aksi</th>
                    </tr>
                    </thead>
                    <tbody id="body-barang">
                    @php($no = 1)
                    @foreach($barang as $r)
                        <tr data-id="{{ $r->id }}"
                            data-kode-barang="{{ $r->kode_barang }}"
                            data-nama-barang="{{ $r->nama_barang }}">
                            <td width="10">{{ $no++ }}.</td>
                            <td>{{ $r->kode_barang }}</td>
                            <td>{{ $r->nama_barang }}</td>
                            <td align="right">{{ Main::format_number($r->total_stok_barang) }}</td>
                            <td>
                                <div class="btn-group m-btn-group m-btn-group--pill">
                                    <button type="button"
                                            class="btn-produk-stok btn btn-info btn-sm">
                                        <i class="la la-briefcase"></i> Stok
                                    </button>
                                    <button type="button"
                                            class="btn-select-produk btn btn-success btn-sm">
                                        <i class="la la-check"></i> Pilih
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>