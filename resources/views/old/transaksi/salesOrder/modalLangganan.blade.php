<div class="modal" id="modal-langganan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-success">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Langganan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable datatable-general">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Langganan</th>
                        <th>Nama Langganan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($no = 1)
                    @foreach($langganan as $r)
                        <tr>
                            <td width="10">{{ $no++ }}.</td>
                            <td>{{ $r->kode_langganan }}</td>
                            <td>{{ $r->langganan }}</td>
                            <td width="30">
                                <button type="button"
                                        class="select-distributor btn btn-success btn-sm m-btn--pill"
                                        data-id="{{ $r->id }}"
                                        data-kode-langganan="{{ $r->kode_langganan }}"
                                        data-nama-langganan="{{ $r->langganan }}"
                                        data-telepon-langganan="{{ $r->langganan_kontak }}"
                                        data-alamat-langganan="{{ $r->langganan_alamat }}">
                                    <i class="la la-check"></i> Pilih
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>