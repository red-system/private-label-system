@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/sales_order.js') }}" type="text/javascript"></script>
@endsection

@section('body')
    @include('old.transaksi.penjualan.modalSalesman')
    @include('old.transaksi.salesOrder.modalBarang')
    @include('old.transaksi.salesOrder.modalStokBarang')
    @include('old.transaksi.salesOrder.modalLangganan')


    @include('old.transaksi.salesOrder.trProduk')

    <form action="{{ route('salesOrderInsert') }}"
          method="post"
          class="form-send"
          data-redirect="{{ route('salesOrderPage') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          style="width: 100%">


        {{ csrf_field() }}
        <input type="hidden" name="id_langganan" class="id-langganan">
        <input type="hidden" name="nama_langganan" class="nama-langganan">
        <input type="hidden" name="total" value="0">
        <input type="hidden" name="grand_total" value="0">
        <input type="hidden" name="bayar" value="0">
        <input type="hidden" name="no_so">
        <input type="hidden" name="urutan">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Order</label>
                                <div class="col-9 col-form-label no_so">-</div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Transaksi</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal" class="form-control m-input"
                                               id="m_datepicker_4_3"
                                               readonly="" value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input" name="keterangan"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Lokasi Order </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <select class="form-control m-select2" name="id_lokasi_penjualan">
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($lokasi as $r)
                                                <option value="{{$r->id}}">{{$r->lokasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Langganan</label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="nama_langganan"
                                               placeholder="Nama Langganan..." disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-langganan">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                                <div class="col-9 col-form-label alamat-langganan">
                                    -
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Telepon</label>
                                <div class="col-9 col-form-label telepon-langganan">
                                    -
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Salesman</label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <input type="hidden" name="id_salesman" class="id_salesman">
                                        <input type="text" class="form-control" name="nama_salesman"
                                               placeholder="Nama Salesman..." disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-salesman">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat Pengiriman </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <textarea class="form-control m-input" name="alamat_pengiriman"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Penerima </label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" class="form-control m-input" name="penerima">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Kirim</label>
                                <div class="col-9">
                                    <div class="input-group date">
                                        <input type="text" name="tanggal_kirim" class="form-control m-input"
                                               id="m_datepicker_5"
                                               value="{{ date('d-m-Y') }}">
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-add-row-produk btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Produk
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-produk table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th width="100">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Lokasi</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Disc (%)</th>
                                <th>Disc (Nominal)</th>
                                <th>Harga Net</th>
                                <th>Sub Total</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="total">0</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc (%)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="diskon_persen_akhir" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Disc Tambahan
                                    (Nominal)</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="potongan_akhir" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Pajak (%)</label>
                                <div class="col-8">
                                    <input type="text" class="form-control input-numeral"
                                           name="pajak" value="0">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Ongkos Kirim</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="ongkos_kirim" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                <div class="col-8 col-form-label">
                                    <strong><span class="grand-total">0</span></strong>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Bayar</label>
                                <div class="col-8">
                                    <input class="form-control m-input input-numeral" type="text"
                                           name="bayar" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-4 col-form-label">Metode Pembayaran </label>
                                <div class="col-8">
                                    <select class="form-control m-select2" name="metode_pembayaran">
                                        <option value="">Pilih Metode</option>
                                        <option value="cash">Cash</option>
                                        <option value="piutang">Piutang</option>
                                        <option value="bank">Bank</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="produksi-buttons">
                                        <span>
                                        <button type="submit"
                                                class="btn-simpan btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                            <span>
                                                <i class="la la-check"></i>Simpan Order</span></button>
                                            </span>
                            <a href="#" onclick="history.back(-1)"
                               class="btn-kembali btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-angle-double-left"></i>
                                            <span>Kembali</span>
                                        </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection