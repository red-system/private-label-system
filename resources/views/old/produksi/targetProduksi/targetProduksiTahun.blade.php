@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    @include('old.produksi.targetProduksi.targetProduksiBulanCreate')
    @include('old.produksi.targetProduksi.targetProduksiBulanEdit')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("targetProduksiPage") }}"
                       class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali ke Target Produksi</span>
                            </span>
                    </a>
                    {!! "&nbsp" !!}
                    <a href="#"
                       class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                       data-toggle="modal" data-target="#modal-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Produksi Target</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <input type="hidden" name="tahun_target" value="{{ $tahun }}">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Bulan</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Target Produksi</th>
                            <th width="180">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->bulan_target }}</td>
                                <td>{{ $r->produk->kode_produk }}</td>
                                <td>{{ $r->produk->nama_produk }}</td>
                                <td align="right">{{ Main::format_number($r->qty) }}</td>
                                <td>
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                        <button type="button"
                                                class="m-btn btn btn-success btn-edit"
                                                data-route="{{ route('targetProduksiUpdate', ['id'=>$r->id]) }}"
                                                data-redirect="?table_value={{ $no-1 }}.&table_index=0">
                                            <i class="la la-edit"></i> Edit
                                        </button>
                                        <button type="button"
                                                class="m-btn btn btn-danger btn-hapus"
                                                data-route='{{ route('targetProduksiDelete', ['id'=>$r->id]) }}'>
                                            <i class="la la-remove"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection