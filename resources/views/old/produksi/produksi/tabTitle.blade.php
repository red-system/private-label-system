<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item akses-daftar_produksi">
                <a class="nav-link m-tabs__link  {{ $tab == 'produksi' ? 'active':''  }}" href="{{ route('produksiPage') }}">
                    <i class="la la-refresh"></i> Daftar Produksi
                </a>
            </li>
            <li class="nav-item m-tabs__item akses_history_produksi">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':''  }}" href="{{ route('produksiHistoryPage') }}">
                    <i class="la la-history"></i> History Produksi
                </a>
            </li>
        </ul>
    </div>
</div>