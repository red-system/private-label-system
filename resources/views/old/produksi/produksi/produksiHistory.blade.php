@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/produksi.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tabs">
                @include('old.produksi.produksi.tabTitle')
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Filter Data
                            </h3>
                        </div>
                    </div>
                </div>
                <form method="get" class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-2 col-sm-12">Tanggal Produksi</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="input-daterange input-group" id="m_datepicker_5">
                                    <input type="text" class="form-control m-input" name="tgl_mulai_produksi"
                                           value="{{ $tgl_mulai_produksi }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">sampai dengan</span>
                                    </div>
                                    <input type="text" class="form-control" name="tgl_selesai_produksi"
                                           value="{{ $tgl_selesai_produksi }}">
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <button type="submit" class="btn btn-success">Filter Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Produksi</th>
                            <th>Pabrik</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Qty Produksi</th>
                            <th>Status</th>
                            {{--<th>Progress </th>--}}
                            <th>Publish</th>
                            <th width="200">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            @php($total_produksi += $r->total_produksi)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->kode_produksi }}</td>
                                <td>{{ $r->lokasi->lokasi }}</td>
                                <td>{{ Main::format_date($r->tgl_mulai_produksi) }}</td>
                                <td>{{ Main::format_date($r->tgl_selesai_produksi) }}</td>
                                <td align="right">{{ Main::format_number($r->total_produksi) }}</td>
                                <td align="center">
                                    <span class="m-badge m-badge--success m-badge--wide">Completed</span>
                                </td>
                                {{--<td>100%</td>--}}
                                <td align="center">
                                    <i class="fa fa-check-circle m--font-success icon-status"></i>
                                </td>
                                <td align="center">
                                    <div class="btn-group m-btn-group m-btn-group--pill">
                                        <button type="button"
                                                class="btn-produksi-detail m-btn btn-sm btn btn-info"
                                                data-route='{{ route('produksiDetail', ['id'=>$r->id]) }}'>
                                            <i class="fa fa-info"></i> Detail Produksi
                                        </button>
                                        <a href="{{ route('produksiProgressHistory', ['id'=> Main::encrypt($r->id)]) }}"
                                           class="m-btn btn-sm btn btn-success">
                                                <i class="fa fa-sync"></i> Detail Progress
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tfoot>
                            <tr>
                                <td colspan="5" align="center">
                                    <strong>TOTAL</strong>
                                </td>
                                <td align="right">
                                    <strong>{{ Main::format_number($total_produksi) }}</strong>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('old.produksi.produksi.produksiDetailModal')
@endsection