@foreach($data as $key=>$row)

    @php
        $qty_diperlukan = $row['qty_diperlukan'];
        $qty_kurang = '<i class="fa fa-check-circle m--font-success icon-status"></i>';
        if($row['qty_diperlukan'] > $row['qty_tersedia']) {
            $qty_kurang = $row['qty_tersedia'] - $row['qty_diperlukan'];
            $qty_kurang = '<span class="m-badge m-badge--danger m-badge--wide"><strong>'.Main::format_number($qty_kurang).'</strong></span>';
        }
    @endphp
    <tr>
        <td class="hidden">
            <input type="hidden" name="id_bahan[{{ $key }}]" value="{{ $row['id_bahan'] }}">
            <input type="hidden" name="id_satuan[{{ $key }}]" value="{{ $row['id_satuan'] }}">
            <input type="hidden" name="qty_diperlukan[{{ $key }}]" value="{{ $row['qty_diperlukan'] }}">
        </td>
        <td>{{ $row['kode_bahan'] }}</td>
        <td>{{ $row['nama_bahan'] }}</td>
        <td align="right">{{ Main::format_number($row['qty_diperlukan']) }}</td>
        <td align="right">{{ Main::format_number($row['qty_tersedia']) }}</td>
        <td align="center">{!! $qty_kurang !!}</td>
        <td width="500">
            @if(count($row['gudang_bahan']) > 0)
                <table width="100%">
                    <thead>
                    <tr>
                        <th>Nama Gudang</th>
                        <th>Stok Tersedia</th>
                        <th width="100">Jumlah Stok Digunakan</th>
                    </tr>
                    </thead>
                    @foreach($row['gudang_bahan'] as $row_gudang_bahan)
                        @php
                        /**
                            untuk mendapatkan qty_bahan yang diambil disetiap gudang
                        */
                            $qty_bahan = 0;
                            $qty_stok = $row_gudang_bahan['qty'];
                            $id_stok_bahan = $row_gudang_bahan['id'];

                            if($qty_diperlukan > 0) {
                                if($qty_diperlukan >= $qty_stok) {
                                    $qty_bahan = $qty_stok;
                                    $qty_diperlukan = $qty_diperlukan - $qty_stok;
                                } else {
                                    $qty_bahan = $qty_diperlukan;
                                    $qty_diperlukan = 0;
                                }
                            }

                        @endphp
                        <tr>
                            <td>{{ $row_gudang_bahan->lokasi->lokasi }}</td>
                            <td>{{ Main::format_number($row_gudang_bahan['qty']) }}</td>
                            <td width="150">
                                <input type="text"
                                       name="qty_digunakan[{{ $key }}][{{ $id_stok_bahan }}]"
                                       value="{{ $qty_bahan }}"
                                       class="form-control touchspin-number-decimal-js"
                                       max="{{ $row['qty_diperlukan'] }}"
                                       min="0"
                                       step="{{ $decimalStep }}">
                            </td>
                        </tr>
                    @endforeach
                </table>
                @if($qty_diperlukan > 0)
                    <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-warning"></i>
                        </div>
                        <div class="m-alert__text">
                            <strong>Perhatian!</strong><br />
                            <strong>{{ $row['nama_bahan'] }} Kurang.</strong> segera tambah bahan pada gudang.<br />
                        </div>
                        <div class="m-alert__close">
                            <button type="button" class="close" data-close="alert" aria-label="Hide">
                            </button>
                        </div>
                    </div>
                @endif
            @else
                <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong>Perhatian!</strong><br />
                        <strong>{{ $row['nama_bahan'] }} Kosong.</strong> tidak ada data di Gudang dalam Sistem.<br />
                        Segera isi ulang untuk melakukan produksi.
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Hide">
                        </button>
                    </div>
                </div>
            @endif
        </td>
    </tr>
@endforeach