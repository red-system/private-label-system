@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/produksi.js') }}"></script>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("produksiPage") }}"
                       class="btn-produk-add btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar Produksi</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">
            <form action="{{ route('produksiInsert') }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('produksiPage') }}"
                  data-alert-show="true"
                  data-alert-field-message="true">
                {{ csrf_field() }}
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-placeholder"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Data Produksi
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Kode
                                        Produksi</label>
                                    <div class="col-9">
                                        <input class="form-control m-input disabled"
                                               type="text"
                                               name="kode_produksi"
                                               value="{{ $prefixProduksi.$urutan_produksi }}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Mulai
                                        Produksi</label>
                                    <div class="col-9">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker"
                                                   name="tgl_mulai_produksi" readonly value="{{ date('d-m-Y') }}"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Selesai
                                        Produksi</label>
                                    <div class="col-9">
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input m_datepicker"
                                                   name="tgl_selesai_produksi" readonly value="{{ date('d-m-Y') }}"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label required">Pabrik</label>
                                    <div class="col-9">
                                        <select class="form-control m-input m-select2" name="id_lokasi">
                                            @foreach($lokasi as $r)
                                                <option value="{{ $r->id }}"
                                                        data-kode-lokasi="{{ $r->kode_lokasi }}"
                                                        data-lokasi="{{ $r->lokasi }}">{{ $r->kode_lokasi.' - '.$r->lokasi }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label">Catatan</label>
                                    <div class="col-9">
                                        <textarea class="form-control m-input" name="catatan"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-box"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Produk yang Produksi
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button"
                                            class="btn-produk-add btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>Tambah Produk</span>
                                    </span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <table class="table-produk table m-table m-table--head-separator-primary">
                                    <thead>
                                    <tr>
                                        <th>Kode Produk</th>
                                        <th>Nama Produk</th>
                                        <th>Qty Produksi</th>
                                        <th>Keterangan</th>
                                        <th width="100">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-bag"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Bahan untuk Diproduksi
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <table class="table-bahan table m-table m-table--head-separator-primary">
                                    <thead>
                                    <tr>
                                        <th>Kode Bahan</th>
                                        <th>Nama Bahan</th>
                                        <th>Qty Diperlukan</th>
                                        <th>Qty Tersedia</th>
                                        <th>Qty Kurang</th>
                                        <th>Gudang</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="produksi-buttons">
                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Produksi</span>
                        </span>
                    </button>

                    <a href="{{ route("produksiPage") }}"
                       class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali ke Daftar Produksi</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="row-produk hidden">
        <table>
            <tr>
                <td>
                    <select class="form-control id-produk" name="id_produk[]" style="width: 100%">
                        <option value="">Pilih Produk</option>
                        @foreach($produk as $r)
                            <option value="{{ $r->id }}"
                                    data-kode-produk="{{ $r->kode_produk }}"
                                    data-nama-produk="{{ $r->nama_produk }}">
                                {{ $r->kode_produk }}
                            </option>
                        @endforeach
                    </select>
                </td>
                <td class="nama-produk">

                </td>
                <td class="qty">
                    <input type="text" class="form-control detail-produksi-qty" name="qty[]" value="1" min="1">
                </td>
                <td>
                    <textarea name="keterangan[]" class="form-control"></textarea>
                </td>
                <td>
                    <button type="button" class="btn-hapus-produk btn m-btn--pill btn-danger btn-sm"
                            data-confirm="false">
                        <i class="la la-remove"></i> Hapus
                    </button>
                </td>
            </tr>
        </table>
    </div>

@endsection