<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'progress' ? 'active':''  }}" href="{{ route('produksiProgress', ['id'=>Main::encrypt($id_produksi)]) }}">
                    <i class="la la-refresh"></i> Hasil Produksi
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link {{ $tab == 'history' ? 'active':''  }}" href="{{ route('produksiProgressHistory', ['id'=>Main::encrypt($id_produksi)]) }}">
                    <i class="la la-history"></i> History Hasil Produksi
                </a>
            </li>
        </ul>
    </div>
</div>