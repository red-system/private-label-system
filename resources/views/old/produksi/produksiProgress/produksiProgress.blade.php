@extends('../general/index')

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/produksi.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/produksi_progress.js') }}"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--tabs">
                {{--@include('produksi.produksiProgress.tabTitle')--}}
                <div class="m-portlet__body">

                    <form action="{{ route('produksiProgressInsert', ['id'=>$id_produksi]) }}"
                          method="post"
                          class="form-send"
                          data-confirm="true"
                          data-redirect="{{ route('produksiPage') }}"
                          data-alert-show="true"
                          data-alert-field-message="true">

                        {{ csrf_field() }}
                        <table class="table table-striped m-table table-bordered table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama Produk</th>
                                {{--                                <th>Qty Produksi</th>--}}
                                {{--                                <th>Qty Sudah Selesai</th>--}}
                                <th width="180">Tanggal Selesai</th>
                                <th width="140">Qty Hasil</th>
                                <th>HPP</th>
                                <th>No Seri Produk</th>
                                <th>Gudang</th>
                                <th>Catatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)
                                @php
                                    $no_pengemas = 1;
                                        $count_success = 0;
                                        $id_detail_produksi = $r->id;
                                        $id_produk = $r->id_produk;
                                        $qty_produksi = $r->qty;
                                        //$qty_selesai = \app\Models\mProgressProduksi::where('id_detail_produksi', $id_detail_produksi)->sum('qty_progress');

                                        //$statusValue = $qty_produksi > $qty_selesai ? FALSE : TRUE;

    /*                                    if($statusValue) {
                                            $statusLabel = '<i class="fa fa-check-circle m--font-default icon-status"></i>';
                                        } else {
                                            $statusLabel = '<i class="fa fa-minus-circle m--font-warning icon-status"></i>';
                                        }*/


                                @endphp


                                {{--        @if($statusValue)
                                            @php(
                                                $count_success++
                                            )
                                            <tr class="m-table__row--success text-info">
                                                <td align="center">{{ $no++ }}.</td>
                                                <td>{{ $r->produk->kode_produk }}</td>
                                                <td>{{ $r->produk->nama_produk }}</td>
                                                <td>{{ Main::format_number($qty_produksi) }}</td>
                                                <td>{{ Main::format_number($qty_selesai) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center">
                                                    {!! $statusLabel !!}
                                                </td>
                                            </tr>
                                        @else--}}
                                <tr data-id-detail-produksi="{{ $id_detail_produksi }}">
                                    <td class="m--hide">
                                        <input type="hidden" name="id_detail_produksi[]"
                                               value="{{ $id_detail_produksi }}">
                                        <input type="hidden" name="id_produk[{{ $id_detail_produksi }}]"
                                               value="{{ $id_produk }}">
                                        <input type="hidden" name="qty_produksi[{{ $id_detail_produksi }}]"
                                               value="{{ $qty_produksi }}">
                                        {{--<input type="hidden" name="qty_selesai[{{ $id_detail_produksi }}]" value="{{ $qty_selesai }}">--}}
                                    </td>
                                    <td align="center">{{ $no++ }}.</td>
                                    <td>{{ '('.$r->produk->kode_produk.') '.$r->produk->nama_produk }}</td>
                                    {{--                                        <td>{{ Main::format_number($qty_produksi) }}</td>--}}
                                    {{--<td class="text-center"><strong>{{ Main::format_number($qty_selesai) }}</strong></td>--}}
                                    <td>
                                        <div class="input-group date">
                                            <input type="text"
                                                   class="form-control m-input m_datepicker"
                                                   name="tgl_selesai[{{ $id_detail_produksi }}]"
                                                   readonly
                                                   value="{{ date('d-m-Y') }}"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text"
                                               class="form-control touchspin-number qty-progress"
                                               name="qty_progress[{{ $id_detail_produksi }}]"
                                               data-id-produk="{{ $r->id_produk }}"
                                               data-id-produksi="{{ $r->id_produksi }}"
                                               data-id-detail-produksi="{{ $r->id }}"
                                               value="0"
                                               data-touchspin-max="{{ $qty_produksi }}">
                                    </td>
                                    <td class="hpp">
                                        <input type="text"
                                               class="form-control touchspin-number-decimal"
                                               name="hpp[{{ $id_detail_produksi }}]">
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-warning"
                                                        type="button"
                                                        data-skin="dark"
                                                        data-toggle="m-tooltip"
                                                        data-placement="top"
                                                        title=""
                                                        data-html="true"
                                                        data-original-title="{{ $ketentuan_no_seri_produksi }}">
                                                    <i class="flaticon-info"></i> Ketentuan
                                                </button>
                                            </div>
                                            <input type="text" name="no_seri_produk[{{ $id_detail_produksi }}]"
                                                   class="form-control">
                                        </div>
                                    </td>
                                    <td>
                                        <select name="id_lokasi[{{ $id_detail_produksi }}]"
                                                class="form-control m-select2"
                                                style="width: 200px">
                                            <option value="">Pilih Gudang</option>
                                            @foreach($gudang as $r_gudang)
                                                <option value="{{ $r_gudang->id }}">{{ $r_gudang->lokasi }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <textarea class="form-control"
                                                  name="deskripsi[{{ $id_detail_produksi }}]"></textarea>
                                    </td>
                                </tr>
                                @if(count($r->produk->pengemas_produk) > 0)
                                <tr>
                                    <td></td>
                                    <td colspan="8">
                                        <h4><i class="flaticon-box"></i> Bahan Pengemas Produk</h4>
                                        <table class="table table-bordered m-table m-table--border-success">
                                            <thead>
                                            <tr>
                                                <th width="15">No</th>
                                                <th>Nama Bahan</th>
                                                <th>Satuan</th>
                                                <th>Total Stok Sekarang</th>
                                                {{--<th>Qty Diperlukan</th>--}}
                                                <th>Qty Total Digunakan</th>
                                                <th>Status Bahan</th>
                                                <th width="440">Gudang Bahan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($r->produk->pengemas_produk as $r_pengemas)

                                                @php(
                                                    $total_stok = \app\Models\mStokBahan::where('id_bahan', $r_pengemas->id_bahan)->sum('qty')
                                                )

                                                @php($id_bahan = $r_pengemas->id_bahan)

                                                <tr class="tr-bahan"
                                                    data-id-detail-produksi="{{ $id_detail_produksi }}"
                                                    data-id-bahan="{{ $id_bahan }}"
                                                    data-total-stok="{{ $total_stok }}"
                                                    data-qty-bahan-pengemas="{{ $r_pengemas->qty }}"
                                                    data-qty-bahan-digunakan="">
                                                    <td class="td-data m--hide">
                                                        <input type="text" name="total_stok[]" value="{{ $total_stok }}">
                                                        <input type="text"
                                                               class="qty-bahan-digunakan"
                                                               name="qty_bahan_digunakan[]"
                                                               data-id-bahan="{{ $id_bahan }}"
                                                               value="">
                                                    </td>
                                                    <td class="align-middle text-center">{{ $no_pengemas++ }}.</td>
                                                    <td class="align-middle">{{ '('.$r_pengemas->bahan->kode_bahan.') '.$r_pengemas->bahan->nama_bahan }}</td>
                                                    <td class="align-middle">{{ $r_pengemas->bahan->satuan->satuan }}</td>
                                                    <td class="align-middle"
                                                        align="right">
                                                        {{ Main::format_number($total_stok) }}
                                                    </td>
                                                    {{--<td align="right"--}}
                                                        {{--class="td-qty-bahan-pengemas align-middle">--}}
                                                        {{--{{ Main::format_number($r_pengemas->qty) }}--}}
                                                    {{--</td>--}}
                                                    <td align="right"
                                                        class="td-qty-bahan-digunakan align-middle">
                                                        0
                                                    </td>
                                                    <td class="td-status-bahan align-middle text-center ">
                                                        <i class="fa fa-check-circle m--font-success icon-status"></i>
                                                    </td>
                                                    <td class="td-pengemas-produk">
                                                        <table class="table table-bordered m-table m-table--border-success">
                                                            <thead>
                                                            <tr>
                                                                <th width="100">Lokasi</th>
                                                                <th>Stok Bahan</th>
                                                                <th>No Seri Bahan</th>
                                                                <th>Jumlah Bahan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($r_pengemas->bahan->stok_bahan as $r_stok_bahan)

                                                                @php($uniqid = uniqid())

                                                            <tr class="tr-stok-pengemas-bahan"
                                                                data-id-produksi="{{ $id_produksi }}"
                                                                data-id-produk="{{ $id_produk }}"
                                                                data-id-bahan="{{ $id_bahan }}"
                                                                data-id-detail-produksi="{{ $id_detail_produksi }}">
                                                                <td>{{ $r_stok_bahan->lokasi->lokasi }}</td>
                                                                <td align="right">{{ Main::format_number($r_stok_bahan->qty) }}</td>
                                                                <td>{{ $r_stok_bahan->no_seri_bahan }}</td>
                                                                <td class="td-qty-bahan-pengemas">
                                                                    <input type="text"
                                                                           class="input-qty-bahan-pengemas form-control touchspin-number"
                                                                           name="qty_bahan_pengemas[{{ $uniqid }}][qty]"
                                                                           value="0">
                                                                    <input type="hidden"
                                                                           name="qty_bahan_pengemas[{{ $uniqid }}][id_stok_bahan]"
                                                                           value="{{ $r_stok_bahan->id }}">
                                                                    <input type="hidden"
                                                                           name="qty_bahan_pengemas[{{ $uniqid }}][id_detail_produksi]"
                                                                           value="{{ $id_detail_produksi }}">
                                                                </td>
                                                            </tr>

                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                @endif
                                {{--                                    </tr>
                                                                @endif--}}
                            @endforeach
                            </tbody>
                        </table>
                        <div class="produksi-buttons">
                            {{--                            @if(!count($list) == $count_success)--}}
                            <button type="submit"
                                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-check"></i>
                                    <span>Simpan Progress</span>
                                </span>
                            </button>
                            {{--@endif--}}
                            <a href="{{ route("produksiPage") }}"
                               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-angle-double-left"></i>
                                    <span>Kembali ke Produksi</span>
                                </span>
                            </a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection