<h4>Detail Produksi</h4>
<hr/>
<div class="m-form">
    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first row data-detail-section">
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Kode Produksi</label>
                    <div class="col-lg-6 col-form-label">
                        {{ $produksi->kode_produksi }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Mulai Produksi</label>
                    <div class="col-lg-6 col-form-label">
                        {{ $produksi->tgl_mulai_produksi }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-6 col-form-label">Selesai Produksi</label>
                    <div class="col-lg-6 col-form-label">
                        {{ $produksi->tgl_selesai_produksi }}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6">
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Pabrik</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $produksi->lokasi->lokasi }}
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Catatan</label>
                    <div class="col-lg-9 col-form-label">
                        {{ $produksi->catatan }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<h4>Produk</h4>
<hr/>
<table class="table m-table m-table--head-separator-primary">
    <thead>
    <tr>
        <th>No</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>Qty</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php(
        $no = 1
    )
    @foreach($detail_produksi as $r)
        <tr>
            <td>{{ $no++ }}.</td>
            <td>{{ $r->produk->kode_produk }}</td>
            <td>{{ $r->produk->nama_produk }}</td>
            <td>{{ Main::format_number($r->qty) }}</td>
            <td>{{ $r->keterangan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<br/>
<h4>Bahan yang Digunakan Produksi</h4>
<hr/>

<ul class="nav nav-tabs nav-fill" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#tab-bahan-produksi">
            <i class="flaticon-shopping-basket"></i> BAHAN PRODUKSI
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#tab-bahan-pengemas">
            <i class="flaticon-price-tag"></i> BAHAN PENGEMAS PRODUK
        </a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="tab-bahan-produksi" role="tabpanel">
        <table class="table m-table m-table--head-separator-primary">
            <thead>
            <tr>
                <th>No</th>
                <th>Kode Bahan</th>
                <th>Nama Bahan</th>
                <th>Qty Diperlukan</th>
                <th>Gudang Bahan</th>
            </tr>
            </thead>
            <tbody>
            @php(
                $no = 1
            )
            @foreach($bahan_produksi as $r)

                <?php
                $gudang_qty = json_decode($r->gudang_qty, TRUE);
                ?>
                <tr>
                    <td>{{ $no++ }}.</td>
                    <td>{{ $r->bahan['kode_bahan'] }}</td>
                    <td>{{ $r->bahan['nama_bahan'] }}</td>
                    <td>{{ Main::format_number($r->qty_diperlukan) }}</td>
                    <td>
                        <table class="table table-bordered m-table">
                            <thead>
                            <tr>
                                <th>Gudang</th>
                                <th>Qty Digunakan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($gudang_qty as $id_stok_bahan=>$qty_digunakan)

                                <?php
                                $gudang = \app\Models\mStokBahan::with('agencies:id,agencies')->where('id', $id_stok_bahan)->first()->lokasi->lokasi;
                                ?>
                                <tr>
                                    <td>{{ $gudang }}</td>
                                    <td>{{ Main::format_number($qty_digunakan) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="tab-bahan-pengemas" role="tabpanel">
        <table class="table m-table m-table--head-separator-primary">
            <thead>
            <tr>
                <th>No</th>
                <th>Kode Bahan</th>
                <th>Nama Bahan</th>
                <th>Penggunaan Bahan</th>
            </tr>
            </thead>
            <tbody>
            @php($no = 1)

            @foreach($progress_produksi_pengemas as $r)

                <?php
                    $no_stok_produk = 1;
                    $id_bahan = $r->id_bahan;
                    $stok_bahan = \app\Models\mProgressProduksiPengemas
                        ::with('stok_bahan.agencies')
                        ->where([
                            'id_progress_produksi'=>$id_progress_produksi,
                            'id_bahan'=>$id_bahan
                        ])
                        ->get();
                ?>
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $r->bahan->kode_bahan }}</td>
                    <td>{{ $r->bahan->nama_bahan }}</td>
                    <td>
                        <table class="table m-table m-table--head-separator-primary">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Gudang</th>
                                <th>No Seri Bahan</th>
                                <th>Qty Digunakan</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($stok_bahan as $r2_stok_bahan)
                                    <tr>
                                        <td>{{ $no_stok_produk++ }}.</td>
                                        <td>{{ $r2_stok_bahan->stok_bahan->lokasi->lokasi }}</td>
                                        <td>{{ $r2_stok_bahan->stok_bahan->no_seri_bahan }}</td>
                                        <td align="right">{{ Main::format_number($r2_stok_bahan->qty) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
