@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th width="230">Tanggal Target</th>
                            <th width="180">Qty</th>
                            <th>Total Produksi Produk</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->produk->kode_produk }}</td>
                                <td>{{ $r->produk->nama_produk }}</td>
                                <td>{{ $r->tgl_target }}</td>
                                <td align="right">{{ Main::format_number($r->qty) }}</td>
                                <td align="right">{{ Main::format_number($r->qtySekarang) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
@endsection