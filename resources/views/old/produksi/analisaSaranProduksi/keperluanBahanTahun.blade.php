@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route("keperluanBahanTarget") }}"
                       class="btn-produk-add btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                @include('old.produksi.analisaSaranProduksi.tabTitle')
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Bahan</th>
                                <th>Nama Bahan</th>
                                <th>Keperluan Bahan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)
                                @php(
                                    $total_bahan += $r->total_bahan_diperlukan
                                )
                                <tr>
                                    <td align="center">{{ $no++ }}.</td>
                                    <td>{{ $r->kode_bahan }}</td>
                                    <td>{{ $r->nama_bahan }}</td>
                                    <td align="right">{{ Main::format_number($r->total_bahan_diperlukan) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" align="center">
                                        <strong>TOTAL</strong>
                                    </td>
                                    <td align="right">
                                        <strong>{{ Main::format_number($total_bahan) }}</strong>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection