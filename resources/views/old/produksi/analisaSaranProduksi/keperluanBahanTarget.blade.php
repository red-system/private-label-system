@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-touchspin.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">


        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <div class="m-portlet m-portlet--tabs">
                @include('old.produksi.analisaSaranProduksi.tabTitle')
                <div class="m-portlet__body akses-keperluan_bahan_target">
                    <div class="tab-content">
                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Tahun Produksi</th>
                                <th>Total Target Qty Produk</th>
                                <th>Total Qty Bahan Produk</th>
                                <th>Total Qty Bahan Diperlukan Produksi</th>
                                <th width="200">Menu</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $r)
                                <tr>
                                    <td align="center">{{ $no++ }}.</td>
                                    <td>{{ $r['tahun_target'] }}</td>
                                    <td align="right">{{ Main::format_number($r['total_target']) }}</td>
                                    <td align="right">{{ Main::format_number($r['total_bahan_produk']) }}</td>
                                    <td align="right">{{ Main::format_number($r['total_bahan']) }}</td>
                                    <td>
                                        <a class="m-btn btn btn-success m-btn--pill m-btn--sm akses-keperluan_bahan_target_pertahun"
                                           href="{{ route('keperluanBahanTahun', ['tahun'=>Main::encrypt($r['tahun_target'])]) }}">
                                            <i class="la la-calendar"></i> Per Tahun {{ $r['tahun_target'] }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection