<div class="m-portlet__head">
    <div class="m-portlet__head-tools">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
            <li class="nav-item m-tabs__item akses-perkiraan_jumalh_produksi">
                <a class="nav-link m-tabs__link {{ $tab == 'perkiraanJumlahProduksi' ? 'active':''  }}" href="{{ route('perkiraanJumlahProduksi') }}">
                    <i class="la la-cog"></i> Perkiraan Jumlah Produksi
                </a>
            </li>
            <li class="nav-item m-tabs__item akses-jumlah_bahan_terpakai">
                <a class="nav-link m-tabs__link {{ $tab == 'jumlahBahanTerpakai' ? 'active':''  }}" href="{{ route('jumlahBahanTerpakai') }}">
                    <i class="la la-briefcase"></i> Jumlah Bahan Terpakai
                </a>
            </li>
            <li class="nav-item m-tabs__item akses-keperluan_bahan_target">
                <a class="nav-link m-tabs__link {{ $tab == 'keperluanBahanTarget' ? 'active':''  }}" href="{{ route('keperluanBahanTarget') }}">
                    <i class="la la-bell-o"></i> Keperluan Bahan Sesuai Target Produksi
                </a>
            </li>
        </ul>
    </div>
</div>