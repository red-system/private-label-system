@extends('../general/index')

@section('js')
    {{--        <script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/demo5_new/js/pages/widgets.js') }}"></script>
    <script src="{{ asset('assets/demo/demo5_new/plugins/global/plugins.bundle.js')}}"></script>
    <script src="{{ asset('assets/demo/demo5_new/js/scripts.bundle.js')}}"></script>
    <script src="{{ asset('assets/demo/demo5_new/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>


    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->




    {{--            type="text/javascript"></script>--}}
    {{--    <script type="text/javascript">--}}
    {{--        $(document).ready(function () {--}}
    {{--            var amChartsChartsDemo = function () {--}}
    {{--                return {--}}
    {{--                    init: function () {--}}
    {{--                        AmCharts.makeChart("m_amcharts_4", {--}}
    {{--                                theme: "light",--}}
    {{--                                type: "serial",--}}
    {{--                                dataProvider: [--}}
    {{--                                        @foreach($data_penjualan as $r)--}}
    {{--                                    {--}}
    {{--                                        month: "{{ $r['month'] }}",--}}
    {{--                                        total_penjualan: "{{ $r['total_penjualan'] }}",--}}
    {{--                                    },--}}
    {{--                                    @endforeach--}}
    {{--                                ], valueAxes: [{--}}
    {{--                                    stackType: "3d", unit: "", position: "left", title: "Total Penjualan Produk"--}}
    {{--                                }--}}
    {{--                                ], startDuration: 1, graphs: [{--}}
    {{--                                    balloonText: "GDP grow in [[category]] (2004): <b>[[value]]</b>",--}}
    {{--                                    fillAlphas: .9,--}}
    {{--                                    lineAlpha: .2,--}}
    {{--                                    title: "2004",--}}
    {{--                                    type: "column",--}}
    {{--                                    valueField: "total_penjualan"--}}
    {{--                                }--}}
    {{--                                ], plotAreaFillAlphas: .1, depth3D: 60, angle: 30, categoryField: "month", categoryAxis: {--}}
    {{--                                    gridPosition: "start"--}}
    {{--                                }--}}
    {{--                                , export: {--}}
    {{--                                    enabled: !0--}}
    {{--                                }--}}
    {{--                            }--}}
    {{--                        )--}}
    {{--                    }--}}
    {{--                }--}}
    {{--            }--}}

    {{--            ();--}}
    {{--            jQuery(document).ready(function () {--}}
    {{--                    amChartsChartsDemo.init()--}}
    {{--                }--}}
    {{--            );--}}
    {{--        });--}}
    {{--    </script>--}}
@endsection

@section('css')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    {{--    <link href="{{ asset('assets/demo/demo5_new/plugins/custom/fullcalendar/ful')}}lcalendar.bundle.css" rel="stylesheet" type="text/css"/>--}}
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->

    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/demo/demo5_new/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('body')
    @include('dashboard/importExcel')
    <div class="container">
        <br>
        <!--begin::Dashboard-->

        <!--begin::Row-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Charts Widget 2-->
                <div class="card card-custom bg-white-100 gutter-b card-stretch card-shadowless">
                    <!--begin::Header-->
                    <div class="card-header h-auto border-0">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                            <h3 class="card-label">
                                <span class="d-block text-dark font-weight-bolder">Agency Growth</span>
                                {{--                                <span class="d-block text-dark-50 mt-2 font-size-sm">Here's how growth compares between Client, Agency, and Fullillment</span>--}}
                            </h3>
                        </div>
                        @if($from[0] == 'admin_id')
                            <div class="card-toolbar">
                                {{--                            <ul class="card-title py-5">--}}
                                {{--                                <li class="nav-item">--}}
                                <a href="#" class="card-title btn btn-success text-light font-size-sm"
                                   data-toggle="modal" data-target="#import-excel">
                                    <i class="la la-plus"></i>Import</a>
                                {{--                                </li>--}}
                                {{--                                <li class="nav-item">--}}
                                {{--                                    <a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_charts_widget_2_chart_tab_3">--}}
                                {{--                                        <span class="nav-text font-size-sm">Day</span>--}}
                                {{--                                    </a>--}}
                                {{--                                </li>--}}
                                {{--                            </ul>--}}
                            </div>
                    @endif
                    <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin::Chart-->
                        <input type="hidden" id="growth" value="{{$growth}}">
                        <div id="kt_charts_widget_2_chart"></div>
                        <!--end::Chart-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Charts Widget 2-->
            </div>
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row">
            <div class="col-xl-8">
                <!--begin::Advance Table Widget 2-->
                <div class="card card-custom gutter-b card-stretch card-shadowless bg-white">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">New Client</span>
                            {{--                            <span class="text-muted mt-3 font-weight-bold font-size-sm">Well done your portfolio is growing</span>--}}
                        </h3>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-3 pb-0">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-borderless table-vertical-center">
                                <thead>
                                <tr>
                                    <th class="p-0" style="width: 50px">No</th>
                                    <th class="p-0" style="min-width: 200px">Client Name</th>
                                    <th class="p-0" style="min-width: 100px">Channel</th>
                                    {{--                                    <th class="p-0" style="min-width: 125px">Agreed Fee</th>--}}
                                    <th class="p-0" style="min-width: 110px">Status</th>
                                    {{--                                    <th class="p-0" style="min-width: 150px"></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @php($no = 1)
                                @foreach($project_data as $pd)
                                    <tr>
                                        <td class="pl-0 py-4">
                                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$no++}}</span>
                                        </td>
                                        <td class="pl-0">
                                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$pd->client_name}}</span>

                                        </td>
                                        <td class="pl-0">
                                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$pd->channel_name}}</span>
                                        </td>
                                        {{--                                    <td class="pl-0">--}}
                                        {{--                                        <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$pd->agreed_fee}}</span>--}}
                                        {{--                                    </td>--}}
                                        <td class="pl-0">
                                            <span class="label font-weight-bold label-lg label-inline"
                                                  style="color: {{$pd->project_status->project_status_text_color}} ; background-color: {{$pd->project_status->project_status_bc_color}} ;">{{$pd->project_status->project_status_name}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 2-->
            </div>
            <div class="col-xl-4">
                <!--begin::List Widget 5-->
                <div class="card card-custom bg-light-warning gutter-b card-stretch">
                    <!--begin::header-->
                    <div class="card-header border-0">
                        <h3 class="card-title font-weight-bolder text-warning">Approvals</h3>
                        <div class="card-toolbar">
                            <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left"
                                 data-original-title="Quick actions">
                            </div>
                        </div>
                    </div>
                    <!--end::header-->
                    <!--begin::Body-->
                    <div class="card-body pt-0">
                        <!--begin::Item-->
                        <div class="d-flex align-items-center mb-6">
                            <!--begin::Text-->
                            <div class="d-flex flex-column flex-grow-1 py-2">
                                <a class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1"
                                   style="text-align: center">Coming Soon</a>
                            </div>
                            <!--end::Text-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                    {{--                        <div class="d-flex align-items-center mb-6">--}}
                    {{--                            <!--begin::Checkbox-->--}}
                    {{--                            <label class="checkbox checkbox-lg checkbox-warning checkbox-single flex-shrink-0 m-0 mr-4">--}}
                    {{--                                <input type="checkbox" value="1"/>--}}
                    {{--                                <span class="bg-gray-300"></span>--}}
                    {{--                            </label>--}}
                    {{--                            <!--end::Checkbox-->--}}
                    {{--                            <!--begin::Text-->--}}
                    {{--                            <div class="d-flex flex-column flex-grow-1 py-2">--}}
                    {{--                                <a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Stakeholder--}}
                    {{--                                    Meeting</a>--}}
                    {{--                                <a href="#" class="text-muted font-weight-bold text-hover-primary">Due in 3 Weeks</a>--}}
                    {{--                            </div>--}}
                    {{--                            <!--end::Text-->--}}
                    {{--                            <!--begin: Dropdown-->--}}
                    {{--                            <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions"--}}
                    {{--                                 data-placement="left">--}}
                    {{--                                <a href="#" class="btn btn-clean btn-hover-warning btn-sm btn-icon"--}}
                    {{--                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--                                    <i class="ki ki-bold-more-hor"></i>--}}
                    {{--                                </a>--}}
                    {{--                                <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">--}}
                    {{--                                    <!--begin::Navigation-->--}}
                    {{--                                    <ul class="navi navi-hover">--}}
                    {{--                                        <li class="navi-header font-weight-bold py-4">--}}
                    {{--                                            <span class="font-size-lg">Choose Label:</span>--}}
                    {{--                                            <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip"--}}
                    {{--                                               data-placement="right" title="Click to learn more..."></i>--}}
                    {{--                                        </li>--}}
                    {{--                                        <li class="navi-separator mb-3 opacity-70"></li>--}}
                    {{--                                        <li class="navi-item">--}}
                    {{--                                            <a href="#" class="navi-link">--}}
                    {{--																		<span class="navi-text">--}}
                    {{--																			<span class="label label-xl label-inline label-light-success">Customer</span>--}}
                    {{--																		</span>--}}
                    {{--                                            </a>--}}
                    {{--                                        </li>--}}
                    {{--                                        <li class="navi-item">--}}
                    {{--                                            <a href="#" class="navi-link">--}}
                    {{--																		<span class="navi-text">--}}
                    {{--																			<span class="label label-xl label-inline label-light-danger">Partner</span>--}}
                    {{--																		</span>--}}
                    {{--                                            </a>--}}
                    {{--                                        </li>--}}
                    {{--                                        <li class="navi-item">--}}
                    {{--                                            <a href="#" class="navi-link">--}}
                    {{--																		<span class="navi-text">--}}
                    {{--																			<span class="label label-xl label-inline label-light-warning">Suplier</span>--}}
                    {{--																		</span>--}}
                    {{--                                            </a>--}}
                    {{--                                        </li>--}}
                    {{--                                        <li class="navi-item">--}}
                    {{--                                            <a href="#" class="navi-link">--}}
                    {{--																		<span class="navi-text">--}}
                    {{--																			<span class="label label-xl label-inline label-light-primary">Member</span>--}}
                    {{--																		</span>--}}
                    {{--                                            </a>--}}
                    {{--                                        </li>--}}
                    {{--                                        <li class="navi-item">--}}
                    {{--                                            <a href="#" class="navi-link">--}}
                    {{--																		<span class="navi-text">--}}
                    {{--																			<span class="label label-xl label-inline label-light-dark">Staff</span>--}}
                    {{--																		</span>--}}
                    {{--                                            </a>--}}
                    {{--                                        </li>--}}
                    {{--                                        <li class="navi-separator mt-3 opacity-70"></li>--}}
                    {{--                                        <li class="navi-footer py-4">--}}
                    {{--                                            <a class="btn btn-clean font-weight-bold btn-sm" href="#">--}}
                    {{--                                                <i class="ki ki-plus icon-sm"></i>Add new</a>--}}
                    {{--                                        </li>--}}
                    {{--                                    </ul>--}}
                    {{--                                    <!--end::Navigation-->--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <!--end::Dropdown-->--}}
                    {{--                        </div>--}}
                    <!--end::Item-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::List Widget 5-->
            </div>
        </div>
        <!--end::Row-->
        <!--end::Dashboard-->
    </div>

@endsection
