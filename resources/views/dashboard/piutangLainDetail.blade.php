<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">No Hutang Pelanggan</label>
        <div class="col-lg-9 col-form-label">
            : {{ $piutang_lain->pl_invoice }}
        </div>
    </div>
</div>
<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">Tanggal Hutang</label>
        <div class="col-lg-9 col-form-label">
            : {{ Main::format_date($piutang_lain->pl_tanggal) }}
        </div>
    </div>
</div>
<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">Tanggal Jatuh Tempo</label>
        <div class="col-lg-9 col-form-label">
            : {{ Main::format_date($piutang_lain->pl_jatuh_tempo) }}
        </div>
    </div>
</div>
<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">Jumlah Hutang</label>
        <div class="col-lg-9 col-form-label">
            : {{ Main::format_money($piutang_lain->pl_amount) }}
        </div>
    </div>
</div>
<div class="m-form__section m-form__section--first">
    <div class="form-group m-form__group row">
        <label class="col-lg-3 col-form-label">Keterangan</label>
        <div class="col-lg-9 col-form-label">
            : {{ $piutang_lain->pl_keterangan }}
        </div>
    </div>
</div>