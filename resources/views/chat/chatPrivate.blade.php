@extends('../general/index')

@section('js')
    <script src="{{asset('assets/demo/demo5_new/js/pages/custom/chat/chat.js')}}"></script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <link href="{{ asset('assets/demo/demo5_new/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('body')
    <input type="hidden" id="chat_list_url" data-list-url="{{route('chatList',$chat_private->chat_id)}}">
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Chat-->
            <div class="d-flex flex-row">
                <!--begin::Aside-->
                <div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px" id="kt_chat_aside">
                    <!--begin::Card-->
                    <div class="card card-custom">
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin:Search-->
                            <div class="input-group">
                                <a href="#" class="btn btn btn-secondary mr-2 font-weight-bolder font-size-sm"
                                   data-toggle="modal" data-target="#modal-create">
                                    <i class="la la-plus"></i>Start New Chat</a>
                            </div>
                            <!--end:Search-->
                            <!--begin:Users-->
                            <div class="mt-7 scroll scroll-pull">
                                <!--begin:User-->
                                @foreach($chat as $c)
                                    <div class="d-flex align-items-center justify-content-between mb-5">
                                        <div class="d-flex align-items-center">
                                            {{--                                        <div class="symbol symbol-circle symbol-50 mr-3">--}}
                                            {{--                                            <img alt="Pic" src="assets/media/users/300_12.jpg" />--}}
                                            {{--                                        </div>--}}
                                            @if($result[0] == 'client_id')
                                                <div class="d-flex flex-column">
                                                    <a href="{{route('chatBox', $c->chat_id)}}"
                                                       class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{$c->agency->agency_name}}</a>
                                                </div>
                                            @else
                                                <div class="d-flex flex-column">
                                                    <a href="{{route('chatBox', $c->chat_id)}}"
                                                       class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{$c->client->client_name}}</a>
                                                </div>
                                            @endif
                                        </div>
                                        {{--                                    <div class="d-flex flex-column align-items-end">--}}
                                        {{--                                        <span class="text-muted font-weight-bold font-size-sm">35 mins</span>--}}
                                        {{--                                    </div>--}}
                                    </div>
                            @endforeach
                            <!--begin:User-->
                            </div>
                            <!--end:Users-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
                    <!--begin::Card-->
                    <div class="card card-custom">
                        <!--begin::Header-->
                        <div class="card-header align-items-center px-4 py-3">
                            <div class="text-center flex-grow-1">
                                @if($result[0] == 'client_id')
                                    <div class="text-dark-75 font-weight-bold font-size-h5">{{$chat_private->agency->agency_name}}</div>
                                @else
                                    <div class="text-dark-75 font-weight-bold font-size-h5">{{$chat_private->client->client_name}}</div>
                                @endif
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Scroll-->
                            <div class="scroll scroll-pull" data-mobile-height="350">
                                <!--begin::Messages-->
                                <div class="messages">
                                @foreach($chat_data as $cd)
                                    @if($cd->from == $user->from)
                                        <!--begin::Message In-->
                                            <div class="d-flex flex-column mb-5 align-items-end">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <a class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                                                        <span class="text-muted font-size-sm">{{ Carbon\Carbon::parse($cd->created_at)->diffForHumans()}}</span>
                                                    </div>
                                                </div>
                                                <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
                                                    {{$cd->data_chat}}
                                                </div>
                                            </div>
                                            <!--end::Message In-->
                                    @else
                                        <!--begin::Message Out-->
                                            <div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <span class="text-muted font-size-sm">{{ Carbon\Carbon::parse($cd->created_at)->diffForHumans()}}</span>
                                                        @if($result[0] == 'client_id')
                                                            <a class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{$cd->chat->agency->agency_name}}</a>
                                                        @else
                                                            <a class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{$cd->chat->client->client_name}}</a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">
                                                    {{$cd->data_chat}}
                                                </div>
                                            </div>
                                            <!--end::Message Out-->
                                        @endif
                                    @endforeach
                                </div>
                                <!--end::Messages-->
                            </div>
                            <!--end::Scroll-->
                        </div>
                        <!--end::Body-->
                        <!--begin::Footer-->
                        <div class="card-footer align-items-center">
                            <form action="{{ route('chatSend', ['id'=>$chat_private->chat_id]) }}"
                                  method="post"
                                  class="form-send"
                                  data-redirect="{{ route('chatBox', ['id'=>$chat_private->chat_id]) }}"
                                  data-alert-show="true"
                                  data-alert-field-message="true"
                                  style="width: 100%">

                            {{ csrf_field() }}
                            <!--begin::Compose-->
                                <input type="hidden" name="from" value="{{$user->from}}">
                                <input type="hidden" name="status" value="belum">
                                <input type="hidden" name="to" value="{{$to}}">
                                <textarea class="form-control" rows="2" placeholder="Type a message" name="data_chat"></textarea>
                                <div class="d-flex align-items-center justify-content-between mt-5">
                                    <div class="mr-3">
                                    </div>
                                    <div>
                                        <button type="submit"
                                                class="btn btn-primary btn-md font-weight-bold">
                                            Send
                                        </button>
                                    </div>
                                </div>
                                <!--end::Compose-->
                            </form>
                        </div>
                        <!--end::Footer-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Chat-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->


@endsection
