<form
        action="{{ route('chatInsert') }}"
        method="post"
        class="form-send"
        autocomplete="off"
        data-alert-show="true"
        data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:50%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">Client </label>
                                <select name="client_id" class="form-control m-select2" style="width: 100%">
                                    <option value="">Select Agency</option>
                                    @foreach($client as $r)
                                        <option value="{{ $r->client_id }}">{{ $r->client_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="agency_id" value="{{$result[1]}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>