@extends('../general/index')
@section('js')
{{--    <script src="{{ asset('js/support.js') }}" type="text/javascript"></script>--}}

@endsection

@section('body')
<div style="width: 100%">
    <div class="d-flex flex-row-fluid bgi-size-cover bgi-position-top"
         style="background-image: url({{asset('assets/demo/demo5_new/media/bg/bg-9.jpg')}})">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center pt-25 pb-35">
                <h3 class="font-weight-bolder text-dark mb-0">Frequently Asked Questions</h3>
            </div>
        </div>
    </div>
    <div class="container mt-n15">
        <!--begin::Card-->
        <div class="card mb-8">
            <!--begin::Body-->
            <div class="card-body p-10" style="min-height: 300px">
                <!--begin::Row-->
                <div class="row">
                    <div class="col-lg-3">
                        <!--begin::Navigation-->
                        <ul class="navi navi-link-rounded navi-accent navi-hover navi-active nav flex-column mb-8 mb-lg-0"
                            role="tablist">
                            <!--begin::Nav Item-->
                            @foreach($support as $key=>$s)
                                <li class="navi-item mb-2">
                                    <a class="navi-link {{ $id == $s->support_id ? 'active':'' }}"
                                       href="{{route('supportCenter', ['id' => $s->support_id])}}">
                                        <span class="navi-text text-dark-50 font-size-h5 font-weight-bold">{{$s->support_title}}</span>
                                    </a>
                                </li>
                        @endforeach
                        <!--end::Nav Item-->
                        </ul>
                        <!--end::Navigation-->
                    </div>
                    <div class="col-lg-9">
                        <!--begin::Tab Content-->
                        <div class="tab-content">
                            <!--begin::Accordion-->
                            <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle"
                                 id="faq">
                                <!--begin::Item-->
{{--                                <input type="hidden" id="support_data" value="{{$support_data}}">--}}
                                @foreach($support_data as $k => $sd)
                                    <div class="card">
                                        <div class="card-header" id="faqHeading{{$k}}">
                                            <a class="card-title text-dark {{$k == 0 ? '' : 'collapsed'}}"  data-toggle="collapse" href="#faq{{$k}}"
                                               aria-expanded="true" aria-controls="faq1" role="button">
																<span class="svg-icon svg-icon-primary">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<polygon
                                                                                    points="0 0 24 0 24 24 0 24"></polygon>
																			<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                                                                  fill="#000000"
                                                                                  fill-rule="nonzero"></path>
																			<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                                                                  fill="#000000" fill-rule="nonzero"
                                                                                  opacity="0.3"
                                                                                  transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                                <div class="card-label text-dark pl-4">{{$sd->support_data}}</div>
                                            </a>
                                        </div>
                                        <div id="faq{{$k}}" class="collapse {{$k == 0 ? 'show' : ''}}" aria-labelledby="faqHeading1"
                                             data-parent="#faq">
                                            <div class="card-body text-dark-50 font-size-lg pl-12" >
                                                <span style="white-space: pre-line" id="data_support_{{$k}}">{!! $sd->support_desc !!}</span>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            <!--end::Item-->
                            </div>
                            <!--end::Accordion-->
                        </div>
                        <!--end::Tab Content-->
                    </div>
                </div>
                <!--end::Row-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Item-->
    </div>
    </div>
@endsection