@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/input-mask.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    <div class="container">
        <input type="hidden" id="list_url" data-list-url="{{route('reportList', $params)}}">
        <div id="table_coloumn" style="display: none">{{$view}} </div>
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-custom gutter-b card-shadowless bg-white">
            <div class="card-header border-0 py-5">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <i class="la la-gear"></i> Filter Data
                        </h3>
                    </div>
                </div>
                <br>
            </div>
            <form method="get" class="m-form m-form--fit m-form--label-align-right">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="row">
                                <div class="col-lg-9">
                                    <label for="client">Client</label>
                                    <div class="input-daterange input-group">
                                        <select multiple class="form-control m-input selectpicker" name="client_id[]">
                                            @if(empty($client_id)||$client_id == null)
                                                <option value="0" selected>All Client</option>
                                            @else
                                                @if(in_array(0, $client_id))
                                                    <option value="0" selected>All Client</option>
                                                @else
                                                    <option value="0">All Client</option>
                                                @endif
                                            @endif
                                            @foreach($client as $c)
                                                @if(empty($client_id))
                                                    <option value="{{$c->client_id}}">{{$c->client_name}}</option>
                                                @else
                                                    @if(in_array($c->client_id, $client_id))
                                                        <option value="{{$c->client_id}}"
                                                                selected>{{$c->client_name}}</option>
                                                    @else
                                                        <option value="{{$c->client_id}}">{{$c->client_name}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-9">
                                    <label for="client">Client Type</label>
                                    <div class="input-daterange input-group">
                                        <select multiple class="form-control m-input selectpicker"
                                                name="client_tye_id[]">
                                            @if(empty($client_type_id)||$client_type_id == null)
                                                <option value="0" selected>All Client Type</option>
                                            @else
                                                @if(in_array(0, $client_type_id))
                                                    <option value="0" selected>All Client Type</option>
                                                @else
                                                    <option value="0">All Client Type</option>
                                                @endif
                                            @endif
                                            @foreach($client_type as $ct)
                                                @if(empty($client_type_id))
                                                    <option value="{{$ct->client_type_id}}">{{$ct->client_type_name}}</option>
                                                @else
                                                    @if(in_array($ct->client_type_id, $client_type_id))
                                                        <option value="{{$ct->client_type_id}}"
                                                                selected>{{$ct->client_type_name}}</option>
                                                    @else
                                                        <option value="{{$ct->client_type_id}}">{{$ct->client_type_name}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="row">
                                <div class="col-lg-9">
                                    <label for="client">Client Status</label>
                                    <div class="input-daterange input-group">
                                        <select multiple class="form-control m-input selectpicker"
                                                name="client_status_id[]">
                                            @if(empty($client_status_id)||$client_status_id == null)
                                                <option value="0" selected>All Client Status</option>
                                            @else
                                                @if(in_array(0, $client_status_id))
                                                    <option value="0" selected>All Client Status</option>
                                                @else
                                                    <option value="0">All Client Status</option>
                                                @endif
                                            @endif
                                            @foreach($client_status as $cs)
                                                @if(empty($client_status_id))
                                                    <option value="{{$cs->client_status_id}}">{{$cs->client_status_name}}</option>
                                                @else
                                                    @if(in_array($cs->client_status_id, $client_status_id))
                                                        <option value="{{$cs->client_status_id}}"
                                                                selected>{{$cs->client_status_name}}</option>
                                                    @else
                                                        <option value="{{$cs->client_status_id}}">{{$cs->client_status_name}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="m-portlet__foot text-center">
                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                        <button type="submit" class="btn btn-warning akses-filter">
                            <i class="la la-search"></i> Filter Data
                        </button>
                        <a
                                href="{{ route('reportPdf', $params) }}"
                                class="btn btn-danger akses-pdf" target="_blank">
                            <i class="la la-file-pdf-o"></i> Print PDF
                        </a>
                        <a
                                href="{{ route('reportExcel', $params) }}"
                                class="btn btn-success akses-excel" target="_blank">
                            <i class="la la-file-excel-o"></i> Print Excel
                        </a>
                    </div>
                </div>
            </form>
            <br>
        </div>
        <div class="card card-custom  gutter-b card-shadowless bg-white">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
            </div>
            <div class="card-body py-0">
                <div class="table-responsive">
                <table class="datatable table table-striped- table-bordered table-hover table-checkable
                            datatable-general">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Client Name</th>
                        <th>Client Contact</th>
                        <th>Client Email</th>
                        <th>Client Type</th>
                        <th>Client Status</th>
                        <th>Client Description</th>
                        <th>Client Data</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                    <br>
            </div>
        </div>
    </div>
@endsection
