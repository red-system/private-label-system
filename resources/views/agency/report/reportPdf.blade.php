
<link rel="stylesheet" type="text/css" href="{{ base_path() }}/public/css/invoice.css">
<style type="text/css">
    .item {
        font-size: 12px;
        font-weight: normal;
    }
    tr.noBorder td {
        border: 0 !important;
    }
</style>

<head style="text-align: center">
    <h4 style="text-align: center">My Client Report</h4>
</head>
<div id="invoice-bot">
    <br><br><br>
    <div id="table">
            <table>
                <thead>
                <tr class="tabletitle">
                    <th class="item" width="20">No</th>
                    <th class="item">Client Name</th>
                    <th class="item">Client Contact</th>
                    <th class="item">Client Email</th>
                    <th class="item">Client Type</th>
                    <th class="item">Client Status</th>
                    <th class="item">Commision</th>
                    <th class="item">Fixed Cost</th>
                    <th class="item">Client Description</th>
                    <th class="item">Client Data</th>
                </tr>
                </thead>
                <tbody>
                @php($no = 1)
                @foreach($list as $r)
                        <tr>
                            <td class="item">{{ $no++ }}</td>
                            <td class="item">{{ $r->client_name }}</td>
                            <td class="item">{{$r->client_contact }}</td>
                            <td class="item">{{$r->client_email }}</td>
                            <td class="item">{{$r->client_type_name }}</td>
                            <td class="item">{{$r->client_status_name }}</td>
                            <td class="item">{{$r->commisions }}</td>
                            <td class="item">{{$r->fixed_cost }}</td>
                            <td class="item">{{$r->client_desc }}</td>
                            <td class="item">{{$r->client_data }}</td>
                        </tr>
                @endforeach
                </tbody>
            </table>
    </div>
</div>