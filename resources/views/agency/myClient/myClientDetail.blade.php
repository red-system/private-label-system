<div class="modal" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Client Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Client Nmae</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="kode_package_edit" name="client_name"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Client Email</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="nama_package_edit" name="client_email"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Client Contact</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="client_contact"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Client Type</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="client_type_name"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Client Status</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="client_status_name"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Commision</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="commisions"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Fixed Cost</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="fixed_cost"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">CLient Description</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="client_desc"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Client Data</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="harga_package_edit" name="client_data"></label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
