<form action=""
      method="post"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-commision-cost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Commision and Cost</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Commsision </label>
                            <input type="text" class="form-control m-input input-numeral" name="commisions">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Fixed Cost </label>
                            <input type="text" class="form-control m-input input-numeral" name="fixed_cost">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
