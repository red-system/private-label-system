<form action=""
      method="post"
      data-alert-show="true"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Row</label>
                            <select name="row" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Row</option>
                                @foreach($row as $r)
                                    <option value="{{ $r }}">{{ $r }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Block</label>
                            <select name="block" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Block</option>
                                @foreach($block as $r)
                                    <option value="{{ $r }}">{{ $r }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Background Color </label>
                            <div class="custom-file">
                                <input class="form-control" type="color" name="bc">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Title </label>
                            <input type="text" class="form-control m-input" name="judul">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Title Color </label>
                            <div class="custom-file">
                                <input class="form-control" type="color" name="judul_bc">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Content </label>
                            {{--                            <input type="text" class="form-control m-input" name="isi">--}}
                            <textarea class="form-control m-input" name="isi"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Content Color </label>
                            <div class="custom-file">
                                <input class="form-control" type="color" name="isi_bc">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Svg Image </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="gambar">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Button </label>
                            <div class="m-checkbox-inline">
                                <input id="button-content-edit" type="checkbox" name="button" value="true">
                                <label>Activate</label>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group hilang-edit" hidden>
                            <label class="form-control-label">Button Color </label>
                            <div class="custom-file">
                                <input class="form-control" type="color" name="button_color">
                            </div>
                        </div>
                        <div class="form-group m-form__group hilang-edit" hidden>
                            <label class="form-control-label">Button Text </label>
                            <div class="custom-file">
                                <input type="text" class="form-control m-input" name="button_text">
                            </div>
                        </div>
                        <div class="form-group m-form__group hilang-edit" hidden>
                            <label class="form-control-label">Button Text Color </label>
                            <div class="custom-file">
                                <input class="form-control" type="color" name="button_text_bc">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
