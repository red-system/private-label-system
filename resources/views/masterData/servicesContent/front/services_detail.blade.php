@extends('../general/index')

@section('js')

    {{--    <script type="text/javascript">--}}
    {{--        jQuery(document).on("click", "#show-more", function (e) {--}}

    {{--            var text = jQuery(this).data("text");--}}
    {{--            $('#text-detail').html(text);--}}
    {{--            $('#modal-detail').modal('show');--}}
    {{--        });--}}
    {{--    </script>--}}
    <script type="text/javascript">
        jQuery(document).on("click", "#show-more", function (e) {
            e.preventDefault();
            var text = jQuery(this).data("text").replace(/\\/g, '');

            var modalBox = "<div id='text-modal' class='inner-modal'>";
            modalBox += "<img class='svg close-modal' src='https://viraloctopus.com/wp-content/themes/vo/img/close-modal.svg' alt='Close Modal' title='Close Modal' onclick='closeModal()' >";

            modalBox += "<div class='text-wrapper'> ";
            modalBox += text;
            modalBox += "</div> ";
            modalBox += "</div> ";


            jQuery("html, body").animate({scrollTop: 0}, "fast");
            jQuery("#modal-container").html(modalBox).show();
            jQuery("#overlay").show();
        });

        function closeModal() {
            jQuery("#modal-container").hide();
            jQuery("#overlay").hide();
            jQuery('#overlay').removeClass('black');
        };

    </script>


@endsection

@section('css')
    {{--    <link rel="stylesheet" id="vocustomshop_css-css"--}}
    {{--          href="{{ asset('assets/services/css/vocustomshop.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="stickylist-css"--}}
    {{--          href="{{ asset('assets/services/css/sticky-list_styles.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="wp-block-library-css"--}}
    {{--          href="{{ asset('assets/services/css/style.min.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="auth0-widget-css"--}}
    {{--          href="{{ asset('assets/services/css/main.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="cookie-notice-front-css"--}}
    {{--          href="{{ asset('assets/services/css/front.min.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    <link rel="stylesheet" id="vo-style-css"
          href="{{ asset('assets/services/css/style.css')}}"
          type="text/css" media="all">
    {{--    <link rel="stylesheet" id="bootstrap-style-css"--}}
    {{--          href="{{ asset('assets/services/css/bootstrap.min.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="social-share-buttons-css"--}}
    {{--          href="{{ asset('assets/services/css/social-share-buttons.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="comments-css"--}}
    {{--          href="{{ asset('assets/services/css/comments.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="filter-menu-css"--}}
    {{--          href="{{ asset('assets/services/css/filter-menu.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="quiz-css"--}}
    {{--          href="{{ asset('assets/services/css/quiz.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="slick-css"--}}
    {{--          href="{{ asset('assets/services/css/slick.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="flex-grid-css"--}}
    {{--          href="{{ asset('assets/services/css/flex-grid.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="vo-v2-css"--}}
    {{--          href="{{ asset('assets/services/css/vo-v2.css')}}"--}}
    {{--          type="text/css" media="all">--}}
    {{--    <link rel="stylesheet" id="gravity-restyle-css"--}}
    {{--          href="{{ asset('assets/services/css/gravity-restyle.css')}}"--}}
    {{--          type="text/css" media="all">--}}

@endsection

@section('body')

    <div class="container">
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <!--begin::Info-->
                        <div class="d-flex align-items-center flex-wrap mr-1">
                            <!--begin::Page Heading-->
                            <div class="d-flex align-items-baseline mr-5">
                                <!--begin::Page Title-->
                                <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                                    {{ $pageTitle }}
                                </h3>
                                {!! $breadcrumb !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {{--        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">--}}
    <!--begin::Subheader-->

        <div id="gig-detail-first" class="card card-custom gutter-b card-shadowless bg-white">
            <div class="container">
                <div class="wrapper">
                    <img class="image img-responsive"
                         src="{{'http://dev-storage.redsystem.id/private-label-system/'.$services->gambar_detail}}"
                         align="right">

                    <h3 class="title">{{$services->judul}}
                    </h3>
                    <p style="font-family: Roboto; font-size: 14px; width: 40%">{!!$services->isi!!}
                    </p>
                </div>

            </div>


        </div>

        @if($content)

        <div id="gig-detail-third" class="gig-detail gig-cat-56">
            <div class="container">
                <div class="row">
                    @foreach($content as $k)
                        <div class="box">
                            <div class="wrapper">
                                <h3 class="title">{!! $k['judul'] !!}</h3>
                                <p>
                                    {{$k['isi_label']}}
                                    <br><br>
                                    @if($k['panjang'] > 490)
                                        <a id="show-more" class="btn btn-warning font-weight-bolder font-size-sm"
                                           data-text="{!! $k['isi'] !!}">Show More →</a>
                                    @endif
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        @endif

        <div class="card card-custom gutter-b card-shadowless bg-white">
            <div class="container">
                <div class="left">
                    <div class="card-body p-10" style="min-height: 300px">
                        <!--begin::Row-->
                        <div class="row">
                            <div class="col-lg-3">
                                <!--begin::Navigation-->
                                <ul class="navi navi-link-rounded navi-accent navi-hover navi-active nav flex-column mb-8 mb-lg-0"
                                    role="tablist">
                                    <!--begin::Nav Item-->
                                    <li class="navi-item mb-2">
                                                <span class="navi-text text-dark-50 font-size-h5 font-weight-bold">HAVE ANY QUESTIONS?<br>
                                                See FAQs</span>
                                    </li>
                                    <!--end::Nav Item-->
                                </ul>
                                <!--end::Navigation-->
                            </div>
                            <div class="col-lg-9">
                                <!--begin::Tab Content-->
                                <div class="tab-content">
                                    <!--begin::Accordion-->
                                    <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle"
                                         id="faq">
                                        <!--begin::Item-->
                                        @foreach($faq as $k => $sd)
                                            <div class="card">
                                                <div class="card-header" id="faqHeading{{$k}}">
                                                    <a class="card-title text-dark {{$k == 0 ? '' : 'collapsed'}}"
                                                       data-toggle="collapse" href="#faq{{$k}}"
                                                       aria-expanded="true" aria-controls="faq1" role="button">
																<span class="svg-icon svg-icon-primary">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<polygon
                                                                                    points="0 0 24 0 24 24 0 24"></polygon>
																			<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                                                                  fill="#000000"
                                                                                  fill-rule="nonzero"></path>
																			<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                                                                  fill="#000000" fill-rule="nonzero"
                                                                                  opacity="0.3"
                                                                                  transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                                        <div class="card-label text-dark pl-4">{{$sd->support_data}}</div>
                                                    </a>
                                                </div>
                                                <div id="faq{{$k}}" class="collapse {{$k == 0 ? 'show' : ''}}"
                                                     aria-labelledby="faqHeading1"
                                                     data-parent="#faq">
                                                    <div class="card-body text-dark-50 font-size-lg pl-12">
                                                        <span style="white-space: pre-line"
                                                              id="data_support_{{$k}}">{!! $sd->support_desc !!}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Entry-->
    </div>
    {{--    </div>--}}

    <div id="modal-detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p id="text-detail"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="overlay" style="display: none;" onclick=""></div>
    <div id="modal-container" style="display: none;">
        <div id="text-modal" class="inner-modal">
            <div class="text-wrapper">
            </div>
        </div>
    </div>



@endsection
