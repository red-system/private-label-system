@extends('../general/index')

@section('js')
    {{--        <script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/demo5_new/js/pages/widgets.js') }}"></script>
    <script src="{{ asset('assets/demo/demo5_new/plugins/global/plugins.bundle.js')}}"></script>
    <script src="{{ asset('assets/demo/demo5_new/js/scripts.bundle.js')}}"></script>
    <script src="{{ asset('assets/demo/demo5_new/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>

@endsection

@section('css')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    {{--    <link href="{{ asset('assets/demo/demo5_new/plugins/custom/fullcalendar/ful')}}lcalendar.bundle.css" rel="stylesheet" type="text/css"/>--}}
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->

    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/demo/demo5_new/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('body')
    <div class="container">
    {{--        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">--}}
    <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <!--begin::Info-->
                        <div class="d-flex align-items-center flex-wrap mr-1">
                            <!--begin::Page Heading-->
                            <div class="d-flex align-items-baseline mr-5">
                                <!--begin::Page Title-->
                                <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                                    {{ $pageTitle }}
                                </h3>
                                {!! $breadcrumb !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Row-->
                @foreach($content as $cont)
                    <div class="row">
                        @foreach($cont as $services)
                            <div class="col-xl-3">
                                <!--begin::Engage Widget 2-->
                                <div class="card card-custom card-stretch gutter-b">
                                    <div class="card-body d-flex p-0">
                                        <div class="flex-grow-1 bg-danger p-8 card-rounded flex-grow-1 bgi-no-repeat"
                                             style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{'http://dev-storage.redsystem.id/private-label-system/'.$services->gambar}})">
                                            <h4 class="text-inverse-danger mt-2 font-weight-bolder" style="color: {{$services->judul_bc}}">{{$services->judul}}</h4>
                                            <p class="text-inverse-danger my-6" style="color: {{$services->isi_bc}}">{{ str_limit($services->isi, 100) }}</p>
                                            @if($services->button == 1)
                                                <a href="{{route('servicesDetail', Main::encrypt($services->content_id))}}" class="btn font-weight-bold py-2 px-6" style="background-color: {{$services->button_color}}; color: {{$services->button_text_bc}}">{{$services->button_text}}</a>
                                            @endif
                                        </div>
{{--                                        <div class="flex-grow-1 bg-danger p-8 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('assets/demo/demo5_new/media/svg/humans/custom-3.svg')}})">--}}
{{--                                            <h4 class="text-inverse-danger mt-2 font-weight-bolder">User Confidence</h4>--}}
{{--                                            <p class="text-inverse-danger my-6">Boost marketing &amp; sales--}}
{{--                                                <br>through product confidence.</p>--}}
{{--                                            <a href="#" class="btn btn-warning font-weight-bold py-2 px-6">Learn</a>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                                <!--end::Engage Widget 2-->
                            </div>
                        @endforeach
                    </div>
            @endforeach
            <!--end::Row-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        {{--        </div>--}}
    </div>

@endsection
