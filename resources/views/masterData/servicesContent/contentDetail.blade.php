<form action=""
      method="post"
      data-alert-show="true"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-view-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Image </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="gambar_detail">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                        @php($no = 1)
                        @foreach($count as $index)
                            <div class="form-group m-form__group">
                                <label class="form-control-label">Text Title - {{$no}} </label>
                                <div class="custom-file">
                                    <input type="text" class="form-control m-input" name="text_title[{{$index}}]">
                                </div>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="form-control-label">Text Detail - {{$no++}} </label>
                                <textarea class="form-control m-input" name="text_detail[{{$index}}]"></textarea>
                            </div>
                        @endforeach
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">FAQ</label>
                            <select name="faq_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select FAQ</option>
                                @foreach($faq as $r)
                                    <option value="{{ $r->support_id }}">{{ $r->support_title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
