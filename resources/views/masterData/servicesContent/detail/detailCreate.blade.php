<form action="{{ route('servicesInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Service Name </label>
                            <input type="text" class="form-control m-input" name="services_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Channel</label>
                            <select name="channel_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Channel</option>
                                @foreach($channel as $r)
                                    <option value="{{ $r->channel_id }}">{{ $r->channel_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Service Fee </label>
                            <input type="text" class="form-control m-input input-numeral" name="services_fee">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Service Cost </label>
                            <input type="text" class="form-control m-input input-numeral" name="services_cost">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Service Data </label>
                            <input type="text" class="form-control m-input" name="services_data">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
