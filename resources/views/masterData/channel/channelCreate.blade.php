<form action="{{ route('channelInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Channel Name </label>
                            <input type="text" class="form-control m-input" name="channel_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Channel Fee </label>
                            <input type="text" class="form-control m-input input-numeral" name="channel_fee">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Channel Cost </label>
                            <input type="text" class="form-control m-input input-numeral" name="channel_cost">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Channel Data </label>
                            <input type="text" class="form-control m-input" name="channel_data">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
