<form action="{{ route('companyInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Name </label>
                            <input type="text" class="form-control m-input" name="company_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Email </label>
                            <input type="email" class="form-control m-input" name="company_email">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Phone </label>
                            <input type="text" class="form-control m-input" name="company_phone">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client </label>
                            <select name="client_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Client</option>
                                @foreach($client as $r)
                                    <option value="{{ $r->client_id }}">{{ $r->client_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Type</label>
                            <select name="company_type_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Company Type</option>
                                @foreach($company_type as $r)
                                    <option value="{{ $r->company_type_id }}">{{ $r->company_type_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Address </label>
                            <textarea class="form-control m-input" name="company_address"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Company Overview </label>
                            <textarea class="form-control m-input" name="company_overview"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Company Data </label>
                            <input type="text" class="form-control m-input" name="company_data">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
