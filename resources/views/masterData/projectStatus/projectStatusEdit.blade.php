<form action=""
      method="post"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status Name </label>
                            <input type="text" class="form-control m-input" name="project_status_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status Text Color </label>
                            <input class="form-control" type="color" value="#ffffff" name="project_status_text_color">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status Background Color </label>
                            <input class="form-control" type="color" value="#000000" name="project_status_bc_color">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Project Status Description </label>
                            <textarea class="form-control m-input" name="project_status_desc"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
