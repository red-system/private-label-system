<form
        action="{{ route('projectStatusInsert') }}"
        method="post"
        class="m-form form-send"
        autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status Name </label>
                            <input type="text" class="form-control m-input" name="project_status_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status Text Color </label>
{{--                            <select name="project_status_label" class="form-control m-select2" style="width: 100%">--}}
{{--                                <option value="">Select Label Color</option>--}}
{{--                                <option style="background-color: #C9F7F5" value="label-light-success">Blue</option>--}}
{{--                                <option style="background-color: #EEE5FF" value="label-light-info">Purple</option>--}}
{{--                                <option style="background-color: #FFF4DE" value="label-light-warning">Yellow</option>--}}
{{--                                <option style="background-color: #FFE2E5" value="label-light-danger">Red</option>--}}
{{--                            </select>--}}
                            <input class="form-control" type="color" value="#ffffff" name="project_status_text_color">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Project Status Background Color </label>
                            {{--                            <select name="project_status_label" class="form-control m-select2" style="width: 100%">--}}
                            {{--                                <option value="">Select Label Color</option>--}}
                            {{--                                <option style="background-color: #C9F7F5" value="label-light-success">Blue</option>--}}
                            {{--                                <option style="background-color: #EEE5FF" value="label-light-info">Purple</option>--}}
                            {{--                                <option style="background-color: #FFF4DE" value="label-light-warning">Yellow</option>--}}
                            {{--                                <option style="background-color: #FFE2E5" value="label-light-danger">Red</option>--}}
                            {{--                            </select>--}}
                            <input class="form-control" type="color" value="#000000" name="project_status_bc_color">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Project Status Description </label>
                            <textarea class="form-control m-input" name="project_status_desc"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
