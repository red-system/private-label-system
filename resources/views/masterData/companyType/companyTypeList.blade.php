@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>

@endsection

@section('body')
    @include('masterData/companyType/companyTypeCreate')
    @include('masterData/companyType/companyTypeEdit')

    <div class="container">
        <input type="hidden" id="list_url" data-list-url="{{route('companyTypeList')}}">
        <div id="table_coloumn" style="display: none">{{$view}}</div>
        <div id="action_list" style="display: none">{{$actionButton}}</div>
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-custom gutter-b card-shadowless bg-white">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title align-items-start flex-column">
                    {{--                            <span class="card-label font-weight-bolder text-dark">Agents Stats</span>--}}
                </h3>
                <div class="card-toolbar">
                    <a href="#" class="btn btn-success font-weight-bolder font-size-sm"
                       data-toggle="modal" data-target="#modal-create">
                        <i class="la la-plus"></i>Add Data</a>
                </div>
            </div>
            <div class="card-body py-0">
                <div class="table-responsive">
                    <table
                            class="datatable table table-striped- table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Company Type Name</th>
                            <th>Company Type Description</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection
