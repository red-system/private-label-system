<form action="{{ route('clientInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Agency</label>
                            <select name="agency_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Agency</option>
                                @foreach($agency as $a)
                                    <option value="{{ $a->agency_id }}">{{ $a->agency_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Name </label>
                            <input type="text" class="form-control m-input" name="client_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Contact </label>
                            <input type="text" class="form-control m-input" name="client_contact">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Email </label>
                            <input type="text" class="form-control m-input" name="client_email">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Type</label>
                            <select name="client_type_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Client Type</option>
                                @foreach($client_type as $r)
                                    <option value="{{ $r->client_type_id }}">{{ $r->client_type_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Status </label>
                            <select name="client_status_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Client Status</option>
                                @foreach($client_status as $r)
                                    <option value="{{ $r->client_status_id }}">{{ $r->client_status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Description </label>
                            <textarea class="form-control m-input" name="client_desc"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Data </label>
                            <input type="text" class="form-control m-input" name="client_data">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
