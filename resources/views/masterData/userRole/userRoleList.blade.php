@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/role_user.js') }}" type="text/javascript"></script>
@endsection

@section('body')
{{--    @include('masterData/userRole/userRoleCreate')--}}
    @include('masterData/userRole/modalEdit')

<div class="container">
    <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
    </div>

    <div class="card card-custom gutter-b card-shadowless bg-white">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
{{--            <h3 class="card-title align-items-start flex-column">--}}
{{--                --}}{{--                            <span class="card-label font-weight-bolder text-dark">Agents Stats</span>--}}
{{--            </h3>--}}
{{--            <div class="card-toolbar">--}}
{{--                <a href="#" class="btn btn-success font-weight-bolder font-size-sm"--}}
{{--                   data-toggle="modal" data-target="#modal-create">--}}
{{--                    <i class="la la-plus"></i>Add Data</a>--}}
{{--            </div>--}}
        </div>
        <div class="card-body py-0">
            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatable-no-pagination">

                    <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Role Name</th>
                            <th>User Role Data</th>
                            <th width="150">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $r)
                            <tr>
                                <td align="center">{{ $no++ }}.</td>
                                <td>{{ $r->user_role_name }}</td>
                                <td>{{ $r->user_role_data }}</td>
                                <td align="center">
                                    <textarea class="row-data hidden">@json($r)</textarea>
                                    <div class="btn-group m-btn-group m-btn-group--pill btn-group-sm">
                                        <a class="m-btn btn btn-primary akses-menu_akses"
                                           href="{{ route('userRoleAkses', ['id'=>Main::encrypt($r->user_role_id)]) }}">
                                            <i class="la la-key"></i> Menu Akses
                                        </a>
                                        <button type="button"
                                                class="m-btn btn btn-success btn-edit-akunting akses-edit"
                                                href="{{ route('userRoleEdit', ['id'=>Main::encrypt($r->user_role_id)]) }}">
                                            <i class="la la-edit"></i> Edit
                                        </button>
{{--                                        <button type="button"--}}
{{--                                                class="m-btn btn btn-danger btn-hapus akses-delete"--}}
{{--                                                data-route='{{ route('userRoleDelete', ['id'=>$r->user_role_id]) }}'>--}}
{{--                                            <i class="la la-remove"></i> Hapus--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                <br>
                <div class="m-portlet m-portlet--mobile hidden">

                    <div class="m-portlet__body">

                        <table class="table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Tanggal</th>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>No Seri Produk</th>
                                <th>Qty In</th>
                                <th>Qty Out</th>
                                <th>Gudang</th>
                                <th>Penerima/Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection