<form action="{{ route('userRoleUpdate', ['id'=>Main::encrypt($user_role_id)]) }}"
      method="post"
      class="m-form form-send"
      enctype="multipart/form-data"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}

    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first">
            <div class="form-group m-form__group">
                <label class="form-control-label required">Role Name </label>
                <input type="text" class="form-control m-input" name="user_role_name"
                       autofocus value="{{$list->user_role_name}}">
            </div>
            <div class="form-group m-form__group">
                <label class="form-control-label">User Role Data </label>
                <textarea class="form-control m-input" name="user_role_data">{{$list->user_role_data}}</textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="margin: 30px -20px 20px;">
        <button type="submit" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
</form>