<form action="{{ route('agencyInsert') }}" method="post" class="m-form form-send" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Agency Name </label>
                            <input type="text" class="form-control m-input" name="agency_name">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Agency Type</label>
                            <select name="agency_type_id" class="form-control m-select2" style="width: 100%;opacity: 1;">
                                <option value="">Select Agency Type</option>
                                @foreach($agency_type as $r)
                                    <option value="{{ $r->agency_type_id }}">{{ $r->agency_type_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Email
                                Address</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                class="la la-at"></i></span></div>
                                <input type="text"
                                       class="form-control m-input"
                                       name="agency_email" value="" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Agency Data </label>
                            <input type="text" class="form-control m-input" name="agency_data">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Agency Description </label>
                            <textarea class="form-control m-input" name="agency_desc"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
