<style type="text/css">
    .table-data {
        border-left: 0.01em solid #ccc;
        border-right: 0;
        border-top: 0.01em solid #ccc;
        border-bottom: 0;
        border-collapse: collapse;
        font-family: Roboto;
    }

    .table-data td,
    .table-data th {
        border-left: 0;
        border-right: 0.01em solid #ccc;
        border-top: 0;
        border-bottom: 0.01em solid #ccc;
        padding: 2px 4px;
        font-size: 12px;
    }
    .alignMe p {
        display: inline-block;
        width: 15%;
        position: relative;
        padding-right: 10px; /* Ensures colon does not overlay the text */
    }

    .alignMe p::after {
        content: ":";
        position: absolute;
        right: 15px;
    }
    ul {
        line-height:0.1;
        list-style: none;
        display: table;
        font-size: 12px;
        font-family: Roboto;
    }
</style>

<ul class="alignMe">
    <li><p>Status</p> {{ $list->project_status->project_status_name }}</li>
    <li><p>Order Date</p> {{ Main::format_date($list->project_order_date) }}</li>
    <li><p>Order Title</p> {{ $list->project_order_title }}</li>
</ul>

<br>
<br>
<table style=" margin-top: 8px; margin-bottom: 10px; width: 100%" class="table-data">
    <thead>
    <tr>
        <td width="15%">Completed Task</td>
        <td width="60%">Description</td>
    </tr>
    </thead>
    <tbody>
    @if(!empty($completed))

        @foreach($completed as $c)
        <tr>
            <td width="15%" style="text-align: left;">{{$c->tasks_name}}</td>
            <td width="60%" style="text-align: left;">{{ $c->tasks_desc }}</td>
        </tr>
    @endforeach
    @else
        <tr>
            <td width="60%" colspan="2" style="text-align: center;">There is no data</td>
        </tr>
    @endif
    </tbody>
</table>

<br>
<label style="font-family: Roboto; font-size: 12px">What we have Planned for next Month</label>
<table style=" margin-top: 8px; margin-bottom: 10px; width: 100%" class="table-data">
    <thead>
    <tr>
        <td width="15%">Next Task</td>
        <td width="60%">Description</td>
    </tr>
    </thead>
    <tbody>
    @if(!empty($next_tasks))

        @foreach($next_tasks as $next)
        <tr>
            <td width="15%" style="text-align: left;">{{$next->tasks_name}}</td>
            <td width="60%" style="text-align: left;">{{ $next->tasks_desc }}</td>
        </tr>
    @endforeach
        @else
        <tr>
            <td width="60%" colspan="2" style="text-align: center;">There is no data</td>
        </tr>
        @endif
    </tbody>
</table>
@if(!empty($file))
<br>
<table style=" margin-top: 8px; margin-bottom: 10px; width: 60%" class="table-data">
    <tbody>
    @foreach($file as $f)
        <tr>
            <td width="50%" style="text-align: left;">{{$f->file_name}}</td>
            <td width="5%" style="text-align: left;"><button></button><a href="{{$f->file_link}}" target="_blank">View</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
    @endif