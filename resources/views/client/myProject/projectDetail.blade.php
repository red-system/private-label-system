<div class="modal" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Project Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Order ID</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" name="project_order_number"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Channel</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8" id="nama_package_edit" name="channel_name"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Order Title</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit" name="project_order_title"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Project Type</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit" name="project_type_name"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Order Date</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit" name="project_order_date"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Project Timeline</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit" name="project_timeline"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Status</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit" name="project_status_name"></label>
                </div>
                <div class="form-group m-form__group row">
                    <label class="form-control-label col-3">Progress</label>
                    <label class="form-control-label col-1">:</label>
                    <label class="form-control-label col-8"
                           id="harga_package_edit" name="project_progress"></label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
