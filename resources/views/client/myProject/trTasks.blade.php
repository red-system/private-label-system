<table class="row-tasks m--hide">
    <tbody>
    <tr data-index="">
        <td class="td-task-name">
            <input class="form-control m-input nama-bank" type="text" name="tasks_name[]" style="width: 250px">
        </td>
        <td class="td-tasks-desc">
            <textarea class="form-control m-input" name="tasks_desc[]" style="width: 300px"></textarea>
        </td>
        <td class="td-master-id">
            <select class="form-control select2-tasks" name="tasks_status[]" style="width: 250px">
                <option value="">Select Status</option>
                <option value="Next Tasks">Next Tasks</option>
                <option value="Completed">Completed</option>
            </select>
        </td>
        <td>
            <button type="button" class="btn-delete-row-tasks btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Delete
            </button>
        </td>
    </tr>
    </tbody>
</table>