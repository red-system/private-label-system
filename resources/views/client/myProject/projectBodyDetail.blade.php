<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Order ID</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8">{{$project->project_order_number}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Channel</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="nama_package_edit">{{$project->channel->channel_name}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Order Title</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{$project->project_order_title}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Project Type</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{$project->project_type->project_type_name}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Order Date</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{\Carbon\Carbon::parse($project->project_order_date)->format('d-m-Y')}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Project Timeline</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8"
           id="harga_package_edit">{{$project->project_timeline.' Days'}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Status</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$project->project_status->project_status_name}}</label>
</div>
<div class="form-group m-form__group row">
    <label class="form-control-label col-3">Progress</label>
    <label class="form-control-label col-1">:</label>
    <label class="form-control-label col-8" id="harga_package_edit">{{$project->project_progress}}</label>
</div>