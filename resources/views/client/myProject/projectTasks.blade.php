<div class="modal fade" id="modal-tasks" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <h3 class="modal-title m--font-light" id="exampleModalLabel"><strong>Project Tasks</strong></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 500px; overflow-y: auto;">

            </div>
            <div class=" modal-footer">
                <button type="button" class="btn btn-secondary btn-batal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

