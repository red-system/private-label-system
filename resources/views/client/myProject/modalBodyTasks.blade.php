<div class="row">
    <input type="hidden" name="project_id" value="{{$project->project_id}}">
    <div class="col-9">
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Order Title</label>
            <div class="col-7 col-form-label">
                <span class="no_bukti">{{$project->project_order_title}}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Order Date</label>
            <div class="col-7 col-form-label">
                <span class="no_bukti">{{ Main::format_date($project->project_order_date) }}</span>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-2 col-form-label">Status</label>
            <div class="col-7 col-form-label">
                <span class="project_status_name">{{$project->project_status->project_status_name}}</span>
            </div>
        </div>
    </div>
</div>

<hr/>
<br>
<label for="example-text-input" class="col-form-label"><strong>Next Tasks</strong></label>
<table class="table-next-tasks table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Task Name</th>
        <th>Task Description</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($next_tasks as $n)
        <tr data-index="">
            <td class="td-task-name">
                {{$n->tasks_name}}
            </td>
            <td class="td-tasks-desc">
                {{$n->tasks_desc}}
            </td>
            <td class="td-master-id">
                {{$n->tasks_status}}

            </td>
        </tr>
    @endforeach
    @if($count_next_tasks == 0)
        <tr>
            <td colspan="3" style="text-align: center">There is no data</td>
        </tr>
    @endif
    </tbody>
</table>
<br/>
<br/>
<label for="example-text-input" class="col-form-label"><strong>Completed Tasks</strong></label>
<table class="table-next-tasks table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Task Name</th>
        <th>Task Description</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($completed as $c)
        <tr data-index="">
            <td class="td-task-name">
                {{$c->tasks_name}}
            </td>
            <td class="td-tasks-desc">
                {{$c->tasks_desc}}
            </td>
            <td class="td-master-id">
                {{$c->tasks_status}}

            </td>
        </tr>
    @endforeach
    @if($count_completed == 0)
        <tr>
            <td colspan="3" style="text-align: center">There is no data</td>
        </tr>
    @endif
    </tbody>
</table>
