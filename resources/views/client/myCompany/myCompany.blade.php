@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>

@endsection

@section('body')
    <div class="container">
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">

                    <form action="{{ route('companyDataUpdate') }}" method="post"
                          class="m-form form-send"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="company_id" value="{{$company->company_id}}">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Name </label>
                            <input type="text" class="form-control m-input" name="company_name" style="width: 500px"
                                   value="{{$company->company_name}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Email </label>
                            <input type="text" class="form-control m-input" name="company_email" style="width: 500px"
                                   value="{{$company->company_email}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Phone </label>
                            <input type="text" class="form-control m-input" name="company_phone" style="width: 500px"
                                   value="{{$company->company_phone}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Address </label>
                            <textarea class="form-control m-input" name="company_address"
                                      style="width: 500px">{{$company->company_address}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Overview </label>
                            <textarea class="form-control m-input"
                                      name="company_overview" style="width: 500px">{{$company->company_overview}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Company Data </label>
                            <input type="text" class="form-control m-input" name="company_data" style="width: 500px"
                                   value="{{$company->company_data}}">
                        </div>

                        <div class="produksi-buttons">
                            <span>
                                <button type="submit"
                                        class="btn-simpan btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-check"></i>Update Data</span></button>
                                    </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
