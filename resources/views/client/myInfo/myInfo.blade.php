@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>

@endsection

@section('body')
    <div class="container">
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h3 class="subheader-title text-dark font-weight-bold my-2 mr-3">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">

                    <form action="{{ route('clientDataUpdate') }}" method="post"
                          class="m-form form-send"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="client_id" value="{{$client->client_id}}">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Name </label>
                            <input type="text" class="form-control m-input" name="client_name" style="width: 500px"
                                   value="{{$client->client_name}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Contact </label>
                            <input type="text" class="form-control m-input" name="client_contact" style="width: 500px"
                            value="{{$client->client_contact}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Email </label>
                            <input type="text" class="form-control m-input" name="client_email" style="width: 500px"
                            value="{{$client->client_email}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Description </label>
                            <textarea class="form-control m-input" name="client_desc" style="width: 500px">{{$client->client_desc}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Client Data </label>
                            <input type="text" class="form-control m-input" name="client_data" style="width: 500px"
                                   value="{{$client->client_data}}">
                        </div>

                        <div class="produksi-buttons">
                            <span>
                                <button type="submit"
                                        class="btn-simpan btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-check"></i>Update Data</span></button>
                                    </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
